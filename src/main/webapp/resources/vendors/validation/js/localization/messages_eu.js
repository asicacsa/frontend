(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else if (typeof module === "object" && module.exports) {
		module.exports = factory( require( "jquery" ) );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: EU (Basque; euskara, euskera)
 */
$.extend( $.validator.messages, {
	required: "Eremu hau beharrezkoa da.",
	remote: "Meaddressz, bete eremu hau.",
	email: "Meaddressz, idatzi baliozko posta helbide bat.",
	url: "Meaddressz, idatzi baliozko URL bat.",
	date: "Meaddressz, idatzi baliozko data bat.",
	dateISO: "Meaddressz, idatzi baliozko (ISO) data bat.",
	number: "Meaddressz, idatzi baliozko zenbaki oso bat.",
	digits: "Meaddressz, idatzi digituak soilik.",
	creditcard: "Meaddressz, idatzi baliozko txartel zenbaki bat.",
	equalTo: "Meaddressz, idatzi berdina berriro ere.",
	extension: "Meaddressz, idatzi onartutako luzapena duen balio bat.",
	maxlength: $.validator.format( "Meaddressz, ez idatzi {0} karaktere baino gehiago." ),
	minlength: $.validator.format( "Meaddressz, ez idatzi {0} karaktere baino gutxiago." ),
	rangelength: $.validator.format( "Meaddressz, idatzi {0} eta {1} karaktere arteko balio bat." ),
	range: $.validator.format( "Meaddressz, idatzi {0} eta {1} arteko balio bat." ),
	max: $.validator.format( "Meaddressz, idatzi {0} edo txikiagoa den balio bat." ),
	min: $.validator.format( "Meaddressz, idatzi {0} edo handiagoa den balio bat." )
} );

}));
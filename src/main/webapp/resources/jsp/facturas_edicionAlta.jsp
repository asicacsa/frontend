<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<%@ page import="java.net.*" %> 
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!-- jsp:directive.page language="java" contentType="text/html" charset="charset=UTF-8" pageEncoding="charset=UTF-8""/-->
	
<HTML>
<head>

  <title> Envio de correos </title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  
	<STYLE>
		<%@ include file="css/modelocorreo.css" %>
	</STYLE>
	
<%
	// parametros

	 String idmodelo = request.getParameter("idmodelo");
	 String titulo = request.getParameter("titulo");
	
	 String nombre = request.getParameter("nombre");
	 //String descripcion = URLDecoder.decode(request.getParameter("descripcion"), "UTF-8");
	 String descripcion = request.getParameter("descripcion");
	 
	 String remitente = request.getParameter("remitente");
	 String asunto = request.getParameter("asunto");
	 String contenido = request.getParameter("contenido");
	 
	 //out.print(contenido);
%>
	
  <script language="javascript" type="text/javascript" src="editorHTML/jscripts/tiny_mce/tiny_mce_src.js"></script>
  	
	<script language="javascript" type="text/javascript">
		function getFormData(form) {
			

  			//recupero los valores de los elementos del formulario
            var idmodelo = document.getElementById("idmodelo").getAttribute("value");
            var nombre = document.getElementById("nombre").getAttribute("value");
            var descripcion = document.getElementById("descripcion").getAttribute("value");
            var remitente = document.getElementById("remitente").getAttribute("value");
            var asunto = document.getElementById("asunto").getAttribute("value");
            
            var contenido = document.getElementById("contenido").value;
            //le pongo el cdata pq no se guarda el la BD
            var newContent = '<![CDATA[' +contenido+ ']]>';

            //compruebo que Nombre y Asunto no sean Nulos
            if (nombre == ''){
		       alert("El Nombre del Modelo es obligatorio");
		       return false;
		    }    	
            if (asunto == ''){
		       alert("El Asunto del Modelo es obligatorio");
		       return false;
		    }    	

			//los asigno al node que he creado en la pagina laszlo           	
          	window.opener.document.getElementById('lzapp').SetVariable('actualizarModeloCorreo.idmodelo',idmodelo);
           	window.opener.document.getElementById('lzapp').SetVariable('actualizarModeloCorreo.nombre',nombre);
           	window.opener.document.getElementById('lzapp').SetVariable('actualizarModeloCorreo.descripcion',descripcion);
           	window.opener.document.getElementById('lzapp').SetVariable('actualizarModeloCorreo.remitente',remitente);
          	window.opener.document.getElementById('lzapp').SetVariable('actualizarModeloCorreo.asunto',asunto);
           	window.opener.document.getElementById('lzapp').SetVariable('actualizarModeloCorreo.contenido',newContent);
			window.opener.document.getElementById('lzapp').SetVariable('actualizarModeloCorreo.userCancelledFileTransfer',false);
          	
           	window.close();

			return false;
		           	
		} 
		function closeForm(){
			
			window.opener.document.getElementById('lzapp').SetVariable('actualizarModeloCorreo.userCancelledFileTransfer',true);
			window.close();
			
		}
		function insertAtCursor(myField, myValue){
			//IE support
			
			if (document.selection){				
				
				document.getElementById("mce_editor_0").contentWindow.document.body.focus();
				sel = document.selection.createRange();
				sel.text = "$" + myValue;
				
			}else if (myField.selectionStart || myField.selectionStart == '0') {
					
				    // MOZILLA/NETSCAPE support
				    var startPos = myField.selectionStart;
				    var endPos = myField.selectionEnd;
				    myField.value = myField.value.substring(0, startPos)
				    + myValue
				    + myField.value.substring(endPos, myField.value.length);					
		    } else {
				    myField.value += myValue;
				   
			}			
		   
		}
		
		function activarEventosTiny () {
			var event = {type : "submit", target : document.getElementById("modelocorreo")};
			tinyMCE.handleEvent(event);
		}
		
  	</script>


	<script language="javascript" type="text/javascript">
	   var sIdioma = "";   
	   sIdioma = "es";
	   
	   
	   tinyMCE.init({
			language : sIdioma,
			theme_advanced_toolbar_location : "top",
			mode : "exact",
			elements: "contenido",			
			theme : "advanced",			
			width : 400,
			height : 200,
			methosPostHandleEvent : getFormData,
			ask : false,
			//content_css : "../css/mfao.css",
			plugins : "advimage,advlink,insertdatetime,preview",
			theme_advanced_buttons1_add : "fontselect,fontsizeselect,forecolor",
			theme_advanced_buttons2_add_before: "cut,copy,paste,separator",
			theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview",
			plugin_insertdate_dateFormat : "%d/%m/%Y",
			plugin_insertdate_timeFormat : "%H:%M:%S",
			extended_valid_elements : "td[*], div[*],iframe[*|frameborder=0],a[alt|class|name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],font[face|size|color],hr[class|width|size|noshade]"
		});
	</script>

</head>

<body onload="activarEventosTiny()">
	
	<form name="modelocorreo" id="modelocorreo" onsubmit="return false;">
		<input type="hidden" id="idmodelo" maxlenght="70" value="<%=idmodelo%>" size="70"/>
		<table border="0" cellspacing="3">
			<p class="title"><%=titulo%></p>				
			<tr >
				<td class="label"><text>Nombre*</text></td>
				<td><input type="text" class="field" id="nombre" maxlenght="70" value="<%=nombre%>" size="70"/></td>
			</tr>			
			<tr>
				<td valign="top" class="label"><text>Descripción</text></td>
				
				<td><textarea class="field" id="descripcion" cols="69" rows="4"><%=descripcion%></textarea></td>
			</tr>
			<tr>
				<td class="label"><text>Remitente</text></td>
				<td><input type="text" class="field" id="remitente" maxlenght="70" value="<%=remitente%>" size="70"/></td>
			</tr>
			<tr>
			
				<td class="label"><text>Asunto*</text></td>
				<td><input type="text" class="field" id="asunto" maxlenght="70" value="<%=asunto%>" height="20" size="70"/></td>
			</tr>
			<tr>
				<td class="label"><text valign="top">Datos cliente</text></td>
				<td>
					<select class="field" name="datoscliente">
						<option value="{nombre}">Nombre</option>
						<option value="{apellido1}">Apellido 1</option>
						<option value="{apellido2}">Apellido 2</option> 
					</select>
					
					<button onClick="insertAtCursor(contenido,document.modelocorreo.datoscliente.options[document.modelocorreo.datoscliente.selectedIndex].value);"> <img src="flecha.gif"> </button>
				</td>
			</tr>			 
			<tr>
				<td valign="top" class="label"><text>Contenido</text></td>
				<td>
					<textarea name="micontenido" id="contenido"><%=contenido%>
					
					</textarea>
				</td>			
			</tr>			 
			<tr>
				<td colspan="2" align="right">
					<input type=submit name="actualizarcorreo" value="aceptar">
				
				<input type=button name="cerrar" onclick="closeForm()" value="cancelar"></td>

			</tr>			 
		</table>
		
	</form>
</body>


</HTML>


PNotify.prototype.options.styling = "bootstrap3";
PNotify.prototype.options.styling = "fontawesome";

function sortJSON(data, prop, asc) {
    data = data.sort(function(a, b) {
        if (asc) {
            return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
        } else {
            return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
        }
    });
    
    return(data);
}

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {   
    document.cookie = name+'=; Max-Age=-99999999;';  
}

// *******************************************************************************************
$(document).ready(function() {
	
	// Activar el comportamiento responsive en las datatables que estan inicialmente ocultas en pesta�as
	// Mas info: https://datatables.net/forums/discussion/28234/responsive-issues-with-multiple-tables-and-bootstrap-tabs
    $("a[data-toggle=\"tab\"]").on("shown.bs.tab", function (e) {
         $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust()
           .responsive.recalc();
     });
    
    if (getCookie("hide_lateral_menu")=="true") {
    	$('a#menu_toggle').click();
    }
    
    $('#main-menu-lateral').show();
    
	$('a#menu_toggle').on("click", function() {
		$("ul.nav.side-menu li.menu_option.active-sm ul.nav.child_menu").hide();
		if (getCookie("hide_lateral_menu")=="true") 
			setCookie("hide_lateral_menu","false",10);
		else
			setCookie("hide_lateral_menu","true",10);
	});

	$('ul.nav.side-menu li.menu_option').on("click", function() {
		$("ul.nav.side-menu li.menu_option.active-sm").removeClass("active-sm");
	});

	$('[data-toggle="tooltip"]').tooltip();

	$('.js-example-basic-single').select2();

	$(document).on('click', 'input', function(e) {
		$("input").each(function() {
			$("#" + this.id).popover('hide');
		});
	});

	$(document).on('click', 'select', function(e) {
		$("select").each(function() {
			$("#" + this.id).popover('hide');
		});
	});
});


//*******************************************************************************************
//Comprueba si los campos de texto de un formulario tienen datos o no (para evitar llamar a base de datos sin filtrar)
function isFormEmpty(form_selector) {
	
	var empty=true;
	$(form_selector+' input').each(function(){
		if ($(this).attr("type")=="text") {			
			if ($(this).val().length>0) empty=false;
		}
	});

	$(form_selector+' select').each(function(){
		if ($(this).val().length>0) empty=false;
	});

	return(empty);
}

// *******************************************************************************************
// Configura el tama�o de la ventana modal de dialogo
function setModalDialogSize(dialog_id, size) {

	$('[data-toggle="tooltip"]').tooltip();
	
	$(dialog_id).removeClass("md-dialog");
	$(dialog_id).removeClass("sm-dialog");
	$(dialog_id).removeClass("xs-dialog");
	if (size.length > 0)
		$(dialog_id).addClass(size + "-dialog");
}

// *******************************************************************************************
// Si el JSON no contiene un elemento de tipo array lo convierte a array y
// adem�s detecta JSON nulos
function sanitizeJSON(data) {

	if (data != null && typeof data != "undefined") {
		if (!Array.isArray(data))
			data = [ data ];
		return data;
	}

	return ("");
}

// *******************************************************************************************
// Sanitiza un array copiando al nuevo array s�lo uno de los campos de los
// objetos del array original
function sanitizeArray(data, field) {

	var result = [];
	for (var i = 0; i < data.length; i++) {
		var val = eval("data[i]." + field);
		result.push(val);
	}

	return (result);
}

// *******************************************************************************************
// Activa los tooltips con listado en las datatables
function activateTooltipsInTable(id_datatable) {
	
	$('#'+id_datatable+' [data-toggle="tooltip-on-click"]').not('[tooltip-active]').tooltip({
		trigger : "manual",
		placement : "bottom",
		html : true
	});
	$('#'+id_datatable+' [data-toggle="tooltip-on-click"]').not('[tooltip-active]').on('click', function() {
		$(this).addClass('tooltip-activated');
		$('[data-toggle="tooltip-on-click"]:not(.tooltip-activated)').tooltip('hide');
		$(this).tooltip('toggle');
		$(this).removeClass('tooltip-activated');
	});

	$('#'+id_datatable+' [data-toggle="tooltip-on-click"]').attr("tooltip-active","true");
	
	// Esto se hace para desactivar el bot�n de siguiente en las tablas serverside (se pone aqu� por conveniencia)
	var info = $("#"+id_datatable).DataTable().page.info();	
	if (info.serverSide) {
    	var currentRowsView=info.length;
    	$("#"+id_datatable+"_next").removeClass("disabled");    	    	
    	if ($("#"+id_datatable).DataTable().rows().count()<currentRowsView) $("#"+id_datatable+"_next").addClass("disabled");
	}	
}

// *******************************************************************************************
// Muestra un tooltip con una lista de datos en una celda de una datatable
function showListInCell(data, itemsName, objectName, tooltipName) {
		if (data != null && typeof data != "undefined") {

		if (Array.isArray(data)) {
			if (data.length > 0) {
				var list = "";
				for (var i = 0; i < data.length; i++) {
					list = list + "<span>" + eval("data[i]." + objectName) + "</span>\n";
				}

				return '<span class="list_data" data-toggle="tooltip-on-click" title="' + list + '">' + data.length + ' ' + itemsName + '</span>';
			} else
				return "";
		} else {
			if (data != "")
				return '<span class="list_data" data-toggle="tooltip-on-click" title="' + eval("data." + objectName) + '">1 ' + itemsName + '</span>';
		}
	} else
		return "";
}

//*******************************************************************************************
//Muestra una lista de datos en una celda de una datatable
function showMultilineListInCell(data, objectName1, objectName2) {
	if (data != null && typeof data != "undefined") {		
		if (Array.isArray(data)) {		
			if (data.length > 0) {
				var list = "";
				for (var i = 0; i < data.length; i++) {	
					if (objectName2!="")
					list = list + "<p>" + eval("data[i]." + objectName1) +" " + eval("data[i]." + objectName2) + "</p>\n";
					else
						list = list + "<p>" + eval("data[i]." + objectName1)  + "</p>\n";
				}
				return list;
			} else
				return "";
		} else {
			if (data != "")
				return '';
		}
	} else
		return "";
}

// *******************************************************************************************
$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name] !== undefined) {
			if (!o[this.name].push) {
				o[this.name] = [ o[this.name] ];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

// *******************************************************************************************
// Internacionalizaci�n del validador de JQuery
$.extend($.validator.messages, {
	required : "Este campo es obligatorio.",
	remote : "Por favor, rellena este campo.",
	email : "Por favor, escribe una direcci�n de correo v�lida.",
	url : "Por favor, escribe una URL v�lida.",
	date : "Por favor, escribe una fecha v�lida.",
	dateISO : "Por favor, escribe una fecha (ISO) v�lida.",
	number : "Por favor, escribe un n�mero v�lido.",
	digits : "Por favor, escribe s�lo d�gitos.",
	creditcard : "Por favor, escribe un n�mero de tarjeta v�lido.",
	equalTo : "Por favor, escribe el mismo valor de nuevo.",
	extension : "Por favor, escribe un valor con una extensi�n aceptada.",
	maxlength : $.validator.format("Por favor, no escribas m�s de {0} caracteres."),
	minlength : $.validator.format("Por favor, no escribas menos de {0} caracteres."),
	rangelength : $.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
	range : $.validator.format("Por favor, escribe un valor entre {0} y {1}."),
	max : $.validator.format("Por favor, escribe un valor menor o igual a {0}."),
	min : $.validator.format("Por favor, escribe un valor mayor o igual a {0}."),
	nifES : "Por favor, escribe un NIF v�lido.",
	nieES : "Por favor, escribe un NIE v�lido.",
	cifES : "Por favor, escribe un CIF v�lido."
});

// *******************************************************************************************
// Internacionalizaci�n del selector de fechas DateRangePicker. El cancelLabel
// lo ponemos como
// borrar porque utilizamos la funcionalidad autoUpdateInfo=false, de otra
// manera no se podr�an
// limpiar las fechas de la entrada y el uso resulta m�s engorroso.
$daterangepicker_sp = {
	"format" : "DD/MM/YYYY",
	"separator" : " - ",
	"applyLabel" : "Aplicar",
	"cancelLabel" : "Borrar",
	"fromLabel" : "Desde",
	"toLabel" : "Hasta",
	"customRangeLabel" : "Personalizado",
	"daysOfWeek" : [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
	"monthNames" : [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
	"firstDay" : 1
};

// *******************************************************************************************
// Aplicaciones por defecto del Sistema, para el di�logo de edici�n de Perfiles en Admon / Seguridad

$appdefecto = [ 
	{value:"/application/admon/espacios/espacios.lzx", desc:"Adm. Gest. Recintos"},
	{value:"/application/admon/seguridad/seguridad.lzx", desc:"Adm. Seguridad"},
	{value:"/application/admon/productostarifas/productostarifas.lzx", desc:"Adm. Productos, tarifas y canales"},
	{value:"/application/admon/programacion/programacion.lzx", desc:"Adm. Programaci�n"},
	{value:"/application/ventareserva/operacionescaja/operacionescaja.lzx", desc:"Superv. de caja"},
	{value:"/application/ventareserva/ventareserva.lzx", desc:"Venta y reserva"},
	{value:"/application/facturacionclientes/facturacionclientes.lzx", desc:"Gesti�n facturas y clientes"},
	{value:"/application/documentacion/documentacion.lzx", desc:"Gesti�n documentaci�n"}
]

// *******************************************************************************************
// Funci�n usada en datatables para que ordene sin tener en cuenta los acentos
function removeAccents(data) {
	return !data ? '' : typeof data === 'string' ? data.replace(/\n/g, ' ').replace(/[�àäâ]/g, 'a').replace(/[éèëê]/g, 'e').replace(/[�ìïî]/g, 'i').replace(/[�òöô]/g, 'o').replace(/[�ùüû]/g, 'u') : data;
}

jQuery.extend(jQuery.fn.dataTableExt.oSort, {
	"spanish-string-asc" : function(s1, s2) {
		if (s1 != null)
			return s1.toString().localeCompare(s2);
	},
	"spanish-string-desc" : function(s1, s2) {
		if (s2 != null)
			return s2.toString().localeCompare(s1);
	}
});

jQuery.fn.DataTable.ext.type.search['spanish-string'] = function(data) {
	return removeAccents(data);
}

jQuery.fn.dataTable.Api.register('processing()', function(show) {
	return this.iterator('table', function(ctx) {
		ctx.oApi._fnProcessingDisplay(ctx, show);
	});
});

// *******************************************************************************************
function generateCSRFHeaders() {

	// Por defecto Spring Security tiene activado protecci�n CSRF por lo que es
	// necesario pasar el token a la petici�n AJAX
	// El token se inserta en un meta tag con la tag <sec:csrfMetaTags/> y se
	// recogen aqu� sus valores
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	var csrfToken = $("meta[name='_csrf']").attr("content");

	var headers = {};
	headers[csrfHeader] = csrfToken;

	return (headers);
}



//*******************************************************************************************
$.fn.clientField = function(url, campoDevueltoNombre, campoDevueltoId, campoDevueltoCif, campoDevueltoCp, campoDevueltoEmail, campoDevueltoTelefono) {

	var id=$(this).attr('id');
	$(this).before("<button id='"+id+"-search-client-button' type='button' class='btn btn-default btn-i18n'><span class='docs-tooltip' data-toggle='tooltip' title='' data-original-title='Buscar cliente'><span class='fa fa-search'></span></span></button>");	
	$(this).wrap( "<div class='btn-client-wrapper'></div>" );
	
	$("#"+id+"-search-client-button").on("click", function(e) {
		
         $("#modal-dialog-form-buscar-cliente .modal-content").load(url, function() {                                                                  
                $("#modal-dialog-form-buscar-cliente").modal('show');
        	 	setModalDialogSize("#modal-dialog-form-buscar-cliente", "lg");
        	 	$("#modal-dialog-form-buscar-cliente input#campoBuscadorClienteDevueltoNombre").val(campoDevueltoNombre);
        	 	$("#modal-dialog-form-buscar-cliente input#campoBuscadorClienteDevueltoId").val(campoDevueltoId);
        	 	$("#modal-dialog-form-buscar-cliente input#campoBuscadorClienteDevueltoCif").val(campoDevueltoCif);        	 	
        	 	$("#modal-dialog-form-buscar-cliente input#campoBuscadorClienteDevueltoCp").val(campoDevueltoCp);        	 	
        	 	$("#modal-dialog-form-buscar-cliente input#campoBuscadorClienteDevueltoEmail").val(campoDevueltoEmail);        	 	
        	 	$("#modal-dialog-form-buscar-cliente input#campoBuscadorClienteDevueltoTelefono").val(campoDevueltoTelefono);        	 	
         });
       
	});

}
	


//*******************************************************************************************
$.fn.i18nField = function(hiddenField, i18nlangs) {

	var id=$(this).attr('id');
	$(this).before("<button id='"+id+"-i18n-button' type='button' class='btn btn-default btn-i18n'><span class='docs-tooltip' data-toggle='tooltip' title='' data-original-title='Internacionalizaci�n'><span class='fa fa-globe'></span></span></button>");	
	$(this).wrap( "<div class='btn-i18n-wrapper'></div>" );
	
	var languagesArray=$(hiddenField).val().split("^");

	var values = new Array();
	var ids = new Array();

	// Generar array a partir de los datos obtenidos por el servicio (esto elimina los duplicados, se queda con el �ltimo valor en ese caso)
	$.each(languagesArray, function(i, item) {
		
		var data=item.split("~");
		
		var id = "";
		var ididioma = "";
		var value = "";
		
		if (data[0] != null && typeof data[0]!="undefined")	id = data[0];	
		if (data.length > 1) ididioma = data[1];		
		if (data.length > 2) value = data[2];
			
		if (value.length > 0 && ididioma!=4) {
	    	values[ididioma]=value;	    
	    	ids[ididioma]=id;	    	
		}

	});
	
	if (values.length>0) $("#"+id+"-i18n-button").css("background-color", "yellow");			
	
	var html="<div id='"+id+"-i18n-popover' class='i18n-popover'>";
	for (var i = 0; i < i18nlangs.ArrayList.LabelValue.length; i++){
	    var obj = i18nlangs.ArrayList.LabelValue[i];
	    if (obj.default!=1) {
	    	var v=values[obj.value];
		    if (v==null || typeof v=="undefined") v="";	    		
	    	var s=ids[obj.value];
		    if (s==null || typeof s=="undefined") s="";	    		
	    	html+="<div class='row'><label for='"+obj.value+"'>"+obj.label+"</label><input type='text' name='"+obj.value+"' id='"+obj.value+"' value='"+v+"' idi18n='"+s+"'/></div>";
	    }
	}
	
	html+="<button id='"+id+"-i18n-popover-button' type='button' class='btn btn-primary btn-xs pull-right'>Guardar</button>";
	html+="<div>";
	
	$("#"+id+"-i18n-button").popover({	       
        placement: 'bottom',
        title: 'Internacionalizaci�n',
        html: true,
        content:  html
    }).on('click', function(e){    	    	
       $(this).popover();
       
       if (e.originalEvent != null && typeof e.originalEvent != "undefined") {
    	   document.dispatchEvent(new CustomEvent("event-show-i18n-"+id,{}));
       }
    })
  .on("show.bs.popover", function(){ 	  
	  $(this).data("bs.popover").tip().css("min-width", "300px");
  })
  .on("shown.bs.popover", function(){ 	  
	  $("#"+id+"-i18n-popover input").each(function() {
		  	var v=values[$(this).attr("id")];
		    if (v==null || typeof v=="undefined") v="";
			$(this).val(v);
	  });	  
  });

	$(document).off("click","#"+id+"-i18n-popover-button");
	$(document).on("click","#"+id+"-i18n-popover-button",function(){

		$("#"+id+"-i18n-button").click();
		
		var s="";
		$("#"+id+"-i18n-popover input").each(function() {
			values[$(this).attr("id")]=$(this).val();
			s+=$(this).attr("idi18n")+"~"+$(this).attr("id")+"~"+$(this).val()+"^";
  	  	});	 

		$(hiddenField).val(s);
		
		document.dispatchEvent(new CustomEvent("event-save-i18n-"+id,{"detail":s}));

	});		
	
}

//*******************************************************************************************
$.fn.i18nField_Encuesta = function(hiddenField, i18nlangs) {

	var id=$(this).attr('id');
	$(this).before("<button id='"+id+"-i18n-button' type='button' class='btn btn-default btn-i18n'><span class='docs-tooltip' data-toggle='tooltip' title='' data-original-title='Internacionalizaci�n'><span class='fa fa-globe'></span></span></button>");	
	$(this).wrap( "<div class='btn-i18n-wrapper'></div>" );
	
	var languagesArray=$(hiddenField).val().split("^");

	var values = new Array();
	var ids = new Array();

	// Generar array a partir de los datos obtenidos por el servicio (esto elimina los duplicados, se queda con el �ltimo valor en ese caso)
	$.each(languagesArray, function(i, item) {
		
		var data=item.split("~");
		
		var id = "";
		var ididioma = "";
		var value = "";
		
		if (data[0] != null && typeof data[0]!="undefined")	id = data[0];	
		if (data.length > 1) ididioma = data[1];		
		if (data.length > 2) value = data[2];
			
		if (value.length > 0) {
	    	values[ididioma]=value;	    
	    	ids[ididioma]=id;	    	
		}

	});
	
	if (values.length>0) $("#"+id+"-i18n-button").css("background-color", "yellow");			
	
	var html="<div id='"+id+"-i18n-popover' class='i18n-popover'>";
	for (var i = 0; i < i18nlangs.ArrayList.LabelValue.length; i++){
	    var obj = i18nlangs.ArrayList.LabelValue[i];
	    if (obj.default!=1) {
	    	var v=values[obj.value];
		    if (v==null || typeof v=="undefined") v="";	    		
	    	var s=ids[obj.value];
		    if (s==null || typeof s=="undefined") s="";	    		
	    	html+="<div class='row'><label for='"+obj.value+"'>"+obj.label+"</label><input type='text' name='"+obj.value+"' id='"+obj.value+"' value='"+v+"' idi18n='"+s+"'/></div>";
	    }
	}
	
	html+="<button id='"+id+"-i18n-popover-button' type='button' class='btn btn-primary btn-xs pull-right'>Guardar</button>";
	html+="<div>";
	
	$("#"+id+"-i18n-button").popover({	       
        placement: 'bottom',
        title: 'Internacionalizaci�n',
        html: true,
        content:  html
    }).on('click', function(e){    	    	
       $(this).popover();
       
       if (e.originalEvent != null && typeof e.originalEvent != "undefined") {
    	   document.dispatchEvent(new CustomEvent("event-show-i18n-"+id,{}));
       }
    })
  .on("show.bs.popover", function(){ 	  
	  $(this).data("bs.popover").tip().css("min-width", "300px");
  })
  .on("shown.bs.popover", function(){ 	  
	  $("#"+id+"-i18n-popover input").each(function() {
		  	var v=values[$(this).attr("id")];
		    if (v==null || typeof v=="undefined") v="";
			$(this).val(v);
	  });	  
  });

	$(document).off("click","#"+id+"-i18n-popover-button");
	$(document).on("click","#"+id+"-i18n-popover-button",function(){

		$("#"+id+"-i18n-button").click();
		
		var s="";
		$("#"+id+"-i18n-popover input").each(function() {
			values[$(this).attr("id")]=$(this).val();
			s+=$(this).attr("idi18n")+"~"+$(this).attr("id")+"~"+$(this).val()+"^";
  	  	});	 

		$(hiddenField).val(s);
		
		document.dispatchEvent(new CustomEvent("event-save-i18n-"+id,{"detail":s}));

	});		
	
}

//*******************************************************************************************
function showFieldSpinner(element) {

	showSpinner(element, 9, 5, 2, 5, "14px", "50%", "relative");
}

//*******************************************************************************************
function showButtonSpinner(element) {

	showSpinner(element, 9, 4, 2, 4, "10px", "50%", "relative");
}	


//*******************************************************************************************
function showButtonTextSpinner(element) {

	showSpinner(element, 9, 4, 2, 4, "10px", "50%", "relative");
}	


//*******************************************************************************************
function showSpinner(element) {
	showSpinner(element, 15, 25, 4, 21, "50%", "50%", "absolute");
}

// *******************************************************************************************
function showSpinner(element, lines, length, width, radius, top, left, position) {

	var opts = {
		lines : lines // The number of lines to draw
		,
		length : length // The length of each line
		,
		width : width // The line thickness
		,
		radius : radius // The radius of the inner circle
		,
		scale : 1 // Scales overall size of the spinner
		,
		corners : 1 // Corner roundness (0..1)
		,
		color : '#000' // #rgb or #rrggbb or array of colors
		,
		opacity : 0.25 // Opacity of the lines
		,
		rotate : 0 // The rotation offset
		,
		direction : 1 // 1: clockwise, -1: counterclockwise
		,
		speed : 1 // Rounds per second
		,
		trail : 60 // Afterglow percentage
		,
		fps : 20 // Frames per second when using setTimeout() as a fallback
					// for CSS
		,
		zIndex : 2e9 // The z-index (defaults to 2000000000)
		,
		className : 'spinner' // The CSS class to assign to the spinner
		,
		top : top // Top position relative to parent
		,
		left : left // Left position relative to parent
		,
		shadow : false // Whether to render a shadow
		,
		hwaccel : false // Whether to use hardware acceleration
		,
		position : position // Element positioning
	}

	var wrap_name = $(element).attr('id') + "_wrapper";

	$(element).css('pointer-events','none');
	
	// Desactivar todos los elementos interactivos en la p�gina
	$(element).wrapInner("<div id='" + wrap_name + "' style='pointer-events:none; opacity:0.2;'></div>")

	var target = $(element).get(0);
	spinner = new Spinner(opts).spin(target);
	$(element).data('spinner', spinner);
}

// *******************************************************************************************
function insertSmallSpinner(element) {
	var opts = {
		lines : 9 // The number of lines to draw
		,
		length : 5 // The length of each line
		,
		width : 2 // The line thickness
		,
		radius : 5 // The radius of the inner circle
		,
		scale : 1 // Scales overall size of the spinner
		,
		corners : 1 // Corner roundness (0..1)
		,
		color : '#000' // #rgb or #rrggbb or array of colors
		,
		opacity : 0.25 // Opacity of the lines
		,
		rotate : 0 // The rotation offset
		,
		direction : 1 // 1: clockwise, -1: counterclockwise
		,
		speed : 1 // Rounds per second
		,
		trail : 60 // Afterglow percentage
		,
		fps : 20 // Frames per second when using setTimeout() as a fallback
					// for CSS
		,
		zIndex : 2e9 // The z-index (defaults to 2000000000)
		,
		className : 'spinner' // The CSS class to assign to the spinner
		,
		top : '50%' // Top position relative to parent
		,
		left : '50%' // Left position relative to parent
		,
		shadow : false // Whether to render a shadow
		,
		hwaccel : false // Whether to use hardware acceleration
		,
		position : 'absolute' // Element positioning
	}

	var target = $(element).get(0);
	$(element).empty();
	spinner = new Spinner(opts).spin(target);
	$(element).data('spinner', spinner);
}

// *******************************************************************************************
function hideSpinner(element) {

	$(element).css('pointer-events','auto');

	if ($(element).data('spinner') != null)
		$(element).data('spinner').stop();

	var wrap_name = $(element).attr('id') + "_wrapper";

	// Activar todos los elementos interactivos en la p�gina
	$("#" + wrap_name).contents().unwrap();
}



// *******************************************************************************************
function json2xml(o, tab) {
	   var toXml = function(v, name, ind) {
	      var xml = "";
	      if (v instanceof Array) {
	         for (var i=0, n=v.length; i<n; i++)
	            xml += ind + toXml(v[i], name, ind+"\t") + "\n";
	      }
	      else if (typeof(v) == "object") {
	         var hasChild = false;
	         xml += ind + "<" + name;
	         for (var m in v) {
	            if (m.charAt(0) == "@")
	               xml += " " + m.substr(1) + "=\"" + v[m].toString() + "\"";
	            else
	               hasChild = true;
	         }
	         xml += hasChild ? ">" : "/>";
	         if (hasChild) {
	            for (var m in v) {
	               if (m == "#text")
	                  xml += v[m];
	               else if (m == "#cdata")
	                  xml += "<![CDATA[" + v[m] + "]]>";
	               else if (m.charAt(0) != "@")
	                  xml += toXml(v[m], m, ind+"\t");
	            }
	            xml += (xml.charAt(xml.length-1)=="\n"?ind:"") + "</" + name + ">";
	         }
	      }
	      else {
    		if (v != null && typeof v != "undefined") {
    			xml += ind + "<" + name + ">" + v.toString() +  "</" + name + ">";
    		}
    		else {
    			xml += ind + "<" + name + "></" + name + ">";    			
    		}
	      }
	      return xml;
	   }, xml="";
	   for (var m in o)
	      xml += toXml(o[m], m, "");
	   return tab ? xml.replace(/\t/g, tab) : xml.replace(/\t|\n/g, "");
	}

//*******************************************************************************************
//Ordena un array json por uno de los atributos

function sortjsonarray(arr,prop,order){

	  if (arr == null) {
	    return [];
	  }

	  if (!Array.isArray(arr)) {
	    throw new TypeError('sort-json-array expects an array.');
	  }

	  if (arguments.length === 1) {
	    return arr.sort();
	  }

	  if (arguments[2] == null || arguments[2] == "asc" ){
	    return arr.sort(compare(prop,1));
	  }
	  else if (arguments[2] == "des"){
	    return arr.sort(compare(prop,0));
	  }
	  else {
	    throw new TypeError('Wrong argument.');
	  }

	};

	function compare(attr,value){
	  if(value){
	    return function(a,b){
	      var x = (a[attr] === null) ? "" : "" + a[attr],
	      y = (b[attr] === null) ? "" : "" + b[attr];
	      return x < y ? -1 :(x > y ? 1 : 0)
	    }
	  }
	  else {
	    return function(a,b){
	      var x = (a[attr] === null) ? "" : "" + a[attr],
	      y = (b[attr] === null) ? "" : "" + b[attr];
	      return x < y ? 1 :(x > y ? -1 : 0)
	    }
	  }
	}
	
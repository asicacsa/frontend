<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
  <head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<link rel="icon" type="image/png" href="<c:url value='/resources/images/ticket_icon.png'/>" />
<meta charset="utf-8"> 
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><spring:message code="app.name" /> | <spring:message code="error.title" /></title>

<link rel="icon" href="<c:url value='/resources/images/favicons/favicon.ico'/>">
<link href="<c:url value='/resources/vendors/bootstrap/dist/css/bootstrap.min.css'/>" rel="stylesheet">	
<link href="<c:url value='/resources/vendors/font-awesome/css/font-awesome.min.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/css/custom.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/css/colossus.css'/>" rel="stylesheet">

</head>

  <body class="nav-md error-page">
    <div class="container body">
      <div class="main_container">
        <!-- page content -->
        <div class="col-md-12">
          <div class="col-middle">
            <div class="text-center">
            <br/>
              <h1 class="error-number">404</h1>              
				<img src="<c:url value='/resources/images/ghost_error_404.png'/>" />
				<br/>              
				<br/>
              <h2><spring:message code="error.404.title" /></h2>
              <h3><spring:message code="error.404.message" /></h3>
				<br/>
				<br/>

            <a href="<c:url value='/'/>" class="btn btn-round btn-primary"><spring:message code="error.404.button" /></a>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>

  </body>

</html>
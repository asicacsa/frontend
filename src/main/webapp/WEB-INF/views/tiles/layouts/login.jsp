<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<link rel="icon" type="image/png" href="<c:url value='/resources/images/ticket_icon.png'/>" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><spring:message code="app.name" /> | <spring:message code="login.title" /></title>

<link href="<c:url value='/resources/vendors/bootstrap/dist/css/bootstrap.min.css'/>" rel="stylesheet">	
<link href="<c:url value='/resources/vendors/font-awesome/css/font-awesome.min.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/css/custom.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/css/colossus.css'/>" rel="stylesheet">

</head>

<body style="background: #F7F7F7;">

	<div id="wrapper">
		<div id="login-panel" class="form">
			<section class="login_content">
				<form id="login-form">
					<h1>
						<img style="width: 100%;" src="<c:url value='/resources/images/cac_logo.png'/>" />
					</h1>
					<h1>
						<img style="width: 100%;" src="<c:url value='/resources/images/title_colossus.png'/>" />
					</h1>
					<div id="username-wrapper">
						<input type="text" class="form-control" id="username" name="username" placeholder="<spring:message code="app.general.input.username"/>" />
					</div>
					<div>
						<input type="password" class="form-control" id="password" name="password" placeholder="<spring:message code="app.general.input.password"/>" />
					</div>
					<div>
						<a class="btn btn-primary submit" href="#"><spring:message code="login.button.login" /></a>
					</div>

					<div class="separator">

						<div class="clearfix"></div>
						<br />
						<div>
							<p>
								<b><spring:message code="app.name" /></b>.
								<spring:message code="app.description"/>.
							</p>
						</div>
					</div>
				</form>
			</section>
		</div>
	</div>

	<script type="text/javascript" src="<c:url value='/resources/js/jquery-2.2.4.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/spin.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/vendors/bootstrap/dist/js/bootstrap.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/vendors/validation/js/jquery.validate.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/vendors/fastclick/lib/fastclick.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/vendors/pnotify/dist/pnotify.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/vendors/pnotify/dist/pnotify.buttons.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/vendors/pnotify/dist/pnotify.confirm.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/vendors/pnotify/dist/pnotify.history.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/vendors/select2/dist/js/select2.full.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/vendors/select2/dist/js/i18n/es.js'/>"></script>	
	<script type="text/javascript" src="<c:url value='/resources/vendors/datatables.net/js/jquery.dataTables.min.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/js/colossus.js'/>"></script>


	<script type="text/javascript">
	jQuery(document).ready(function($) {
		
		<c:if test="${param.timeout != null}">
			$(".dialog-timeout-user").modal('show');		 
		</c:if>
		
		$(document).on('click', 'input',  function(e){
			
			$("input").each(function() {
				$("#"+this.id).popover('hide');
			});						
		});				
														
		$('#password').popover({
			  placement: 'bottom',
			  offset: 20,
			  trigger: 'manual',
			  content: "<spring:message	code='login.jscript.enter_password'/>"
		});
		
		$('#username').popover({
			  placement: 'bottom',
			  offset: 20,
			  trigger: 'manual',
			  content: "<spring:message	code='login.jscript.enter_username'/>"
		});	
					
		$('#username-wrapper').popover({
			  placement: 'top',
			  offset: 20,
			  trigger: 'manual',
			  title: "Error",
			  content: '<spring:message	code="login.jscript.login_error"/>',
			  template: '<div class="popover popover-error"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'			  
		});		
				
		$("#login-form").validate({
              
			  onfocusout: false,
			  onkeyup: false,
			  rules: {
				  username: {
				      required: true
				    },
				   password: {
				    	required: true
				    }  
			  }
			  ,
			  messages: {
				  username: {
				    	required: "<spring:message	code='login.jscript.enter_username'/>"
				     	},
				    password: {
						    	required: "<spring:message	code='login.jscript.enter_password'/>"
						     	}
			  },
			  unhighlight: function(element, errClass) {
				    $(element).popover('hide');
				  },		
			  errorPlacement: function(err, element) {
				  err.hide();				  				  
				  $(element).attr('data-content', err.text());
				  $(element).popover('show');				  
			  	  },		
			  submitHandler: function(form) {
				    tryLogin();
			      }				  
			}
			
		);						

		$.validator.addMethod("passwordcheck", function(value) {
			   return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // La contrase�a puede tener estos caracteres
			       && /\d/.test(value) // La contrase�a debe tener un d�gito			      
				   && /[A-Za-z]/.test(value) // A�adir esto si quieres comprobar que tenga alguna letra
		});;		
				
		$("body").keydown(function(e) {
			
			// Si hay activa una ventana modal, ignorar el procesamiento de teclado
			if ( $("div.modal-backdrop").length > 0 ) {
				if ($("#username").is(":focus")) $('#username').popover('hide');
				if ($("#new_password").is(":focus")) $('#new_password').popover('hide');
				if ($("#repeat_password").is(":focus")) $('#repeat_password').popover('hide');
				$('#username-wrapper').popover('hide');
				return;
			}
			
			var key = e.which;
			if (key == 13)
				$("#login-form").submit();
		});		
		
		$("a.submit").on("click", function(event) {
	        $("a.submit").blur(); 
			$("#login-form").submit();
		});
			
	});

			
	function tryLogin() {
		// Obtener los datos antes de hace showSpinner() porque sino los campos est�n disabled y no recupera los datos
		showSpinner("#login-panel");		
						
		$.ajax({
			contentType: "application/x-www-form-urlencoded; charset=utf-8",
			type : "POST",
			url : "./login.action",
			data : {},
			timeout : 100000,
	
			beforeSend: function(xhr) {
				xhr.setRequestHeader('Authorization', 'Basic ' + window.btoa(unescape(encodeURIComponent($('#username').val()+ ':' + $('#password').val()))));
			},			
			success : function(data) {
				if (data!="null")
					{
					data=data.replace(".lzx",".do");
					data=data.replace("/application/","");
					window.location.replace("./"+data);
					}
				else
					window.location.replace("./");
			},
			error : function(data) {
				hideSpinner("#login-panel");
				$('#username-wrapper').popover("show");
			}
			});
		}

	</script>
			
	<div class="modal fade dialog-timeout-user" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div id="dialog-timeout-user-content" class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">�</span>
					</button>
					<h4 class="modal-title">
						<spring:message code="login.dialog.timeout_user.title" />
					</h4>
				</div>
						<div class="modal-body">
							<p>
								<spring:message code="login.dialog.timeout_user.text" />
							</p>
						</div>
						
							<br />
							<div class="modal-footer">
								<button type="button" class="btn btn-success" data-dismiss="modal"><spring:message code="common.button.close" /></button>
							</div>						
			</div>
		</div>
	</div>
		
</body>
</html>
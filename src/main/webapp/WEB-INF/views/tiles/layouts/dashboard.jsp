<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<link rel="icon" type="image/png" href="<c:url value='/resources/images/ticket_icon.png'/>" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">


<title><spring:message code="app.name" /> | <spring:message	code="dashboard.title" /></title>
				
<tiles:insertAttribute name="css" />
<tiles:insertAttribute name="scripts" />
</head>

<body class="nav-sm dashboard">
   
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col" id="main-menu-lateral" style="display:none;">
				<div class="left_col scroll-view">
				
					<!-- menu -->
					<tiles:insertAttribute name="menu" />
					<!-- /menu -->
					
				</div>
			</div>

			<!-- top navigation -->
			<div class="top_nav">
			<tiles:insertAttribute name="top-nav" />
			</div>
			<!-- /top navigation -->

			<!-- page content -->
			<div id="main-content-area" style="" class="right_col" style="min-height: 100vh;" role="main">
			
				<tiles:insertAttribute name="content" />
				
			</div>
			<!-- /page content -->

			<!-- footer content -->
			<footer>				
				<div class="pull-right">
						<b><spring:message code="app.name" /></b>.
						<spring:message code="app.description" />.
				</div>
				<div class="clearfix"></div>
			</footer>
			<!-- /footer content -->
		</div>
	</div>


	
	<script type="text/javascript" src="<c:url value='/resources/js/custom.js'/>"></script>			

</body>
</html>
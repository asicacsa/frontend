<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editarperfilvisitante_datos_perfil}" var="editarperfilvisitante_datos_perfil_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">x</span>
	</button>
	<h4 class="modal-title">
		<x:choose>
			<x:when select="not($editarperfilvisitante_datos_perfil_xml/perfilvisitante)">
				<spring:message code="administracion.productos.tabs.perfilesvisitante.dialog.crear_perfil.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.productos.tabs.perfilesvisitante.dialog.editar_perfil.title" />
			</x:otherwise>
		</x:choose>
	</h4>
</div>

<div class="modal-body" id="principal">

	<form id="form_perfilvisitante_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

		<input id="idperfilvisitante" name="idperfilvisitante" type="hidden" value="<x:out select = "$editarperfilvisitante_datos_perfil_xml/perfilvisitante/idperfilvisitante" />" />
		<input id="i18nFieldNombre_perfilvisitante" name="i18nFieldNombre_perfilvisitante" type="hidden" value="<x:forEach select="$editarperfilvisitante_datos_perfil_xml/perfilvisitante/i18ns/I18n" var="item"><x:out select="$item/idi18n" />~<x:out select="$item/idioma/ididioma" />~<x:out select="$item/nombre" />^</x:forEach>" />
		<div class="col-md-12 col-sm-12 col-xs-12">
			 <div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.perfilesvisitante.field.nombre" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editarperfilvisitante_datos_perfil_xml/perfilvisitante/nombre" />">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.perfilesvisitante.field.descripcion" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<textarea class="form-control" rows="3" name="descripcion" id="descripcion"><x:out select="$editarperfilvisitante_datos_perfil_xml/perfilvisitante/descripcion" /></textarea>
				</div>
			</div>		
			
		  	<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.perfilesvisitante.field.orden" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">					
					<input name="orden" id="orden" type="text" class="form-control" data-inputmask="'mask': '9{0,10}'" value="<x:out select = "$editarperfilvisitante_datos_perfil_xml/perfilvisitante/orden" />">
				</div>
			</div>					
					
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.perfilesvisitante.field.pasarasap" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input id="basesapinvitacion-switch" name="basesapinvitacion" type="checkbox" class="js-switch" <x:if select="$editarperfilvisitante_datos_perfil_xml/perfilvisitante/sapinvitacion='1'">checked=""</x:if> data-switchery="true" style="display: none;">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.perfilesvisitante.field.requiereacredit" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input id="acreditacion-switch" name="acreditacion" type="checkbox" class="js-switch" <x:if select="$editarperfilvisitante_datos_perfil_xml/perfilvisitante/requiereAcreditacion='1'">checked=""</x:if> data-switchery="true" style="display: none;">
				</div>
			</div>
			
			<x:if select="not($editarperfilvisitante_datos_perfil_xml/perfilvisitante)">
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.perfilesvisitante.field.activo" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input id="activo-switch" name="activo" type="checkbox" class="js-switch" checked="checked" data-switchery="true" style="display: none;">
				</div>
			</div>		
			</x:if>
			
			<x:if select="$editarperfilvisitante_datos_perfil_xml/perfilvisitante">
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.perfilesvisitante.field.activo" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input id="activo-switch" name="activo" type="checkbox" class="js-switch" <x:if select="$editarperfilvisitante_datos_perfil_xml/perfilvisitante/activo='1'">checked=""</x:if> data-switchery="true" style="display: none;">
				</div>
			</div>		
			</x:if>
			</div>
		  
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="modal-footer">
					<button id="save_perfil_button" type="submit" class="btn btn-primary save_dialog">
						<spring:message code="common.button.save" />
					</button>
					<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
						<spring:message code="common.button.cancel" />
					</button>
				</div>
			</div>
	</form>
</div>

<script>

$(":input").inputmask();
//*********************************************
hideSpinner("#tab_perfilesvisitante_new");
hideSpinner("#tab_perfilesvisitante_edit");
//********************************************************************************
$("#nombre").i18nField("#i18nFieldNombre_perfilvisitante",<c:out value="${sessionScope.i18nlangs}"  escapeXml="false" />);
//********************************************************************************
$(".js-switch").each(function(index) {
	var s = new Switchery(this, {
		size : 'small'
	});
});
//********************************************************************************
$("#form_perfilvisitante_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFormDataPerfilvisitante();
		}
	}
	);
	//********************************************************************************
	
	function saveFormDataPerfilvisitante() 
	{
		var data = $("#form_perfilvisitante_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/productos/perfilesvisitante/save_perfilvisitante.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
			
				
				$("#modal-dialog-form").modal('hide');
				
				dt_listperfilesvisitante.ajax.reload(null,false);
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});		
	}
</script>
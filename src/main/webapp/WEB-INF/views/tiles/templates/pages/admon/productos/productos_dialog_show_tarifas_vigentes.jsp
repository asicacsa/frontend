<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${show_tarifas_vigentes}" var="show_tarifas_vigentes_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
				<spring:message code="administracion.productos.tabs.contenidos.dialog.tarifas_vigentes.title" />				
	</h4>	
</div>


<div class="modal-body">
	<form id="form_contenido_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="tarifasvigentes[]" id="selector_tarifasvigentes" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
						<x:forEach select="$show_tarifas_vigentes_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>		
		 		
			</div>	
 </form>
	
	<div class="modal-footer">
		<button id="save_tarifasvigentes_button" type="button" class="btn btn-primary save_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>	
</div>	

<script>

	/*Para cambiar el aspecto de los selectores*/
	$(".select-select2").select2({
		language : "es",
		containerCssClass : "single-line"
	});	
	
	
	$("#save_tarifasvigentes_button").on("click", function(e) {		
		//A�adimos las nuevas tarifas a la tabla
		var tds;
		//Columnas idTarifa y Nombre
		$("#selector_tarifasvigentes option:selected").each(function() {
			var table = $('#table-listatarifas').DataTable();
			 
			var rowNode = table
			    .row.add( [$(this).val(), $(this).text()],"","","" )
			    .draw()
			    .node();			  						
		});
		
		//Cerramos la ventana
		$("#modal-dialog-form-2").modal('hide');
	});
	</script>
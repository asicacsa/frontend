<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_productos_disponibles}" var="ventareserva_productos_disponibles_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.tabs.gestion_pases_club-dialog.listado-pases-renovar.title" />
	</h4>
</div>
<div class="modal-body">
	<form id="form_renovar" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="col-md-9 col-sm-9 col-xs-12">
			<div class="form-group">
								<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.field.producto" /></label>
								<div class="col-md-8 col-sm-8 col-xs-12">
									<select class="form-control" name="idproducto_gestion_pases_club_renovar" id="idproducto_gestion_pases_club_renovar">
										<option value=""></option>
										<x:forEach select="$ventareserva_productos_disponibles_xml/ArrayList/LabelValue" var="item">
											<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
										</x:forEach>
									</select>
								</div>
			</div>
		</div>	
		<div class="col-md-3 col-sm-3 col-xs-12">
			<div class="btn-group">
					<button type="button" class="btn btn-info" id="tab_pases_club_vender_renovacion">
						<spring:message code="venta.ventareserva.tabs.gestion_pases_club.button.renovacion"/>
					</button>	
			</div>
		</div>
	</div>
	</form>


	<div class="modal-footer">
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>		

			
</div>
<script>

var str_titular="<titularGrupo><idpase/><esAbono/></titularGrupo>";

//***************************************************************
$("#tab_pases_club_vender_renovacion").on("click", function(e) { 		
   var idproducto = $("#idproducto_gestion_pases_club_renovar").val();
   //TODO GGL Ojo que meterlo a pinyon no es buena idea 
   var idcanal="2";
   
   var data = dt_listpaseclubeditar.rows( { selected: true } ).data();
	
   list_abonos= []; 
	
	
	var xml = "<list>";
	for(i=0;i<data.length;i++)
		{	
		xml+="<PaseDtoSalida>";
		var json_pase = dt_listpaseclubeditar.rows( { selected: true } ).data()[i][10];
     	xml += json2xml(json_pase);
     	xml+="</PaseDtoSalida>";
		}
	
	xml +="</list>";
	
	var parametros = "idtipoventa=5&idproducto="+idproducto+"&idcanal="+idcanal+"&xml="+xml;
    
	var url = "<c:url value='/ajax/venta/ventareserva/list_lineas_detalles_abono.do'/>?"+encodeURI(parametros);

   
	 $.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : url,
			timeout : 100000,
			data: { 
				
				}, 
			success : function(data) {
				var lineasDetalle = data.lineadetalles.LineadetalleVentaPasesDTO;

				if(lineasDetalle.length>0)
					{
					$.each(data.lineadetalles.LineadetalleVentaPasesDTO, function(i, item) {
					    var linea=[];
					    linea['LineadetalleVentaPasesDTO'] = item;
					     list_abonos.push(linea);
					});
					}
				else
					{
					list_abonos.push(data.lineadetalles);
					}	
				
				renovacion="1";
				setModalDialogSize("#modal-dialog-form-2", "md");
				$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_productos.do'/>", function() {
					$("#modal-dialog-form-2").modal('show');
					$("#modal-dialog-form").modal('hide');
					
				}); 
			},
			error : function(exception) {
				hideSpinner("#tab_pases_club_vender_renovacion");

				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",		     
					  delay: 5000,
					  buttons: { sticker: false }
				   });			
			}
	
	 })
})



</script>
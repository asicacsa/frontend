<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_cliente_pais}" var="editar_cliente_pais_xml" />

<div class="col-md-12 col-sm-12 col-xs-12">
			<input type="hidden" name="idDireccion_${direccion}" id="idDireccion_${direccion}" value="<x:out select = "$direccion_xml/iddireccion" />"/>
			<input type="hidden" name="idPais_${direccion}" id="idPais_${direccion}" value="<x:out select = "$direccion_xml/pais/idpais" />"/>
			<input type="hidden" name="idProvincia_${direccion}"  id="idProvincia_${direccion}" value="<x:out select = "$direccion_xml/provincia/idprovincia" />"/>
			<input type="hidden" name="idMunicipio_${direccion}" id="idMunicipio_${direccion}" value="<x:out select = "$direccion_xml/municipio/idmunicipio" />"/>
			<input type="hidden" name="idCp_${direccion}"   id="idCp_${direccion}" value="<x:out select = "$direccion_xml/cp/idcp" />"/>
			<input type="hidden" name="cpExtanjero_${direccion}"   id="cpExtanjero_${direccion}" value="<x:out select = "$direccion_xml/cp/cp" />"/>
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.field.personaContacto" />*</label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input name="personacontacto_${direccion}" id="personacontacto_${direccion}" type="text" class="form-control" required="required" value="<x:out select = "$direccion_xml/personacontacto" />">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.field.direccion" />*</label>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<select id="tipovia_${direccion}" name="tipovia_${direccion}" class="form-control" required="required">
							<option value=""></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.alameda" />"><spring:message code="facturacion.facturas.tabs.clientes.field.alameda" /></option>							
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.avenida" />"><spring:message code="facturacion.facturas.tabs.clientes.field.avenida" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.calle" />"><spring:message code="facturacion.facturas.tabs.clientes.field.calle" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.camino" />"><spring:message code="facturacion.facturas.tabs.clientes.field.camino" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.carretera" />"><spring:message code="facturacion.facturas.tabs.clientes.field.carretera" /></option>							
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.glorieta" />"><spring:message code="facturacion.facturas.tabs.clientes.field.glorieta" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.pasaje" />"><spring:message code="facturacion.facturas.tabs.clientes.field.pasaje" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.paseo" />"><spring:message code="facturacion.facturas.tabs.clientes.field.paseo" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.plaza" />"><spring:message code="facturacion.facturas.tabs.clientes.field.plaza" /></option>							
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.rambla" />"><spring:message code="facturacion.facturas.tabs.clientes.field.rambla" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.ronda" />"><spring:message code="facturacion.facturas.tabs.clientes.field.ronda" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.sector" />"><spring:message code="facturacion.facturas.tabs.clientes.field.sector" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.travesia" />"><spring:message code="facturacion.facturas.tabs.clientes.field.travesia" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.urbanizacion" />"><spring:message code="facturacion.facturas.tabs.clientes.field.urbanizacion" /></option>							
					</select>					
				</div>
				<div class="col-md-5 col-sm-5 col-xs-5">
				    <c:set var = "campo" scope = "request" value = "editar_cliente_xml/direccion${direccion}/direccion"/>	
					<input name="direccion_${direccion}" id="direccion_${direccion}" type="text" class="form-control" required="required" value="<x:out select = "$direccion_xml/direccion" />">
				</div>
			</div>	
			
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.field.pais" />*</label>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<select name="selector_pais_${direccion}" name="" id="selector_pais_${direccion}" class="form-control;position:absolute;z-index: 10" required="required">
						<option value=""></option>
						<x:forEach select="$editar_cliente_pais_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>					
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.field.provincia" />*</label>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<input id="filter_provincia_${direccion}" type="text" class="form-control" value="" disabled/>
					<select name="provincia_${direccion}" id="selector_provincia_${direccion}" class="form-control" required="required">
					   <option value="" ></option>
					</select>					
				</div>
			</div>	
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.field.poblacion" />*</label>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<input id="filter_poblacion_${direccion}" type="text" class="form-control" value="" disabled/>
					<select name="poblacion_${direccion}" id="selector_poblacion_${direccion}" class="form-control" required="required">
					    <option value="" ></option>
					</select>					
				</div>
			</div>
			
			<div class="form-group capa_cp_${direccion}" hidden="hidden">
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.field.cp" />*</label>
				<div class="col-md-5 col-sm-5 col-xs-5">
					<input id="filter_cp_${direccion}" type="text" class="form-control" value=""/>
					<select name="cp" id="selector_cp_${direccion}" class="form-control" required="required">
					    <option value="" ></option>
					</select>					
				</div>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<a type="button" class="btn btn-info" id="button_cp_new_${direccion}" >
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.clientes.list.button.nuevo" />"> <span class="fa fa-plus"></span>
						</span>
					</a>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.field.telefono" />*</label>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<input id="telefonofijo_${direccion}" name="telefono_${direccion}" type="text" class="form-control" value="<x:out select = "$direccion_xml/telefonofijo" />"/>
				</div>
				<div class="col-md-5 col-sm-5 col-xs-5">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.field.fax" />*</label>
					<div class="col-md-7 col-sm-7 col-xs-7">
						<input id="fax_${direccion}" name="fax_${direccion}" type="text" class="form-control" value="<x:out select = "$direccion_xml/fax" />" />
					</div>
				</div>
			</div>	
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.field.telefonomovil" />*</label>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<input id="telefonomovil_${direccion}" name="telefonomovil_${direccion}" type="text" class="form-control" value="<x:out select = "$direccion_xml/telefonomovil" />"/>
				</div>
				<div class="col-md-5 col-sm-5 col-xs-5">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.field.correoelectronico" />*</label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input id="correoelectronico_${direccion}" name="correoelectronico_${direccion}" type="text" class="form-control" value="<x:out select = "$direccion_xml/correoelectronico" />" />
					</div>
				</div>
			</div>
</div>
<script>

$('#telefonofijo_${direccion}').inputmask({alias: 'numeric', 
    allowMinus: false  
    });
    
$('#fax_${direccion}').inputmask({alias: 'numeric', 
    allowMinus: false,  
   });
    
$('#telefonomovil_${direccion}').inputmask({alias: 'numeric', 
    allowMinus: false,  
    });
   
    

$("#selector_provincia_${direccion}").hide();
$("#selector_poblacion_${direccion}").hide();
$("#selector_cp_${direccion}").hide();

var tipoVia = "<x:out select = "$direccion_xml/tipovia"/>";
$('#tipovia_${direccion} option[value="'+tipoVia+'"]').attr("selected", "selected");

var idPais = "<x:out select = "$direccion_xml/pais/idpais"/>";
$('#selector_pais_${direccion} option[value="'+idPais+'"]').attr("selected", "selected");

if(idPais>0)
{
$( "#filter_provincia_${direccion}" ).prop( "disabled", false );
var provincia = "<x:out select = "$direccion_xml/provincia/nombreprovincia"/>";
if(provincia!='')
	{
	$("#filter_provincia_${direccion}").val(provincia);
	$( "#filter_poblacion_${direccion}" ).prop( "disabled", false );
	}
var poblacion = "<x:out select = "$direccion_xml/municipio/nombre"/>";
if(poblacion!='')
	{
	$("#filter_poblacion_${direccion}").val(poblacion);
	$( "#filter_cp_${direccion}" ).prop( "disabled", false );
	}
if(idPais=="472")
	{
	var cp = "<x:out select = "$direccion_xml/cp/cp"/>";
	$("#filter_cp_${direccion}").val(cp);
	$(".capa_cp_${direccion}").show();
	}
else
	{
		var cp = "<x:out select = "$direccion_xml/cp/cp"/>";
		$("#filter_cp_${direccion}").val(cp);
		$(".capa_cp_${direccion}").show();
		$("#button_cp_new_${direccion}").hide();
	}
	
}

$("#selector_pais_${direccion}").change(function() {	
	var idPais= $("#selector_pais_${direccion}").val();
	
	if(idPais=="472")
		{
		$("#button_cp_new_${direccion}").show();
		$(".capa_cp_${direccion}").show();
		}
	else
		{
		$(".capa_cp_${direccion}").show();
		$("#filter_cp_${direccion}").prop( "disabled", false );
		$("#button_cp_new_${direccion}").hide();
		$( "#filter_poblacion_${direccion}" ).prop( "disabled", false );
		}
	$( "#filter_provincia_${direccion}" ).prop( "disabled", false );
	
});




$( "#filter_provincia_${direccion}" ).keyup(function() {
	
	var idPais= $("#selector_pais_${direccion}").val();
	
	if(idPais=="472")
	{
	var nombre_provincia = $("#filter_provincia_${direccion}").val();
	var idPais = $("#selector_pais_${direccion}").val();
	var provincia = $('#selector_provincia_${direccion}');
	filtrarProvincia(nombre_provincia,idPais,provincia);
	}
});

$("#selector_provincia_${direccion}").change(function() {
	$("#idProvincia_${direccion}").val($("#selector_provincia_${direccion}").val())
	$("#filter_provincia_${direccion}").val($("#selector_provincia_${direccion} option:selected").text());	
	$("#filter_provincia_${direccion}").show();
	$("#selector_provincia_${direccion}").hide();
	$("#idProvincia_${direccion}").val($("#selector_provincia_${direccion}").val());
	$("#filter_poblacion_${direccion}" ).prop( "disabled", false );
});



$( "#filter_poblacion_${direccion}" ).keyup(function() {
	
	var idPais= $("#selector_pais_${direccion}").val();
	
	if(idPais=="472")
	{
	var nombre_poblacion = $("#filter_poblacion_${direccion}").val();
	var idPais= $("#selector_pais_${direccion}").val();
	var idProvincia= $("#selector_provincia_${direccion}").val();
	var poblacion = $('#selector_poblacion_${direccion}');
	filtrarPoblacion(nombre_poblacion,idPais,idProvincia,poblacion);
	}
});

$("#selector_poblacion_${direccion}").change(function() {
	$("#idMunicipio_${direccion}").val($("#selector_poblacion_${direccion}").val())
	$("#filter_poblacion_${direccion}").val($("#selector_poblacion_${direccion} option:selected").text());	
	$("#filter_poblacion_${direccion}").show();
	$("#selector_poblacion_${direccion}").hide();
	$("idDireccion_${direccion}").val("");
	$("#filter_cp_${direccion}").prop( "disabled", false );
	$("#idMunicipio_${direccion}").val($("#selector_poblacion_${direccion}").val());
});



$( "#filter_cp_${direccion}" ).keyup(function() {
	var numeroCp = $("#filter_cp_${direccion}");
	
	if(numeroCp.val().length==5)
		{
		var idPais= $("#selector_pais_${direccion}");
		var idProvincia= $("#selector_provincia_${direccion}").val();
		var idPoblacion= $("#selector_poblacion_${direccion}").val();
		var cp = $('#selector_cp_${direccion}');
		var idCp = $('#idCp_${direccion}');
		
		returnfilterprovincia=$("#filter_provincia_${direccion}");
		returnfilterpoblacion=$("#filter_poblacion_${direccion}");
		
		var strIdPais = "selector_pais_${direccion}";
		
		returnidprovincia = $("#idProvincia_${direccion}");
		returnidpoblacion = $("#idMunicipio_${direccion}");
		retunidcp         = $("#idCp_${direccion}");
		
		
		filtrarCP(numeroCp,idPais,idProvincia,idPoblacion,cp,idCp)
		$("#idDireccion_${direccion}").val("");
		}
});


$( "#button_cp_new_${direccion}" ).on("click", function(e) {	
	$("#modal_nuevo_cp").modal('show');
	setModalDialogSize("#modal_nuevo_cp", "sm");
	$("#selector_new_provincia").hide();
	$("#selector_new_poblacion").hide();
	$("#selector_new_cp").hide();
	$("#filter_new_provincia").val("");
	$("#filter_new_poblacion").val("");
	$("#new_cp").val("");
	
	returnprovincia = $("#selector_provincia_${direccion}");
	
	returnidprovincia = $("#idProvincia_${direccion}");
	returnidpoblacion = $("#idMunicipio_${direccion}");
	retunidcp         = $("#idCp_${direccion}");


	
	returnpoblacion = $("#selector_poblacion_${direccion}");
	returncp = $("#selector_cp_${direccion}");
	returnfilterprovincia=$("#filter_provincia_${direccion}");
	returnfilterpoblacion=$("#filter_poblacion_${direccion}");
	retunfiltercp=$("#filter_cp_${direccion}");
	
});

$( "#filter_cp_${direccion}" ).prop( "disabled", false );
$("#filter_cp_${direccion}").show();
$(".capa_cp_${direccion}").show();
</script>
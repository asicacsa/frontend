<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_entradas}" var="ventareserva_entradas_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.entradas_impresas.title" />
	</h4>
</div>
<div class="modal-body">

	<div class="btn-group pull-right btn-datatable">
	<a type="button" class="btn btn-info" id="button_venta_resumen_entradas_editar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.entradas_editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="button_venta_resumen_entradas_reimprimir">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.entradas_reimprimir" />"> <span class=" fa fa-paint-brush"></span>
			</span>
		</a>		
		<a type="button" class="btn btn-info" id="button_venta_resumen_entradas_imprimir">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.entradas_imprimir" />"> <span class="fa fa-print"></span>
			</span>
		</a>			
	</div>
	<table id="datatable_list_venta_entradas" class="table table-striped table-bordered dt-responsive ventareserva" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.producto" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.fecha" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.contenido" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.perfil" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.descuentos" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.etd" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.imp" /></th>		
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.impr" /></th>
			</tr>
		</thead>
		<tbody>			
		</tbody>				
	</table>


		
			<div class="modal-footer">			
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		
</div>

<tiles:insertAttribute name="modal_editar_entradas" />

<script>
hideSpinner("#button_reserva_resumen_entradas");
//************************************************
var json_entradas = ${ventareserva_entradas_json};
//************************************************
var dt_entradas=$('#datatable_list_venta_entradas').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	  initComplete: function( settings, json ) {
	    	 window.setTimeout(CargarLineasDetalle, 100);  
	    	},
	scrollCollapse: true,
	ordering:  false,
	paging: false,	
    select: { style: 'single' },
    columns: [
              {},              
              {},
              {},
              {},
              {},
              {},
              {},
              {},
              {}           
              
        ],
        "columnDefs": [
                       { "visible": false, "targets": [0]}
                     ],
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('datatable_list_venta_entradas');
   	}
    
} );
//******************************************************************
function CargarLineasDetalle()
	{
		var i = 0;
		
		var lineasdetalle = json_entradas.Reserva.lineadetalles;
				
		if (lineasdetalle !=""){
		if (lineasdetalle.Lineadetalle!="") {
	        var item=lineasdetalle.Lineadetalle;
	        if (item.length>0)
	            $.each(item, function(key, lineadetalle){
	            	CargarLineaDetalle(lineadetalle,dt_entradas)
	            });
	        else
	        	CargarLineaDetalle(item,dt_entradas)
	}
	}
	}
		

	//*****************************************************************************************
	function CargarLineaDetalle(lineadetalle, tabla)
	{	
		
		var fechas= "", contenidos= "", disponibles= "",retorno= false;
		var impreso = lineadetalle.entradasimpresas+"/"+lineadetalle.entradasnoanuladas;
		var sesiones = lineadetalle.lineadetallezonasesions.Lineadetallezonasesion;
		var descuento_nombre=""+lineadetalle.descuentopromocional.nombre;
		var numbonoagencia= lineadetalle.numerobonoagencia;
			
		
		if(descuento_nombre=="undefined")
			descuento_nombre =" ";
		
		var contenidos;
		
		 if (sesiones.length>0)		 
		 {
				for (var i=0; i<sesiones.length; i++) {
					if (retorno) {
						fechas+="</br>";
						contenidos+= "</br>";						
					}				
					var f= sesiones[i].zonasesion.sesion.fecha.split("-")[0];
					fechas+= f;					
					var hi=sesiones[i].zonasesion.sesion.horainicio;
					var  no=sesiones[i].zonasesion.sesion.contenido.nombre;
					contenidos+= hi+" "+no;					
					retorno= true;
				}	 
				
		 }
		 else
		 {
			fechas= lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.fecha.split("-")[0];
			contenidos= lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.horainicio+" "+lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.contenido.nombre;
				
		 }
		 
		 var o = document.createElement('input');
		 o.type = "text";
		 o.name = "lalala";
		 o.value = "";
		 
		 
		 
				tabla.row.add([
					lineadetalle,
				    lineadetalle.producto.nombre, 
					fechas,         
				    contenidos, 
				    lineadetalle.perfilvisitante.nombre,
				    descuento_nombre,
				    lineadetalle.cantidad, 
				    lineadetalle.numimpresiones,
				    lineadetalle.imprimir				     
				]).draw();
}
	
//*************************************************
$("#button_venta_resumen_entradas_editar").on("click", function(e) { 
	
	if (dt_entradas.rows( { selected: true }).data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.entradas.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	if (dt_entradas.rows( { selected: true }).data().length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.entradas.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	$("#numero_imprimir").val(dt_entradas.rows( { selected: true }).data()[0][8]);
	$("#modal_editar_entradas").modal('show');
	setModalDialogSize("#modal_editar_entradas", "xs");		 
}); 

//*************************************************
$("#button_venta_resumen_entradas_imprimir").on("click", function(e) { 
	var xml ="";
	xml="<Venta>";
	xml+="<idventa>${idVenta}</idventa>";
	xml+="<lineadetalles>";
	
	var xml_detalles="";
	for (var i=0; i<dt_entradas.rows().data().length; i++) { 
		xml_detalles+="<Lineadetalle>";
		xml_detalles+=json2xml(dt_entradas.rows().data()[i][0],"");
		xml_detalles+="</Lineadetalle>";
	}
	xml+=xml_detalles;
	xml+="</lineadetalles>";
	xml+="</Venta>";
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/lanzar_imprimir_entradas.do'/>",
		timeout : 100000,
		data: {
			xml: xml
		},
		success : function(data) {
			
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text:  '<spring:message code="venta.ventareserva.tabs.entradas_imprimir.error" />',
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});	
	
}); 

//*************************************************

$("#button_venta_resumen_entradas_reimprimir").on("click", function(e) {
	//Si numero de impresiones es menor que el nº de entradas da mensaje
	var num_entradas_impresas;
	var num_entradas_a_imprimir;
	for (var i=0; i<dt_entradas.rows().data().length; i++) { 
	
		num_entradas_a_imprimir=dt_entradas.rows().data()[0][8]
		num_entradas_impresas=dt_entradas.rows().data()[0][7];
		
		if (num_entradas_a_imprimir>num_entradas_impresas){
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="venta.ventareserva.list.alert.entradas.reimpresion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}
	}
}); 

</script>
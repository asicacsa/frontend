<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${programacion_selector_unidades}" var="programacion_selector_unidades_xml" />

<div id="principal">
	<div class="row x_panel">
	<div class="x_panel filter_list thin_padding">
		<div class="x_title">
			<h2><spring:message code="common.text.filter_list" /></h2>
			<ul class="nav navbar-right panel_toolbox">
				<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="x_content" style="display: block;">
	
			<form id="form_filter_list_sesiones" class="form-horizontal form-label-left">
		
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.programacion.field.unidades" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<select class="form-control" name="idunidad" id="idunidad">
								<option value=""></option>
	 							<x:forEach select="$programacion_selector_unidades_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
							</select>
						</div>
					</div>
	
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.programacion.field.recintos" /></label>
						<div id="recinto_div" class="col-md-9 col-sm-9 col-xs-9">
							<select class="form-control" name="idrecinto" id="idrecinto" required="required">
								<option value=""></option>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.programacion.field.contenido" /></label>
						<div id="contenido_div" class="col-md-9 col-sm-9 col-xs-9">
							<select class="form-control" name="idcontenido" id="idcontenido">
								<option value=""></option>
							</select>
						</div>
					</div>
					
				</div>
	
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="form-group date-picker">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.programacion.field.fecha" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<a type="button" class="btn btn-default btn-clear-date" id="button_fecha_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
							</a>			
	                        <div class="input-prepend input-group">
	                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	                          <input type="text" name="fecha" id="fecha" class="form-control" value="" readonly/>
	                          <input type="hidden" name="fechaIni" value=""/>
	                          <input type="hidden" name="fechaFin" value=""/>
	                        </div>
						</div>
					</div>
	
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.programacion.field.horaDesde" /></label>
						<div class="col-md-3 col-sm-3 col-xs-3">
	                          <input type="text" name="horaDesde" id="horaDesde" class="form-control" data-inputmask="'mask': '99:99'" value=""/>
						</div>
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.programacion.field.horaHasta" /></label>
						<div class="col-md-3 col-sm-3 col-xs-3">
	                          <input type="text" name="horaHasta" id="horaHasta" class="form-control" data-inputmask="'mask': '99:99'" value=""/>
						</div>
					</div>
					
				</div>
	
				<div class="clearfix"></div>
				<div class="ln_solid"></div>
				<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
					<button id="button_filter_list_programacion" type="button" class="btn btn-success pull-right">
						<spring:message code="common.button.filter" />
					</button>
				</div>
				
				<input type="hidden" name="start" value=""/>
				<input type="hidden" name="length" value=""/>
	
			</form>
	
	
		</div>
	</div>
	
	<div class="col-md-12 col-sm-12 col-xs-12">
	
		<div class="btn-group pull-right btn-datatable">
			<a type="button" class="btn btn-info button_programacion_new" id="button_programacion_new">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.nuevo" />"> <span class="fa fa-plus"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="button_programacion_edit">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.editar" />"> <span class="fa fa-pencil"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="button_programacion_remove">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.eliminar" />"> <span class="fa fa-trash"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="button_programacion_lock">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.bloquear" />"> <span class="fa fa-lock"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="button_programacion_unlock">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.desbloquear" />"> <span class="fa fa-unlock"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="button_programacion_cupos">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.cupos" />"> <span class="fa fa-list"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info button_programacion_franjas" id="button_programacion_franjas">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.franjas" />"> <span class="fa fa-calendar-plus-o"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="button_programacion_bloqueos">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.bloqueos" />"> <span class="fa fa-user-times"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="button_programacion_calendario">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.calendario" />"> <span class="fa fa-calendar"></span>
				</span>
			</a>
 			<a type="button" class="btn btn-info" id="button_programacion_seleccionar_todo">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.seleccionar_todo" />"> <span class="fa fa-check-square"></span>
				</span>
			</a>
		</div>
	
		<table id="datatable-list-programacion" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th><spring:message code="administracion.programacion.programacion.list.header.fecha" /></th>
					<th><spring:message code="administracion.programacion.programacion.list.header.horaInicio" /></th>
					<th><spring:message code="administracion.programacion.programacion.list.header.horaFin" /></th>
					<th><spring:message code="administracion.programacion.programacion.list.header.contenido" /></th>
					<th><spring:message code="administracion.programacion.programacion.list.header.recinto" /></th>
					<th><spring:message code="administracion.programacion.programacion.list.header.bloqueado" /></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>

<div id="secundaria" style="display:none;">
	<div class="row x_panel">
		<div class="x_title">
			<h5><spring:message code="administracion.programacion.programacion.calendar.field.recinto" />&nbsp;<span id="nombre_recinto"></span></h5>
			<div class="clearfix"></div>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12">
		
			<div class="btn-group pull-right btn-datatable">
				<a type="button" class="btn btn-info button_programacion_new" id="button_calendario_new">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.calendar.button.nuevo" />"> <span class="fa fa-plus"></span>
					</span>
				</a>
				<a type="button" class="btn btn-info button_programacion_franjas" id="button_calendario_franjas">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.calendar.button.franjas" />"> <span class="fa fa-calendar-plus-o"></span>
					</span>
				</a>
				<a type="button" class="btn btn-info" id="button_calendario_list">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.calendar.button.list" />"> <span class="fa fa-list-alt"></span>
					</span>
				</a>
			</div>
		
			<div id="calendario">
			</div>
			
		</div>
	
	</div>
</div>

<script>
$(":input").inputmask();

$today= moment().format("DD/MM/YYYY");
$('input[name="fechaIni"]').val($today);
$('input[name="fechaFin"]').val($today);
$('input[name="fecha"]').val($today + ' - ' + $today);

$('input[name="fecha"]').daterangepicker({
		autoUpdateInput: false,
		linkedCalendars: false,
		autoApply: true,
      	locale: $daterangepicker_sp
		}, function(start,end) {
      	  	 $('input[name="fecha"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
      	  	 $('input[name="fechaIni"]').val(start.format('DD/MM/YYYY'));
      	  	 $('input[name="fechaFin"]').val(end.format('DD/MM/YYYY'));
    	 });
    	 
$("#button_fecha_clear").on("click", function(e) {
    $('input[name="fecha"]').val('');
    $('input[name="fechaIni"]').val('');
    $('input[name="fechaFin"]').val('');
});

var refrescar_calendario= true;

$("#idunidad").on("change", function(e) {
	showFieldSpinner("#recinto_div");
	$select=$("#idrecinto");
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/admon/programacion/programacion/list_recintos.do'/>",
		timeout : 100000,
		data: {
				idunidadnegocio: $("#idunidad").val()
			  }, 
		success : function(data) {
			hideSpinner("#recinto_div");
			$select.html('');
			$select.prepend("<option value='' selected='selected'></option>");
			if (data.ArrayList!="") {
				var item=data.ArrayList.LabelValue;
				if (item.length>0)
				    $.each(item, function(key, val){
				      $select.append('<option value="' + val.value + '">' + val.label + '</option>');
				    });
				else
			      	$select.append('<option value="' + item.value + '">' + item.label + '</option>');
			}
		},
		error : function(exception) {
			hideSpinner("#recinto_div");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});	

}); 	

$("#idrecinto").on("change", function(e) {
	showFieldSpinner("#contenido_div");
	$select=$("#idcontenido");
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/admon/programacion/list_contenido.do'/>",
		timeout : 100000,
		data: {
				idrecinto: $("#idrecinto").val()
			  }, 
		success : function(data) {
			hideSpinner("#contenido_div");
			$select.html('');
			$select.prepend("<option value='' selected='selected'></option>");
		    $.each(data.ArrayList.LabelValue, function(key, val){
		      $select.append('<option value="' + val.value + '">' + val.label + '</option>');
		    });
		    refrescar_calendario= true;
		},
		error : function(exception) {
			hideSpinner("#contenido_div");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});	

}); 	

var dt_listprogramacion=$('#datatable-list-programacion').DataTable( {	
	serverSide: true,
	searching: false,
	ordering: false,
	pagingType: "simple",
	info: false,
	deferLoading: 0,
	ajax: {
        url: "<c:url value='/ajax/admon/programacion/list_sesiones.do'/>",
        rowId: 'idsesion',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Sesion)); return(""); },
        data: function (params) {
        	$('#form_filter_list_sesiones input[name="start"]').val(params.start);
        	$('#form_filter_list_sesiones input[name="length"]').val(params.length);
        	return ($("#form_filter_list_sesiones").serializeObject());
       	},
        error: function (xhr, error, code)
        {
        	$("#datatable-list-programacion_processing").hide();
           	new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : xhr.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}					
			});
        }
       	
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listprogramacion.data().count()>0) dt_listprogramacion.columns.adjust().draw(); });
	},
    columns: [
        { data: "fecha", className: "cell_centered",
            render: function ( data, type, row, meta ) {
              var datetime = data.split("-");
          	  if (data) return datetime[0]; else return '';	
            }
        },
        { data: "horainicio", className: "cell_centered", type: "spanish-string", defaultContent: "" },
        { data: "horafin", className: "cell_centered", type: "spanish-string", defaultContent: "" },
        { data: "contenido.nombre", className: "cell_centered", type: "spanish-string", defaultContent: "" },
        { data: "contenido.tipoproducto.recinto.nombre", className: "cell_centered", type: "spanish-string", defaultContent: "" },
        { data: "bloqueado", className: "text_icon cell_centered",
          render: function ( data, type, row, meta ) {
        	  if (data) return '<i class="fa fa-check-circle" aria-hidden="true"></i>'; else return '';	
          }
        }, 
        
    ],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-programacion') },
    select: { style: 'os'},
	language: dataTableLanguage,
	processing: true
} );


insertSmallSpinner("#datatable-list-programacion_processing");

//********************************************************************************
$(".button_programacion_new").on("click", function(e) {

	if ($("#idrecinto").val()=="") {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.programacion.programacion.list.alert.no_recinto" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	showButtonSpinner("#button_programacion_new");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/programacion/show_sesion.do'/>", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
});

$("#button_programacion_edit").on("click", function(e) {
	
	var data = sanitizeArray(dt_listprogramacion.rows( { selected: true } ).data(),"idsesion");
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.programacion.programacion.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	

	if (data.length>1) {
		showButtonSpinner("#button_programacion_edit");
		$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/programacion/show_sesion_multiple.do'/>?data="+data.toString(), function() {
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "xs");
		});
		return;
	}

	showButtonSpinner("#button_programacion_edit");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/programacion/show_sesion.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
});

$("#button_programacion_seleccionar_todo").on("click", function(e) {
	dt_listprogramacion.rows().select();	
});

$("#button_programacion_calendario").on("click", function(e) {
	if ($("#idrecinto").val()!="") {
		$("#principal").hide();
		$("#secundaria").show();
		$("#nombre_recinto").html($("#idrecinto :selected").text());
		if (refrescar_calendario) { 
			$('#calendario').fullCalendar('removeEvents');
			$('#calendario').fullCalendar('refetchEvents');
			refrescar_calendario= false;
		}
	}
	else {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
		      text: '<spring:message code="administracion.programacion.programacion.list.alert.recinto" />',
			  type: "error",
	  	  	  delay : 5000,
			  buttons: { sticker: false }				  
		   });		
	}
});

//********************************************************************************
$("#button_filter_list_programacion").on("click", function(e) { dt_listprogramacion.ajax.reload(); })

//********************************************************************************
// PÁGINA DE CALENDARIO
//********************************************************************************
$('#calendario').fullCalendar({
	locale: 'es',
	titleRangeSeparator: "-",
	header: {
		left: 'prev,next today',
		center: 'title',
		right: 'month,agendaWeek,agendaDay'
	},	
    loading: function (isloading,view) {
        if (isloading) 
        	showSpinner('#calendario');
        else
       		hideSpinner('#calendario');
    },
    eventLimit: true,
    views: {
        month: {
            eventLimit: 4
        }
    },    
    events: function (start, end, timezone, callback) {
    	var date = $("#calendario").fullCalendar('getDate');
        $.ajax({
            url: "<c:url value='/ajax/admon/programacion/list_sesiones_recinto.do'/>",
            type: "POST",
            datatype: 'json',
            data:{
            	idrecinto: $("#idrecinto").val(),
            	mes: date.month()+1,
            	anyo: date.year()
            },
            success: function (data) {
                var events = [];
                if (data!=null && typeof data.vcalendar!="undefined") {
                	var anyo="year"+date.year();
                	var mes="month"+(date.month()+1);
                	for (item in data["vcalendar"][anyo][mes]) {
                		var dia= data["vcalendar"][anyo][mes][item];
                		if (dia.event.uid!=null) {  // hay un único evento para ese día
                			var val= dia.event;
                			events.push({
	                        	id: val.uid.value,
	                            title: val.summary.value,
	                            start: val.start.year+"-"+("0" + val.start.month).slice(-2)+"-"+("0" + val.start.day).slice(-2)+"T"+("0" + val.start.hour).slice(-2)+":"+val.start.minute+":00",
	                            end: val.end.year+"-"+("0" + val.end.month).slice(-2)+"-"+("0" + val.end.day).slice(-2)+"T"+("0" + val.end.hour).slice(-2)+":"+val.end.minute+":00"
	                        })
                		}
                		else {
	                		for (i in dia.event) {
	                			var val= dia.event[i];
	                			events.push({
		                        	id: val.uid.value,
		                            title: val.summary.value,
		                            start: val.start.year+"-"+("0" + val.start.month).slice(-2)+"-"+("0" + val.start.day).slice(-2)+"T"+("0" + val.start.hour).slice(-2)+":"+val.start.minute+":00",
		                            end: val.end.year+"-"+("0" + val.end.month).slice(-2)+"-"+("0" + val.end.day).slice(-2)+"T"+("0" + val.end.hour).slice(-2)+":"+val.end.minute+":00"
		                        })
	               			}
                		}
                  	}
                }
                callback(events);
            },error: function (err) {
                alert('Error in fetching data');
            }
        });
    },
    eventClick: function (calEvent, jsEvent, view) {
        /*alert(calEvent.title);*/
    }
});

$("#button_calendario_list").on("click", function(e) {
	$("#secundaria").hide();
	$("#principal").show();
});


//********************************************************************************
$("#button_programacion_remove").on("click", function(e) { 
		
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="administracion.programacion.programacion.list.confirm.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
		  buttons: { closer: false, sticker: false	},
		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
	
		    dt_listprogramacion.processing(true);
			
			var data = sanitizeArray(dt_listprogramacion.rows( { selected: true } ).data(),"idsesion");
		   
			$.ajax({
				contenttype: "application/json; charset=utf-8",
				type : "post",
				url : "<c:url value='/ajax/admon/programacion/remove_sesions.do'/>",
				timeout : 100000,
				data: { data: data.toString() }, 
				success : function(data) {
					dt_listprogramacion.processing(false);	
					dt_listprogramacion.rows( { selected: true } ).remove().draw();
				},
				error : function(exception) {
					dt_listprogramacion.processing(false);
					
					new PNotify({
					      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					      text: exception.responseText,
						  type: "error",		     
						  delay: 5000,
						  buttons: { sticker: false }
					   });			
				}
			});

	   }).on('pnotify.cancel', function() {
	   });		 

}); 

//********************************************************************************
$("#button_programacion_unlock").on("click", function(e) { 
	
	dt_listprogramacion.processing(true);
	
	var data = sanitizeArray(dt_listprogramacion.rows( { selected: true } ).data(),"idsesion");
				
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/admon/programacion/unlock_sesions.do'/>",
		timeout : 100000,
		data: {
	    	   data: data.toString()
			  }, 
		success : function(data) {
			dt_listprogramacion.processing(false);	
			dt_listprogramacion.ajax.reload(null,false);
		},
		error : function(exception) {
			dt_listprogramacion.processing(false);
			
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});	

}); 



//********************************************************************************
$("#button_programacion_lock").on("click", function(e) { 
	
	dt_listprogramacion.processing(true);
	
	var data = sanitizeArray(dt_listprogramacion.rows( { selected: true } ).data(),"idsesion");
				
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/admon/programacion/lock_sesions.do'/>",
		timeout : 100000,
		data: {
	    	   data: data.toString()
			  }, 
		success : function(data) {
			dt_listprogramacion.processing(false);	
			dt_listprogramacion.ajax.reload();
		},
		error : function(exception) {
			dt_listprogramacion.processing(false);
			
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",	
				  delay: 5000,
				  buttons: { closer:true, sticker: false }
			   });			
		}
	});

}); 

//********************************************************************************
$("#button_programacion_cupos").on("click", function(e) { 
	
	var data = sanitizeArray(dt_listprogramacion.rows( { selected: true } ).data(),"idsesion");
	
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.programacion.programacion.list.alert.cupos.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.programacion.programacion.list.alert.cupos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	

	showButtonSpinner("#button_programacion_cupos");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/programacion/show_establecer_cupos.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "sm");
	});

}); 

//********************************************************************************
$(".button_programacion_franjas").on("click", function(e) { 
	showButtonSpinner("#button_programacion_franjas");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/programacion/show_franjas.do'/>", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "sm");
	});

});

//********************************************************************************
$("#button_programacion_bloqueos").on("click", function(e) { 
	
	var data = sanitizeArray(dt_listprogramacion.rows( { selected: true } ).data(),"idsesion");
		
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.programacion.programacion.list.alert.localidades.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	

	if (data.length>1) {
		showButtonSpinner("#button_programacion_bloqueos");
		$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/programacion/show_bloqueos_multiple.do'/>?data="+data.toString(), function() {
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "md");
		});
		return;
	}
	
	showButtonSpinner("#button_programacion_bloqueos");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/programacion/show_bloqueos.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});

});

</script>

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-2"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-3"/>
<tiles:insertAttribute name="modal_dialog" />

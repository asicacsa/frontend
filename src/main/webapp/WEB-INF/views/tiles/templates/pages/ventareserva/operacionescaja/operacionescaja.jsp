<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${operacionescaja_selector_usuarios}" var="operacionescaja_selector_usuarios_xml" />
<x:parse xml="${operacionescaja_selector_canales}" var="operacionescaja_selector_canales_xml" />

<div id="principal">
	<div class="row x_panel">
	<div class="x_panel filter_list thin_padding">
		<div class="x_title">
			<h2><spring:message code="common.text.filter_list" /></h2>
			<ul class="nav navbar-right panel_toolbox">
				<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="x_content" style="display: block;">
	
			<form id="form_filter_list_cajas" class="form-horizontal form-label-left">
	
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="form-group date-picker">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="ventareserva.operacionescaja.operacionescaja.field.apertura" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							  <a type="button" class="btn btn-default btn-clear-date" id="button_apertura_clear">
									<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="ventareserva.operacionescaja.operacionescaja.list.button.clear" />"> <span class="fa fa-trash"></span>
									</span>
							  </a>			
	                        <div class="input-prepend input-group">
	                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	                          <input type="text" name="apertura" id="apertura" class="form-control" value="" readonly/>
	                          <input type="hidden" name="fechaAperturaIni" value=""/>
	                          <input type="hidden" name="fechaAperturaFin" value=""/>
	                        </div>
						</div>
					</div>
	
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="ventareserva.operacionescaja.operacionescaja.field.num_caja_ini" /></label>
						<div class="col-md-3 col-sm-3 col-xs-3">
	                          <input type="text" name="numCajaIni" id="numCajaIni" class="form-control" value=""/>
						</div>
						<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="ventareserva.operacionescaja.operacionescaja.field.num_caja_fin" /></label>
						<div class="col-md-3 col-sm-3 col-xs-3">
	                          <input type="text" name="numCajaFin" id="numCajaFin" class="form-control" value=""/>
						</div>
					</div>
					
				</div>
	
	
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="ventareserva.operacionescaja.operacionescaja.field.canal" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<select class="form-control" name="idcanal" id="idcanal">
								<option value=""></option>
	 							<x:forEach select="$operacionescaja_selector_canales_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
							</select>
						</div>
					</div>
	
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="ventareserva.operacionescaja.operacionescaja.field.usuarios_canal" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9" id="div_idusuario">
							<select class="form-control" name="idusuario" id="idusuario">
								<option value=""></option>
	 							<x:forEach select="$operacionescaja_selector_usuarios_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
							</select>
						</div>
					</div>
					
				</div>
	
				<div class="clearfix"></div>
				<div class="ln_solid"></div>
				<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
					<button id="button_filter_list_cajas" type="button" class="btn btn-success pull-right">
						<spring:message code="common.button.filter" />
					</button>
				</div>
				
				<input type="hidden" name="start" value=""/>
				<input type="hidden" name="length" value=""/>
	
			</form>
	
	
		</div>
	</div>
	
	<div class="col-md-12 col-sm-12 col-xs-12">
	
		<div class="btn-group pull-right btn-datatable">
			<a type="button" class="btn btn-info" id="operacionescaja_edit">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="ventareserva.operacionescaja.operacionescaja.list.button.editar" />"> <span class="fa fa-pencil"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="operacionescaja_lock">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="ventareserva.operacionescaja.operacionescaja.list.button.bloquear" />"> <span class="fa fa-lock"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="operacionescaja_vbo">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="ventareserva.operacionescaja.operacionescaja.list.button.vbo" />"> <span class="fa fa-check"></span>
				</span>
			</a>
		</div>
	
		<table id="datatable-list-cajas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th><spring:message code="ventareserva.operacionescaja.operacionescaja.list.header.numero" /></th>
					<th><spring:message code="ventareserva.operacionescaja.operacionescaja.list.header.usuario" /></th>
					<th><spring:message code="ventareserva.operacionescaja.operacionescaja.list.header.taquillas" /></th>
					<th><spring:message code="ventareserva.operacionescaja.operacionescaja.list.header.apertura" /></th>
					<th><spring:message code="ventareserva.operacionescaja.operacionescaja.list.header.cierre" /></th>
					<th><spring:message code="ventareserva.operacionescaja.operacionescaja.list.header.visto_bueno" /></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	</div>
</div>

<div id="secundaria" style="display:none;">
	<div class="row x_panel">
	<div class="x_panel filter_list thin_padding">
		<div class="x_title">
			<h2><spring:message code="ventareserva.operacionescaja.editarcaja.field.numCaja" />&nbsp;<span id="num_caja"></span></h2>
			<div class="clearfix"></div>
		</div>
		<div class="x_content" style="display: block;">
	
			<form id="form_filter_list_operaciones" class="form-horizontal form-label-left">
	
				<div class="col-md-5 col-sm-5 col-xs-12">
					<div class="form-group">
						<strong><spring:message code="ventareserva.operacionescaja.editarcaja.field.operador" /></strong>&nbsp;<span id="usuario_operador"></span>
					</div>
					
				</div>
	
			</form>
	
		</div>
	</div>
	
	<div class="col-md-12 col-sm-12 col-xs-12">
	
		<div class="btn-group pull-right btn-datatable">
			<a type="button" class="btn btn-info" id="button_return_ventas">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="ventareserva.operacionescaja.editarcaja.list.button.ventas" />"> <span class="fa fa-shopping-cart"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="button_return_infocierre">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="ventareserva.operacionescaja.editarcaja.list.button.infocierre" />"> <span class="fa fa-lock"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="button_return_infocierredet">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="ventareserva.operacionescaja.editarcaja.list.button.infocierredet" />"> <span class="fa fa-euro"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="button_return_infocaja">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="ventareserva.operacionescaja.editarcaja.list.button.infocaja" />"> <span class="fa fa-inbox"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="operacioncaja_edit">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="ventareserva.operacionescaja.editarcaja.list.button.editar" />"> <span class="fa fa-pencil"></span>
				</span>
			</a>
		</div>
	
		<table id="datatable-list-operaciones" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th></th>
					<th><spring:message code="ventareserva.operacionescaja.editarcaja.list.header.operacion" /></th>
					<th><spring:message code="ventareserva.operacionescaja.editarcaja.list.header.taquilla" /></th>
					<th><spring:message code="ventareserva.operacionescaja.editarcaja.list.header.usuario" /></th>
					<th><spring:message code="ventareserva.operacionescaja.editarcaja.list.header.fecha" /></th>
					<th><spring:message code="ventareserva.operacionescaja.editarcaja.list.header.importe" /></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<button id="button_return_list_operaciones" type="button" class="btn btn-primary pull-right">
			<spring:message code="common.button.return" />
		</button>
	</div>
	</div>
	
</div>

<script>
$today= moment().format("DD/MM/YYYY");
$('input[name="fechaAperturaIni"]').val($today);
$('input[name="fechaAperturaFin"]').val($today);
$('input[name="apertura"]').val($today + ' - ' + $today);

$('input[name="apertura"]').daterangepicker({
		autoUpdateInput: false,
		linkedCalendars: false,
		autoApply: true,
      	locale: $daterangepicker_sp
		}, function(start,end) {
      	  	 $('input[name="apertura"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
      	  	 $('input[name="fechaAperturaIni"]').val(start.format('DD/MM/YYYY'));
      	  	 $('input[name="fechaAperturaFin"]').val(end.format('DD/MM/YYYY'));
    	 });
    	 
$("#button_apertura_clear").on("click", function(e) {
    $('input[name="apertura"]').val('');
    $('input[name="fechaAperturaIni"]').val('');
    $('input[name="fechaAperturaFin"]').val('');
});


$("#idcanal").on("change", function(e) {
	showFieldSpinner("#div_idusuario");
	
	var data = $("#idcanal").val();
	$select=$("#idusuario");
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/operacionescaja/list_usuarios_canal.do'/>",
		timeout : 100000,
		data: {
	    	   data: data.toString()
			  }, 
		success : function(data) {
			hideSpinner("#div_idusuario");
			$select.html('');
			$select.prepend("<option value='' selected='selected'></option>");

			if (data.ArrayList!="") {
				var item= data.ArrayList.LabelValue;
				if (item.length>0)
				    $.each(item, function(key, val){
				      $select.append('<option value="' + val.value + '">' + val.login + '</option>');
				    });
				else
				      $select.append('<option value="' + item.value + '">' + item.login + '</option>');
			}
		},
		error : function(exception) {
			hideSpinner("#div_idusuario");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});	

}); 	

var dt_listcajas=$('#datatable-list-cajas').DataTable( {
	serverSide: true,
	searching: false,
	ordering: false,
	pagingType: "simple",
	info: false,
    ajax: {
        url: "<c:url value='/ajax/ventareserva/operacionescaja/list_cajas.do'/>",
        rowId: 'idcaja',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Caja)); return(""); },
        data: function (params) {
        	$('input[name="start"]').val(params.start);
        	$('input[name="length"]').val(params.length);
        	return ($("#form_filter_list_cajas").serializeObject());
       	},
        error: function (xhr, error, code)
        {
           	new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : xhr.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}					
			});
        }
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listcajas.data().count()>0) dt_listcajas.columns.adjust().draw(); });
	},
    columns: [
        { data: "idcaja", type: "numeric"}, 
        { data: "usuarioOperador.nombrecompleto", type: "spanish-string"}, 
        { data: "taquillas.taquilla", className: "cell_centered", defaultContent: "",
          render: function ( data, type, row, meta ) { 
        	  if (!data) 
        		  return '<span class="list_data">Sin taquilla</span>'
        	  else
        	  	  return showListInCell(data,"<spring:message code="ventareserva.operacionescaja.operacionescaja.list.text.taquillas" />","nombre"); 
          }	
        },
        { data: "fechaapertura", className: "cell_centered", type: "date", defaultContent: "" },
        { data: "fechacierre", className: "cell_centered", type: "date", defaultContent: "" },
        { data: "vistobueno", className: "text_icon cell_centered",
          render: function ( data, type, row, meta ) {
        	  if (data) return '<i class="fa fa-check-circle" aria-hidden="true"></i>'; else return '';	
          }
        }, 
        
    ],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-cajas') },
    select: { style: 'single', selector:'td:not(:nth-child(3))'},
	language: dataTableLanguage,
	processing: true
} );


insertSmallSpinner("#datatable-list-cajas_processing");


//********************************************************************************
$("#button_filter_list_cajas").on("click", function(e) { dt_listcajas.ajax.reload(); })

$("#operacionescaja_vbo").on("click", function(e) {
	var data = dt_listcajas.rows( { selected: true } ).data();

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="ventareserva.operacionescaja.operacionescaja.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	if (data[0].fechacierre==="") {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="ventareserva.operacionescaja.operacionescaja.list.alert.caja_no_cerrada" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/operacionescaja/darvistobueno.do'/>",
		timeout : 100000,
		data: {
	    	   data: data[0].idcaja
			  }, 
		success : function(data) {
			dt_listcajas.processing(false);	
			dt_listcajas.ajax.reload(null,false);
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : { sticker : false	}
			});			
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});	
});

$("#operacionescaja_lock").on("click", function(e) {
	var data = dt_listcajas.rows( { selected: true } ).data();

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="ventareserva.operacionescaja.operacionescaja.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	if (data[0].fechacierre!="") {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="ventareserva.operacionescaja.operacionescaja.list.alert.caja_cerrada" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/ventareserva/operacionescaja/cierre_caja.do'/>?id="+data[0].idcaja, function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "sm");
	});

});

//********************************************************************************
var dt_listoperaciones=$('#datatable-list-operaciones').DataTable( {
    ajax: {
        url: "<c:url value='/ajax/ventareserva/operacionescaja/list_operaciones.do'/>",
        rowId: 'idcaja',
        type: 'POST',
        deferLoading: 0,
        dataSrc: function (json) { if (json!=null && typeof json!="undefined" && json.Caja!=null && typeof json.Caja!="undefined") return (sanitizeJSON(json.Caja.operacioncajas.Operacioncaja)); return(""); },
		data: function (params) {
			var data = dt_listcajas.rows( { selected: true } ).data();
			if (data.length>0) {
				$("#principal").hide();
				$("#secundaria").show();
				$("#num_caja").html(data[0].idcaja);
				$("#usuario_operador").html(data[0].usuarioOperador.nombrecompleto);
	        	return ({data: data[0].idcaja});
			}
			else
				new PNotify({
				      title: '<spring:message code="common.dialog.text.error" />',
				      text: '<spring:message code="ventareserva.operacionescaja.operacionescaja.list.alert.ninguna_seleccion" />',
					  type: "error",
					  delay: 5000,
					  buttons: { sticker: false }
				   });
				return ({data: "0"});
		},
        error: function (xhr, error, code)
        {
           	new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : xhr.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}					
			});
        }
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listoperaciones.data().count()>0) dt_listoperaciones.columns.adjust().draw(); });
	},
	columnDefs: [
        { "visible": false, "targets": 0 }
    ],
    columns: [
        { data: "idoperacioncaja", type: "numeric", defaultContent: "" }, 
        { data: "tipooperacioncaja.nombre", type: "spanish-string", defaultContent: ""}, 
        { data: "taquilla.nombre", type: "spanish-string", defaultContent: ""}, 
        { data: "usuario.nombrecompleto", type: "spanish-string", defaultContent: ""}, 
        { data: "fechayhoraoperacioncaja", className: "cell_centered", type: "date", defaultContent: ""}, 
        { data: "importe", type: "numeric"}, 
    ],
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-operaciones') },
    select: { style: 'single', selector:'td:not(:last-child)'},
	language: dataTableLanguage,
	processing: true,
} );

insertSmallSpinner("#datatable-list-operaciones_processing");

$("#operacionescaja_edit").on("click", function(e) {
		dt_listoperaciones.ajax.reload();
});

$("#operacioncaja_edit").on("click", function(e) {
	var data = dt_listoperaciones.rows( { selected: true } ).data();

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="ventareserva.operacionescaja.operacionescaja.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	
	
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/ventareserva/operacionescaja/edicion_op_caja.do'/>?id="+data[0].idoperacioncaja, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "sm");
	});

});

$("#button_return_list_operaciones").on("click", function(e) {
	$("#secundaria").hide();
	dt_listoperaciones.clear();
	dt_listoperaciones.draw();
	$("#principal").show();
});

$("#button_return_ventas").on("click", function(e) {
	showButtonSpinner("#button_return_ventas");
	var data = dt_listcajas.rows( { selected: true } ).data();
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ventareserva/operacionescaja/list_ventas_caja.do'/>?idcaja="+data[0].idcaja, function() {										  
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "md");
	});
	
});

$("#button_return_infocierre").on("click", function(e) {
	var data = dt_listcajas.rows( { selected: true } ).data();
	
	window.open("../../verInformeCierreCaja.action?idcaja="+data[0].idcaja+"&nombreinforme=VR_CJ_001", "_blank");
	
});

$("#button_return_infocierredet").on("click", function(e) {
	var data = dt_listcajas.rows( { selected: true } ).data();

	window.open("../../verInformeCierreDetCaja.action?Caja_Ini="+data[0].idcaja+"&Caja_Fin="+data[0].idcaja+"&nombreinforme=VR_CJ_002","_blank");

});

$("#button_return_infocaja").on("click", function(e) {
	window.open("${operacionescaja_urlgestor}","_blank");
});

</script>

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-2"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-3"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-4"/>
<tiles:insertAttribute name="modal_dialog" />

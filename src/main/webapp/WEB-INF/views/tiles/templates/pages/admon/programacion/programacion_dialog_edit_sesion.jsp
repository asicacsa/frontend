<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editarsesion_datos_sesion}" var="editarsesion_datos_sesion_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<x:choose>
			<x:when select="not($editarsesion_datos_sesion_xml/Altasesionparam)">
				<spring:message code="administracion.programacion.programacion.dialog.crear_sesion.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.programacion.programacion.dialog.editar_sesion.title" />
			</x:otherwise>
		</x:choose>
	</h4>
</div>

<div class="modal-body">

	<form id="form_sesion_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

		<input id="idsesion" name="idsesion" type="hidden" value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/idsesion" />" />
		<input id="idcontenido_dialogo" name="idcontenido_dialogo" type="hidden" value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/contenido/idcontenido" />" />

		<div id="columna-izquierda" class="col-md-6 col-sm-12 col-xs-12">
		
			<div id="grupo-principal">
			
				<div class="form-group button-dialog">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.dialog.editar_sesion.field.contenido" />*</label>
					<div class="col-md-9 col-sm-9 col-xs-9">
			  			<a type="button" class="btn btn-default btn-dialog" id="button-select-contenido" >
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.dialog.editar_sesion.list.button.select_contenido" />"> <span class="fa fa-pencil"></span></span>
				  		</a>		
                        <div class="input-prepend">
							<input name="contenido" id="contenido" type="text" class="form-control" required="required" readonly value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/contenido/nombre" />">
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.dialog.editar_sesion.field.recinto" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="idrecinto_dialogo" id="idrecinto_dialogo" type="hidden"/>
						<input name="recinto" id="recinto" type="text" class="form-control" readonly value=""/>
					</div>
				</div>
			
			</div>

			<div id="grupo-programacion">
		
				<div class="x_title">
					<h5><spring:message code="administracion.programacion.dialog.editar_sesion.title.programacion" /></h5>
					<div class="clearfix"></div>
				</div>
			
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.dialog.editar_sesion.field.horainicio" />*</label>
					<div class="col-md-2 col-sm-2 col-xs-2">
	                	<input type="text" name="horainicio" id="horainicio" class="form-control" data-inputmask="'mask': '99:99'" required="required" value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/horainicio" />"/>
					</div>
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.dialog.editar_sesion.field.horafin" />*</label>
					<div class="col-md-2 col-sm-2 col-xs-2">
	                	<input type="text" name="horafin" id="horafin" class="form-control" data-inputmask="'mask': '99:99'" required="required" value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/horafin" />"/>
					</div>
				  	<a type="button" class="btn btn-default btn-search-hour" id="button-select-franja">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.dialog.editar_sesion.list.button.clear" />"> <span class="fa fa-binoculars"></span>
						</span>
				  	</a>			
				</div>
				
				<div id="fecha-programacion-edicion" style="display: none;">
				
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.dialog.editar_sesion.field.fecha" />*</label>
						<div class="col-md-9 col-sm-9 col-xs-9">
	                       <div class="input-prepend input-group">
	                         <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	                         <input type="text" name="fecha_sesion" id="fecha_sesion" class="form-control" required="required" value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/fecha" />" readonly/>
	                       </div>
						</div>
					</div>
				
				</div>
				
				<div id="fecha-programacion-creacion">
					
					<fieldset id="fecha-programacion">
					
						<div class="form-group">
							<div class="checkbox col-md-5 col-sm-5 col-xs-5">
								<input type="radio" name="fecha-programacion" id="periodo-tiempo" checked value="" data-parsley-multiple="programacion-select" class="flat" style="position: absolute; opacity: 0;">
								<strong><spring:message code="administracion.programacion.dialog.editar_sesion.field.periodo-tiempo" /></strong>
							</div>
						</div>
						<div id="periodo-tiempo-div">
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.programacion.field.fecha" /></label>
								<div class="col-md-9 col-sm-9 col-xs-9">
			                        <div class="input-prepend input-group">
			                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
			                          <input type="text" name="fecha_creacion" id="fecha_creacion" class="form-control" value="" readonly/>
			                          <input type="hidden" name="fecha_creacion_ini" value=""/>
			                          <input type="hidden" name="fecha_creacion_fin" value=""/>
			                        </div>
								</div>
							</div>
		
							<fieldset id="periodicidad">
								<div class="form-group">
									<div class="col-md-6 col-sm-6 col-xs-6 col-md-offset-1">
										<input type="radio" name="periodicidad" id="diario" checked value="" class="flat" style="position: absolute; opacity: 0;">
										<strong><spring:message code="administracion.programacion.dialog.editar_sesion.field.diario" /></strong>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-6 col-sm-6 col-xs-6 col-md-offset-1">
										<input type="radio" name="periodicidad" id="dias-semana" value="" class="flat" style="position: absolute; opacity: 0;">
										<strong><spring:message code="administracion.programacion.dialog.editar_sesion.field.dias_semana" /></strong>
									</div>
									<div class="col-md-5 col-sm-5 col-xs-5" style="margin-top:-10px">
										<select name="dias-semana-select" id="dias-semana-select" class="form-control select-select2 select2_single_line select2_multiple" multiple="multiple" disabled="disabled" style="width: 100%">
										  <option><spring:message code="common.dialog.text.weekdays.mo" /></option>
										  <option><spring:message code="common.dialog.text.weekdays.tu" /></option>
										  <option><spring:message code="common.dialog.text.weekdays.we" /></option>
										  <option><spring:message code="common.dialog.text.weekdays.th" /></option>
										  <option><spring:message code="common.dialog.text.weekdays.fr" /></option>
										  <option><spring:message code="common.dialog.text.weekdays.sa" /></option>
										  <option><spring:message code="common.dialog.text.weekdays.su" /></option>
										</select>
									</div>
								</div>						
							</fieldset>
						</div>
			
						<div class="form-group">
							<div class="checkbox col-md-5 col-sm-5 col-xs-5">
								<input type="radio" name="fecha-programacion" id="dias-concretos" value="" data-parsley-multiple="programacion-select" class="flat" style="position: absolute; opacity: 0;">
								<strong><spring:message code="administracion.programacion.dialog.editar_sesion.field.dias-concretos" /></strong>
							</div>
						</div>
						<div id="dias-concretos-div">
							<div class="form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.dialog.editar_sesion.field.fecha" /></label>
								<div class="col-md-9 col-sm-9 col-xs-9">
				                       <div class="input-prepend input-group">
				                         <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
				                         <input type="text" name="fecha_creacion_simple" id="fecha_creacion_simple" class="form-control" disabled="disabled" value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/fecha" />" readonly/>
				                       </div>
								</div>
							</div>
						</div>
					
					</fieldset>
				
				</div>
	
				<div id="afluencia" class="form-group button-dialog">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.dialog.editar_sesion.field.horas" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
			  			<a type="button" class="btn btn-default btn-dialog" id="button-mostrar-afluencia" style="display:none;" >
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.dialog.editar_sesion.list.button.ver_afluencia" />"> <span class="fa fa-group"></span></span>
				  		</a>		
			  			<a type="button" class="btn btn-default btn-dialog" id="button-select-horas" >
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.dialog.editar_sesion.list.button.select_plantilla_horas" />"> <span class="fa fa-clock-o"></span></span>
				  		</a>		
                        <div class="input-prepend">
							<input name="horas_id" id="horas_id" type="hidden" value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/plantillafranjas/idplantillafranjas" />">
							<input name="horas" id="horas" type="text" class="form-control" readonly value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/plantillafranjas/nombre" />">
						</div>
					</div>			
				</div>
				
			</div>	

		</div>

		<div id="columna-derecha" class="col-md-6 col-sm-12 col-xs-12">
		
			<div id="grupo-aforo">
		
				<div class="x_title">
					<h5><spring:message code="administracion.programacion.dialog.editar_sesion.title.aforo" /></h5>
					<div class="clearfix"></div>
				</div>
				
				<div class="form-group">
					<div class="checkbox col-md-6 col-sm-6 col-xs-6">
						<input type="checkbox" name="oaforo" id="oaforo" value="" class="flat" style="position: absolute; opacity: 0;">
						<strong><spring:message code="administracion.programacion.dialog.editar_sesion.field.oaforo" /></strong>
					</div>
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.dialog.editar_sesion.field.cantidad_oaforo" /></label>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<input name="overbookingaforo" id="overbookingaforo" type="text" disabled="disabled" class="form-control" value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/overbookingaforo" />">
					</div>
				</div>
	
				<div class="form-group">
					<div class="checkbox col-md-6 col-sm-6 col-xs-6">
						<input type="checkbox" name="oventa" id="oventa" value="" class="flat" style="position: absolute; opacity: 0;">
						<strong><spring:message code="administracion.programacion.dialog.editar_sesion.field.oventa" /></strong>
					</div>
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.dialog.editar_sesion.field.cantidad_oventa" /></label>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<input name="overbookingventa" id="overbookingventa" type="text" disabled="disabled" class="form-control" value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/overbookingventa" />">
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="administracion.programacion.dialog.editar_sesion.field.aforo_total" /></label>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<input name="aforo-total" id="aforo-total" type="hidden" value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/contenido/tipoproducto/aforo" />">
						<input name="aforo-total-calculado" id="aforo-total-calculado" type="text" class="form-control" readonly value="">
					</div>
				</div>
			
			</div>
							
			<div id="grupo-restricciones">
			
				<div class="x_title">
					<h5><spring:message code="administracion.programacion.dialog.editar_sesion.title.restricciones" /></h5>
					<div class="clearfix"></div>
				</div>
				
				<fieldset id="inicio-venta">
				
					<div class="form-group">
						<div class="checkbox col-md-5 col-sm-5 col-xs-5">
							<input type="radio" name="inicio-venta" id="iniventa" checked value="" data-parsley-multiple="inicio_sesion" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="administracion.programacion.dialog.editar_sesion.field.iniventa" /></strong>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-3">
							<input name="fechainicioventa" id="fechainicioventa" type="text" class="form-control" data-inputmask="'mask': '99/99/9999'" value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/fechainicioventa" />">
						</div>
						<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="administracion.programacion.dialog.editar_sesion.field.hora_iniventa" /></label>
						<div class="col-md-2 col-sm-2 col-xs-2">
							<input name="horainicioventa" id="horainicioventa" type="text" class="form-control" data-inputmask="'mask': '99:99'" value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/horainicioventa" />">
						</div>
					</div>
		
					<div class="form-group">
						<div class="checkbox col-md-5 col-sm-5 col-xs-5">
							<input type="radio" name="inicio-venta" id="hora-antes" value="" data-parsley-multiple="inicio_sesion" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="administracion.programacion.dialog.editar_sesion.field.hora_antes" /></strong>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-2">
							<input name="cantidad-horas-antes" id="cantidad-horas-antes" disabled="disabled" type="text" class="form-control" data-inputmask="'mask': '99:99'" value="">
						</div>
					</div>
				
				</fieldset>

				<fieldset id="fin-venta">
				
					<div class="form-group">
						<div class="checkbox col-md-5 col-sm-5 col-xs-5">
							<input type="radio" name="fin-venta" id="finventa" checked value="" data-parsley-multiple="fin_sesion" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="administracion.programacion.dialog.editar_sesion.field.finventa" /></strong>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-3">
							<input name="fechafinventa" id="fechafinventa" type="text" class="form-control" data-inputmask="'mask': '99/99/9999'" value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/fechafinventa" />">
						</div>
						<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="administracion.programacion.dialog.editar_sesion.field.hora_finventa" /></label>
						<div class="col-md-2 col-sm-2 col-xs-2">
							<input name="horafinventa" id="horafinventa" type="text" class="form-control" data-inputmask="'mask': '99:99'" value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/horafinventa" />">
						</div>
					</div>
		
					<div class="form-group">
						<div class="checkbox col-md-5 col-sm-5 col-xs-5">
							<input type="radio" name="fin-venta" id="hora-despues" value="" data-parsley-multiple="fin_sesion" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="administracion.programacion.dialog.editar_sesion.field.hora_despues" /></strong>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-2">
							<input name="cantidad-horas-despues" id="cantidad-horas-despues" disabled="disabled" type="text" class="form-control" data-inputmask="'mask': '99:99'" value="">
						</div>
					</div>

				</fieldset>
			
				<div class="form-group">
					<div class="col-md-8 col-sm-8 col-xs-8">
						<label class="control-label"><spring:message code="administracion.programacion.dialog.editar_sesion.field.apertura_tornos" /></label>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-2">
						<input name="aperturatornos" id="aperturatornos" type="text" class="form-control" data-inputmask="'mask': '99:99'" value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/aperturatornos" />">
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-8 col-sm-8 col-xs-8">
						<label class="control-label"><spring:message code="administracion.programacion.dialog.editar_sesion.field.cierre_tornos" /></label>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-2">
						<input name="cierretornos" id="cierretornos" type="text" class="form-control" data-inputmask="'mask': '99:99'" value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/cierretornos" />">
					</div>
				</div>
				
			</div>
			
			<div id="grupo-incidencias" style="display: none;">
			
				<div class="x_title">
					<h5><spring:message code="administracion.programacion.dialog.editar_sesion.title.incidencias" /></h5>
					<div class="clearfix"></div>
				</div>
				
				<div class="form-group">
					<label class="control-label"><spring:message code="administracion.programacion.dialog.editar_sesion.field.incidencias" /></label>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<select class="form-control" name="idincidencia" id="idincidencia">
							<option value=""></option>
							<option value="000"><spring:message code="administracion.programacion.dialog.editar_sesion.field.incidencias_000" /></option>
							<option value="005"><spring:message code="administracion.programacion.dialog.editar_sesion.field.incidencias_005" /></option>
							<option value="006"><spring:message code="administracion.programacion.dialog.editar_sesion.field.incidencias_006" /></option>
							<option value="007"><spring:message code="administracion.programacion.dialog.editar_sesion.field.incidencias_007" /></option>
							<option value="008"><spring:message code="administracion.programacion.dialog.editar_sesion.field.incidencias_008" /></option>
							<option value="111"><spring:message code="administracion.programacion.dialog.editar_sesion.field.incidencias_111" /></option>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-2 col-sm-2 col-xs-2">
						<input name="idincidencia-txt" id="idincidencia-txt" type="text" class="form-control" value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/IncidenciaICAA/codigo" />" disabled/>
					</div>
				</div>
			
			</div>

		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_sesion_button" type="submit" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
		
	</form>

</div>


<script>



var aforo = 0;

<x:forEach select="$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/contenido" var="item">
	<c:set var="aforo"><x:out select="$item/aforo"/></c:set>
	<c:if test="${not empty aforo}">
		aforo = aforo+ <x:out select="$item/aforo" />;
	</c:if>
</x:forEach>

if(aforo == 0)
  aforo = '<x:out select="$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/zonasesions/Zonasesion/zona/aforo" />';
	
	
$('#aforo-total').val(aforo);


// Al calcular el aforo-total-calculado se suma 0 al principio del parseInt para evitar que el resultado de la operación de NaN porque uno de ellos es null
function calcular_aforo_total() {
	var aforo_total_calculado= parseInt(0+$('#aforo-total').val());
	
	

	if ($("#oaforo:checked").length && $('#overbookingaforo').val().length && (!isNaN($('#overbookingaforo').val()))) 
		{
		var overbooking = parseInt($('#overbookingaforo').val());
		aforo_total_calculado+= overbooking;
		}
		return(aforo_total_calculado);
}

$(document).ready(function() {
	hideSpinner("#button_programacion_new");
	hideSpinner("#button_programacion_edit");
	
	$('#idincidencia option[value="<x:out select = "$editarsesion_datos_sesion_xml/Altasesionparam/sesionPlantilla/IncidenciaICAA/codigo" />"]').attr("selected", "selected");
	
	$(".select-select2").select2({
		language : "es",
	});

	$('input[name="fecha_creacion_ini"]').val($today);
	$('input[name="fecha_creacion_fin"]').val($today);
	$('input[name="fecha_creacion"]').val($today + ' - ' + $today);

	$('input[name="fecha_creacion"]').daterangepicker({
			autoUpdateInput: false,
			linkedCalendars: false,
			autoApply: true,
	      	locale: $daterangepicker_sp
			}, function(start,end) {
	      	  	 $('input[name="fecha_creacion"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
	      	  	 $('input[name="fecha_creacion_ini"]').val(start.format('DD/MM/YYYY'));
	      	  	 $('input[name="fecha_creacion_fin"]').val(end.format('DD/MM/YYYY'));
	    	 });

	$('input[name="fecha_creacion_simple"]').datepicker({
		language: 'es',
		multidate: true
	});
	
	if ("${nueva_sesion}"=="0") {
		$("#grupo-aforo").detach().appendTo($("#columna-izquierda"));
		$("#grupo-incidencias").show();
		$("#afluencia").removeClass("button-dialog").addClass("button-2dialog");
		$("#button-mostrar-afluencia").show();
		$("#fecha-programacion-edicion").show();
		$("#fecha-programacion-creacion").hide();
	}
	else {
		/* Se inicializan campos */
		$('input[name="fechainicioventa"]').val($today);
		$('input[name="horainicioventa"]').val(moment().format("hh:mm"));
		$('input[name="aperturatornos"]').val("00:00");
		$('input[name="cierretornos"]').val("00:00");
	}
	 
	$(":input").inputmask();
	
	if ($("input.flat")[0]) {
        $(document).ready(function () {
            $('input.flat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
    }
	
	$('input[name="recinto"]').val($("#idrecinto :selected").text());
	$('input[name="idrecinto_dialogo"]').val($("#idrecinto :selected").val());
	
    $('input[name="fecha_sesion"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
      	locale: $daterangepicker_sp
    });

    $("#oaforo").on("ifChanged", function(e) {
    	if ($('input[name="overbookingaforo"]').attr('disabled')) 
    		$('input[name="overbookingaforo"]').removeAttr('disabled');
        else 
        	$('input[name="overbookingaforo"]').attr('disabled', 'disabled');
    	$('#aforo-total-calculado').val(calcular_aforo_total());
    });
    
    $("#oventa").on("ifChanged", function(e) {
    	if ($('input[name="overbookingventa"]').attr('disabled')) 
    		$('input[name="overbookingventa"]').removeAttr('disabled');
        else 
        	$('input[name="overbookingventa"]').attr('disabled', 'disabled');
    });

    $("#iniventa").on("ifChanged", function(e) {
    	if ($('input[name="fechainicioventa"]').attr('disabled')) { 
    		$('input[name="fechainicioventa"]').removeAttr('disabled');
    		$('input[name="horainicioventa"]').removeAttr('disabled');
    	}
        else { 
        	$('input[name="fechainicioventa"]').attr('disabled', 'disabled');
        	$('input[name="horainicioventa"]').attr('disabled', 'disabled');
        }
    });

    $("#hora-antes").on("ifChanged", function(e) {
    	if ($('input[name="cantidad-horas-antes"]').attr('disabled')) 
    		$('input[name="cantidad-horas-antes"]').removeAttr('disabled');
        else 
        	$('input[name="cantidad-horas-antes"]').attr('disabled', 'disabled');
    });

    $("#finventa").on("ifChanged", function(e) {
    	if ($('input[name="fechafinventa"]').attr('disabled')) { 
    		$('input[name="fechafinventa"]').removeAttr('disabled');
    		$('input[name="horafinventa"]').removeAttr('disabled');
    	}
        else { 
        	$('input[name="fechafinventa"]').attr('disabled', 'disabled');
        	$('input[name="horafinventa"]').attr('disabled', 'disabled');
        }
    });

    $("#hora-despues").on("ifChanged", function(e) {
    	if ($('input[name="cantidad-horas-despues"]').attr('disabled')) 
    		$('input[name="cantidad-horas-despues"]').removeAttr('disabled');
        else 
        	$('input[name="cantidad-horas-despues"]').attr('disabled', 'disabled');
    });

    $("#periodo-tiempo").on("ifChanged", function(e) {
    	if ($('input[name="fecha_creacion"]').attr('disabled')) { 
	    	$("#periodo-tiempo-div :input").removeAttr("disabled");
	    	if (!$("#dias-semana:checked").length) $("#dias-semana-select").attr("disabled", "disabled");
    	}
    	else
    		$("#periodo-tiempo-div :input").attr("disabled", "disabled");
    });

    $("#dias-concretos").on("ifChanged", function(e) {
    	if ($('input[name="fecha_creacion_simple"]').attr('disabled')) 
	    	$("#dias-concretos-div :input").removeAttr("disabled");
    	else
    		$("#dias-concretos-div :input").attr("disabled", "disabled");
    });

    $("#dias-semana").on("ifChanged", function(e) {
    	if ($('#dias-semana-select').attr('disabled')) 
	    	$("#dias-semana-select").removeAttr("disabled");
    	else
    		$("#dias-semana-select").attr("disabled", "disabled");
    });

    /* Selector de contenido en un diálogo adicional */

    $("#button-select-contenido").on("click", function(e) {
    	
    	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/select_contenido.do'/>?id="+$("#idrecinto :selected").val(), function() {
        	   
            $("#modal-dialog-form-2").modal('show');
            setModalDialogSize("#modal-dialog-form-2", "xs");
      });
    	
    });
    
    /* Selector de franjas horarias */

    $("#button-select-franja").on("click", function(e) {
    	
    	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/select_franja.do'/>", function() {
        	   
            $("#modal-dialog-form-2").modal('show');
            setModalDialogSize("#modal-dialog-form-2", "xs");
      });
    	
    });
    
    /* Selector de plantillas de presentación */

    $("#button-select-horas").on("click", function(e) {
    	
    	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/select_plantillas_presentacion.do'/>", function() {
        	   
            $("#modal-dialog-form-2").modal('show');
            setModalDialogSize("#modal-dialog-form-2", "sm");
      });
    	
    });
    
    /* Ver afluencia */

    $("#button-mostrar-afluencia").on("click", function(e) {

    	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/select_ver_afluencia.do'/>", function() {
        	   
            $("#modal-dialog-form-2").modal('show');
            setModalDialogSize("#modal-dialog-form-2", "sm");
      });
    	
    });
    
    /* Overbooking */
    
    if ($("#overbookingaforo").val()!=0) $('#oaforo').iCheck('check'); 
    if ($("#overbookingventa").val()!=0) $('#oventa').iCheck('check');
    
	$('#aforo-total-calculado').val(calcular_aforo_total());
	
    $("#overbookingaforo").on("change", function(e) {
    	$('#aforo-total-calculado').val(calcular_aforo_total());
    });
    
    /* Incidencias */
    $("#idincidencia").on("change", function(e) {
    	$("#idincidencia-txt").val($("#idincidencia option:selected").val());
    });
    
    /* Procesado de datos */
    
	$("#form_sesion_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			if ( $('input[name="aperturatornos"]').val()=="00:00" &&
				 $('input[name="cierretornos"]').val()=="00:00") {
				new PNotify({
                    title: '<spring:message code="administracion.programacion.dialog.editar_sesion.alert.tornos.title" />',
	                text: '<spring:message code="administracion.programacion.dialog.editar_sesion.alert.tornos.text" />',
                    icon: 'glyphicon glyphicon-question-sign',
                    hide: false,
                    confirm: {
                        confirm: true,
                        buttons: [
                        {
                          text: 'Aceptar',
                          closer: true,
                          addClass: 'btn-primary',
                          click: function(notice){
                              notice.remove();
                              saveSesionData();
                          }
                        },
                        {
                          text: 'Cancelar',
                          closer: true,
                          click: function(notice){
                        	  notice.remove();
                          }
                        }
                      ]
                    },
                    buttons: {
                        closer: false,
                        sticker: false
                    },
                    history: {
                        history: false
                    },
                    addclass: 'stack-modal',
                    stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
				});
			}
			else
				saveSesionData();
		}
	});
    
	function saveSesionData() {
		var data = $("#form_sesion_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/programacion/save_sesion.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				dt_listprogramacion.ajax.reload(null,false);			
				$("#modal-dialog-form").modal('hide');
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
	}
    
});

</script>

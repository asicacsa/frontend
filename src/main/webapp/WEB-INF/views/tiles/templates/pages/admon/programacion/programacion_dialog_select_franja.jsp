<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="administracion.programacion.dialog.select_franja.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_select_franja" class="form-horizontal form-label-left">

		<div class="col-md-12 col-sm-12 col-xs-12 vigente">
		
			<table id="datatable-list-franjas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th><spring:message code="administracion.programacion.franja.list.header.nombre" /></th>
						<th><spring:message code="administracion.programacion.franja.list.header.horaInicio" /></th>
						<th><spring:message code="administracion.programacion.franja.list.header.horaFin" /></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			
		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_franja" type="button" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
		
	</form>
</div>


<script>

var dt_listfranjas=$('#datatable-list-franjas').DataTable( {
	language: dataTableLanguage,
	processing: false,
	scrollY: "200px",
	scrollCollapse: true,
	paging: false,
	info: false,
	searching: false,
    select: { style: 'single'},
    ajax: {
        url: "<c:url value='/ajax/admon/programacion/list_franjas.do'/>",
        rowId: 'idfranjahoraria',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Franjahoraria)); return(""); },
        data: null
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listfranjas.data().count()>0) dt_listfranjas.columns.adjust().draw(); });
	},
    columns: [
        { data: "nombre", className: "cell_centered", type: "spanish-string", defaultContent: "" },
        { data: "horainicio", className: "cell_centered", type: "spanish-string", defaultContent: "" },
        { data: "horafin", className: "cell_centered", type: "spanish-string", defaultContent: "" },
    ],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-franjas') }
} );

$("#save_franja").on("click", function(e) {
	$("#modal-dialog-form-2").modal('hide');
	$("#horainicio").val(dt_listfranjas.rows('.selected').data()[0].horainicio);
	$("#horafin").val(dt_listfranjas.rows('.selected').data()[0].horafin);
});

function ajustar_cabeceras_datatable()
{
	$('#datatable-list-franjas').DataTable().columns.adjust().draw();
}

setTimeout(ajustar_cabeceras_datatable, 200);

</script>

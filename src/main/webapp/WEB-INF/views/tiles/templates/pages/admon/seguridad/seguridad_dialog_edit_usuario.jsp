<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editarusuario_datos_usuario}" var="editarusuario_datos_usuario_xml" />
<x:parse xml="${editarusuario_selector_canal}" var="editarusuario_selector_canal_xml" />
<x:parse xml="${editarusuario_selector_perfiles}" var="editarusuario_selector_perfiles_xml" />
<x:parse xml="${editarusuario_selector_unidades}" var="editarusuario_selector_unidades_xml" />
<x:parse xml="${editarusuario_selector_grupos}" var="editarusuario_selector_grupos_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">x</span>
	</button>
	<h4 class="modal-title">
		<x:choose>
			<x:when select="not($editarusuario_datos_usuario_xml/Usuario)">
				<spring:message code="administracion.seguridad.tabs.usuarios.dialog.crear_usuario.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.seguridad.tabs.usuarios.dialog.editar_usuario.title" />
			</x:otherwise>
		</x:choose>
	</h4>
</div>
<div class="modal-body">

	<form id="form_user_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

		<input id="idusuario" name="idusuario" type="hidden" value="<x:out select = "$editarusuario_datos_usuario_xml/Usuario/idusuario" />" />

		<div class="col-md-5 col-sm-5 col-xs-12">

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.nombre" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editarusuario_datos_usuario_xml/Usuario/nombre" />">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.apellidos" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="apellidos" id="apellidos" type="text" class="form-control" required="required" value="<x:out select = "$editarusuario_datos_usuario_xml/Usuario/apellidos" />">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.dni" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="dni" type="text" class="form-control" value="<x:out select = "$editarusuario_datos_usuario_xml/Usuario/dni" />">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.login" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="login" type="text" class="form-control" required="required" value="<x:out select = "$editarusuario_datos_usuario_xml/Usuario/login" />">
				</div>
			</div>

			<x:if select="not($editarusuario_datos_usuario_xml/Usuario)">
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.contrasena" />*</label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="contrasena" id="contrasena" class="form-control" required="required" type="password">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.confirmar_contrasena" />*</label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="confirmar_contrasena" id="confirmar_contrasena" class="form-control" required="required" type="password">
					</div>
				</div>
			</x:if>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.canal" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="idcanal" id="selector_canal" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$editarusuario_selector_canal_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.bloqueado" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="bloqueado" type="checkbox" class="js-switch" <x:if select="$editarusuario_datos_usuario_xml/Usuario/cuentabloqueada=1">checked=""</x:if> data-switchery="true" style="display: none;">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.auditado" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="auditado" type="checkbox" class="js-switch" <x:if select="$editarusuario_datos_usuario_xml/Usuario/auditado=1">checked=""</x:if> data-switchery="true" style="display: none;">
				</div>
			</div>

		</div>


		<div class="col-md-7 col-sm-7 col-xs-12">

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.unidades" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="unidades[]" id="selector_unidades" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%" required="required">
						<x:forEach select="$editarusuario_selector_unidades_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.grupos" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="grupos[]" id="selector_grupos" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
						<x:forEach select="$editarusuario_selector_grupos_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.perfiles" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="perfiles[]" id="selector_perfiles" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%" required="required">
						<x:forEach select="$editarusuario_selector_perfiles_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>


			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.redes" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select id="selector_redes" class="select-select2 select2_multiple form-control" multiple="multiple" style="width: 100%" disabled="true">
						<x:forEach select="$editarusuario_datos_usuario_xml/Usuario/reds/Red" var="item">
							<option selected="true" value="<x:out select="$item/idred" />"><x:out select="$item/nombre" /></option>
						</x:forEach>
					</select>
				</div>
			</div>

		</div>
	</form>

		<div class="modal-footer">
			<x:if select="($editarusuario_datos_usuario_xml/Usuario)">
				<button id="change_user_password" type="button" class="btn btn-danger" onclick="changeUserPasswordByAdmin();">
					<spring:message code="dashboard.profile_menu.edit_password" />
				</button>
			</x:if>
			<button id="save_user_button" type="button" class="btn btn-primary save_dialog">
				<spring:message code="common.button.save" />
			</button>
			<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
				<spring:message code="common.button.cancel" />
			</button>
		</div>

</div>

<div id="form_notice_change_password_by_admin" style="display: none;">
	<form id="change_user_password_by_admin_form" class="pf-form pform_custom">

		<input id="id_usuario" name="id_usuario" type="hidden" value="<x:out select = "$editarusuario_datos_usuario_xml/Usuario/idusuario" />" />

		<div class="pf-element pf-heading">
			<h3 style="margin-top: 0;">
				<spring:message code="dashboard.profile_menu.edit_password" />
			</h3>
		</div>
		<div class="pf-element">
			<label> <span class="pf-label"><spring:message code="dashboard.profile_menu.new_password" /></span> <input class="pf-field form-control" type="password" name="new_password" id="new_password"/>
			</label>
		</div>
		<div class="pf-element">
			<label> <span class="pf-label"><spring:message code="dashboard.profile_menu.repeat_password" /></span> <input class="pf-field form-control" type="password" name="repeat_password" id="repeat_password"/>
			</label>
		</div>
		<div class="pf-element pf-buttons pf-centered">
			<input class="pf-button btn btn-primary" type="submit" name="submit" value="<spring:message code="common.button.save" />"> <input class="pf-button btn btn-default" type="button" name="cancel" value="<spring:message code="common.button.cancel" />">
		</div>
	</form>
</div>

<script>
	hideSpinner("#tab_usuarios_new");
	hideSpinner("#tab_usuarios_edit");

	$("#save_user_button").on("click", function(e) {
		$("#form_user_data").submit();	
	})
	
	$(".select-select2").select2({
		language : "es",
		containerCssClass : "single-line"
	});

	$(".js-switch").each(function(index) {
		var s = new Switchery(this, {
			size : 'small'
		});
	});

	var selectedValues = new Array();
	<x:forEach select="$editarusuario_datos_usuario_xml/Usuario/usuariounidadnegocios/Usuariounidadnegocio/unidadnegocio" var="item">
	selectedValues.push('<x:out select="$item/idunidadnegocio" />');
	</x:forEach>
	$('#selector_unidades').val(selectedValues);
	$('#selector_unidades').trigger('change.select2');

	selectedValues = new Array();
	<x:forEach select="$editarusuario_datos_usuario_xml/Usuario/grupousuarios/Grupousuario/grupo" var="item">
	selectedValues.push('<x:out select="$item/idgrupo" />');
	</x:forEach>
	$('#selector_grupos').val(selectedValues);
	$('#selector_grupos').trigger('change.select2');

	selectedValues = new Array();
	<x:forEach select="$editarusuario_datos_usuario_xml/Usuario/perfilusuariousuarios/Perfilusuariousuario/perfilusuario" var="item">
	selectedValues.push('<x:out select="$item/idperfilusuario" />');
	</x:forEach>
	$('#selector_perfiles').val(selectedValues);
	$('#selector_perfiles').trigger('change.select2');

	$('#selector_canal option[value="<x:out select = "$editarusuario_datos_usuario_xml/Usuario/canal/idcanal" />"]').attr("selected", "selected");

	$("#form_user_data").validate({
		onfocusout : false,
		onkeyup : false,
		rules : {
			confirmar_contrasena : {
				equalTo : "#contrasena"
			}
		},
		unhighlight : function(element, errClass) {
			$(element).popover('hide');
		},
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({
				placement : 'bottom',
				offset : 20,
				trigger : 'manual'
			});
			$(element).popover('show');
		},
		submitHandler : function(form) {
			saveFormData();
		}
	}

	);

	
	//********************************************************************************	
	function saveFormData() {
		var data = $("#form_user_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/seguridad/seguridad/save_user.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				dt_listusers.ajax.reload();

				$("#modal-dialog-form").modal('hide');

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});

			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
	}

	
	
	//********************************************************************************		
	function changeUserPasswordByAdmin() {

		$('#modal-dialog-form').modal('hide'); 
		
		var notice = new PNotify({
			text : $('#form_notice_change_password_by_admin').html(),
			icon : false,
			addclass : 'change-user-password-by-admin-notify',
			type : 'info',
			width : 'auto',
			hide : false,
			buttons : {
				closer : false,
				sticker : false
			},
			insert_brs : false
		});
		notice.get().find('form.pf-form').on('click', '[name=cancel]', function() {
			notice.remove();
		}).submit(function() {

			var data = $(".change-user-password-by-admin-notify #change_user_password_by_admin_form").serializeObject();

			$.ajax({
				type : "post",
				url : "<c:url value='/ajax/change_user_password_by_admin.do'/>",
				timeout : 100000,
				data : data,
				success : function(data) {
					notice.update({
						title : '<spring:message code="common.dialog.text.operacion_realizada" />',
						text : '<spring:message code="common.dialog.text.datos_guardados" />',
						icon : true,
						width : PNotify.prototype.options.width,
						hide : true,
						buttons : {
							closer : true,
							sticker : false
						},
						type : 'success'
					});
				},
				error : function(exception) {
					new PNotify({
						title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						text : exception.responseText,
						type : "error",
						delay : 5000,
						buttons : {
							closer : true,
							sticker : false
						}
					});
				}
			});

			return false;
		});
	}
</script>

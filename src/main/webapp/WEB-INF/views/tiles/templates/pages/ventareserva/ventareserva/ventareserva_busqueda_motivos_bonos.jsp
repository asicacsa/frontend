<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<x:parse xml="${ventareserva_motivos_bonos}" var="ventareserva_motivos_bonos_xml" />

<div class="modal fade" id="motivos_bonos"
	role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<input type="hidden" id="bonoMotivosAction" name="bonoMotivosAction"/>
			<div class="modal-header">
				<button type="button" class="close close_dialog" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">�</span>
				</button>
				<h4 class="modal-title">
					<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.motivo.seleccionar.activar.title" />					
				</h4>
			</div>
			<div class="modal-body">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.motivo.seleccionar.activar.text" />
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<select class="form-control" name="motivo" id="motivo">
						<option value=""></option>
						<x:forEach select="$ventareserva_motivos_bonos_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
				<br/><br/>
				<div class="modal-footer">
					<button type="button" id="aceptar_activar_dialog" class="btn aceptar_activar_dialog"
						data-dismiss="modal">
						<spring:message code="common.button.ok" />
					</button>
					<button type="button" class="btn btn-success close_dialog"
						data-dismiss="modal">
						<spring:message code="common.button.cancel" />
					</button>
				</div>

			</div>
			
		</div>
	</div>
</div>

<script>
//*****************************************************
$("#aceptar_activar_dialog").on("click", function(e) {
	
	var accion = $("#bonoMotivosAction").val();
	
	var data_select = sanitizeArray(dt_listbono.rows( { selected: true } ).data(),"idbono");
	
	var xml = "<list>";
		
	for(i=0;i<data_select.length;i++)
	{
	xml+="<Estadobono>"
	xml+= "<bono><idbono>"+data_select[i]+"</idbono></bono>";
	xml+="<motivo>"+$("#motivo").text().trim()+"</motivo>";
	xml+="<estadoaux><idestadoaux>"+accion+"</idestadoaux></estadoaux>";
	xml+="</Estadobono>"
	}
	xml +="</list>";
	
	
	$("#xmlBonos").val(xml);
	
	var url = "";
	
	var data = $("#form_busqueda_bono").serializeObject();
	
	
	if(accion=="2")
		url = "<c:url value='/ajax/ventareserva/busqueda/bono/activar.do'/>";
	else
		url=  "<c:url value='/ajax/ventareserva/busqueda/bono/anular.do'/>";
		
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : url,
		timeout : 100000,
		data: data, 
		success : function(data) {
			dt_listbono.ajax.reload();
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
})
</script>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!-- Pesta�as ------------------------------------------------------------->
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 maintab">
		<div class="" role="tabpanel" data-example-id="togglable-tabs">
			<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
				<li role="presentation" class="active"><a href="#tab_taquillas" id="taquillas-tab" role="tab" data-toggle="tab" aria-expanded="true"><spring:message code="administracion.recintos.tabs.taquillas.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_recintos" role="tab" id="recintos-tab" data-toggle="tab" aria-expanded="false"><spring:message code="administracion.recintos.tabs.recintos.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_unidades_negocio" role="tab" id="unidades_negocio-tab" data-toggle="tab" aria-expanded="false"><spring:message code="administracion.recintos.tabs.unidades_negocio.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_ubicaciones" role="tab" id="ubicaciones-tab" data-toggle="tab" aria-expanded="false"><spring:message code="administracion.recintos.tabs.ubicaciones.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_puntos_recogida" role="tab" id="puntos_recogida-tab" data-toggle="tab" aria-expanded="false"><spring:message code="administracion.recintos.tabs.puntos_recogida.title" /></a></li>				
			</ul>
		</div>
	</div>
</div>

<!-- Area de trabajo ------------------------------------------------------>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel thin_padding">

			<div id="myTabContent" class="tab-content">
				
				<div role="tabpanel" class="tab-pane fade active in" id="tab_taquillas" aria-labelledby="taquillas-tab">
					<tiles:insertAttribute name="tab_taquillas" />
				</div>
				
				<div role="tabpanel" class="tab-pane fade" id="tab_recintos" aria-labelledby="recintos-tab">
					<tiles:insertAttribute name="tab_recintos" />
				</div>	
					
				<div role="tabpanel" class="tab-pane fade in" id="tab_unidades_negocio" aria-labelledby="unidades_negocio-tab">
					<tiles:insertAttribute name="tab_unidades_negocio" />
				</div>
				
				<div role="tabpanel" class="tab-pane fade" id="tab_ubicaciones" aria-labelledby="ubicaciones-tab">
					<tiles:insertAttribute name="tab_ubicaciones" />
				</div>
				
				<div role="tabpanel" class="tab-pane fade" id="tab_puntos_recogida" aria-labelledby="puntos_recogida-tab">
					<tiles:insertAttribute name="tab_puntos_recogida" />
				</div>	
			</div>

			<div class="clearfix"></div>
			
		
		</div>
	</div>
</div>

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-2"/>
<tiles:insertAttribute name="modal_dialog" />



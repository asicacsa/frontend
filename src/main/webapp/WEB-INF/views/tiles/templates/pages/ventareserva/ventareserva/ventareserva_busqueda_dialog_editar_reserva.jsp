
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_reserva}" var="editar_reserva_xml" />
<x:parse xml="${listado_estados_reserva}" var="listado_estados_venta_xml" />
<x:parse xml="${listado_puntos_recogida}" var="listado_puntos_recogida_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.venta_resumen.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_reserva_editar_dialog" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input type="hidden" name="observaciones_reserva" id="observaciones_reserva" value="<x:out select = "$editar_reserva_xml/Reserva/observaciones" />">
		<input type="hidden" name="reserva_xml" id="reserva_xml">
		<input type="hidden" name="fecha_reserva" id="fecha_reserva" value="<x:out select = "$editar_reserva_xml/Reserva/fechayhorareserva" />">
		<input type="hidden" id="idsesion_reserva" name="idsesion_reserva">
		<input type="hidden" id="idlineadetalle_reserva" name="idlineadetalle_reserva">
		<input type="hidden" name="idreserva" id="idreserva" value="<x:out select = "$editar_reserva_xml/Reserva/idreserva" />">
		<input type="hidden" name="idcanal" id="idcanal" value="<x:out select = "$editar_reserva_xml/Reserva/canal/idcanal" />">
		
		<div id="columna-izquierda" class="col-md-5 col-sm-12 col-xs-12">
		
			 <div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.refreserva" /></label>
					<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$editar_reserva_xml/Reserva/idreserva" /></div>
				</div>
	
				<div class="col-md-12 col-sm-12 col-xs-12">
					<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.editar_reserva.field.fecha_reserva" /></label>
																												
					<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$editar_reserva_xml/Reserva/fechayhorareserva" /></div>
				</div>
	
				<div class="col-md-12 col-sm-12 col-xs-12">
					<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.usuario" /></label>
					<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$editar_reserva_xml/Reserva/usuario/nombre" />&nbsp;<x:out select = "$editar_reserva_xml/Reserva/usuario/apellidos" /></div>
				</div>
	
				<div class="col-md-12 col-sm-12 col-xs-12">
					<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.canal" /></label>
					<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$editar_reserva_xml/Reserva/canal/nombre" /></div>
				</div>
	
				<div class="col-md-12 col-sm-12 col-xs-12">
					<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.estado" /></label>
					<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$editar_reserva_xml/Reserva/estadooperacion/nombre" /></div>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.editar_reserva.field.loc_agencia" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" name="loc_agencia" id="loc_agencia" class="form-control" value="<x:out select = "$editar_reserva_xml/Reserva/localizadoragencia" />"   />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_individual.field.email" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" name="email" id="email" class="form-control" value="<x:out select = "$editar_reserva_xml/Reserva/email" />"   />
				</div>
			</div>
					
		</div> 

		 <div id="columna-central" class="col-md-5 col-sm-12 col-xs-12">
		
			<div class="callcenter">
			
				<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.cliente" /></label>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<input type="text" name="idcliente" id="idcliente" class="form-control" value="<x:out select = "$editar_reserva_xml/Reserva/cliente/idcliente" />" disabled="disabled"/>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<input type="text" name="nombrecliente" id="nombrecliente" class="form-control" value="<x:out select = "$editar_reserva_xml/Reserva/cliente/nombre" />"  disabled="disabled"/>
					</div>
				</div>
				</div>

				<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.nombre" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input type="text" name="nombre" id="nombre" class="form-control" value="<x:out select = "$editar_reserva_xml/Reserva/nombre" />" />
					</div>
				</div>
				</div>
				<div class="form-group button-dialog capa_cp_dialog_reserva_individual">
	
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="venta.ventareserva.dialog.venta_individual.field.nif" /></label>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<input type="text" name="nif" id="nif" class="form-control" value="<x:out select = "$editar_reserva_xml/Reserva/nif" />"  disabled="disabled" />
						</div>
					</div>
				
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.cp" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<input id="cp_callcenter" type="text" class="form-control" value="<x:out select = "$editar_reserva_xml/Reserva/cp" />"  disabled="disabled" />
						</div>
					</div>
					
				</div>		
				
				<div class="form-group">	
				<div class="col-md-12 col-sm-12 col-xs-12">					
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.editar_reserva.field.persona_contacto" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<input type="text" name="persona_contacto" id="persona_contacto" class="form-control" value="<x:out select = "$editar_reserva_xml/Reserva/personacontacto" />"  />
						</div>		
						</div>			
				</div>		
				<div class="form-group button-dialog capa_cp_dialog_reserva_individual">
	
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.editar_reserva.field.edad" /></label>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<input type="text" name="edad" id="edad" class="form-control" value="<x:out select = "$editar_reserva_xml/Reserva/edadgrupo" />"   />
						</div>
					</div>
				
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.editar_reserva.field.curso" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<input id="curso" name="curso" type="text" class="form-control" value="<x:out select = "$editar_reserva_xml/Reserva/cursoescolar" />"  />
						</div>
					</div>
					
				</div>		
				
				<div class="form-group">
	
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="venta.ventareserva.dialog.venta_individual.field.telefono" /></label>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<input type="text" name="telefono" id="telefono" class="form-control" value="<x:out select = "$editar_reserva_xml/Reserva/telefono" />"  />
						</div>
					</div>
				
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.movil" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<input name="movil" id="movil" type="text" class="form-control" value="<x:out select = "$editar_reserva_xml/Reserva/telefonomovil" />"  />
							
						</div>
					</div>
					
				</div>		
				
				
				<div class="form-group">	
					<div class="col-md-12 col-sm-12 col-xs-12">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.editar_reserva.field.punto_recogida" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">							
							<select name="puntos_recogida" id="puntos_recogida" class="form-control">
								<option value=""></option>
								<x:forEach select="$listado_puntos_recogida_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
							</select>
						
						</div>
					</div>
					
					</div>
				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.editar_reserva.field.comp_grupo" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<input type="text" name="composicion_grupo" id="composicion_grupo" class="form-control" value="<x:out select = "$editar_reserva_xml/Reserva/composiciongrupo" />"  />
						</div>
					</div>
					
				</div>		
				
					
			</div>
					
		</div>
		
		<div id="columna-derecha" class="col-md-2 col-sm-12 col-xs-12">
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<span class="fa" id="check-facturada"/>&nbsp;<spring:message code="venta.ventareserva.dialog.venta_resumen.field.facturada" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<span class="fa" id="check-entradas"/>&nbsp;<spring:message code="venta.ventareserva.dialog.venta_resumen.field.entradas" />
				</div>
			</div>
		</div> 
		
		<div class="col-md-12 col-sm-12 col-xs-12">
		
			<div class="totales-group totales_reserva_individual_resumen">
				Total: <span class="total-lbl"><span id="total-val">0.00</span>&euro;</span>&nbsp;<span class="descuento-lbl">Descuento: <span id="total-desc">0.00</span>&euro;&nbsp;(<span id="total-prc">0.00</span>%)</span>
			</div>
		
		<div class="btn-group pull-right btn-datatable">
		 	<a type="button" class="btn btn-info" id="tab_busqueda_informe_resumen_reserva">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.dialog.editar_venta.list.button.resumen_venta" />"> <span class="fa fa-file"></span>
				</span>
			</a>	
			<a type="button" class="btn btn-info" id="tab_venta_reserva_confirmar_reserva">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.button.reserva" />"> <span class="fa fa-check-square-o"></span>
			</span>
			</a>
			<a type="button" class="btn btn-info" id="tab_busqueda_historico_cambios_reserva">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.dialog.editar_venta.list.button.historico" />"> <span class="fa fa-mail-reply"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="tab_busqueda_observaciones_reserva">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.dialog.editar_venta.list.button.observaciones" />"> <span class="fa fa-bookmark"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="tab_busqueda_ver_formas_pago_reserva">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.dialog.editar_venta.list.button.formas_pago" />"> <span class="fa fa-euro"></span>
				</span>
			</a>		
			<a type="button" class="btn btn-info" id="tab_busqueda_carta_confirmacion_reserva">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.dialog.editar_venta.list.button.carta" />"> <span class="fa fa-envelope"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="tab_busqueda_usos_impresiones_reserva">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.dialog.editar_venta.list.button.usos" />"> <span class="fa fa-clone"></span>
				</span>
			</a>	
			<a type="button" class="btn btn-info" id="tab_busqueda_modificar_venta_reserva">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.ventareserva.dialog.editar_reserva.list.button.modificar_venta" />"> <span class="fa fa-align-left"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="tab_busqueda_modificar_reserva">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.ventareserva.dialog.editar_reserva.list.button.modificar_reserva" />"> <span class="fa fa-plus-circle"></span>
				</span>
			</a>		
			
			<a type="button" class="btn btn-info" id="button_reserva_resumen_entradas">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.dialog.venta_resumen.button.entradas" />"> <span class="fa fa-ticket"></span>
					</span>
				</a>		
		</div>
		<!-- Pestañas ------------------------------------------------------------->
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 maintab">
				<div class="" role="tabpanel" data-example-id="togglable-tabs">
					<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#tab_lineas_detalle_reserva" id="lineas_detalle_reserva-tab" role="tab" data-toggle="tab" aria-expanded="true">
							<spring:message	code="venta.ventareserva.tabs.busquedas.venta_reserva.dialog.editar_reserva.tabs.venta_reserva.title" /></a></li>
						<li role="presentation" class="">
							<a href="#tab_lineas_detalle_anuladas" role="tab" id="lineas_detalle_anuladas-tab" data-toggle="tab" aria-expanded="false">
							<spring:message	code="venta.ventareserva.tabs.busquedas.venta_reserva.dialog.editar_reserva.tabs.anuladas.title" /></a></li>			
					</ul>
				</div>
			</div>
		</div>
		
			<!-- Area de trabajo ------------------------------------------------------>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel thin_padding">
					<div id="myTabContent" class="tab-content">				
						<div role="tabpanel" class="tab-pane fade active in" name="tab_lineas_detalle_reserva" id="tab_lineas_detalle_reserva" aria-labelledby="lineas_detalle_reserva-tab">
							<table id="datatable-ventareserva" class="table table-striped table-bordered dt-responsive ventareserva" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_individual_resumen.list.header.venta" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.producto" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.fecha" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.contenido" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.etd" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.perfil" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.descuentos" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.importeunitario" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.importe" /></th>
						<th><spring:message code="venta.ventareserva.tabs.venta_resumen.list.header.impreso" /></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="tab_lineas_detalle_anuladas" name="tab_lineas_detalle_anuladas" aria-labelledby="lineas_detalle_anuladas-tab">
							<table id="datatable-anuladas" class="table table-striped table-bordered dt-responsive ventareserva" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_individual_resumen.list.header.venta" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.producto" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.fecha" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.contenido" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.etd" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.perfil" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.descuentos" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.importeunitario" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.importe" /></th>
						<th><spring:message code="venta.ventareserva.tabs.venta_resumen.list.header.impreso" /></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>		

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
			<button id="horas_presentacion_button" type="button" class="btn btn-primary horas_presentacion">
					<spring:message code="venta.ventareserva.tabs.ventareserva.busqueda.list.button.ver_horas" />
				</button>
				<button id="save_reserva_button" type="submit" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
		
	</form>

</div>

<script>

hideSpinner("#tab_venta_reserva_editar");

showSpinner("#modal-dialog-form .modal-content");

//************************************************
var reserva_json=${editar_reserva_json};
//************************************************
function ocultar_spinner()
{
	hideSpinner("#modal-dialog-form .modal-content");
}
//************************************************
/*Si la reserva no está confirmada (vendida) no se puede modificar la venta*/ 
 var venta=""+reserva_json.Reserva.ventas.Venta; 
 if (venta=="undefined")
	$("#tab_busqueda_modificar_venta_reserva").prop("disabled", true);
 else
	$("#tab_busqueda_modificar_venta_reserva").prop("disabled", false);


//************************************************
setTimeout(ocultar_spinner, 3000);
//************************************************
var reserva_json=${editar_reserva_json};
//********************************************************
$('#puntos_recogida option[value="<x:out select = "$editar_reserva_xml/Reserva/puntosrecogida/idpuntosrecogida" />"]').attr("selected", "selected");
//*********************************************************

var fecha_sesion="",
	contenido= "",
	facturada= Number("<x:out select = "$editar_reserva_xml/Reserva/ventas/Venta/facturada" />"),
	entradasimpresas= Number("<x:out select = "$editar_reserva_xml/Reserva/numeroentradasimpresas" />");
	
	if (entradasimpresas>0)
		entradasimpresas=1;
	else
		entradasimpresas=0;
		
	
$("#check-facturada").addClass(facturada>0?'fa-check':'fa-close');
$("#check-facturada").css("color",facturada>0?'green':'red');
$("#check-entradas").addClass(entradasimpresas>0?'fa-check':'fa-close');
$("#check-entradas").css("color",entradasimpresas>0?'green':'red');

//*****************************************************************************************

var dt_reserva=$('#datatable-ventareserva').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	  initComplete: function( settings, json ) {
	    	 window.setTimeout(CargarLineasDetalle, 100);  
	    	// window.setTimeout(cargarBonotonesInicio, 1000);
	    	 
	 	  },
	scrollCollapse: true,

	paging: false,	
    select: { style: 'single' },
    columns: [
              {},              
              {},
              {},
              {},
              {},
              {},
              {},
              {},
              {},
              {},
              {}
        ],
        "columnDefs": [
                       { "visible": false, "targets": [0]}
                     ]
    
    
} );
//****************************************************************
function CargarLineasDetalle()
	{
	    var json = ${editar_reserva_json};
		var i = 0;
		
		var lineasdetalle = json.Reserva.lineadetalles;
		
		if (lineasdetalle !=""){
			if (lineasdetalle.Lineadetalle!="") {
		        var item=lineasdetalle.Lineadetalle;
		        if (item.length>0)
		            $.each(item, function(key, lineadetalle){
		            	CargarLineaDetalle(lineadetalle,dt_reserva);
		            });
		        else
		        	CargarLineaDetalle(item,dt_reserva);
			}
		}
	
	}
		

	//*****************************************************************************************
	function CargarLineaDetalle(lineadetalle, tabla)
	{	
			
		var fechas= "", contenidos= "", disponibles= "",retorno= false;
		
		var sesiones = lineadetalle.lineadetallezonasesions.Lineadetallezonasesion;
		
		var descuento_nombre=""+lineadetalle.descuentopromocional.nombre;
		var venta="";
		if (lineadetalle.venta.idventa!=undefined)
			//venta=reserva_json.Reserva.ventas.Venta.idventa;
			venta=""+lineadetalle.venta.idventa;
		
		
		var producto=lineadetalle.producto.nombre;
		var num_ent=lineadetalle.cantidad;
		var perfil=lineadetalle.perfilvisitante.nombre;
		var importe_unitario= lineadetalle.tarifaproducto.importe;
		var importe=lineadetalle.importe;
		var impreso = lineadetalle.entradasimpresas+"/"+lineadetalle.entradasnoanuladas;
		
		if(descuento_nombre=="undefined")
			descuento_nombre =" ";
		
		var contenidos;
		
		 if (sesiones.length>0)		 
		 {
				for (var i=0; i<sesiones.length; i++) {
					if (retorno) {
						fechas+="</br>";
						contenidos+= "</br>";						
					}				
					var f= sesiones[i].zonasesion.sesion.fecha.split("-")[0];
					fechas+= f;					
					var hi=sesiones[i].zonasesion.sesion.horainicio;
					var  no=sesiones[i].zonasesion.sesion.contenido.nombre;
					contenidos+= hi+" "+no;					
					retorno= true;
				}	 
				
		 }
		 else
		 {
			fechas= lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.fecha.split("-")[0];
			contenidos= lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.horainicio+" "+lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.contenido.nombre;
				
		 } 
		 tabla.row.add([
					lineadetalle,
					venta, 
					producto,
					fechas,         
				    contenidos, 
				    num_ent, 
				    perfil, 
				    descuento_nombre, 
				    importe_unitario,
				    importe,
				    impreso 
				]).draw();
}


//*********************************************
var dt_listanuladas=$('#datatable-anuladas').DataTable( {
		language: dataTableLanguage,
		info: false,
		searching: false,
		  initComplete: function( settings, json ) {
		    	 window.setTimeout(CargarLineasDetalleAnuladas, 200);    	 
		 	  },
		scrollCollapse: true,
		ordering:  false,
		paging: false,	
	    select: { style: 'single' },
	    columns: [	              
	              {},	             
	              {},
	              {},
	              {},
	              {},
	              {},
	              {},
	              {},
	              {},
	              {},
	              {}
	        ],
	        "columnDefs": [
	                       { "visible": false, "targets": [0]}
	                     ]
	   
	    
	} ); 
//*********************************
function CargarLineasDetalleAnuladas()
	{
		var json = ${editar_reserva_json};
		var i = 0;
		
		
		var lineasdetalle = json.Reserva.lineadetallesanuladas;
		
		if (lineasdetalle !=""){
		if (lineasdetalle.Lineadetalle!="") {
	        var item=lineasdetalle.Lineadetalle;
	        if (item.length>0)
	            $.each(item, function(key, lineadetalle){
	            	CargarLineaDetalle(lineadetalle,dt_listanuladas);
	            });
	        else
	        	CargarLineaDetalle(item,dt_listanuladas);
	}
		}
	}
//***************************************************
obtener_totales_venta();
//********************************************************************************
function obtener_totales_venta() {
	var porcentaje = "<x:out select = "$editar_reserva_xml/Reserva/porcentajesDescuentoLD/porcentajedescuento" />";
	var total =  "<x:out select = "$editar_reserva_xml/Reserva/importetotalreserva" />";
	var por=parseFloat(porcentaje);
	var tot=parseFloat(total);
	var i = (por/100);
	var descuento= ((tot /(1-i))*i).toFixed(2);
	
	$("#total-val").text(tot);
	
	if (por>0){
		$("#total-desc").text(descuento);
		$("#total-prc").text(por);
	}	
}


//*******************************************************************
$("#tab_busqueda_informe_resumen_reserva").on("click", function(e) {
	window.open("../../jasper.post?idReserva=<x:out select = "$editar_reserva_xml/Reserva/idreserva" />&report=Resumen_Reserva","_blank");		 
});
//****************************************************************
$("#tab_busqueda_historico_cambios_reserva").on("click", function(e) {
	showButtonSpinner("#tab_busqueda_historico_cambios_reserva");
	var id="<x:out select = "$editar_reserva_xml/Reserva/idreserva" />";
	

	$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/ventareserva/busqueda/show_historico_cambios_reserva.do'/>?id="+id, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "lg");
	});
	
});

//****************************************************************
$("#tab_busqueda_observaciones_reserva").on("click", function(e) {
	showButtonSpinner("#tab_busqueda_observaciones_reserva");

	var id="<x:out select = "$editar_reserva_xml/Reserva/idreserva" />";
	var observaciones=$("#observaciones_reserva").val();

	$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/ventareserva/busqueda/show_observaciones_reserva.do'/>?id="+id+"&observaciones=", function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "sm");
	});	
});

//****************************************************************


$("#tab_busqueda_ver_formas_pago_reserva").on("click", function(e) {
	showButtonSpinner("#tab_busqueda_ver_formas_pago_reserva");
	
	var ventas=""+reserva_json.Reserva.ventas;
	
		
	
	if (ventas!="")
	{
		
		var idreserva="<x:out select = "$editar_reserva_xml/Reserva/idreserva" />";
		var idVenta = reserva_json.Reserva.ventas.Venta.idventa;
		var idcanal="${sessionScope.idcanal}";
		
		
		
		$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/ventareserva/busqueda/show_formas_de_pago_reserva.do'/>?idReserva="+idreserva+"&idVenta="+idVenta+"&idCanal="+idcanal, function() {			
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "sm");
		});
		
		
	}else{			
		
		
		hideSpinner("#tab_busqueda_ver_formas_pago_reserva");
		new PNotify({
		     title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="venta.ventareserva.reserva.dialog.formas_pago_reserva.alert.sin_confirmar" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
	  		  
		   })
		
		
		
	}
});

//****************************************************************
$("#tab_busqueda_carta_confirmacion_reserva").on("click", function(e) {
	showButtonSpinner("#tab_busqueda_carta_confirmacion_reserva");

	var id="<x:out select = "$editar_reserva_xml/Reserva/idreserva" />";
	
	$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/ventareserva/busqueda/show_carta_confirmacion_reserva.do'/>?idReserva="+id, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "xs");
	});	
});

//****************************************************************
$("#tab_busqueda_usos_impresiones_reserva").on("click", function(e) {
	showButtonSpinner("#tab_busqueda_usos_impresiones_reserva");

	var id="<x:out select = "$editar_reserva_xml/Reserva/idreserva" />";
	
	$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/ventareserva/busqueda/show_usos_impresiones_reserva.do'/>?idReserva="+id, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "lg");
	});		
});
//*****************************************************************

$("#tab_busqueda_modificar_venta_reserva").on("click", function(e) {
	
	var ventas=""+reserva_json.Reserva.ventas;
	
		
	
	if (ventas!="")
	{	
	if(facturada==1)
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.dialog.venta_resumen.message.error.ya.facturada" />',
			  type: "warning",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
	else
		{	
	
		var id="<x:out select = "$editar_reserva_xml/Reserva/ventas/venta/idventa" />";
	
		$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/ventareserva/busqueda/modificar_venta.do'/>?idVenta="+id, function() {
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "lg");
		});	
		}
	}
	else
		{
		hideSpinner("#tab_busqueda_modificar_venta_reserva");
		new PNotify({
		     title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="venta.ventareserva.reserva.dialog.editar_lineas_venta_reserva.alert.sin_confirmar" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
	  		  
		   })
		}
});


//**********************************************


$("#tab_busqueda_modificar_reserva").on("click", function(e) {
showButtonSpinner("#tab_busqueda_modificar_lineas_detalle");
	
	if(facturada==1)
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.dialog.venta_resumen.message.error.ya.facturada" />',
			  type: "warning",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
	else
		{	
	
	
		//$("#idsesion_reserva").val(dt_reserva.rows(0).data()[0][0].lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.idsesion);
		$("#idlineadetalle_reserva").val(dt_reserva.rows(0).data()[0][0].idlineadetalle);
		
		$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/ventareserva/busqueda/reserva/showLineas.do'/>", function() {
		
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "lg");
		});		
		}
	
});



//********************************************************
$("#form_reserva_editar_dialog").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			grabarReserva();
		}
	}
	);
//********************************************************
function grabarReserva() { 	
	
	reserva_json.Reserva.localizadoragencia=$("#loc_agencia").val();
	reserva_json.Reserva.email=$("#email").val();
	reserva_json.Reserva.nombre=$("#nombre").val();
	reserva_json.Reserva.personacontacto=$("#persona_contacto").val();
	reserva_json.Reserva.edadgrupo=$("#edad").val();
	reserva_json.Reserva.cursoescolar=$("#curso").val();
	reserva_json.Reserva.telefono=$("#telefono").val();
	reserva_json.Reserva.telefonomovil=$("#movil").val();
	reserva_json.Reserva.observaciones=$("#observaciones_reserva").val();
	reserva_json.Reserva.composiciongrupo=$("#composicion_grupo").val();
	reserva_json.Reserva.puntosrecogida.idpuntosrecogida=$("#puntos_recogida").val();
	reserva_json.Reserva.puntosrecogida.nombre=$("#puntos_recogida option:selected").text();
	
	var temp_reserva_xml = json2xml(reserva_json);
	temp_reserva_xml =temp_reserva_xml.replace("<Reserva>", "<reserva>");
	temp_reserva_xml =temp_reserva_xml.replace("</Reserva>", "</reserva>");
	
	$("#reserva_xml").val(temp_reserva_xml);
	
	
	  var data = $("#form_reserva_editar_dialog").serializeObject();	 
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/busqueda/save_reserva.do'/>",
		timeout : 100000,
		data: data, 
		success : function(data) {	
			$("#modal-dialog-form").modal('hide');
			dt_listbusqueda.ajax.reload(null,false);
			
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
			
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});  
	
	
}
//*********************************************************
$("#horas_presentacion_button").on("click", function(e) {
	showButtonSpinner("#horas_presentacion_button");	

	var lineasdetalle= dt_reserva.rows().data();
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/busqueda/show_hora_presentacion.do'/>",
		timeout : 100000,
		data: {
			button: "horas_presentacion_button",
			list: "dt_reserva",
			xml: construir_xml_lineasdetalle(lineasdetalle)
		},
		success : function(data) {
			$("#modal-dialog-form-2 .modal-content:first").html(data);
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "sm");
		},
		error : function(exception) {
			hideSpinner("#horas_presentacion_button");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});
});

//*********************************
function construir_xml_lineasdetalle(lineas_detalle_sel) {
	
	var xml_lineasdetalle= "<lineadetalles>";

	for (var i=0; i<lineas_detalle_sel.length; i++) { 
		xml_lineasdetalle+="<Lineadetalle>";
		var lineaLoop = lineas_detalle_sel[i][0];
		var strLineaNueva = ""+lineaLoop.Lineadetalle;
		if(strLineaNueva!='undefined')
			lineaLoop = lineaLoop.Lineadetalle;
		xml_lineasdetalle+=json2xml(lineaLoop,"");
		xml_lineasdetalle+="</Lineadetalle>";
	}
	xml_lineasdetalle+="</lineadetalles>";
	
	
	return(xml_lineasdetalle);
}



//******************************************************************************
$("#button_reserva_resumen_entradas").on("click", function(e) { 
	showButtonSpinner("#button_reserva_resumen_entradas");
	 $("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/venta/ventareserva/imprimir_entradas_reserva.do?idreserva="+$("#idreserva").val()+"'/>", function() {										  
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "md");
		});
});

var Venta = ""+ reserva_json.Reserva.ventas;
var idReserva = $("#idreserva").val();



if(Venta!="")
{
	$("#tab_venta_reserva_confirmar_reserva").attr("disabled",true);
}
var confirmar_desde_dentro=false;

//******************************************BOTÓN CONFIRMAR RESERVAR****************************
$("#tab_venta_reserva_confirmar_reserva").on("click", function(e) {
		confirmar_desde_dentro = true;
		showButtonSpinner("#tab_venta_reserva_confirmar_reserva");
		
		$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_vender_reserva.do'/>?idcanal=${sessionScope.idcanal}&idreserva="+idReserva, function() {
			hideSpinner("#tab_venta_reserva_confirmar_reserva");
			$("#modal-dialog-form-3").modal('show');
			setModalDialogSize("#modal-dialog-form-3", "md");
		});
	
	
		
	
})

</script>
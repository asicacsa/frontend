<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<x:parse xml="${asignarTarifas_productos_tarifas}" var="asignarTarifas_productos_tarifas_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="facturacion.facturas.tabs.clientes.dialog.crearTarifa.title" />
	</h4>	
</div>
	<div class="modal-body">
	<form id="form_cliente_crear_tarifas" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.clientes.asignar_tarifa.field.tarifa" />*</label>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<select name="idtarifa" id="idtarifa"  class="form-control" required="required">
							<option> </option>
							<x:forEach select="$asignarTarifas_productos_tarifas_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
						</select>
					</div>					
				</div>
				<div class="form-group date-picker">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.productos.field.tarifas.fecha" />*</label>
					<div class="col-md-6 col-sm-6 col-xs-6">
						  <a type="button" class="btn btn-default btn-clear-date" id="button_apertura_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="ventareserva.operacionescaja.operacionescaja.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
						  </a>			
	                       <div class="input-prepend input-group">
	                         <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	                         <input type="text" name="fecha_tarifa" id="fecha_tarifa" class="form-control" value="" readonly/>
	                         <input type="hidden" required="required" id="fechainiciotarifa" name="fechainiciotarifa" />
	                         <input type="hidden" id="fechafintarifa" name="fechafintarifa" />
	                       </div>
					</div>
				</div>	
				
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.productos.field.tarifas.importe" />*</label>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<input type="text" name="importe" id="importe" class="form-control" required="required"/>								
					</div>
					<div class="col-md-5 col-sm-5 col-xs-5"></div>				
				</div>	
				

	</form>
	<div class="modal-footer">
	
		<button id="save_cliente_button" type="button" class="btn btn-primary close_crear_tarifas">
			<spring:message code="common.button.accept" />
		</button>	
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>	
	
</div>

<script>
$today= moment().format("DD/MM/YYYY");
//Ahora venta
dia_inicio = $today;
dia_fin = $today;

	 $('input[name="fechainiciotarifa"]').val(dia_inicio);
	 $('input[name="fechafintarifa"]').val(dia_fin);

$('input[name="fecha_tarifa"]').val( dia_inicio + ' - ' + dia_fin);

$('input[name="fecha_tarifa"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
  	  	 $('input[name="fecha_tarifa"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('input[name="fechainiciotarifa"]').val(start.format('DD/MM/YYYY'));
  	  	 $('input[name="fechafintarifa"]').val(end.format('DD/MM/YYYY'));
	});
	
$(".close_crear_tarifas").on("click", function(e) { 
	  var xml = "<parametro><list>"+tablatarifasXML();
	  xml+="<Tarifaproducto>";
	  xml+="<tarifa><nombre>Prueba</nombre><idtarifa>"+$('#idtarifa').val()+"</idtarifa></tarifa>";
	  xml+="<importe>"+$('#importe').val()+"</importe>";
	  xml+="<fechainiciovigencia>"+$('#fechainiciotarifa').val()+"</fechainiciovigencia><fechafinvigencia>"+$('#fechafintarifa').val()+"</fechafinvigencia>";
	  xml+="</Tarifaproducto></list>";
	  xml+="<idcliente>"+$('#idcliente').val()+"</idcliente><idproducto>"+$('#idproducto').val()+"</idproducto></parametro>";
	  
	  $("#modal-dialog-form-2").modal('hide');
	  $("#xmlEnvio").val(xml) ;
	  acturlizarTarifasBd()
	
})
</script>


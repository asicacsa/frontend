<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2><spring:message code="common.text.filter_list" /></h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">

		<form id="form_filter_list_clientes" class="form-horizontal form-label-left">

			<div class="col-md-5 col-sm-5 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.clientes.field.codigocliente" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<input name="codigo" type="text" class="form-control">
					</div>
				</div>
			
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.clientes.field.nif" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<input name="dni" type="text" class="form-control">
					</div>
				</div>

			</div>


			<div class="col-md-7 col-sm-7 col-xs-12">

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.clientes.field.nombre" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<input name="nombre_razon" type="text" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.clientes.field.apellido1" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<input name="apellido1" type="text" class="form-control">
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.clientes.field.apellido2" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<input name="apellido2" type="text" class="form-control">
					</div>
				</div>


			</div>
				

			<div class="clearfix"></div>
			<div class="ln_solid"></div>
			<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
				<button id="button_clean_venta_clientes_numerada" type="button" class="btn btn-success pull-right">
					<spring:message code="venta.ventareserva.tabs.canje-bono.button.clean" />
				</button>
				<button id="button_filter_list_clientes" type="button" class="btn btn-success pull-right">
					<spring:message code="common.button.filter" />
				</button>
			</div>

			<input type="hidden" name="start" value=""/>
			<input type="hidden" name="length" value=""/>

		</form>


	</div>
</div>

<!--  -->


<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_clientes_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada_busquedas.tabs.clientes.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
	</div>

	<table id="datatable-list-clientes" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="venta.numerada_busquedas.tabs.clientes.list.header.nombre" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.clientes.list.header.primer_apellido" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.clientes.list.header.segundo_apellido" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.clientes.list.header.nif" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.clientes.list.header.telefono" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.clientes.list.header.movil" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script>


$("#button_filter_list_clientes").on("click", function(e) { 
	
	if (!isFormEmpty("#form_filter_list_clientes")) {
		dt_listclientes.ajax.reload();
	}
	else {
		new PNotify({
			  title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
		      text: '<spring:message code="common.dialog.text.empty_filter" />',
			  type: "error",
			  buttons: { sticker: false }				  
		   });
	}
	
});


var dt_listclientes=$('#datatable-list-clientes').DataTable( {
	serverSide: true,
	searching: false,
	ordering: false,
	deferLoading: 0,
	pagingType: "simple",
	info: false,
    ajax: {
        url: "<c:url value='/ajax/list_clientes.do'/>",
        rowId: 'idcliente',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Cliente)); return(""); },
        data: function (params) {
        	$('#form_filter_list_clientes input[name="start"]').val(params.start);
        	$('#form_filter_list_clientes input[name="length"]').val(params.length);
        	return ($("#form_filter_list_clientes").serializeObject());
       	}
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listclientes.data().count()>0) dt_listclientes.columns.adjust().draw(); });
	},
    columns: [
              { data: "nombre", type: "spanish-string" ,  defaultContent:""} ,			
              { data: "apellido1", type: "spanish-string" ,  defaultContent:""} ,			
              { data: "apellido2", type: "spanish-string" ,  defaultContent:""} ,			
              { data: "identificador", type: "spanish-string" ,  defaultContent:""} ,
              { data: "direccionorigen.telefonofijo" , type: "spanish-string" ,  defaultContent:""} ,	              
              { data: "direccionorigen.telefonomovil" , type: "spanish-string" ,  defaultContent:""} ,	                     
    ],    
    select: { style: 'single'},
	language: dataTableLanguage,
	processing: true
} );


insertSmallSpinner("#datatable-list-clientes_processing");


	$("#tab_clientes_edit").on("click", function(e) { 
 		
	 	if(dt_listclientes==null)
	 		{
	 		new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="facturacion.facturas.tabs.clientes.list.alert.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;	
	 		}
	 	
		var data = sanitizeArray(dt_listclientes.rows( { selected: true } ).data(),"idcliente");
			
		if (data.length>1) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="facturacion.facturas.tabs.clientes.list.alert.seleccion_multiple" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}

		if (data.length<=0) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="facturacion.facturas.tabs.clientes.list.alert.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}	
		
		showButtonSpinner("#tab_clientes_edit");

		$("#modal-dialog-form .modal-content:first").load("<c:url value='/ajax/venta/numerada/show_cliente.do'/>?id="+data[0], function() {
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "lg");
		});
	})
	/***********************************************BOT�N LIMPIAR  *********************************/
$("#button_clean_venta_clientes_numerada").on("click", function(e) {
	 
	$('input[name="codigo"]').val('');
	$('input[name="dni"]').val('');
	$('input[name="nombre_razon"]').val('');
	$('input[name="apellido1"]').val('');
	$('input[name="apellido2"]').val('');
})

</script>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


   <div class="col-md-12 col-sm-12 col-xs-12">
   <div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_tarifas_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.tarifas.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_tarifas_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.tarifas.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_tarifas_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.tarifas.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>		
	</div>
   
	<table id="datatable-lista-tarifas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="administracion.productos.tabs.tarifas.list.header.orden" /></th>
				<th><spring:message code="administracion.productos.tabs.tarifas.list.header.nombre" /></th>
				<th><spring:message code="administracion.productos.tabs.tarifas.list.header.descripcion" /></th>
				<th><spring:message code="administracion.productos.tabs.tarifas.list.header.perfiles" /></th>	
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>


<script>
var dt_listtarifas=$('#datatable-lista-tarifas').DataTable( {	
    ajax: {
        url: "<c:url value='/ajax/admon/productos/tarifas/list_tarifas.do'/>",
        rowId: 'idtarifa',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Tarifa)); return(""); 
        },
    error: function (xhr, error, thrown) {
         if (xhr.responseText=="403") {
               $("#tarifas-tab").hide();
         }      
         else
	      	 {
	      	 	$("#datatable-lista-tarifas_processing").hide();
	      		new PNotify({
						title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						text : xhr.responseText,
						type : "error",
						delay : 5000,
						buttons : {
							closer : true,
							sticker : false
						}					
					});
	      	 }
  },	
       
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listtarifas.data().count()>0) dt_listtarifas.columns.adjust().draw(); });
	},
    columns: [
              { data: "orden", type: "spanish-string" ,  defaultContent:""} ,
              { data: "nombre", type: "spanish-string" ,  defaultContent:""} ,
              { data: "descripcion", type: "spanish-string" ,  defaultContent:""},
              { data: "perfilvisitantes.Perfilvisitante", className: "cell_centered",
             	  render: function ( data, type, row, meta ) { return showListInCell(data,"<spring:message code="administracion.productos.tabs.tarifas.list.text.perfiles" />","nombre"); }	
                   },           
              
    ],    
    drawCallback: function( settings ) {
    	activateTooltipsInTable('datatable-lista-tarifas')
    },
    select: { style: 'os', selector:'td:not(:last-child)'},
	language: dataTableLanguage,
	processing: true,
} );

insertSmallSpinner("#datatable-lista-tarifas_processing");

/***********************************************BOT�N NUEVO**************************************/
  $("#tab_tarifas_new").on("click", function(e) { 
		showButtonSpinner("#tab_tarifas_new");
		$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/productos/tarifas/show_tarifa.do'/>", function() {										  
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "md");
		});
	})
 /***********************************************BOT�N EDITAR*************************************/
$("#tab_tarifas_edit").on("click", function(e) {
	var data = sanitizeArray(dt_listtarifas.rows( { selected: true } ).data(),"idtarifa");
		
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.productos.tabs.tarifas.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.productos.tabs.tarifas.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
		
	showButtonSpinner("#tab_tarifas_edit");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/productos/tarifas/show_tarifa.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
})
/***********************************************BOT�N ELIMIMAR***********************************/
$("#tab_tarifas_remove").on("click", function(e) { 
		
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="administracion.productos.tabs.tarifas.list.confirm.eliminar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
	  		  buttons: { closer: false, sticker: false	},
	  		  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
		
			   dt_listtarifas.processing(true);
				
				var data = sanitizeArray(dt_listtarifas.rows( { selected: true } ).data(),"idtarifa");
			   
				$.ajax({
					contenttype: "application/json; charset=utf-8",
					type : "post",
					url : "<c:url value='/ajax/admon/productos/tarifas/remove_tarifas.do'/>",
					timeout : 100000,
					data: { data: data.toString() }, 
					success : function(data) {
						dt_listtarifas.ajax.reload();					
					},
					error : function(exception) {
						dt_listtarifas.processing(false);
						
						new PNotify({
						      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						      text: exception.responseText,
							  type: "error",		     
							  delay: 5000,
							  buttons: { sticker: false }
						   });			
					}
				});

		   }).on('pnotify.cancel', function() {
		   });		 

	});
</script>


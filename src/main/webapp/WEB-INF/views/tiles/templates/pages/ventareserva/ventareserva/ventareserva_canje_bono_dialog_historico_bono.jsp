<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${historico_bono}" var="historico_bono_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.tabs.canje-bono.dialog.historico-bono.title" />: ${idbono}
	</h4>
</div>
<div class="modal-body">
	<table id="historico_bono_table" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
			  <th id="fechaBono"><spring:message code="venta.ventareserva.tabs.canje-bono.dialog.historico-bono.field.fecha"/></th>
			  <th id="loginBono"><spring:message code="venta.ventareserva.tabs.canje-bono.dialog.historico-bono.field.usuario"/></th>
			  <th id="estadoBono"><spring:message code="venta.ventareserva.tabs.canje-bono.dialog.historico-bono.field.estado" /></th>
			  <th id="motivo"><spring:message code="venta.ventareserva.tabs.canje-bono.dialog.historico-bono.field.motivo" /></th>							  						 
			</tr>
		</thead>
		<tbody>				
			<x:forEach select="$historico_bono_xml/ArrayList/Estadobono" var="item">
		   		<tr class="seleccionable">
					<td id="fechaBono">
						<c:set var="fecha"><x:out select="$item/fecha"/></c:set>
						<fmt:parseDate value="${fecha}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateada"/>
						<fmt:formatDate value="${fechaformateada}" pattern="dd/MM/yyyy" type="DATE"/>									
					</td>						
					<td id="loginBono" ><x:out select="$item/usuario/login"/></td>
					<td id="estadoBono" ><x:out select="$item/estadoaux/nombre" /></td> 
					<td id="motivoBono"><x:out select="$item/motivo" /></td>																	
				</tr>																
			</x:forEach>
		</tbody>				
	</table>			

	<div class="modal-footer">
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>		

</div>
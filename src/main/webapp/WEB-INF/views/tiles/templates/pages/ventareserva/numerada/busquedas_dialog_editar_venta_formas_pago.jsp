<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${selector_motivos}" var="motivosModificacion_xml" />
<x:parse xml="${selector_formas_pago}" var="selectorFormasdePagoPorCanal_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="venta.numerada.tabs.venta_edit.editar.formas.title" />				
	</h4>	
</div>

<div class="modal-body">
	<form id="form_formas_pago_guardar" data-parsley-validate="" class="form-horizontal form-label-left" >
	<input type="hidden" id="xmlPagos" name="xmlPagos" />
	<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">	
		<div class="col-md-1 col-sm-1 col-xs-12">
			<a type="button" class="btn btn-default btn-new-operacion1" id="button_new_forma">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada.tabs.venta_formaPago.button.nuevo_forma" />"> <span class="fa fa-plus"></span>
				</span>
			 </a>
		</div>
	</div>
	</div>
		<table id="datatable-list-edit_forma_pago" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th></th>
				<th></th>
				<th><spring:message code="venta.numerada.tabs.venta_formaPago.list.header.num_operacion" /></th>
				<th><spring:message code="venta.numerada.tabs.venta_formaPago.list.header.importe" /></th>
				<th><spring:message code="venta.numerada.tabs.venta_formaPago.list.header.formaPago" /></th>
				<th><spring:message code="venta.numerada.tabs.venta_formaPago.list.header.num_caja" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	
	
		<div class="form-group">			
			<label class="control-label col-md-6 col-sm-6 col-xs-12"><spring:message	code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.motivos" /></label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<select required="required" class="form-control" name="idmotivo_formapago" id="idmotivo_formapago">
						<option value=""></option>
							<x:forEach select="$motivosModificacion_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out
											select="$item/label" /></option>
								</x:forEach>
					</select>								
				</div>		
		</div>	
		
		<div class="modal-footer">
			<button id="aceptar_formas_button"  class="btn btn-primary anular_venta_reserva_dialog">
						<spring:message code="common.button.accept" />
					</button>
			<button type="button" class="btn btn-cancel close_dialog2" data-dismiss="modal">
				<spring:message code="common.button.cancel"/>
			</button>
		</div>		
	</form>
</div>

<script>

var json = ${datos_venta};

var dt_listformaspago=$('#datatable-list-edit_forma_pago').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	  initComplete: function( settings, json ) {
	    	window.setTimeout(CargarFormasPago, 100);  
	    },
	scrollCollapse: true,
	ordering:  false,
	paging: false,	
    select: { style: 'os' },
    columns: [
              {},   
              {},
              {},
              {},
              {},
              {}      
              
        ],
        "columnDefs": [
                       { "visible": false, "targets": [0,1]}
                     ],
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('datatable-list-edit_venta_numerada');
   	}
    
} ); 

function CargarFormasPago()
{
	var importes = ""+json.ArrayList.Importeparcial;
	if(importes!="undefined")
		{
		if(json.ArrayList.Importeparcial.length>0)
			{
			$.each( json.ArrayList.Importeparcial, function( index, Importeparcial ){
				dt_listformaspago.row.add([
					   						Importeparcial,          
					   						"",		
					   						Importeparcial.idimporteparcial,
					   						Importeparcial.importe, 
					   						Importeparcial.formapago.nombre,         
					   						""									    
					   					]).draw();
			})
			}
		else
			{
			dt_listformaspago.row.add([
			   						json.ArrayList.Importeparcial,          
			   						"",	
			   						json.ArrayList.Importeparcial.idimporteparcial,
			   						json.ArrayList.Importeparcial.importe, 
			   						json.ArrayList.Importeparcial.formapago.nombre,         
			   						""									    
			   					]).draw();
			}
		}
}

//****************************************************
$("#button_new_forma").on("click", function(e) {
	 $("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/venta/numerada/editarVenta/FormasPagoAnadir.do'/>", function() {
			$("#modal-dialog-form-3").modal('show');
			setModalDialogSize("#modal-dialog-form-3", "xs");
		});
});

//****************************************************
$("#aceptar_formas_button").on("click", function(e) {
	$("#form_formas_pago_guardar").validate();
})

$("#form_formas_pago_guardar").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {			
			guardarFormasPago()			
		}
	}
	);


var xml="";

function guardarFormasPago()
{
	var idVenta = $("#idVentaEditar").val();
	xml = "<Modificacion><idmodificacion/><observaciones/>";
	xml+="<motivomodificacion>"+$("#idmotivo_formapago option:selected").text()+"</motivomodificacion>";
	xml+="<venta><idventa>"+idVenta+"</idventa><entradasimpresas>false</entradasimpresas><reciboimpreso>false</reciboimpreso><imprimirSoloEntradasXDefecto/></venta>";
	xml+="<reserva><idreserva/></reserva><anulaciondesdecaja/>";
	xml+="<modificacionlineadetalles><Modificacionlineadetalle><lineadetalle><idlineadetalle/></lineadetalle></Modificacionlineadetalle></modificacionlineadetalles>";
	xml+="<modificacionimporteparcials>";
	var pagos = dt_listformaspago.rows().data();
	var numero = pagos.length;
	if(numero>0)
		for(i=0;i<numero;i++)
			{
			proscesarPago(pagos[i]);		
			}
	else
		proscesarPago(pagos)
	xml+="</modificacionimporteparcials>";	
	xml+="</Modificacion>";
	$("#xmlPagos").val(xml);
	
	
	
	var data = $("#form_formas_pago_guardar").serializeObject();
	
	$.ajax({
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/actualizarPagos.do'/>",
		timeout : 100000,
		data : data,
		success : function(data) {		
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
			$("#modal-dialog-form-2").modal('hide');
		},
		error : function(exception) {
			

			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : exception.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		}
	});

}


function proscesarPago(fila)
{
	if(fila[1]=="")
	{
	
	var jsonFila = fila[0];
	xml+="<Modificacionimporteparcial>";
	xml+="<tipomodificacion>0</tipomodificacion>";
	xml+="<importeparcial isnew=\"false\" anulado=\"0\">";
	xml+="<idimporteparcial>"+jsonFila.idimporteparcial+"</idimporteparcial>";
	xml+="<formapago>"+json2xml(jsonFila.formapago)+"</formapago>";
	xml+="<importe>"+jsonFila.importe+"</importe>";
	xml+="<modificacionimporteparcials>"+json2xml(jsonFila.modificacionimporteparcials)+"</modificacionimporteparcials>";
	xml+="<venta/><bono/><rts/><numpedido/>";
	xml+="</importeparcial>";			
	xml+="</Modificacionimporteparcial>";
	}
else
	{
	xml+="<Modificacionimporteparcial>";
	xml+="<tipomodificacion>0</tipomodificacion>";
	xml+="<importeparcial isnew=\"true\" anulado=\"0\">";
	xml+="<formapago>";
	xml+="<idformapago>"+fila[1]+"</idformapago>";
	xml+="<nombre></nombre>";
	xml+="</formapago>";
	xml+="<importe>"+fila[3]+"</importe>";
	xml+="<bono><idbono/></bono><rts/><numpedido/><anulado>0</anulado>";
	xml+="</importeparcial>";
	xml+="</Modificacionimporteparcial>";
	}
}

</script>

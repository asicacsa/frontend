<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<

<div class="modal fade" id="anulacion_entradas"
	role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<input type="hidden" id="bonoMotivosAction" name="bonoMotivosAction"/>
			<div class="modal-header">
				<button type="button" class="close close_dialog" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">�</span>
				</button>
				<h4 class="modal-title">
					<spring:message code="venta.ventareserva.busqueda.tabs.entradas.canceladas.title" />					
				</h4>
			</div>
			<div class="modal-body">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<table id="datatable-list-entradas-anulacion" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th><spring:message code="venta.ventareserva.tabs.entradas.list.header.entrada" /></th>
								<th><spring:message code="venta.ventareserva.busqueda.tabs.entradas.canceladas.producto.text" /></th>
								<th><spring:message code="venta.ventareserva.busqueda.tabs.entradas.canceladas.codigo.barras.text" /></th>
																							
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>				
				</div>
				
				<br/><br/>
				<div class="modal-footer">
					<button type="button" id="aceptar_anulacion_entradas" class="btn btn-primary"
						data-dismiss="modal">
						<spring:message code="common.button.accept" />
					</button>					
				</div>

			</div>
			
		</div>
	</div>
</div>

<script>
var dt_listentradas_anuladas =$('#datatable-list-entradas-anulacion').DataTable();
</script>
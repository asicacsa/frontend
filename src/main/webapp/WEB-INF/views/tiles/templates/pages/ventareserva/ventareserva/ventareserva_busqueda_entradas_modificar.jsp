<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="venta.ventareserva.busqueda.tabs.entradas.modificar" />			
	</h4>	
</div>

<div class="modal-body">
	<form id="form_modificar_entrda" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input type="hidden" name="xml" id="strXml_entradas_modificar"/>
		<input type="hidden" name="idEntrada" id="idEntradaModificar"/>
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
				<label  class="control-label col-md-6 col-sm-6 col-xs-12"><spring:message code="venta.ventareserva.tabs.entradas.list.header.usos" /></label>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<input name="usos" id="entradas_usos" type="text" data-inputmask="'mask': '9{0,10}'" class="form-control" value="${numUsos}">
				</div>
			</div>					
		</div>
			
	</form>
	<div class="modal-footer">
		<button id="modificar_usos_button" type="button" class="btn btn-primary">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>		

</div>

<script>

hideSpinner("#tab_entradas_modificar");
hideSpinner("#tab_entradas_venta_modificar_usos");
hideSpinner("#tab_entradas_reserva_modificar_usos");

//**************************************************************
$(":input").inputmask();
//***************************************************************
$("#modificar_usos_button").on("click", function(e) {
	var entrada;
	var entradaXml;


	if("${tipoBonos}"=="0")
	{
		entrada = dt_listentradas.rows( { selected: true } ).data()[0];
		entradaXml = "<Entrada>"+json2xml(entrada, '')+"</Entrada>";
		$("#strXml_entradas_modificar").val(entradaXml);
	}
	else if("${tipoBonos}"=="2")
	{
		entrada = dtentradasVenta.rows( { selected: true } ).data()[0][0];
		$("#idEntradaModificar").val(entrada);
	}else if("${tipoBonos}"=="3")
	{
		entrada = dtentradasReserva.rows( { selected: true } ).data()[0][0];
		$("#idEntradaModificar").val(entrada);
	}
	else
	{
		entrada = dtentradasBono.rows( { selected: true } ).data()[0][0];
		$("#idEntradaModificar").val(entrada);
	}
		
    modificarEntrada();
});

function modificarEntrada()
{
	
	var data = $("#form_modificar_entrda").serializeObject();
	var usos = $("#entradas_usos").val();
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/busqueda/entrada/actualizarUsos.do'/>",
		timeout : 100000,
		data: data, 
		success : function(data) {
			
			if("${tipoBonos}"=="0")
				{
				$("#modal-dialog-form").modal('hide');
				dt_listentradas.ajax.reload();
				}
			else if("${tipoBonos}"=="2")
			{
				$("#modal-dialog-form-3").modal('hide');
				
				dtentradasVenta
				  .rows({ selected: true })
				  .every(function (rowIdx, tableLoop, rowLoop) {
					  dtentradasVenta.cell(rowIdx,9).data(usos);					  
				  })
				  .draw();				
			}
			else if("${tipoBonos}"=="3")
			{
				$("#modal-dialog-form-3").modal('hide');
				
				dtentradasReserva
				  .rows({ selected: true })
				  .every(function (rowIdx, tableLoop, rowLoop) {
					  dtentradasReserva.cell(rowIdx,9).data(usos);					  
				  })
				  .draw();				
			}
			else	
				{
				$("#modal-dialog-form-2").modal('hide');
				dtentradasBono.rows( { selected: true } ).cell(':eq(9)').data(usos).draw();
				
				dtentradasBono
				  .rows({ selected: true })
				  .every(function (rowIdx, tableLoop, rowLoop) {
					  dtentradasBono.cell(rowIdx,9).data(usos);					  
				  })
				  .draw();			
				
				
				}
				
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});

}


</script>

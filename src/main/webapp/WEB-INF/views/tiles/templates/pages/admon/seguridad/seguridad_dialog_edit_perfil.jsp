<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editarperfil_datos_perfil}" var="editarperfil_datos_perfil_xml" />
<x:parse xml="${editarperfil_selector_unidades}" var="editarperfil_selector_unidades_xml" />
<x:parse xml="${editarperfil_selector_roles}" var="editarperfil_selector_roles_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">x</span>
	</button>
	<h4 class="modal-title">
		<x:choose>
			<x:when select="not($editarperfil_datos_perfil_xml/Perfilusuario)">
				<spring:message code="administracion.seguridad.tabs.perfiles.dialog.crear_perfil.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.seguridad.tabs.perfiles.dialog.editar_perfil.title" />
			</x:otherwise>
		</x:choose>
	</h4>
</div>
<div class="modal-body" id="principal">

	<form id="form_perfil_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

		<input id="idperfil" name="idperfil" type="hidden" value="<x:out select = "$editarperfil_datos_perfil_xml/Perfilusuario/idperfilusuario" />" />

		<div class="col-md-6 col-sm-6 col-xs-12">

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.perfiles.field.nombre" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editarperfil_datos_perfil_xml/Perfilusuario/nombre" />">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.perfiles.field.descripcion" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<textarea class="form-control" rows="3" name="descripcion" id="descripcion"><x:out select="$editarperfil_datos_perfil_xml/Perfilusuario/descripcion" /></textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.perfiles.field.appdefecto" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="idappdefecto" id="selector_appdefecto" class="form-control" required="required">
						<option value=""></option>
					</select>
				</div>
			</div>

			<div class="form-group" id="selector_usuarios_form_group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.perfiles.field.usuarios" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="usuarios[]" id="selector_usuarios" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
					</select>
				</div>
			</div>

		</div>

		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.perfiles.field.unidades" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="idunidad" id="selector_unidad" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$editarperfil_selector_unidades_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.perfiles.field.roles" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="idrol" id="selector_rol" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$editarperfil_selector_roles_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.perfiles.field.funcionalidades" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="funcionalidades[]" id="selector_funcionalidades" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
					</select>
				</div>
			</div>

		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_perfil_button" type="submit" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>

	</form>

</div>

<script>
	hideSpinner("#tab_perfiles_new");
	hideSpinner("#tab_perfiles_edit");

	$(".select-select2").select2({
		language : "es",
		containerCssClass : "single-line"
	});

	cargar_usuarios()
	cargar_funcionalidades();

	var appdefecto= $appdefecto;
	appdefecto.forEach(function (item) {
		$("#selector_appdefecto").append($("<option/>", {value: item.value, text: item.desc}))
	});

	$('#selector_rol option[value="<x:out select = "$editarperfil_datos_perfil_xml/Perfilusuario/roljasper/idroljasper" />"]').attr("selected", "selected");
	$('#selector_unidad option[value="<x:out select = "$editarperfil_datos_perfil_xml/Perfilusuario/unidadnegocio/idunidadnegocio" />"]').attr("selected", "selected");
	$('#selector_appdefecto option[value="<x:out select = "$editarperfil_datos_perfil_xml/Perfilusuario/appdefecto" />"]').attr("selected", "selected");

	$("#form_perfil_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFormData();
		}
	});

	//********************************************************************************
	function cargar_funcionalidades()
	{
		var data = [ <c:out value="${json_selector_funcionalidades}"  escapeXml="false" /> ];
		
		$("#selector_funcionalidades").select2ToTree({treeData: {dataArr: data}, containerCssClass : "single-line"});
		$("#selector_funcionalidades .non-leaf").attr("disabled","disabled");
		
		var selectedValuesFuncionalidades = new Array();
		<x:forEach select="$editarperfil_datos_perfil_xml/Perfilusuario/perfilusufuncs/Perfilusufunc" var="item">
		selectedValuesFuncionalidades.push('<x:out select="$item/funcionalidad/idfuncionalidad" />');
		</x:forEach>

		$('#selector_funcionalidades').val(selectedValuesFuncionalidades);
		$('#selector_funcionalidades').trigger('change.select2');	
	}
	
	
	//********************************************************************************
	function cargar_usuarios()
	{
		showFieldSpinner("#selector_usuarios_form_group");			

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/seguridad/seguridad/select_list_usuario.do'/>",
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				$.each(data.ArrayList.LabelValue, function(i, item) {
				    $("#selector_usuarios").append('<option value="'+item.value+'">'+item.label+'</option>');
				});				
			    
				var selectedValues = new Array();
				<x:forEach select="$editarperfil_datos_perfil_xml/Perfilusuario/perfilusuariousuarios/Perfilusuariousuario/usuario" var="item">
					selectedValues.push('<x:out select="$item/idusuario" />');						
				</x:forEach>
				
				$('#selector_usuarios').val(selectedValues);
				$('#selector_usuarios').trigger('change.select2');
			    					
		    	hideSpinner("#selector_usuarios_form_group");
			}
		});
	}
	
	

	//********************************************************************************	
	function saveFormData() {
		
		var data = $("#form_perfil_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/seguridad/seguridad/save_perfil.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				dt_listperfiles.ajax.reload(null,false);						
				$("#modal-dialog-form").modal('hide');
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
	}
</script>

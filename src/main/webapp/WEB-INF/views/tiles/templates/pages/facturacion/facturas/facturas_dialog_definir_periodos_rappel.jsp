<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_periodo_rappel}" var="editar_periodo_rappel_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="facturacion.facturas.tabs.rappels.dialog.definir_periodos_rappel.title" />
	</h4>	
</div>

<div class="modal-body">
	<form id="form_periodo_rappel_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<div class="col-md-12 col-sm-12 col-xs-12">		
			<div class="form-group date-picker">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.rappels.field.fechacalculo" />:</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<a type="button" class="btn btn-default btn-clear-date" id="button_calculo_clear">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.rappels.list.button.clear" />"> <span class="fa fa-trash"></span>
						</span>
					</a>			
                    <div class="input-prepend input-group">
                    	<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                        <input type="text" name="calculo" id="calculo" class="form-control" value="" readonly/>
                        <input type="hidden" required="required" name="fechacalculo" value="<x:out select = "$editar_periodo_rappel_xml/PeriodosRappel/diacalculorappel" />"/>
                    </div>
				</div>
			</div>
			<div class="form-group date-picker">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.rappels.field.periodocalculo" />:</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<a type="button" class="btn btn-default btn-clear-date" id="button_periodo_clear">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.rappels.list.button.clear" />"> <span class="fa fa-trash"></span>
						</span>
					</a>			
                    <div class="input-prepend input-group">
                    	<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                        <input type="text" name="periodo" id="periodo" class="form-control" value="" readonly/>
                        <input type="hidden" required="required" name="fechainicioperiodo" value="<x:out select = "$editar_periodo_rappel_xml/PeriodosRappel/fechainicioperiodo" />"/>
                        <input type="hidden" required="required" name="fechafinperiodo" value="<x:out select = "$editar_periodo_rappel_xml/PeriodosRappel/fechafinperiodo" />"/>
                    </div>
				</div>
			</div>	
		
		</div>
	</form>
	<div class="modal-footer">
		<button id="save_periodo_rappel_button" type="button" class="btn btn-primary save_peridos_rappel">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>	


<script>
hideSpinner("#tab_rappels_definir_periodo");

//******************************************************************
//dia calculo rappel
$today= moment().format("DD/MM/YYYY");
dia_inicio = $today;

if($('input[name="fechacalculo"]').val()!='')
{	
dia_inicio = $('input[name="fechacalculo"]').val().substring(0, 10);
}

$('input[name="calculo"]').val( dia_inicio);

$('input[name="calculo"]').daterangepicker({
	singleDatePicker: true,
	autoUpdateInput: false,
	linkedCalendars: false,
	showDropdowns: true,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start) {
  	  	 $('input[name="calculo"]').val(start.format('DD/MM/YYYY'));
  	  	 $('input[name="fechacalculo"]').val(start.format('DD/MM/YYYY'));
  	  	 
	 });
	 
$("#button_calculo_clear").on("click", function(e) {
    $('input[name="calculo"]').val('');
    $('input[name="fechacalculo"]').val('');
    
});	 
//*******************************************************************************
//Fecha fin/inicio periodo
var dia_inicio = $today;
var dia_fin = $today;

if($('input[name="fechainicioperiodo"]').val()!='')
{	
dia_inicio = $('input[name="fechainicioperiodo"]').val().substring(0, 10);
}
if($('input[name="fechafinperiodo"]').val()!='')
{	
dia_fin = $('input[name="fechafinperiodo"]').val().substring(0, 10);
}


$('input[name="periodo"]').val( dia_inicio + ' - ' + dia_fin);
$('input[name="fechainicioperiodo"]').val( dia_inicio);
$('input[name="fechafinperiodo"]').val( dia_fin);


$('input[name="periodo"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
  	  	 $('input[name="periodo"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('input[name="fechainicioperiodo"]').val(start.format('DD/MM/YYYY'));  	  	 
  	  	 $('input[name="fechafinperiodo"]').val(end.format('DD/MM/YYYY'));
	 });

$("#button_periodo_clear").on("click", function(e) {
    $('input[name="periodo"]').val('');
    $('input[name="fechainicioperiodo"]').val('');
    $('input[name="fechafinperiodo"]').val('');
});

//******************************GRABAR PERIODOS*************************************
$(".save_peridos_rappel").on("click", function(e) { 
	var data = $("#form_periodo_rappel_data").serializeObject();
	showSpinner("#modal-dialog-form .modal-content");

	$.ajax({
		type : "post",
		url : "<c:url value='/ajax/facturacion/facturas/rappels/save_periodo_rappel.do'/>",
		timeout : 100000,
		data : data,
		success : function(data) {
			hideSpinner("#modal-dialog-form .modal-content");
			$("#modal-dialog-form").modal('hide');				
			
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
			
		},
		error : function(exception) {
			hideSpinner("#modal-dialog-form .modal-content");

			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : exception.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		}
	});
		
})
//***************************************************************************************
</script>
	
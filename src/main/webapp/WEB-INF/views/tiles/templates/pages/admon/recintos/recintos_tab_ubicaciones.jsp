<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_ubicaciones_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.ubicaciones.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_ubicaciones_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.ubicaciones.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_ubicaciones_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.ubicaciones.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>		
	</div>
	
	<table id="datatable-lista-ubicaciones" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="administracion.recintos.tabs.ubicaciones.list.header.nombre" /></th>
				<th><spring:message code="administracion.recintos.tabs.ubicaciones.list.header.descripcion" /></th>
				<th><spring:message code="administracion.recintos.tabs.ubicaciones.list.header.taquillas" /></th>			
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	

</div>

<script>

var dt_listubicaciones=$('#datatable-lista-ubicaciones').DataTable( {
    ajax: {
        url: "<c:url value='/ajax/admon/recintos/ubicaciones/list_ubicaciones.do'/>",
        rowId: 'idubicacion',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Ubicacion)); return(""); },
        error: function (xhr, error, thrown) {
            if (xhr.responseText=="403") {
                  $("#tab_ubicaciones").hide();
            }                   
     },
        
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listubicaciones.data().count()>0) dt_listubicaciones.columns.adjust().draw(); });
	},
    columns: [
        { data: "nombre", type: "spanish-string" ,  defaultContent:""} ,
        { data: "descripcion", type: "spanish-string" ,  defaultContent:""},
        { data: "taquillas.Taquilla", className: "cell_centered",
        	  render: function ( data, type, row, meta ) { return showListInCell(data,"<spring:message code="administracion.recintos.tabs.recintos.list.text.taquillas" />","nombre"); }	
              } 
       
        
    ],    
    drawCallback: function( settings ) {
    	activateTooltipsInTable('datatable-lista-ubicaciones')
    },
    select: { style: 'os', selector:'td:not(:last-child)'},
	language: dataTableLanguage,
	processing: true,
} );

insertSmallSpinner("#datatable-lista-ubicaciones_processing");
//********************************************************************************
$("#tab_ubicaciones_edit").on("click", function(e) { 
	showButtonSpinner("#tab_ubicaciones_edit");
	var data = sanitizeArray(dt_listubicaciones.rows( { selected: true } ).data(),"idubicacion");
		
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.recintos.tabs.ubicaciones.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		hideSpinner("#tab_ubicaciones_edit");
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.recintos.tabs.ubicaciones.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		hideSpinner("#tab_ubicaciones_edit");
		return;
	}	
		
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/recintos/ubicaciones/show_ubicacion.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "sm");
	});
})

//********************************************************************************
$("#tab_ubicaciones_new").on("click", function(e) { 
	showButtonSpinner("#tab_ubicaciones_new");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/recintos/ubicaciones/show_ubicacion.do'/>", function() {										  
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "sm");
	});
})
//********************************************************************************
$("#tab_ubicaciones_remove").on("click", function(e) { 
	showButtonSpinner("#tab_ubicaciones_remove");	
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="administracion.recintos.tabs.ubicaciones.list.confirm.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
  		  buttons: { closer: false, sticker: false	},
  		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
	
		   dt_listubicaciones.processing(true);
			
			var data = sanitizeArray(dt_listubicaciones.rows( { selected: true } ).data(),"idubicacion");
		   
			$.ajax({
				contenttype: "application/json; charset=utf-8",
				type : "post",
				url : "<c:url value='/ajax/admon/recintos/ubicaciones/remove_ubicaciones.do'/>",
				timeout : 100000,
				data: { data: data.toString() }, 
				success : function(data) {
					dt_listubicaciones.ajax.reload();					
				},
				error : function(exception) {
					dt_listubicaciones.processing(false);
					
					new PNotify({
					      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					      text: exception.responseText,
						  type: "error",		     
						  delay: 5000,
						  buttons: { sticker: false }
					   });			
				}
			});

	   }).on('pnotify.cancel', function() {
	   });		 
	hideSpinner("#tab_ubicaciones_remove");
}); 

</script>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${disponibles_datos_sesiones}" var="disponibles_datos_sesiones_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.disponibles.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_establecer_disponibles" class="form-horizontal form-label-left">
	
		<div class="form-group">
		
				<label class="control-label col-md-1 col-sm-1 col-xs-12"><spring:message code="venta.ventareserva.dialog.edit_sesion.field.numero" /></label>				
				<div class=" col-md-2 col-sm-2 col-xs-12">
  				   <button type="button"class="input-group-button btn-number" id="minus" data-type="minus" data-field="quant[1]"> <i class="fa fa-minus" aria-hidden="true"></i> </button>
  				    <input type="text" min="1" class="form-control input-number" max="10000" name="quant[1]" id="cantidad" value="1" >  				
  				    <button type="button"class="input-group-button btn-number" id="plus" data-type="plus" data-field="quant[1]"> <i class="fa fa-plus" aria-hidden="true"></i> </button>
				</div>				
				
					
				<label class="control-label col-md-1 col-sm-1 col-xs-12"><spring:message code="venta.ventareserva.dialog.edit_sesion.field.tarifa" /></label>
				<div class="col-md-2 col-sm-2 col-xs-12">
					<select class="form-control" name="idtarifa" id="idtarifa">
						<option value=""></option>
					</select>
				</div>		
				
					
				<label class="control-label col-md-1 col-sm-1 col-xs-12"><spring:message code="venta.ventareserva.dialog.edit_sesion.field.descuentos" /></label>
				<div id="descuentos_div" class="col-md-2 col-sm-2 col-xs-12">
					<select class="form-control" name="iddescuento" id="iddescuento">
						<option value=""></option>						
					</select>
				</div>
				
							
				<label class="control-label col-md-2 col-sm-2x col-xs-12"><spring:message code="venta.ventareserva.dialog.edit_sesion.field.bono_a" /></label>
				<div class="col-md-1 col-sm-1 col-xs-12">
					<input name="bono" id="bono" type="text" class="form-control" value="${bono}">
				</div>		
				
		</div>
		
		
			<x:forEach select = "$disponibles_datos_sesiones_xml/ArrayList/ArrayList" var = "item">
	
				<div id="tabla-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />" class="col-md-12 col-sm-12 col-xs-12 disponibles">
				
					<div class="col-md-10 col-sm-10 col-xs-12">
	
						<div class="form-group encabezado-disponibilidad">
							<label class="control-label col-md-8 col-sm-8 col-xs-12 label-disponibilidad">Disponibilidad <span class="nombre-producto"><x:out select = "$item/Tipoproducto/nombre" /></span></label>
							<label class="control-label col-md-1 col-sm-1 col-xs-4""><spring:message code="venta.ventareserva.dialog.disponibles.field.fecha" /></label>
							<div class="col-md-3 col-sm-3 col-xs-8" id="div-fecha-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />">
			                      <div class="input-prepend input-group">
			                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
			                        <input type="text" name="fecha_disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />" id="fecha_disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />" class="form-control" readonly value="<x:out select = "$item/String" />"/>
			                      </div>
							</div>
						</div>
					</div>
				
					<div class="btn-group pull-right btn-datatable">
						<a type="button" class="btn btn-info" id="button_disponibles_lock-<x:out select = "$item/Tipoproducto/idtipoproducto" />">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.bloquear" />"> <span class="fa fa-lock"></span>
							</span>
						</a>
						<a type="button" class="btn btn-info" id="button_disponibles_unlock-<x:out select = "$item/Tipoproducto/idtipoproducto" />">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.desbloquear" />"> <span class="fa fa-unlock"></span>
							</span>
						</a>
					</div>
				
					<div class="col-md-12 col-sm-12 col-xs-12">
						<table id="datatable-list-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />" class="table table-striped table-bordered dt-responsive sesiones" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.idsesion" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.fecha" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.hora" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.overbooking" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.contenido" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.idzona" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.zona" /></th>
									<th class="disponibles"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.disponibles" /></th>
									<th class="bloqueadas"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.bloqueadas" /></th>
									<th class="reservadas"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.reservadas" /></th>
									<th class="vendidas"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.vendidas" /></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
						<span>&nbsp;</span>
					</div>
				</div>
				
			</x:forEach>
		

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_disponibles_button" type="button" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
			
	</form>
	
</div>

<script>
hideSpinner("#button_add_${buttonAdd}");
hideSpinner("#button_${buttonAdd}_sesiones");

if(boton_venta_directa!="")
	{
	hideSpinner(boton_venta_directa);
	boton_venta_directa="";
	}


hideSpinner("#tab_venta_individual");	
hideSpinner("#tab_reserva_grupo");
hideSpinner("#tab_reserva_individual");

function cargarDescuentos()
{
	//var perfilVisitante= obtenerPerfilVisitante(sesion_data.Lineadetalle.perfiles.Perfilvisitante,$("#idtarifa").val());
	jsonProducto.nombrePerfil = $("#idtarifa :selected").text();
	jsonProducto.idPerfil = $("#idtarifa :selected").val();
	jsonProducto.nombreTarifa = $("#idtarifa :selected").attr("nombre_tarifa");
	jsonProducto.importe = calcularImporteTotal();
	jsonProducto.importeUnitario = Number($("#idtarifa :selected").attr("importe"));
	showFieldSpinner("#descuentos_div");
	$select=$("#iddescuento");
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/list_descuentos.do'/>",
		timeout : 100000,
		data: {
				idproducto: "${idProducto}",
				idcanal: "${sessionScope.idcanal}",
				idtarifa: $("#idtarifa :selected").attr("idtarifa")
			  }, 
		success : function(data) {
			hideSpinner("#descuentos_div");
			$select.html('');
			$select.prepend("<option value='' selected='selected'></option>");
			if (data.ArrayList!="") {
				var item=data.ArrayList.LabelValue;
				if (item.length>0)
				    $.each(item, function(key, val){
				      $select.append('<option value="' + val.value + '">' + val.label + '</option>');
				    });
				else
			      	$select.append('<option value="' + item.value + '">' + item.label + '</option>');
				var edicion=${edicion};
				if(edicion=="1")
					cargarDatosInicialesEdiccion();
			}
		},
		error : function(exception) {
			hideSpinner("#descuentos_div");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});	
}

//*************************************************
$("#idtarifa").on("change", function(e) {
	cargarDescuentos();
}); 

var json_sesiones= [ <c:out value="${disponibles_datos_sesiones_json}"  escapeXml="false" /> ];
var fecha_seleccionada="${fecha}";


var jsonProducto = ${jsonProducto};


$( document ).ready(function() {
	cargarTarifas();
	});


/******************************************/
function cargarTarifas()
{
	var id_producto = ""+ jsonProducto.Producto.idproducto;
	tarifas = jsonProducto.Producto.tarifaproductos.Tarifaproducto;
	var strtarifas = ""+tarifas;

	var $select= $("#idtarifa");
	$select.html('');
	if(strtarifas!="undefined")
		{
		
		
	if(tarifas.length>0)
		{
		$.each(tarifas, function(key, tarifa){	
			var perfil = ""+tarifa.perfilvisitantes;
			if(perfil!="undefined")
				{				
				var perfiles = tarifa.perfilvisitantes.Perfilvisitante;
				if(perfiles.length>0)
					$.each(perfiles, function(key, perfil){
						$select.append('<option value="' + perfil.idperfilvisitante + '" idtarifa="' + tarifa.tarifa.idtarifa + '" nombre_tarifa="'+tarifa.tarifa.nombre+'" importe="' + tarifa. importe + '">' + perfil.nombre + '</option>');
					})
				else
					$select.append('<option value="' + perfiles.idperfilvisitante + '" idtarifa="' + tarifa.tarifa.idtarifa +  '" nombre_tarifa="'+tarifa.tarifa.nombre+'" importe="' + tarifa. importe + '">' + perfiles.nombre + '</option>');
				}				
		})
		}
	else
		{
		tarifa = tarifas;
		var perfil = tarifa.perfilvisitantes;
		if(perfil==undefined)
			{
			$select.append('<option value="' + perfiles.idperfilvisitante + '" idtarifa="' + tarifa.tarifa.idtarifa +  '" nombre_tarifa="'+tarifa.tarifa.nombre+'" importe="' + tarifa. importe + '">' + perfiles.nombre + '</option>');
			}
		else
			{
			var perfiles = tarifas.perfilvisitantes.Perfilvisitante;
			if(perfiles.length>0)
				$.each(perfiles, function(key, perfil){
					$select.append('<option value="' + perfil.idperfilvisitante + '" idtarifa="' + tarifa.tarifa.idtarifa + '" nombre_tarifa="'+tarifa.tarifa.nombre+'" importe="' + tarifa. importe + '">' + perfil.nombre + '</option>');
				})
			else
				$select.append('<option value="' + perfiles.idperfilvisitante + '" idtarifa="' + tarifa.tarifa.idtarifa +  '" nombre_tarifa="'+tarifa.tarifa.nombre+'" importe="' + tarifa. importe + '">' + perfiles.nombre + '</option>');
			}
		}
		
	
	
   
	$select.html($select.children('option').sort(function (a, b) {
      return a.text === b.text ? 0 : a.text < b.text ? -1 : 1;
    }));
	
	if($('#idtarifa option').size()>1) 
		{
		/*if((id_producto=="4933475") || (id_producto=="8531"))
			$select.val("146567");
		else	
			$select.prop("selectedIndex", 1);*/
			$('#idtarifa option')
		    .filter(function(index) { return $(this).text() === 'Precio único'; })
		    .prop('selected', true);
			$('#idtarifa option')
		    .filter(function(index) { return $(this).text() === 'Adulto'; })
		    .prop('selected', true);
			$('#idtarifa option')
		    .filter(function(index) { return $(this).text() === 'EOLA'; })
		    .prop('selected', true);
		}		
	else
		$select.prop("selectedIndex", 0);
	
	cargarDescuentos();}
	
}



/******************************************/
function check_selected(tabla) {
	sesiones= tabla.rows().data();
	$.each(sesiones, function(key_sesiones,fila_sesiones) {
		if (${list}.rows({selected: true}).data().length>0) {
			var item= ${list}.rows({selected: true}).data()[0][11];
			if (item.length>0) {
				$.each(item, function(key_seleccionadas,fila_seleccionadas) {
					if (fila_sesiones[1]==fila_seleccionadas[1] && fila_sesiones[6]==fila_seleccionadas[6]) tabla.row(key_sesiones).select().draw();
				});
			}
		}
			
	});
}

function get_sesion(idtipoproducto,idsesion,idzona,sesionesdata) {
	var result= null;
	if (sesionesdata==null) sesionesdata= json_sesiones[0].ArrayList;
	if (sesionesdata!="") {
		var item= sesionesdata.ArrayList;
		if (typeof item!="undefined")
			if (item.length>0)
			    $.each(item, function(key, val){
			    	if (val.Tipoproducto.idtipoproducto==idtipoproducto) {
			    		if (val.sesiones!="") {
				   			var sesiones= val.sesiones.Sesion;
				   			if (sesiones.length>0) 
				   			    $.each(sesiones, function(key, val){
				   			    	//if (typeof val.Sesion!="undefined")
					   			    	//if (val.Sesion.idsesion==idsesion) result= val.Sesion
					   			    	if (val.idsesion==idsesion && val.zonasesions.Zonasesion.zona.idzona==idzona) result= val
				   			    });
			   			    else if (sesiones.idsesion==idsesion && sesiones.zonasesions.Zonasesion.zona.idzona==idzona) result= sesiones;
				    	}
			    	}
			    })
			else if (item.Tipoproducto.idtipoproducto==idtipoproducto) {
				if (item.sesiones!="") {
		   			var sesion= item.sesiones.Sesion;
		   			if (sesion.length>0) 
		   			    $.each(sesion, function(key, val){
		   			    	if (val.idsesion==idsesion && val.zonasesions.Zonasesion.zona.idzona==idzona) result= val
		   			    });
	   			    else if (sesion.idsesion==idsesion && sesion.zonasesions.Zonasesion.zona.idzona==idzona) result= sesion;
				}
			}
	}
	return result;
}

<x:forEach select = "$disponibles_datos_sesiones_xml/ArrayList/ArrayList" var = "item">

	$('input[name="fecha_disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />"]').daterangepicker({
	    singleDatePicker: true,
	    showDropdowns: true,
		minDate: moment(),
	  	locale: $daterangepicker_sp
	});

	var dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />=$('#datatable-list-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />').DataTable( {
		language: dataTableLanguage,
		info: false,
		searching: false,
		scrollY: "300px",
		scrollCollapse: true,
		paging: false,
	    select: { style: 'single' },
		columnDefs: [
            { "targets": 0, "visible": false },
            { "targets": 1, "visible": false },
            { "targets": 2, "visible": false },
            { "targets": 6, "visible": false },
            { "targets": 12, "visible": false }
	    ],
	    "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
            /* Append the grade to the default row class name */
            var sesion = aData[0];
            if(sesion.bloqueado==1)
            	$('td', nRow).css('color', 'Red');
            
        },
	    drawCallback: function( settings ) { 
	    	activateTooltipsInTable('datatable-list-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />');
	   	}
	} );
	
	function ajustar_cabeceras_datatable_<x:out select = "$item/Tipoproducto/idtipoproducto" />()
	{
		$('#datatable-list-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />').DataTable().columns.adjust().draw();
		if($('#datatable-list-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />').DataTable().rows().count()==1)
			{
			$('#datatable-list-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />').DataTable().row(0).select().draw();
			}
		
	}
	
	insertSmallSpinner("#datatable-list-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />_processing");
	
	<x:forEach select = "$item/sesiones/Sesion" var="sesion">
		dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.row.add([
	         get_sesion("<x:out select = "$item/Tipoproducto/idtipoproducto" />","<x:out select = "$sesion/idsesion" />","<x:out select = "$sesion/zonasesions/Zonasesion/zona/idzona" />",null), 
	         "<x:out select = "$sesion/idsesion" />", 
	         "<x:out select = "$sesion/fecha" />",         
	         "<x:out select = "$sesion/horainicio" />", 
	         "<x:out select = "$sesion/overbookingventa" />", 
	         "<span style='white-space: nowrap' title='<x:out select = "$sesion/contenido/descripcion" />'> <x:out select = "$sesion/contenido/nombre" /> </span>",
	         "<x:out select = "$sesion/zonasesions/Zonasesion/zona/idzona" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/zona/nombre" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/numlibres" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/numbloqueadas" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/numreservadas" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/numvendidas" />",
	         "<span style='white-space: nowrap' title='<x:out select = "$sesion/contenido/descripcion" />'> <x:out select = "$sesion/horainicio" /> <x:out select = "$sesion/contenido/nombre" /> </span>",
	    ]);
	</x:forEach>
	dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.draw();
	setTimeout(ajustar_cabeceras_datatable_<x:out select = "$item/Tipoproducto/idtipoproducto" />, 800);
	check_selected(dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />);

	//********************************************************************************
		
	$('input[name="fecha_disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />"]').on("change", function() { 
		//dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.processing(true);
		showFieldSpinner("#div-fecha-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />");
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/venta/ventareserva/list_tipos_producto.do'/>",
			timeout : 100000,
			data: {
	  			   idtipoproducto: "<x:out select = "$item/Tipoproducto/idtipoproducto" />",
		    	   fecha: $('input[name="fecha_disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />"]').val()
				  }, 
			success : function(data) {
				if (data.ArrayList!="") {
					dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.rows().remove().draw();
					var item= data.ArrayList.sesiones.Sesion;
					if (typeof item!="undefined")
						if (item.length>0) {
						    $.each(item, function(key, sesion){
								dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.row.add([
									 sesion,								                                                                                   
							         sesion.idsesion, 
							         sesion.fecha, 
							         sesion.horainicio, 
							         sesion.overbookingventa, 
							         "<span title='"+sesion.contenido.descripcion+"'> "+sesion.contenido.nombre+" </span>", 
							         sesion.zonasesions.Zonasesion.zona.idzona, 
							         sesion.zonasesions.Zonasesion.zona.nombre, 
							         sesion.zonasesions.Zonasesion.numlibres, 
							         sesion.zonasesions.Zonasesion.numbloqueadas, 
							         sesion.zonasesions.Zonasesion.numreservadas, 
							         sesion.zonasesions.Zonasesion.numvendidas,
							         "<span style='white-space: nowrap' title='"+sesion.contenido.descripcion+"'> "+sesion.horainicio+" "+sesion.contenido.nombre+" </span>"					         
							    ]);
						    });
						    dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.draw();
						}
						else {
							dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.row.add([
							     item,                                                                              			
						         item.idsesion, 
						         item.fecha, 
						         item.horainicio, 
						         item.overbookingventa, 
						         "<span title='"+item.contenido.descripcion+"'> "+item.contenido.nombre+" </span>", 
						         item.zonasesions.Zonasesion.zona.idzona, 
						         item.zonasesions.Zonasesion.zona.nombre, 
						         item.zonasesions.Zonasesion.numlibres, 
						         item.zonasesions.Zonasesion.numbloqueadas, 
						         item.zonasesions.Zonasesion.numreservadas, 
						         item.zonasesions.Zonasesion.numvendidas,
						         "<span style='white-space: nowrap' title='"+item.contenido.descripcion+"'> "+item.horainicio+" "+item.contenido.nombre+" </span>"					         
						    ]);		
						    dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.row(0).select().draw();
						}
						
					//dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.processing(false);
					hideSpinner("#div-fecha-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />");
				    
				}
			},
			error : function(exception) {
				//dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.processing(false);
				hideSpinner("#div-fecha-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />");
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",
					  buttons: { sticker: false }				  
				   });		
			}
		});	
	});

	//********************************************************************************
	$("#button_disponibles_lock-<x:out select = "$item/Tipoproducto/idtipoproducto" />").on("click", function(e) {
		var data = dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.rows( { selected: true } ).data();
		if (data.length<=0) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="administracion.programacion.programacion.list.alert.localidades.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}	

		var button_bloqueos= "button_disponibles_lock-<x:out select = "$item/Tipoproducto/idtipoproducto" />",
			lista_bloqueos="dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />";
		
		showButtonSpinner("#"+button_bloqueos);
		$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/show_new_bloqueo.do'/>?id="+data[0][1]+"&button="+button_bloqueos+"&lista="+lista_bloqueos+"&zona="+data[0][6]+"&es_programacion=false&idx_libres=8&idx_bloqueadas=9", function() {
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "sm");
		});
	});
	
	//********************************************************************************
	$("#button_disponibles_unlock-<x:out select = "$item/Tipoproducto/idtipoproducto" />").on("click", function(e) { 
		
		var data = dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.rows( { selected: true } ).data();
		
		if (data.length<=0) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="administracion.programacion.programacion.list.alert.localidades.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}	
		
		var button_bloqueos= "button_disponibles_unlock-<x:out select = "$item/Tipoproducto/idtipoproducto" />",
		lista_bloqueos="dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />";
		
		showButtonSpinner("#button_programacion_bloqueos");
		$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/show_bloqueos.do'/>?id="+data[0][1]+"&button="+button_bloqueos+"&lista="+lista_bloqueos+"&zona="+data[0][6]+"&es_programacion=false&idx_libres=8&idx_bloqueadas=9", function() {
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "md");
		});

	});	

</x:forEach>

function obtenerIdTarifa(tarifas,idperfilvisitante) {
	for (var i=0; i<tarifas.length; i++) 
		if (tarifas[i].idperfilvisitante==idperfilvisitante) return(tarifas[i].tarifa.idtarifa); 
	return("");
}

function construir_xml_sesiones(sesiones_sel) {
	var xml_sesiones= "<sesiones>";
	for (var i=0; i<sesiones_sel.length; i++) { 
		xml_sesiones+="<Sesion>";
		xml_sesiones+=json2xml(sesiones_sel[i][0],"");
		xml_sesiones+="</Sesion>";
	}
	xml_sesiones+="</sesiones>";
	return(xml_sesiones);
}


//*************************************************
function actualizarDescuentosDetalle(sesion_data,fechas,contenidos,disponibles,sesiones) {
	actualizando= true;
	
		showFieldSpinner("#descuentos_div");
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/venta/ventareserva/aplicar_descuentos.do'/>",
			timeout : 100000,
			data: {
					idcliente: "${idCliente}",
					lineadetalle: "<Lineadetalle>"+json2xml(sesion_data.Lineadetalle,"")+"</Lineadetalle>"
				  }, 
			success : function(data) {
				hideSpinner("#descuentos_div");

				
				sesion_data.Lineadetalle.importe= data.Lineadetalle.importe;
				sesion_data.Lineadetalle.tarifaproducto.importe= data.Lineadetalle.tarifaproducto.importe;
				var edicion=${edicion};
				if(edicion=="1")
					{					
					${list}.rows( { selected: true } ).remove().draw();
					}
				
				var bono = sesion_data.Lineadetalle.numerobonoagencia;
				if(bono!="")
					conAbono= true;
				
				${list}.row.add([sesion_data,
				                 "${idProducto}", 
		                         "${producto}",
					             fechas, // fecha
					             contenidos, // contenido
					             disponibles, // disponibles
					             sesion_data.Lineadetalle.cantidad,  // Nº Etd
					             typeof sesion_data.Lineadetalle.perfilvisitante.nombre=="undefined"?"":sesion_data.Lineadetalle.perfilvisitante.nombre, // Perfil/Tarifa
					             sesion_data.Lineadetalle.descuentopromocional.nombre, // Descuentos
					             sesion_data.Lineadetalle.tarifaproducto.importe, // I. Unitario
					             sesion_data.Lineadetalle.importe,  // Importe total
					             sesiones, // Datos de las sesiones
					             sesion_data.Lineadetalle.numerobonoagencia, // Bono
					             typeof sesion_data.Lineadetalle.perfiles.Perfilvisitante=="undefined"?"":obtenerIdTarifa(sesion_data.Lineadetalle.perfiles.Perfilvisitante,sesion_data.Lineadetalle.perfilvisitante.idperfilvisitante),
					             sesion_data.Lineadetalle.descuentopromocional.iddescuentopromocional,  // iddescuento
					             sesion_data.Lineadetalle.perfilvisitante.idperfilvisitante   // idperfil
				                ]).draw();
				obtener_totales_venta_temporal("${buttonAdd}","${buttonAdd}","${idCliente}","${idTipoVenta}");


			},
			error : function(exception) {
				hideSpinner("#descuentos_div");
				actualizando= false;
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",
					  buttons: { sticker: false }				  
				   });		
			}
		});
		
}

$("#save_disponibles_button").on("click", function(e) {
	
	var sesiones= $(".sesiones").DataTable().rows( { selected: true } ).data();
	if (sesiones.length < $(".sesiones.dtr-inline").length) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	for (var i=0; i<sesiones.length; i++) 
	{
		if(sesiones[i][0].bloqueado==1)
			{
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="venta.ventareserva.list.alert.productos.seleccion.bloqueado" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
			}			
	}
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/list_datos_sesiones.do'/>",
		timeout : 100000,
		data: {
				idproducto: "${idProducto}",
				idcliente: "${idCliente}",
				idtipoventa: "${idTipoVenta}",
				idcanal: "${sessionScope.idcanal}",
				fecha: "${fecha}",
				xml: construir_xml_sesiones(sesiones)
			  }, 
		success : function(data) {
			var fechas= "", contenidos= "", disponibles= "",
				retorno= false;
			for (var i=0; i<sesiones.length; i++) {
				if (retorno) {
					fechas+="</br>";
					contenidos+= "</br>";
					disponibles+= "</br>";
				}
				fechas+= sesiones[i][2].split("-")[0],
				//contenidos+= sesiones[i][3]+" "+sesiones[i][5];
				contenidos+= sesiones[i][12];
				disponibles+= sesiones[i][8];
				retorno= true;
			}
			var bono= "";
			
			
			
			var sesion_data= data;
			
			sesion_data.Lineadetalle.cantidad = $("#cantidad").val();
			sesion_data.Lineadetalle.perfilvisitante.nombre = $("#idtarifa :selected").text();
			sesion_data.Lineadetalle.tarifaproducto.importe = Number($("#idtarifa :selected").attr("importe"));
			sesion_data.Lineadetalle.importe = calcularImporteTotal();
			sesion_data.Lineadetalle.perfilvisitante.idperfilvisitante = $("#idtarifa :selected").val();
			
			sesion_data.Lineadetalle.numerobonoagencia= $("#bono").val();
			
			sesion_data.Lineadetalle.descuentopromocional.iddescuentopromocional= $("#iddescuento").val();
			sesion_data.Lineadetalle.descuentopromocional.nombre= $("#iddescuento :selected").text();

			actualizarDescuentosDetalle(sesion_data,fechas,contenidos,disponibles,sesiones);
			
			
					},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});	

	$("#modal-dialog-form").modal('hide');
});	




//*************************************************
function calcularImporteTotal() {
	var total= Number($("#idtarifa :selected").attr("importe"))*parseInt($("#cantidad").val());
	return total.toFixed(2);	
}

//***************************************
//Botón + - 
$('.btn-number').click(function(e){
	
  e.preventDefault();
  
  fieldName = $(this).attr('data-field');
  type      = $(this).attr('data-type');
  var input = $("input[name='"+fieldName+"']");
  var currentVal = parseInt(input.val());
  if (!isNaN(currentVal)) {
      if(type == 'minus') {        	
          if(currentVal > input.attr('min')) {
              input.val(currentVal - 1).change();
            //  $("#plus").attr('disabled', false);
          } 
          if(parseInt(input.val()) == input.attr('min')) {
              $(this).attr('disabled', true);
          }

      } else if(type == 'plus') {
	
          if(currentVal < input.attr('max')) {
              input.val(currentVal + 1).change();
              //$("#minus").attr('disabled', false);                
          }
          if(parseInt(input.val()) == input.attr('max')) {
              $(this).attr('disabled', true);
          }

      }
  } else {
      input.val(0);
  }
});
//*************************************************
$('.input-number').focusin(function(){
 $(this).data('oldValue', $(this).val());
});
//*************************************************
$('.input-number').change(function() {
  
  minValue =  parseInt($(this).attr('min'));
  maxValue =  parseInt($(this).attr('max'));
  valueCurrent = parseInt($(this).val());
  
  name = $(this).attr('name');
  if(valueCurrent >= minValue) {
      $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
  } else {
      alert('El valor mínimo es 1');
      $(this).val($(this).data('oldValue'));
  }
  if(valueCurrent <= maxValue) {
      $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
  } else {
      alert('El valor máximo es 10000');
      $(this).val($(this).data('oldValue'));
  }
});
//*************************************************
$(".input-number").keydown(function (e) {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
           // Allow: Ctrl+A
          (e.keyCode == 65 && e.ctrlKey === true) || 
           // Allow: home, end, left, right
          (e.keyCode >= 35 && e.keyCode <= 39)) {
               // let it happen, don't do anything
               return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
  });
//*************************************************

	function cargarDatosInicialesEdiccion()
	{
	var datos = ${list}.rows( { selected: true }).data()[0];
	var numEntradas = datos[6];
	var perfil = datos[15];
	$("#cantidad").val(numEntradas);	
	$('#idtarifa option[value="'+perfil+'"]').attr("selected", "selected");
	}

</script>


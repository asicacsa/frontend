<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${listar_facturas}" var="listar_facturas_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="facturacion.facturas.tabs.geberacionfacturas.dialog.listado-facturas.title" />:								
	</h4>
</div>
<div class="modal-body">
	<table id="listado_facturas_generadas_table" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
			  <th id="idfactura"></th>
			  <th id="tipoventa"></th>
			  <th id="numfactura"><spring:message code="facturacion.facturas.tabs.geberacionfacturas.dialog.listado-facturas.field.numfactura"/></th>
			  <th id="cliente"><spring:message code="facturacion.facturas.tabs.geberacionfacturas.dialog.listado-facturas.field.cliente"/></th>
			  <th id="fgeneracion"><spring:message code="facturacion.facturas.tabs.geberacionfacturas.dialog.listado-facturas.field.fechageneracion" /></th>
			  <th id="importetotal"><spring:message code="facturacion.facturas.tabs.geberacionfacturas.dialog.listado-facturas.field.importe" /></th>							  						 
			  <th id="tipo"><spring:message code="facturacion.facturas.tabs.geberacionfacturas.dialog.listado-facturas.field.tipo" /></th>
			</tr>
		</thead>
		<tbody>				
			<x:forEach select="$listar_facturas_xml/ArrayList/ResultadoFacturacion" var="item">
		   		<tr class="seleccionable">
		   			<td id="idfactura"><x:out select="$item/factura/idfactura" /></td>
		   			<th id="tipoventa"><x:out select="$item/factura/ventafacturas/Ventafactura/venta/tipoventa/idtipoventa" /></th>
					<td id="numfactura" ><x:out select="$item/factura/serieynumero" /></td>
					<td id="cliente" ><x:out select="$item/factura/cliente/nombrecompleto" /></td>
					<td id="fgeneracion" ><x:out select="$item/factura/fechageneracion" /></td>
					<td id="importetotal"><x:out select="$item/factura/importetotal" /></td>		
					<td id="tipo"><x:choose><x:when select="$item/factura/esrectificativa='1'"><spring:message code="facturacion.facturas.tabs.geberacionfacturas.dialog.listado-facturas.field.rectificativa" /></x:when><x:otherwise><spring:message code="facturacion.facturas.tabs.geberacionfacturas.dialog.listado-facturas.field.original" /></x:otherwise></x:choose> </td>
				</tr>																
			</x:forEach>
		</tbody>				
	</table>			

	<div class="modal-footer">
		<button id="ver_factura_generada_button" type="button" class="btn btn-primary ver_factura_generada_dialog">
			<spring:message code="facturacion.facturas.tabs.geberacionfacturas.dialog.listado-facturas.button.ver" />
		</button>
		<button id="imprimir_factura_generada_button" type="button" class="btn btn-primary imprimir_factura_generada_dialog">
			<spring:message code="facturacion.facturas.tabs.geberacionfacturas.dialog.listado-facturas.button.imprimir" />
		</button>		
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>		

</div>

<script>
hideSpinner("#tab_generacionfacturas_generar");
//*************************************************
var dtlistadofacturasgeneradas=$('#listado_facturas_generadas_table').DataTable( {
		"scrollCollapse": true,
        "paging":         false, 	
         "info": false,
         "searching": false,
         select: { style: 'os'},
         language: dataTableLanguage,         
         "columnDefs": [
                         { "visible": false, "targets": [0,1]}
                       ]
         });

/***********************************************BOTÓN VER*************************************/
$("#ver_factura_generada_button").on("click", function(e) { 
	
	var data = dtlistadofacturasgeneradas.rows( '.selected' ).data();
		
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="facturacion.facturas.tabs.factura.list.ver.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="facturacion.facturas.tabs.factura.list.ver.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	if (data[0][1]==4){	
	window.open("../../jasper.post?idFactura="+data[0][0]+"&format=pdf&viewOnly=1&report=FacturaNormalYRectificativa","_blank");
	}else if (data[0][1]==2){
	//Si el tipo de venta es Venta de bonos prepago la llamada es:
	window.open("../../jasper.post?idFactura="+data[0][0]+"&format=pdf&viewOnly=1&report=FacturaBonoPrepagoYRectificativa","_blank");
	}
	})
	/***********************************************BOTÓN IMPRIMIR*************************************/
	$("#imprimir_factura_generada_button").on("click", function(e) { 
	var data = dtlistadofacturasgeneradas.rows( '.selected' ).data();
		
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="facturacion.facturas.tabs.factura.list.imprimir.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="facturacion.facturas.tabs.factura.list.imprimir.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	


	if (data[0][1]==4){	
		window.open("../../jasper.post?idFactura="+data[0][0]+"&format=pdf&viewOnly=0&report=FacturaNormalYRectificativa","_blank");
		}else if (data[0][1]==2){
		//Si el tipo de venta es Venta de bonos prepago la llamada es:
			window.open("../../jasper.post?idFactura="+data[0][0]+"&format=pdf&viewOnly=0&report=FacturaBonoPrepagoYRectificativa","_blank");
		}
	
	
	})
	

</script>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_cliente}" var="editar_cliente_xml" />
<x:parse xml="${editar_cliente_perodo_facturacion}" var="editar_cliente_perodo_facturacion_xml" />
<x:parse xml="${editar_cliente_tipos_cliente}" var="editar_cliente_tipos_cliente_xml" />
<x:parse xml="${editar_cliente_tipos_rappel}" var="editar_cliente_tipos_rappel_xml" />
<x:parse xml="${editar_cliente_direcciones_envio}" var="editar_cliente_direcciones_envio_xml" />
<x:parse xml="${editar_cliente_periodos_envio}" var="editar_cliente_periodos_envio_xml" />
<x:parse xml="${editar_cliente_forma_pago}" var="editar_cliente_forma_pago_xml" />
<x:parse xml="${editar_cliente_grupos_empresas}" var="editar_cliente_grupos_empresas_xml" />
<x:parse xml="${editar_cliente_canales}" var="editar_cliente_canales_xml" />

<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group" id="div_periodo_facturacion">
				<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="facturacion.facturas.tabs.clientes.field.periodoFacturacion" />*</label>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<select name="idperiodofacturacion" id="idperiodofacturacion"  class="form-control" required="required">
						<option> </option>
						<x:forEach select="$editar_cliente_perodo_facturacion_xml/ArrayList/Periodofacturacion" var="item">
							<option value="<x:out select="$item/idperiodofacturacion" />"><x:out select="$item/nombre" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="facturacion.facturas.tabs.clientes.field.tiposCliente" />*</label>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<select name="idtipocliente" id="idtipocliente"  class="form-control" required="required">
						<option> </option>
						<x:forEach select="$editar_cliente_tipos_cliente_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>		
			<div class="form-group" id="div_idtiporappel">
				<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="facturacion.facturas.tabs.clientes.field.tiposRappel" />*</label>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<select name="idtiporappel" id="idtiporappel"  class="form-control" required="required">
						<option> </option>
						<x:forEach select="$editar_cliente_tipos_rappel_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="facturacion.facturas.tabs.clientes.field.direccionFacturacion" />*</label>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<select name="iddireccionenvio" id="iddireccionenvio"  class="form-control" required="required">
						<option> </option>
						<x:forEach select="$editar_cliente_direcciones_envio_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>	
			
			<div class="form-group" id="div_idperiodoenvio">
				<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="facturacion.facturas.tabs.clientes.text.facturable_electronico" /></label>
				<div class="col-md-6 col-sm-6 col-xs-6">
						<input name="facturable_electronico" id="facturable_electronico" type="checkbox" <x:if select="$editar_cliente_xml/Cliente/facturable_electronico='1'">checked=""</x:if> class="js-switch" data-switchery="true" style="display: none;">
				</div>
			</div>
						
			<div class="form-group" id="div_facturable_electronico">
				<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="facturacion.facturas.tabs.clientes.field.periodoEnvio" />*</label>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<select name="idperiodoenvio" id="idperiodoenvio"  class="form-control" required="required">
						<option> </option>
						<x:forEach select="$editar_cliente_periodos_envio_xml/ArrayList/Periodoenvio" var="item">
							<option value="<x:out select="$item/idperiodoenvio" />"><x:out select="$item/nombre" /></option>
						</x:forEach>
					</select>
				</div>
			</div>			
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group" id="div_canal">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.clientes.field.canales" />*</label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<select name="idcanal" id=idcanal  class="form-control" required="required">
						<option> </option>
						<x:forEach select="$editar_cliente_canales_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			
			<div class="form-group" id="div_grupos_empresas">
				<div class="col-md-5 col-sm-5 col-xs-5">
					<input name="grupo" type="checkbox" <x:if select="($editar_cliente_xml/Cliente/grupoempresasactivo/idgrupoempresas > 0)">checked=""</x:if> class="js-switch" data-switchery="true" style="display: none;">
					<label class="control-label"><spring:message code="facturacion.facturas.tabs.clientes.field.grupoEmpresas" />*</label>
				</div>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<select name="idgrupoempresas" id=idgrupoempresas  class="form-control" required="required">
						<option> </option>
						<x:forEach select="$editar_cliente_grupos_empresas_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>					
				</div>
			</div>	
			
			<div class="form-group" id="div_aparecepersonaenfactura">
				<label class="control-label col-md-8 col-sm-8 col-xs-8"><spring:message code="facturacion.facturas.tabs.clientes.field.aparecepersonaenfactura" /></label>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<input name="aparecepersonaenfactura" type="checkbox" <x:if select="$editar_cliente_xml/Cliente/aparecepersonaenfactura='1'">checked=""</x:if> class="js-switch" data-switchery="true" style="display: none;">
				</div>
			</div>					

			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.clientes.field.formaPago" />*</label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<select name="idformapago" id=idformapago  class="form-control" required="required">
						<option> </option>
						<x:forEach select="$editar_cliente_forma_pago_xml/ArrayList/Formapago" var="item">
							<option value="<x:out select="$item/idformapago" />"><x:out select="$item/nombre" /></option>
						</x:forEach>					
					</select>
				</div>
			</div>
				
		
</div>


<script>

$('#idperiodofacturacion option[value="<x:out select = "$editar_cliente_xml/Cliente/periodofacturacion/idperiodofacturacion"/>"]').attr("selected", "selected");
$('#idtipocliente option[value="<x:out select = "$editar_cliente_xml/Cliente/tipoclienteactivo/idtipocliente"/>"]').attr("selected", "selected");
$('#idtiporappel option[value="<x:out select = "$editar_cliente_xml/Cliente/tiporappelactivo/idtiporappel"/>"]').attr("selected", "selected");
$('#iddireccionenvio option[value="<x:out select = "$editar_cliente_xml/Cliente/direccionenvio/iddireccionenvio"/>"]').attr("selected", "selected");
$('#idperiodoenvio option[value="<x:out select = "$editar_cliente_xml/Cliente/periodoenvio/idperiodoenvio"/>"]').attr("selected", "selected");
$('#idcanal option[value="<x:out select = "$editar_cliente_xml/Cliente/canal/idcanal"/>"]').attr("selected", "selected");
$('#idformapago option[value="<x:out select = "$editar_cliente_xml/Cliente/formapago/idformapago"/>"]').attr("selected", "selected");
$('#idgrupoempresas option[value="<x:out select = "$editar_cliente_xml/Cliente/grupoempresasactivo/idgrupoempresas"/>"]').attr("selected", "selected");


</script>

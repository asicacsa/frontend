<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_formaspago}" var="ventareserva_formaspago_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.emision_bono.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_emision_bono_dialog" data-parsley-validate="" autocomplete="off" class="form-horizontal form-label-left" novalidate="">
	
		<input id="nombre" type="hidden" value=""/>
		<input id="nif" type="hidden" value=""/>
		<input id="telefono" type="hidden" value=""/>
		<input id="email" type="hidden" value=""/>

		<div id="columna-izquierda" class="col-md-6 col-sm-12 col-xs-12">
		
			<div class="form-group button-dialog capa_cp_dialog_emision_bono">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.emision_bono.field.cp" />*</label>
				<div class="col-md-5 col-sm-5 col-xs-5">
					<!-- <button id='emision-bono-search-button' type='button' class='btn btn-default btn-i18n'>
						<span class='docs-tooltip' data-toggle='tooltip' title='' data-original-title='Buscar CP'><span class='fa fa-search'></span></span>
					</button>
					<div class="input-prepend">  -->
						<input id="cp_text" type="text" class="form-control" value=""/>
					<!-- </div> -->
				</div>
			</div>		
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.emision_bono.field.observaciones" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<textarea class="form-control" rows="3" name="observaciones" id="observaciones"></textarea>
				</div>
			</div>

			<div class="x_title">
				<h5><spring:message code="venta.ventareserva.dialog.emision_bono.field.opciones_impresion" /></h5>
				<div class="clearfix"></div>
			</div>

			<div class="form-group">
				<div class="checkbox col-md-6 col-sm-6 col-xs-6">
					<input type="checkbox" name="selector" id="oimprecibo" value="" checked class="flat" style="position: absolute; opacity: 0;">
					<strong><spring:message code="venta.ventareserva.dialog.emision_bono.field.recibo" /></strong>
				</div>
			</div>

			<div class="form-group">
				<div class="checkbox col-md-6 col-sm-6 col-xs-6">
					<input type="checkbox" name="selector" id="oimpbono" value="" checked class="flat" style="position: absolute; opacity: 0;">
					<strong><spring:message code="venta.ventareserva.dialog.emision_bono.field.bono" /></strong>
				</div>
			</div>
				
		</div>

		<div id="columna-derecha" class="col-md-6 col-sm-12 col-xs-12">

			<div class="x_title">
				<h5><spring:message code="venta.ventareserva.dialog.emision_bono.field.forma_pago" /></h5>
				<div class="clearfix"></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 forma-pago">
   				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
			            <label class="control-label col-md-4 col-sm-4 col-xs-4">
			            	<spring:message code="venta.ventareserva.dialog.emision_bono.field.imp_cobrar" />                               
			            </label>
			            <label class="control-label col-md-8 col-sm-8 col-xs-8 euros">
							<span class="total-lbl"><span id="total-cobrar">0.00</span>&euro;</span>		
			            </label>
		            </div>
		        </div>
   				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
	                   <label class="control-label col-md-4 col-sm-4 col-xs-12">
                       		<spring:message code="venta.ventareserva.dialog.emision_bono.field.imp_entregado" />                               
                       </label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<div class="input-prepend">
								<input name="importe1_text" id="importe1_text" type="text" class="form-control importe" disabled="disabled">
							</div>
							<span class="importe">&euro;</span>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12">
	                       <select name="importe1" id="selector_importe1" class="form-control"  disabled="disabled">
								<option value=""></option>
								<x:forEach select="$ventareserva_formaspago_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
					       </select>
						</div>                       
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
	                   <label class="control-label col-md-4 col-sm-4 col-xs-12">
                       		<spring:message code="venta.ventareserva.dialog.emision_bono.field.imp_entregado" />                               
                       </label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input name="importe2_text" id="importe2_text" type="text" class="form-control importe" disabled="disabled">
							<span class="importe">&euro;</span>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12">
	                       <select name="importe2" id="selector_importe2" class="form-control"  disabled="disabled">
								<option value=""></option>
								<x:forEach select="$ventareserva_formaspago_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
					       </select>
						</div>                       
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
	                   <label class="control-label col-md-4 col-sm-4 col-xs-12">
                       		<spring:message code="venta.ventareserva.dialog.emision_bono.field.imp_entregado" />                               
                       </label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input name="importe3_text" id="importe3_text" type="text" class="form-control importe" disabled="disabled">
							<span class="importe">&euro;</span>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12">
	                       <select name="importe3" id="selector_importe3" class="form-control"  disabled="disabled">
								<option value=""></option>
								<x:forEach select="$ventareserva_formaspago_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
					       </select>
						</div>                       
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
			            <label class="control-label col-md-4 col-sm-4 col-xs-4">
			            	<spring:message code="venta.ventareserva.dialog.emision_bono.field.imp_devolver" />                               
			            </label>
			            <label class="control-label col-md-8 col-sm-8 col-xs-8 euros">
							<span class="total-lbl"><span id="total-devolver">0.00</span>&euro;</span>		
			            </label>
		            </div>
		        </div>
				
			</div>

		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_emision_bono_button" type="button" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
		
	</form>

</div>


<script>

hideSpinner("#button_emision_bono_vender");

if ($("input.flat")[0]) {
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });
}

var importe_total=Number($(".totales_emision_bono #total-val").text()),
	importe_devolver= -importe_total,
	importe_pendiente=0,
	idpais= "472", // Se establece España por defecto ya que no se solicita en el diálogo
	tipo_bono= comprobarTipoBono();

function calcularPendiente() {
	var suma= Number(isNaN($("#importe1_text").val())?"0":$("#importe1_text").val())+Number(isNaN($("#importe2_text").val())?"0":$("#importe2_text").val())+Number(isNaN($("#importe3_text").val())?"0":$("#importe3_text").val());
	importe_pendiente= importe_total-suma;
	return;	
}

function calcularDevolver() {
	var suma= Number(isNaN($("#importe1_text").val())?"0":$("#importe1_text").val())+Number(isNaN($("#importe2_text").val())?"0":$("#importe2_text").val())+Number(isNaN($("#importe3_text").val())?"0":$("#importe3_text").val());
	importe_devolver= suma-importe_total;
	$("#total-devolver").text(importe_devolver.toFixed(2));
	return;	
}

/* Se agrupan las funciones relativas a la modificación de los importes ya que son comunes 
 * para las tres.
 */

function selectorImporteChange(nombre) {

	$("#selector_"+nombre).on("change", function(e) {
		$("#"+nombre+"_text").val("");
		calcularPendiente();
		if ($("#selector_"+nombre+" :selected").text()=="") {
			$('input[name="'+nombre+'_text"]').val("");
	    	$('input[name="'+nombre+'_text"]').attr('disabled', 'disabled');
		} else {
			$('input[name="'+nombre+'_text"]').removeAttr('disabled');
	    	$("#"+nombre+"_text").val(importe_pendiente.toFixed(2));
		}
		calcularDevolver();
	});

	$("#"+nombre+"_text").on("change", function(e) {
		calcularDevolver();	
	});

	$("#"+nombre+"_text").on("focusout", function(e) {
		var valor= Number($("#"+nombre+"_text").val());
		$("#"+nombre+"_text").val(valor.toFixed(2));
	});
	
}

selectorImporteChange("importe1");
selectorImporteChange("importe2");
selectorImporteChange("importe3");

if ($("#idcliente_emision_bono").val()!="") {
	$("#cp_text").val($("#cpcliente_emision_bono").val());
	$("#nombre").val($("#cliente_emision_bono").val());
	$("#nif").val($("#cifcliente_emision_bono").val());
	$("#telefono").val($("#telefonocliente_emision_bono").val());
	$("#email").val($("#emailcliente_emision_bono").val());
}

$("#total-cobrar").text($(".totales_emision_bono #total-val").text());

if (tipo_bono==1) { // Bono Crédito. Se deshabilitan las formas de pago
	$("#importe1_text").val($(".totales_emision_bono #total-val").text());
}
else {
	$('#selector_importe1').removeAttr('disabled');
	$('#selector_importe2').removeAttr('disabled');
	$('#selector_importe3').removeAttr('disabled');
}

//********************************************************************************
function construir_xml_emision_bono() {
	var imp_recibo= ($("#oimprecibo").is(":checked"))?"true":"false",
		imp_bono= ($("#oimpbono").is(":checked"))?"true":"false",
		xml_lineasdetalle= "<Venta>";
	xml_lineasdetalle+="<canalByCanalventa>";
	
	
	
	//El día 14 de Junio se comenta el canal porquieda problemas y se pasa el canl vacio
	//xml_lineasdetalle+="<idcanal>${sessionScope.idcanal}</idcanal>";
	
	xml_lineasdetalle+="<idcanal></idcanal>";
	
	
	xml_lineasdetalle+="</canalByCanalventa>";
	xml_lineasdetalle+="<cliente>";
	xml_lineasdetalle+="<idcliente>"+$("#idcliente_emision_bono").val()+"</idcliente>";
	xml_lineasdetalle+="</cliente>";
	xml_lineasdetalle+="<cp>"+$("#cp_text").val()+"</cp>";
	xml_lineasdetalle+="<pais>";
	xml_lineasdetalle+="<idpais>"+idpais+"</idpais>";
	xml_lineasdetalle+="</pais>";
	xml_lineasdetalle+="<financiada>false</financiada>";
	xml_lineasdetalle+="<dadodebaja>0</dadodebaja>";
	xml_lineasdetalle+="<email>"+$("#email").val()+"</email>";
	xml_lineasdetalle+="<entradasimpresas>"+imp_bono+"</entradasimpresas>";
	xml_lineasdetalle+="<imprimirSoloEntradasXDefecto></imprimirSoloEntradasXDefecto>";
	xml_lineasdetalle+="<isBono>false</isBono>";
	xml_lineasdetalle+="<importeparcials>"; 
	xml_lineasdetalle+="<Importeparcial>";
	xml_lineasdetalle+="<formapago>";
	xml_lineasdetalle+="<idformapago>"+$("#selector_importe1").val()+"</idformapago>";
	xml_lineasdetalle+="</formapago>";
	xml_lineasdetalle+="<importe>"+$("#importe1_text").val()+"</importe>";
	xml_lineasdetalle+="</Importeparcial>";
	xml_lineasdetalle+="<Importeparcial>";
	xml_lineasdetalle+="<formapago>";
	xml_lineasdetalle+="<idformapago>"+$("#selector_importe2").val()+"</idformapago>";
	xml_lineasdetalle+="</formapago>";
	xml_lineasdetalle+="<importe>"+$("#importe2_text").val()+"</importe>";
	xml_lineasdetalle+="</Importeparcial>";
	xml_lineasdetalle+="<Importeparcial>";
	xml_lineasdetalle+="<formapago>";
	xml_lineasdetalle+="<idformapago>"+$("#selector_importe3").val()+"</idformapago>";
	xml_lineasdetalle+="</formapago>";
	xml_lineasdetalle+="<importe>"+$("#importe3_text").val()+"</importe>";
	xml_lineasdetalle+="</Importeparcial>";
	xml_lineasdetalle+="</importeparcials>";
	xml_lineasdetalle+="<importetotalventa>"+importe_total+"</importetotalventa>";
	xml_lineasdetalle+="<incidencias/>";
	xml_lineasdetalle+="<infocomplementarias/>";
	xml_lineasdetalle+= construir_xml_lineasdetalle($("#datatable_list_emision_bono").DataTable().rows().data());
	xml_lineasdetalle+="<modificacions/>";
	xml_lineasdetalle+= "<reciboimpreso>"+imp_recibo+"</reciboimpreso>";
	xml_lineasdetalle+= "<nif>"+$("#nif").val()+"</nif>";
	xml_lineasdetalle+= "<nombre>"+$("#nombre").val()+"</nombre>";
	xml_lineasdetalle+= "<observaciones>"+$("#observaciones").val()+"</observaciones>";
	xml_lineasdetalle+= "<telefono>"+$("#telefono").val()+"</telefono>";
	xml_lineasdetalle+= "<telefonomovil></telefonomovil>";
	xml_lineasdetalle+= "<usuario/>";
	xml_lineasdetalle+="</Venta>";
	return(xml_lineasdetalle);
}

//********************************************************************************	
function saveFormEmisionBono() {
	showSpinner("#modal-dialog-form .modal-content");

	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/realizar_emision_bono.do'/>",
		timeout : 100000,
		data: {
			xml: construir_xml_emision_bono()
		},
		success : function(data) {
			hideSpinner("#modal-dialog-form .modal-content");
			$("#modal-dialog-form").modal('hide');
			showSpinner("#main-ventareserva");
			
			var idVenta = data.ok.int;
			console.log("idVenta="+idVenta);
			$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_resumen_emision_bono.do?idventa="+idVenta+"'/>", function() {
				$("#modal-dialog-form-2").modal('show');
				setModalDialogSize("#modal-dialog-form-2", "md");
			});
			
			if ($("#oimpbono").is(":checked"))
				imprimir(idVenta);
		},
		error : function(exception) {
			hideSpinner("#modal-dialog-form .modal-content");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});		
}	

function imprimir(idVenta)
{
	
	var xml="<int>"+idVenta+"</int>";	
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/imprimirBonosVenta.do'/>",
		timeout : 100000,
		data: {
			xmlImpresion: xml
		},
		success : function(data) {},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });
		}
	});
}

$("#save_emision_bono_button").on("click", function(e) {
	$("#form_emision_bono_dialog").submit();
})

$("#form_emision_bono_dialog").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
      $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		saveFormEmisionBono();		
	}
});


</script>

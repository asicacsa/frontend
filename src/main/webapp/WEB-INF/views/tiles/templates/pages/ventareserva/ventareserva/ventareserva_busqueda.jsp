<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!-- Pestañas ------------------------------------------------------------->
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 maintab">
		<div class="" role="tabpanel" data-example-id="togglable-tabs">
			<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
				<li role="presentation" class="tabventas active"><a href="#tab_venta" id="venta-tab" role="tab" data-toggle="tab" aria-expanded="true"><spring:message code="venta.ventareserva.busqueda.tabs.venta.title" /></a></li>
				<li role="presentation" class="tabbonos"><a href="#tab_bono" id="bono-tab" role="tab" data-toggle="tab" aria-expanded="true"><spring:message code="venta.ventareserva.busqueda.tabs.bonos.title" /></a></li>
				<li role="presentation" class="tabventasBonos"><a href="#tab_venta_bono" role="tab" id="venta_bono-tab" data-toggle="tab" aria-expanded="false"><spring:message code="venta.ventareserva.busqueda.tabs.venta_bonos.title" /></a></li>
				<li role="presentation" class="tabentradas"><a href="#tab_entradas" role="tab" id="entradas-tab" data-toggle="tab" aria-expanded="false"><spring:message code="venta.ventareserva.busqueda.tabs.entradas.title" /></a></li>
				<li role="presentation" class="tababonos"><a href="#tab_venta_abonos" role="tab" id="venta_abonos-tab" data-toggle="tab" aria-expanded="false"><spring:message code="venta.ventareserva.busqueda.tabs.venta_abonos.title" /></a></li>
			</ul>
		</div>
	</div>
</div>

<!-- Area de trabajo ------------------------------------------------------>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div id = "main-busquedas" class="x_panel thin_padding">

			<div id="myTabContent" class="tab-content">
				<div role="tabpanel" class="tab-pane fade active in" id="tab_venta" aria-labelledby="venta-tab">
					<tiles:insertAttribute name="tab_venta_reserva" />
				</div>

				<div role="tabpanel" class="tab-pane fade" id="tab_bono" aria-labelledby="bono-tab">
					<tiles:insertAttribute name="tab_bono" />
				</div>

				<div role="tabpanel" class="tab-pane fade" id="tab_venta_bono" aria-labelledby="vventa_bono-tab">
					<tiles:insertAttribute name="tab_venta_bono" />
				</div>

				<div role="tabpanel" class="tab-pane fade" id="tab_entradas" aria-labelledby="entradas-tab">
					<tiles:insertAttribute name="tab_entradas" />
				</div>

				<div role="tabpanel" class="tab-pane fade" id="tab_venta_abonos" aria-labelledby="venta_abonos-tab">
					<tiles:insertAttribute name="tab_venta_abonos" />
				</div>


			</div>

			<div class="clearfix"></div>
			
		
		</div>
	</div>
</div>

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-2"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-3"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-4"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-buscar-cliente"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-editar-cliente"/>
<tiles:insertAttribute name="modal_dialog" />

<tiles:insertAttribute name="motivos_bonos" />

<tiles:insertAttribute name="anulacion_entradas" />

<script>
var url_validacion="${url_validacion}";


//******************CARGAR SELECTOR IDIOMAS------------------------
function cargarSelectorIdiomas(nombre_selector){
	var i18nlangs= ${sessionScope.i18nlangs};
	for (var i = 0; i < i18nlangs.ArrayList.LabelValue.length; i++){
	   var obj = i18nlangs.ArrayList.LabelValue[i];
	   $("#"+nombre_selector).append('<option value='+obj.value+'>'+obj.label+'</option>');		
	}
}
</script>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!--<x:parse xml="${editarusuario_datos_usuario}" var="editarusuario_datos_usuario_xml" />
<x:parse xml="${editarusuario_selector_canal}" var="editarusuario_selector_canal_xml" />-->


<div class="modal-body">

	<!-- <div class="x_content" style="display: block;">
						<div class="">
							<div class="x_content">
								<div class="">
									<x:forEach var="zona"
										select="$tabzonas_data_xml/ArrayList/Zona">

										<ul class="to_do">
											<li>

												<div class="icheckbox_flat-green"
													style="position: relative;">
													<input type="checkbox" class="flat"
														style="position: absolute; opacity: 0;" value="idzona">
												</div> <x:out select="recinto/nombre" />-<x:out select="nombre" />

											</li>

										</ul>
									</x:forEach>
								</div>
							</div>
						</div>
					</div> -->



	<div class="modal-footer">

		<button type="button" class="btn btn-primary save_dialog" data-dismiss="modal">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>

</div>


<script>
	
</script>

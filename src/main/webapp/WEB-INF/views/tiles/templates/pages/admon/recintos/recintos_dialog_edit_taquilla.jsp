<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_taquilla}" var="editar_taquilla_xml" />
<x:parse xml="${editartaquilla_selector_recintos}" var="editartaquilla_selector_recintos_xml" />
<x:parse xml="${editartaquilla_selector_ubicaciones}" var="editartaquilla_selector_ubicaciones_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">
		<x:choose>
			<x:when select="not($editar_taquilla_xml/Taquilla)">
				<spring:message code="administracion.recintos.tabs.taquillas.dialog.crear_taquilla.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.recintos.tabs.taquillas.dialog.editar_taquilla.title" />
			</x:otherwise>
		</x:choose>
	</h4>	
</div>

<div class="modal-body">
	<form id="form_taquilla_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	    <input id="idTaquilla" name="idtaquilla" type="hidden" value="<x:out select = "$editar_taquilla_xml/Taquilla/idtaquilla" />" />
	    
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.recintos.tabs.taquilla.field.nombre" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="nombre" id="nombre" required="required" type="text" class="form-control required" required="required" value="<x:out select = "$editar_taquilla_xml/Taquilla/nombre" />">
				</div>
			</div>		
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.recintos.tabs.taquilla.field.recinto" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="idrecinto" id="selector_recinto" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$editartaquilla_selector_recintos_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>		
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.recintos.tabs.taquilla.field.ubicacion" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="idubicacion" id="selector_ubicacion" class="form-control" >
						<option value=""></option>
						<x:forEach select="$editartaquilla_selector_ubicaciones_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>	
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.recintos.tabs.taquilla.field.ip" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="dirip" id="ip"  type="text" class="form-control"  value="<x:out select = "$editar_taquilla_xml/Taquilla/dirip" />">
				</div>
			</div>
			
			<div class="color_form_edit">
				<br/>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.recintos.tabs.taquilla.field.configdatafono" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="configdatafono" id="configdatafono"  type="text" class="form-control"  value="<x:out select = "$editar_taquilla_xml/Taquilla/configdatafono" />">
						<span class="field_comment"><spring:message code="administracion.recintos.tabs.taquilla.field.configdatafono.text"/></span>
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.recintos.tabs.taquilla.field.terminaldatafono" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="terminaldatafono" id="terminaldatafono"  type="text" class="form-control"  value="<x:out select = "$editar_taquilla_xml/Taquilla/terminaldatafono" />">
					</div>
				</div>
			</div>
	</form>

	<div class="modal-footer">
		<button id="save_taquilla_button" type="button" class="btn btn-primary save_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>

</div>
<script>

hideSpinner("#tab_taquillas_new");
hideSpinner("#tab_taquillas_edit");
//**********************************************
	$('#selector_recinto option[value="<x:out select = "$editar_taquilla_xml/Taquilla/recinto/idrecinto" />"]').attr("selected", "selected");
	$('#selector_ubicacion option[value="<x:out select = "$editar_taquilla_xml/Taquilla/ubicacion/idubicacion" />"]').attr("selected", "selected");
	
	$("#save_taquilla_button").on("click", function(e) {
		$("#form_taquilla_data").submit();	
	})



	$("#form_taquilla_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFormData();
		}
	}
	);

	//********************************************************************************	
	function saveFormData() 
	{
		var data = $("#form_taquilla_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/recintos/taquilla/save_taquilla.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				modificadoPuntoRecogida = true;
				
				$("#modal-dialog-form").modal('hide');
				
				dt_listtaquillas.ajax.reload(null,false);
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});		
	}

</script>
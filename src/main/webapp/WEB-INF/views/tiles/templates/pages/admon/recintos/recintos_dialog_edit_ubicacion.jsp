<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_ubicacion}" var="editar_ubicacion_xml" />
<x:parse xml="${editarubicacion_selector_taquillas}" var="editarubicacion_selector_taquillas_xml" />



<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">
		<x:choose>
			<x:when select="not($editar_ubicacion_xml/Ubicacion)">
				<spring:message code="administracion.recintos.tabs.ubicaciones.dialog.crear_ubicacion.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.recintos.tabs.ubicaciones.dialog.editar_ubicacion.title" />
			</x:otherwise>
		</x:choose>
	</h4>	
</div>

<div class="modal-body">
	<form id="form_ubicacion_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	    <input id="idubicacion" name="idubicacion" type="hidden" value="<x:out select = "$editar_ubicacion_xml/Ubicacion/idubicacion" />" />
	    
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.recintos.tabs.ubicaciones.field.nombre" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="nombre" id="nombre" type="text" class="form-control required" required="required" value="<x:out select = "$editar_ubicacion_xml/Ubicacion/nombre" />">
				</div>
			</div>	
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.recintos.tabs.ubicaciones.field.descripcion" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<textarea name="descripcion" id="descripcion" class="form-control"  ><x:out select = "$editar_ubicacion_xml/Ubicacion/descripcion" /></textarea>
				</div>
			</div>	
 
 			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.recintos.tabs.ubicaciones.field.taquillas" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="taquillas[]" id="selector_taquillas" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
						<x:forEach select="$editarubicacion_selector_taquillas_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
 
 
	</form>
	
	<div class="modal-footer">

		<button id="save_ubicacion_button" type="button" class="btn btn-primary save_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>	
</div>	

<script>

hideSpinner("#tab_ubicaciones_new");
hideSpinner("#tab_ubicaciones_edit");
//*****************************************
	$(".select-select2").select2({
		language : "es",
		containerCssClass : "single-line"
	});

	$(".js-switch").each(function(index) {
		var s = new Switchery(this, {
			size : 'small'
		});
	});
	
	
	var selectedValues = new Array();
	<x:forEach select="$editar_ubicacion_xml/Ubicacion/taquillas/Taquilla" var="item">
	selectedValues.push('<x:out select="$item/idtaquilla" />');
	</x:forEach>
	
	
	$('#selector_taquillas').val(selectedValues);
	$('#selector_taquillas').trigger('change.select2');
	
	
	$("#save_ubicacion_button").on("click", function(e) {
		$("#form_ubicacion_data").submit();	
	})
	
	$("#form_ubicacion_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFormData();
		}
	}
	);
	
	
	//********************************************************************************	
	function saveFormData() 
	{
		var data = $("#form_ubicacion_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/recintos/ubicacion/save_ubicacion.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				modificadoPuntoRecogida = true;
				
				$("#modal-dialog-form").modal('hide');
				
				dt_listubicaciones.ajax.reload(null,false);
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});

	}
	
	
</script>
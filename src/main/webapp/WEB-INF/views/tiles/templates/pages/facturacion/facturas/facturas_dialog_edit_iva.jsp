<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_iva}" var="editar_iva_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	 <h4 class="modal-title">	
		<x:choose>
			<x:when select="not($editar_iva_xml/Iva)">
				<spring:message code="facturacion.facturas.tabs.iva.dialog.crear_iva.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="facturacion.facturas.tabs.iva.dialog.editar_iva.title" />
			</x:otherwise>
		</x:choose>		
	</h4>	
</div>


<div class="modal-body">
	<form id="form_iva_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input id="idiva" name="idiva" type="hidden" value="<x:out select = "$editar_iva_xml/Iva/idiva" />" />
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="form-group">
			<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="facturacion.facturas.tabs.iva.field.codigosap" />*</label>
			<div class="col-md-7 col-sm-7 col-xs-7">
				<input name="codigosap" id="codigosap" type="text" class="form-control" required="required" value="<x:out select = "$editar_iva_xml/Iva/codigosap" />">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="facturacion.facturas.tabs.iva.field.nombre" />*</label>
			<div class="col-md-7 col-sm-7 col-xs-7">
				<input name="nombreiva" id="nombreiva" type="text" class="form-control" required="required" value="<x:out select = "$editar_iva_xml/Iva/nombre" />">
			</div>
		</div>			
		<div class="form-group">
			<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="facturacion.facturas.tabs.iva.field.porcentaje" />*</label>
			<div class="col-md-5 col-sm-5 col-xs-5">	
				<input name="porcentaje" id="porcentaje" data-inputmask="'mask': '9{0,10}.9{0,2}'" data-mask required="required" class="form-control"  value="<x:out select = "$editar_iva_xml/Iva/porcentaje" />"/>	
			</div>
			<div class="col-md-2 col-sm-2 col-xs-2">	
				<label class="control-label col-md-3 col-sm-3 col-xs-3">%</label>
			</div>				
		</div>							
	</div>	
	</form>
	<div class="modal-footer">
		<button id="save_iva_button" type="button" class="btn btn-primary save_iva_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>	

<script>
hideSpinner("#tab_iva_new");
hideSpinner("#tab_iva_edit");
//*********************************************
$(":input").inputmask();
//*********************************************
$(".save_iva_dialog").on("click", function(e) {
	$("#form_iva_data").submit();	
})
//*********************************************		
$("#form_iva_data").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		guardarIVA()
	}
})
//********************************************
	function guardarIVA()
	{
		var data = $("#form_iva_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/facturacion/facturas/iva/save_iva.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				
				$("#modal-dialog-form").modal('hide');
				dt_listiva.ajax.reload(null,false);
								
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});

	}

</script>

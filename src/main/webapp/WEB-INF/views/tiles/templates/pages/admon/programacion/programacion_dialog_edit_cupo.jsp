<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editarcupo_datos_cupo}" var="editarcupo_datos_cupo_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="administracion.programacion.programacion.dialog.editar_cupos.title" />
	</h4>	
</div>

<div class="modal-body">
	<form id="form_editarcupo_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	    <input id="idcanal" name="idcanal" type="hidden" value="${idCanal}" />
	    
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.programacion.dialog.editar_cupos.canal" />*</label>
			<div class="col-md-9 col-sm-9 col-xs-9">
				<select name="selector_canal" id="selector_canal" class="form-control" required="required">
					<option value=""></option>
					<x:forEach select="$editarcupo_datos_cupo_xml/ArrayList/LabelValue" var="item">
						<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
					</x:forEach>
				</select>
			</div>
		</div>
			    
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.programacion.dialog.editar_cupos.cupo" /></label>
			<div class="col-md-9 col-sm-9 col-xs-9">
				<input name="cupo" id="cupo" type="text" class="form-control" value="">
			</div>
		</div>
			
	</form>

	<div class="modal-footer">
		<button id="save_editarcupo_button" type="button" class="btn btn-primary save_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>

</div>

<script>
hideSpinner("#button_cupos_new");
hideSpinner("#button_cupos_edit");

var idCanal = $("#idcanal").val();

if(idCanal!='')	{
		var datos_cupo= dt_listcupos.rows( '.selected' ).data()[0];
		$('#selector_canal option[value="'+datos_cupo[0]+'"]').attr("selected", "true");
		$("#cupo").val(datos_cupo[2]);
}

$("#save_editarcupo_button").on("click", function(e) {
	$("#form_editarcupo_data").submit();
});

$("#form_editarcupo_data").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		saveCupoData();		
	}
});

function saveCupoData() {
	if(idCanal!='')	dt_listcupos.rows('.selected').remove();
	
	var valor_cupo= $("#cupo").val();
	if (valor_cupo=="") valor_cupo= "0";
	dt_listcupos.row.add([$("#selector_canal option:selected").val(), $("#selector_canal option:selected").text(), valor_cupo]).draw();
	$("#modal-dialog-form-2").modal('hide');
};

</script>
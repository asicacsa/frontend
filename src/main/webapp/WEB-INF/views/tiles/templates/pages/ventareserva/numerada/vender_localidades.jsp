<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>



<div class="modal-body">
	<form id="form_selector_venta_individual" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	 	<input type="hidden" id="cifcliente_venta_individual" value=""/>
		<input type="hidden" id="cpcliente_venta_individual" value=""/>
		<input type="hidden" id="emailcliente_venta_individual" value=""/>
		<input type="hidden" id="telefonocliente_venta_individual" value=""/>
		<input type="hidden" id="telefonomovilcliente_venta_individual" value=""/>
	 	<div class="col-md-12 col-sm-12 col-xs-12">
		 	<div class="col-md-5 col-sm-5 col-xs-12">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.emision_bono.field.cliente" /></label>
						<div class="col-md-2 col-sm-2 col-xs-12">
							<input name="idcliente_venta_individual" id="idcliente_venta_individual" style="height:10" type="text" class="form-control">
						</div>					
						<div class="col-md-7 col-sm-7 col-xs-12">
							<input name="cliente_venta_individual" id="cliente_venta_individual" type="text" class="form-control" readonly>
						</div>
					</div>
			</div>
		
	 
	 
		  <div class="btn-group pull-right btn-datatable">
			<a type="button" class="btn btn-info" id="button_venta_individual_vender">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.venta_individual.list.button.vender" />"> <span class="fa fa-shopping-cart"></span>
				</span>
			</a>			
			<a type="button" class="btn btn-info" id="button_venta_individual_cancelar">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.venta_individual.list.button.cancelar" />"> <span class="fa fa-close"></span>
				</span>
			</a>			
		  </div>
	  </div>
	  <table id="datatable-list-lineas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th></th>
				<th><spring:message code="venta.numerada.tabs.lineas.list.contenido" /></th>
				<th><spring:message code="venta.numerada.tabs.lineas.list.fila" /></th>
				<th><spring:message code="venta.numerada.tabs.lineas.list.butaca" /></th>
				<th><spring:message code="venta.numerada.tabs.lineas.list.zona" /></th>
				<th><spring:message code="venta.numerada.tabs.lineas.list.perfil" /></th>
				<th><spring:message code="venta.numerada.tabs.lineas.list.descuento" /></th>
				<th><spring:message code="venta.numerada.tabs.lineas.list.fecha" /></th>
				<th><spring:message code="venta.numerada.tabs.lineas.list.hora" /></th>
				<th><spring:message code="venta.numerada.tabs.lineas.list.importe" /></th>				
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	</form>
</div>

<script>

$("#cliente_venta_individual").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>?editable=1", "#cliente_venta_individual", "#idcliente_venta_individual", "#cifcliente_venta_individual", "#cpcliente_venta_individual", "#emailcliente_venta_individual", "#telefonocliente_venta_individual");

//********************************************************************************
var dt_listlineas=$('#datatable-list-lineas').DataTable( {
	searching: false,
	ordering: false,
	deferLoading: 0,
	pageLength: 50,
	lengthChange: false,
	pagingType: "simple",
	info: false,
    select: { style: 'os'},
	language: dataTableLanguage,
	 initComplete: function( settings, json ) {
    	 window.setTimeout(CargarLineasDetalle, 100);  
	 },	
	"columnDefs": [
                   { "visible": false, "targets": [0]}
                 ]
} );

var json= ${localidades};

function CargarLineasDetalle()
{
	var lineasdetalle = json.ArrayList;
	
	if (lineasdetalle !=""){
	if (lineasdetalle.Lineadetalle!="") {
        var item=lineasdetalle.Lineadetalle;
        if (item.length>0)
            $.each(item, function(key, lineadetalle){
            	CargarLineaDetalle(lineadetalle,dt_listlineas)
            });
        else
        	CargarLineaDetalle(item,dt_listlineas)
}
}
}

//*****************************************************************************************
function CargarLineaDetalle(lineadetalle, tabla)
{	
	var descuento_nombre="";
	var perfilvisitante_nombre="";
	
	
	tabla.row.add([
				   lineadetalle,
				   lineadetalle.producto.nombre, 
				   lineadetalle.estadolocalidads.Estadolocalidad.localidad.fila,         
				   lineadetalle.estadolocalidads.Estadolocalidad.localidad.butaca, 
				   lineadetalle.estadolocalidads.Estadolocalidad.localidad.zona.nombre,
				   perfilvisitante_nombre,
				   descuento_nombre, 				    
				   lineadetalle.estadolocalidads.Estadolocalidad.sesion.fecha.split("-")[0],
				   lineadetalle.estadolocalidads.Estadolocalidad.sesion.horainicio, 
				   lineadetalle.importe				    
				]).draw();
}

//**********************************************
var celda;
$("#datatable-list-lineas tbody").delegate("td", "click", function() {
	celda=this;
	var posicionCelda=0;    	
	posicionCelda=$("td", celda).context.cellIndex;
	if ( posicionCelda=="4" || posicionCelda=="5")    		
		window.setTimeout(editarLineaDetalleVentaIndividual, 100);	
});   




function editarLineaDetalleVentaIndividual()
{
	
	if (dt_listlineas.rows( { selected: true }).data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	if (dt_listlineas.rows( { selected: true }).data().length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	var rowdata= dt_listlineas.rows( { selected: true }).data()[0][0];
	
	var idProducto= rowdata.producto.idproducto;
	var idCanal="${sessionScope.idcanal}";
	var idSesion=  rowdata.estadolocalidads.Estadolocalidad.sesion.idsesion;
	var idPerfil = rowdata.perfilvisitante.idperfilvisitante;
	var iddescuento = rowdata.descuentopromocional.iddescuentopromocional;
	
		
	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_edit_sesion_numerada.do'/>?idSesion="+idSesion+"&idproducto="+idProducto+"&idcanal=${sessionScope.idcanal}&idperfil="+idPerfil+"&iddescuento="+iddescuento+"&list=dt_listlineas", function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "xs");
	});
}

$("#button_venta_individual_cancelar").on("click", function(e) {
	//cambiarZona();
	$("#modal-dialog-form-2").modal('hide');
	$("#modal-dialog-form-1").modal('hide');
})


//********************************************************************************
$("#button_venta_individual_vender").on("click", function(e) {
	
	if (dt_listlineas.rows().data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.venta_vacia" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	showButtonSpinner("#button_venta_individual_vender");	

	var data = $("#form_selector_venta_individual").serializeObject();

	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/venta/numerada/show_vender_venta.do'/>", function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "md");
	});
});

//******************CARGAR SELECTOR IDIOMAS------------------------
function cargarSelectorIdiomas(nombre_selector){
	var i18nlangs= ${sessionScope.i18nlangs};
	for (var i = 0; i < i18nlangs.ArrayList.LabelValue.length; i++){
	   var obj = i18nlangs.ArrayList.LabelValue[i];	   
	   $("#"+nombre_selector).append('<option value='+obj.value+'>'+obj.label+'</option>');		
	}
}

//****************************************************
$("#idcliente_venta_individual").blur(function() {
	var idCliente =""+$("#idcliente_venta_individual").val(); 
	
	if(idCliente!="")
	{
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/facturacion/buscarClientePorId.do'/>",
		timeout : 100000,
		data: {
			idcliente: idCliente,    			
		},
		success : function(data) {    		     
				$("#cliente_venta_individual").val(data.Cliente.nombrecompleto); 
				$("#cifcliente_venta_individual").val(data.Cliente.identificador); 
				$("#cpcliente_venta_individual").val(data.Cliente.direccionorigen.cp.cp); 
				$("#emailcliente_venta_individual").val(data.Cliente.direccionorigen.correoelectronico); 
				$("#telefonocliente_venta_individual").val(data.Cliente.direccionorigen.telefonofijo); 
				$("#telefonomovilcliente_venta_individual").val(data.Cliente.direccionorigen.telefonomovil);
				
				
		},
		error : function(exception) {    	
			$("#idcliente_venta_individual").val("");
	    	$("#cliente_venta_individual").val("");
	    	$("#cpcliente_venta_individual").val(""); 
			$("#emailcliente_venta_individual").val(""); 
			$("#telefonocliente_venta_individual").val(""); 
			$("#telefonomovilcliente_venta_individual").val("");

			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});
	}
})


</script>

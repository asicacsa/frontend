<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_unidad_negocio}" var="editar_unidad_negocio_xml" />
<x:parse xml="${editarunidad_selector_entidadGestora}" var="editarunidad_selector_entidadGestora_xml" />
<x:parse xml="${editarunidad_selector_redes}" var="editarunidad_selector_redes_xml" />



<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">
		<x:choose>
			<x:when select="not($editar_unidad_negocio_xml/Unidadnegocio)">
				<spring:message code="administracion.recintos.tabs.unidades.dialog.crear_unidad.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.recintos.tabs.unidades.dialog.editar_unidad.title" />
			</x:otherwise>
		</x:choose>
	</h4>	
</div>

<div class="modal-body">
	<form id="form_unidad_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	    <input id="idunidadnegocio" name="idunidadnegocio" type="hidden" value="<x:out select = "$editar_unidad_negocio_xml/Unidadnegocio/idunidadnegocio" />" />
	    
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.recintos.tabs.unidades.field.nombre" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="nombre" id="nombre" type="text" class="form-control required" required="required" value="<x:out select = "$editar_unidad_negocio_xml/Unidadnegocio/nombre" />">
				</div>
			</div>	
		
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.recintos.tabs.unidades.field.entidad_gestora" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="idEntidadGestora" id="selector_entidad_gestora" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$editarunidad_selector_entidadGestora_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.recintos.tabs.unidades.field.orden" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="orden" id="orden" data-inputmask="'mask': '9{0,10}'" data-mask type="text" class="form-control"  value="<x:out select = "$editar_unidad_negocio_xml/Unidadnegocio/orden" />">
				</div>
			</div>	
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.mostrar" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="mostrar" type="checkbox" class="js-switch" <x:if select="$editar_unidad_negocio_xml/Unidadnegocio/mostrar=1">checked=""</x:if> data-switchery="true" style="display: none;">
				</div>
			</div>	
			
			<div class="form-group" id="div_recintos">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.recintos.tabs.unidades.field.recintos" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="recintos[]" id="selector_recintos" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
					</select>
				</div>
			</div>	
			
	  		<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.recintos.tabs.unidades.field.redes" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="redes[]" id="selector_redes" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%"></select>
				</div>
			</div>	
			
			<div class="form-group" id="div_usuarios">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.recintos.tabs.unidades.field.usuarios" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="usuarios[]" id="selector_usuarios" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
					</select>
				</div>
			</div>	
			
  		
	</form>
	
	<div class="modal-footer">
		<button id="save_unidad_button" type="button" class="btn btn-primary save_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>	
</div>

<script>
hideSpinner("#tab_unidades_new");
hideSpinner("#tab_unidades_edit");
//*********************************************
	$(":input").inputmask();
//*********************************************
	$(".js-switch").each(function(index) {
	var s = new Switchery(this, {
		size : 'small'
	});
	});
	
	$(".select-select2").select2({
		language : "es",
		containerCssClass : "single-line"
	});	
	
	<x:forEach select="$editarunidad_selector_redes_xml/ArrayList/LabelValue" var="item">
		$("#selector_redes").append('<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>');		
	</x:forEach> 
	
	selectedValues = new Array();
	<x:forEach select="$editar_unidad_negocio_xml/Unidadnegocio/redunidadnegocios/Redunidadnegocio/red" var="item">
		selectedValues.push('<x:out select="$item/idred" />');
		
	</x:forEach>
	
	$('#selector_redes').val(selectedValues);
	$('#selector_redes').trigger('change.select2');	
	
	//showSpinner("#div_recintos");
	showSpinner("#div_usuarios");

	cargar_recintos();
	cargar_usuarios();
	
	var cargando = 0;
			
	function cargar_recintos()
	{
		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/recintos/select_list_recintos.do'/>",
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				$.each(data.ArrayList.LabelValue, function(i, item) {
				    $("#selector_recintos").append('<option value="'+item.value+'">'+item.label+'</option>');
				});

				var selectedValues = new Array();
				<x:forEach select="$editar_unidad_negocio_xml/Unidadnegocio/recintounidadnegocios/Recintounidadnegocio/recinto" var="item">
					selectedValues.push('<x:out select="$item/idrecinto" />');		
				</x:forEach>
				
				
				
				$('#selector_recintos').val(selectedValues);	
				$('#selector_recintos').trigger('change.select2');					
					
				cargando = cargando + 1;
				
			    
			    
			    if(cargando == 2)
			    	{			    			    	
			    	hideSpinner("#div_usuarios");
			    	}
			}
		});
	}
	
	
	
	function cargar_usuarios()
	{
		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/recintos/select_list_usuario.do'/>",
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				$.each(data.ArrayList.LabelValue, function(i, item) {
				    $("#selector_usuarios").append('<option value="'+item.value+'">'+item.label+'</option>');
				});				
			    
				selectedValues = new Array();
				<x:forEach select="$editar_unidad_negocio_xml/Unidadnegocio/usuariounidadnegocios/Usuariounidadnegocio/usuario" var="item">
					selectedValues.push('<x:out select="$item/idusuario" />');						
				</x:forEach>
				
				$('#selector_usuarios').val(selectedValues);
				$('#selector_usuarios').trigger('change.select2');
			    
			    					
			    cargando = cargando + 1;
			    
			    if(cargando == 2)
			    	{			    		    	
			    	hideSpinner("#div_usuarios");
			    	}
			}
		});
	}
	
	$("#save_unidad_button").on("click", function(e) {
		$("#form_unidad_data").submit();	
	})
	
	$("#form_unidad_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFormData();
		}
	}
	);
	
	//********************************************************************************	
	function saveFormData() 
	{
		var data = $("#form_unidad_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/recintos/unidad/save_unidad.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				modificadoPuntoRecogida = true;
				
				$("#modal-dialog-form").modal('hide');
				
				dt_listunidades.ajax.reload(null,false);
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});

	}
	
	$('#selector_entidad_gestora option[value="<x:out select = "$editar_unidad_negocio_xml/Unidadnegocio/entidadgestora/identidadgestora" />"]').attr("selected", "selected");
		
</script>
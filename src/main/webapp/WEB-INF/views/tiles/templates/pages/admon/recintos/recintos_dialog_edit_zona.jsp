<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="modal-header"> 
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">
		<x:choose>
			<x:when select="not($idzona)">
				<spring:message code="administracion.recintos.tabs.recintos.dialog.crear_zona.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.recintos.tabs.recintos.dialog.editar_zona.title" />
			</x:otherwise>
		</x:choose>
	</h4>
</div>

<div class="modal-body">
	<form id="form_zona_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	    <input id="idzona" name="idzona" type="hidden" value="${idzona}" />
	    <input id="i18nFieldNombre_zona" name="i18nFieldNombre_zona" type="hidden" value="${i18n_nombre}" />
	    <input id="i18nFieldDescripcion_zona" name="i18nFieldDescripcion_zona" type="hidden" value="${i18n_descripcion}" />
	    	    
	    <div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.recintos.tabs.zona.field.nombre" />*</label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<input name="nombreZona" id="nombreZona" required="required" type="text" class="form-control required" required="required" value="${nombre}">
				</div>
		</div>
		
		<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.recintos.tabs.zona.field.descripcion" /></label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<textarea name="descripcionZona" id="descripcionZona" class="form-control"  >${descripcion}</textarea>
				</div>
		</div>
		
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.recintos.tabs.zona.field.aforo" />*</label>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<input name="aforoZona" id="aforoZona" required="required" type="text" data-inputmask="'mask': '9{0,10}'" data-mask class="form-control" value="${aforo}" >
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6"></div>				
			</div>	
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.recintos.tabs.zona.field.orden" /></label>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<input name="ordenZona" id="ordenZona" type="text" data-inputmask="'mask': '9{0,10}'" data-mask class="form-control" value="${orden}">
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6"></div>				
			</div>	
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.recintos.tabs.zona.field.numerada" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7">	
				  <input id ="cbnumerada" name="cbnumerada" type="checkbox"  <c:if test="${(not empty numerada) and (numerada='true')}">checked=""</c:if> data-switchery="true" style="display: none;">
				</div>
			</div>				
	</form>

	<div class="modal-footer">

		<button id="save_zona_button" type="button" class="btn btn-primary save_dialog2">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>
	
</div>
<script>
	$(":input").inputmask();
	//*********************************************
    
	if('${subrecinto}'=='0')
		{
    	$("#nombreZona").i18nField("#i18nFieldNombre_zona",<c:out value="${sessionScope.i18nlangs}"  escapeXml="false" />);
    	$("#descripcionZona").i18nField("#i18nFieldDescripcion_zona",<c:out value="${sessionScope.i18nlangs}"  escapeXml="false" />);
		}
	
	$("#cbnumerada").each(function(index) {
		var s = new Switchery(this, {
			size : 'small'
		});
	});


	$("#save_zona_button").on("click", function(e) {
		$("#form_zona_data").submit();	
	})
	
	$("#form_zona_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFormData();
		}
	}
	);
	
//********************************************************************************	
function saveFormData() 
{
	 	
	var valorCombo = false;
	if($('#cbnumerada').is(':checked'))
		valorCombo = true;
		 
	if("${creando}"=="true")
		{
		dtzonas.row.add( [ $("#idzona").val(),$("#nombreZona").val(), $("#aforoZona").val(),
	                        $("#descripcionZona").val(),$("#ordenZona").val(),valorCombo,$("#i18nFieldNombre_zona").val(),$("#i18nFieldDescripcion_zona").val()] )
	    .draw();
	    $("#modal-dialog-form-2").modal('hide');
	    }
	else
		{
		dtzonas
		  .rows({ selected: true })
		  .every(function (rowIdx, tableLoop, rowLoop) {
			  dtzonas.cell(rowIdx,0).data($("#idzona").val());
			  dtzonas.cell(rowIdx,1).data($("#nombreZona").val());
			  dtzonas.cell(rowIdx,2).data($("#aforoZona").val());
			  dtzonas.cell(rowIdx,3).data($("#descripcionZona").val());
			  dtzonas.cell(rowIdx,4).data($("#ordenZona").val());
			  dtzonas.cell(rowIdx,5).data(valorCombo);
			  dtzonas.cell(rowIdx,6).data($("#i18nFieldNombre_zona").val());
			  dtzonas.cell(rowIdx,7).data($("#i18nFieldDescripcion_zona").val()).draw();
		  })
		  .draw();	
		
		

		$("#modal-dialog-form-2").modal('hide');
		}
	
}




</script>
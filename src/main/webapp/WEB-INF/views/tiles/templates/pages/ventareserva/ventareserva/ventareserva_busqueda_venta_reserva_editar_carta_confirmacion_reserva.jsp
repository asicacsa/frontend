<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${idioma_carta}" var="idioma_carta_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	     
			<spring:message code="venta.ventareserva.tabs.ventareserva.dialog.opciones_carta_confirmacion.title" />		
	</h4>	
</div>

<div class="modal-body">
		<form id="form-carta_confirmacion_reserva" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
			<div class="col-md-12 col-sm-12 col-xs-12">		 
				<div class="form-group">
					<label class="control-label col-md-6 col-sm-6 col-xs-12"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.opciones_carta_confirmacion.field.forma_envio" /></label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select name="forma_envio" id="forma_envio" class="form-control"  required="required">
								<option value="1" selected="true"><spring:message code="common.dialog.text.forma_envi.pdf" /></option>
								<option value="2"><spring:message code="common.dialog.text.forma_envi.email" /></option>
								<option value="3"><spring:message code="common.dialog.text.forma_envi.fax" /></option>																
							</select>					
					</div>				
				</div>
				<div class="form-group">
					<label class="control-label col-md-6 col-sm-6 col-xs-12"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.opciones_carta_confirmacion.field.idioma_carta" /></label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select class="form-control" name="idioma_carta" id="idioma_carta" required="required">
							<x:forEach select="$idioma_carta_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
						</select>			
					</div>				
				</div>					
			<br/><br/>		
			</div>
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="modal-footer">	
					<button id="carta_confirmacion_enviar" type="button"  class="btn btn-primary close_carta_confirmacion">
						<spring:message code="common.button.accept" />
					</button>
					<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
						<spring:message code="common.button.cancel"/>
					</button>
				</div>
			</div>
		</form>		
	</div>

<script>
hideSpinner("#tab_busqueda_carta_confirmacion_reserva");

$("#idioma_carta").val("4");
/*
//*****************************************************
$("#form-carta_confirmacion_reserva").validate({
			onfocusout : false,
			onkeyup : false,
			unhighlight: function(element, errClass) {
	            $(element).popover('hide');
			},		
			errorPlacement : function(err, element) {
				err.hide();
				$(element).attr('data-content', err.text());
				$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
				$(element).popover('show');									
			},
			submitHandler : function(form) {				
				abrirCartaConfirmacion()
			}
		});	*/
		
		//****************************************************************
		$("#carta_confirmacion_enviar").on("click", function(e) {
			abrirCartaConfirmacion();
		});
//*****************************************************
function abrirCartaConfirmacion(){	
	
	var idioma = $('#idioma_carta').val();
	var idreserva ="${idReserva}";
	var formato = $("#forma_envio option:selected").text();
	
	var medio =""; 
	
	if(formato=="email")
		medio=formato;
	
	url="../../cartaConfirmacion.post?medioEnvio="+medio+"&format="+formato+"&idioma="+idioma+"&anulada=0&idReserva="+idreserva;
	
	if(formato=="pdf")
		window.open(url, "_blank");
	else
		{
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : url,
			timeout : 100000,
			data: {}, 
			success : function(data) {
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="venta.ventareserva.dialog.venta.cartaConfirmacion.enviada.message" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});					
			},
			error : function(exception) {				
			}
		});
		}
	
	$("#modal-dialog-form-2").modal('hide');	
	
}
//**************************************************************************

</script>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_cliente}" var="editar_cliente_xml" />
<x:parse xml="${editar_cliente_pais}" var="editar_cliente_pais_xml" />
<x:parse xml="${editar_cliente_categorias_actividad}" var="editar_cliente_categorias_actividad_xml" />
<x:parse xml="${editar_cliente_tipos_documento}" var="editar_cliente_tipos_documento_xml" />
	
	<input id="idcliente" name="idcliente" type="hidden" value="<x:out select = "$editar_cliente_xml/Cliente/idcliente" />" />
	<input id="xmlDireccionOrigen" name="xmlDireccionOrigen" type="hidden" />
	<input id="xmlDireccionFiscal" name="xmlDireccionFiscal" type="hidden" />
	<input id="xmlDireccionAuxiliar" name="xmlDireccionAuxiliar" type="hidden" />
	
	<div class="col-md-7 col-sm-7 col-xs-12">
			<input id="idprincipal" name="idprincipal" type="hidden" value="<x:out select = "$editar_cliente_xml/Cliente/cliente/idcliente" />" />
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.field.persona" />*</label>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<select name="personafisica" id="personafisica"  class="form-control" required="required">
						<option value="1">F�sica</option>
						<option value="0">Juridica</option>
					</select>
				</div>
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.field.nombre" />*</label>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editar_cliente_xml/Cliente/nombre" />">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.field.apellido1" />*</label>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<input name="apellido1" id="apellido1" type="text" class="form-control" required="required" value="<x:out select = "$editar_cliente_xml/Cliente/apellido1" />">
				</div>
			
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.field.apellido2" />*</label>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<input name="apellido2" id="apellido2" type="text" class="form-control" required="required" value="<x:out select = "$editar_cliente_xml/Cliente/apellido2" />">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.field.razon" />*</label>
				<div class="col-md-10 col-sm-10 col-xs-4">
					<input name="razonSocial" id="razonSocial" type="text" class="form-control" required="required" value="<x:out select = "$editar_cliente_xml/Cliente/razonsocial" />">
				</div>			
			</div>		
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.field.actividad" />*</label>
				<div class="col-md-5 col-sm-5 col-xs-5">
					<select name="idcategoriacategoriaactividad" id=idcategoriacategoriaactividad  class="form-control" required="required">
						<option> </option>
						<x:forEach select="$editar_cliente_categorias_actividad_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
				<div class="col-md-5 col-sm-5 col-xs-5">
					<select name="idactividad" id="selector_actividad"  class="form-control" required="required">
						
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-3 col-sm-3 col-xs-12">
					<strong><spring:message code="facturacion.facturas.tabs.clientes.field.identificacion" /></strong>
				</div>			
				<div class="checkbox col-md-1 col-sm-1 col-xs-1">									
					<input type="radio" name="identificador_identificador" id="tipo_identificador" checked value="2" data-parsley-multiple="tipo_identificacion" class="flat" style="position: absolute; opacity: 0;"/>			
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<select name="idtipoidentificador" id="idtipoidentificador"  class="form-control" required="required">
						<option> </option>
						<x:forEach select="$editar_cliente_tipos_documento_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<input name="identificador" id="identificador" type="text" class="form-control" required="required" value="<x:out select = "$editar_cliente_xml/Cliente/identificador"/>"/>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-3 col-sm-3 col-xs-12">
				<strong><spring:message code="facturacion.facturas.tabs.clientes.field.cif" /></strong>
				</div>
				<div class="checkbox col-md-1 col-sm-1 col-xs-1 ">
						<input type="radio" name="identificador_identificador" id="tipo_cif"  value="0" data-parsley-multiple="tipo_identificacion" class="flat" style="position: absolute; opacity: 0;"/>			
				</div>
				<div class="col-md-3 col-sm-3 col-xs-3">
					<input name="ciffiscal" id="ciffiscal" type="text" class="form-control" required="required" value="<x:out select = "$editar_cliente_xml/Cliente/cliente/identificador"/>"/>
				</div>
				<div class="col-md-5 col-sm-5 col-xs-12">
				</div>
			</div>
						
	</div>
	<div class="col-md-5 col-sm-5 col-xs-12">
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.clientes.field.observaciones" /></label>
				<div class="col-md-8 col-sm-8 col-xs-8">					
					<textarea name="observaciones" id="observaciones" class="form-control"  ><x:out select = "$editar_cliente_xml/Cliente/observaciones" /></textarea>
				</div>
			</div>
			<div class="form-group" >
				<div class="col-md-12 col-sm-12 col-xs-12">
						<spring:message code="facturacion.facturas.tabs.clientes.text.facturar" />
						<input name="facturable" id="facturable" type="checkbox" <x:if select="$editar_cliente_xml/Cliente/facturable='1'">checked=""</x:if> class="js-switch" data-switchery="true" style="display: none;">
				</div>
				<div class="checkbox col-md-12 col-sm-12 col-xs-12 facturacion_ocultar" style="margin-left:50px;">									
					<div class="checkbox col-md-12 col-sm-12 col-xs-12">
						<input type="radio" name="principal" id="principal" checked value="0" data-parsley-multiple="tipo_identificacion" class="flat" style="position: absolute; opacity: 0;"/>
						<strong><spring:message code="facturacion.facturas.tabs.clientes.field.f_principal" /></strong>
					</div>	
					<br/><br/>	
					<div class="checkbox col-md-12 col-sm-12 col-xs-12 facturacion_ocultar">									
						<input type="radio" name="principal" id="sucursal" value="1" data-parsley-multiple="tipo_identificacion" class="flat" style="position: absolute; opacity: 0;"/>
						<strong><spring:message code="facturacion.facturas.tabs.clientes.field.f_sucursal" /></strong>			
					</div>
					<br/><br/>
					<div class="col-md-12 col-sm-12 col-xs-12 facturacion_ocultar">	
						<input name="generarrappelporsucursal" type="checkbox" <x:if select="$editar_cliente_xml/Cliente/generarrappelporsucursal='1'">checked=""</x:if> class="js-switch" data-switchery="true" style="display: none;">
						<strong><spring:message code="facturacion.facturas.tabs.clientes.text.generarrappelporsucursal" /></strong>
					</div>
					<br/><br/>	
					<div class="col-md-12 col-sm-12 col-xs-12 facturacion_ocultar">	
						<input name="rappelautomatico" type="checkbox" <x:if select="$editar_cliente_xml/Cliente/rappelautomatico='1'">checked=""</x:if> class="js-switch" data-switchery="true" style="display: none;">
						<strong><spring:message code="facturacion.facturas.tabs.clientes.text.rappelautomatico" /></strong>
					</div>	
				</div>
		 	</div>	

			<div class="form-group"  id="div_sucursal">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.clientes.field.sucursalDe" /></label>
				<div class="col-md-8 col-sm-8 col-xs-8">
                	<input name="cliente_dialog_edit" id="cliente_dialog_edit" type="text" class="form-control" readonly value="<x:out select = "$editar_cliente_xml/Cliente/cliente/nombre" />" onchange="sucursalChange()">
	             </div>				
			</div>
			<div class="form-group" id="div_selector_sucursales">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.clientes.field.sucursales" /></label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<select name="sucursales[]" id="selector_sucursales" multiple="multiple" style="width: 100%">
							<x:forEach select="$editar_cliente_xml/Cliente/clientes/Cliente" var="item">
								<option value=""><x:out select="$item/nombrecompleto" /></option>
							</x:forEach>				
					</select>
				</div>
			</div>	
		 			
	</div>
	
	

	


<script>

function sucursalChange() {
	esSucursal();
	$("#tipo_cif").iCheck("check");
	$("#ciffiscal").val($("#identificador").val());
	$("#identificador").val("");
}


function esSucursal() {
	$("#tipo_cif").iCheck("check");
	$("#personafisica").attr( "disabled", true);
	$("#idcategoriacategoriaactividad").attr( "disabled", true);
	$("#selector_actividad").attr( "disabled", true);	
	$("#tipo_identificador").attr( "disabled", true);	
    $('#ciffiscal').attr('disabled', true); 	
	$('#idcanal').attr('disabled', false);
	$('#descripcion').attr('disabled', true);
	$('#principal').iCheck('disable');
	$('#sucursal').iCheck('disable');
	$('#facturable').parent().css("pointer-events", "none");
	$('#facturable').parent().css("opacity","0.5");

	setTimeout(function() {
		$('#tab_direccion_fiscal select').attr('disabled', true);
		$('#tab_direccion_fiscal input').attr('disabled', true);
		$('#tab_direccion_fiscal button').attr('disabled', true);
		$('#tab_direccion_auxiliar select').attr('disabled', true);
		$('#tab_direccion_auxiliar input').attr('disabled', true);
		$('#tab_direccion_auxiliar button').attr('disabled', true);
		$('#tab_condiciones_cliente select').attr('disabled', true);
		$('#tab_condiciones_cliente input').attr('disabled', true);
		$('#tab_condiciones_cliente button').attr('disabled', true);
		
		$('#tab_condiciones_cliente input[name="facturable_electronico"]').parent().css("pointer-events", "none");
		$('#tab_condiciones_cliente input[name="facturable_electronico"]').parent().css("opacity","0.5");
		$('#tab_condiciones_cliente input[name="grupo"]').parent().css("pointer-events", "none");
		$('#tab_condiciones_cliente input[name="grupo"]').parent().css("opacity","0.5");
	}, 2000);

}

$("document").ready(function() {
	
<c:set var="isprincipal">true</c:set>
<c:set var="isfacturableelectronico">false</c:set>
<c:set var="iseditabletipoidentificacion">true</c:set>
<c:set var="modoEdicion">0</c:set>

<c:set var="editando"><x:out select = "$editar_cliente_xml/Cliente/idcliente"/></c:set>

<c:if test="${!empty editando}">
	<c:set var="modoEdicion">1</c:set>
	
	<c:set var="id_tipo_identificador"><x:out select = "$editar_cliente_xml/Cliente/cliente/tipoidentificador/idtipoidentificador"/></c:set>
	<c:if test="${empty id_tipo_identificador}">
		<c:set var="id_tipo_identificador"><x:out select = "$editar_cliente_xml/Cliente/tipoidentificador/idtipoidentificador"/></c:set>
		$("#tipo_cif").iCheck('disable');
	</c:if>
	
	<%-- Es editable si no es NIF ni CIF --%>
	<c:if test="${id_tipo_identificador!=1 && id_tipo_identificador!=2}">
		<c:set var="iseditabletipoidentificacion">false</c:set>
 		$("input[name='identificador_identificador']").iCheck('disable');
	</c:if>

	<c:set var="existe_cif"><x:out select = "$editar_cliente_xml/Cliente/cliente/identificador"/></c:set>
	<c:if test="${!empty existe_cif}">
		$("input[name='identificador_identificador'][value='1']").iCheck("check");
		$("input[name='identificador_identificador']").iCheck("disable");
	</c:if>
	
	
	<c:set var="id_principal"><x:out select = "$editar_cliente_xml/Cliente/cliente/idcliente"/></c:set>
	
	<c:if test="${!empty id_principal}">
		<c:set var="isprincipal">false</c:set>
	</c:if>

	<c:set var="hay_sucursales"><x:out select = "$editar_cliente_xml/Cliente/clientes/Cliente"/></c:set>

	<c:if test="${!empty hay_sucursales}">
			$("#div_sucursal").hide();
			$("#div_selector_sucursales").show();
	</c:if>

	<c:if test="${empty hay_sucursales}">
			$("#div_sucursal").show();
			$("#div_selector_sucursales").hide();
			
			<c:if test="${isprincipal}"> 
				$("#cliente_dialog_edit").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>", "#cliente_dialog_edit", "#idprincipal");
			</c:if>
	</c:if>

	<c:set var="es_facturable"><x:out select = "$editar_cliente_xml/Cliente/facturable"/></c:set>
	<c:set var="es_facturable_electronico"><x:out select = "$editar_cliente_xml/Cliente/facturable_electronico"/></c:set>

	<c:if test="${es_facturable==1}">

		$("#idtipoidentificador").attr( "disabled", true);
		$("#identificador").attr( "disabled", true);
		$("#tipo_identificador").attr( "disabled", true);	
	    $('#ciffiscal').attr('disabled', true); 	
		$("#facturable_electronico").attr( "disabled", false);	
				
		<c:if test="${es_facturable_electronico==1}">
			<c:set var="isfacturableelectronico">true</c:set>
		</c:if>
	
	</c:if>
	
	<c:if test="${es_facturable!=1 && (id_tipo_identificador==1 || id_tipo_identificador==2)}">
		$("#idtipoidentificador").attr( "disabled", false);
		$("#identificador").attr( "disabled", false);		
		$("#tipo_identificador").attr( "disabled", false);	
	    $('#ciffiscal').attr('disabled', false); 	
		$("#facturable_electronico").attr( "disabled", true);		
	</c:if>

</c:if>

<c:if test="${empty editando}">
	$("#div_sucursal").show();
	$("#div_selector_sucursales").hide();
</c:if>

$('#idcanal').attr('disabled', true);

<c:if test="${!isprincipal}">
	esSucursal();
</c:if>

hideSpinner("#tab_clientes_new");
hideSpinner("#tab_clientes_edit");

$(".js-switch").each(function(index) {
	var s = new Switchery(this, {
		size : 'small'
	});
});

function deshabilitarPersonaFisica()
{
	var personafisica= $("#personafisica").val();
	if(personafisica=='0')
	{
		$( "#apellido1" ).prop( "disabled", true );
		$( "#apellido2" ).prop( "disabled", true );
	}
	else
	{
		$( "#apellido1" ).prop( "disabled", false );
		$( "#apellido2" ).prop( "disabled", false );
	}
}
var personafisica = '<x:out select = "$editar_cliente_xml/Cliente/personafisica" />';
$('#personafisica option[value="'+personafisica+'"]').attr("selected", "selected");
deshabilitarPersonaFisica();

$("#personafisica").change(function() {
	deshabilitarPersonaFisica();		
});


var categoriacategoriaactividad = '<x:out select = "$editar_cliente_xml/Cliente/actividad/categoriaactividad/idcategoriacategoriaactividad" />';
var idactividad = '<x:out select = "$editar_cliente_xml/Cliente/actividad/idactividad" />';
if(categoriacategoriaactividad!='')
{
	$('#idcategoriacategoriaactividad option[value="'+categoriacategoriaactividad+'"]').attr("selected", "selected");
	cargarActividadesPorCategoria(categoriacategoriaactividad);	
	
}


function cambiarIdentificador(tipoIdentificador)
{	
	if (tipoIdentificador=='0') 
		  {
			$("#tipo_cif").attr('checked', true);
			$("#tipo_identificador").removeAttr('checked');
			$('#identificador').attr('disabled', 'true');
		    $('#idtipoidentificador').attr('disabled', 'true');
		    $('#ciffiscal').removeAttr('disabled');         
		  }
	else 
		  {
			$("#tipo_identificador").attr('checked', true);
			$("#tipo_cif").removeAttr('checked');
		    $('#identificador').removeAttr('disabled');
		    $('#idtipoidentificador').removeAttr('disabled');
		    $('#ciffiscal').attr('disabled', 'disabled'); 	
		  }
}

var tipoIdentificador = '<x:out select = "$editar_cliente_xml/Cliente/tipoidentificador/idtipoidentificador" />';
$('#idtipoidentificador option[value="'+tipoIdentificador+'"]').attr("selected", "selected");

var principal = '<x:out select = "$editar_cliente_xml/Cliente/facturarsucursal" />';
$("input[name=principal][value='" + principal + "']").attr('checked', 'checked');

$("input[name='identificador_identificador']").on("ifChanged", function(e) {
	cambiarIdentificador($("input[name='identificador_identificador']:checked").val());
 });


$("#idcategoriacategoriaactividad").change(function() {
	var idCategoria= $("#idcategoriacategoriaactividad").val();
	cargarActividadesPorCategoria(idCategoria);		
});

function cargarActividadesPorCategoria(idCategoria)
{
	$.ajax({
		type : "post",
		url : "<c:url value='/ajax/facturacion/clientes/select_list_actividad_por_tipo.do'/>?idCategoria="+idCategoria,
		timeout : 100000,
		data : null,
		dataType : 'json',
		success : function(data) {
			$('#selector_actividad')
		    .find('option')
		    .remove().end()
		    .append('<option value=""></option>');
			$.each(data.ArrayList.LabelValue, function(i, item) {
			    $("#selector_actividad").append('<option value="'+item.value+'">'+item.label+'</option>');			    
			});	
			$('#selector_actividad option[value="'+idactividad+'"]').attr("selected", "selected");
		}
	});
}


if ($("input.flat")[0]) {
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });
    
 
}

});
</script>
	
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${nuevodocumento_selector_categorias}" var="nuevodocumento_selector_categorias_xml" />
<x:parse xml="${nuevodocumento_rutaweb}" var="nuevodocumento_rutaweb_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">x</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="documentacion.documentos.gestion.dialog.nuevo.crear_documento.title" />
	</h4>
</div>

<div class="modal-body" id="principal">

	<form id="form_documento_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	
		<input type="hidden" name="rutaweb" id="rutaweb" value="<x:out select = "$nuevodocumento_rutaweb_xml/string" />" />
		<input type="hidden" name="ruta" id="ruta" />

		<div class="col-md-12 col-sm-12 col-xs-12">

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="documentacion.documentos.gestion.dialog.nuevo.field.categoria" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select class="form-control" name="idcategoria_nuevo" id="idcategoria_nuevo" required="required">
						<option value=""></option>
							<x:forEach select="$nuevodocumento_selector_categorias_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="documentacion.documentos.gestion.dialog.nuevo.field.nombre" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="documentacion.documentos.gestion.dialog.nuevo.field.descripcion" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<textarea class="form-control" rows="3" name="descripcion" id="descripcion"></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="documentacion.documentos.gestion.dialog.nuevo.field.fechainiciovigencia" /></label>
				<div class="col-md-3 col-sm-3 col-xs-9">
                      <div class="input-prepend input-group">
                       <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                       <input type="text" name="fechainiciovigencia" id="fechainiciovigencia" class="form-control" readonly/>
                      </div>
				</div>
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="documentacion.documentos.gestion.dialog.nuevo.field.fechafinvigencia" /></label>
				<div class="col-md-3 col-sm-3 col-xs-9">
                      <div class="input-prepend input-group">
                       <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                       <input type="text" name="fechafinvigencia" id="fechafinvigencia" class="form-control" readonly/>
                      </div>
				</div>
			</div>
			
			<div class="col-md-9 col-sm-9 col-xs-9 col-md-offset-3 col-sm-offset-3 col-xs-offset-3">
				<div class="form-group">
				  	<span class="btn btn-success fileinput-button">
				        <i class="glyphicon glyphicon-plus"></i>
				        <span><spring:message code="documentacion.documentos.gestion.dialog.nuevo.button.seleccionar_fichero" /></span>
				        <input id="fileupload" type="file" name="file" single>
				    </span>
				    <br>
				    <div id="progress" class="progress">
				        <div class="progress-bar progress-bar-success"></div>
				    </div>
				    <div id="files" class="files"></div>
				</div>
			</div>
		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_documento_button" type="submit" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>

	</form>

</div>

<script>
hideSpinner("#gestiondocumentos_new");

$('input[name="fechainiciovigencia"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
  	locale: $daterangepicker_sp
});

$('input[name="fechafinvigencia"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
  	locale: $daterangepicker_sp
});

$('#fileupload').fileupload({	
    url: "<c:url value='/ajax/documentacion/singleUpload.do'/>",
    dataType: 'json',
    done: function (e, data) {
        $('<p/>').text(data.result.name).appendTo('#files');
        $('#ruta').val(data.result.saved);
        
    },
    progressall: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }
}).prop('disabled', !$.support.fileInput)
    .parent().addClass($.support.fileInput ? undefined : 'disabled');

$("#form_documento_data").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
           $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		saveDocumentoData();
	}
});

//********************************************************************************	
function saveDocumentoData() {
	
	var data = $("#form_documento_data").serializeObject();

	showSpinner("#modal-dialog-form .modal-content");

	$.ajax({
		type : "post",
		url : "<c:url value='/ajax/documentacion/documentos/save_documento.do'/>",
		timeout : 100000,
		data : data,
		success : function(data) {
			hideSpinner("#modal-dialog-form .modal-content");
			$('#idcategoria').val($("#idcategoria_nuevo").val());
			dt_listdocumentos.ajax.reload(null,false);						
			$("#modal-dialog-form").modal('hide');
			
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
			
		},
		error : function(exception) {
			hideSpinner("#modal-dialog-form .modal-content");

			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : exception.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		}
	});
}
</script>

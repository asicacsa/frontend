<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">		
		<spring:message code="encuestas.mantenimientoencuestas.dialog.search_pregunta.title" />
	</h4>	
</div>


<div class="modal-body">
	<form id="form_busqueda_preguntas_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input id="xml_datos_pregunta" name="xml_datos_pregunta" type="hidden" />
		<input id="cod_encuesta" name="cod_encuesta" type="hidden" />
		
		
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.field.pregunta" /></label>
			<div class="col-md-6 col-sm-6 col-xs-12">	
				<input name="texto_pregunta" id="texto_pregunta" class="form-control"  />	
			</div>	
			<button id="button_search_pregunta" type="button" class="btn btn-success pull-right">
					<spring:message code="encuestas.mantenimientoencuestas.dialog.search_pregunta.list.button.search" />
			</button>				
		</div>	
		<div class="col-md-12 col-sm-12 col-xs-12" id="preguntas-encontradas" hidden >
			<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="encuestas.mantenimientoencuestas.dialog.search_pregunta.field.listadopreguntas" /></label>
			<table id="table-lista-preguntas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%" >
				<thead>
					<tr>	
						<th id="codpre"></th>
						<th id="codtipores"></th>
						<th id="textopre"><spring:message code="encuestas.mantenimientoencuestas.dialog.search_encuesta.list.header.texto" /></th>
						<th id="tipores"><spring:message code="encuestas.mantenimientoencuestas.dialog.search_encuesta.list.header.tipo" /></th>
					</tr>
				</thead>
				<tbody>													
				</tbody>
			</table>
		</div>	
	</form>
	<div class="modal-footer">
		<button id="save_pregunta_button" type="button" class="btn btn-primary">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>	

<script>
var dtpreguntas_encontradas=null;

$("#cod_encuesta").val($("#idencuesta_editar").val());
//********************************************************
$("#button_search_pregunta").on("click", function(e) {
	$("#preguntas-encontradas").show();
	if(dtpreguntas_encontradas==null)
		{		
		dtpreguntas_encontradas=$('#table-lista-preguntas').DataTable( {	
		 ajax: {
		 url: "<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/list_preguntas.do'/>",
		 rowId: 'codPregunta',
		 type: 'POST',
		 dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Pregunta)); return(""); },
		 error: function (xhr, error, thrown) {	
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : xhr.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}					
				});
	 		},
		 data: function (params) { return($("#form_busqueda_preguntas_data").serializeObject()); }
		 },
		 initComplete: function( settings, json ) {
		 $('a#menu_toggle').on("click", function () {if (dtpreguntas_encontradas.data().count()>0) dtpreguntas_encontradas.columns.adjust().draw(); });
		 },
		 columns: [
		 { data: "id.codPregunta", type: "spanish-string" ,  defaultContent:""} ,
		 { data: "tipoRespuesta.cod", type: "spanish-string" ,  defaultContent:""},
		 { data: "des", type: "spanish-string" ,  defaultContent:""} ,
		 { data: "tipoRespuesta.des", type: "spanish-string" ,  defaultContent:""}
		 
		 ],    
		 drawCallback: function( settings ) {
		 $('[data-toggle="tooltip"]').tooltip();
		 },
		"paging": false,
		"info": false,
		"searching": false,
		 select: { style: 'os'},
		 language: dataTableLanguage,
		 "columnDefs": [
		                { "visible": false, "targets": [0,1]}
		              ],
		 processing: true,
		 } );
		}
		else
		{			
			dtpreguntas_encontradas.ajax.reload();	
		}
	
		insertSmallSpinner("#table-lista-preguntas_processing");
	})

//******************************************************
$("#save_pregunta_button").on("click", function(e) {
	
	var data = dtpreguntas_encontradas.rows( { selected: true }).data();
	

	if (data.length<=0) 
	{
		return;
	}
	
	//******************************************
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="encuestas.mantenimientoencuestas.dialog.search_encuesta.guardar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
	  		  buttons: { closer: false, sticker: false	},
	  		  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
				
				showSpinner("#modal-dialog-form .modal-content");
				var id_encuesta=$("#idencuesta_editar").val();
					
				var xml = "<list>";
				
				for(i=0;i<data.length;i++)
					{
					var tmp = data[i];					
					var idpregunta=tmp.id.codPregunta;
					xml = xml + "<PreguntaEncuestaId>";
					xml = xml + "<codEncuesta>"+id_encuesta+"</codEncuesta>";
					xml = xml + "<codIdioma>1</codIdioma>";
					xml = xml +"<codPregunta>"+idpregunta+"</codPregunta>";
					xml = xml +"</PreguntaEncuestaId>";
					}
				xml = xml+"</list>";
				
				$("#xml_datos_pregunta").val(xml);
				var datosForm = $("#form_busqueda_preguntas_data").serializeObject();
				$.ajax({
					type : "post",
					url : "<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/asignar_pregunta.do'/>",
					timeout : 100000,
					data : datosForm,
					success : function(resultado) {
						hideSpinner("#modal-dialog-form .modal-content");				
						$("#modal-dialog-form-2").modal('hide');
						for(i=0;i<data.length;i++)
						{
							var tmp = data[i];
							var codpregunta=tmp.id.codPregunta;
							var codtiporespuesta=tmp.tipoRespuesta.cod;
							var despregunta=tmp.des;
							var destiporespuesta=tmp.tipoRespuesta.des;
							dtpreguntas.row.add([codpregunta,codtiporespuesta,despregunta,destiporespuesta]).draw();
						}
						new PNotify({
							title : '<spring:message code="common.dialog.text.operacion_realizada" />',
							text : '<spring:message code="common.dialog.text.datos_guardados" />',
							type : "success",
							delay : 5000,
							buttons : {
								closer : true,
								sticker : false
							}
						});
						},
						error : function(exception) {
						hideSpinner("#modal-dialog-form .modal-content");

						new PNotify({
							title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
							text : exception.responseText,
							type : "error",
							delay : 5000,
							buttons : {
								closer : true,
								sticker : false
							}
						});
					}
				});	
				
		   }).on('pnotify.cancel', function() {
		   });		
	//******************************************
	
	

	
	
	
	
	
	
		
	
	
	
	
})

</script>

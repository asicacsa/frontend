<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2><spring:message code="common.text.filter_list" /></h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">
	<form id="form_filter_list_gestionfacturasporcliente" class="form-horizontal form-label-left">
		<input id="idcliente" name="idcliente" type="hidden" value="${idcliente}" />
		<input id="tipo-ventas" name="tipo-ventas" type="hidden" value="1" />
		 <div class="form-group" id="refventageneracion">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.generacionfacturas.field.refventa" />:</label>					
			<div class="col-md-3 col-sm-3 col-xs-3">
               	<input type="text" name="refventageneracioninicial" id="refventageneracioninicial" class="form-control" value=""/>
			</div>
			<label class="control-label col-md-1 col-sm-1 col-xs-1"><spring:message code="facturacion.facturas.tabs.generacionfacturas.field.a" /></label>
			<div class="col-md-3 col-sm-3 col-xs-3">
               	<input type="text" name="refventageneracionfinal" id="refventageneracionfinal" class="form-control" value=""/>
			</div>
		</div>	
		
	    <div class="form-group date-picker" id="fechaventageneracion">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.generacionfacturas.field.fechaventa" />:</label>
			<div class="col-md-5 col-sm-5 col-xs-5">
				<a type="button" class="btn btn-default btn-clear-date" id="button_fechaventageneracion_clear">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.generacionfacturas.list.button.clear" />"> <span class="fa fa-trash"></span>
					</span>
				</a>			
                   	<div class="input-prepend input-group">
                      	<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                       	<input type="text" name="ventageneracion" id="ventageneracion" class="form-control" value="" readonly/>
                        	<input type="hidden" required="required" name="fechainicioventageneracion" value=""/>
                        	<input type="hidden" required="required" name="fechafinventageneracion" value=""/>
                      </div>
			</div>
		</div>
		
		<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
				<button id="button_filter_list_ventasporcliente" type="button" class="btn btn-success pull-right">
					<spring:message code="common.button.filter" />
				</button>
		</div>
	</form>
	<table id="datatable-list-ventasporcliente" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="facturacion.facturas.tabs.generacionfacturas.list.header.refventa" /></th>
				<th><spring:message code="facturacion.facturas.tabs.generacionfacturas.list.header.codcliente" /></th>
				<th><spring:message code="facturacion.facturas.tabs.generacionfacturas.list.header.cliente" /></th>
				<th><spring:message code="facturacion.facturas.tabs.generacionfacturas.list.header.fechaventa" /></th>
				<th><spring:message code="facturacion.facturas.tabs.generacionfacturas.list.header.importe" /></th>
				
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	<div class="modal-footer">
		<button id="rectificar_venta_button" type="button" class="btn btn-primary rectificar_venta_button">
			<spring:message code="common.button.accept" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>
</div>

<script>
var dt_ventasporcliente=$('#datatable-list-ventasporcliente').DataTable( {
	serverSide: true,
	searching: false,
	ordering: false,
	pagingType: "simple",
	info: false,
	deferLoading: 0,		
	
	 ajax: {
	        url: "<c:url value='/ajax/facturacion/facturas/generacionfacturas/list_generacionfacturas.do'/>",
	        rowId: 'idventa',
	        type: 'POST',
	        dataSrc: function (json) {
	        	if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Venta)); return(""); },
	        data: function (params) {
	        	$('#form_filter_list_gestionfacturasporcliente input[name="start"]').val(params.start);
	        	$('#form_filter_list_gestionfacturasporcliente input[name="length"]').val(params.length);
	        	return($("#form_filter_list_gestionfacturasporcliente").serializeObject());
	    	
	        },
	        
	        error: function (xhr, error, thrown) {
	            if (xhr.responseText=="403") {
	                  $("#generacionfacturas-tab").hide();
	            }   
	            else
	            	{
	            	   	new PNotify({
	     					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
	     					text : xhr.responseText,
	     					type : "error",
	     					delay : 5000,
	     					buttons : {
	     						closer : true,
	     						sticker : false
	     					}					
	     				});
	                    	
	            	}
	     },
	    },
	    initComplete: function( settings, json ) {
	        $('a#menu_toggle').on("click", function () {if (dt_listgeneracionfacturas.data().count()>0) dt_listgeneracionfacturas.columns.adjust().draw(); });
		}  
	    ,
	    columns: [

			{ data: "idventa", type: "spanish-string", defaultContent: ""}, 
			{ data: "cliente.idcliente", type: "spanish-string", defaultContent: ""}, 
			{ data: "cliente.nombrecompleto", type: "spanish-string", defaultContent: "" },		 
			{ data: "fechayhoraventa", type: "spanish-string" ,  defaultContent:"",
				 render: function ( data, type, row, meta ) {			 
		    	  	  return data.substr(0, 10);}	 
			 } ,
			{ data: "importetotalventa", type: "spanish-string", defaultContent: ""},
	    ],    
	    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-generacionfacturas') },
	    select: { style: 'os'},
		language: dataTableLanguage,
		processing: true,
} );

//********************************************************************************
$("#button_filter_list_ventasporcliente").on("click", function(e) {	
	dt_ventasporcliente.ajax.reload();
	})
	
//Fecha venta:
$('input[name="fechainicioventageneracion"]').val(dia_inicio);
$('input[name="fechafinventageneracion"]').val(dia_fin);
$('input[name="ventageneracion"]').val( dia_inicio + ' - ' + dia_fin);


$('input[name="ventageneracion"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
  	  	 $('input[name="ventageneracion"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('input[name="fechainicioventageneracion"]').val(start.format('DD/MM/YYYY'));
  	  	 $('input[name="fechafinventageneracion"]').val(end.format('DD/MM/YYYY'));
	 });	


/***********************************************BOT�N EDITAR*************************************/
$("#rectificar_venta_button").on("click", function(e) { 
	var data = dt_ventasporcliente.rows( { selected: true }).data();
	
	if (data.length==1) {
		datos=data[0];
		
		fecha = datos.fechayhoraventa.substr(0, 10)
		dtlistadoventas.row.add([datos.idventa,datos.cliente.idcliente,datos.cliente.nombrecompleto,1,fecha,datos.importetotalventa]).draw();
		$("#modal-dialog-form-2").modal('hide');
	}
})
dtlistadoventas

</script>
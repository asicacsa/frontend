<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>



<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="facturacion.facturas.tabs.royalties.dialog.new_distribuidora.title" />
	</h4>	
</div>

<div class="modal-body">
	<form id="form_new_distribuidora_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<div class="col-md-12 col-sm-12 col-xs-12">		
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.tipo_clientes.field.nombre" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="nombredistribuidora" id="nombredistribuidora" type="text" class="form-control" required="required" value="">
				</div>
			</div>
			
			<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.rappels.field.descripcion" /></label>
			<div class="col-md-9 col-sm-9 col-xs-9">					
				<textarea name="descripciondistribuidora" id="descripciondistribuidora" class="form-control" ></textarea>
			</div>
			</div>
					
		
		</div>
	</form>
	<div class="modal-footer">
		<button id="save_distribuidora_button" type="button" class="btn btn-primary save_distribuidora_royalty">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>	


<script>
hideSpinner("#tab_distribuidoras_royalty_new");


$(".save_distribuidora_royalty").on("click", function(e) {
	$("#form_new_distribuidora_data").submit();	
})
//*****************************************************************
$("#form_new_distribuidora_data").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		guardarDistribuidora();
	}
})

	function guardarDistribuidora()
	{	
		var data = $("#form_new_distribuidora_data").serializeObject();

		showSpinner("#modal-dialog-form-2 .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/facturacion/facturas/royalties/save_distribuidora_royalty.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				
				$("#modal-dialog-form-2").modal('hide');
				dt_listdistribuidorasroyalty.ajax.reload();									
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form-2 .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});

	}

//***************************************************************************************
</script>
	
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_productos_principales}" var="ventareserva_productos_principales_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.tabs.ventareserva.dialog.resumen.title" />
	</h4>
</div>

<div class="modal-body">
	<form id="form_venta_reserva_modificar_venta_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
			
			<input type="hidden" id="idcliente_venta_individual" name="idcliente_venta_individual" value=""/>
			<input type="hidden" id="cifcliente_venta_individual" value=""/>
			<input type="hidden" id="cpcliente_venta_individual" value=""/>
			<input type="hidden" id="emailcliente_venta_individual" value=""/>
			<input type="hidden" id="telefonocliente_venta_individual" value=""/>
			<input type="hidden" id="fecha_venta_individual" name="fecha_venta_individual" value=""/>
			
		
				 
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label class="control-label"><spring:message code="venta.ventareserva.tabs.emision_bono.field.unidad" /></label>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<select class="form-control ventareserva" name="idunidad_venta_individual" id="idunidad_venta_individual" size="5" required="required">
							<x:forEach select="$ventareserva_productos_principales_xml/ArrayList/Entry/unidadnegocio" var="item">
								<option value="<x:out select="$item/idunidadnegocio" />"><x:out select="$item/nombre" /></option>
							</x:forEach>
						</select>
					</div>
				</div>
			</div>
				
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label class="control-label"><spring:message code="venta.ventareserva.tabs.emision_bono.field.producto" /></label>
					<div class="col-md-12 col-sm-12 col-xs-12" id="div_producto_venta_individual">
						<select class="form-control ventareserva" name="idproducto_venta_individual" id="idproducto_venta_individual" size="5" required="required">
							<option value=""></option>
						</select>
					</div>
				</div>				
			</div>
		

		<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
			<button id="button_add_venta_individual" type="button" class="btn btn-success pull-right">
				<spring:message code="venta.ventareserva.tabs.emision_bono.button.add" />
			</button>
		</div>
		</form>
	<div class="col-md-12 col-sm-12 col-xs-12">
	
		<div class="totales-group totales_venta_individual">
			Total: <span class="total-lbl"><span id="total-val_mod_res">0.00</span>&euro;</span>&nbsp;<span class="descuento-lbl">Descuento: <span id="total-desc_mod_res">0.00</span>&euro;&nbsp;(<span id="total-prc_mod_res">0.00</span>%)</span>
		</div>
	
		<div class="btn-group pull-right btn-datatable">
			<a type="button" class="btn btn-info" id="button_venta_individual_sesiones">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.sesiones" />"> <span class="fa fa-history"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="button_venta_individual_combinar">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.combinar" />"> <span class="fa fa-object-group"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="button_venta_individual_descombinar">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.descombinar" />"> <span class="fa fa-object-ungroup"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="button_venta_individual_duplicar">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.emision_bono.list.button.duplicar" />"> <span class="fa fa-clone"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="button_venta_individual_eliminar">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.emision_bono.list.button.eliminar" />"> <span class="fa fa-trash"></span>
				</span>
			</a>			
		</div>
	
		<table id="datatable_list_venta_individual" class="table table-striped table-bordered dt-responsive ventareserva" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.producto" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.fecha" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.contenido" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.disponible" />.</th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.cnd" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.perfil" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.descuento" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.impUnitario" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.importe" /></th>
					<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.sesiones" /></th>
					<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.bono" /></th>
					<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.idtarifa" /></th>
					<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.iddescuento" /></th>
					<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.idperfil" /></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
		
		
		
		
	<div class="modal-footer">
		<button id="save_lineas_detalle_venta_button" type="button" class="btn btn-primary rectificar_dialog-save">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>
<script>

var idcliente = $("#idcliente_rectificacion").val();
var idtipoventa = $("#idTipoVenta_rectificacion").val();
var fechaoperacion =$("#fecha_reserva").val().substr(0,10);

$("#idcliente_venta_individual").val($("#idcliente").val());
$("#fecha_venta_individual").val($("#fecha_reserva").val().substr(0,10));

var lineasDetallesEliminadas = [];





//***************************************

var dt_listventaindividual=$('#datatable_list_venta_individual').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	scrollCollapse: true,
	paging: false,
	initComplete: function( settings, json ) {
	
   	 window.setTimeout(cargarLineasParaVenta, 100);    	 
	},
    select: { style: 'os' },
    columnDefs: [
        { "targets": 0, "visible": false },
        { "targets": 1, "visible": false },
        { "targets": 11, "visible": false },
        { "targets": 12, "visible": false },
        { "targets": 13, "visible": false },
        { "targets": 14, "visible": false },
        { "targets": 15, "visible": false }
    ],
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('datatable_list_venta_individual');
   	}
} );

//******************************************************************************

//******************************************************************************
var unidadesnegocio = new Array();
var productos= new Array();
<x:forEach select="$ventareserva_productos_principales_xml/ArrayList/Entry" var="item">
	productos=[];
	<x:forEach select="$item/productos/Producto" var="producto">
		productos.push({value: '<x:out select="$producto/value" />', label: '<x:out select="$producto/label" />'});
	</x:forEach>
	unidadesnegocio['<x:out select="$item/unidadnegocio/nombre" />']= productos;
</x:forEach>

$("#idunidad_venta_individual").on("change", function(e) {
	showFieldSpinner("#div_producto_venta_individual");
	
	var data = $("#idunidad_venta_individual :selected").val();
	$select=$("#idproducto_venta_individual");
	
	/* Primero se a�aden los productos principales */
	
	$select.html('');
	unidadesnegocio[$("#idunidad_venta_individual :selected").text()].forEach( function(element) { 
	      $select.append('<option class="option-main-product" value="' + element.value + '">' + element.label + '</option>');
	});
	
	/* Luego se a�aden los subproductos por ajax */
	
	if (data!="") { // La unidad de negocio COMBINADAS no tiene id
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/venta/ventareserva/list_subproductos.do'/>",
			timeout : 100000,
			data: {
		    	   id: data.toString()
				  }, 
			success : function(data) {
				hideSpinner("#div_producto_venta_individual");
				if (data.ArrayList!="") {
					var item= data.ArrayList.LabelValue;
					if (item.length>0) {
						item=sortJSON(item,"label",true);
					    $.each(item, function(key, val){
					      $select.append('<option value="' + val.value + '">' + val.label + '</option>');
					    });
					}
					else
					      $select.append('<option value="' + item.value + '">' + item.label + '</option>');
				}
			},
			error : function(exception) {
				hideSpinner("#div_producto_venta_individual");
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",
					  buttons: { sticker: false }				  
				   });		
			}
		});	
	}
	else hideSpinner("#div_producto_venta_individual");

});
//********************************************************************************
$("#button_add_venta_individual").on("click", function(e) { 
	
	if ($("#idproducto_venta_individual option:selected").length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	

	//showButtonSpinner("#button_add_venta_individual");
	
	var formdata = $("#form_venta_reserva_modificar_venta_data").serializeObject();
	
	
	
	if ($("#idunidad_venta_individual :selected").val()!="" && $("#idproducto_venta_individual option:selected").hasClass("option-main-product")) {
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/venta/ventareserva/get_producto.do'/>",
			timeout : 100000,
			data: {
				idcliente: formdata.idcliente_venta_individual,
				idcanal: "${sessionScope.idcanal}",
				idtipoventa: 4, // Tipo Venta no numerada
				idproducto: formdata.idproducto_venta_individual,
				fecha: formdata.fecha_venta_individual,
				principal: $("#idproducto_venta_individual :selected").hasClass("option-main-product"),
				autosesion: "true"
			},
			success : function(data) {
				var contenido="",
					fecha_sesion= "",
					disponibles= "";
				item= data.Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion;
				if (item.length>0) {
					contenido= item[0].sesion.horainicio+" "+item[0].sesion.contenido.nombre;
					fecha_sesion= item[0].sesion.fecha;
					disponibles= item[0].numlibres;
				}
				else {
					contenido= item.sesion.horainicio+" "+item.sesion.contenido.nombre;
					fecha_sesion= item.sesion.fecha;
					disponibles= item.numlibres;
				}
				var tarifa= data.Lineadetalle.perfilvisitante.nombre;
				var importe= data.Lineadetalle.tarifaproducto.importe;
				dt_listventaindividual.row.add([data.Lineadetalle,
		                                	$("#idproducto_venta_individual :selected").val(), 
				                            $("#idproducto_venta_individual :selected").text(),
				                            fecha_sesion.split("-")[0],
				                            contenido,
				                            disponibles,
				                            1,  // N� Etd
				                            typeof tarifa!="undefined"?tarifa:"", // Perfil/Tarifa
				                            "", // Descuentos
				                            importe, // I. Unitario
				                            importe,  // Importe
				                            "", // Datos de las sesiones
				                            "", // Bono
				                            "", // idtarifa
				                            "",	// iddescuento
				                            "" 	// idperfil
			                               ]).draw();
				hideSpinner("#button_add_venta_individual");
				
			    obtener_totales_venta_temporal(dt_listventaindividual,idcliente,idtipoventa);

			},
			error : function(exception) {
				hideSpinner("#button_add_venta_individual");
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",
					  delay: 5000,
					  buttons: { closer:true, sticker: false }			  
				   });		
			}
		});	
	}
	else {
		
			
		
		
		
		$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/facturacion/ventareserva/show_disponibles.do'/>?idproducto="+formdata.idproducto_venta_individual+"&idcliente="+$("#idcliente").val()+"&idtipoventa=4&producto="+encodeURIComponent($("#idproducto_venta_individual option:selected").text())+"&fecha="+fechaoperacion+"&button=venta_individual&list=dt_listventaindividual", function() {
			$("#modal-dialog-form-3").modal('show');
			setModalDialogSize("#modal-dialog-form-3", "md");
		});	
		
		
	}

}); 

//********************************************************************************

var idcliente = $("#idcliente").val();
var idtipoventa = 4;
//*************************************************************************************
function obtener_totales_venta_temporal(dataTable,idcliente1,idtipoventa1) {
	

	var tabla_set= {};
	$("#total-val_mod_res").text(0);
	$("#total-desc_mod_res").text(0);
	$("#total-prc_mod_res").text(0);
	
	


	if(dataTable.rows().length>0)
		{
		var xml_lineasdetalle = construir_xml_lineasdetalle(dataTable.rows().data());
		
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/venta/ventareserva/obtener_totales_ventatemporal.do'/>",
			timeout : 100000,
			data: {
				idcliente: idcliente,
				idtipoventa: idtipoventa,
				xml: xml_lineasdetalle
			},
			success : function(data) {
				$("#total-val_mod_res").text(data.TotalVentaDTO.importeventa);
				$("#total-desc_mod_res").text(data.TotalVentaDTO.importedc);
				$("#total-prc_mod_res").text(data.TotalVentaDTO.porcentaje);
			},
			error : function(exception) {
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",
					  delay: 5000,
					  buttons: { closer:true, sticker: false }			  
				   });		
			}
		});	
	}
}

//****************************************************

/* Funci�n gen�rica para obtener el xml de las lineas de detalle en las operaciones de Venta y Reserva */
function construir_xml_lineasdetalle(lineas_detalle_sel) {
	
	var xml_lineasdetalle= "<lineadetalles>";

	for (var i=0; i<lineas_detalle_sel.length; i++) { 
		xml_lineasdetalle+="<Lineadetalle>";
		var lineaLoop = lineas_detalle_sel[i][0];
		var strLineaNueva = ""+lineaLoop.Lineadetalle;
		if(strLineaNueva!='undefined')
			lineaLoop = lineaLoop.Lineadetalle;
		xml_lineasdetalle+=json2xml(lineaLoop,"");
		xml_lineasdetalle+="</Lineadetalle>";
	}
	xml_lineasdetalle+="</lineadetalles>";
	return(xml_lineasdetalle);
}

//***************************************************
function cargarLineasParaVenta()
{
	
var fecha_sesion= "", contenido= "", disponibles= "",sesiones="", retorno= false;
var data = dt_reserva.rows().data();

data.each(function (value, index) {
	var lineaDetalle = dt_reserva.rows( index ).data()[0];
	CargarLineaVentaDetalle(lineaDetalle[0],dt_listventaindividual);
	});
	obtener_totales_venta_temporal(dt_listventaindividual,idcliente,idtipoventa)

}

//***************

function CargarLineaVentaDetalle(lineadetalle, tabla)
	{	
		
		var fechas= "", contenidos= "", disponibles= "",retorno= false;
		var impreso = lineadetalle.entradasimpresas+"/"+lineadetalle.entradasnoanuladas;
		var sesiones = lineadetalle.lineadetallezonasesions.Lineadetallezonasesion;
		var descuento_nombre=""+lineadetalle.descuentopromocional.nombre;
		
		var id_tarifa="",idperfilvisitante="",iddescuento="";
		var numbonoagencia= lineadetalle.numerobonoagencia;
		
		
		id_tarifa = lineadetalle.perfilvisitante.tarifa.idtarifa;
		idperfilvisitante = lineadetalle.perfilvisitante.idperfilvisitante;
		
		
		if(descuento_nombre=="undefined")
			descuento_nombre =" ";
		else
			iddescuento = lineadetalle.descuentopromocional.iddescuentopromocional;
				
		var contenidos;
		
		 if (sesiones.length>0)		 
		 {
				for (var i=0; i<sesiones.length; i++) {
					if (retorno) {
						fechas+="</br>";
						contenidos+= "</br>";
						disponibles+="</br>";
					}				
					var f= sesiones[i].zonasesion.sesion.fecha.split("-")[0];
					fechas+= f;					
					var hi=sesiones[i].zonasesion.sesion.horainicio;
					var  no=sesiones[i].zonasesion.sesion.contenido.nombre;
					contenidos+= hi+" "+no;
					var d=sesiones[i].zonasesion.numlibres;
					disponibles+=d;
					retorno= true;
				}	 
				
		 }
		 else
		 {
			fechas= lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.fecha.split("-")[0];
			contenidos= lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.horainicio+" "+lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.contenido.nombre;
			disponibles=lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.numlibres;			
				
		 }
				
		 dt_listventaindividual.row.add([
				     lineadetalle,
				     lineadetalle.producto.idproducto, 
				     lineadetalle.producto.nombre,
				   	 fechas,
					 contenidos,
				 	 disponibles,
				 	 lineadetalle.cantidad, 
				 	 lineadetalle.perfilvisitante.nombre, 
				 	 descuento_nombre, 
				 	 lineadetalle.tarifaproducto.importe,
  				 	 lineadetalle.importe,  // Importe
					 "", // Datos de las sesiones
					 "", // Bono
					 id_tarifa, // idtarifa
	                 iddescuento,	// iddescuento
	                 idperfilvisitante 	// idperfil
					 ]).draw();
				
				
}
//**********************************************
$("#button_venta_individual_sesiones").on("click", function(e) {
	
	if (dt_listventaindividual.rows( { selected: true }).data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	if (dt_listventaindividual.rows( { selected: true }).data().length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#button_venta_individual_sesiones");	
	
		
	var formdata = $("#form_venta_reserva_modificar_venta_data").serializeObject(),
		rowdata= dt_listventaindividual.rows( { selected: true }).data();
	
	var sesiones = rowdata[0][0].lineadetallezonasesions.Lineadetallezonasesion;
	var map = "<tipoProducto>";
	if (sesiones.length>=0)
		{
		for(i=0;i<sesiones.length;i++)
			{
			var session = sesiones[i].zonasesion.sesion;
			var id_producto_sesion = ""+session.contenido.tipoproducto.idtipoproducto;
			var fecha_sesion = session.fecha;
			map +="<entry><int>"+id_producto_sesion+"</int><date>"+fecha_sesion+"</date></entry>"			
			}
		}
	else
		{
		var session = sesiones.zonasesion.sesion;
		var id_producto_sesion = ""+session.contenido.tipoproducto.idtipoproducto;
		var fecha_sesion = session.fecha;
		map +="<entry><int>"+id_producto_sesion+"</int><date>"+fecha_sesion+"</date></entry>"
		}
	
	map +="</tipoProducto>";
	

	/*$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_disponibles.do'/>?idproducto="+rowdata[0][1]+"&idcliente="+formdata.idcliente_venta_individual+"&producto="+encodeURIComponent(rowdata[0][2])+"&fecha="+formdata.fecha_venta_individual+"&map="+map+"&button=venta_individual&list=dt_listventaindividual&edicion=1", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});*/
	
	
	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/facturacion/ventareserva/show_disponibles.do'/>?idproducto="+rowdata[0][1]+"&idcliente="+formdata.idcliente_venta_individual+"&producto="+encodeURIComponent(rowdata[0][2])+"&fecha="+formdata.fecha_venta_individual+"&map="+map+"&button=venta_individual&list=dt_listventaindividual&edicion=1", function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "md");
	});
		
	
});

//***************************************************************
$("#button_venta_individual_combinar").on("click", function(e) {
	
	if (dt_listventaindividual.rows( { selected: true }).data().length<=1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.combinados.no_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#button_venta_individual_combinar");	
	
	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/facturacion/show_list_combinados.do'/>?tabla=venta_individual&list=dt_listventaindividual", function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "xs");
	});
});

//********************************************************************************
$("#button_venta_individual_descombinar").on("click", function(e) {
	
	if (dt_listventaindividual.rows( { selected: true }).data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#button_venta_individual_descombinar");	
	
	if (dt_listventaindividual.rows( { selected: true }).data()[0][0].producto.tipoprodproductos.Tipoprodproducto.length>0)
	{
		showButtonSpinner("#button_venta_individual_descombinar");	
		
		$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/facturacion/show_list_productos.do'/>?tabla=venta_individual&list=dt_listventaindividual", function() {
            $("#modal-dialog-form-3").modal('show');
            setModalDialogSize("#modal-dialog-form-3", "xs");
      });

	}else
	{
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.producto_no_combinado" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		hideSpinner("#button_venta_individual_descombinar");	
		return;
	}
});
//********************************************************************************
$("#button_venta_individual_duplicar").on("click", function(e) { 
	
	
	var data= $.extend(true,[],dt_listventaindividual.rows( { selected: true } ).data());

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	dt_listventaindividual.rows( { selected: true } ).data()[0][0].idlineadetalle="";
	var lineaDetalleZonaSession = dt_listventaindividual.rows( { selected: true } ).data()[0][0].lineadetallezonasesions.Lineadetallezonasesion;
	
	if(lineaDetalleZonaSession.length>0)
		{
		for( i=0;i<lineaDetalleZonaSession.length;i++)
			dt_listventaindividual.rows( { selected: true } ).data()[0][0].lineadetallezonasesions.Lineadetallezonasesion[i].idlineadetallezonasesion = "";
		}
	else
		{
		dt_listventaindividual.rows( { selected: true } ).data()[0][0].lineadetallezonasesions.Lineadetallezonasesion.idlineadetallezonasesion = "";
		}
	
	
	dt_listventaindividual.rows.add(data).draw();
	
	var idcliente = $("#idcliente").val();
	var idtipoventa = 4;
    obtener_totales_venta_temporal(dt_listventaindividual,idcliente,idtipoventa);

}); 

//********************************************************************************
$("#button_venta_individual_eliminar").on("click", function(e) { 
		
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.confirm.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
		  buttons: { closer: false, sticker: false	},
		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
		   var lineas_seleccionadas =  dt_listventaindividual.rows( { selected: true } );
		   for(i=0;i<lineas_seleccionadas.length;i++)
			   lineasDetallesEliminadas.push(lineas_seleccionadas.data()[i][0]);
		   dt_listventaindividual.rows( { selected: true } ).remove().draw();
			var idcliente = $("#idcliente").val();
			var idtipoventa = 4;
		   obtener_totales_venta_temporal(dt_listventaindividual,idcliente,idtipoventa);
	   }).on('pnotify.cancel', function() {
	   });		 

}); 


//**********************************************
var celda;
$("#datatable_list_venta_individual tbody").delegate("td", "click", function() {
	celda=this;
	var posicionCelda=0;    	
	posicionCelda=$("td", celda).context.cellIndex;
	if (posicionCelda=="4" || posicionCelda=="5" || posicionCelda=="6")    		
		window.setTimeout(editarLineaDetalleVentaIndividual, 100);
});   

//***************************************************************************************

function editarLineaDetalleVentaIndividual()
{
	
	if (dt_listventaindividual.rows( { selected: true }).data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	if (dt_listventaindividual.rows( { selected: true }).data().length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	var data = $("#form_selector_venta_individual").serializeObject(),
	rowdata= dt_listventaindividual.rows( { selected: true }).data();

	
	
	var idproductolinea_detalle=dt_listventaindividual.rows( { selected: true }).data()[0][0].producto.idproducto;
	var nombre_producto=dt_listventaindividual.rows( { selected: true }).data()[0][2];
	var fecha=dt_listventaindividual.rows( { selected: true }).data()[0][3];
	var idtarifa=dt_listventaindividual.rows( { selected: true } ).data()[0][0].perfilvisitante.tarifa.idtarifa;
	//dt_listventaindividual.rows( { selected: true } ).data()[0][13];
	
	
	var idcliente = $("#idcliente").val();
	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/ventareserva/busqueda/venta/show_edit_sesion.do'/>?idproducto="+idproductolinea_detalle+"&idcliente="+idcliente+"&producto="+encodeURIComponent(nombre_producto)+"&fecha="+fecha+"&idtarifa="+idtarifa+"&idcanal=${sessionScope.idcanal}&idxnumero=6&idxunitario=9&idxtotal=10&idxbono=12&&idxtarifa_txt=7&idxtarifa_id=13&idxdescuento_txt=8&idxdescuento_id=14&idxperfil=15&button=venta_individual&list=dt_listventaindividual", function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "lg");
	});
}
//***************************************
$("#save_lineas_detalle_venta_button").on("click", function(e) {
	//Si alguno de los importes de alguna de las lineas de detalle es nulo no dejamos grabar
	
	for(i=0; i<dt_listventaindividual.rows().data().length;i++)
	{
		if(dt_listventaindividual.rows().data()[i][9]=="undefined"){
			new PNotify({
			      title: '<spring:message code="venta.ventareserva.dialog.edit_sesion.field.numero_lineadetalle" />',
			      text: "",
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
			return;
		}
	}
	
	var idventa =dt_reserva.rows().data()[0][1];
    var idcanal=$("#idcanal").val();
   
	var url= "<c:url value='/ajax/ventareserva/busqueda/venta/editarPago.do'/>?id="+idventa+"&importetotalventa="+$("#total-val_mod_res").text()+"&importetotalpagado="+$("#total-val").text()+"&idcanal="+${sessionScope.idcanal}+"&nombretabla=dt_listventaindividual";
	$("#modal-dialog-form-3 .modal-content").load(url, function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "sm");
	});
})


function actualizarResumen(id)
{
	$("#modal-dialog-form .modal-content").load("<c:url value='/venta/ventareserva/busqueda/ventareserva/editar_reserva.do'/>?id="+$("#idreserva").val(), function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
}

//********************************************************************************

$( "#button_add_venta_individual" ).hide();

$( "#idproducto_venta_individual" ).click(function() {    	
	$( "#button_add_venta_individual" ).click();
	});

</script>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_royalty}" var="editar_royalty_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	 <h4 class="modal-title">	
		<x:choose>
			<x:when select="not($editar_royalty_xml/Marca)">
				<spring:message code="facturacion.facturas.tabs.royalties.dialog.crear_royalty.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="facturacion.facturas.tabs.royalties.dialog.editar_royalty.title" />
			</x:otherwise>
		</x:choose>		
	</h4>	
</div>


<div class="modal-body">
	<form id="form_marca_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input id="idroyalty" name="idroyalty" type="hidden" value="<x:out select = "$editar_royalty_xml/Marca/idmarca" />" />
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="facturacion.facturas.tabs.royalties.field.nombre" />*</label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editar_royalty_xml/Marca/nombre" />">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="facturacion.facturas.tabs.royalties.field.descripcion" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<textarea name="descripcion" id="descripcion" class="form-control"><x:out select = "$editar_royalty_xml/Marca/descripcion" /></textarea>
				</div>
			</div>
				<div class="form-group date-picker">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="facturacion.facturas.tabs.royalties.field.comienzo" />:</label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<a type="button" class="btn btn-default btn-clear-date" id="button_comienzocalculo_clear">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.royalties.list.button.clear" />"> <span class="fa fa-trash"></span>
						</span>
					</a>			
                    <div class="input-prepend input-group">
                    	<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                        <input type="text" name="comienzocalculo" id="comienzocalculo" class="form-control" value="" readonly/>
                        <input type="hidden" required="required" name="fechacomienzocalculo" value="<x:out select = "$editar_royalty_xml/Marca/fechacomienzocalculoanual" />"/>
                    </div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="facturacion.facturas.tabs.royalties.field.umbral" />*</label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input name="umbral" id="umbral" type="text" data-inputmask="'mask': '9{0,10}.9{0,2}'" data-mask  class="form-control" required="required" value="<x:out select = "$editar_royalty_xml/Marca/umbralanualbase" />">
				</div>
			</div>		
			
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="facturacion.facturas.tabs.royalties.field.porcentaje" />*</label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input name="porcentaje" id="porcentaje" type="text" data-inputmask="'mask': '9{0,10}.9{0,2}'" data-mask  class="form-control" required="required" value="<x:out select = "$editar_royalty_xml/Marca/porcentajeroyalty" />">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="facturacion.facturas.tabs.royalties.field.importe" />*</label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input name="importe" id="importe" type="text" data-inputmask="'mask': '9{0,10}.9{0,2}'" data-mask  class="form-control" required="required" value="<x:out select = "$editar_royalty_xml/Marca/royaltyminimo" />">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="facturacion.facturas.tabs.royalties.field.tipo"/>*</label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input name="tipocambio" id="tipocambio" data-inputmask="'mask': '9{0,10}.9{0,2}'" data-mask type="text" class="form-control" required="required" value="<x:out select = "$editar_royalty_xml/Marca/tipocambiodolar" />">
				</div>
			</div>					
	</div>
	
	</form>
	<div class="modal-footer">

		<button id="save_marca_button" type="button" class="btn btn-primary save_marca_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>	

<script>
hideSpinner("#tab_royalty_new");
hideSpinner("#tab_royalty_edit");
//*********************************************
$(":input").inputmask();
//*********************************************
//dia comienzo c�lculo
$today= moment().format("DD/MM/YYYY");
dia_inicio = $today;

if($('input[name="fechacomienzocalculo"]').val()!='')
{	
dia_inicio = $('input[name="fechacomienzocalculo"]').val().substring(0, 10);
}

$('input[name="comienzocalculo"]').val( dia_inicio);

$('input[name="comienzocalculo"]').daterangepicker({
	singleDatePicker: true,
	autoUpdateInput: false,
	linkedCalendars: false,
	showDropdowns: true,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start) {
  	  	 $('input[name="comienzocalculo"]').val(start.format('DD/MM/YYYY'));
  	  	 $('input[name="fechacomienzocalculo"]').val(start.format('DD/MM/YYYY'));
  	  	 
	 });
	 
$("#button_comienzocalculo_clear").on("click", function(e) {
    $('input[name="comienzocalculo"]').val('');
    $('input[name="fechacomienzocalculo"]').val('');
    
});	 
//*******************************************************************************

$(".save_marca_dialog").on("click", function(e) {
	$("#form_marca_data").submit();	
})
//*******************************************************************************
$("#form_marca_data").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		guardarRoyalty()
	}
})
//*************************************************************************************
	function guardarRoyalty()
	{
		var data = $("#form_marca_data").serializeObject();
		
		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/facturacion/facturas/royalties/save_royalty.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				
				$("#modal-dialog-form").modal('hide');
				dt_listroyalties.ajax.reload(null,false);
								
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});

	}

</script>

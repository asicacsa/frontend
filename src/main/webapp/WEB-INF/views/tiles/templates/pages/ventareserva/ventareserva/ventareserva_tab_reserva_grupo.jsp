<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_productos_principales}" var="ventareserva_productos_principales_xml" />
<x:parse xml="${ventareserva_canales_indirectos}" var="ventareserva_canales_indirectos_xml" />

<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2><spring:message code="venta.ventareserva.selector" /></h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">
		<form id="form_selector_reserva_grupo" class="form-horizontal form-label-left">
			<!--  <input type="hidden" id="idcliente_reserva_grupo" name="idcliente_reserva_grupo" value=""/>-->
			<input type="hidden" id="cifcliente_reserva_grupo" value=""/>
			<input type="hidden" id="cpcliente_reserva_grupo" value=""/>
			<input type="hidden" id="emailcliente_reserva_grupo" value=""/>
			<input type="hidden" id="telefonocliente_reserva_grupo" value=""/>

			<div class="col-md-5 col-sm-5 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.emision_bono.field.cliente" /></label>
					<div class="col-md-2 col-sm-2 col-xs-12">
						<input name="idcliente_reserva_grupo" id="idcliente_reserva_grupo" style="height:10" type="text" class="form-control"><!--  <button id="buscarClienteIndividual">v</button>-->
					</div>
					<div class="col-md-7 col-sm-7 col-xs-12">
						<input name="cliente_reserva_grupo" id="cliente_reserva_grupo" type="text" class="form-control" readonly>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.emision_bono.field.canal" /></label>
					<div class="col-md-8 col-sm-8 col-xs-12">
						<select class="form-control" name="idcanal_reserva_grupo" id="idcanal_reserva_grupo">
							<option value=""></option>
							<x:forEach select="$ventareserva_canales_indirectos_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.emision_bono.field.fecha" /></label>
					<div class="col-md-9 col-sm-9 col-xs-12">
	                      <div class="input-prepend input-group">
	                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	                        <input type="text" name="fecha_reserva_grupo" id="fecha_reserva_grupo" class="form-control" readonly/>
	                      </div>
					</div>
				</div>
			</div>
			
			<div class="row botones-venta-directa">		
				<x:forEach select="$ventareserva_productos_principales_xml/ArrayList/Entry" var="item">
					<x:if select="not($item/unidadnegocio/idunidadnegocio='')">
						&nbsp;
						<x:forEach select="$item/productos/Producto" var="producto">
							<button type="button" id="reserva_grupo_<x:out select="$producto/value" />" value="<x:out select="$producto/value" />" class="btn btn-primary btn-reserva-grupo-directo"><x:out select="$producto/label" /></button>					
						</x:forEach>
						&nbsp;
					</x:if>
				</x:forEach>
			</div>
				
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label class="control-label"><spring:message code="venta.ventareserva.tabs.emision_bono.field.unidad" /></label>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<select class="form-control ventareserva" name="idunidad_reserva_grupo" id="idunidad_reserva_grupo" size="5" required="required">
							<x:forEach select="$ventareserva_productos_principales_xml/ArrayList/Entry/unidadnegocio" var="item">
								<option value="<x:out select="$item/idunidadnegocio" />"><x:out select="$item/nombre" /></option>
							</x:forEach>
						</select>
					</div>
				</div>
			</div>
				
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label class="control-label"><spring:message code="venta.ventareserva.tabs.emision_bono.field.producto" /></label>
					<div class="col-md-12 col-sm-12 col-xs-12" id="div_producto_reserva_grupo">
						<select class="form-control ventareserva" name="idproducto_reserva_grupo" id="idproducto_reserva_grupo" size="5" required="required">
							<option value=""></option>
						</select>
					</div>
				</div>				
			</div>
		
		</form>
		<div class="clearfix"></div>
		<div class="ln_solid"></div>
		<div class="col-md-12 col-sm-12 col-xs-12">
			<button id="button_add_reserva_grupo" type="button" class="btn btn-success pull-right">
				<spring:message code="venta.ventareserva.tabs.emision_bono.button.add" />
			</button>
			<button id="button_disp_reserva_grupo" type="button" class="btn pull-right">
				<spring:message code="venta.ventareserva.tabs.emision_bono.button.disp" />
			</button>
		</div>
	</div>
</div>

	
<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="totales-group totales_reserva_grupo">
		Total: <span class="total-lbl"><span id="total-val">0.00</span>&euro;</span>&nbsp;<span class="descuento-lbl">Descuento: <span id="total-desc">0.00</span>&euro;&nbsp;(<span id="total-prc">0.00</span>%)</span>
	</div>

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="button_reserva_grupo_sesiones">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.sesiones" />"> <span class="fa fa-history"></span>
			</span>
		</a>
    	 <a type="button" class="btn btn-info" id="button_reserva_grupo_multisesion">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.multisesion" />"> <span class="fa fa-flag"></span>
			</span>
		</a> 
		
		
		<a type="button" class="btn btn-info" id="button_reserva_grupo_horas">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.horas" />"> <span class="fa fa-clock-o"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="button_reserva_grupo_descombinar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.descombinar" />"> <span class="fa fa-object-ungroup"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="button_reserva_grupo_combinar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.combinar" />"> <span class="fa fa-object-group"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="button_reserva_grupo_prerreservar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.prerreservar" />"> <span class="fa fa-bookmark-o"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="button_reserva_grupo_reservar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.reservar" />"> <span class="fa fa-book"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="button_reserva_grupo_duplicar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.emision_bono.list.button.duplicar" />"> <span class="fa fa-clone"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="button_reserva_grupo_eliminar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.emision_bono.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="button_reserva_grupo_cancelar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.cancelar" />"> <span class="fa fa-close"></span>
			</span>
		</a>
	</div>

	<table id="datatable_list_reserva_grupo" class="table table-striped table-bordered dt-responsive ventareserva" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.idproducto" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.producto" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.fecha" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.contenido" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.disp" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.etd" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.perfil" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.descuentos" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.importeunitario" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.importe" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.sesiones" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.bono" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.idtarifa" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.iddescuento" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.idperfil" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>
$('input[name="fecha_reserva_grupo"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
	minDate: moment(),
  	locale: $daterangepicker_sp
});

var dt_listreservagrupo=$('#datatable_list_reserva_grupo').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	scrollCollapse: true,
	paging: false,
    select: { style: 'os' },
	columnDefs: [
        { "targets": 0, "visible": false },
        { "targets": 1, "visible": false },
        { "targets": 11, "visible": false },
        { "targets": 12, "visible": false },
        { "targets": 13, "visible": false },
        { "targets": 14, "visible": false },
        { "targets": 15, "visible": false }
    ],
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('datatable_list_reserva_grupo');
   	}
} );

$("#cliente_reserva_grupo").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>?editable=1", "#cliente_reserva_grupo", "#idcliente_reserva_grupo", "#cifcliente_reserva_grupo", "#cpcliente_reserva_grupo", "#emailcliente_reserva_grupo", "#telefonocliente_reserva_grupo");

$("#cliente_reserva_grupo").on("change", function(e) {
	obtener_totales_venta_temporal("reserva_grupo","reserva_grupo",$("#idcliente_reserva_grupo").val(),4);
});

$("#idunidad_reserva_grupo").on("change", function(e) { 
	showFieldSpinner("#div_producto_reserva_grupo");
	
	var data = $("#idunidad_reserva_grupo :selected").val();
	$select=$("#idproducto_reserva_grupo");
	
	/* Primero se a�aden los productos principales */
	
	$select.html('');
	unidadesnegocio[$("#idunidad_reserva_grupo :selected").text()].forEach( function(element) { 
	      $select.append('<option class="option-main-product" value="' + element.value + '">' + element.label + '</option>');
	});
	
	/* Luego se a�aden los subproductos por ajax */
	
	if (data!="") { // La unidad de negocio COMBINADAS no tiene id
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/venta/ventareserva/list_subproductos.do'/>",
			timeout : 100000,
			data: {
		    	   id: data.toString()
				  }, 
			success : function(data) {
				hideSpinner("#div_producto_reserva_grupo");
				if (data.ArrayList!="") {
					var item= data.ArrayList.LabelValue;
					if (item.length>0) {
						item=sortJSON(item,"label",true);
					    $.each(item, function(key, val){
					      $select.append('<option value="' + val.value + '">' + val.label + '</option>');
					    });
					}
					else
					      $select.append('<option value="' + item.value + '">' + item.label + '</option>');
				}
			},
			error : function(exception) {
				hideSpinner("#div_producto_reserva_grupo");
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",
					  buttons: { sticker: false }				  
				   });		
			}
		});	
	}
	else hideSpinner("#div_producto_reserva_grupo");

}); 	

//********************************************************************************
$(".btn-reserva-grupo-directo").on("click", function(e) {
	var formdata = $("#form_selector_reserva_grupo").serializeObject();
	
	showButtonSpinner(e.currentTarget);
	
	boton_venta_directa= "#"+this.id;
	
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_disponibles.do'/>?idproducto="+$(e.currentTarget).val()+"&idcliente="+formdata.idcliente_reserva_grupo+"&producto="+encodeURIComponent($(e.currentTarget).text())+"&fecha="+formdata.fecha_reserva_grupo+"&button=reserva_grupo&list=dt_listreservagrupo", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
	
});

//********************************************************************************
$("#button_add_reserva_grupo").on("click", function(e) { 
	
	if ($("#idproducto_reserva_grupo option:selected").length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	var formdata = $("#form_selector_reserva_grupo").serializeObject();
	
	showSpinner("#tab_reserva_grupo");	
	
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_disponibles.do'/>?idproducto="+formdata.idproducto_reserva_grupo+"&idcliente="+formdata.idcliente_reserva_grupo+"&producto="+encodeURIComponent($("#idproducto_reserva_grupo option:selected").text())+"&fecha="+formdata.fecha_reserva_grupo+"&button=reserva_grupo&list=dt_listreservagrupo", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
});

//********************************************************************************
/* $("#button_reserva_grupo_editar").on("click", function(e) {
	
	if (dt_listreservagrupo.rows( { selected: true }).data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	if (dt_listreservagrupo.rows( { selected: true }).data().length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#button_reserva_grupo_editar");	

	var data = $("#form_selector_reserva_grupo").serializeObject(),
		rowdata= dt_listreservagrupo.rows( { selected: true }).data();
	
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_edit_sesion.do'/>?idproducto="+rowdata[0][1]+"&idcliente="+data.idcliente_reserva_grupo+"&producto="+encodeURIComponent(rowdata[0][2])+"&fecha="+data.fecha_reserva_grupo+"&idtarifa="+dt_listreservagrupo.rows( { selected: true } ).data()[0][13]+"&idcanal=${sessionScope.idcanal}&idxnumero=6&idxunitario=9&idxtotal=10&idxbono=12&&idxtarifa_txt=7&idxtarifa_id=13&idxdescuento_txt=8&idxdescuento_id=14&idxperfil=15&button=reserva_grupo&list=dt_listreservagrupo", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "sm");
	});
}); */

//********************************************************************************
$("#button_reserva_grupo_sesiones").on("click", function(e) {
	
	if (dt_listreservagrupo.rows( { selected: true }).data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	if (dt_listreservagrupo.rows( { selected: true }).data().length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#button_reserva_grupo_sesiones");	
	
	var formdata = $("#form_selector_reserva_grupo").serializeObject();
	var rowdata= dt_listreservagrupo.rows( { selected: true }).data();
	
	var sesiones = rowdata[0][0].Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion;
	var map = "<tipoProducto>";
	if (sesiones.length>0)
		{
		for(i=0;i<sesiones.length;i++)
			{
			var session = sesiones[i].zonasesion.sesion;
			var id_producto_sesion = ""+session.contenido.tipoproducto.idtipoproducto;
			var fecha_sesion = session.fecha;
			map +="<entry><int>"+id_producto_sesion+"</int><date>"+fecha_sesion+"</date></entry>"			
			}
		}
	
	map +="</tipoProducto>";

	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_disponibles.do'/>?idproducto="+rowdata[0][1]+"&idcliente="+formdata.idcliente_reserva_grupo+"&producto="+encodeURIComponent(rowdata[0][2])+"&map="+map+"&fecha="+formdata.fecha_reserva_grupo+"&button=reserva_grupo&list=dt_listreservagrupo&edicion=1", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
});

//********************************************************************************
$("#button_reserva_grupo_duplicar").on("click", function(e) { 
		
	var data= $.extend(true,[],dt_listreservagrupo.rows( { selected: true } ).data());

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	dt_listreservagrupo.rows.add(data).draw();
	obtener_totales_venta_temporal("reserva_grupo","reserva_grupo",$("#idcliente_reserva_grupo").val(),4);

}); 

//********************************************************************************
$("#button_reserva_grupo_eliminar").on("click", function(e) { 
		
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.confirm.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
		  buttons: { closer: false, sticker: false	},
		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
			dt_listreservagrupo.rows( { selected: true } ).remove().draw();
			obtener_totales_venta_temporal("reserva_grupo","reserva_grupo",$("#idcliente_reserva_grupo").val(),4);
	   }).on('pnotify.cancel', function() {
	   });		 

}); 

//********************************************************************************
$("#button_reserva_grupo_cancelar").on("click", function(e) { 

	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.confirm.cancelar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
		  buttons: { closer: false, sticker: false	},
		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
			$.ajax({
				contenttype: "application/json; charset=utf-8",
				type : "post",
				url : "<c:url value='/ajax/venta/ventareserva/cancelar_prerreserva.do'/>",
				timeout : 100000,
				data: null,
				success : function(data) {
					dt_listreservagrupo.rows().remove().draw();
					$("#idcliente_reserva_grupo").val("");
			    	$("#cliente_reserva_grupo").val("");
			    	$("#cpcliente_reserva_grupo").val(""); 
					$("#emailcliente_reserva_grupo").val(""); 
					$("#telefonocliente_reserva_grupo").val(""); 
					$("#telefonomovilcliente_reserva_grupo").val("");
					obtener_totales_venta_temporal("reserva_grupo","reserva_grupo",$("#idcliente_reserva_grupo").val(),4);
				},
				error : function(exception) {
					new PNotify({
					      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					      text: exception.responseText,
						  type: "error",
						  delay: 5000,
						  buttons: { closer:true, sticker: false }			  
					   });		
				}
			});		   
	   }).on('pnotify.cancel', function() {
	   });		 

});

//********************************************************************************
$("#button_reserva_grupo_prerreservar").on("click", function(e) {
	
	showButtonSpinner("#button_reserva_grupo_prerreservar");
	
	var lineasdetalle= dt_listreservagrupo.rows().data();
	if (lineasdetalle.length <= 0) {
		hideSpinner("#button_reserva_grupo_prerreservar");
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.prerreserva_vacia" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/actualizar_prerreserva.do'/>",
		timeout : 100000,
		data: {
			xml: construir_xml_lineasdetalle(lineasdetalle)
		},
		success : function(data) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.prerreserva_realizada" />',
			      text: '<spring:message code="venta.ventareserva.list.alert.productos.prerreserva_realizada" />',
				  type: "info",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			
			hideSpinner("#button_reserva_grupo_prerreservar");
		},
		error : function(exception) {
			hideSpinner("#button_reserva_grupo_prerreservar");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});
});

//********************************************************************************
$("#button_reserva_grupo_reservar").on("click", function(e) {
	
	if (dt_listreservagrupo.rows().data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.venta_vacia" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
/*	if ($("#idcliente_reserva_grupo").val()=="") {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="venta.ventareserva.list.alert.productos.cliente_requerido" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
	}*/
	
	showButtonSpinner("#button_reserva_grupo_reservar");
	

	var data = $("#form_selector_reserva_grupo").serializeObject();

	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_vender_reserva_grupo.do'/>", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
});

//********************************************************************************
$("#button_reserva_grupo_combinar").on("click", function(e) {
	
	if (dt_listreservagrupo.rows( { selected: true }).data().length<=1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.combinados.no_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	showButtonSpinner("#button_reserva_grupo_combinar");	
	
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_list_combinados.do'/>?tabla=reserva_grupo&list=dt_listreservagrupo", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "xs");
	});
});

//********************************************************************************
$("#button_reserva_grupo_descombinar").on("click", function(e) {
	
	if (dt_listreservagrupo.rows( { selected: true }).data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#button_reserva_grupo_descombinar");	
	
	
	if (dt_listreservagrupo.rows( { selected: true }).data()[0][0].Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.length>0)
	{
		showButtonSpinner("#button_reserva_grupo_descombinar");	
		
		$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_list_productos.do'/>?tabla=reserva_grupo&list=dt_listreservagrupo", function() {
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "xs");
		});
	}else
	{
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.producto_no_combinado" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		hideSpinner("#button_reserva_grupo_descombinar");	
		return;
	}
	
	
	
});

//********************************************************************************
$("#button_reserva_grupo_horas").on("click", function(e) {
	showButtonSpinner("#button_reserva_grupo_horas");	

	var lineasdetalle= dt_listreservagrupo.rows().data();
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/show_hora_presentacion.do'/>",
		timeout : 100000,
		data: {
			button: "reserva_grupo_horas",
			list: "dt_listreservagrupo",
			xml: construir_xml_lineasdetalle(lineasdetalle)
		},
		success : function(data) {
			$("#modal-dialog-form .modal-content").html(data);
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "sm");
		},
		error : function(exception) {
			hideSpinner("#button_reserva_grupo_horas");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});
});

//********************************************************************************
$("#button_disp_reserva_grupo").on("click", function(e) {
	showButtonSpinner("#button_disp_reserva_grupo");	

	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_tabla_disponibilidad.do'/>?button=disp_reserva_grupo", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
});
//***********************************************************************************
 var celda;
    $("#datatable_list_reserva_grupo tbody").delegate("td", "click", function() {
    	celda=this;
    	var posicionCelda=0;    	
    	posicionCelda=$("td", celda).context.cellIndex;
    	if (posicionCelda=="4" || posicionCelda=="5" || posicionCelda=="6")
    		window.setTimeout(editarLineaDetalleReservaGrupo, 100);
    });
    //***************************************************************************************
    
    function editarLineaDetalleReservaGrupo()
    {
    	
    	if (dt_listreservagrupo.rows( { selected: true }).data().length<=0) {
    		new PNotify({
    		      title: '<spring:message code="common.dialog.text.error" />',
    		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
    			  type: "error",
    			  delay: 5000,
    			  buttons: { sticker: false }
    		   });
    		return;
    	}	
    	if (dt_listreservagrupo.rows( { selected: true }).data().length>1) {
    		new PNotify({
    		      title: '<spring:message code="common.dialog.text.error" />',
    		      text: '<spring:message code="venta.ventareserva.list.alert.productos.seleccion_multiple" />',
    			  type: "error",
    			  delay: 5000,
    			  buttons: { sticker: false }
    		   });
    		return;
    	}	
    	
    	var data = $("#form_selector_reserva_grupo").serializeObject(),
		rowdata= dt_listreservagrupo.rows( { selected: true }).data();
    	
    	var sesiones = rowdata[0][0].Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion;
		var map = "<tipoProducto>";
		if (sesiones.length>0)
			{
			for(i=0;i<sesiones.length;i++)
				{
				var session = sesiones[i].zonasesion.sesion;
				var id_producto_sesion = ""+session.contenido.tipoproducto.idtipoproducto;
				var fecha_sesion = session.fecha;
				map +="<entry><int>"+id_producto_sesion+"</int><date>"+fecha_sesion+"</date></entry>"			
				}
			}
		else
			{
			var session = sesiones.zonasesion.sesion;
			var id_producto_sesion = ""+session.contenido.tipoproducto.idtipoproducto;
			var fecha_sesion = session.fecha;
			map +="<entry><int>"+id_producto_sesion+"</int><date>"+fecha_sesion+"</date></entry>"			
			}
		
		map +="</tipoProducto>";
	
	showSpinner("#tab_reserva_grupo");	
		
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_edit_sesion.do'/>?idproducto="+rowdata[0][1]+"&idcliente="+data.idcliente_reserva_grupo+"&producto="+encodeURIComponent(rowdata[0][2])+"&fecha="+data.fecha_reserva_grupo+"&idtarifa="+dt_listreservagrupo.rows( { selected: true } ).data()[0][13]+"&map="+map+"&idcanal=${sessionScope.idcanal}&idxnumero=6&idxunitario=9&idxtotal=10&idxbono=12&&idxtarifa_txt=7&idxtarifa_id=13&idxdescuento_txt=8&idxdescuento_id=14&idxperfil=15&button=reserva_grupo&list=dt_listreservagrupo", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "lg");
	});
    }
    
    $( "#button_add_reserva_grupo" ).hide();
    
    $( "#idproducto_reserva_grupo" ).click(function() {    	
    	$( "#button_add_reserva_grupo" ).click();
    	});
    
    $("#idcliente_reserva_grupo").blur(function() {
    	var idCliente =""+$("#idcliente_reserva_grupo").val(); 
    	
    	if(idCliente!="")
    	{
    	$.ajax({
    		contenttype: "application/json; charset=utf-8",
    		type : "post",
    		url : "<c:url value='/ajax/facturacion/buscarClientePorId.do'/>",
    		timeout : 100000,
    		data: {
    			idcliente: idCliente,    			
    		},
    		success : function(data) {    		     
    				$("#cliente_reserva_grupo").val(data.Cliente.nombrecompleto); 
    				$("#cifcliente_reserva_grupo").val(data.Cliente.identificador); 
    				$("#cpcliente_reserva_grupo").val(data.Cliente.direccionorigen.cp.cp); 
    				$("#emailcliente_reserva_grupo").val(data.Cliente.direccionorigen.correoelectronico); 
    				$("#telefonocliente_reserva_grupo").val(data.Cliente.direccionorigen.telefonofijo); 
    				$("#telefonomovilcliente_reserva_grupo").val(data.Cliente.direccionorigen.telefonomovil);
    				obtener_totales_venta_temporal("reserva_grupo","reserva_grupo",$("#idcliente_reserva_grupo").val(),4);
    				
    				
    		},
    		error : function(exception) {    	
    			$("#idcliente_reserva_grupo").val("");
		    	$("#cliente_reserva_grupo").val("");
		    	$("#cpcliente_reserva_grupo").val(""); 
				$("#emailcliente_reserva_grupo").val(""); 
				$("#telefonocliente_reserva_grupo").val(""); 
				$("#telefonomovilcliente_reserva_grupo").val("");
				obtener_totales_venta_temporal("reserva_grupo","reserva_grupo",$("#idcliente_reserva_grupo").val(),4);

    			new PNotify({
    			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
    			      text: exception.responseText,
    				  type: "error",
    				  delay: 5000,
    				  buttons: { closer:true, sticker: false }			  
    			   });		
    		}
    	});
    	}
    	else
    		{
    		$("#idcliente_reserva_grupo").val("");
	    	$("#cliente_reserva_grupo").val("");
	    	$("#cpcliente_reserva_grupo").val(""); 
			$("#emailcliente_reserva_grupo").val(""); 
			$("#telefonocliente_reserva_grupo").val(""); 
			$("#telefonomovilcliente_reserva_grupo").val("");
			obtener_totales_venta_temporal("reserva_grupo","reserva_grupo",$("#idcliente_reserva_grupo").val(),4);
    		}
    })
    
     //********************************************************************************
	$("#button_reserva_grupo_multisesion").on("click", function(e) {
		
		tabla = dt_listreservagrupo;
		var filas = tabla.rows().data();
		var xml = "<parametro><arrayProductoTipoProductoYFechas>";
		
		
		if(filas.length>0)
			{

			for(i=0;i<filas.length;i++)
				{
				
				var lineaDetalle = filas[i][0].Lineadetalle;				
				xml+="<ProductoTipoProductoYFechas>";
				xml+="<idProducto>"+lineaDetalle.producto.idproducto+"</idProducto>";
				var sesiones = lineaDetalle.lineadetallezonasesions.Lineadetallezonasesion;
				xml += "<tipoProducto>";
				if (sesiones.length>0)
					{
					for(j=0;j<sesiones.length;j++)
						{
						var session = sesiones[j].zonasesion.sesion;
						var id_producto_sesion = ""+session.contenido.tipoproducto.idtipoproducto;
						var fecha_sesion = session.fecha;
						xml +="<entry><int>"+id_producto_sesion+"</int><date>"+fecha_sesion+"</date></entry>"			
						}
					}
				else
					{
					var session = sesiones.zonasesion.sesion;
					var id_producto_sesion = ""+session.contenido.tipoproducto.idtipoproducto;
					var fecha_sesion = session.fecha;
					xml +="<entry><int>"+id_producto_sesion+"</int><date>"+fecha_sesion+"</date></entry>"			
					}
				
				xml +="</tipoProducto>";
				xml+="</ProductoTipoProductoYFechas>";				
				}			
			}
			
		else
			{			
			lineaDetalle = filas[0][0].Lineadetalle;
			xml+="<ProductoTipoProductoYFechas>";
			xml+="<idProducto>"+lineaDetalle.producto.idproducto+"</idProducto>";
			var sesiones = lineaDetalle.lineadetallezonasesions.Lineadetallezonasesion;
			xml+="<tipoProducto>";
			if (sesiones.length>0)
				{
				for(i=0;i<sesiones.length;i++)
					{
					var session = sesiones[i].zonasesion.sesion;
					var id_producto_sesion = ""+session.contenido.tipoproducto.idtipoproducto;
					var fecha_sesion = session.fecha;
					xml +="<entry><int>"+id_producto_sesion+"</int><date>"+fecha_sesion+"</date></entry>"			
					}
				}
			else
				{
				var session = sesiones.zonasesion.sesion;
				var id_producto_sesion = ""+session.contenido.tipoproducto.idtipoproducto;
				var fecha_sesion = session.fecha;
				xml +="<entry><int>"+id_producto_sesion+"</int><date>"+fecha_sesion+"</date></entry>"			
				}
			
			xml +="</tipoProducto>";			
			xml+="</ProductoTipoProductoYFechas>";
			}
			
		xml+="</arrayProductoTipoProductoYFechas>";
		xml+="<fecha>"+$("#fecha_venta_individual").val()+"</fecha>";
		xml +="</parametro>";
		
		var formdata = $("#form_selector_venta_individual").serializeObject();
				
		$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_multisesion.do'/>?idproducto=1&idcliente="+formdata.idcliente_venta_individual+"&producto=1&fecha="+formdata.fecha_venta_individual+"&xml="+xml+"&button=venta_individual&list=dt_listventaindividual&edicion=1", function() {
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "md");
		});
	});


</script>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_datos_subrecinto}" var="editar_datos_subrecinto_xml" />
<x:parse xml="${editar_datos_recinto_selector_unidades}" var="editar_datos_recinto_selector_unidades_xml" />



<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">
		<x:choose>
			<x:when select="not($editar_datos_subrecinto_xml/Recinto)">
				<spring:message code="administracion.recintos.tabs.subrecintos.dialog.crear_zona.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.recintos.tabs.subrecintos.dialog.editar_zona.title" />
			</x:otherwise>
		</x:choose>
	</h4>
</div>

<div class="modal-body">
	<form id="form_recinto_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input id="idPadre" name="idPadre" type="hidden" value="${id_padre}" />
	    <input id="idSubrecinto" name="idrecinto" type="hidden" value="<x:out select = "$editar_datos_subrecinto_xml/Recinto/idrecinto" />" />
	    <input id="zonasSeleccionadas" name="zonasSeleccionadas" type="hidden"/>
	    <div class="col-md-6 col-sm-6 col-xs-6">
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="administracion.recintos.tabs.recintos.field.nombre" />*</label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input name="nombre" id="nombre" type="text" class="form-control required" required="required" value="<x:out select = "$editar_datos_subrecinto_xml/Recinto/nombre" />">
				</div>
			</div>	
			
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="administracion.recintos.tabs.recintos.field.horario" /></label>
				<div class="col-md-3 col-sm-3 col-xs-3">
					<input name="horaapertura" id="horaapertura" data-inputmask="'mask': '99:99'" type="text" class="form-control" value="<x:out select = "$editar_datos_subrecinto_xml/Recinto/horaapertura" />">
				</div>
				<label class="control-label col-md-1 col-sm-1 col-xs-1"><spring:message code="administracion.recintos.tabs.recintos.field.horario.hasta" /></label>
				<div class="col-md-3 col-sm-3 col-xs-3">
					<input name="horacierre" id="horacierre" type="text" data-inputmask="'mask': '99:99'" class="form-control" value="<x:out select = "$editar_datos_subrecinto_xml/Recinto/horacierre" />">
				</div>	
				<div class="col-md-1 col-sm-1 col-xs-1"></div>
			</div>	
			
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="administracion.recintos.tabs.recintos.field.reimpresiones" />*</label>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<input name="maxreimpresiones" id="maxreimpresiones" type="text" data-inputmask="'mask': '9{0,10}'" class="form-control required" value="<x:out select = "$editar_datos_subrecinto_xml/Recinto/maxreimpresiones" />">
				</div>
				<div class="col-md-5 col-sm-5 col-xs-5"></div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="administracion.recintos.tabs.recintos.field.orden" /></label>
				<div class="col-md-2 col-sm-2 col-xs-2">
					<input name="orden" id="orden" type="text" class="form-control" data-inputmask="'mask': '9{0,10}'" value="<x:out select = "$editar_datos_subrecinto_xml/Recinto/orden" />">
				</div>
				<div class="col-md-5 col-sm-5 col-xs-5"></div>				
			</div>		
			
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="administracion.recintos.tabs.recintos.field.overbooking" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7">	
				   	<input name="overbooking" type="checkbox" class="js-switch" <x:if select="$editar_datos_subrecinto_xml/Recinto/overbooking='true'">checked=""</x:if> data-switchery="true" style="display: none;">
				</div>
			</div>	
			
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="administracion.recintos.tabs.recintos.field.bloqueado" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7">	
				   	<input name="bloqueado" type="checkbox" class="js-switch" <x:if select="$editar_datos_subrecinto_xml/Recinto/bloqueado='true'">checked=""</x:if> data-switchery="true" style="display: none;">
				</div>
			</div>	
			
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="administracion.recintos.tabs.recintos.field.admitesolapamiento" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7">	
				   	<input name="admitesolapamiento" type="checkbox" class="js-switch" <x:if select="$editar_datos_subrecinto_xml/Recinto/admitesolapamiento='1'">checked=""</x:if> data-switchery="true" style="display: none;">
				</div>
			</div>	
		</div>
		<div class="col-md-6 col-sm-6 col-xs-6">	
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.recintos.tabs.recintos.field.taquillas" /></label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<select name="taquillas[]" id="selector_taquillas" class="select-select2 select2_multiple form-control" multiple="multiple"  style="width: 100%" >
						<x:forEach select="$editar_datos_subrecinto_xml/Recinto/taquillas/Taquilla" var="item">
							<option selected="true" value="<x:out select="$item/idtaquilla" />"><x:out select="$item/nombre" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			
			<div class="form-group" hidden>
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.recintos.tabs.recintos.field.unidades" />*</label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<select name="unidades[]" id="selector_unidades" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
						<x:forEach select="$editar_datos_recinto_selector_unidades_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>	
			
			
			
			<div class="form-group" id="div_recintos" hidden>
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.recintos.tabs.recintos.field.subrecintos" /></label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<div class="btn-group pull-right btn-datatable">
						<a type="button" class="btn btn-info" id="tab_subrecintos_new">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.subrecintos.list.button.nuevo" />"> <span class="fa fa-plus"></span>
							</span>
						</a>
						<a type="button" class="btn btn-info" id="tab_subrecintos_edit">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.subrecintos.list.button.editar" />"> <span class="fa fa-pencil"></span>
							</span>
						</a>
						<a type="button" class="btn btn-info" id="tab_subrecintos_remove">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.subrecintos.list.button.eliminar" />"> <span class="fa fa-trash"></span>
							</span>
						</a>		
					</div>			
					<table id="subrecintos_table" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
							  <th id="id"></th>
							  <th id="nombre"><spring:message code="administracion.recintos.tabs.zona.field.nombre"/></th>
							</tr>
						</thead>
						<tbody>				
							<x:forEach select="$editar_datos_subrecinto_xml/Recinto/recintos/Recinto" var="item">
							   <tr class="seleccionable">
							   		<td id="id" ><x:out select="$item/idrecinto" /></td>
									<td id="nombre"><x:out select="$item/nombre" /></td>
								</tr>
							</x:forEach>
						</tbody>												
					</table>			
				</div>
			</div>
			
					
			<div class="form-group" id="div_zonas">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.recintos.tabs.recintos.field.zonas" />*</label>
				<div class="col-md-8 col-sm-8 col-xs-8">			
					<div class="btn-group pull-right btn-datatable">
						<a type="button" class="btn btn-info" id="tab_zonas_new">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.zonas.list.button.nuevo" />"> <span class="fa fa-plus"></span>
							</span>
						</a>
						<a type="button" class="btn btn-info" id="tab_zonas_edit">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.zonas.list.button.editar" />"> <span class="fa fa-pencil"></span>
							</span>
						</a>
						<a type="button" class="btn btn-info" id="tab_zonas_remove">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.zonas.list.button.eliminar" />"> <span class="fa fa-trash"></span>
							</span>
						</a>		
					</div>			
			
			
					<table id="zonas_table" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
							  <th id="id"></th>
							  <th id="nombre"><spring:message code="administracion.recintos.tabs.zona.field.nombre"/></th>
							  <th id="aforo"><spring:message code="administracion.recintos.tabs.zona.field.aforo" /></th>
							  <th id="descripcion"></th>
							  <th id="orden" ></th>
							  <th id="numerada"></th>
 							  <th id="i18n_zona_nombre"></th>
							  <th id="i18n_zona_descripcion"></th>							  
							</tr>
						</thead>
						<tbody>				
							<x:forEach select="$editar_datos_subrecinto_xml/Recinto/zonas/Zona" var="item">
							   <tr class="seleccionable">
							   		<td id="id" ><x:out select="$item/idzona" /></td>
									<td id="nombre"><x:out select="$item/nombre" /></td>
									<td id="aforo"><x:out select="$item/aforo" /></td>
									<td id="descripcion" ><x:out select="$item/descripcion" /></td>
									<td id="orden" ><x:out select="$item/orden" /></td>
									<td id="numerada" ><x:out select="$item/numerada" /></td>
									<td id="i18n_zona_nombre" ><x:forEach select="$item/i18ns/I18n" var="item_i18n"><x:out select="$item_i18n/idi18n" />~<x:out select="$item_i18n/idioma/ididioma" />~<x:out select="$item_i18n/nombre" />^</x:forEach></td>																			
									<td id="i18n_zona_descripcion" ><x:forEach select="$item/i18ns/I18n" var="item_i18n"><x:out select="$item_i18n/idi18n" />~<x:out select="$item_i18n/idioma/ididioma" />~<x:out select="$item_i18n/descripcion" />^</x:forEach></td>								

								</tr>
							</x:forEach>
						</tbody>				
					</table>
				</div>
			</div>
		</div>
		
	</form>
	
	<div class="modal-footer">

		<button id="save_recinto_button" type="button" class="btn btn-primary save_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn close_dialog2">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>

<script>

$(":input").inputmask();
//*********************************************

var dtzonas;

$(document).ready(function() {
		dtzonas=$('#zonas_table').DataTable( {
			"paging": false,
			"info": false,
			"searching": false,
			select: { style: 'os'},
			language: dataTableLanguage,
			 "columnDefs": [
			                { "visible": false, "targets": [0,3,4,5,6,7]}
			              ]
			});
		
		dtsubrecintos=$('#subrecintos_table').DataTable( {
			"paging": false,
			"info": false,
			"searching": false,
			select: { style: 'os'},
			language: dataTableLanguage,
			 "columnDefs": [
			                { "visible": false, "targets": [0]}
			              ]
			});		
} );



$(".js-switch").each(function(index) {
	var s = new Switchery(this, {
		size : 'small'
	});
});


$("#tab_zonas_new").on("click", function(e) { 
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/recintos/recintos/show_zona.do'/>?subrecinto=1", function() {										  
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "sm");
	});
})






$("#tab_zonas_edit").on("click", function(e) { 
	
	
	if(dtzonas.rows( { selected: true } ).data().length==1)		
    {
		var datos = dtzonas.rows( '.selected' ).data()[0];
		var creando = false;
		var id=datos[0];
		var nombre=datos[1];
		var aforo=datos[2];
		var descripcion=datos[3];
		var orden=datos[4];
		var numerada=datos[5];
		var i18n_nombre=datos[6];
		var i18n_descripcion=datos[7];		
	
		var url = "<c:url value='/ajax/admon/recintos/recintos/show_zona.do'/>?subrecinto=1&creando="+creando+"&id="+id+"&orden="+orden+"&numerada="+numerada+"&nombre="+nombre+"&descripcion="+descripcion+"&aforo="+aforo+"&i18n_nombre="+i18n_nombre+"&i18n_descripcion="+i18n_descripcion;

		$("#modal-dialog-form-2 .modal-content").load(url, function() {										  
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "sm");
		});
    }
    else
    	{
    	if(dtzonas.rows( { selected: true } ).data().length==0)
	    	{
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="administracion.recintos.tabs.zonas.list.alert.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
	    	}
	    	
		else
			{
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="administracion.recintos.tabs.zonas.list.alert.seleccion_multiple" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			}
		}
})

	$("#tab_zonas_remove").on("click", function(e) { 
			dtzonas
			    .rows( '.selected' )
			    .remove()
			    .draw();
		   
	})



	
	$(".select-select2").select2({
		language : "es",
		containerCssClass : "single-line"
	});
	
	var selectedValues = new Array();
	<x:forEach select="$editar_datos_subrecinto_xml/Recinto/recintounidadnegocios/Recintounidadnegocio/unidadnegocio" var="item">
	selectedValues.push('<x:out select="$item/idunidadnegocio" />');
	</x:forEach>
	$('#selector_unidades').val(selectedValues);
	$('#selector_unidades').trigger('change.select2');
	

	$("#save_recinto_button").on("click", function(e) {
		$("#form_recinto_data").submit();	
	})

	$("#form_recinto_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			validateZonasRecintos();
		}
	}
	);

	function validateZonasRecintos()
	{
		if(dtzonas.rows().data().length==0)
		{
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="administracion.recintos.tabs.zonas.list.alert.ninguna_zona.error" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;	    		
		}
		else
			{
			var data = dtzonas.rows().data();
			 $("#zonas_seleccionados").val("");
			 var zonas = "";
			 data.each(function (value, index) {
				var linea = ""+dtzonas.rows( index ).data()[0][0]+"|";
				linea = linea + dtzonas.rows( index ).data()[0][1]+"|";
				linea = linea + dtzonas.rows( index ).data()[0][2]+"|";
				linea = linea + dtzonas.rows( index ).data()[0][3]+"|";
				linea = linea + dtzonas.rows( index ).data()[0][4]+"|";
				linea = linea + dtzonas.rows( index ).data()[0][5];
				
				if(zonas!="")
					zonas = zonas + "$";
				zonas = zonas + linea;				
			 });
		
			 $("#zonasSeleccionadas").val(zonas);
			 saveSubRecintosFormData();			
			}
	}
	
	//********************************************************************************	
	function saveSubRecintosFormData() 
	{
		var data = $("#form_recinto_data").serializeObject();
		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/recintos/recinto/save_subRecinto.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				modificadoPuntoRecogida = true;
				
				//$("#modal-dialog-form").modal('hide');
				
				$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/recintos/recintos/show_recinto.do'/>?id="+$("#idPadre").val(), function() {
					$("#modal-dialog-form").modal('show');
					setModalDialogSize("#modal-dialog-form", "");
				});
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});		
	}

	
	
	//********************************************************************************
	$(".close_dialog2").on("click", function(e) { 
		$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/recintos/recintos/show_recinto.do'/>?id="+$("#idPadre").val(), function() {
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "");
		});
	})
	
	//********************************************************************************
	$("#tab_subrecintos_remove").on("click", function(e) { 
			
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="administracion.recintos.tabs.subrecintos.list.confirm.eliminar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
	  		  buttons: { closer: false, sticker: false	},
	  		  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
		
			   dtsubrecintos.processing(true);
				
			   var subrecintos = dtsubrecintos.rows(".selected").data();
			   var data=[];
			
			 
				 subrecintos.each(function (value, index) {
					data.push(dtsubrecintos.rows( index ).data()[0][0]); 
				 })			   
			   	$.ajax({
					contenttype: "application/json; charset=utf-8",
					type : "post",
					url : "<c:url value='/ajax/admon/recintos/recintos/remove_recintos.do'/>",
					timeout : 100000,
					data: { data: data.toString() }, 
					success : function(data) {
						dtsubrecintos
					    .rows( '.selected' )
					    .remove()
					    .draw();				
					},
					error : function(exception) {
						dtsubrecintos.processing(false);
						
						new PNotify({
						      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						      text: exception.responseText,
							  type: "error",		     
							  delay: 5000,
							  buttons: { sticker: false }
						   });			
					}
				});

		   }).on('pnotify.cancel', function() {
		   });		 

	}); 	
</script>-
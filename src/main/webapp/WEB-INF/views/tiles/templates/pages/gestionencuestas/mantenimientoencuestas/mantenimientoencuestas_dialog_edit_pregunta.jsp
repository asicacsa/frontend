<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_pregunta}" var="editar_pregunta_xml" />
<x:parse xml="${traduccion_pregunta}" var="traduccion_pregunta_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	 <h4 class="modal-title">	
		<x:choose>
			<x:when select="not($editar_pregunta_xml/Pregunta)">
				<spring:message code="encuestas.mantenimientoencuestas.dialog.crear_pregunta.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="encuestas.mantenimientoencuestas.dialog.editar_pregunta.title" />
			</x:otherwise>
		</x:choose>		
	</h4>	
</div>


<div class="modal-body">
	<form id="form_pregunta_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input id="xml_datos_pregunta" name="xml_datos_pregunta" type="hidden" />
		<input id="id_pregunta" name="id_pregunta" type="hidden" value="<x:out select = "$idpregunta"/>" />
		<input id="cod_tiporespuesta" name="cod_tiporespuesta" type="hidden" value="<x:out select = "$editar_pregunta_xml/Pregunta/tipoRespuesta/cod"/>" />
		<input id="xmllist_respuestas" name="xmllist_respuestas" type="hidden" />
		<input id="idpregunta_nueva" name="idpregunta_nueva" type="hidden" value="<x:out select = "$editar_pregunta_xml/Encuesta/preguntaEncuestas/PreguntaEncuesta/id/codPregunta"/>" />	
		<input id="i18nFieldTexto_pregunta" name="i18nFieldTexto_pregunta" type="hidden" value="<x:forEach select="$traduccion_pregunta_xml/TreeSet/Pregunta" var="item"><x:out select="$item/id/codPregunta" />~<x:out select="$item/id/codIdioma" />~<x:out select="$item/des" />^</x:forEach>" />
		
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.field.texto" />*</label>
			<div class="col-md-9 col-sm-9 col-xs-12">	
				<input name="texto" id="texto" required="required" class="form-control"  />	
			</div>							
		</div>
		<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.field.Tiporespuesta" /></label>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<select name="tiporespuesta" id="tiporespuesta" required="required" class="form-control">
				<option value="0"><spring:message code="encuestas.mantenimientoencuestas.dialog.editar_pregunta.text.abierta" /></option>
				<option value="1"><spring:message code="encuestas.mantenimientoencuestas.dialog.editar_pregunta.text.discreta" /></option>
			</select>
		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12" id="respuestas"  hidden>
			<div class="btn-group pull-right btn-datatable">
				<a type="button" class="btn btn-info" id="tab_respuestas_new">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.list.button.nuevo" />"> <span class="fa fa-plus"></span>
					</span>
				</a>
				<a type="button" class="btn btn-info" id="tab_respuestas_edit">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.list.button.editar" />"> <span class="fa fa-pencil"></span>
					</span>
				</a>
				<a type="button" class="btn btn-info" id="tab_respuestas_remove">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.list.button.borrar" />"> <span class="fa fa-trash"></span>
					</span>
				</a>
			</div>
			<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.field.listadorespuestas" /></label>
			<table id="table-listarespuestas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%" >
				<thead>
					<tr>	
						<th id="codres"></th>						
						<th id="textores"><spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.list.header.texto" /></th>
					</tr>
				</thead>
				<tbody>		
					<x:forEach select="$editar_pregunta_xml/Pregunta/tipoRespuesta/valorRespuestas/ValorRespuesta" var="item">
						<tr>							
							<td id="codres"><x:out select="$item/id/codValorResp" /></td>
							<td id="textores"><x:out select="$item/valorResp" />								
							</td>					
						</tr>
					</x:forEach>
				</tbody>
			</table>
		</div>
		
	</form>
	
	<br/>
	<div class="modal-footer">
		<button id="save_pregunta_button" type="button" class="btn btn-primary save_pregunta_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>	

<script>


$("#texto").i18nField_Encuesta("#i18nFieldTexto_pregunta",<c:out value="${sessionScope.i18nlangsEncuesta}"  escapeXml="false" />);
//*****************************************************************************
var count = 0;
$(document).ready(function() {
	dtrespuestas=$('#table-listarespuestas').DataTable( {
		"paging": false,
		"info": false,
		"searching": false,
		select: { style: 'os'},
		language: dataTableLanguage,
		 "columnDefs": [
		                { "visible": false, "targets": [0]}
		              ]
		});
} );


//******************************************************************************
<c:if test="${not empty idpregunta}">
	var datos = dtpreguntas.rows( '.selected' ).data()[0];
	$('input[name="texto"]').val(datos[2]);
	
	var respuesta = datos[1];
	
		
	if (respuesta > 0)	{
		$('#tiporespuesta option[value="1"]').attr("selected", "selected");
		$("#respuestas").show();	
	}else	
		$('#tiporespuesta option[value="0"]').attr("selected", "selected");
	
	$("#tiporespuesta").attr("disabled", true);
</c:if>


//Si es un alta el botón de internacionalización se desactiva

<x:if select="not($editar_pregunta_xml/Pregunta)">
	$("#texto-i18n-button").prop("disabled",true);
</x:if>

//************************************NUEVA RESPUESTA*********************************

$("#tab_respuestas_new").on("click", function(e) {
	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/show_respuesta.do'/>?codtiporespuesta="+$("#cod_tiporespuesta").val(), function() {										  
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "xs");
	});	 
})
//***************************************EDITAR RESPUESTA***********************
	
	$("#tab_respuestas_edit").on("click", function(e) { 
		var codtiporespuesta=$("#cod_tiporespuesta").val();
		var destiporespuesta=$("#tiporespuesta option:selected").text();
		var codvalorrespuesta=dtrespuestas.rows('.selected').data()[0][0];
		var valorrespuesta=dtrespuestas.rows('.selected').data()[0][1];
		
				
		var url = "<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/show_respuesta.do'/>?"+encodeURI("codtiporespuesta="+codtiporespuesta+"&destiporespuesta="+destiporespuesta+"&codvalorrespuesta="+codvalorrespuesta+"&valorrespuesta="+valorrespuesta);
		
        
        $("#modal-dialog-form-3 .modal-content").load(url, function() {
               $("#modal-dialog-form-3").modal('show');
               setModalDialogSize("#modal-dialog-form-3", "xs");
        });

		
		
	})
//******************************************************
$("#save_pregunta_button").on("click", function(e) {
			$("#form_pregunta_data").submit();	
		})
//************************************************************
$("#form_pregunta_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFormDataPregunta();
		}
	}
	);
	//*************************************GUARDAR PREGUNTA*******************************************	
	function saveFormDataPregunta() 
	{		
		
		showSpinner("#modal-dialog-form .modal-content");	
		
		//EDITAR PREGUNTA
		if($("#id_pregunta").val()!="" && $("#id_pregunta").val()!= "undefined"){
			
			xml = "<getEdicionAltaPregunta><Pregunta>";		
			xml = xml +"<id>";
			xml = xml +"<codPregunta>"+$("#id_pregunta").val()+"</codPregunta>";
			xml = xml +"<codIdioma>1</codIdioma>";
			xml = xml +"<codIdiomaOld>1</codIdiomaOld>";
			xml = xml +"</id>";
			xml = xml +"<des>"+$("#texto").val()+"</des>";
			xml = xml +"<tipoRespuesta>";
			xml = xml +"<cod>"+$("#cod_tiporespuesta").val()+"</cod>";
			xml = xml +"<des>"+$("#tiporespuesta option:selected").text()+"</des>";
			xml = xml +"<valorRespuestas>";
			var data = dtrespuestas.rows().data();
			data.each(function (value, index) {			
				xml = xml +"<ValorRespuesta>";
				xml = xml +"<id>";
				xml = xml +"<codTipoResp>"+$("#cod_tiporespuesta").val()+"</codTipoResp>";
				xml = xml +"<codIdioma>1</codIdioma>";
				xml = xml +"<codValorResp>"+dtrespuestas.rows(index).data()[0][0]+"</codValorResp>";
				xml = xml +"</id>";
				xml = xml +"<valorResp>"+dtrespuestas.rows(index).data()[0][1]+"</valorResp>";
				xml = xml +"</ValorRespuesta>";
			})
			xml = xml +"</valorRespuestas>";
			xml = xml +"</tipoRespuesta>";
			xml = xml +"</Pregunta></getEdicionAltaPregunta>";
		}else{		
			//CREAR PREGUNTA
			 
			xml = "<newXMLPreguntaEncuesta><Encuesta>";
			xml = xml +"<cod>"+$("#idencuesta_editar").val()+"</cod>";
			xml = xml +"<codOld>"+$("#idencuesta_editar").val()+"</codOld>";
			xml = xml +"<des>"+$("#nombre").val()+"</des>";
			xml = xml +"<fecIni>"+$("#lanzamiento").val()+"</fecIni>";
			xml = xml +"<fecFin>"+$("#vencimiento").val()+"</fecFin>";
			xml = xml +"<dadoDeBaja>0</dadoDeBaja>";
			xml = xml +"<preguntaEncuestas>";
			xml = xml +"<PreguntaEncuesta>";
			xml = xml +"<id>";
			xml = xml +"<codEncuesta>"+$("#idencuesta_editar").val()+"</codEncuesta>";
			xml = xml +"<codIdioma>1</codIdioma>";
			xml = xml +"<codPregunta>"+$("#idpregunta_nueva").val()+"</codPregunta>";
			xml = xml +"</id>";
			xml = xml +"<orden>-1</orden>";
			xml = xml +"<pregunta>";
			xml = xml +"<id>";
			xml = xml +"<codPregunta>"+$("#idpregunta_nueva").val()+"</codPregunta>";
			xml = xml +"<codIdioma>1</codIdioma>";
			xml = xml +"<codIdiomaOld>1</codIdiomaOld>";
			xml = xml +"</id>";
			xml = xml +"<des>"+$("#texto").val()+"</des>";
			
			xml = xml +"<tipoRespuesta>";
			xml = xml +"<cod>"+$("#tiporespuesta option:selected").val()+"</cod>";
			xml = xml +"<des/>";
			xml = xml +"</tipoRespuesta>";
			xml = xml +"</pregunta>";
			xml = xml +"</PreguntaEncuesta>";
			xml = xml +"</preguntaEncuestas>";
			xml = xml +"</Encuesta></newXMLPreguntaEncuesta>";
		}
		
		$("#xml_datos_pregunta").val(xml);
		var data = $("#form_pregunta_data").serializeObject();	
			
		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/save_pregunta.do'/>",
			timeout : 100000,			
			data: data, 
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");				
				$("#modal-dialog-form-2").modal('hide');				
				
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/show_encuesta.do'/>?id="+$("#idencuesta_editar").val(), function() {
					$("#modal-dialog-form").modal('show');
					setModalDialogSize("#modal-dialog-form", "sm");
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
	}
	
	
//**************************************ELIMINAR RESPUESTA***************************************
	
	$("#tab_respuestas_remove").on("click", function(e) { 
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta_eliminar_respuesta.confirm.eliminar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
	  		  buttons: { closer: false, sticker: false	},
	  		  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
				var data = dtrespuestas.rows( { selected: true } ).data();
								
				var xmlList="";				
				xmlList = "<list>";
				for(i=0;i<data.length;i++)
				{				
					xmlList = xmlList + "<ValorRespuesta>";
					xmlList = xmlList + "<id>";
					xmlList = xmlList + "<codTipoResp>"+$("#cod_tiporespuesta").val() +"</codTipoResp>";
					xmlList = xmlList + "<codIdioma>1</codIdioma>";
					xmlList = xmlList + "<codValorResp>"+dtrespuestas.rows( { selected: true } ).data()[i][0]+"</codValorResp>";
					xmlList = xmlList + "</id>";
					xmlList = xmlList + "</ValorRespuesta>";
				}				
				xmlList = xmlList+ "</list>";
				$("#xmllist_respuestas").val(xmlList);
				var data = $("#form_pregunta_data").serializeObject();				
				
				$.ajax({					
					contenttype: "application/json; charset=utf-8",
					type : "post",
					url : "<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/remove_respuesta.do'/>",
					timeout : 100000,
					data: data, 
					success : function(data) {
						dtrespuestas.rows( '.selected' ).remove().draw();						
					},
					error : function(exception) {
						new PNotify({
						      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						      text: exception.responseText,
							  type: "error",		     
							  delay: 5000,
							  buttons: { sticker: false }
						   });			
					}
				});

		   }).on('pnotify.cancel', function() {
		   });		 	
	
	})
	
	
	//*******************GUARDAR INTERNACIONALIZACION ENCUESTA*************************	
	document.addEventListener("event-save-i18n-texto", function(e) {
	     
		     
		 $.ajax({					
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/save_internacionalizacion_pregunta.do'/>",
			timeout : 100000,
			data: {
				  data: e.detail,
				  codPregunta: $("#id_pregunta").val(),
				  idTipoPregunta: $("#tiporespuesta").val(),
				  TipoPreguntaTexto: $("#tiporespuesta option:selected").text()
				  
				}, 
			success : function(data) {
				//cargarDatosInternacionalizacion(false);		
			},
			error : function(exception) {
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",		     
					  delay: 5000,
					  buttons: { sticker: false }
				   });			
			}
		});
			
	})	
	//**********************************************
	
	
</script>

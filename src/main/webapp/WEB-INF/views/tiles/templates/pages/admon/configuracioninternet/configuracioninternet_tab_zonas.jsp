<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${tabzonas_data}" var="tabzonas_data_xml" />

<div class="modal-body">

	<form id="form_zonas_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<select name="selector_zonas[]" id="selector_zonas"  class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
			</select>
	</form>
	<div class="modal-footer">
			<button type="button" id="save_zonas_button" class="btn btn-success" >
				<spring:message code="common.button.submit_data" />
			</button>			
			
		</div>
</div>	
<!--*************************************************************************************************************** -->
<script>
cargar_zonas();

function cargar_zonas()
{

	var count=0;
	var data = [ <c:out value="${json_editarzonasinternet_selector_zonas}"  escapeXml="false" /> ];
				
				
	$("#selector_zonas").select2ToTree({treeData: {dataArr: data}, containerCssClass : "single-line"});
	$("#selector_zonas .non-leaf").attr("disabled","disabled");
				
	var selectedValuesZonas = new Array();
	<x:forEach select="$tabzonas_data_xml/ArrayList/Zona" var="item">				
	selectedValuesZonas.push('<x:out select="$item/idzona" />');
	</x:forEach>
				
	$('#selector_zonas').val(selectedValuesZonas);
	$('#selector_zonas').trigger('change.select2');
};


$("#save_zonas_button").on("click", function(e) {
	
// Obtener los datos antes de hace showSpinner() porque sino los campos est�n disabled y no recupera los datos
var data = $("#form_zonas_data").serializeObject();	

//Muestra Cargando...
showSpinner("#x_panel");

$
		.ajax({
			type : "POST",
			url : "<c:url value='/admon/configuracioninternet/save_zonas_configuracion_internet.do'/>",
			data : data,
			timeout : 100000,
			success : function(data) {
				hideSpinner("#x_panel");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});	
				
				
			},
			error : function(data, textStatus, errorThrown) {
				hideSpinner("#x_panel");
				alert("ERROR");
			}
		});



});


</script>
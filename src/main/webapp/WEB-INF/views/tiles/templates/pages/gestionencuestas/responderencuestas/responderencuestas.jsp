<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2>
			<spring:message code="encuestas.mantenimientoencuestas.dialog.responder.title" />
		</h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">

		<form id="form_filter_list_idiomas_encuesta"class="form-horizontal form-label-left">	
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-2 col-sm-2 col-xs-12"><spring:message code="encuestas.mantenimientoencuestas.dialog.responder.field.idioma" /></label>			
				</div>			
			</div>			
				
		</form>
		
		
		
		
	</div>
</div>  

<div class="col-md-12 col-sm-12 col-xs-12">  
   
	<table id="datatable-lista-idiomas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>			
			    <th></th>				
			    <th><spring:message code="encuestas.mantenimientoencuestas.dialog.responder.list.header.nombre" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	
	<div class="modal-footer">
			<button id="seleccionar_idioma" type="button" class="btn btn-primary">
				<spring:message code="common.button.continue" />
			</button>
	</div>	
</div>

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-2"/>
<tiles:insertAttribute name="modal_dialog" />

<script>

var json_idiomas=${sessionScope.i18nlangsEncuesta};

dt_idiomas=$('#datatable-lista-idiomas').DataTable( {	
    "scrollCollapse": true,
    "paging":         false, 	
     "info": false,
     "searching": false,
     select: { style: 'single'},
     language: dataTableLanguage,
     initComplete: function( settings, json ) {
    		window.setTimeout(CargarIdiomas, 100);  
        },
     "columnDefs": [
                     { "visible": false, "targets": [0]}
                   ]
     });
//*****************************************************

 function CargarIdiomas()
	{	
	
	var idioma = json_idiomas.ArrayList.LabelValue;
		 if (idioma.length>0)		 
		 {
				for (var i=0; i<idioma.length; i++) {						
					dt_idiomas.row.add([
									idioma[i].value,          
									idioma[i].label
												    
								]).draw();
				}	 
				
		 }
		 else
		 {
			 dt_idiomas.row.add([
								idioma.value,          
								idioma.label
											    
							]).draw();
		 }
			
} 
//**************************************************
 $("#seleccionar_idioma").on("click", function(e) {
	 var data= dt_idiomas.rows( { selected: true } ).data();	 
	 if (data.length<=0) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="encuestas.mantenimientoencuestas.dialog.responder.list.alert.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}	
	 	 
	 var code=data[0][0];
	 showButtonSpinner("#seleccionar_idioma");
		$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/gestionencuestas/encuestas/encuestasIdioma.do'/>?id="+code, function() {										  
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "md");
		});
	 
 })
 //********************************************************************************

</script>
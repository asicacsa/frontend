<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_rappel}" var="editar_rappel_xml" />
<x:parse xml="${editar_rappel_unidades_negocio}" var="editar_rappel_unidades_negocio_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	 <h4 class="modal-title">	
		<x:choose>
			<x:when select="not($editar_rappel_xml/Tiporappel)">
				<spring:message code="facturacion.facturas.tabs.rappels.dialog.crear_rappel.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="facturacion.facturas.tabs.rappels.dialog.editar_rappel.title" />
			</x:otherwise>
		</x:choose>		
	</h4>	
</div>

<div class="modal-body">
	<form id="form_rappel_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	<input id="idtiporappel" name="idtiporappel" type="hidden" value="<x:out select = "$editar_rappel_xml/Tiporappel/idtiporappel" />" />
	<input id="intervalos_rappel" name="intervalos_rappel" type="hidden"  />
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.rappels.field.nombre" />*</label>
			<div class="col-md-9 col-sm-9 col-xs-9">
				<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editar_rappel_xml/Tiporappel/nombre" />">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.rappels.field.descripcion" /></label>
			<div class="col-md-9 col-sm-9 col-xs-9">					
				<textarea name="descripcion" id="descripcion" class="form-control"  ><x:out select = "$editar_rappel_xml/Tiporappel/descripcion" /></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.rappels.field.criterio" />*</label>
			<div class="col-md-9 col-sm-9 col-xs-9">
				<select name="criterio" id="criterio" class="form-control" required="required">
					<option value=""></option>
					<option value="0"><spring:message code="facturacion.facturas.tabs.rappels.field.option.importe" /></option>
					<option value="1"><spring:message code="facturacion.facturas.tabs.rappels.field.option.volumen" /></option>
				</select>				
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturass.tabs.rappels.field.unidaddenegocio" />*</label>
			<div class="col-md-9 col-sm-9 col-xs-9">
				<select name="idunidadnegocio" id="selector_unidadnegocio" class="form-control" required="required">
					<option value=""></option>
					<x:forEach select="$editar_rappel_unidades_negocio_xml/ArrayList/LabelValue" var="item">
					<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
					</x:forEach>
				</select>					
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.rappels.field.acumulativo" /></label>
			<div class="col-md-9 col-sm-9 col-xs-9">	
				<input name="acumulativo" type="checkbox" <x:if select="$editar_rappel_xml/Tiporappel/acumulativo='1'">checked=""</x:if> class="js-switch" data-switchery="true" style="display: none;">
			</div>
		</div>		
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.rappels.field.intervalos" /></label>
			<div class="col-md-9 col-sm-9 col-xs-9">			
				<div class="btn-group pull-right btn-datatable">
					<a type="button" class="btn btn-info" id="tab_intervalos_new" >
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.rappels.intervalos.list.button.nuevo" />"> <span class="fa fa-plus"></span>
						</span>
					</a>					
					<a type="button" class="btn btn-info" id="tab_intervalos_remove">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.rappels.intervalos.list.button.eliminar" />"> <span class="fa fa-trash"></span>
						</span>
					</a>		
				</div>			
			
			<table id="intervalos_table" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>					  
					  <th id="idintervalo"></th>
					  <th id="orden"></th>
					  <th id="intervaloorden"><spring:message code="facturacion.facturas.tabs.rappels.field.orden"/></th>
					  <th id="liminferior"><spring:message code="facturacion.facturas.tabs.rappels.field.lim_inferior" /></th>
					  <th id="limsuperior"><spring:message code="facturacion.facturas.tabs.rappels.field.lim_superior"/></th>
					  <th id="rappel"></th>					  
					   <th id="rappelporcentaje"><spring:message code="facturacion.facturas.tabs.rappels.field.rappel"/></th>
					</tr>
				</thead>
				<tbody>	
					<x:forEach select="$editar_rappel_xml/Tiporappel/intervalos/Intervalo" var="item">
					   <tr class="seleccionable">
							<td id="idintervalo" ><x:out select="$item/idintervalo" /></td>							
							<td id="orden"><x:out select="$item/numordinalintervalo" /></td>
							<td id="intervaloorden"><spring:message code="facturacion.facturas.tabs.rappels.field.intervalo"/> <x:out select="$item/numordinalintervalo" /></td>
							<td id="liminferior" ><x:out select="$item/limiteinferior" /></td>
							<td id="limsuperior" ><x:out select="$item/limitesuperior" /></td>
							<td id="rappel" ><x:out select="$item/rappel" /></td>
							<td id="rappelporcentaje" ><x:out select="$item/rappel" /> %</td>
						</tr>																
					</x:forEach>
				</tbody>
				</table>	
				</div>
		</div>			
	</div>
	
	</form>
	<div class="modal-footer">

		<button id="save_rappel_button" type="button" class="btn btn-primary save_rappel">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>	


<script>
hideSpinner("#tab_rappels_new");
hideSpinner("#tab_rappels_edit");

$(".js-switch").each(function(index) {
	var s = new Switchery(this, {
		size : 'small'
	});
});

$('#criterio option[value="<x:out select = "$editar_rappel_xml/Tiporappel/criteriorappel" />"]').attr("selected", "selected");
$('#selector_unidadnegocio option[value="<x:out select = "$editar_rappel_xml/Tiporappel/unidadnegocio/idunidadnegocio" />"]').attr("selected", "selected");

var dtintervalos;
//**********************************************************************************************
$(document).ready(function() {
	dtintervalos=$('#intervalos_table').DataTable( {
		"paging": false,
		"info": false,
		"searching": false,
		select: { style: 'os'},
		language: dataTableLanguage,
		 "columnDefs": [
		                { "visible": false, "targets": [0,1,5]}
		              ]
		});
	
	//**********************************************************************************************
	$("#tab_intervalos_new").on("click", function(e) { 	
		
		var url = "<c:url value='/ajax/facturacion/facturas/rappels/new_intervalo_rappel.do'/>";
		
		$("#modal-dialog-form-2 .modal-content").load(url, function() {										  
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "xs");				
		});
	})
	
//**********************************************************************************************

$("#form_rappel_data").validate({
			onfocusout : false,
			onkeyup : false,
			unhighlight: function(element, errClass) {
	            $(element).popover('hide');
			},		
			errorPlacement : function(err, element) {
				err.hide();
				$(element).attr('data-content', err.text());
				$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
				$(element).popover('show');									
			},
			submitHandler : function(form) {
				guardarDatosRappel();
			}
		}
		);		
		
		
//****************************************************************		
		$(".save_rappel").on("click", function(e) {
			$("#form_rappel_data").submit();	
		})
//*****************************************************************
	function guardarDatosRappel()
	{
		//**Tabla intervalos//		
	       var intervalos_rappel= dtintervalos.rows().data();
	       var intervalos_xml= "<intervalos>";
	       
	       intervalos_rappel.each(function (value, index) {
	    	   intervalos_xml+= "<Intervalo>";
	    	   intervalos_xml+= "<idintervalo>"+dtintervalos.rows(index).data()[0][0]+"</idintervalo>";
	    	   intervalos_xml+= "<numordinalintervalo>"+dtintervalos.rows(index).data()[0][1]+"</numordinalintervalo>";
	    	   intervalos_xml+= "<limiteinferior>"+dtintervalos.rows(index).data()[0][3]+"</limiteinferior>"
	    	   intervalos_xml+= "<limitesuperior>"+dtintervalos.rows(index).data()[0][4]+"</limitesuperior>"
	    	   intervalos_xml+= "<rappel>"+dtintervalos.rows(index).data()[0][5]+"</rappel>"
	    	   intervalos_xml+= "</Intervalo>";
	       });
	       intervalos_xml+= "</intervalos>";
	       $("#intervalos_rappel").val(intervalos_xml);
		
		
		var data = $("#form_rappel_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/facturacion/facturas/rappels/save_rappel.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				dt_listrappels.ajax.reload(null,false);			
				$("#modal-dialog-form").modal('hide');				
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
	}
//*****************************************************************	
$("#tab_intervalos_remove").on("click", function(e) { 
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="facturacion.facturas.tabs.rappels.intervalos.eliminar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
			  buttons: { closer: false, sticker: false	},
			  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
			   dtintervalos
			    .rows( '.selected' )
			    .remove()
			    .draw();
		   })
		
		})
});


</script>
	
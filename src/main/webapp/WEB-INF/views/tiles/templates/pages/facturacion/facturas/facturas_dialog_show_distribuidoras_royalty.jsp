<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>



 <div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="facturacion.facturas.tabs.royalties.dialog.listar_distribuidoras.title" />
	</h4>	
</div>
<div class="modal-body">
<form id="form_cliente_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_distribuidoras_royalty_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.royalties.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>						
		<a type="button" class="btn btn-info" id="tab_distribuidoras_royalty_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.royalties.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>		
	</div>
	<table id="datatable-list-distribuidoras-royalty" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="facturacion.facturas.tabs.royalties.distribuidoras.list.header.nombre" /></th>
				<th><spring:message code="facturacion.facturas.tabs.royalties.distribuidoras.list.header.descripcion" /></th>				
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div> 
</form>
</div>
<script>
hideSpinner("#tab_royalty_distribuidora");

//*********************************************************************
var dt_listdistribuidorasroyalty=$('#datatable-list-distribuidoras-royalty').DataTable( {
	
    ajax: {
    	
        url: "<c:url value='/ajax/facturacion/facturas/royalties/list_distribuidoras_royalty.do'/>",
        rowId: 'iddistribuidora',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Distribuidora)); return(""); },
        
       
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {dt_listdistribuidorasroyalty.columns.adjust().draw(); });
	},
    columns: [			
		{ data: "nombre", type: "spanish-string", defaultContent: ""}, 
		{ data: "descripcion", type: "spanish-string", defaultContent: ""},			     
    ],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-distribuidoras-royalty') },
    select: { style: 'os'},
	language: dataTableLanguage,
	processing: true,
} );
//*********************************************BOT�N NUEVO*************************************
$("#tab_distribuidoras_royalty_new").on("click", function(e) {
	 showButtonSpinner("#tab_distribuidoras_royalty_new");
	 $("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/facturacion/facturas/royalties/new_distribuidora_royalty.do'/>", function() {										  
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "xs");
		
	});
})
//***********************************************BOT�N ELIMIMAR***********************************
$("#tab_distribuidoras_royalty_remove").on("click", function(e) { 
		
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="facturacion.facturas.tabs.royalties.distribuidoras.list.confirm.eliminar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
	  		  buttons: { closer: false, sticker: false	},
	  		  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
		
			   dt_listdistribuidorasroyalty.processing(true);
				
				var data = sanitizeArray(dt_listdistribuidorasroyalty.rows( { selected: true } ).data(),"iddistribuidora");
			   
				$.ajax({
					contenttype: "application/json; charset=utf-8",
					type : "post",
					url : "<c:url value='/ajax/facturacion/facturas/rappels/remove_distribuidoras_royalty.do'/>",
					timeout : 100000,
					data: { data: data.toString() }, 
					success : function(data) {
						dt_listdistribuidorasroyalty.ajax.reload();					
					},
					error : function(exception) {
						dt_listdistribuidorasroyalty.processing(false);
						
						new PNotify({
						      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						      text: exception.responseText,
							  type: "error",		     
							  delay: 5000,
							  buttons: { sticker: false }
						   });			
					}
				});

		   }).on('pnotify.cancel', function() {
		   });		 

	});
//*********************************************************************


</script>
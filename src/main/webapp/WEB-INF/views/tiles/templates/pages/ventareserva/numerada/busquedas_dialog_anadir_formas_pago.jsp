<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${selector_formas_pago}" var="selectorFormasdePagoPorCanal_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="venta.numerada.tabs.venta_edit.editar.formas.title" />				
	</h4>	
</div>

<div class="modal-body">
	<form id="form_formas_pago_anadir" data-parsley-validate="" class="form-horizontal form-label-left" >
	
	<div class="form-group">			
		<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message	code="venta.ventareserva.tabs.busquedas.venta_reserva.formas_pago_reserva.field.importe" /></label>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<input required="required" name="importeAnadir" id="importeAnadir" type="text" class="form-control" value="">
		</div>
	</div>
	
	
	<div class="form-group">	
			<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message	code="venta.ventareserva.tabs.busquedas.venta_reserva.formas_pago_reserva.field.formapago" /></label>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<select required="required" class="form-control" name="id_formapago" id="id_formapago">
						<option value=""></option>
							<x:forEach select="$selectorFormasdePagoPorCanal_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out
											select="$item/label" /></option>
								</x:forEach>
					</select>								
				</div>		
		</div>	
	
	
	
		<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="modal-footer">
			<button id="anadir_forma_button"  class="btn btn-primary">
						<spring:message code="common.button.accept" />
			</button>
			<button type="button" class="btn btn-cancel close_dialog3" data-dismiss="modal">
				<spring:message code="common.button.cancel"/>
			</button>
		</div>
		</div>		
	</form>
</div>

<script>
$("#anadir_forma_button").on("click", function(e) { 
	$("#form_formas_pago_anadir").validate();			
})

	$("#form_formas_pago_anadir").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			anadirPago()
			return;
		}
	}
	);
	
	function anadirPago()
	{
		dt_listformaspago.row.add([
			   						"",          
			   						""+$("#id_formapago").val(),		
			   						"",
			   						""+$("#importeAnadir").val(), 
			   						""+$("#id_formapago option:selected").text(),        
			   						""									    
			   					]).draw();		
		
		$("#modal-dialog-form-3").modal('hide');
	}
</script>

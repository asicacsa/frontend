<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_listado_tiposbono}" var="ventareserva_listado_tiposbono_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	
</div>

<div class="modal-body">

	<form id="form_edit_emision_bono" class="form-horizontal form-label-left">

		<div class="col-md-12 col-sm-12 col-xs-12">
		
			<div class="form-group date-picker">
				<label class="control-label col-md-1 col-sm-1 col-xs-12"><spring:message code="administracion.programacion.programacion.field.fecha" /></label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<a type="button" class="btn btn-default btn-clear-date" id="button_fecha_clear">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.clear" />"> <span class="fa fa-trash"></span>
						</span>
					</a>			
                       <div class="input-prepend input-group">
                         <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                         <input type="text" name="fecha" id="fecha" class="form-control" required="required" value="" readonly/>
                         <input type="hidden" name="fechaIni" value=""/>
                         <input type="hidden" name="fechaFin" value=""/>
                       </div>
				</div>
					
		
			
				<label class="control-label col-md-1 col-sm-1 col-xs-12"><spring:message code="venta.ventareserva.dialog.edit_emision_bono.field.numero" /></label>
				<div class="col-md-2 col-sm-2 col-xs-12">
					<button type="button"class="input-group-button btn-number" id="minus" data-type="minus" data-field="quant[1]"> <i class="fa fa-minus" aria-hidden="true"></i> </button>
					<input  min="1"  id="cantidad" max="10000" type="text" class="form-control input-number" required="required" name="quant[1]" value="${numero}">
					<button type="button"class="input-group-button btn-number" id="plus" data-type="plus" data-field="quant[1]"> <i class="fa fa-plus" aria-hidden="true"></i> </button>
				</div>
			

			
				<label class="control-label col-md-1 col-sm-1 col-xs-12"><spring:message code="venta.ventareserva.dialog.edit_sesion.field.tarifa" /></label>
				<div class="col-md-2 col-sm-2 col-xs-12">
					<select class="form-control" name="idtarifa" id="idtarifa" required="required">
						<option value=""></option>
					</select>
				</div>
			
			
				<label class="control-label col-md-1 col-sm-1 col-xs-12"><spring:message code="venta.ventareserva.dialog.edit_emision_bono.field.tipobono" /></label>
				<div class="col-md-1 col-sm-1 col-xs-12">
					<select class="form-control" name="tipobono" id="tipobono">
						<option value=""></option>
						<x:forEach select="$ventareserva_listado_tiposbono_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			<!--  <div class="col-md-2 rigth_col">
					<a type="button" class="btn btn-info" id="save_edit_emision_bono">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.venta_individual.list.button.vender" />"> <span class="fa fa-floppy-o"></span>
						</span>
					</a>	
					<a type="button" class="btn btn-info" data-dismiss="modal" id="cerrar_ediccion" data-dismiss="modal">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="common.button.cancel" />"> <span class="fa fa-remove"></span>
						</span>
					</a>
				</div>-->
			
		</div>
		
		 <div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_edit_emision_bono" type="button" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div> 
	</form>
</div>


<script>
//Ahora venta




hideSpinner("#button_emision_bono_editar");

var sesion_data=$.extend(true,[],dt_listemisionbono.rows( { selected: true } ).data()[0][0]);

var dia_inicio= moment().format("DD/MM/YYYY"); 
var dia_fin = moment().add("days",1).format("DD/MM/YYYY");

if (sesion_data.Lineadetalle.fechaIniVigencia!=""){
	$('input[name="fechaIni"]').val(sesion_data.Lineadetalle.fechaIniVigencia.split("-")[0]);
	$('input[name="fechaFin"]').val(sesion_data.Lineadetalle.fechaFinVigencia.split("-")[0]);	
}
else {
    $('input[name="fechaIni"]').val(dia_inicio);   
    $('input[name="fechaFin"]').val(dia_fin);
  	sesion_data.Lineadetalle.fechaIniVigencia= dia_inicio;
	sesion_data.Lineadetalle.fechaFinVigencia= dia_fin;
}

$('input[name="fecha"]').val( $('input[name="fechaIni"]').val() + ' - ' + $('input[name="fechaFin"]').val());



var item=sesion_data.Lineadetalle.perfiles.Perfilvisitante;
if (typeof item!="undefined") {
	var $select= $("#idtarifa");
	$select.html('');
	$select.prepend("<option value='' selected='selected'></option>");
	if (item.length>0)
	    $.each(item, function(key, val){
	      $select.append('<option value="' + val.idperfilvisitante + '" idtarifa="' + val.tarifa.idtarifa + '" importe="' + val.tarifa.Tarifaproducto.importe + '">' + val.nombre + '</option>');
	    })
	else
		$select.append('<option value="' + item.idperfilvisitante + '" idtarifa="' + item.tarifa.idtarifa + '" importe="' + item.tarifa.Tarifaproducto.importe + '">' + item.nombre + '</option>');
}

$("#cantidad").val(sesion_data.Lineadetalle.cantidad);
$('#idtarifa option[value="'+ sesion_data.Lineadetalle.perfilvisitante.idperfilvisitante +'"]').attr("selected", "selected");
$('#tipobono option[value="'+ sesion_data.Lineadetalle.idTipoBono +'"]').attr("selected", "selected");







$('input[name="fecha"]').daterangepicker({
		autoUpdateInput: false,
		linkedCalendars: false,
		autoApply: true,
      	locale: $daterangepicker_sp
		}, function(start,end) {
     	  	 sesion_data.Lineadetalle.fechaIniVigencia= start.format('DD/MM/YYYY');
      	  	 sesion_data.Lineadetalle.fechaFinVigencia= end.format('DD/MM/YYYY');
      	  	 $('input[name="fecha"]').val(sesion_data.Lineadetalle.fechaIniVigencia + ' - ' + sesion_data.Lineadetalle.fechaFinVigencia);
      	  	 $('input[name="fechaIni"]').val(sesion_data.Lineadetalle.fechaIniVigencia);
      	  	 $('input[name="fechaFin"]').val(sesion_data.Lineadetalle.fechaFinVigencia);
    	 });

$("#button_fecha_clear").on("click", function(e) {
    $('input[name="fecha"]').val('');
    $('input[name="fechaIni"]').val('');
    $('input[name="fechaFin"]').val('');
});    	 


function calcularImporteTotal() {
	var total= Number($("#idtarifa :selected").attr("importe"))*parseInt($("#cantidad").val());
	return total.toFixed(2);	
}

$("#cantidad").on("change", function(e) {
	sesion_data.Lineadetalle.cantidad= $("#cantidad").val();
	sesion_data.Lineadetalle.importe= calcularImporteTotal();
});

function obtenerPerfilVisitante(tarifas,idperfilvisitante) {
	if (typeof tarifas.length!="undefined") {
		for (var i=0; i<tarifas.length; i++) 
			if (tarifas[i].idperfilvisitante==idperfilvisitante) return(tarifas[i]);
	}
	else if (tarifas.idperfilvisitante==idperfilvisitante) return(tarifas)
	return("");
}

$("#idtarifa").on("change", function(e) {
	var perfilVisitante= obtenerPerfilVisitante(sesion_data.Lineadetalle.perfiles.Perfilvisitante,$("#idtarifa").val());
	sesion_data.Lineadetalle.perfilvisitante.nombre= perfilVisitante.nombre;
	sesion_data.Lineadetalle.perfilvisitante.idperfilvisitante= perfilVisitante.idperfilvisitante;
	sesion_data.Lineadetalle.perfilvisitante.tarifa.nombre= perfilVisitante.tarifa.nombre;
	sesion_data.Lineadetalle.importe= calcularImporteTotal();
	showFieldSpinner("#descuentos_div");
	$select=$("#iddescuento");
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/list_descuentos.do'/>",
		timeout : 100000,
		data: {
				idproducto: "${idProducto}",
				idcanal: "${sessionScope.idcanal}",
				idtarifa: $("#idtarifa :selected").attr("idtarifa")
			  }, 
		success : function(data) {
			hideSpinner("#descuentos_div");
			$select.html('');
			$select.prepend("<option value='' selected='selected'></option>");
			if (data.ArrayList!="") {
				var item=data.ArrayList.LabelValue;
				if (item.length>0)
				    $.each(item, function(key, val){
				      $select.append('<option value="' + val.value + '">' + val.label + '</option>');
				    });
				else
			      	$select.append('<option value="' + item.value + '">' + item.label + '</option>');
			}
		},
		error : function(exception) {
			hideSpinner("#descuentos_div");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});	
}); 

$("#tipobono").on("change", function(e) {
	var perfilVisitante= obtenerPerfilVisitante(sesion_data.Lineadetalle.perfiles.Perfilvisitante,$("#idtarifa").val());
	sesion_data.Lineadetalle.perfilvisitante.nombre= perfilVisitante.nombre;
	sesion_data.Lineadetalle.perfilvisitante.idperfilvisitante= perfilVisitante.idperfilvisitante;
	sesion_data.Lineadetalle.perfilvisitante.tarifa.nombre= perfilVisitante.tarifa.nombre;
	sesion_data.Lineadetalle.importe= calcularImporteTotal();
	sesion_data.Lineadetalle.idTipoBono= $("#tipobono :selected").val();
});

$("#save_edit_emision_bono").on("click", function(e) {
	$("#form_edit_emision_bono").submit();
});

$("#form_edit_emision_bono").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		saveEmisionBonoData();		
	}
});

function saveEmisionBonoData() {
	dt_listemisionbono.rows( { selected: true } ).data()[0][0]= sesion_data;
	dt_listemisionbono.rows( { selected: true } ).data()[0][5]= $('input[name="fechaIni"]').val();
	dt_listemisionbono.rows( { selected: true } ).data()[0][6]= $('input[name="fechaFin"]').val();
	dt_listemisionbono.rows( { selected: true } ).data()[0][7]= $("#cantidad").val();
	dt_listemisionbono.rows( { selected: true } ).data()[0][8]= $("#idtarifa :selected").text();
	dt_listemisionbono.rows( { selected: true } ).data()[0][9]= $("#tipobono :selected").text();
	dt_listemisionbono.rows( { selected: true } ).data()[0][10]= typeof sesion_data.Lineadetalle.importe!="undefined"?sesion_data.Lineadetalle.importe:"--";
	dt_listemisionbono.rows( { selected: true } ).data()[0][11]= $("#tipobono :selected").val();
	dt_listemisionbono.rows().invalidate().draw();
	actualizar_totales_emision_bono();
	$("#modal-dialog-form").modal('hide');
	dt_listemisionbono.rows().deselect();
}
//****************************
$("#cerrar_ediccion").on("click", function(e) {
	dt_listemisionbono.rows().deselect();
});
//**********************************
$("#cerrar_con_x").on("click", function(e) {
	dt_listemisionbono.rows().deselect();
});


//***************************************
//Botón + - 
$('.btn-number').click(function(e){
  e.preventDefault();
  
  fieldName = $(this).attr('data-field');
  type      = $(this).attr('data-type');
  var input = $("input[name='"+fieldName+"']");
  var currentVal = parseInt(input.val());
  if (!isNaN(currentVal)) {
      if(type == 'minus') {        	
          if(currentVal > input.attr('min')) {
              input.val(currentVal - 1).change();
            //  $("#plus").attr('disabled', false);
          } 
          if(parseInt(input.val()) == input.attr('min')) {
              $(this).attr('disabled', true);
          }

      } else if(type == 'plus') {
	
          if(currentVal < input.attr('max')) {
              input.val(currentVal + 1).change();
              //$("#minus").attr('disabled', false);                
          }
          if(parseInt(input.val()) == input.attr('max')) {
              $(this).attr('disabled', true);
          }

      }
  } else {
      input.val(0);
  }
});

$('.input-number').focusin(function(){
 $(this).data('oldValue', $(this).val());
});

$('.input-number').change(function() {
  
  minValue =  parseInt($(this).attr('min'));
  maxValue =  parseInt($(this).attr('max'));
  valueCurrent = parseInt($(this).val());
  
  name = $(this).attr('name');
  if(valueCurrent >= minValue) {
      $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
  } else {
      alert('El valor mínimo es 1');
      $(this).val($(this).data('oldValue'));
  }
  if(valueCurrent <= maxValue) {
      $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
  } else {
      alert('El valor máximo es 10000');
      $(this).val($(this).data('oldValue'));
  }
  
  
});
$(".input-number").keydown(function (e) {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
           // Allow: Ctrl+A
          (e.keyCode == 65 && e.ctrlKey === true) || 
           // Allow: home, end, left, right
          (e.keyCode >= 35 && e.keyCode <= 39)) {
               // let it happen, don't do anything
               return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
  });

</script>
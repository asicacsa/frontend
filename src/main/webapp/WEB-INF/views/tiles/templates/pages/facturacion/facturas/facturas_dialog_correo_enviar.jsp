<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${modelos_correo}" var="modelos_correo_xml" />



<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">
	     
			<spring:message code="facturacion.facturas.tabs.clientes.dialog.correo.title" />
		
	</h4>	
</div>

<div class="modal-body">
		<form id="form-enviar" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
			<div class="col-md-12 col-sm-12 col-xs-12">
				  
			 
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.correo.field.modelo" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="modelo_correo" id="modelo_correo" class="form-control">
						<x:forEach select="$modelos_correo_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>					
				</div>				
			</div>
			
			
			<br/><br/>
			
			</div>
			
			
</form>
				


	<div class="modal-footer">
	
		<button id="enviar_correo_button" type="button" class="btn btn-primary close_bloqueo_cliente">
			<spring:message code="common.button.accept" />
		</button>	
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>		

</div>

<script>


$( "#enviar_correo_button" ).on("click", function(e) {	
	var modelo_correo =  $("#modelo_correo option:selected").val();
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/facturacion/facturas/cliente/enviar_correo.do'/>?bloqueado=${bloqueado}&ids=${ids}&id_modelo="+modelo_correo,
		timeout : 100000,
		data: {  }, 
		success : function(data) {
			
			dt_listclientes.ajax.reload();	
			$("#modal-dialog-form").modal('hide');
			enviadosOk = data.ResultadoEnviarCorreos.enviadosOk;
			enviadosWrong = data.ResultadoEnviarCorreos.enviadosWrong;
			idsfallidos = data.ResultadoEnviarCorreos.idError;
			mensaje="<spring:message code="facturacion.facturas.tabs.clientes.dialog.correo.mensaje.trozo1" />"+enviadosOk+" <spring:message code="facturacion.facturas.tabs.clientes.dialog.correo.mensaje.trozo2" /> "+enviadosWrong+"."+idsfallidos;
			
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : mensaje,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		},
		error : function(exception) {
			dt_listclientes.processing(false);
			
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
})


</script>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${tababonos_selector_temporada}" var="tababonos_selector_temporada_xml" />
<x:parse xml="${tablocalidadesabonos_selector_areas}" var="tababonos_selector_areas_xml" />

<div class="x_panel filter_list thin_padding"> 
	<div class="x_title"> 
		<h2>
			<spring:message code="common.text.filter_list" />
		</h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">

		<form id="form_filter_list_abonos" class="form-horizontal form-label-left">

			<div class="col-md-6 col-sm-6 col-xs-12">

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.abonos.field.nombre_abonado" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<input name="nombreabonado" type="text" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.abonos.field.numero_abonado_abono" /></label>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<input name="numabonado" type="text" class="form-control">
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<input name="numabono" type="text" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.abonos.field.dni_cif" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<input name="dni" type="text" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.abonos.field.cliente" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<input name="cliente_abonos" id="cliente_abonos" type="text" class="form-control" readonly>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label"> <input type="radio" class="flat" checked name="bloqueado" value=""> <spring:message code="venta.numerada_busquedas.tabs.abonos.field.radio.todos" />
					</label> <label class="control-label"> <input type="radio" class="flat" name="bloqueado" value="1"> <spring:message code="venta.numerada_busquedas.tabs.abonos.field.radio.bloqueados" />
					</label> <label class="control-label"> <input type="radio" class="flat" name="bloqueado" value="0"> <spring:message code="venta.numerada_busquedas.tabs.abonos.field.radio.no_bloqueados" />
					</label>
				</div>

			</div>


			<div class="col-md-6 col-sm-6 col-xs-12">
			
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.abonos.field.ref_venta_entrada" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<input name="refventa" type="text" class="form-control">
					</div>
				</div>
			
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.abonos.field.temporada" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">

						<select class="form-control" name="idtemporada" id="idtemporada_abonos">
							<option value=""></option>
							<x:forEach select="$tababonos_selector_temporada_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.abonos.field.tipo_abono" /></label>
					<div id="tipo_abonos_div" class="col-md-8 col-sm-8 col-xs-8">

						<select class="form-control" name="idtipoabono" id="idtipoabono_abonos">
							<option value=""></option>
						</select>
					</div>
				</div>

			
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.abonos.field.zona" /></label>
					<div id="zona_div" class="col-md-8 col-sm-8 col-xs-8">

						<select class="form-control" name="nombrearea" id="nombrearea">
							<option value=""></option>
							<x:forEach select="$tababonos_selector_areas_xml/ArrayList/String" var="item">
								<option value="<x:out select="$item" />"><x:out select="$item" /></option>
							</x:forEach>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.abonos.field.fila_desde" /></label>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<input type="text" name="filadesde" class="form-control" value="" />
					</div>
					<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="venta.numerada_busquedas.tabs.abonos.field.fila_hasta" /></label>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<input type="text" name="filahasta" class="form-control" value="" />
					</div>
				</div>

			</div>

			<div class="clearfix"></div>
			<div class="ln_solid"></div>
			<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
				<button id="button_filter_list_abonos" type="button" class="btn btn-success pull-right">
					<spring:message code="common.button.filter" />
				</button>
				<button id="button_clean_venta_abono_numerada" type="button" class="btn btn-success pull-right">
					<spring:message code="venta.ventareserva.tabs.canje-bono.button.clean" />
				</button>
			</div>

			<input type="hidden" name="idcliente_abonos" value=""/>
			<input type="hidden" name="start" value=""/>
			<input type="hidden" name="length" value=""/>

		</form>


	</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_abonos_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada_busquedas.tabs.abonos.list.button.modificar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_abonos_print_ticket">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada_busquedas.tabs.abonos.list.button.imprimir_entradas" />"> <span class="fa fa-ticket"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_abonos_unlock">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada_busquedas.tabs.abonos.list.button.desbloquear" />"> <span class="fa fa-unlock"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_abonos_print">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada_busquedas.tabs.abonos.list.button.imprimir" />"> <span class="fa fa-ticket"></span>
			</span>
		</a>		
	</div>

	<table id="datatable-list-abonos" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.abonos.list.header.numero" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.abonos.list.header.abonado" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.abonos.list.header.tipo" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.abonos.list.header.fila" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.abonos.list.header.butaca" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.abonos.list.header.zona" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.abonos.list.header.acceso" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.abonos.list.header.nombre" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.abonos.list.header.importe" /></th>				
				<th><spring:message code="venta.numerada_busquedas.tabs.abonos.list.header.bloqueado" /></th>				
				<th><spring:message code="venta.numerada_busquedas.tabs.abonos.list.header.observaciones" /></th>				
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>


<!--  -->

<script>

var data_abono_imprimir;

$("#cliente_abonos").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>", "#cliente_abonos", "#idcliente_abonos");

$("#idtemporada_abonos").on("change", function(e) {
	showFieldSpinner("#tipo_abonos_div");	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/list_tipos_abono.do'/>",
		timeout : 100000,
		data: {
				idtemporada: $("#idtemporada_abonos").val()
			  }, 
		success : function(data) {
			hideSpinner("#tipo_abonos_div");
			var $select=$("#idtipoabono_abonos");
			$select.html('');
			$select.prepend("<option value='' selected='selected'></option>");
		    $.each(data.ArrayList, function(key, val){
		      $select.append('<option value="' + val.value + '">' + val.label + '</option>');
		    });
		},
		error : function(exception) {
			hideSpinner("#tipo_abonos_div");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});
	
});


//********************************************************************************

var dt_listabonos=$('#datatable-list-abonos').DataTable( {
	serverSide: true,
	searching: false,
	ordering: false,
	deferLoading: 0,
	pagingType: "simple",
	info: false,
    ajax: {
        url: "<c:url value='/ajax/venta/numerada/list_abonos.do'/>",
        rowId: 'idabono',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Abono)); return(""); },
        data: function (params) {
        	$('#form_filter_list_abonos input[name="start"]').val(params.start);
        	$('#form_filter_list_abonos input[name="length"]').val(params.length);
        	return ($("#form_filter_list_abonos").serializeObject());
       	}
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listabonos.data().count()>0)  dt_listabonos.columns.adjust().draw(); });
	},
    columns: [
        { data: null, type: "spanish-string", defaultContent: "", render: function ( data, type, row, meta ) { return data; }},
        { data: "idabono", type: "numeric"}, 
        { data: "nroabonado", type: "numeric"}, 
        { data: "tipoabono.nombre", type: "spanish-string", defaultContent: ""}, 
        { data: "localidad.fila", type: "spanish-string", defaultContent: ""},
        { data: "localidad.butaca", type: "spanish-string", defaultContent: ""},
        { data: "localidad.zona.nombre", type: "spanish-string", defaultContent: ""},
        { data: "localidad.acceso.nombre", type: "spanish-string", defaultContent: ""},
        { data: "nombreabonado", type: "spanish-string", defaultContent: ""},        
        { data: "lineadetalle.importe", type: "numeric"},   
		{ data: "bloqueado", className: "text_icon cell_centered",
    		render: function ( data, type, row, meta ) {
  	  	  	if (data==1) return '<i class="fa fa-check" aria-hidden="true"></i>'; else return '<i class="fa fa-close" aria-hidden="true"></i>';	
    	}},	  
    	{ data: "observaciones", type: "spanish-string", defaultContent: ""},
    ],    
	columnDefs: [
	             { "targets": 0, "visible": false },
	         ],
    select: { style: 'single'},
	language: dataTableLanguage,
	processing: true
} );


//********************************************************************************

$("#button_filter_list_abonos").on("click", function(e) { 
	
	if (!isFormEmpty("#form_filter_list_abonos")) {
		dt_listabonos.ajax.reload(); 
	}
	else {
		new PNotify({
			  title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
		      text: '<spring:message code="common.dialog.text.empty_filter" />',
			  type: "error",
			  buttons: { sticker: false }				  
		   });
	}
	
});

//********************************************************************************
$("#tab_abonos_print").on("click", function(e) { 
	
	data_abono_imprimir = dt_listabonos.rows( { selected: true } ).data();

	if (data_abono_imprimir.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '1<spring:message code="venta.numerada_busquedas.tabs.abonos.list.message.no_seleccionado" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	if (data_abono_imprimir.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.numerada_busquedas.tabs.abonos.list.message.mas_uno_seleccionado" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	$("#modal-dialog-form .modal-content:first").load("<c:url value='/ajax/venta/numerada/imprimir_abono.do'/>", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "xs");
	});
})

insertSmallSpinner("#datatable-list-abonos_processing");
/***********************************************BOT�N LIMPIAR  *********************************/
$("#button_clean_venta_abono_numerada").on("click", function(e) {
	
    $('input[name="nombreabonado"]').val('');
    $('input[name="numabonado"]').val('');
    $('input[name="numabono"]').val('');
    $('input[name="dni"]').val('');
    $('input[name="cliente_abonos"]').val('');
    $('input[name="refventa"]').val('');
    $('input[name="filadesde"]').val('');
    $('input[name="filahasta"]').val('');
    
	
	
	 $("#idtemporada_abonos option:first").prop("selected", "selected");
	 $("#idtipoabono_abonos option:first").prop("selected", "selected");
	 $("#nombrearea option:first").prop("selected", "selected");
	 
	
})

</script>


<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.tabs.gestion_pases_club-dialog.sacar_foto.title" />
	</h4>
</div>
<div class="modal-body">	
	<form id="form_selector_reserva_grupo" class="form-horizontal form-label-left">
		<div class="col-md-1 col-sm-1 col-xs-12">&nbsp;</div>	
			<div class="col-md-10 col-sm-10 col-xs-12">	
	<video onclick="snapshot(this);" width=240 height=320 id="video" controls autoplay></video>
	
	</div>
	<div class="col-md-1 col-sm-1 col-xs-12">&nbsp;</div>
	</form>
	<div class="modal-footer">
		<button id="take_foto" type="button" class="btn btn-primary take_foto">
		   <spring:message code="venta.ventareserva.tabs.gestion_pases_club-dialog.sacar_foto" />
		</button>	
		
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>		

</div>
<script>
inicializarCamara();

   /* navigator.getUserMedia = ( navigator.getUserMedia ||
                       navigator.webkitGetUserMedia ||
                       navigator.mozGetUserMedia ||
                       navigator.msGetUserMedia);*/

var video;
var webcamStream;


function tieneSoporteUserMedia() {
    return !!(navigator.getUserMedia || (navigator.mozGetUserMedia || navigator.mediaDevices.getUserMedia) || navigator.webkitGetUserMedia || navigator.msGetUserMedia)
}

function _getUserMedia() {
    return (navigator.getUserMedia || (navigator.mozGetUserMedia || navigator.mediaDevices.getUserMedia) || navigator.webkitGetUserMedia || navigator.msGetUserMedia).apply(navigator, arguments);
}




function startWebcam() {
	if (tieneSoporteUserMedia()) {
		 _getUserMedia({       
           video: true
        },

        function(localMediaStream) {
           video = document.querySelector('video');
           video.srcObject = localMediaStream;
           webcamStream = localMediaStream;
        },

        function(err) {
           console.log("The following error occured: " + err);
        }
     );
  } else {
     console.log("getUserMedia not supported");
  }  
}

var canvas, ctx;

function inicializarCamara() {
  
  startWebcam();
  canvas = $(document).find('#myCanvas').get(0);
  ctx = canvas.getContext('2d');
}

function snapshot() {
  ctx.drawImage(video, 0,0, canvas.width, canvas.height);
  $("#modal-dialog-form-3").modal('hide'); 
}

///**************************************************************************************
$("#take_foto").on("click", function(e) { 
	snapshot() 		
	})

</script>
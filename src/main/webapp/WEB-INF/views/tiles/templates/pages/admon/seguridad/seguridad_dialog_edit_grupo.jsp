<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editargrupo_datos_grupo}" var="editargrupo_datos_grupo_xml" />
<x:parse xml="${editargrupo_selector_unidades}" var="editargrupo_selector_unidades_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<x:choose>
			<x:when select="not($editargrupo_datos_grupo_xml/Grupo)">
				<spring:message code="administracion.seguridad.tabs.grupos.dialog.crear_grupo.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.seguridad.tabs.grupos.dialog.editar_grupo.title" />
			</x:otherwise>
		</x:choose>
	</h4>
</div>
<div class="modal-body" id="principal">

	<form id="form_grupo_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

		<input id="idgrupo" name="idgrupo" type="hidden" value="<x:out select = "$editargrupo_datos_grupo_xml/Grupo/idgrupo" />" />

		<div class="col-md-12 col-sm-12 col-xs-12">

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.grupos.field.nombre" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editargrupo_datos_grupo_xml/Grupo/nombre" />">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.grupos.field.unidades" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					  <select name="idunidad" id="selector_unidad" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$editargrupo_selector_unidades_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.grupos.field.usuarios" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="usuarios[]" id="selector_usuarios" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
					</select>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_grupo_button" type="submit" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
	</form>

</div>


<script>
	hideSpinner("#tab_grupos_new");
	hideSpinner("#tab_grupos_edit");

	$(".select-select2").select2({
		language : "es",
		containerCssClass : "single-line"
	});

	showSpinner("#principal");

	$('#selector_unidad option[value="<x:out select = "$editargrupo_datos_grupo_xml/Grupo/unidadnegocio/idunidadnegocio" />"]').attr("selected", "selected");

	cargar_usuarios();
	
	$("#form_grupo_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFormData();
		}
	});
	
	//********************************************************************************
	function cargar_usuarios()
	{
		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/seguridad/seguridad/select_list_usuario.do'/>",
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				$.each(data.ArrayList.LabelValue, function(i, item) {
				    $("#selector_usuarios").append('<option value="'+item.value+'">'+item.label+'</option>');
				});				
			    
				selectedValues = new Array();
				<x:forEach select="$editargrupo_datos_grupo_xml/Grupo/grupousuarios/Grupousuario/usuario" var="item">
					selectedValues.push('<x:out select="$item/idusuario" />');						
				</x:forEach>
				
				$('#selector_usuarios').val(selectedValues);
				$('#selector_usuarios').trigger('change.select2');
			    					
		    	hideSpinner("#principal");
			}
		});
	}

	//********************************************************************************	
	function saveFormData() {
		var data = $("#form_grupo_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/seguridad/seguridad/save_grupo.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				dt_listgrupos.ajax.reload(null,false);			
				$("#modal-dialog-form").modal('hide');
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
	}
</script>

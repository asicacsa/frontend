<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${historico_anuladas}" var="historico_anuladas_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.numerada.tabs.ventas.dialog.historico-anuladas.title" /> <x:out select="$historico_anuladas_xml/Venta/idventa"/>
	</h4>
</div>
<div class="modal-body">
	<table id="historico_anuladas_venta_table" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
			  <th id="num_entrada"><spring:message code="venta.numerada.tabs.ventas.dialog.historico-anuladas.field.num-entrada"/></th>
			  <th id="contenido"><spring:message code="venta.numerada.tabs.ventas.dialog.historico-anuladas.field.contenido"/></th>
			  <th id="sesion"><spring:message code="venta.numerada.tabs.ventas.dialog.historico-anuladas.field.sesion" /></th>
			  <th id="importe"><spring:message code="venta.numerada.tabs.ventas.dialog.historico-anuladas.field.importe" /></th>							  						 
			  <th id="num_i"><spring:message code="venta.numerada.tabs.ventas.dialog.historico-anuladas.field.num_i" /></th>
			  <th id="area"><spring:message code="venta.numerada.tabs.ventas.dialog.historico-anuladas.field.area" /></th>
			  <th id="fila"><spring:message code="venta.numerada.tabs.ventas.dialog.historico-anuladas.field.fila" /></th>
			  <th id="butaca"><spring:message code="venta.numerada.tabs.ventas.dialog.historico-anuladas.field.butaca" /></th>
			  
			</tr>
		</thead>
		<tbody>				
			
		</tbody>				
	</table>			

	<div class="modal-footer">
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.close"/>
		</button>
	</div>		

</div>

<script>
hideSpinner("#tabs_venta_edit_ver_anuladas");
//**********************************************************************
 var dt_listanuladas=$('#historico_anuladas_venta_table').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	  initComplete: function( settings, json ) {
	    	window.setTimeout(CargarLineasDetalleAnuladas, 100);  
	    },
	scrollCollapse: true,
	ordering:  false,
	paging: false,	
    select: { style: 'os' },
    columns: [
                            
              {},
              {},
              {},
              {},
              {},
              {},
              {},
              {}
              
              
        ],
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('historico_anuladas_venta_table');
   	}
    
} ); 
//****************************************************************
 function CargarLineasDetalleAnuladas()
	{
		var json = ${historico_anuladas_json};
		var i = 0;
		
		
		var lineasdetalle = json.Venta.lineadetalles;
		
		
		if (lineasdetalle !=""){
		if (lineasdetalle.Lineadetalle!="") {
	        var item=lineasdetalle.Lineadetalle;
	        if (item.length>0)
	            $.each(item, function(key, lineadetalle){	            	
	            	CargarLineaDetalleAnulada(lineadetalle,dt_listanuladas)
	            });
	        else
	        	CargarLineaDetalleAnulada(item,dt_listanuladas)
	}
		}
		dt_listanuladas.rows().invalidate().draw();
	} 
		

	//*****************************************************************************************
	 function CargarLineaDetalleAnulada(lineadetalle, tabla)
	{	
		var fechas= "", entrada="", contenidos= "", retorno= false;
		var sesiones = lineadetalle.lineadetallezonasesions.Lineadetallezonasesion;
		entrada= lineadetalle.entradas.Entrada.identrada;
		
			
		if (sesiones.length>0)		 
		{
				for (var i=0; i<sesiones.length; i++) {
					if (retorno) {
						fechas+="</br>";
						contenidos+= "</br>";						
					}				
					var f= sesiones[i].zonasesion.sesion.fecha.split("-")[0];
					var hi=sesiones[i].zonasesion.sesion.horainicio;
					fechas+= hi+" "+f;	
					var  no=sesiones[i].zonasesion.sesion.contenido.nombre;
					contenidos+= no;					
									
					retorno= true;
				}	 
				
		 }
		 else
		 {
			fechas= lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.horainicio+" - "+lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.fecha.split("-")[0];
			contenidos= lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.contenido.nombre;
				
		 }
				tabla.row.add([
					entrada,					    
				    contenidos,
				    fechas,
				    lineadetalle.importe, 
				    lineadetalle.cantidad,	
				    "",
				    "",
				    ""
				]).draw();
} 

//***************************************************************
function ajustar_cabeceras_datatable()
{
	$('#historico_anuladas_venta_table').DataTable().columns.adjust().draw();
}


</script>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${tablocalidades_selector_temporada}" var="tablocalidades_selector_temporada_xml" />
<x:parse xml="${tablocalidadesabonos_selector_areas}" var="tablocalidades_selector_areas_xml" />

<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2>
			<spring:message code="common.text.filter_list" />
		</h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">

		<form id="form_filter_list_localidades" class="form-horizontal form-label-left">

			<div class="col-md-6 col-sm-6 col-xs-12">

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.localidades.field.temporada" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">

						<select class="form-control" name="idtemporada" id="idtemporada_localidades">
							<option value=""></option>
							<x:forEach select="$tablocalidades_selector_temporada_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.localidades.field.espectaculo" /></label>
					<div id="espectaculo_localidades_div" class="col-md-8 col-sm-8 col-xs-8">

						<select class="form-control" name="idespectaculo_localidades" id="idespectaculo_localidades">
							<option value=""></option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.localidades.field.sesion" /></label>
					<div id="sesion_localidades_div" class="col-md-8 col-sm-8 col-xs-8">

						<select class="form-control" name="idsesion" id="idsesion_localidades">
							<option value=""></option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label"> <input type="radio" class="flat" checked name="bloqueada" value=""> <spring:message code="venta.numerada_busquedas.tabs.localidades.field.radio.todos" />
					</label> <label class="control-label"> <input type="radio" class="flat" name="bloqueada" value="1"> <spring:message code="venta.numerada_busquedas.tabs.localidades.field.radio.bloqueados" />
					</label> <label class="control-label"> <input type="radio" class="flat" name="bloqueada" value="0"> <spring:message code="venta.numerada_busquedas.tabs.localidades.field.radio.no_bloqueados" />
					</label>
				</div>

			</div>


			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.localidades.field.zona" /></label>
					<div id="zona_div" class="col-md-8 col-sm-8 col-xs-8">

						<select class="form-control" name="nombrearea" id="nombrearea_localidades">
							<option value=""></option>
							<x:forEach select="$tablocalidades_selector_areas_xml/ArrayList/String" var="item">
								<option value="<x:out select="$item" />"><x:out select="$item" /></option>
							</x:forEach>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.localidades.field.fila_desde" /></label>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<input type="text" name="filadesde" class="form-control" value="" />
					</div>
					<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="venta.numerada_busquedas.tabs.localidades.field.fila_hasta" /></label>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<input type="text" name="filahasta" class="form-control" value="" />
					</div>
				</div>

				<div class="form-group">
					<label class="control-label pull-right"> <input type="radio" class="flat" name="impares" value="1"> <spring:message code="venta.numerada_busquedas.tabs.localidades.field.radio.impares" />
					</label> <label class="control-label pull-right"> <input type="radio" class="flat" name="impares" value="0"> <spring:message code="venta.numerada_busquedas.tabs.localidades.field.radio.pares" />
					</label> <label class="control-label pull-right"> <input type="radio" class="flat" checked name="impares" value=""> <spring:message code="venta.numerada_busquedas.tabs.localidades.field.radio.todas" />
					</label>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.localidades.field.observaciones" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<textarea name="observaciones" id="observaciones_localidades" class="form-control" style="resize: none; height: 60px;"></textarea>
					</div>
				</div>


			</div>

			<div class="clearfix"></div>
			<div class="ln_solid"></div>
			<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
				<button id="button_clean_venta_localidades_numerada" type="button" class="btn btn-success pull-right">
					<spring:message code="venta.ventareserva.tabs.canje-bono.button.clean" />
				</button>
				<button id="button_filter_list_localidades" type="button" class="btn btn-success pull-right">
					<spring:message code="common.button.filter" />
				</button>
			</div>

			<input type="hidden" name="start" value=""/>

		</form>


	</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_localidades_unlock">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada_busquedas.tabs.localidades.button.desbloquear" />"> <span class="fa fa-unlock"></span>
			</span>
		</a>
	</div>

	<table id="datatable-list-localidades" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="venta.numerada_busquedas.tabs.localidades.list.header.numero" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.localidades.list.header.observaciones" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.localidades.list.header.area" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.localidades.list.header.fila" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.localidades.list.header.butaca" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.localidades.list.header.espectaculo" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.localidades.list.header.fecha" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.localidades.list.header.hora" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.localidades.list.header.estado" /></th>				
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>


<!--  -->

<script>
//********************************************************************************
$("#idtemporada_localidades").on("change", function(e) {
	
	showFieldSpinner("#espectaculo_localidades_div");	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/list_espectaculos.do'/>",
		timeout : 100000,
		data: {
				idtemporada: $("#idtemporada_localidades").val()
			  }, 
		success : function(data) {
			hideSpinner("#espectaculo_localidades_div");
			var $select=$("#idespectaculo_localidades");
			$select.html('');
			$select.prepend("<option value='' selected='selected'></option>");
		    $.each(data.ArrayList.LabelValue, function(key, val){
		      $select.append('<option value="' + val.value + '">' + val.label + '</option>');
		    });
		},
		error : function(exception) {
			hideSpinner("#espectaculo_localidades_div");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,				  
				  buttons: { sticker: false }				  
			   });		
		}
	});
	
});

//********************************************************************************
$("#idespectaculo_localidades").on("change", function(e) {
	
	showFieldSpinner("#sesion_localidades_div");	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/list_sesiones.do'/>",
		timeout : 100000,
		data: {
				idespectaculo_localidades: $("#idespectaculo_localidades").val()
			  }, 
		success : function(data) {
			hideSpinner("#sesion_localidades_div");
			var $select=$("#idsesion_localidades");
			$select.html('');
			$select.prepend("<option value='' selected='selected'></option>");
			if(data.ArrayList.Sesion.length>0)
		    	$.each(data.ArrayList.Sesion, function(key, val){		    	
		      		$select.append('<option value="' + val.idsesion + '">' + val.fechahora+ '</option>');
		    	});
			else
				$select.append('<option value="' + data.ArrayList.Sesion.idsesion + '">' + data.ArrayList.Sesion.fechahora+ '</option>');
				
		},
		error : function(exception) {
			hideSpinner("#sesion_localidades_div");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,				  
				  buttons: { sticker: false }				  
			   });		
		}
	});
	
});	


//********************************************************************************
var dt_listlocalidades=$('#datatable-list-localidades').DataTable( {
	serverSide: true,
	searching: false,
	ordering: false,
	deferLoading: 0,
	pageLength: 50,
	lengthChange: false,
	pagingType: "simple",
	info: false,
    ajax: {
        url: "<c:url value='/ajax/venta/numerada/list_localidades.do'/>",
        rowId: 'idestadolocalidad',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Estadolocalidad)); return(""); },
        data: function (params) {
        	$('#form_filter_list_localidades input[name="start"]').val(params.start);
        	return ($("#form_filter_list_localidades").serializeObject());
       	},
        error : function(exception) {
        	dt_listlocalidades.processing(false);
			
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}        
    },	
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {
        	if (dt_listlocalidades.data().count()>0 && $("#idespectaculo_localidades").val()!="") 	dt_listlocalidades.columns.adjust().draw();         	
        });
	},
    columns: [
        { data: "localidad.idlocalidad", type: "spanish-string", defaultContent: ""}, 
        { data: "observaciones", type: "spanish-string", defaultContent: ""}, 
        { data: "localidad.nombrearea", type: "spanish-string", defaultContent: ""}, 
        { data: "localidad.fila", className: "cell_centered", defaultContent: ""}, 
        { data: "localidad.butaca", className: "cell_centered", defaultContent: ""}, 
        { data: "sesion.contenido.nombre", type: "spanish-string", defaultContent: ""}, 
        { data: "sesion.fecha", type: "spanish-string", defaultContent: "",
        	render: function ( data, type, row, meta ) {
  	  	  		return(data.split("-")[0]);	
        	} 
        }, 
        { data: "sesion.horainicio", type: "spanish-string", defaultContent: ""}, 
        { data: "estado.nombre", type: "spanish-string", defaultContent: ""}, 
    ],    
    select: { style: 'os'},
	language: dataTableLanguage,
	processing: true,
} );


insertSmallSpinner("#datatable-list-localidades_processing");


//********************************************************************************
$("#button_filter_list_localidades").on("click", function(e) {
	if ($("#idespectaculo_localidades").val()=="") {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
		      text: '<spring:message code="venta.numerada_busquedas.tabs.localidades.alert.espectaculo_no_seleccionado.error" />',
			  type: "error",
			  delay: 5000,			  
			  buttons: { sticker: false }				  
		   });				
	}
	else {
		dt_listlocalidades.ajax.reload();
	}
})


//********************************************************************************
$("#tab_localidades_unlock").on("click", function(e) { 
	
	var data = dt_listlocalidades.rows( { selected: true } ).data();

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.numerada_busquedas.tabs.localidades.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	var vendidas=false;
	for (var i = 0; i < data.length; i++) {
		if (data[i].estado.idestado==8) vendidas=true; 
	}

	if (vendidas) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.numerada_busquedas.tabs.localidades.list.alert.seleccion_vendida" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	$("#modal-dialog-localidades-desbloquear").modal('show');
	setModalDialogSize("#modal-dialog-localidades-desbloquear", "xs");
}); 
/***********************************************BOT�N LIMPIAR  *********************************/
$("#button_clean_venta_localidades_numerada").on("click", function(e) {
	
    $('input[name="filadesde"]').val('');
    $('input[name="filahasta"]').val('');
    $("#observaciones_localidades").val("");
	
	
	 $("#idtemporada_localidades option:first").prop("selected", "selected");
	 $("#idespectaculo_localidades option:first").prop("selected", "selected");
	 $("#idsesion_localidades option:first").prop("selected", "selected");
	 $("#nombrearea_localidades option:first").prop("selected", "selected");
	 
	
})

</script>


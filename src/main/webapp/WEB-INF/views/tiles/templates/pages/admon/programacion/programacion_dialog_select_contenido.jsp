<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${programacion_selector_tiposproducto}" var="programacion_selector_tiposproducto_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="administracion.programacion.dialog.select_contenido.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_contenido_nuevo" class="form-horizontal form-label-left">

		<div class="col-md-12 col-sm-12 col-xs-12">
		
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.dialog.select_contenido.field.tipo_producto" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select class="form-control" name="idtipoprod" id="idtipoprod">
						<option value=""></option>
							<x:forEach select="$programacion_selector_tiposproducto_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.dialog.editar_sesion.field.contenido" /></label>
				<div id="contenido_nuevo_div" class="col-md-9 col-sm-9 col-xs-9">
					<select class="form-control" name="idcontenido_nuevo" id="idcontenido_nuevo">
						<option value=""></option>
					</select>
				</div>
			</div>
			
		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_contenido_nuevo" type="button" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
	</form>
</div>


<script>

$("#idtipoprod").on("change", function(e) {
	showFieldSpinner("#contenido_nuevo_div");
	$select=$("#idcontenido_nuevo");
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/admon/productos/contenidos/list_contenidos.do'/>",
		timeout : 100000,
		data: {
				idtipoproducto: $("#idtipoprod").val()
			  }, 
		success : function(data) {
			hideSpinner("#contenido_nuevo_div");
			$select.html('');
			$select.prepend("<option value='' aforo='0' selected='selected'></option>");
			
			if (data.ArrayList!="") {
				var item= data.ArrayList.Contenido;
				if (item.length>0)
				    $.each(item, function(key, val){
				      $select.append('<option value="' + val.idcontenido + '" aforo="'+ val.aforo +'">' + val.nombre + '</option>');
				    });
				else
				      $select.append('<option value="' + item.idcontenido + '" aforo="'+ item.aforo +'">' + item.nombre + '</option>');
			}
		},
		error : function(exception) {
			hideSpinner("#contenido_nuevo_div");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});
}); 	

$("#save_contenido_nuevo").on("click", function(e) {
	
	$("#idcontenido_dialogo").val($("#idcontenido_nuevo :selected").val());
	$("#contenido").val($("#idcontenido_nuevo :selected").text());
	$("#aforo-total").val($("#idcontenido_nuevo :selected").attr("aforo"));
	$('#aforo-total-calculado').val(calcular_aforo_total()); // calcular_aforo_total() está definida en el diálogo padre
	$("#modal-dialog-form-2").modal('hide');
	
});

</script>

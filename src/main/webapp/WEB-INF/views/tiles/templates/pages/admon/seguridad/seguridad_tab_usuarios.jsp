<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${tabusuarios_selector_perfiles}" var="tabusuarios_selector_perfiles_xml" />
<x:parse xml="${tabusuarios_selector_unidades}" var="tabusuarios_selector_unidades_xml" />
<x:parse xml="${tabusuarios_selector_grupos}" var="tabusuarios_selector_grupos_xml" /> 

<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2><spring:message code="common.text.filter_list" /></h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">

		<form id="form_filter_list_users" class="form-horizontal form-label-left">

			<input id="activo" name="activo" type="hidden" value=""/>
			
			<div class="col-md-5 col-sm-5 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.nombre" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="nombre" type="text" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.apellidos" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="apellidos" type="text" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.dni" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="dni" type="text" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.login" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="login" type="text" class="form-control" style="position: relative;">
					</div>
				</div>
			</div>


			<div class="col-md-7 col-sm-7 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.grupo" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<select class="form-control" name="idgrupo" id="idgrupo">
							<option value=""></option>
							<x:forEach select="$tabusuarios_selector_grupos_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.unidad" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<select class="form-control" name="idunidadnegocio"  id="idunidadnegocio">
							<option value=""></option>
							<x:forEach select="$tabusuarios_selector_unidades_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.usuarios.field.perfil" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<select class="form-control" name="idperfil"  id="idperfil">
							<option value=""></option>
							<x:forEach select="$tabusuarios_selector_perfiles_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
						</select>
					</div>
				</div>
			</div>

			<div class="clearfix"></div>
			<div class="ln_solid"></div>
			<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
				<button id="button_clean_usuarios" type="button" class="btn btn-success pull-right">
						<spring:message code="venta.ventareserva.tabs.canje-bono.button.clean" />
				</button>
				<button id="button_filter_list_users" type="button" class="btn btn-success pull-right">
					<spring:message code="common.button.filter" />
				</button>
			</div>

		</form>


	</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_usuarios_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.seguridad.tabs.usuarios.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_usuarios_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.seguridad.tabs.usuarios.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_usuarios_with_session">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.seguridad.tabs.usuarios.list.button.con_sesion" />"> <span class="fa fa-plug"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_usuarios_lock">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.seguridad.tabs.usuarios.list.button.bloquear" />"> <span class="fa fa-lock"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_usuarios_unlock">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.seguridad.tabs.usuarios.list.button.desbloquear" />"> <span class="fa fa-unlock"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_usuarios_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.seguridad.tabs.usuarios.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>
	</div>

	<table id="datatable-list-users" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="administracion.seguridad.tabs.usuarios.list.header.nombre" /></th>
				<th><spring:message code="administracion.seguridad.tabs.usuarios.list.header.apellidos" /></th>
				<th><spring:message code="administracion.seguridad.tabs.usuarios.list.header.unidad" /></th>
				<th><spring:message code="administracion.seguridad.tabs.usuarios.list.header.grupos" /></th>
				<th><spring:message code="administracion.seguridad.tabs.usuarios.list.header.redes" /></th>
				<th><spring:message code="administracion.seguridad.tabs.usuarios.list.header.bloqueado" /></th>
				<th><spring:message code="administracion.seguridad.tabs.usuarios.list.header.auditado" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>

var dt_listusers=$('#datatable-list-users').DataTable( {	
    ajax: {
        url: "<c:url value='/ajax/admon/seguridad/seguridad/list_users.do'/>",
        rowId: 'idusuario',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Usuario)); return(""); },
        data: function (params) { return($("#form_filter_list_users").serializeObject()); },
        error: function (xhr, error, code)
        {
        	$("#datatable-list-users_processing").hide();
           	new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : xhr.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}					
			});
        }
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listusers.data().count()>0) dt_listusers.columns.adjust().draw(); });
	},
    columns: [
        { data: "nombre", type: "spanish-string", defaultContent: ""}, 
        { data: "apellidos", type: "spanish-string", defaultContent: ""}, 
        { data: "usuariounidadnegocios.Usuariounidadnegocio", className: "cell_centered",
       	  render: function ( data, type, row, meta ) { return showListInCell(data,"<spring:message code="administracion.seguridad.tabs.usuarios.list.text.unidades" />","unidadnegocio.nombre"); }	
        }, 
        { data: "grupousuarios.Grupousuario", className: "cell_centered",
       	  render: function ( data, type, row, meta ) { return showListInCell(data,"<spring:message code="administracion.seguridad.tabs.usuarios.list.text.grupos" />","grupo.nombre"); }	
        },  
        { data: "reds.Red", className: "cell_centered",
       	  render: function ( data, type, row, meta ) { return showListInCell(data,"<spring:message code="administracion.seguridad.tabs.usuarios.list.text.redes" />","nombre"); }	
        }, 
        { data: "cuentabloqueada", className: "text_icon cell_centered",
          render: function ( data, type, row, meta ) {
        	  	  if (data==1) return '<i class="fa fa-check-circle" aria-hidden="true"></i>'; else return '';	
          }}, 
        { data: "auditado", className: "text_icon cell_centered",
          render: function ( data, type, row, meta ) {
	       	  	  if (data==1) return '<i class="fa fa-check-circle" aria-hidden="true"></i>'; else return '';	
          }},        
    ],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-users', 'right');   },
    select: { style: 'os', selector:'td:not(:nth-child(3)):not(:nth-child(4))'},
	language: dataTableLanguage,
	processing: true,
} );


insertSmallSpinner("#datatable-list-users_processing");


//********************************************************************************
$("#button_filter_list_users").on("click", function(e) { dt_listusers.ajax.reload(); })


//********************************************************************************

$("#tab_usuarios_new").on("click", function(e) {
	
	showButtonSpinner("#tab_usuarios_new");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/seguridad/seguridad/show_user.do'/>", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
});



//********************************************************************************
$("#tab_usuarios_edit").on("click", function(e) {
	
	var data = sanitizeArray(dt_listusers.rows( { selected: true } ).data(),"idusuario");
	
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.seguridad.tabs.usuarios.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.seguridad.tabs.usuarios.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	

	showButtonSpinner("#tab_usuarios_edit");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/seguridad/seguridad/show_user.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
});


//********************************************************************************
$("#tab_usuarios_with_session").on("click", function(e) {
	$("#form_filter_list_users #activo").val("1");
	dt_listusers.ajax.reload();
	$("#form_filter_list_users #activo").val("");
});


//********************************************************************************
$("#tab_usuarios_unlock").on("click", function(e) { 
	
	dt_listusers.processing(true);
	
	var data = sanitizeArray(dt_listusers.rows( { selected: true } ).data(),"idusuario");
				
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/admon/seguridad/seguridad/lock_users.do'/>",
		timeout : 100000,
		data: {
	 	       lock: false,
	    	   data: data.toString()
			  }, 
		success : function(data) {
			dt_listusers.processing(false);	
			dt_listusers.ajax.reload(null,false);
		},
		error : function(exception) {
			dt_listusers.processing(false);
			
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});	

}); 



//********************************************************************************
$("#tab_usuarios_lock").on("click", function(e) { 
	
	dt_listusers.processing(true);
	
	var data = sanitizeArray(dt_listusers.rows( { selected: true } ).data(),"idusuario");
				
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/admon/seguridad/seguridad/lock_users.do'/>",
		timeout : 100000,
		data: {
	 	       lock: true,
	    	   data: data.toString()
			  }, 
		success : function(data) {
			dt_listusers.processing(false);	
			dt_listusers.ajax.reload();
		},
		error : function(exception) {
			dt_listusers.processing(false);
			
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",	
				  delay: 5000,
				  buttons: { closer:true, sticker: false }
			   });			
		}
	});

}); 


//********************************************************************************
$("#tab_usuarios_remove").on("click", function(e) { 
		
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="administracion.seguridad.tabs.usuarios.list.confirm.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
  		  buttons: { closer: false, sticker: false	},
  		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
	
			dt_listusers.processing(true);
			
			var data = sanitizeArray(dt_listusers.rows( { selected: true } ).data(),"idusuario");
		   
			$.ajax({
				contenttype: "application/json; charset=utf-8",
				type : "post",
				url : "<c:url value='/ajax/admon/seguridad/seguridad/remove_users.do'/>",
				timeout : 100000,
				data: { data: data.toString() }, 
				success : function(data) {
					dt_listusers.processing(false);	
					dt_listusers.rows( { selected: true } ).remove().draw();
				},
				error : function(exception) {
					dt_listusers.processing(false);
					
					new PNotify({
					      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					      text: exception.responseText,
						  type: "error",		     
						  delay: 5000,
						  buttons: { sticker: false }
					   });			
				}
			});

	   }).on('pnotify.cancel', function() {
	   });		 

}); 

/***********************************************BOTÓN EDITAR VENTA/RESERVA  *********************************/
$("#button_clean_usuarios").on("click", function(e) {
	$('input[name="nombre"]').val('');
	$('input[name="apellidos"]').val('');
	$('input[name="dni"]').val('');
	$('input[name="login"]').val('');

    $("#idgrupo option:first").prop("selected", "selected");
    $("#idunidadnegocio option:first").prop("selected", "selected");
    $("#idperfil option:first").prop("selected", "selected");
})



</script>


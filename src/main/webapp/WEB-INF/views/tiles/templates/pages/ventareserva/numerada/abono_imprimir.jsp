
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.numerada.tabs.abono.imprimnir.title" />
	</h4>
</div>

<div class="modal-body">
<form id="form_numerada_editar_venta_dialog" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
  
  	<div class="form-group">
			<div class="checkbox col-md-3 col-sm-3 col-xs-3">
				<input type="checkbox" name="oabonado" id="oabonado" value="" class="flat" style="position: absolute; opacity: 0;">
				<strong><spring:message code="venta.numerada.tabs.imprimir_abono.abonado" /></strong>
			</div>
			<div class="checkbox col-md-3 col-sm-3 col-xs-3">
				<input type="checkbox" name="ocliente" id="ocliente" value="" class="flat" style="position: absolute; opacity: 0;">
				<strong><spring:message code="venta.numerada.tabs.imprimir_abono.cliente" /></strong>
			</div>
			<a type="button" class="btn btn-info" id="imprimir_abono">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada_busquedas.tabs.abonos.list.button.imprimir" />"> <span class="fa fa-ticket"></span>
			</span>
		</a>	

	</div>	
   
   <div class="tarjeta" id="abono_tarjeta">   
   		<div class="col-md-12 col-sm-12 col-xs-12" id="info_abonado">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.numerada.tabs.imprimir_abono.abonado" />:</label>
				<div class="col-md-7 col-sm-7 col-xs-7" ></div>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12" id="info_cliente">
			<label class="control-label col-md-5 col-sm-5 col-xs-12">
				<spring:message code="venta.numerada.tabs.imprimir_abono.cliente" />:</label>
				<div class="col-md-7 col-sm-7 col-xs-7" ></div>
		</div>			
		<!--FILA-BUTACA-->	
		<div class="col-md-12 col-sm-12 col-xs-12" >
			<div class="col-md-5 col-sm-5 col-xs-12" >
				<label class="control-label col-md-6 col-sm-6 col-xs-12"><spring:message code="venta.numerada.tabs.imprimir_abono.fila" />:</label>
				<div class="control-label col-md-6 col-sm-6 col-xs-12" id="filaImprimir"></div>
			</div>
			<div class="col-md-5 col-sm-5 col-xs-12" >
				<label class="control-label col-md-6 col-sm-6 col-xs-12"><spring:message code="venta.numerada.tabs.imprimir_abono.butaca" />:</label>
				<div class="control-label col-md-6 col-sm-6 col-xs-12" id="butacaImprimir"></div>
			</div>							
		</div>			
		<!--ACCESO-->
		<div class="col-md-12 col-sm-12 col-xs-12" >
			<div class="col-md-5 col-sm-5 col-xs-12" >
				<label class="control-label col-md-6 col-sm-6 col-xs-12"><spring:message code="venta.numerada.tabs.imprimir_abono.acceso" />:</label>
				<div class="control-label col-md-6 col-sm-6 col-xs-12" id="accesoImprimir"></div>
			</div>					
		</div>			
		<!--ZONA-->		
		<div class="col-md-12 col-sm-12 col-xs-12" >					
			<div class="col-md-5 col-sm-5 col-xs-12" >
					<label class="control-label col-md-6 col-sm-6 col-xs-12"><spring:message code="venta.numerada.tabs.imprimir_abono.zona" />:</label>
					<div class="control-label col-md-6 col-sm-6 col-xs-12" id="zonaImprimir"></div>
			</div>	
		</div>				
		<!--TIPO ABONO-->				
		<div class="col-md-12 col-sm-12 col-xs-12" >
			<div class="col-md-10 col-sm-10 col-xs-12" >
				<label class="control-label col-md-6 col-sm-6 col-xs-12"><spring:message code="venta.numerada.tabs.imprimir_abono.tipo" />:</label>
				<div class="control-label col-md-6 col-sm-6 col-xs-12" id="tipoImprimir"></div>
			</div>	
		</div>		
		<!--VALIDO-->		
		<div class="col-md-12 col-sm-12 col-xs-12" >
			<div class="col-md-10 col-sm-10 col-xs-12" >
				<label class="control-label col-md-6 col-sm-6 col-xs-12"><spring:message code="venta.numerada.tabs.imprimir_abono.fecha" />:</label>
				<div class="control-label col-md-6 col-sm-6 col-xs-12" id="validoImprimir"></div>
			</div>				
		</div>			
		<!--ABONO-->
		<div class="col-md-12 col-sm-12 col-xs-12" >
			<div class="col-md-10 col-sm-10 col-xs-12" >
				<label class="control-label col-md-6 col-sm-6 col-xs-12"><spring:message code="venta.numerada.tabs.imprimir_abono.abono" />:</label>
				<div class="control-label col-md-6 col-sm-6 col-xs-12" id="abonoImprimir"></div>
			</div>	
		</div>			
		<div class="codBarrasAbono" id="barcode1D"></div>  
   </div> 
   

	
   	 
</form>
</div>


<script>

mostrarAbono();
$("#info_abonado").hide();
$("#info_cliente").hide();

function mostrarAbono()
{var settings = {
        output:"bmp",
        bgColor: '#FFFFFF',
        color: '#000000',
        barWidth: '1',
        barHeight:'20',
        moduleSize: '5',
        posX: '10',
        posY: '20',
        addQuietZone: '1'
      };

var datos = data_abono_imprimir[0];
$("#barcode1D").barcode(data_abono_imprimir[0].codigobarras, "code128",settings); 
$("#barcode1D").src = $("#barcode1D").html();	
$("#filaImprimir").text(datos.localidad.fila);
$("#butacaImprimir").text(datos.localidad.butaca);
$("#accesoImprimir").text(datos.localidad.acceso.nombre);
$("#zonaImprimir").text(datos.localidad.zona.nombre);
$("#tipoImprimir").text(datos.tipoabono.nombre);
var fecha= datos.fechacaducidad;
fecha = fecha.substr(0,10);
$("#validoImprimir").text(fecha);
$("#abonoImprimir").text(datos.idabono);

}


$('input.flat').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green'
});

$("#oabonado").on("ifChanged", function(e) {
    $("#info_abonado").toggle();
})

$("#ocliente").on("ifChanged", function(e) {
	$("#info_cliente").toggle();
})


//********************************************************************************
$("#imprimir_abono").on("click", function(e) { 
	$("#abono_tarjeta").print();
})


</script>






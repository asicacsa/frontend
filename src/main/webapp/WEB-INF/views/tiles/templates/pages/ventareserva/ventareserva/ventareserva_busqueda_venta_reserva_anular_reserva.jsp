<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${motivosModificacion}" var="motivosModificacion_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.reserva.title" /> ${idReserva}
	</h4>	
</div>

<div class="modal-body">
	<form id="form_anular_venta_reserva" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input type="hidden" name="id_anulacion" id="id_anulacion" value="${idReserva}" />
		<input type="hidden" name="xml_anulacion" id="xml_anulacion" />
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
				<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.motivos" /></label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<select name="selector_motivos_anulacion" id="selector_motivos_anulacion" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$motivosModificacion_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>	
			<div class="form-group">
				<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.observaciones" /></label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<textarea name="observacion_anulacion" id="observacion_anulacion" class="form-control"  ></textarea>
				</div>
			</div>				
		</div>		
	
			<div >
				<div class="clearfix"></div>
			</div>		


	<div class="modal-footer">
		<button id="anular_venta_reserva_button" type="submit" class="btn btn-primary anular_venta_reserva_dialog">
					<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.list.button.anular" />
				</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>		
	</form>
</div>
<script>

hideSpinner("#tab_venta_reserva_anular");
hideSpinner("#tab_anular_anular");
hideSpinner("#tab_abono_anular");

$("#operacion_anulacion_titulo").hide();
$("#operacion_anulacion_1").hide();
$("#operacion_anulacion_2").hide();
$("#operacion_anulacion_3").hide();
$("#diferencia").hide();



var importe= 0;
$('input[name="importe1"]').val(importe);
$("#selector_operacion1 option[value=2]").attr("selected",true);
$("#selector_operacion2 option[value=0]").attr("selected",true);
$("#selector_operacion3 option[value=0]").attr("selected",true);


//********************************************************
$('#importe1').inputmask({alias: 'numeric', 
                       allowMinus: false,  
                       digits: 2, 
                       max: 99999.99});

//*****************************************************
$('#importe2').inputmask({alias: 'numeric', 
                       allowMinus: false,  
                       digits: 2, 
                       max: 99999.99});
//*****************************************************
$('#importe3').inputmask({alias: 'numeric', 
                       allowMinus: false,  
                       digits: 2, 
                       max: 99999.99});
//****************************************************
if ($("input.flat")[0]) {
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });
}
//****************************************************
 
 var contador=0;
 var tipo_forma_pago;
 
 

//****************************************************
$("#form_anular_venta_reserva").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		anularVentaReserva();
	}
}
);
//****************************************************
function anularVentaReserva(){			
		showSpinner("#modal-dialog-form .modal-content");
		
		var xml;
		xml="<Modificacion>";
		xml = xml+"<idmodificacion/>";
		xml = xml+"<motivomodificacion>"+$("#selector_motivos_anulacion option:selected").text()+"</motivomodificacion>";
		xml = xml+"<observaciones>"+$("#observacion_anulacion").val()+"</observaciones>";
		xml = xml+"<venta><idventa/></venta>";
		
		xml = xml+"<reserva>";
			xml = xml+"<idreserva>"+$("#id_anulacion").val()+"</idreserva>";
		xml = xml+"</reserva>";
		xml = xml+"<modificacionimporteparcials>";
			xml = xml+"<Modificacionimporteparcial><importeparcial>";
			xml = xml+"<formapago><idformapago/></formapago>";
			xml = xml+"<bono><numeracion/></bono>";		
			xml = xml+"<importe/><rts/><numpedido/><anulado/>";	
			xml = xml+"</importeparcial></Modificacionimporteparcial>";
			
			xml = xml+"<Modificacionimporteparcial><importeparcial>";
			xml = xml+"<formapago><idformapago/></formapago>";
			xml = xml+"<bono><numeracion/></bono>";		
			xml = xml+"<importe/><rts/><numpedido/><anulado/>";	
			xml = xml+"</importeparcial></Modificacionimporteparcial>";
			
			xml = xml+"<Modificacionimporteparcial><importeparcial>";
			xml = xml+"<formapago><idformapago/></formapago>";
			xml = xml+"<bono><numeracion/></bono>";		
			xml = xml+"<importe/><rts/><numpedido/><anulado/>";	
			xml = xml+"</importeparcial></Modificacionimporteparcial>";
			
		xml = xml+"</modificacionimporteparcials>";
				
		xml = xml+"<modificacionlineadetalles>";
			xml = xml+"<Modificacionlineadetalle>";
				xml = xml+"<lineadetalle>";
					xml = xml+"<idlineadetalle/>";
						xml = xml+"</lineadetalle>";
				xml = xml+"</Modificacionlineadetalle>";
			xml = xml+"</modificacionlineadetalles>";
			xml = xml+"<anulaciondesdecaja/>";
			xml = xml+"</Modificacion>";
		    
			console.log(xml);
			
			$("#xml_anulacion").val(xml);
			
			var data = $("#form_anular_venta_reserva").serializeObject();
		
		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/ventareservas/anular_reserva.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");	
				$("#modal-dialog-form").modal('hide');
				var tipo = "${tipo}";
				if(tipo=="A")
					dt_listventaAbonos.ajax.reload(null,false);
				else
					dt_listbusqueda.ajax.reload(null,false);
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");
	
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});		
		
		
	}

</script>
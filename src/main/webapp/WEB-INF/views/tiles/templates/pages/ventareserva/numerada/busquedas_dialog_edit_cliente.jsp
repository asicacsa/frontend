<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_cliente}" var="editar_cliente_xml" />
<x:parse xml="${editar_cliente_pais}" var="editar_cliente_pais_xml" />
<x:parse xml="${editar_cliente_categorias_actividad}" var="editar_cliente_categorias_actividad_xml" />
<x:parse xml="${editar_cliente_tipos_documento}" var="editar_cliente_tipos_documento_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<x:choose>
			<x:when select="not($editar_cliente_xml/Cliente)">
				<spring:message code="facturacion.facturas.tabs.clientes.dialog.crear_cliente.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="facturacion.facturas.tabs.clientes.dialog.editar_cliente.title" />
			</x:otherwise>
		</x:choose>		
	</h4>	
</div>


<!-- Pesta�as ------------------------------------------------------------->
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 maintab">
		<div class="" role="tabpanel" data-example-id="togglable-tabs">
			<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
				<li role="presentation" class="active"><a href="#tab_datos" id="datos-tab" role="tab" data-toggle="tab" aria-expanded="true"><spring:message code="facturacion.facturas.tabs.clientes.datos.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_direccion" role="tab" id="direccion-tab" data-toggle="tab" aria-expanded="false"><spring:message code="facturacion.facturas.tabs.clientes.direccion.origen.title" /></a></li>
			</ul>
		</div>
	</div>
</div>

  


<div class="modal-body">
	<form id="form_cliente_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	<input type="hidden" name="cliente_xml" id="cliente_xml">
	
		<!-- Area de trabajo ------------------------------------------------------>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel thin_padding">
		
					<div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade active in" id="tab_datos" aria-labelledby="datos-tab">
							<tiles:insertAttribute name="datos_cliente" />
						</div>	
						<div role="tabpanel" class="tab-pane fade" id="tab_direccion" aria-labelledby="direccion-tab">
							<c:set var = "direccion" scope = "request" value = "origen"/>
							<x:set var = "direccion_xml" select = "$editar_cliente_xml/Cliente/direccionorigen" scope="request"/>
							<tiles:insertAttribute name="datos_direccion" />						
						</div>	
					</div>
		
					<div class="clearfix"></div>
				</div>
			</div>
		</div>


	</form>
	
	<div class="modal-footer">

		<button id="save_cliente_button" type="button" class="btn btn-primary save_cliente_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>
<tiles:insertAttribute name="nuevo_cp" />

<script>

hideSpinner("#tab_clientes_edit");

function generarXmlDireccion(tipoDireccion)
{
  var xml="<direccion"+tipoDireccion+">";
  xml+="<iddireccion>"+$("#idDireccion_"+tipoDireccion).val()+"</iddireccion>";
  xml+="<personacontacto>"+$("#personacontacto_"+tipoDireccion).val()+"</personacontacto>";
  var tipo_via = $("#tipovia_"+tipoDireccion+" option:selected").text();
  xml+="<tipovia>"+tipo_via+"</tipovia>";
  xml+= "<direccion>"+$("#direccion_"+tipoDireccion).val()+"</direccion>";
  var pais = $("#selector_pais_"+tipoDireccion).val();
  if(pais=='')
	  {
	  pais = '472';
	  }
  xml+="<pais><idpais>"+pais+"</idpais>";
  var nombrePais = $("#selector_pais_"+tipoDireccion+" option:selected").text();
  xml+="<nombre>"+nombrePais+"</nombre></pais>";
  xml+="<provincia><idprovincia>"+$("#idProvincia_"+tipoDireccion).val()+"</idprovincia>";
  xml+="<nombreprovincia>"+$("#filter_provincia_"+tipoDireccion).val()+"</nombreprovincia></provincia>";  
  xml+="<cp><idcp>"+$("#idCp_"+tipoDireccion).val()+"</idcp>";
  var cp = $("#filter_cp_"+tipoDireccion).val();
  xml+="<cp>"+$("#filter_cp_"+tipoDireccion).val()+"</cp></cp>";  
  xml+="<municipio><idmunicipio>"+$("#idMunicipio_"+tipoDireccion).val()+"</idmunicipio>";
  xml+="<nombre>"+$("#filter_poblacion_"+tipoDireccion).val()+"</nombre></municipio>";  
  xml+="<telefonofijo>"+$("#telefonofijo_"+tipoDireccion).val()+"</telefonofijo>";
  xml+="<telefonomovil>"+$("#telefonomovil_"+tipoDireccion).val()+"</telefonomovil>";
  xml+="<fax>"+$("#fax_"+tipoDireccion).val()+"</fax>";
  xml+="<correoelectronico>"+$("#correoelectronico_"+tipoDireccion).val()+"</correoelectronico>";  
  xml+="</direccion"+tipoDireccion+">";
  return xml;
}




function filtrarProvincia(nombre,idPais,provincia) {
        
    if(nombre!='' && nombre.length>2)
    {
    	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/facturacion/facturas/clientes/select_list_provincias.do?idPais='/>"+idPais+"&nombre="+nombre,
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				provincia.find('option').remove();
				provincia.append('<option selected value="">Seleccione provincia</option>');
				if (data.ArrayList!="") {
                    var item=data.ArrayList.LabelValue;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	provincia.append('<option value="' + val.value + '">' + val.label + '</option>');
                        });
                    else
                    	{
                    	provincia.append('<option value="' + item.value + '">' + item.label + '</option>');
                    	}
             }

				provincia.show();
			
			}
		});

    }
}




function filtrarPoblacion(nombre,idPais,idProvincia,poblacion) {
    
    if(nombre!='' && nombre.length>3)
    {

    	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/facturacion/facturas/clientes/select_list_poblaciones.do?idPais='/>"+idPais+"&idProvincia="+idProvincia+"&nombre="+nombre,
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				poblacion.find('option').remove();	
				poblacion.append('<option selected value="">Seleccione poblaci�n</option>');
				if (data.ArrayList!="") {
                    var item=data.ArrayList.LabelValue;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	poblacion.append('<option value="' + val.value + '">' + val.label + '</option>');
                        });
                    else
                    	poblacion.append('<option selected value="' + item.value + '">' + item.label + '</option>');
             }				
				poblacion.show();				
			}
		});

    }
}





function filtrarCP(nombreCp,idPais,idProvincia,idMunicipio,cp,idCp) {
	nombre = nombreCp.val();
    if(nombre!='' && nombre.length==5)
    {
     	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/facturacion/facturas/clientes/select_list_CP.do?idPais='/>"+idPais+"&idProvincia="+idProvincia+"&idMunicipio="+idMunicipio+"&nombre="+nombre,
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				
				cp.find('option').remove();	
				if (data.ArrayList!="") {
                    var item=data.ArrayList.MunicipioCP;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	cp.append('<option value="' + val.value + '">' + val.label + '</option>');
                        });
                    else
                    	{
                    	nombreCp.val(item.texto);
                    	idCp.val(item.idcp);
                    	cp.append('<option selected value="' + item.idcp + '">' + item.texto + '</option>');
                    	}
                    
             }				
			// cp.show();
				
			}
		});

    }
}

$(".save_cliente_dialog").on("click", function(e) {

	showSpinner("#modal-dialog-form .modal-content");
	
	var cliente_json=${editar_cliente_json};
	
	cliente_json.Cliente.nombre=$("#form_cliente_data #nombre").val();
	cliente_json.Cliente.apellido1=$("#form_cliente_data #apellido1").val();
	cliente_json.Cliente.apellido2=$("#form_cliente_data #apellido2").val();
	cliente_json.Cliente.nombrecompleto=$("#form_cliente_data #nombre").val()+" "+$("#form_cliente_data #apellido1").val()+" "+$("#form_cliente_data #apellido2").val();
	cliente_json.Cliente.actividad.idactividad=$("#form_cliente_data #idcategoriacategoriaactividad").val();
	cliente_json.Cliente.tipoidentificador.idtipoidentificador=$("#form_cliente_data #idtipoidentificador").val();
	cliente_json.Cliente.identificador=$("#form_cliente_data #identificador").val();
	cliente_json.Cliente.observaciones=$("#form_cliente_data #descripcion").val();

	var xml=json2xml(cliente_json);	
	xml=xml.replace(/(<direccionorigen\>).*(<\/direccionorigen>)/g, generarXmlDireccion("origen"));

	$("#cliente_xml").val(xml);
	var data = $("#form_cliente_data").serializeObject();		
	
    $.ajax({
			type : "post",
			url : "<c:url value='/ajax/venta/numerada/save_cliente.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				
				$("#modal-dialog-form").modal('hide');
										
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
});


</script>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<x:parse xml="${tabtiposproductos_data}" var="tabtiposproductos_datas_xml" />

<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2><spring:message code="common.text.filter_list" /></h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">

		<form id="form_filter_list_tiposproductos" class="form-horizontal form-label-left">
		
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.tiposproductos.field.unidad" /></label>
					<div class="col-md-5 col-sm-5 col-xs-5">
						<select class="form-control" id="idunidadnegocio" name="idunidadnegocio">
							<option value=""></option>
							<x:forEach select="$tabtiposproductos_datas_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
						</select>
					</div>
				</div>				
			</div>
			
			<div class="clearfix"></div>
			<div class="ln_solid"></div>			
			<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
				<button id="button_filter_list_tiposproductos" type="button" class="btn btn-success pull-right">
					<spring:message code="common.button.filter" />
				</button>
			</div>					
		</form>
	</div>
</div>

 <div class="col-md-12 col-sm-12 col-xs-12">
 
 
 <div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_tipoproductos_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.tiposproductos.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_tipoproductos_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.tiposproductos.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_tipoproductos_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.tiposproductos.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>		
	</div>
	<table id="datatable-lista-tiposproductos" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="administracion.productos.tabs.tiposproductos.list.header.nombre" /></th>
				<th><spring:message code="administracion.productos.tabs.tiposproductos.list.header.descripcion" /></th>								
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>

var dt_listtiposproductos=$('#datatable-lista-tiposproductos').DataTable( {	
	 ajax: {
	 url: "<c:url value='/ajax/admon/productos/tiposproductos/list_tiposproductos.do'/>",
	 rowId: 'idtipoproducto',
	 type: 'POST',
	 dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.tipoproducto)); return(""); },
	 error: function (xhr, error, thrown) {
		 
	        if (xhr.responseText=="403") {
	              $("#tiposproductos-tab").hide();
	        }    
	        else
	      	 {
	      	 	$("#datatable-lista-tiposproductos_processing").hide();
	      		new PNotify({
						title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						text : xhr.responseText,
						type : "error",
						delay : 5000,
						buttons : {
							closer : true,
							sticker : false
						}					
					});
	      	 }
	 },

	 data: function (params) { return($("#form_filter_list_tiposproductos").serializeObject()); }
	 },
	 initComplete: function( settings, json ) {
	 $('a#menu_toggle').on("click", function () {if (dt_listtiposproductos.data().count()>0) dt_listtiposproductos.columns.adjust().draw(); });
	 },
	 columns: [
	 { data: "nombre", type: "spanish-string" ,  defaultContent:""} ,
	 { data: "descripcion", type: "spanish-string" ,  defaultContent:""}
	 
	 ],    
	 drawCallback: function( settings ) {
	 $('[data-toggle="tooltip"]').tooltip();
	 },
	 select: { style: 'os'},
	 language: dataTableLanguage,
	 processing: true,
	 } );
	 
	insertSmallSpinner("#datatable-lista-tiposproductos_processing");
/************************************************BOT�N FILTRAR******************************************************/
$("#button_filter_list_tiposproductos").on("click", function(e) {	
	dt_listtiposproductos.ajax.reload();
})
/************************************************BOT�N ELIMINAR******************************************************/
 $("#tab_tipoproductos_remove").on("click", function(e) { 
		
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="administracion.productos.tabs.tiposproductos.list.confirm.eliminar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
	  		  buttons: { closer: false, sticker: false	},
	  		  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
		
			   dt_listtiposproductos.processing(true);
				
				var data = sanitizeArray(dt_listtiposproductos.rows( { selected: true } ).data(),"idtipoproducto");
			   
				$.ajax({
					contenttype: "application/json; charset=utf-8",
					type : "post",
					url : "<c:url value='/ajax/admon/productos/contenidos/remove_tipoproductos.do'/>",
					timeout : 100000,
					data: { data: data.toString() }, 
					success : function(data) {
						dt_listtiposproductos.ajax.reload();
						recargar_tipos_producto()
					},
					error : function(exception) {
						dt_listtiposproductos.processing(false);
						
						new PNotify({
						      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						      text: exception.responseText,
							  type: "error",		     
							  delay: 5000,
							  buttons: { sticker: false }
						   });			
					}
				});

		   }).on('pnotify.cancel', function() {
		   });		 

	});  




	$("#tab_tipoproductos_new").on("click", function(e) { 
		idunidadnegocio = $("#idunidadnegocio").val();
		if(idunidadnegocio!='')
			{
			showButtonSpinner("#tab_tipoproductos_new");
			$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/productos/productos/show_tipo_producto.do'/>?idUnidadNegocio="+idunidadnegocio, function() {										  
				$("#modal-dialog-form").modal('show');
				setModalDialogSize("#modal-dialog-form", "md");
			});
			return;
			}
		else
			{
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.productos.tabs.tipoproductos.list.alert.seleccion_unidad" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
			}
		
	})
	
	$("#tab_tipoproductos_edit").on("click", function(e) { 
	var data = sanitizeArray(dt_listtiposproductos.rows( { selected: true } ).data(),"idtipoproducto");
	idunidadnegocio = $("#idunidadnegocio").val();
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.productos.tabs.tipoproductos.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.productos.tabs.tipoproductos.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#tab_tipoproductos_edit");
	$("#modal-dialog-form .modal-content:first").load("<c:url value='/ajax/admon/productos/productos/show_tipo_producto.do'/>?idUnidadNegocio="+idunidadnegocio+"&id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
})
	


</script>


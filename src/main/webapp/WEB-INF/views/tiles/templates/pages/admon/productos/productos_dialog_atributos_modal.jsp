<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="modal fade" id="modal_atributos" tabindex="-1"
	role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close close_dialog" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">�</span>
				</button>
				<h4 class="modal-title">
						<spring:message code="administracion.productos.tabs.productos.atributos.title" />			
				</h4>
			</div>
			<div class="modal-body">

				<form id="contenido_atributos" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
				<input type="hidden" id="idTipoProductoAtributo" />
				<div class="col-md-12 col-sm-12 col-xs-12">
		
					<div class="form-group">
								<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.tipoproductos.field.nombre" />*</label>
								<div class="col-md-8 col-sm-8 col-xs-8">
									<input type="text" name="nombreAtributo" id="nombreAtributo" class="form-control" value=""/>
								</div>
					</div>
		
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.tipoproductos.field.tipo" />*</label>
						<div class="col-md-8 col-sm-8 col-xs-8">
							<select name="idatributo" id="selector_atributo" class="form-control" required="required">
															
							</select>
						</div>
					</div>				    
		
		
				</div>
		
			</form>
	




		  <div class="modal-footer">
		
				<button id="close_atributo" type="button" class="btn btn-primary" data-dismiss="modal">
					<spring:message code="common.button.accept" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
		</div>
			</div>
			
		</div>
	</div>
</div>

<script>

function limpiarModalAtributo(datatableNew)
{
	datatableAtributo = datatableNew;
	$("#idTipoProductoAtributo").val("");
	$("#idatributo").val("");
	$("#nombreAtributo").val("");
	$("#modal_atributos").modal('show');
	setModalDialogSize("#modal_atributos", "sm");	
}

function editarModalAtributo(datatableNew)
{
	datatableAtributo = datatableNew;
	var datos = datatableAtributo.rows( '.selected' ).data()[0];
	var creando = false;
	$("#idTipoProductoAtributo").val(datos[0]);
	$("#selector_atributo").val(datos[1]);
	$("#nombreAtributo").val(datos[2]);
	$("#modal_atributos").modal('show');
	setModalDialogSize("#modal_atributos", "sm");	
}

$("#close_atributo").on("click", function(e) { 

	var idTipoProductoAtributo = $("#idTipoProductoAtributo").val();
	
	if(idTipoProductoAtributo!='')
		datatableAtributo.rows( '.selected' ).remove(); 
	
	
		
	datatableAtributo.row.add( [  $("#idTipoProductoAtributo").val(),$("#selector_atributo").val(),$("#nombreAtributo").val(), $("#selector_atributo option:selected").text()] )
	    	.draw();
	$("#modal_atributos").modal('hide');
		
})

</script>

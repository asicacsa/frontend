<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${newabono_selector_productosdisponibles}" var="newabono_selector_productosdisponibles_xml" />
<x:parse xml="${newabono_selector_modelospase}" var="newabono_selector_modelospase_xml" />
<x:parse xml="${newabono_selector_tiposidentificador}" var="newabono_selector_tiposidentificador_xml" />
<x:parse xml="${newabono_selector_sexos}" var="newabono_selector_sexos_xml" />
<!--< xml="newabono_selector_tiposvia}" var="newabono_selector_tiposvia_xml" />--><!-- TODO QUITAR -->
<x:parse xml="${newabono_selector_provincias}" var="newabono_selector_provincias_xml" />
<x:parse xml="${newabono_selector_parentesco}" var="newabono_selector_parentesco_xml" />
				


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.editar_abono.title" />:					
	</h4>
</div>
<div class="modal-body">
		
<form id="form_edit_abono" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="form-group"><!--  TODO Hacerlo no editable -->
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.producto" />:</label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<select name="selector_producto_pases_bono" id="selector_producto_pases_bono" class="form-control" required="required" disabled">
						<option value=""></option>
						<x:forEach select="$newabono_selector_productosdisponibles_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.modelo" />:</label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<select name="selector_modelo_pases_bono" id="selector_modelo_pases_bono" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$newabono_selector_modelospase_xml/ArrayList/ModeloPase" var="item">
							<option value="<x:out select="$item/idmodelopase" />"><x:out select="$item/descripcion" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			<div class="form-group col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-3 col-sm-3 col-xs-12">
					<select name="selector_identificador_pases_bono" id="selector_identificador_pases_bono" class="form-control" >
						<option value=""></option>
						<x:forEach select="$newabono_selector_tiposidentificador_xml/ArrayList/Tipoidentificador" var="item">
							<option value="<x:out select="$item/idtipoidentificador" />"><x:out select="$item/nombre" /></option>
						</x:forEach>
					</select>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12">
						<input name="identificador_pases_club" id="identificador_pases_club" type="text" class="form-control" required="required" value="">
					</div>
		
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.nombre" /></label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<input name="nombre_new_pases_club" id="nombre_new_pases_club" type="text" class="form-control" required="required" value="">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.apellido1" /></label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<input name="apellido1_new_pases_club" id="apellido1_new_pases_club" type="text" class="form-control" required="required" value="">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.apellido2" /></label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<input name="apellido2_new_pases_club" id="apellido2_new_pases_club" type="text" class="form-control" value="">
				</div>
			</div>	
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.sexo" />:</label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<select name="selector_sexo_pases_bono" id="selector_sexo_pases_bono" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$newabono_selector_sexos_xml/ArrayList/CacSexo" var="item">
							<option value="<x:out select="$item/codSexo" />"><x:out select="$item/descripcion" /></option>
						</x:forEach>
					</select>
				
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.perfil" /></label>
				<div class="col-md-8 col-sm-8 col-xs-12">
				     <select name="selector_perfil_pases_bono" id="selector_perfil_pases_bono" class="form-control" required="required">
						<option value=""></option>						
					</select>
				</div>
			</div>	
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.fechanacimiento" /></label>
				<div class="col-md-9 col-sm-9 col-xs-12">
	            	<div class="form-group date-picker">
						<div class="col-md-6 col-sm-6 col-xs-12">
						  <button type="button" class="btn btn-default btn-clear-date" id="button_fecha_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.formasdepago.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
						  </button>			
                        <div class="input-prepend input-group">
                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                          <input type="text" name="fecha_nacimiento_pases" id="fecha_nacimiento_pases" class="form-control" value=""/>
                          <input type="hidden" required="required" name="fecha_nacimiento_pases_club" />
                        </div>
					</div>
				</div>		
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.tipovia" />:</label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<select name="selector_tipos_via_pases_bono" id="selector_tipos_via_pases_bono" class="form-control" required="required">
						<option value=""></option>
						<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.alameda.new" />"><spring:message code="facturacion.facturas.tabs.clientes.field.alameda.new" /></option>							
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.avenida" />"><spring:message code="facturacion.facturas.tabs.clientes.field.avenida" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.calle.new" />"><spring:message code="facturacion.facturas.tabs.clientes.field.calle.new" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.camino.new" />"><spring:message code="facturacion.facturas.tabs.clientes.field.camino.new" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.carretera" />"><spring:message code="facturacion.facturas.tabs.clientes.field.carretera" /></option>							
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.glorieta" />"><spring:message code="facturacion.facturas.tabs.clientes.field.glorieta" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.pasaje" />"><spring:message code="facturacion.facturas.tabs.clientes.field.pasaje" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.paseo.new" />"><spring:message code="facturacion.facturas.tabs.clientes.field.paseo.new" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.plaza.new" />"><spring:message code="facturacion.facturas.tabs.clientes.field.plaza.new" /></option>							
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.rambla" />"><spring:message code="facturacion.facturas.tabs.clientes.field.rambla" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.ronda.new" />"><spring:message code="facturacion.facturas.tabs.clientes.field.ronda.new" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.sector" />"><spring:message code="facturacion.facturas.tabs.clientes.field.sector" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.travesia" />"><spring:message code="facturacion.facturas.tabs.clientes.field.travesia" /></option>
							<option value="<spring:message code="facturacion.facturas.tabs.clientes.field.urbanizacion" />"><spring:message code="facturacion.facturas.tabs.clientes.field.urbanizacion" /></option>
						<!--  COGEMOS LOS DE COLOSSUS PARA DESACOPLAR SIM
						<x:forEach select=newabono_selector_tiposvia_xml/ArrayList/CacTipoVia" var="item">
							<option value="< select=item/codTipoVia" />">< select=item/descripcion" /></option>
						</x:forEach>
						-->
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.via" /></label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<input name="tipovia_new_pases_club" id="tipovia_new_pases_club" type="text" class="form-control"  value="">
				</div>
			</div>
			<!-- 
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.numvia" /></label>
				<div class="col-md-5 col-sm-5 col-xs-12">
					<input name="numvia_new_pases_club" id="numvia_new_pases_club" type="text" class="form-control"  value="">
				</div>
			</div>		
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.complemento" /></label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<input name="complementovia_new_pases_club" id="complementovia_new_pases_club" type="text" class="form-control" value="">
				</div>
			</div>
			 -->
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.cp" /></label>
				<div class="col-md-5 col-sm-5 col-xs-12">
					<input name="cp_new_pases_club" id="cp_new_pases_club" type="text" class="form-control"  value="">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.provincia" />:</label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<select name="selector_provincia_pases_bono" id="selector_provincia_pases_bono" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$newabono_selector_provincias_xml/ArrayList/Provincia" var="item">
							<option value="<x:out select="$item/idprovincia" />"><x:out select="$item/nombreprovincia" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.poblacion" /></label>
				<div class="col-md-8 col-sm-8 col-xs-12">
						
					<select name="selector_poblacion_pases_bono" id="selector_poblacion_pases_bono" class="form-control" >
						<option value=""></option>						
					</select>
				</div>
			</div>	
			
						
	</div>
<div class="col-md-6 col-sm-6 col-xs-12">
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.tfnofijo" /></label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<input name="tfno_fijo_new_pases_club" id="tfno_fijo_new_pases_club" data-inputmask="'mask': '9{0,10}'" data-mask type="text" class="form-control" value="">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.tfnomovil" /></label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<input name="tfno_movil_new_pases_club" id="tfno_movil_new_pases_club" data-inputmask="'mask': '9{0,10}'" data-mask type="text" class="form-control" value="">
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">	
			<input name="informacionmovil" id="informacionmovil" type="checkbox" class="js-switch" data-switchery="true" style="display: none;">
				<strong><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.informacion" /></strong>
		</div>
	</div>		
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.email" /></label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<input name="email_new_pases_club" id="email_new_pases_club" type="text" class="form-control" value="">
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">	
			<input name="informacionemail" id="informacionemail" type="checkbox" class="js-switch" data-switchery="true" style="display: none;">
			<strong><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.informacion" /></strong>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.rol" />:</label>
		<div class="col-md-8 col-sm-8 col-xs-12">
			<select name="selector_rol_pases_bono" id="selector_rol_pases_bono" class="form-control" required="required">
				<option value=""></option>
				<x:forEach select="$newabono_selector_parentesco_xml/ArrayList/ParentescoNew" var="item">
					<option value="<x:out select="$item/codParentesco" />"><x:out select="$item/descripcion" /></option>
				</x:forEach>
			</select>
		</div>
	</div>
	<div class="form-group">
	
		<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.nombreFoto" />:</label>
		<div class="col-md-9 col-sm-9 col-xs-12">
			<input name="nombrefoto" id="nombrefoto" type="text" class="form-control" value="">
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1 col-sm-1 col-xs-12">&nbsp;</div>
		    <div class="col-md-10 col-sm-10 col-xs-12">	
			<!-- <br/><canvas id="myCanvas" width="380" height="350"></canvas><br/>	 -->
			<br/><canvas id="myCanvas" width="240" height="320"></canvas><br/>
			</div>
			<div class="col-md-1 col-sm-1 col-xs-12">&nbsp;</div>	    
		</div>	
		 <div class="btn-group pull-right  btn-datatable">	
		<button type="button" class="btn btn-success" id="tab_pases_club_foto">
			<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.foto"/>
		</button>
		</div>
	</div>	
</div>
</form>
	<div class="modal-footer">
		<button id="finalizar_button"  type="button" class="btn btn-primary save_dialog">
					<spring:message code="common.button.ok" />
				</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>		
	</div>		

</div>

<script>
//***************************************************************
var json_datos=${newabono_datos_usuario};

cargar();






function cargar()
{
	 $('#selector_producto_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompletoNew.Producto.idproducto+'"]').attr("selected", "selected");
	 new_abono_cargar_selector_perfiles();
	 $('#selector_provincia_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompletoNew.Provincia.idprovincia+'"]').attr("selected", "selected");  
	 new_abono_cargar_selector_poblacion(json_datos.DatosClienteClubDTOCompletoNew.Provincia.idprovincia);
	 $('#selector_modelo_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompletoNew.CacModeloPase+'"]').attr("selected", "selected");
	 $('#selector_identificador_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompletoNew.Tipoidentificador.idtipoidentificador+'"]').attr("selected", "selected");
	 $('#selector_rol_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompletoNew.ParentescoNew.codParentesco+'"]').attr("selected", "selected");
	 $("#nombre_new_pases_club").val(json_datos.DatosClienteClubDTOCompletoNew.nombre);
	 $("#identificador_pases_club").val(json_datos.DatosClienteClubDTOCompletoNew.nif);
	 $("#apellido1_new_pases_club").val(json_datos.DatosClienteClubDTOCompletoNew.apellido1);
	 $("#apellido2_new_pases_club").val(json_datos.DatosClienteClubDTOCompletoNew.apellido2);
	 $('#selector_sexo_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompletoNew.CacSexo+'"]').attr("selected", "selected");
	 $('#selector_perfil_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompletoNew.Perfilvisitante.idperfilvisitante+'"]').attr("selected", "selected");
	 $('#selector_tipos_via_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompletoNew.CacTipoVia+'"]').attr("selected", "selected");//TODO REVISAR NO CARGA LO VIEJO
	 //GGL Meto la fecha de nacimiento porque no venía
	 $("#fecha_nacimiento_pases").val(json_datos.DatosClienteClubDTOCompletoNew.fechanacimiento.split("-")[0]);
	 $("#tipovia_new_pases_club").val(json_datos.DatosClienteClubDTOCompletoNew.via);
	 //$("#numvia_new_pases_club").val(json_datos.DatosClienteClubDTOCompletoNew.numvia);
	 $("#cp_new_pases_club").val(json_datos.DatosClienteClubDTOCompletoNew.cp);	 
	 $("#tfno_fijo_new_pases_club").val(json_datos.DatosClienteClubDTOCompletoNew.fijo);
	 $("#tfno_movil_new_pases_club").val(json_datos.DatosClienteClubDTOCompletoNew.movil);
	 $("#email_new_pases_club").val(json_datos.DatosClienteClubDTOCompletoNew.email);
	 //GGL ESTO SOBRA $('#selector_rol_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompletoNew.ParentescoNew+'"]').attr("selected", "selected");
	 $('#nombrefoto').val(json_datos.DatosClienteClubDTOCompletoNew.nombrefoto);
	
	 if(json_datos.DatosClienteClubDTOCompletoNew.deseaemail==1)
		{
		 $('#informacionemail').prop('checked', true);			
		}
	 
	 if(json_datos.DatosClienteClubDTOCompletoNew.deseamovil==1)
		{		 
		 $('#informacionmovil').prop('checked', true);		 
		}
	 
	//**************************************************************
	 $(".js-switch").each(function(index) {
	 	var s = new Switchery(this, {
	 		size : 'small'
	 	});
	 });
	 
	 $("#finalizar_button").show();
	 
	
	 
}


var data;
//***************************************************************

//GGL$("#grabar_anadir_button").hide();
//***************************************************************
hideSpinner("#tab_pases_club_new");
//***************************************************************
// $("#tab_pases_club_new_validar").on("click", function(e) {
	
// 	if( $('#selector_producto_pases_bono').val()=="")
// 	{
// 		new PNotify({
// 			title: '<spring:message code="common.dialog.text.error" />',
// 			text: '<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.alert.ningun_producto" />',
// 			type: "error",
// 			delay: 5000,
// 			buttons: { sticker: false }
// 			});
// 	 }else
// 	 {
// 		 if( $('#selector_identificador_pases_bono').val()=="")
// 			{
// 				new PNotify({
// 					title: '<spring:message code="common.dialog.text.error" />',
// 					text: '<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.alert.ningun_identificador" />',
// 					type: "error",
// 					delay: 5000,
// 					buttons: { sticker: false }
// 					});
// 			 }else
// 				 {	
// 				  var data = $("#form_new_abono").serializeObject();
// 				  showSpinner("#modal-dialog-form .modal-content");			
// 					$.ajax({
// 						type : "post",
// 						url : "<c:url value='/ajax/venta/ventareserva/pases_club/validar_contra_sim.do'/>",
// 						timeout : 100000,
// 						data : data,
// 						success : function(data) {
// 							 hideSpinner("#modal-dialog-form .modal-content");
// 						     json_datos=data;							 
// 							 $("#nombre_new_pases_club").val(json_datos.DatosClienteClubDTOCompleto.nombre);
// 							 $("#apellido1_new_pases_club").val(json_datos.DatosClienteClubDTOCompleto.apellido1);
// 							 $("#apellido2_new_pases_club").val(json_datos.DatosClienteClubDTOCompleto.apellido2);
// 							 $('#selector_sexo_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompleto.CacSexo.codSexo+'"]').attr("selected", "selected");
// 							 $('#selector_perfil_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompleto.Perfilvisitante+'"]').attr("selected", "selected");
// 							 $('#selector_tipos_via_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompleto.CacTipoVia.codTipoVia+'"]').attr("selected", "selected");
// 							 $("#tipovia_new_pases_club").val(json_datos.DatosClienteClubDTOCompleto.via);
// 							 $("#numvia_new_pases_club").val(json_datos.DatosClienteClubDTOCompleto.numvia);
// 							 $("#cp_new_pases_club").val(json_datos.DatosClienteClubDTOCompleto.cp);
// 							 $('#selector_provincia_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompleto.Provincia.idprovincia+'"]').attr("selected", "selected");
// 							 $('#selector_poblacion_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompleto.Municipio.idmunicipio+'"]').attr("selected", "selected");
// 							 $("#tfno_fijo_new_pases_club").val(json_datos.DatosClienteClubDTOCompleto.fijo);
// 							 $("#tfno_movil_new_pases_club").val(json_datos.DatosClienteClubDTOCompleto.movil);
// 							 $("#email_new_pases_club").val(json_datos.DatosClienteClubDTOCompleto.email);
// 							 $('#selector_rol_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompleto.CacParentesco+'"]').attr("selected", "selected");
							 
// 							 $("#finalizar_button").show();
// 							 $("#grabar_anadir_button").show();
							 
							 
							
// 						},
// 						error : function(exception) {
							
// 							hideSpinner("#modal-dialog-form .modal-content");
			
// 							new PNotify({
// 								title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
// 								text : exception.responseText,
// 								type : "error",
// 								delay : 5000,
// 								buttons : {
// 									closer : true,
// 									sticker : false
// 								}
// 							});
// 						}
// 					});
				 
// 				 }//fin else
// 	 }
// });
//**************************************************************
$( "#selector_producto_pases_bono" ).change(function() {
	new_abono_cargar_selector_perfiles();
})	

//**************************************************************
function new_abono_cargar_selector_perfiles()
{	
	var producto = $("#selector_producto_pases_bono").val();
	$("#selector_perfil_pases_bono").find("option").remove();
		
	$.ajax({
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/pases_club/select_list_perfiles.do'/>?id="+producto,
		timeout : 100000,
		data : null,
		dataType : 'json',
		success : function(data) {			
			if (data.ArrayList.LabelValue!=null && typeof data.ArrayList.LabelValue!="undefined") {
			$.each(data.ArrayList.LabelValue, function(i, item) {
			    $("#selector_perfil_pases_bono").append('<option value="'+item.value[0]+'">'+item.label+'</option>');			    
			});
			$('#selector_perfil_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompletoNew.Perfilvisitante.idperfilvisitante+'"]').attr("selected", "selected");
			}
		}
	});
}
//**************************************************************
$today= moment().format("DD/MM/YYYY");
dia_inicio = $today;

if($('input[name="fecha_nacimiento_pases"]').val()!='')
{	
dia_inicio = $('input[name="fecha_nacimiento_pases"]').val().substring(0, 10);
}

$('input[name="fecha_nacimiento_pases_club"]').val( dia_inicio);

$('input[name="fecha_nacimiento_pases"]').daterangepicker({
	singleDatePicker: true,
	autoUpdateInput: false,
	linkedCalendars: false,
	showDropdowns: true,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start) {
  	  	 $('input[name="fecha_nacimiento_pases"]').val(start.format('DD/MM/YYYY'));
  	  	 $('input[name="fecha_nacimiento_pases_club"]').val(start.format('DD/MM/YYYY'));
  	  	 
	 });
//*******************************************************************************
$("#button_fecha_clear").on("click", function(e) {
    $('input[name="fecha_nacimiento_pases"]').val('');
    $('input[name="fecha_nacimiento_pases_club"]').val('');    
});

//**************************************************************
$( "#selector_provincia_pases_bono" ).change(function() {
	var provincia = $("#selector_provincia_pases_bono").val();
	new_abono_cargar_selector_poblacion(provincia);
})	
//**************************************************************
function new_abono_cargar_selector_poblacion(provincia)
{			
	$("#selector_poblacion_pases_bono").find("option").remove();
	
	$.ajax({
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/pases_club/select_list_municipios.do'/>?id="+provincia,
		timeout : 100000,
		data : null,
		dataType : 'json',
		success : function(data) {
			
			
			if (data.ArrayList.Municipio!=null && typeof data.ArrayList.Municipio!="undefined") {
				$.each(data.ArrayList.Municipio, function(i, item) {
				    $("#selector_poblacion_pases_bono").append('<option value="'+item.idmunicipio+'">'+item.nombre+'</option>');			    
				});
			 $('#selector_poblacion_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompletoNew.Municipio.idmunicipio+'"]').attr("selected", "selected");
			}
		}
	});
}

//***************************************************************
$(":input").inputmask();
//*********************************************
$("#tab_pases_club_foto").on("click", function(e) {
	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/venta/ventareserva/tomar_foto.do'/>", function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "xs");
	});     
});

///**************************************************************************************
$("#finalizar_button").on("click", function(e) { 
		$("#form_edit_abono").submit();			
	})
//***************************************************************************************
$("#form_edit_abono").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFormDataFinalizar();
		}
	}
);



//******************************************************************

function saveFormDataFinalizar()
{
	data = $("#form_edit_abono").serializeObject();
	showButtonSpinner("#finalizar_button");
    
	
	var parametros="<DatosClienteClubDTO>";
	parametros+="<identificadorpase>";
	parametros+="	<idpase>"+json_datos.DatosClienteClubDTOCompletoNew.IdentificadorPasesDto.idpase+"</idpase>";
	parametros+="	<esAbono>1</esAbono>";//Ojo que siempre es fijo
	
	parametros+="</identificadorpase>";
	parametros+="<idtipoidentificador>"+data.selector_identificador_pases_bono+"</idtipoidentificador>";
	parametros+="<nif>"+data.identificador_pases_club+"</nif>";
	parametros+="<nombre>"+data.nombre_new_pases_club+"</nombre>";
	parametros+="<apellido1>"+data.apellido1_new_pases_club+"</apellido1>";
	parametros+="<apellido2>"+ data.apellido2_new_pases_club+"</apellido2>";
	parametros+="<codSexo>"+data.selector_sexo_pases_bono+"</codSexo>";
	parametros+="<fechanacimiento>"+data.fecha_nacimiento_pases_club+"</fechanacimiento>";
	parametros+="<codTipoVia>"+data.selector_tipos_via_pases_bono+"</codTipoVia>";
	parametros+="<via>"+data.tipovia_new_pases_club+"</via>";
	//parametros+="<numvia>"+data.numvia_new_pases_club+"</numvia>";
	//parametros+="<complementovia>"+data.complementovia_new_pases_club+"</complementovia>";
	parametros+="<cp>"+data.cp_new_pases_club+"</cp>";
	var municipio = ""+data.selector_poblacion_pases_bono;
	if(municipio=="undefined")
		municipio = "0";
	parametros+="<idmunicipio>"+municipio+"</idmunicipio>";
	parametros+="<idprovincia>"+data.selector_provincia_pases_bono+"</idprovincia>";
	parametros+="<fijo>"+data.tfno_fijo_new_pases_club+"</fijo>";
	parametros+="<movil>"+data.tfno_movil_new_pases_club+"</movil>";
	parametros+="<email>"+data.email_new_pases_club+"</email>";
	var deseamovil = data.informacionmovil;
	if(deseamovil==undefined)
		deseamovil="0"
	else
		deseamovil="1"		
	
	var deseaemail = data.informacionemail;
	if(deseaemail==undefined)
		deseaemail="0"
	else
		deseaemail="1"	
	parametros+="<deseamovil>"+deseamovil+"</deseamovil>";
	parametros+="<deseaemail>"+deseaemail+"</deseaemail>";
	parametros+="<codParentesco>"+data.selector_rol_pases_bono+"</codParentesco>";
	parametros+="<idModelo>"+data.selector_modelo_pases_bono+"</idModelo>";
	parametros+="<nombrefoto>"+data.nombrefoto+"</nombrefoto>";
	parametros+="</DatosClienteClubDTO>";
	var nombre_foto = data.nombrefoto;
	console.log("-------------------saveFormDataFinalizar--1------------------------"+parametros);
	guardarFoto(nombre_foto);
	$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/venta/ventareserva/pases_club/guardar_pase.do'/>",
			timeout : 100000,
			data: { 
				xml:parametros
				}, 
			success : function(data) {
				var data = sanitizeArray(dt_listpasesclub.rows( { selected: true } ).data(),"grupo");
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.list.guardar.pase.alert.success" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				$("#modal-dialog-form-2").modal('hide');
				
				$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/pases_club/editar_grupo_abono.do'/>?id="+data[0], function() {
					
				});
			},
			error : function(exception) {
				hideSpinner("#finalizar_button");	
				
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",		     
					  delay: 5000,
					  buttons: { sticker: false }
				   });			
			}
		});

	
	
	
	
}

/** Lo anterior
 * function to_image(){
	    var canvas = document.getElementById("canvas");
	    document.getElementById("theimage").src = canvas.toDataURL("image/jpeg", 0.85);
	    Canvas2Image.saveAsJPEG(canvas);
	}
 */

//********************************************************
function guardarFoto(nombreFoto)
{
	var contenidoImagen = $(document).find('#myCanvas').get(0).toDataURL();
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/savePhoto.do'/>",
		timeout : 100000,
		data: {
			nombreFoto: nombreFoto,
			contenido:contenidoImagen
		}, 
		success : function(data) {

				
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: "No se pudo guardar la foto. "+exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });	
		}
	});
	
	
}
</script>
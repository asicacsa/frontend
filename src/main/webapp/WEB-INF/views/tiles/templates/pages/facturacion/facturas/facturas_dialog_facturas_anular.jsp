<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${motivo_anulacion}" var="motivo_anulacion_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	 <h4 class="modal-title">	
		<spring:message code="facturacion.facturas.tabs.facturas.dialog.anular_factura.title" />				
	</h4>	
</div>

<div class="modal-body">
		<form>
			<div class="form-group">
				<label class="control-label col-md-12 col-sm-12 col-xs-12"><spring:message code="facturacion.facturas.tabs.facturas.dialog.anular_factura.anular.texto" /></label>
			</div>	
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12 checkbox ">
					<input type="radio" name="tipo_anulacion" id="anulacion_facturas"  checked value="1" data-parsley-multiple="tipo_identificacion" class="flat" style="position: absolute; opacity: 0;"/>
					<spring:message code="facturacion.facturas.tabs.facturas.dialog.anular_factura.anular.facturas" />
				</div>													
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12 checkbox ">
					<input type="radio" name="tipo_anulacion" id="anulacion_facturas"  checked value="2" data-parsley-multiple="tipo_identificacion" class="flat" style="position: absolute; opacity: 0;"/>
					<spring:message code="facturacion.facturas.tabs.facturas.dialog.anular_factura.anular.facturasyvantas" />
				</div>													
			</div>		
			<div class="form-group">
				<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="facturacion.facturas.tabs.facturas.dialog.anular_factura.motivo.texto" />*</label>				
				<div class="col-md-6 col-sm-6 col-xs-6">
					<select name="idMotivoAnulacion" id="idMotivoAnulacion"  class="form-control" required="required">
						<option> </option>
						<x:forEach select="$motivo_anulacion_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select><br/>
				</div>
			</div>
			
			
					
				
		</form>
		<div class="modal-footer">
			<button id="anular_factura_button" type="button" class="btn btn-primary save_modificar_dialog">
				<spring:message code="common.button.save" />
			</button>
			<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
				<spring:message code="common.button.cancel" />
			</button>
		</div>
</div>
<script>

hideSpinner("#tab_gestion_factura_anular");

if ($("input.flat")[0]) {
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });
}


$("#anular_factura_button").on("click", function(e) {
	AnularFactura();
})

function AnularFactura()
{
	var data = sanitizeArray(dt_listgestionfacturas.rows( { selected: true } ).data(),"idfactura");
	var tipo_anulacion = $('input[name="tipo_anulacion"]').filter(':checked').val();
	var motivo = $("#idMotivoAnulacion").val();
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/factura/factura/factura_anular.do'/>",
		timeout : 100000,
		data: { data: data.toString(),tipo_anulacion:tipo_anulacion,motivo:motivo}, 
		success : function(data) {
			var xml = json2xml(data.ArrayList,''); 						
			showButtonSpinner("#anular_factura_button");
			var url = "<c:url value='/ajax/facturacion/facturas/generacionfacturas/show_listadofacturas.do'/>?"+encodeURI("facturas="+xml);
			$("#modal-dialog-form .modal-content").load(url, function() {
				$("#modal-dialog-form").modal('show');
				setModalDialogSize("#modal-dialog-form", "md");
			});
		},
		error : function(exception) {
			//dt_listcanales.processing(false);
			
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
	
}

</script>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="modal fade" id="modal-dialog-ventas-imprimir-entradas"
	role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close close_dialog" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">�</span>
				</button>
				<h4 class="modal-title">
					<spring:message code="venta.numerada_busquedas.dialog.imprimir_entradas.title" />			
				</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="control-label"> <input type="radio" class="flat" checked name="tipoimpresion" value="1"> 
						<spring:message code="venta.numerada_busquedas.dialog.reimprimir_sin_anular.option" />
					</label>
					<br/>
					 <label class="control-label"> <input type="radio" class="flat" name="tipoimpresion" value="2">
						<spring:message code="venta.numerada_busquedas.dialog.reimprimir_anulando.option" />
					</label>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success close_dialog" id="tab_ventas_dialog_imprimir_entradas"
						data-dismiss="modal">
						<spring:message code="common.button.ok" />
					</button>
					<button type="button" class="btn btn-default close_dialog"
						data-dismiss="modal">
						<spring:message code="common.button.cancel" />
					</button>
				</div>

			</div>
			
		</div>
	</div>
</div>

<!--  -->

<script>
//********************************************************************************
$("#tab_ventas_dialog_imprimir_entradas").on("click", function(e) { 

	if ($("input[name='tipoimpresion']:checked").val()=="1") {
		busquedasNumeradaVentasImprimirEntradas();
	}
	else {
		busquedasNumeradaVentasRegenerarImprimirEntradas();
	}
});	

</script>


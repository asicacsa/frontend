<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="venta.ventareserva.busqueda.tabs.entradas.modificar" />			
	</h4>	
</div>

<div class="modal-body">
	<form id="form_modificar_entrda" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input type="hidden" name="xml" id="strXml_entradas_modificar"/>
		<input type="hidden" name="idEntrada" id="idEntradaModificar"/>
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
				<label  class="control-label col-md-6 col-sm-6 col-xs-12"><spring:message code="venta.ventareserva.tabs.entradas.list.header.usos" /></label>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<input name="usos" id="entradas_usos" type="text" data-inputmask="'mask': '9{0,10}'" class="form-control" value="0">
				</div>
			</div>					
		</div>
			
	</form>
	<div class="modal-footer">
		<button id="modificar_usos_button" type="button" class="btn btn-primary">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>		

</div>

<script>

hideSpinner("#tab_entradas_modificar");
hideSpinner("#tab_entradas_venta_modificar_usos");
hideSpinner("#tab_entradas_reserva_modificar_usos");

//**************************************************************
$(":input").inputmask();
//***************************************************************
$("#modificar_usos_button").on("click", function(e) {
	var nusos= $("#entradas_usos").val();
	if(dtentradasVenta.rows( { selected: true } ).data().length<=0)
		$(".usos_impresiones").val(nusos)
	else
		$("tr[role='row'].selected .usos_impresiones").val(nusos)
		
	$("#modal-dialog-form-3").modal('hide');
});




</script>

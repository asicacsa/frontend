<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_tipo_cliente}" var="editar_tipo_cliente_xml" />
<x:parse xml="${editar_tipo_cliente_selector_unidades}" var="editar_tipo_cliente_selector_unidades_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<x:choose>
			<x:when select="not($editar_tipo_cliente_xml/Tipocliente)">
				<spring:message code="facturacion.facturas.tabs.clientes.dialog.crear_tipo_cliente.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="facturacion.facturas.tabs.clientes.dialog.editar_tipo_cliente.title" />
			</x:otherwise>
		</x:choose>		
	</h4>	
</div>

<div class="modal-body">
	<form id="form_tipo_cliente_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input id="idtipocliente" name="idtipocliente" type="hidden" value="<x:out select = "$editar_tipo_cliente_xml/Tipocliente/idtipocliente" />" />
		<input id="comisiones_asignados" name="comisiones_asignados" type="hidden"  />
		<input id="descuentos_asignados" name="descuentos_asignados" type="hidden"  />	
		<div class="col-md-5 col-sm-5 col-xs-12">
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.tipo_clientes.field.nombre" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editar_tipo_cliente_xml/Tipocliente/nombre" />">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.tipo_clientes.field.unidades" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="idunidadnegocio" id="selector_unidad_negocio" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$editar_tipo_cliente_selector_unidades_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>	
			<div class="color_form_edit">
				<fieldset id="tipo_contrato">	
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.tipo_clientes.field.tipo_contrato" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
								<div class="checkbox col-md-6 col-sm-6 col-xs-6">
									<input type="radio" name="tipo_contrato" id="credito" checked value="1" data-parsley-multiple="tipo_contrato" class="flat" style="position: absolute; opacity: 0;">
									<strong><spring:message code="facturacion.facturas.tabs.tipo_clientes.field.credito" /></strong>
								</div>
								<div class="checkbox col-md-6 col-sm-6 col-xs-6">
									<input type="radio" name="tipo_contrato" id="prepago" value="0" data-parsley-multiple="tipo_contrato" class="flat" style="position: absolute; opacity: 0;">
									<strong><spring:message code="facturacion.facturas.tabs.tipo_clientes.field.prepago" /></strong>
								</div>				
						</div>
					</div>	
				</fieldset>	
			</div>
		</div>
		<div class="col-md-7 col-sm-7 col-xs-12">

			<div class="form-group" id="div_comisiones">
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.tipo_clientes.field.comisiones" /></label>
				<div class="col-md-10 col-sm-10 col-xs-10">			
					<div class="btn-group pull-right btn-datatable">
						<a type="button" class="btn btn-info" id="tab_comisiones_new">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.comisiones.list.button.nuevo" />"> <span class="fa fa-plus"></span>
							</span>
						</a>
						<a type="button" class="btn btn-info" id="tab_comisiones_edit">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.comisiones.list.button.editar" />"> <span class="fa fa-pencil"></span>
							</span>
						</a>
						<a type="button" class="btn btn-info" id="tab_comisiones_remove">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.comisiones.list.button.eliminar" />"> <span class="fa fa-trash"></span>
							</span>
						</a>		
					</div>			
			    </div>
			
					<table id="comisiones_table" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
							  <th id="id"></th>
							  <th id="fechainiciovigencia"><spring:message code="facturacion.facturas.tabs.tipo_clientes.field.inicio"/></th>
							  <th id="fechafinvigencia"><spring:message code="facturacion.facturas.tabs.tipo_clientes.field.fin" /></th>
							  <th id="porcentajedescuento"><spring:message code="facturacion.facturas.tabs.tipo_clientes.field.descuento" /></th>							 
							</tr>
						</thead>
						<tbody>				
							<x:forEach select="$editar_tipo_cliente_xml/Tipocliente/comisions/Comision" var="item">
							   <tr class="seleccionable">
							   		<td id="id" ><x:out select="$item/idcomision" /></td>
									<td id="fechainiciovigencia">
										<c:set var="fechainicionovigencia"><x:out select="$item/fechainiciovigencia"/></c:set>
										<fmt:parseDate value="${fechainicionovigencia}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadainicionovigencia"/>
										<fmt:formatDate value="${fechaformateadainicionovigencia}" pattern="dd/MM/yyyy" type="DATE"/>									
									</td>
									<td id="fechafinvigencia">
										<c:set var="fechafinvigencia"><x:out select="$item/fechafinvigencia"/></c:set>
										<fmt:parseDate value="${fechafinvigencia}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadafinvigencia"/>
										<fmt:formatDate value="${fechaformateadafinvigencia}" pattern="dd/MM/yyyy" type="DATE"/>									
									</td>
									<td id="porcentajecomision" ><x:out select="$item/porcentajecomision" /></td>																		
								</tr>																
							</x:forEach>
						</tbody>				
					</table>				
			</div>

			<div class="form-group" id="div_descuentos">
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.tipo_clientes.field.descuentos" /></label>
				<div class="col-md-10 col-sm-10 col-xs-10">			
					<div class="btn-group pull-right btn-datatable">
						<a type="button" class="btn btn-info" id="tab_descuentos_new">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.descuentos.list.button.nuevo" />"> <span class="fa fa-plus"></span>
							</span>
						</a>
						<a type="button" class="btn btn-info" id="tab_descuentos_edit">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.descuentos.list.button.editar" />"> <span class="fa fa-pencil"></span>
							</span>
						</a>
						<a type="button" class="btn btn-info" id="tab_descuentos_remove">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.descuentos.list.button.eliminar" />"> <span class="fa fa-trash"></span>
							</span>
						</a>		
					</div>			
			    </div>
			
					<table id="descuentos_table" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
							  <th id="id"></th>
							  <th id="fechainiciovigencia"><spring:message code="facturacion.facturas.tabs.tipo_clientes.field.inicio"/></th>
							  <th id="fechafinvigencia"><spring:message code="facturacion.facturas.tabs.tipo_clientes.field.fin" /></th>
							  <th id="porcentajedescuento"><spring:message code="facturacion.facturas.tabs.tipo_clientes.field.descuento" /></th>							 
							</tr>
						</thead>
						<tbody>				
							<x:forEach select="$editar_tipo_cliente_xml/Tipocliente/descuentos/Descuento" var="item">
							   <tr class="seleccionable">
							   		<td id="id" ><x:out select="$item/iddescuento" /></td>
									<td id="fechainiciovigencia">
										<c:set var="fechainicionovigencia"><x:out select="$item/fechainiciovigencia"/></c:set>
										<fmt:parseDate value="${fechainicionovigencia}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadainicionovigencia"/>
										<fmt:formatDate value="${fechaformateadainicionovigencia}" pattern="dd/MM/yyyy" type="DATE"/>									
									</td>
									<td id="fechafinvigencia">
										<c:set var="fechafinvigencia"><x:out select="$item/fechafinvigencia"/></c:set>
										<fmt:parseDate value="${fechafinvigencia}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadafinvigencia"/>
										<fmt:formatDate value="${fechaformateadafinvigencia}" pattern="dd/MM/yyyy" type="DATE"/>									
									</td>
									<td id="porcentajedescuento" ><x:out select="$item/porcentajedescuento" /></td>																		
								</tr>																
							</x:forEach>
						</tbody>				
					</table>
			</div>	
		</div>
	</form>
	<div class="modal-footer">

		<button id="save_canal_button" type="button" class="btn btn-primary save_tipo_cliente_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>		

</div>

<script>
    
    
	var dtdescuentos,dtcomisiones;
    
	$(document).ready(function() {
	
		
		hideSpinner("#tab_tiposcliente_new");
		hideSpinner("#tab_tiposcliente_edit");
		
		dtdescuentos=$('#descuentos_table').DataTable( {
			"paging": false,
			"info": false,
			"searching": false,
			select: { style: 'os'},
			language: dataTableLanguage,
			 "columnDefs": [
			                { "visible": false, "targets": [0]}
			              ]
			});
		
		dtcomisiones=$('#comisiones_table').DataTable( {
			"paging": false,
			"info": false,
			"searching": false,
			select: { style: 'os'},
			language: dataTableLanguage,
			 "columnDefs": [
			                { "visible": false, "targets": [0]}
			              ]
			});

		
			
		$("#tab_comisiones_new").on("click", function(e) {
			$('.titulo_atributos').text('<spring:message code="facturacion.facturas.tabs.tipo_clientes.field.comision.crear.texto" />');
			limpiarModaltPorcentajes(dtcomisiones)
		});
		
		
		$("#tab_comisiones_edit").on("click", function(e) {
			
			if(dtcomisiones.rows( { selected: true } ).data().length==1)		
		    {
				$('.titulo_atributos').text('<spring:message code="facturacion.facturas.tabs.tipo_clientes.field.comision.editar.texto" />');
				editarModalPorcentajes(dtcomisiones)
		    }
		    else
		    	{
		    	if(dtcomisiones.rows( { selected: true } ).data().length==0)
			    	{
					new PNotify({
					      title: '<spring:message code="common.dialog.text.error" />',
					      text: '<spring:message code="facturacion.facturas.tabs.tipo_clientes.field.comision.alert.ninguna_seleccion" />',
						  type: "error",
						  delay: 5000,
						  buttons: { sticker: false }
					   });
			    	}
			    	
				else
					{
					new PNotify({
					      title: '<spring:message code="common.dialog.text.error" />',
					      text: '<spring:message code="facturacion.facturas.tabs.tipo_clientes.field.comision.alert.seleccion_multiple" />',
						  type: "error",
						  delay: 5000,
						  buttons: { sticker: false }
					   });
					}
				}		
			
			

		});	
		
		$("#tab_comisiones_remove").on("click", function(e) { 
			new PNotify({
			      title: '<spring:message code="common.dialog.text.atencion" />',
			      text: '<spring:message code="facturacion.facturas.tabs.tipo_clientes.field.comision.eliminar" />',
				  hide: false,
				  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
				  buttons: { closer: false, sticker: false	},
				  history: { history: false	}
			   }).get().on('pnotify.confirm', function() {
				   dtcomisiones
				    .rows( '.selected' )
				    .remove()
				    .draw();
			   })			
			})		
		
		$("#tab_descuentos_new").on("click", function(e) {
			$('.titulo_atributos').text('<spring:message code="facturacion.facturas.tabs.tipo_clientes.field.descuento.crear.texto" />');
			limpiarModaltPorcentajes(dtdescuentos)			
		});
		
		$("#tab_descuentos_edit").on("click", function(e) {

			if(dtdescuentos.rows( { selected: true } ).data().length==1)		
		    {
				$('.titulo_atributos').text('<spring:message code="facturacion.facturas.tabs.tipo_clientes.field.descuento.editar.texto" />');
				editarModalPorcentajes(dtdescuentos)
		    }
		    else
		    	{
		    	if(dtdescuentos.rows( { selected: true } ).data().length==0)
			    	{
					new PNotify({
					      title: '<spring:message code="common.dialog.text.error" />',
					      text: '<spring:message code="facturacion.facturas.tabs.tipo_clientes.field.descuento.alert.ninguna_seleccion" />',
						  type: "error",
						  delay: 5000,
						  buttons: { sticker: false }
					   });
			    	}
			    	
				else
					{
					new PNotify({
					      title: '<spring:message code="common.dialog.text.error" />',
					      text: '<spring:message code="facturacion.facturas.tabs.tipo_clientes.field.descuento.alert.seleccion_multiple" />',
						  type: "error",
						  delay: 5000,
						  buttons: { sticker: false }
					   });
					}
				}							
		});
		
		$("#tab_descuentos_remove").on("click", function(e) { 
			new PNotify({
			      title: '<spring:message code="common.dialog.text.atencion" />',
			      text: '<spring:message code="facturacion.facturas.tabs.tipo_clientes.field.descuento.eliminar" />',
				  hide: false,
				  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
				  buttons: { closer: false, sticker: false	},
				  history: { history: false	}
			   }).get().on('pnotify.confirm', function() {
				   dtdescuentos
				    .rows( '.selected' )
				    .remove()
				    .draw();
			   })			
			})

		
		$('#selector_unidad_negocio option[value="<x:out select = "$editar_tipo_cliente_xml/Tipocliente/unidadnegocio/idunidadnegocio" />"]').attr("selected", "selected");
	 
		if('<x:out select = "$editar_tipo_cliente_xml/Tipocliente/condicionpagoultima/credito" />'=="0")
		   $("#prepago").prop("checked", true);
		
		if ($("input.flat")[0]) {
	        $(document).ready(function () {
	            $('input.flat').iCheck({
	                checkboxClass: 'icheckbox_flat-green',
	                radioClass: 'iradio_flat-green'
	            });
	        });
	    }
	})
	
	$(".save_tipo_cliente_dialog").on("click", function(e) {	
		$("#form_tipo_cliente_data").submit();	
	});

	$("#form_tipo_cliente_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
	        $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			guardarTipoCliente()
		}
	})
	

		
	
	function guardarTipoCliente()
	{
		     
		   // Se genera el xml de comisiones de cupo
	       var asignaciones_comisiones= dtcomisiones.rows().data();
	       var comisiones_xml= "<comisions>";
	       
	       asignaciones_comisiones.each(function (value, index) {
	    	   comisiones_xml+= "<Comision>";
	    	   comisiones_xml+= "<idcomision>"+dtcomisiones.rows(index).data()[0][0]+"</idcomision>";
	    	   comisiones_xml+= "<fechainiciovigencia>"+dtcomisiones.rows(index).data()[0][1]+"</fechainiciovigencia>";
	    	   comisiones_xml+= "<fechafinvigencia>"+dtcomisiones.rows(index).data()[0][2]+"</fechafinvigencia>"
	    	   comisiones_xml+= "<porcentajecomision>"+dtcomisiones.rows(index).data()[0][3]+"</porcentajecomision>"
	    	   comisiones_xml+= "</Comision>";
	       });
	       comisiones_xml+= "</comisions>";
	       $("#comisiones_asignados").val(comisiones_xml);
	       
	       // Se genera el xml de dtdescuentos de cupo
	       var asignaciones_descuentos= dtdescuentos.rows().data();
	       var descuentos_xml= "<descuentos>";
	       asignaciones_descuentos.each(function (value, index) {
	    	   descuentos_xml+= "<Descuento>";
	    	   descuentos_xml+= "<iddescuento>"+dtdescuentos.rows(index).data()[0][0]+"</iddescuento>";
	    	   descuentos_xml+= "<fechainiciovigencia>"+dtdescuentos.rows(index).data()[0][1]+"</fechainiciovigencia>";
	    	   descuentos_xml+= "<fechafinvigencia>"+dtdescuentos.rows(index).data()[0][2]+"</fechafinvigencia>"
	    	   descuentos_xml+= "<porcentajedescuento>"+dtdescuentos.rows(index).data()[0][3]+"</porcentajedescuento>"
	    	   descuentos_xml+= "</Descuento>";
	       });
	       descuentos_xml+= "</descuentos>";
	       $("#descuentos_asignados").val(descuentos_xml);

	       var data = $("#form_tipo_cliente_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/facturacion/facturas/clientes/save_tipo_cliente.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				
				$("#modal-dialog-form").modal('hide');
				
				dt_listtiposcliente.ajax.reload(null,false);
						
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
		
		
	}

	


</script>
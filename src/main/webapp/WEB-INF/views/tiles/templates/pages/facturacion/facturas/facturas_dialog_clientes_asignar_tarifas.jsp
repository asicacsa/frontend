<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<x:parse xml="${asignarTarifas_productos}" var="asignarTarifas_productos_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="facturacion.facturas.tabs.clientes.dialog.asignarTarifa.title" />
	</h4>	
</div>

<div class="modal-body">
		<form id="form_cliente_asignar_tarifas" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
				<input type="hidden" id="idcliente" name="idcliente" value="${idcliente}"/>
				<input type="hidden" id="xmlEnvio" name="xmlEnvio" value="${idcliente}"/>
				<div class="form-group">
					<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.asignar_tarifa.field.producto" />*</label>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<select name="idproducto" id="idproducto"  class="form-control" required="required">
							<option> </option>
							<x:forEach select="$asignarTarifas_productos_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
						</select>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
						
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="btn-group pull-right btn-datatable">
							<a type="button" class="btn btn-info" id="tab_tarifas_productos_new">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.productos.tarifas.button.nuevo" />"> <span class="fa fa-plus"></span>
								</span>
							</a>
							<a type="button" class="btn btn-info" id="tab_tarifas_productos_remove">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.productos.tarifas.button.eliminar" />"> <span class="fa fa-trash"></span>
								</span>
							</a>		
						</div>							
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<table id="table-listaProductostarifas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
						<thead>
								<tr>	
									<th class="idtarifaproducto"></th>
									<th class="id_tarifa"></th>
									<th class="id_producto"></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.nombretarifa" /></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.fechainicio" /></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.fechafin" /></th>				
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.importe" /></th>							
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>	
				
				
				
		</form>
	<div class="modal-footer">
	
		<button id="save_cliente_button" type="button" class="btn btn-primary close_asignar_tarifas">
			<spring:message code="common.button.accept" />
		</button>	
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>	
	
</div>




<script>
	hideSpinner("#tab_clientes_tarifas");
	
	$( ".close_asignar_tarifas" ).on("click", function(e) {	
				$("#modal-dialog-form").modal('hide');
	})

	$(".select-select2").select2({
		language : "es",
		containerCssClass : "single-line"
	});	
	
	$("#idproducto").change(function() {
		var idCliente = "${idcliente}";
		var idProducto = $("#idproducto").val();
		dtproductostarifas.ajax.reload(); 
	})
	
	$( "#tab_tarifas_productos_new" ).on("click", function(e) {	
		var data = sanitizeArray(dtproductostarifas.rows().data(),"tarifa.idtarifa");
		
		$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/facturacion/facturas/clientes/pantalla_creacion_tarifas.do'/>?data="+data, function() {
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "sm");
		});
	})
	
	$( "#tab_tarifas_productos_remove" ).on("click", function(e) {	
		dtproductostarifas
	    .rows( '.selected' )
	    .remove()
	    .draw();
		 var xml = "<parametro><list>"+tablatarifasXML();
		 xml+="</list><idcliente>"+$('#idcliente').val()+"</idcliente><idproducto>"+$('#idproducto').val()+"</idproducto></parametro>";
		
		 $("#xmlEnvio").val(xml) ;
		 acturlizarTarifasBd()
		 
	})	


	function acturlizarTarifasBd()
	{
		var data = $("#form_cliente_asignar_tarifas").serializeObject();
		 showSpinner("#modal-dialog-form .modal-content");

		 
		 $.ajax({
				type : "post",
				url : "<c:url value='/ajax/facturacion/facturas/clientes/asignar_tarifas.do'/>",
				timeout : 100000,
				data : data,
				success : function(data) {
					hideSpinner("#modal-dialog-form .modal-content");
					
					dtproductostarifas.ajax.reload(null,false);
							
					new PNotify({
						title : '<spring:message code="common.dialog.text.operacion_realizada" />',
						text : '<spring:message code="common.dialog.text.datos_guardados" />',
						type : "success",
						delay : 5000,
						buttons : {
							closer : true,
							sticker : false
						}
					});
					
					
				},
				error : function(exception) {
					hideSpinner("#modal-dialog-form .modal-content");

					new PNotify({
						title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						text : exception.responseText,
						type : "error",
						delay : 5000,
						buttons : {
							closer : true,
							sticker : false
						}
					});
				}
			});
	
	}
	
	
	
	
	var dtproductostarifas=$('#table-listaProductostarifas').DataTable( {	
	    ajax: {
	        url: "<c:url value='/ajax/facturacion/facturas/clientes/list_tarifas_productos.do'/>",
	        rowId: 'idtarifaproducto',
	        type: 'POST',
	        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Tarifaproducto)); return(""); },
	        data: function (params) {
	          	return ($("#form_cliente_asignar_tarifas").serializeObject());
	       	},	
	        error: function (xhr, error, thrown) {
	            if (xhr.responseText=="403") {
	                  $("#table-listaProductostarifas").hide();
	            }                   
	     },
	        
	    },
	    initComplete: function( settings, json ) {
	        $('a#menu_toggle').on("click", function () {dt_listgrupos.columns.adjust().draw(); });
		},
	    columns: [
				  { data: "idtarifaproducto", type: "spanish-string" ,  defaultContent:""} ,
				  { data: "tarifa.idtarifa", type: "spanish-string" ,  defaultContent:""} ,
				  { data: "producto.idproducto", type: "spanish-string" ,  defaultContent:""} ,
	              { data: "tarifa.nombre", type: "spanish-string" ,  defaultContent:""} ,
	              { data: "fechainiciovigencia", type: "spanish-string" ,  defaultContent:"",
	         		 render: function ( data, type, row, meta ) {			 
	             	  	  return data.substr(0, 10);}	 
	         	 } ,
	         	 { data: "fechafinvigencia", type: "spanish-string" ,  defaultContent:"",
	         		 render: function ( data, type, row, meta ) {			 
	            	  	  return data.substr(0, 10);} 
	         	 
	         	}, 
	         	{ data: "importe", type: "spanish-string" ,  defaultContent:""} ,	              
	    ],    
	    drawCallback: function( settings ) {
	    	 activateTooltipsInTable('datatable-lista-grupos')
	    },
	    select: { style: 'os'},
		language: dataTableLanguage,
		processing: true,
		"columnDefs": [
		                { "visible": false, "targets": [0,1]}
		              ]
	} );
	
	
	
	
	
	function tablatarifasXML()
	{
	 var xmlTarifas="";
     dtproductostarifas.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
    	  var data = this.data();
    	  xmlTarifas+="<Tarifaproducto>";
    	  xmlTarifas+="<idtarifaproducto>"+data.idtarifaproducto+"</idtarifaproducto>";
    	  xmlTarifas+="<tarifa><idtarifa>"+data.tarifa.idtarifa+"</idtarifa></tarifa>";
    	  xmlTarifas+="<producto><idproducto>"+data.producto.idproducto+"</idproducto></producto>";
    	  xmlTarifas+="<importe>"+data.importe+"</importe>";
    	  xmlTarifas+="<fechainiciovigencia>"+data.fechainiciovigencia+"</fechainiciovigencia><fechafinvigencia>"+data.fechafinvigencia+"</fechafinvigencia>";
    	  xmlTarifas+="</Tarifaproducto>";
      })
     
     return xmlTarifas;
	}
	
	
	
</script>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editarrestriccion_datos_restriccion}" var="editarrestriccion_datos_restriccion_xml" />
<x:parse xml="${editarrestriccion_selector_redorigen}" var="editarrestriccion_selector_redorigen_xml" />
<x:parse xml="${editarrestriccion_selector_reddestino}" var="editarrestriccion_selector_reddestino_xml" />
<x:parse xml="${editarrestriccion_selector_funcionalidades}" var="editarrestriccion_selector_funcionalidades_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<x:choose>
			<x:when select="not($editarrestriccion_datos_restriccion_xml/Restriccionredes)">
				<spring:message code="administracion.seguridad.tabs.restricciones.dialog.crear_restriccion.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.seguridad.tabs.restricciones.dialog.editar_restriccion.title" />
			</x:otherwise>
		</x:choose>
	</h4>
</div>
<div class="modal-body" id="principal">

	<form id="form_restriccion_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

		<input id="idrestriccion" name="idrestriccion" type="hidden" value="<x:out select = "$editarrestriccion_datos_restriccion_xml/Restriccionredes/idrestriccionredes" />" />

		<div class="col-md-12 col-sm-12 col-xs-12">

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.restricciones.field.nombre" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editarrestriccion_datos_restriccion_xml/Restriccionredes/nombre" />">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.restricciones.field.redorigen" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					  <select name="idredorigen" id="selector_redorigen" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$editarrestriccion_selector_redorigen_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.restricciones.field.reddestino" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					  <select name="idreddestino" id="selector_reddestino" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$editarrestriccion_selector_reddestino_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>

		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.restricciones.field.funcionalidades" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="funcionalidades[]" id="selector_funcionalidades" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
					</select>
				</div>
			</div>
		
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
		
				<button id="save_restriccion_button" type="submit" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
	</form>

</div>


<script>
	hideSpinner("#tab_restricciones_new");
	hideSpinner("#tab_restricciones_edit");

	$(".select-select2").select2({
		language : "es",
		containerCssClass : "single-line"
	});

	cargar_funcionalidades();

	$('#selector_redorigen option[value="<x:out select = "$editarrestriccion_datos_restriccion_xml/Restriccionredes/redorigen/idred" />"]').attr("selected", "selected");
	$('#selector_reddestino option[value="<x:out select = "$editarrestriccion_datos_restriccion_xml/Restriccionredes/reddestino/idred" />"]').attr("selected", "selected");
	
	$("#form_restriccion_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFormData();
		}
	});
	
	//********************************************************************************
	function cargar_funcionalidades()
	{
		
		var data = [		            
					<x:forEach select="$editarrestriccion_selector_funcionalidades_xml/ArrayList/Clasiffuncionalidades" var="nivel1" varStatus="status1">
						{id:"<x:out select="$nivel1/idclasiffuncionalidades" />",text:"<x:out select="$nivel1/nombre" />",				
							inc:[
									<x:forEach select="$nivel1/Clasiffuncionalidades" var="nivel2"  varStatus="status2">
										{id:"<x:out select="$nivel2/idclasiffuncionalidades" />",text:"<x:out select="$nivel2/nombre" />",
											<x:if select="$nivel2/funcionalidads">					
												inc:[
													<x:forEach select="$nivel2/funcionalidads/Funcionalidad" var="func">
														{id:"<x:out select="$func/idfuncionalidad" />",text:"<x:out select="$func/nombre" />"},									     
													</x:forEach>
												]	
											</x:if>								
										},
									</x:forEach>
										
									<x:forEach select="$nivel1/funcionalidads/Funcionalidad" var="func">
										{id:"<x:out select="$func/idfuncionalidad" />",text:"<x:out select="$func/nombre" />"},									     
									</x:forEach>									
						     ]
						},				            	            				
					</x:forEach>	            	            
			   		];
		
		$("#selector_funcionalidades").select2ToTree({treeData: {dataArr: data}, containerCssClass : "single-line"});
		$("#selector_funcionalidades .non-leaf").attr("disabled","disabled");
		
		var selectedValuesFuncionalidades = new Array();
		<x:forEach select="$editarrestriccion_datos_restriccion_xml/Restriccionredes/restrredesfuncs/Restrredesfunc/funcionalidad" var="item">
		selectedValuesFuncionalidades.push('<x:out select="$item/idfuncionalidad" />');
		</x:forEach>

		$('#selector_funcionalidades').val(selectedValuesFuncionalidades);
		$('#selector_funcionalidades').trigger('change.select2');	
	}
		
	//********************************************************************************	
	function saveFormData() {
		var data = $("#form_restriccion_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/seguridad/seguridad/save_restriccion.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				dt_listrestricciones.ajax.reload(null,false);			
				$("#modal-dialog-form").modal('hide');
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
	}
</script>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="administracion.programacion.programacion.dialog.aforo.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_establecer_aforo" class="form-horizontal form-label-left">
	
		<div class="form-group">
	
			<div id="tabla-aforo" class="col-md-12 col-sm-12 col-xs-12">
			
				<div class="col-md-12 col-sm-12 col-xs-12">
					<table id="datatable-list-aforo" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th><spring:message code="administracion.programacion.programacion.dialog.aforo.list.header.idzona" /></th>
								<th><spring:message code="administracion.programacion.programacion.dialog.aforo.list.header.zona" /></th>
								<th><spring:message code="administracion.programacion.programacion.dialog.aforo.list.header.libres" /></th>
								<th><spring:message code="administracion.programacion.programacion.dialog.aforo.list.header.bloqueadas" /></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					<span>&nbsp;</span>
				</div>
			</div>
		</div>
		
	</form>

	<div class="modal-footer">
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.close" />
		</button>
	</div>
	
</div>

<script>
hideSpinner("#button_bloqueos_info");
showSpinner('#tabla-aforo');

var dt_listaforo=$('#datatable-list-aforo').DataTable( {
	language: dataTableLanguage,
	processing: false,
	scrollY: "200px",
	scrollCollapse: true,
	paging: false,
	info: false,
	searching: false,
    select: { style: 'os'},
    ajax: {
        url: "<c:url value='/ajax/admon/programacion/list_aforo.do'/>",
        rowId: 'idfranjahoraria',
        type: 'POST',
		data: {
	    	   id: "${idSesion}"
	  	},
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.Altasesionparam.sesionPlantilla.zonasesions.Zonasesion)); return(""); },
        error: function (xhr, error, code)
        {
        	$("#datatable-list-aforo_processing").hide();
           	new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : xhr.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}					
			});
        }
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listaforo.data().count()>0) dt_listaforo.columns.adjust().draw(); });
        hideSpinner('#tabla-aforo');
	},
	columnDefs: [
        { "targets": 0, "visible": false },
    ],
    columns: [
        { data: "zona.idzona", type: "spanish-string", defaultContent: "" },
        { data: "zona.nombre", type: "spanish-string", defaultContent: "" },
        { data: "numlibres", className: "cell_centered", type: "spanish-string", defaultContent: "" },
        { data: "numbloqueadas", className: "cell_centered", type: "spanish-string", defaultContent: "" }
    ],    
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('datatable-list-aforo');
   	}
	
} );

function ajustar_cabeceras_datatable()
{
	$('#datatable-list-aforo').DataTable().columns.adjust().draw();
}

setTimeout(ajustar_cabeceras_datatable, 200);

insertSmallSpinner("#datatable-list-aforo_processing");

</script>


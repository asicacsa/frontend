<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_unidades_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.unidades.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_unidades_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.unidades.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_unidades_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.unidades.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>		
	</div>
	
	<table id="datatable-lista-unidades" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="administracion.recintos.tabs.unidades.list.header.nombre" /></th>
			  	<th><spring:message code="administracion.recintos.tabs.unidades.list.header.recintos" /></th>
				<th><spring:message code="administracion.recintos.tabs.unidades.list.header.redes" /></th>
				<th><spring:message code="administracion.recintos.tabs.unidades.list.header.usuarios" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>

var dt_listunidades=$('#datatable-lista-unidades').DataTable( {
    ajax: {
        url: "<c:url value='/ajax/admon/recintos/unidades/list_unidades.do'/>",
        rowId: 'idunidadnegocio',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Unidadnegocio)); return(""); },
        error: function (xhr, error, thrown) {
            if (xhr.responseText=="403") {
                  $("#tab_unidades_negocio").hide();
            }  
        },
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listunidades.data().count()>0) dt_listunidades.columns.adjust().draw(); });
	},
    columns: [
        { data: "nombre", type: "spanish-string" ,  defaultContent:""} ,
        { data: "recintounidadnegocios.Recintounidadnegocio", className: "cell_centered",
        	  render: function ( data, type, row, meta ) { return showListInCell(data,"<spring:message code="administracion.recintos.tabs.unidades.list.header.recintos" />","recinto.nombre"); }	
              },
        { data: "redunidadnegocios.Redunidadnegocio", className: "cell_centered",
      	  render: function ( data, type, row, meta ) { return showListInCell(data,"<spring:message code="administracion.recintos.tabs.unidades.list.header.redes" />","red.nombre"); }	
            },
        { data: "usuariounidadnegocios.Usuariounidadnegocio", className: "cell_centered",
        	  render: function ( data, type, row, meta ) { return showListInCell(data,"<spring:message code="administracion.recintos.tabs.unidades.list.header.usuarios" />","usuario.login"); }	
              }
       
        
    ],    
    drawCallback: function( settings ) {
    	activateTooltipsInTable('datatable-lista-unidades')
    },
    select: { style: 'os', selector:'td:not(:nth-child(2),:nth-child(3),:nth-child(4))'},
	language: dataTableLanguage,
	processing: true,
} );

insertSmallSpinner("#datatable-lista-unidades_processing");
//********************************************************************
$("#tab_unidades_new").on("click", function(e) { 
	showButtonSpinner("#tab_unidades_new");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/recintos/unidades/show_unidades.do'/>", function() {										  
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
})
//********************************************************************
$("#tab_unidades_edit").on("click", function(e) { 
	showButtonSpinner("#tab_unidades_edit");
	var data = sanitizeArray(dt_listunidades.rows( { selected: true } ).data(),"idunidadnegocio");
		
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.recintos.tabs.unidades.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		hideSpinner("#tab_unidades_edit");
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.recintos.tabs.unidades.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		hideSpinner("#tab_unidades_edit");		
		return;
	}	
		
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/recintos/unidades/show_unidades.do'/>?id="+data[0], function() {
		setModalDialogSize("#modal-dialog-form", "md");
		$("#modal-dialog-form").modal('show');
	});
})
//********************************************************************************
$("#tab_unidades_remove").on("click", function(e) { 
	showButtonSpinner("#tab_unidades_remove");	
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="administracion.recintos.tabs.unidades.list.confirm.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
  		  buttons: { closer: false, sticker: false	},
  		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
	
		   dt_listubicaciones.processing(true);
			
			var data = sanitizeArray(dt_listunidades.rows( { selected: true } ).data(),"idunidadnegocio");
		   
			$.ajax({
				contenttype: "application/json; charset=utf-8",
				type : "post",
				url : "<c:url value='/ajax/admon/recintos/unidades/remove_unidades.do'/>",
				timeout : 100000,
				data: { data: data.toString() }, 
				success : function(data) {
					dt_listunidades.ajax.reload();					
				},
				error : function(exception) {
					dt_listunidades.processing(false);
					
					new PNotify({
					      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					      text: exception.responseText,
						  type: "error",		     
						  delay: 5000,
						  buttons: { sticker: false }
					   });			
				}
			});

	   }).on('pnotify.cancel', function() {
	   });		 
	hideSpinner("#tab_unidades_remove");
}); 
</script>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>



<x:parse xml="${editar_canal}" var="editar_canal_xml" />
<x:parse xml="${editar_canal_selector_unidad}" var="editar_canal_selector_unidad_xml" />
<x:parse xml="${editar_canal_selector_tipo_canal}" var="editar_canal_selector_tipo_canal_xml" />

<x:parse xml="${editar_canal_selector_operacion}" var="editar_canal_selector_operacion_xml" />
<x:parse xml="${editar_canal_selector_tarifas}" var="editar_canal_selector_tarifas_xml" />
<x:parse xml="${editar_canal_selector_formasPago}" var="editar_canal_selector_formasPago_xml" />
<x:parse xml="${editar_canal_selector_productos}" var="editar_canal_selector_productos_xml" />
<x:parse xml="${editar_canal_selector_descuentosPromo}" var="editar_canal_selector_descuentosPromo_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<x:choose>
			<x:when select="not($editar_canal_xml/Canal)">
				<spring:message code="administracion.productos.tabs.canales.dialog.crear_canal.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.productos.tabs.canales.dialog.editar_canal.title" />
			</x:otherwise>
		</x:choose>		
	</h4>	
</div>

<div class="modal-body">
	<form id="form_canal_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input id="idcanal" name="idcanal" type="hidden" value="<x:out select = "$editar_canal_xml/Canal/idcanal" />" />
		<input id="idcliente" name="idcliente" type="hidden" value="<x:out select = "$editar_canal_xml/Canal/clientes/Cliente/idcliente" />" />
		<div class="col-md-6 col-sm-6 col-xs-12">
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.canales.field.nombre" />*</label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editar_canal_xml/Canal/nombre" />">
				</div>
			</div>
		
			
			<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.canales.field.descripcion" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">					
						<textarea name="descripcion" id="descripcion" class="form-control"  ><x:out select = "$editar_canal_xml/Canal/descripcion" /></textarea>
					</div>
			</div>
			
		   <div class="form-group button-dialog">
	           <label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.descuentospromo.field.cliente" /></label>
	           	<div class="col-md-8 col-sm-8 col-xs-8">
                	<input name="cliente" id="cliente" type="text" class="form-control" readonly value="<x:out select = "$editar_canal_xml/Canal/clientes/Cliente/nombre" />">
	             </div>
            </div>
			
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.canales.field.orden" /></label>
				<div class="col-md-3 col-sm-3 col-xs-3">					
					<input name="orden" id="orden" class="form-control" data-inputmask="'mask': '9{0,10}'" value="<x:out select = "$editar_canal_xml/Canal/orden" />"/>
				</div>
				<div class="col-md-5 col-sm-5 col-xs-5"></div>
			</div>	
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.canales.field.unidad" />*</label>
				<div class="col-md-8 col-sm-8 col-xs-6">
					<select name="idunidadnegocio" id="selector_unidad" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$editar_canal_selector_unidad_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
		
		
			<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.canales.field.tipo.canal" />*</label>
					<div class="col-md-8 col-sm-8 col-xs-6">
						<select name="idtipocanal" id="selector_tipo_canal" class="form-control" required="required">
							<option value=""></option>
							<x:forEach select="$editar_canal_selector_tipo_canal_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
						</select>
					</div>
			</div>
			
			<div class="form-group" id="div_usuarios">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.canales.field.usuarios" /></label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<select name="usuarios[]" id="selector_usuarios" multiple="multiple" style="width: 100%">
							<x:forEach select="$editar_canal_xml/Canal/usuarios/Usuario" var="item">
								<option value="<x:out select="$item/idusuario" />"><x:out select="$item/nombrecompleto" /></option>
							</x:forEach>				
					</select>
				</div>
			</div>	
			
			<div class="form-group" id="div_tarifas">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.productos.field.operaciones" /></label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<select name="operaciones[]" id="selector_operaciones" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
							<x:forEach select="$editar_canal_selector_operacion_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>				
					</select>
				</div>
			</div>	
			
			
			
		</div>	
				
		
		<div class="col-md-6 col-sm-6 col-xs-12">
		
			
			
			<div class="form-group" id="div_productos">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.canales.field.productos" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="productos[]" id="selector_productos" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
							<x:forEach select="$editar_canal_selector_productos_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>				
					</select>
				</div>
			</div>	
			
			
	    	<div class="form-group" id="div_tarifas">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.canales.field.tarifas" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="tarifas[]" id="selector_tarifas" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
							<x:forEach select="$editar_canal_selector_tarifas_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>				
					</select>
				</div>
			</div>			

	    	<div class="form-group" id="div_tarifas">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.canales.field.formaspago" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="formaspago[]" id="selector_formasPago" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
							<x:forEach select="$editar_canal_selector_formasPago_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>				
					</select>
				</div>
			</div>			
		
	    	<div class="form-group" id="div_tarifas">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.canales.field.descuentosPromocional" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="descuentosPromocional[]" id="selector_descuentosPromocional" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
							<x:forEach select="$editar_canal_selector_descuentosPromo_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>				
					</select>
				</div>
			</div>	
		
    			
		</div>		
		
		
		
	</form>
	<div class="modal-footer">

		<button id="save_canal_button" type="button" class="btn btn-primary save_canal_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>	


<script>

$(":input").inputmask();
//*********************************************

$("#cliente").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>", "#cliente", "#idcliente");

	hideSpinner("#tab_canales_new");
	hideSpinner("#tab_canales_edit");
	
	$(".select-select2").select2({
		language : "es",
		containerCssClass : "single-line"
	});	
	
	selectedValues = new Array();
	<x:forEach select="$editar_canal_xml/Canal/descuentopromocionalcanals/Descuentopromocionalcanal/descuentopromocional" var="item">
		selectedValues.push('<x:out select="$item/iddescuentopromocional" />');
	</x:forEach>
	
	$('#selector_descuentosPromocional').val(selectedValues);
	$('#selector_descuentosPromocional').trigger('change.select2');	
	
	
	selectedValues = new Array();
	<x:forEach select="$editar_canal_xml/Canal/formapagocanals/Formapagocanal/formapago" var="item">
		selectedValues.push('<x:out select="$item/idformapago" />');
	</x:forEach>
	$('#selector_formasPago').val(selectedValues);
	$('#selector_formasPago').trigger('change.select2');	
	
	
		
	selectedValues = new Array();
	<x:forEach select="$editar_canal_xml/Canal/tarifacanals/Tarifacanal/tarifa" var="item">
		selectedValues.push('<x:out select="$item/idtarifa" />');
	</x:forEach>
	$('#selector_tarifas').val(selectedValues);
	$('#selector_tarifas').trigger('change.select2');	
	
	
	
	selectedValues = new Array();
	<x:forEach select="$editar_canal_xml/Canal/operacioncanals/Operacioncanal/operacion" var="item">
		selectedValues.push('<x:out select="$item/idoperacion" />');
	</x:forEach>
	$('#selector_operaciones').val(selectedValues);
	$('#selector_operaciones').trigger('change.select2');	
	
	
	
	selectedValues = new Array();
	<x:forEach select="$editar_canal_xml/Canal/productoscanals/Productoscanal/producto" var="item">
		selectedValues.push('<x:out select="$item/idproducto" />');
	</x:forEach>
	$('#selector_productos').val(selectedValues);	
	$('#selector_productos').trigger('change.select2');	
	
	$('#selector_unidad option[value="<x:out select = "$editar_canal_xml/Canal/unidadnegocio/idunidadnegocio" />"]').attr("selected", "selected");
	$('#selector_tipo_canal option[value="<x:out select = "$editar_canal_xml/Canal/tipocanal/idtipocanal" />"]').attr("selected", "selected");
	
	
	$(".save_canal_dialog").on("click", function(e) { 
		$("#form_canal_data").submit();			
	})
	
	
	$("#form_canal_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			guardarCanal()
		}
	}
	);
	
	function guardarCanal()
	{
		var data = $("#form_canal_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/productos/productos/save_canal.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				
				$("#modal-dialog-form").modal('hide');
				
				dt_listcanales.ajax.reload(null,false);
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});

	}


</script>
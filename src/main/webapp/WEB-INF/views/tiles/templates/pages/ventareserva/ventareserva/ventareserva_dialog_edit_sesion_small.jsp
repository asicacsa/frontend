<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<x:parse xml="${disponibles_datos_sesiones}" var="disponibles_datos_sesiones_xml" />
<x:parse xml="${ventareserva_selector_descuentos}" var="ventareserva_selector_descuentos_xml" />

 <div class="modal-header">
 	<button type="button" id="cerrar_con_x" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button> 
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.edit_sesion.title" />
	</h4>	
</div>
 
<div class="modal-body">

	<form id="form_edit_sesion" class="form-horizontal form-label-left">
		<div class="col-md-12 col-sm-12 col-xs-12">		
			<div class="form-group">
				
						
				<label class="control-label col-md-1 col-sm-1 col-xs-12"><spring:message code="venta.ventareserva.dialog.edit_sesion.field.numero" /></label>				
				<div class=" col-md-2 col-sm-2 col-xs-12">
  				   <button type="button"class="input-group-button btn-number" id="minus" data-type="minus" data-field="quant[1]"> <i class="fa fa-minus" aria-hidden="true"></i> </button>
  				    <input type="text" min="1" class="form-control input-number" max="10000" name="quant[1]" id="cantidad" value="1" >  				
  				    <button type="button"class="input-group-button btn-number" id="plus" data-type="plus" data-field="quant[1]"> <i class="fa fa-plus" aria-hidden="true"></i> </button>
				</div>				
				
					
				<label class="control-label col-md-1 col-sm-1 col-xs-12"><spring:message code="venta.ventareserva.dialog.edit_sesion.field.tarifa" /></label>
				<div class="col-md-2 col-sm-2 col-xs-12">
					<select class="form-control" name="idtarifa" id="idtarifa">
						<option value=""></option>
					</select>
				</div>		
				
					
				<label class="control-label col-md-1 col-sm-1 col-xs-12"><spring:message code="venta.ventareserva.dialog.edit_sesion.field.descuentos" /></label>
				<div id="descuentos_div" class="col-md-2 col-sm-2 col-xs-12">
					<select class="form-control" name="iddescuento" id="iddescuento">
						<option value=""></option>
						<x:forEach select="$ventareserva_selector_descuentos_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
				
							
				<label class="control-label col-md-2 col-sm-2x col-xs-12"><spring:message code="venta.ventareserva.dialog.edit_sesion.field.bono_a" /></label>
				<div class="col-md-1 col-sm-1 col-xs-12">
					<input name="bono" id="bono" type="text" class="form-control" value="${bono}">
				</div>		
				
				
				
			</div>
			<x:forEach select = "$disponibles_datos_sesiones_xml/ArrayList/ArrayList" var = "item">
	
				<div id="tabla-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />" class="col-md-12 col-sm-12 col-xs-12 disponibles">
				
					<div class="col-md-10 col-sm-10 col-xs-12">
	
						<div class="form-group encabezado-disponibilidad">
							<label class="control-label col-md-8 col-sm-8 col-xs-12 label-disponibilidad">Disponibilidad <span class="nombre-producto"><x:out select = "$item/Tipoproducto/nombre" /></span></label>
							<label class="control-label col-md-1 col-sm-1 col-xs-4""><spring:message code="venta.ventareserva.dialog.disponibles.field.fecha" /></label>
							<div class="col-md-3 col-sm-3 col-xs-8" id="div-fecha-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />">
			                      <div class="input-prepend input-group">
			                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
			                        <input type="text" name="fecha_disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />" id="fecha_disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />" class="form-control" readonly value="<x:out select = "$item/String" />"/>
			                      </div>
							</div>
						</div>
					</div>
				
					<div class="btn-group pull-right btn-datatable">
						<a type="button" class="btn btn-info" id="button_disponibles_lock-<x:out select = "$item/Tipoproducto/idtipoproducto" />">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.bloquear" />"> <span class="fa fa-lock"></span>
							</span>
						</a>
						<a type="button" class="btn btn-info" id="button_disponibles_unlock-<x:out select = "$item/Tipoproducto/idtipoproducto" />">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.desbloquear" />"> <span class="fa fa-unlock"></span>
							</span>
						</a>
					</div>
				
					<div class="col-md-12 col-sm-12 col-xs-12">
						<table id="datatable-list-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />" class="table table-striped table-bordered dt-responsive sesiones" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.idsesion" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.fecha" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.hora" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.overbooking" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.contenido" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.idzona" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.zona" /></th>
									<th class="disponibles"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.disponibles" /></th>
									<th class="bloqueadas"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.bloqueadas" /></th>
									<th class="reservadas"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.reservadas" /></th>
									<th class="vendidas"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.vendidas" /></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
						<span>&nbsp;</span>
					</div>
				</div>
				
			</x:forEach>
		</div>
		
		 <div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
					<button type="button" class="btn btn-info" id="save_edit_sesion">
						<spring:message code="common.button.save" />
					</button>	
					<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal" id="cerrar_ediccion">
						<spring:message code="common.button.cancel" />
					</button>
			</div>
		</div> 
	</form>
</div>


<script>
hideSpinner("#button_${buttonEdit}_editar");
hideSpinner("#tab_venta_individual");	
hideSpinner("#tab_reserva_grupo");
hideSpinner("#tab_reserva_individual");

var sesion_data=$.extend(true,[],${list}.rows( { selected: true } ).data()[0][0]), 
	cerrar_dialogo= false, actualizando= false;

var item=sesion_data.Lineadetalle.perfiles.Perfilvisitante;
if (typeof item!="undefined") {
	var $select= $("#idtarifa");
	$select.html('');
	$select.prepend("<option value='' selected='selected'></option>");
	if (item.length>0)
	    $.each(item, function(key, val){
	      $select.append('<option value="' + val.idperfilvisitante + '" idtarifa="' + val.tarifa.idtarifa + '" importe="' + val.tarifa.Tarifaproducto.importe + '">' + val.nombre + '</option>');
	    })
	else
		$select.append('<option value="' + item.idperfilvisitante + '" idtarifa="' + item.tarifa.idtarifa + '" importe="' + item.tarifa.Tarifaproducto.importe + '">' + item.nombre + '</option>');
	$select.html($select.children('option').sort(function (a, b) {
	      return a.text === b.text ? 0 : a.text < b.text ? -1 : 1;
	}));
}

$("#cantidad").val(sesion_data.Lineadetalle.cantidad);

$("#bono").val(sesion_data.Lineadetalle.numerobonoagencia);
if (typeof sesion_data.Lineadetalle.perfilvisitante.idperfilvisitante!="undefined") {
	$('#idtarifa option[value="'+ sesion_data.Lineadetalle.perfilvisitante.idperfilvisitante +'"]').attr("selected", "selected");
}
$('#iddescuento option[value="'+ ${list}.rows( { selected: true } ).data()[0][${idxDescuento_id}] +'"]').attr("selected", "selected");


var sesion_data_old;
var cantidad_old;
var id_tarifa_old;
var idPerfil_old;
var id_tarifa_txt_old;
var id_descuento_old;
var id_descuento_text_old;
var idbono_old;
var importe_unitario_old;
var importe_old;
//*************************************************
function invalidar_linea()
{
	$("#modal-dialog-form").modal('hide');
	${list}.rows().deselect();
	${list}.rows().invalidate().draw();
}
//*************************************************
function volverLinea()
{
	new PNotify({
	      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
	      text: '<spring:message code="venta.error.tarifa.producto" />',
		  type: "error",
		  delay: 5000,
		  buttons: { closer:true, sticker: false }			  
	   });	
	${list}.rows( { selected: true } ).data()[0][0]= sesion_data_old;
	${list}.rows( { selected: true } ).data()[0][${idxNumero}]= cantidad_old;
	if ($("#idtarifa :selected").val()!="") {
		${list}.rows( { selected: true } ).data()[0][${idxTarifa_id}]= id_tarifa_old;
		${list}.rows( { selected: true } ).data()[0][${idxPerfil}]= idPerfil_old;
		${list}.rows( { selected: true } ).data()[0][${idxTarifa_txt}]= id_tarifa_txt_old;
		${list}.rows( { selected: true } ).data()[0][${idxDescuento_id}]= id_descuento_old;
		${list}.rows( { selected: true } ).data()[0][${idxDescuento_txt}]= id_descuento_text_old;
		${list}.rows( { selected: true } ).data()[0][${idxBono}]= idbono_old;
		${list}.rows( { selected: true } ).data()[0][${idxUnitario}]= importe_unitario_old;
		${list}.rows( { selected: true } ).data()[0][${idxTotal}]= importe_old;
	}
	${list}.rows().invalidate().draw();
}
//*************************************************
function cerrarActualizarTabla() {
	actualizado_linea = true;
	if(${list}.rows( { selected: true } ).data().length==0)
		return;
	sesion_data_old = ${list}.rows( { selected: true } ).data()[0][0];
	cantidad_old = ${list}.rows( { selected: true } ).data()[0][${idxNumero}];
	id_tarifa_old = ${list}.rows( { selected: true } ).data()[0][${idxTarifa_id}];
	idPerfil_old = ${list}.rows( { selected: true } ).data()[0][${idxPerfil}];
	id_tarifa_txt_old = ${list}.rows( { selected: true } ).data()[0][${idxTarifa_txt}];
	id_descuento_old = ${list}.rows( { selected: true } ).data()[0][${idxDescuento_id}];
	id_descuento_text_old = ${list}.rows( { selected: true } ).data()[0][${idxDescuento_txt}];
	idbono_old = ${list}.rows( { selected: true } ).data()[0][${idxBono}];
	importe_unitario_old = ${list}.rows( { selected: true } ).data()[0][${idxUnitario}]
	importe_old = ${list}.rows( { selected: true } ).data()[0][${idxTotal}];
	
	${list}.rows( { selected: true } ).data()[0][0]= sesion_data;
	${list}.rows( { selected: true } ).data()[0][${idxNumero}]= $("#cantidad").val();
	if ($("#idtarifa :selected").val()!="") {
		${list}.rows( { selected: true } ).data()[0][${idxTarifa_id}]= $("#idtarifa :selected").attr("idtarifa");
		${list}.rows( { selected: true } ).data()[0][${idxPerfil}]= $("#idtarifa :selected").val();
		${list}.rows( { selected: true } ).data()[0][${idxTarifa_txt}]= $("#idtarifa :selected").text();
		
		${list}.rows( { selected: true } ).data()[0][${idxDescuento_id}]= $("#iddescuento :selected").val();
		${list}.rows( { selected: true } ).data()[0][${idxDescuento_txt}]= $("#iddescuento :selected").text();
		var bono = ""+ $("#bono").val();
		if(bono!="")
			conAbono= true;
			
		${list}.rows( { selected: true } ).data()[0][${idxBono}]= $("#bono").val();
		${list}.rows( { selected: true } ).data()[0][${idxUnitario}]= $("#idtarifa :selected").attr("importe");
		${list}.rows( { selected: true } ).data()[0][${idxTotal}]= sesion_data.Lineadetalle.importe;
	}
	
	actualizarSesionesDisponibles();
	//obtener_totales_venta_temporal("${buttonEdit}","${buttonEdit}","${idCliente}","${idTipoVenta}");
}
//*************************************************
function calcularImporteTotal() {
	var total= Number($("#idtarifa :selected").attr("importe"))*parseInt($("#cantidad").val());
	return total.toFixed(2);	
}
//*************************************************
function actualizarLineaDetalle() {
	actualizando= true;
	if ($("#iddescuento").val()!="") {
		showFieldSpinner("#descuentos_div");
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/venta/ventareserva/aplicar_descuentos.do'/>",
			timeout : 100000,
			data: {
					idcliente: "${idCliente}",
					lineadetalle: "<Lineadetalle>"+json2xml(sesion_data.Lineadetalle,"")+"</Lineadetalle>"
				  }, 
			success : function(data) {
				hideSpinner("#descuentos_div");
				console.log(data);
				sesion_data.Lineadetalle.importe= data.Lineadetalle.importe;
				if (cerrar_dialogo) cerrarActualizarTabla();
				actualizando= false;
			},
			error : function(exception) {
				hideSpinner("#descuentos_div");
				actualizando= false;
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",
					  buttons: { sticker: false }				  
				   });		
			}
		});
	}
	else {
		sesion_data.Lineadetalle.importe= calcularImporteTotal();
		actualizando= false;
	}
}
//*************************************************
$("#cantidad").on("change", function(e) {
	sesion_data.Lineadetalle.cantidad= $("#cantidad").val();
	actualizarLineaDetalle();
});
//*************************************************
function obtenerPerfilVisitante(tarifas,idperfilvisitante) {
	for (var i=0; i<tarifas.length; i++) 
		if (tarifas[i].idperfilvisitante==idperfilvisitante) return(tarifas[i]); 
	return("");
}
//*************************************************
$("#idtarifa").on("change", function(e) {
	var perfilVisitante= obtenerPerfilVisitante(sesion_data.Lineadetalle.perfiles.Perfilvisitante,$("#idtarifa").val());
	sesion_data.Lineadetalle.perfilvisitante.nombre= perfilVisitante.nombre;
	sesion_data.Lineadetalle.perfilvisitante.idperfilvisitante= perfilVisitante.idperfilvisitante;
	sesion_data.Lineadetalle.perfilvisitante.tarifa.nombre= perfilVisitante.tarifa.nombre;
	sesion_data.Lineadetalle.importe= calcularImporteTotal();
	showFieldSpinner("#descuentos_div");
	$select=$("#iddescuento");
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/list_descuentos.do'/>",
		timeout : 100000,
		data: {
				idproducto: "${idProducto}",
				idcanal: "${sessionScope.idcanal}",
				idtarifa: $("#idtarifa :selected").attr("idtarifa")
			  }, 
		success : function(data) {
			hideSpinner("#descuentos_div");
			$select.html('');
			$select.prepend("<option value='' selected='selected'></option>");
			if (data.ArrayList!="") {
				var item=data.ArrayList.LabelValue;
				if (item.length>0)
				    $.each(item, function(key, val){
				      $select.append('<option value="' + val.value + '">' + val.label + '</option>');
				    });
				else
			      	$select.append('<option value="' + item.value + '">' + item.label + '</option>');
			}
		},
		error : function(exception) {
			hideSpinner("#descuentos_div");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});	
}); 
//*************************************************
$("#iddescuento").on("change", function(e) {
	sesion_data.Lineadetalle.descuentopromocional.iddescuentopromocional= $("#iddescuento").val();
	sesion_data.Lineadetalle.descuentopromocional.nombre= $("#iddescuento").text();
	actualizarLineaDetalle();		
});
//*************************************************
$("#bono").on("change", function(e) {
	sesion_data.Lineadetalle.numerobonoagencia= $("#bono").val();
	actualizarLineaDetalle();		
});

//*************************************************
$("#save_edit_sesion").on("click", function(e) {
	var sesiones= $(".sesiones").DataTable().rows( { selected: true } ).data();
	if (sesiones.length < $(".sesiones.dtr-inline").length) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}else
		$("#form_edit_sesion").submit();	
});
//*************************************************
$("#form_edit_sesion").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		saveSesionData();		
	}
});
//****************************************
function saveSesionData() {
	cerrar_dialogo= true;	
	if (!actualizando) 
		cerrarActualizarTabla();
	
}
//**********************************
$("#cerrar_ediccion").on("click", function(e) {
	${list}.rows().deselect();
});
//**********************************
$("#cerrar_con_x").on("click", function(e) {
	${list}.rows().deselect();
});
//***************************************
//Botón + - 
$('.btn-number').click(function(e){
	
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {        	
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
              //  $("#plus").attr('disabled', false);
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {
	
            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
                //$("#minus").attr('disabled', false);                
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
//*************************************************
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
//*************************************************
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('El valor mínimo es 1');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('El valor máximo es 10000');
        $(this).val($(this).data('oldValue'));
    }
});
//*************************************************
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
//*************************************************

/****** Código sesiones disponibles ****/
var json_sesiones= [ <c:out value="${disponibles_datos_sesiones_json}"  escapeXml="false" /> ];
var fecha_seleccionada="${fecha}";

function check_selected(tabla) {
	sesiones= tabla.rows().data();
	$.each(sesiones, function(key_sesiones,fila_sesiones) {
		
		if (${list}.rows({selected: true}).data().length>0) {
			var item= ${list}.rows({selected: true}).data()[0][11];
			if (item.length>0) {
				$.each(item, function(key_seleccionadas,fila_seleccionadas) {
					if (fila_sesiones[1]==fila_seleccionadas[1] && fila_sesiones[6]==fila_seleccionadas[6]) tabla.row(key_sesiones).select().draw();
				});
			}
		}
			
	});
}
//*************************************************
function get_sesion(idtipoproducto,idsesion,idzona,sesionesdata) {
	var result= null;
	if (sesionesdata==null) sesionesdata= json_sesiones[0].ArrayList;
	if (sesionesdata!="") {
		var item= sesionesdata.ArrayList;
		if (typeof item!="undefined")
			if (item.length>0)
			    $.each(item, function(key, val){
			    	if (val.Tipoproducto.idtipoproducto==idtipoproducto) {
			    		if (val.sesiones!="") {
				   			var sesiones= val.sesiones.Sesion;
				   			if (sesiones.length>0) 
				   			    $.each(sesiones, function(key, val){
				   			    	//if (typeof val.Sesion!="undefined")
					   			    	//if (val.Sesion.idsesion==idsesion) result= val.Sesion
					   			    	if (val.idsesion==idsesion && val.zonasesions.Zonasesion.zona.idzona==idzona) result= val
				   			    });
			   			    else if (sesiones.idsesion==idsesion && sesiones.zonasesions.Zonasesion.zona.idzona==idzona) result= sesiones;
				    	}
			    	}
			    })
			else if (item.Tipoproducto.idtipoproducto==idtipoproducto) {
				if (item.sesiones!="") {
		   			var sesion= item.sesiones.Sesion;
		   			if (sesion.length>0) 
		   			    $.each(sesion, function(key, val){
		   			    	if (val.idsesion==idsesion && val.zonasesions.Zonasesion.zona.idzona==idzona) result= val
		   			    });
	   			    else if (sesion.idsesion==idsesion && sesion.zonasesions.Zonasesion.zona.idzona==idzona) result= sesion;
				}
			}
	}
	return result;
}
//*************************************************
<x:forEach select = "$disponibles_datos_sesiones_xml/ArrayList/ArrayList" var = "item">

	$('input[name="fecha_disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />"]').daterangepicker({
	    singleDatePicker: true,
	    showDropdowns: true,
		minDate: moment(),
	  	locale: $daterangepicker_sp
	});

	var dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />=$('#datatable-list-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />').DataTable( {
		language: dataTableLanguage,
		info: false,
		searching: false,
		scrollY: false,
		scrollCollapse: true,
		paging: false,
	    select: { style: 'single' },
		columnDefs: [
            { "targets": 0, "visible": false },
            { "targets": 1, "visible": false },
            { "targets": 2, "visible": false },
            { "targets": 6, "visible": false },
            { "targets": 12, "visible": false }
	    ],
	    "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
            /* Append the grade to the default row class name */
            var sesion = aData[0];
            if(sesion.bloqueado==1)
            	$('td', nRow).css('color', 'Red');
            
        },
	    drawCallback: function( settings ) { 
	    	activateTooltipsInTable('datatable-list-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />');
	   	}
	} );
	//*************************************************	
	function ajustar_cabeceras_datatable_<x:out select = "$item/Tipoproducto/idtipoproducto" />()
	{
		$('#datatable-list-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />').DataTable().columns.adjust().draw();
	}
	
	insertSmallSpinner("#datatable-list-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />_processing");
	
	<x:forEach select = "$item/sesiones/Sesion" var="sesion">
		dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.row.add([
	         get_sesion("<x:out select = "$item/Tipoproducto/idtipoproducto" />","<x:out select = "$sesion/idsesion" />","<x:out select = "$sesion/zonasesions/Zonasesion/zona/idzona" />",null), 
	         "<x:out select = "$sesion/idsesion" />", 
	         "<x:out select = "$sesion/fecha" />",         
	         "<x:out select = "$sesion/horainicio" />", 
	         "<x:out select = "$sesion/overbookingventa" />", 
	         "<span style='white-space: nowrap' title='<x:out select = "$sesion/contenido/descripcion" />'> <x:out select = "$sesion/contenido/nombre" /> </span>",
	         "<x:out select = "$sesion/zonasesions/Zonasesion/zona/idzona" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/zona/nombre" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/numlibres" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/numbloqueadas" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/numreservadas" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/numvendidas" />",
	         "<span style='white-space: nowrap' title='<x:out select = "$sesion/contenido/descripcion" />'> <x:out select = "$sesion/horainicio" /> <x:out select = "$sesion/contenido/nombre" /> </span>",
	    ]);
	</x:forEach>
	dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.draw();
	setTimeout(ajustar_cabeceras_datatable_<x:out select = "$item/Tipoproducto/idtipoproducto" />, 500);
	check_selected(dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />);

	//********************************************************************************
		
	$('input[name="fecha_disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />"]').on("change", function() { 
		//dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.processing(true);
		showFieldSpinner("#div-fecha-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />");
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/venta/ventareserva/list_tipos_producto.do'/>",
			timeout : 100000,
			data: {
	  			   idtipoproducto: "<x:out select = "$item/Tipoproducto/idtipoproducto" />",
		    	   fecha: $('input[name="fecha_disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />"]').val()
				  }, 
			success : function(data) {
				if (data.ArrayList!="") {
					dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.rows().remove().draw();
					var item= data.ArrayList.sesiones.Sesion;
					if (typeof item!="undefined")
						if (item.length>0) {
						    $.each(item, function(key, sesion){
								dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.row.add([
									 sesion,								                                                                                   
							         sesion.idsesion, 
							         sesion.fecha, 
							         sesion.horainicio, 
							         sesion.overbookingventa, 
							         "<span title='"+sesion.contenido.descripcion+"'> "+sesion.contenido.nombre+" </span>", 
							         sesion.zonasesions.Zonasesion.zona.idzona, 
							         sesion.zonasesions.Zonasesion.zona.nombre, 
							         sesion.zonasesions.Zonasesion.numlibres, 
							         sesion.zonasesions.Zonasesion.numbloqueadas, 
							         sesion.zonasesions.Zonasesion.numreservadas, 
							         sesion.zonasesions.Zonasesion.numvendidas,
							         "<span style='white-space: nowrap' title='"+sesion.contenido.descripcion+"'> "+sesion.horainicio+" "+sesion.contenido.nombre+" </span>"					         
							    ]);
						    });
						}
						else {
							dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.row.add([
							     item,                                                                              			
						         item.idsesion, 
						         item.fecha, 
						         item.horainicio, 
						         item.overbookingventa, 
						         "<span title='"+item.contenido.descripcion+"'> "+item.contenido.nombre+" </span>", 
						         item.zonasesions.Zonasesion.zona.idzona, 
						         item.zonasesions.Zonasesion.zona.nombre, 
						         item.zonasesions.Zonasesion.numlibres, 
						         item.zonasesions.Zonasesion.numbloqueadas, 
						         item.zonasesions.Zonasesion.numreservadas, 
						         item.zonasesions.Zonasesion.numvendidas,
						         "<span style='white-space: nowrap' title='"+item.contenido.descripcion+"'> "+item.horainicio+" "+item.contenido.nombre+" </span>"					         
						    ]);
						    dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.row(0).select().draw();
						}
						
					//dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.processing(false);
					hideSpinner("#div-fecha-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />");
				    dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.draw();
				}
			},
			error : function(exception) {
				//dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.processing(false);
				hideSpinner("#div-fecha-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />");
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",
					  buttons: { sticker: false }				  
				   });		
			}
		});	
	});

	//********************************************************************************
	$("#button_disponibles_lock-<x:out select = "$item/Tipoproducto/idtipoproducto" />").on("click", function(e) {
		var data = dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.rows( { selected: true } ).data();
		if (data.length<=0) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="administracion.programacion.programacion.list.alert.localidades.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}	

		var button_bloqueos= "button_disponibles_lock-<x:out select = "$item/Tipoproducto/idtipoproducto" />",
			lista_bloqueos="dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />";
		
		showButtonSpinner("#"+button_bloqueos);
		$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/show_new_bloqueo.do'/>?id="+data[0][1]+"&button="+button_bloqueos+"&lista="+lista_bloqueos+"&zona="+data[0][6]+"&es_programacion=false&idx_libres=8&idx_bloqueadas=9", function() {
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "sm");
		});
	});
	
	//********************************************************************************
	$("#button_disponibles_unlock-<x:out select = "$item/Tipoproducto/idtipoproducto" />").on("click", function(e) { 
		
		var data = dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.rows( { selected: true } ).data();
		
		if (data.length<=0) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="administracion.programacion.programacion.list.alert.localidades.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}	
		
		var button_bloqueos= "button_disponibles_unlock-<x:out select = "$item/Tipoproducto/idtipoproducto" />",
		lista_bloqueos="dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />";
		
		showButtonSpinner("#button_programacion_bloqueos");
		$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/show_bloqueos.do'/>?id="+data[0][1]+"&button="+button_bloqueos+"&lista="+lista_bloqueos+"&zona="+data[0][6]+"&es_programacion=false&idx_libres=8&idx_bloqueadas=9", function() {
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "md");
		});

	});	

</x:forEach>
//*************************************************
function obtenerIdTarifa(tarifas,idperfilvisitante) {
	for (var i=0; i<tarifas.length; i++) 
		if (tarifas[i].idperfilvisitante==idperfilvisitante) return(tarifas[i].tarifa.idtarifa); 
	return("");
}
//*************************************************
function construir_xml_sesiones(sesiones_sel) {
	var xml_sesiones= "<sesiones>";
	for (var i=0; i<sesiones_sel.length; i++) { 
		xml_sesiones+="<Sesion>";
		xml_sesiones+=json2xml(sesiones_sel[i][0],"");
		xml_sesiones+="</Sesion>";
	}
	xml_sesiones+="</sesiones>";
	return(xml_sesiones);
}

function actualizarSesionesDisponibles()
{
	var sesiones= $(".sesiones").DataTable().rows( { selected: true } ).data();
	if (sesiones.length < $(".sesiones.dtr-inline").length) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	for (var i=0; i<sesiones.length; i++) 
	{
		if(sesiones[i][0].bloqueado==1)
			{
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="venta.ventareserva.list.alert.productos.seleccion.bloqueado" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
			}			
	}
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/list_datos_sesiones.do'/>",
		timeout : 100000,
		data: {
				idproducto: "${idProducto}",
				idcliente: "${idCliente}",
				idtipoventa: "${idTipoVenta}",
				idcanal: "${sessionScope.idcanal}",
				fecha: "${fecha}",
				xml: construir_xml_sesiones(sesiones)
			  }, 
		success : function(data) {

			var fechas= "", contenidos= "", disponibles= "",
				retorno= false;
			for (var i=0; i<sesiones.length; i++) {
				if (retorno) {
					fechas+="</br>";
					contenidos+= "</br>";
					disponibles+= "</br>";
				}
				fechas+= sesiones[i][2].split("-")[0],
				//contenidos+= sesiones[i][3]+" "+sesiones[i][5];
				contenidos+= sesiones[i][12];
				disponibles+= sesiones[i][8];
				retorno= true;
			}
			var bono= "";
			
			sesion_data=$.extend(true,[],${list}.rows( { selected: true } ).data()[0][0]);
			sesion_data.Lineadetalle.lineadetallezonasesions= data.Lineadetalle.lineadetallezonasesions;
			bono= ${list}.rows( { selected: true } ).data()[0][12];
			${list}.rows( { selected: true } ).remove(); // Se borra la línea si se edita la sesión
		
			sesion_data.Lineadetalle.cantidad = $("#cantidad").val();
			sesion_data.Lineadetalle.perfilvisitante.nombre = $("#idtarifa :selected").text();
			sesion_data.Lineadetalle.tarifaproducto.importe = Number($("#idtarifa :selected").attr("importe"));
			//sesion_data.Lineadetalle.importe = calcularImporteTotal();
			actualizarLineaDetalle();
			sesion_data.Lineadetalle.perfilvisitante.idperfilvisitante = $("#idtarifa :selected").val();
			
			sesion_data.Lineadetalle.numerobonoagencia= $("#bono").val();
			
			sesion_data.Lineadetalle.descuentopromocional.iddescuentopromocional= $("#iddescuento :selected").val();
			sesion_data.Lineadetalle.descuentopromocional.nombre= $("#iddescuento :selected").text();

			
			${list}.row.add([sesion_data,
			                 "${idProducto}", 
	                         "${producto}",
				             fechas, // fecha
				             contenidos, // contenido
				             disponibles, // disponibles
				             sesion_data.Lineadetalle.cantidad,  // Nº Etd
				             typeof sesion_data.Lineadetalle.perfilvisitante.nombre=="undefined"?"":sesion_data.Lineadetalle.perfilvisitante.nombre, // Perfil/Tarifa
				             $("#iddescuento :selected").text(), // Descuentos
				             sesion_data.Lineadetalle.tarifaproducto.importe, // I. Unitario
				             sesion_data.Lineadetalle.importe,  // Importe total
				             sesiones, // Datos de las sesiones
				             bono, // Bono
				             typeof sesion_data.Lineadetalle.perfiles.Perfilvisitante=="undefined"?"":obtenerIdTarifa(sesion_data.Lineadetalle.perfiles.Perfilvisitante,sesion_data.Lineadetalle.perfilvisitante.idperfilvisitante),
				             $("#iddescuento :selected").val(),  // iddescuento
				             sesion_data.Lineadetalle.perfilvisitante.idperfilvisitante   // idperfil
			                ]).draw();
			obtener_totales_venta_temporal("${buttonEdit}","${buttonEdit}","${idCliente}","${idTipoVenta}");
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});	

	$("#modal-dialog-form").modal('hide');

}


//*************************************************

</script>
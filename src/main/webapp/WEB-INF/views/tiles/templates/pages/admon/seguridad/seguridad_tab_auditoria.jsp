<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${tabauditoria_datos_auditoria}" var="tabauditoria_datos_auditoria_xml" />

<div id="principal">

	<form id="form_auditoria_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

		<div class="col-md-6 col-sm-6 col-xs-12">

			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.seguridad.tabs.auditoria.field.activada" /></label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<input id="activada-switch" name="activada" type="checkbox" class="js-switch-activacion" <x:if select="$tabauditoria_datos_auditoria_xml/Auditoria/auditoriaActivada='true'">checked=""</x:if> data-switchery="true" style="display: none;">
				</div>
			</div>
			
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12">
		
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.seguridad.tabs.auditoria.field.usuarios" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
					<select name="usuarios[]" id="selector_usuarios_auditoria" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
					</select>
					</div>
				</div>
			</div>
	
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.seguridad.tabs.auditoria.field.funcionalidades" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<select name="funcionalidades[]" id="selector_funcionalidades_auditoria" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
						</select>
					</div>
				</div>
			</div>
			
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
		
				<button type="submit" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button id="cancel_auditoria_button" type="button" class="btn btn-cancel">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
		
	</form>

</div>

<script>

$("#cancel_auditoria_button").on("click", function(e) {
	var selectedValues = new Array();
	<x:forEach select="$tabauditoria_datos_auditoria_xml/Auditoria/ArrayList/Usuario" var="item">
		selectedValues.push('<x:out select="$item/idusuario" />');						
	</x:forEach>
	
	$('#selector_usuarios_auditoria').val(selectedValues);
	$('#selector_usuarios_auditoria').trigger('change.select2');

	var selectedValuesFuncionalidades = new Array();
	<x:forEach select="$tabauditoria_datos_auditoria_xml/Auditoria/ArrayList/Funcionalidad" var="item">
	selectedValuesFuncionalidades.push('<x:out select="$item/idfuncionalidad" />');
	</x:forEach>

	$('#selector_funcionalidades_auditoria').val(selectedValuesFuncionalidades);
	$('#selector_funcionalidades_auditoria').trigger('change.select2');	
});

$(".select-select2").select2({
	language : "es",
	containerCssClass : "single-line"
});

$("#activada-switch").each(function(index) {
	var s = new Switchery(this, {
		size : 'small'
	});
});

cargar_usuarios();
cargar_funcionalidades();

$("#form_auditoria_data").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		saveFormData();
	}
});

//********************************************************************************
function cargar_usuarios()
{
	$.ajax({
		type : "post",
		url : "<c:url value='/ajax/admon/seguridad/seguridad/select_list_usuario.do'/>",
		timeout : 100000,
		data : null,
		dataType : 'json',
		success : function(data) {
			$.each(data.ArrayList.LabelValue, function(i, item) {
			    $("#selector_usuarios_auditoria").append('<option value="'+item.value+'">'+item.label+'</option>');
			});				
		    
			var selectedValues = new Array();
			<x:forEach select="$tabauditoria_datos_auditoria_xml/Auditoria/ArrayList/Usuario" var="item">
				selectedValues.push('<x:out select="$item/idusuario" />');						
			</x:forEach>
			
			$('#selector_usuarios_auditoria').val(selectedValues);
			$('#selector_usuarios_auditoria').trigger('change.select2');
		}
	});
}

//********************************************************************************
function cargar_funcionalidades()
{
	var data = [ <c:out value="${json_selector_funcionalidades}"  escapeXml="false" /> ];
		
	$("#selector_funcionalidades_auditoria").select2ToTree({treeData: {dataArr: data}, containerCssClass : "single-line"});
	$("#selector_funcionalidades_auditoria .non-leaf").attr("disabled","disabled");
	
	var selectedValuesFuncionalidades = new Array();
	<x:forEach select="$tabauditoria_datos_auditoria_xml/Auditoria/ArrayList/Funcionalidad" var="item">
	selectedValuesFuncionalidades.push('<x:out select="$item/idfuncionalidad" />');
	</x:forEach>

	$('#selector_funcionalidades_auditoria').val(selectedValuesFuncionalidades);
	$('#selector_funcionalidades_auditoria').trigger('change.select2');	
}
	
//********************************************************************************	
function saveFormData() {
	var data = $("#form_auditoria_data").serializeObject();

	showSpinner("#principal");

	$.ajax({
		type : "post",
		url : "<c:url value='/ajax/admon/seguridad/seguridad/save_auditoria.do'/>",
		timeout : 100000,
		data : data,
		success : function(data) {
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
			
		},
		error : function(exception) {
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : exception.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		}
	});
}

</script>


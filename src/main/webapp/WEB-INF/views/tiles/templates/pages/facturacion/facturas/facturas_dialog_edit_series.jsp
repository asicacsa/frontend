<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_serie}" var="editar_serie_xml" />
<x:parse xml="${editar_serie_entidadgestora}" var="editar_serie_entidadgestora_xml" />
<x:parse xml="${editar_serie_serierectificativa}" var="editar_serie_serierectificativa_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	 <h4 class="modal-title">	
		<x:choose>
			<x:when select="not($editar_serie_xml/Tipofactura)">
				<spring:message code="facturacion.facturas.tabs.series.dialog.crear_serie.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="facturacion.facturas.tabs.series.dialog.editar_serie.title" />
			</x:otherwise>
		</x:choose>		
	</h4>	
</div>


<div class="modal-body">
	<form id="form_serie_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input id="idtipofactura" name="idtipofactura" type="hidden" value="<x:out select = "$editar_serie_xml/Tipofactura/idtipofactura" />" />
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.series.field.nombre" />*</label>
			<div class="col-md-9 col-sm-9 col-xs-9">
				<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editar_serie_xml/Tipofactura/nombre" />">
			</div>
		</div>			
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.series.field.serie" />*</label>
			<div class="col-md-9 col-sm-9 col-xs-9">	
				<input name="serie" id="serie" data-inputmask="'mask': '9{0,10}'" required="required" class="form-control"  value="<x:out select = "$editar_serie_xml/Tipofactura/serie" />"/>	
			</div>							
		</div>		
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.series.field.descripcion" /></label>
			<div class="col-md-9 col-sm-9 col-xs-9">	
				<input name="descripcion" id="descripcion" class="form-control"  value="<x:out select = "$editar_serie_xml/Tipofactura/descripcion" />"/>	
			</div>							
		</div>
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.series.field.valoractual" />*</label>
			<div class="col-md-9 col-sm-9 col-xs-9">	
				<input name="valor-actual" id="valor-actual" data-inputmask="'mask': '9{0,10}'" required="required" class="form-control"  value="<x:out select = "$editar_serie_xml/Tipofactura/numcontador" />"/>	
			</div>							
		</div>
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.series.field.entidadgestora" />*</label>
			<div class="col-md-9 col-sm-9 col-xs-9">
				<select name="identidadgestora" id="editar_serie_selector_entidadgestora" required="required" class="form-control"">
					<option value=""></option>
					<x:forEach select="$editar_serie_entidadgestora_xml/ArrayList/LabelValue" var="item">
						<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
					</x:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.series.field.serierectificativa" /></label>
			<div class="col-md-9 col-sm-9 col-xs-9">
				<select name="idserierectificativa" id="editar_serie_selector_serierectificativa"  class="form-control"">
					<option value=""></option>
					<x:forEach select="$editar_serie_serierectificativa_xml/ArrayList/Tipofactura" var="item">
						<option value="<x:out select="$item/idtipofactura" />"><x:out select="$item/nombre" /></option>
					</x:forEach>
				</select>
			</div>
		</div>		
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.series.field.rectificativa" /></label>
			<div class="col-md-9 col-sm-9 col-xs-9">	
				<input name="rectificativa" type="checkbox" <x:if select="$editar_serie_xml/Tipofactura/rectificativa='1'">checked=""</x:if> class="js-switch" data-switchery="true" style="display: none;">
			</div>
		</div>	
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.series.field.pordefecto" /></label>
			<div class="col-md-9 col-sm-9 col-xs-9">	
				<input name="pordefecto" type="checkbox" <x:if select="$editar_serie_xml/Tipofactura/pordefecto='1'">checked=""</x:if> class="js-switch" data-switchery="true" style="display: none;">
			</div>
		</div>
				<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.series.field.activa" /></label>
			<div class="col-md-9 col-sm-9 col-xs-9">	
				<input name="activa" type="checkbox" <x:if select="$editar_serie_xml/Tipofactura/activa='1'">checked=""</x:if> class="js-switch" data-switchery="true" style="display: none;">
			</div>
		</div>
	</div>
	</form>
	<div class="modal-footer">
		<button id="save_serie_button" type="button" class="btn btn-primary save_serie_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>	

<script>
$(document).ready(function() {
	hideSpinner("#tab_series_new");
	hideSpinner("#tab_series_edit");
	//*********************************************
	$(":input").inputmask();
	//*********************************************
	$('#editar_serie_selector_entidadgestora option[value="<x:out select = "$editar_serie_xml/Tipofactura/entidadgestora/identidadgestora" />"]').attr("selected", "selected");
	$('#editar_serie_selector_serierectificativa option[value="<x:out select = "$editar_serie_xml/Tipofactura/idtipofacturarectificativa" />"]').attr("selected", "selected");
	//*********************************************
	$(".js-switch").each(function(index) {
		var s = new Switchery(this, {
			size : 'small'
		});
	});
	//*********************************************
		$(".save_serie_dialog").on("click", function(e) {
			$("#form_serie_data").submit();	
		})
	//********************************************		
	$("#form_serie_data").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		guardarSerie()
	}
})
	//***********************************************
	function guardarSerie()
	{
		var data = $("#form_serie_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/facturacion/facturas/series/save_serie.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				
				$("#modal-dialog-form").modal('hide');
				dt_listseries.ajax.reload(null,false);
								
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});

	}
});

</script>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editarred_datos_red}" var="editarred_datos_red_xml" />
<x:parse xml="${editarred_selector_unidades}" var="editarred_selector_unidades_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<x:choose>
			<x:when select="not($editarred_datos_red_xml/Red)">
				<spring:message code="administracion.seguridad.tabs.redes.dialog.crear_red.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.seguridad.tabs.redes.dialog.editar_red.title" />
			</x:otherwise>
		</x:choose>
	</h4>
</div>
<div class="modal-body" id="principal">

	<form id="form_red_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

		<input id="idred" name="idred" type="hidden" value="<x:out select = "$editarred_datos_red_xml/Red/idred" />" />

		<div class="col-md-12 col-sm-12 col-xs-12">

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.redes.field.nombre" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editarred_datos_red_xml/Red/nombre" />">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.redes.field.administrador" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					  <select name="idusuario" id="selector_usuario" class="form-control" required="required">
						<option value=""></option>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.redes.field.descripcion" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
                    <textarea class="form-control" rows="3" name="descripcion" id="descripcion"><x:out select = "$editarred_datos_red_xml/Red/descripcion" /></textarea>
                </div>				
			</div>
			
		</div>

		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.seguridad.tabs.redes.field.unidades" /></label>
			<div class="col-md-9 col-sm-9 col-xs-9">
				<select name="unidades[]" id="selector_unidades" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
					<x:forEach select="$editarred_selector_unidades_xml/ArrayList/LabelValue" var="item">
						<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
					</x:forEach>
				</select>
			</div>
		</div>

		<div class="modal-footer">
	
			<button id="save_red_button" type="submit" class="btn btn-primary save_dialog">
				<spring:message code="common.button.save" />
			</button>
			<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
				<spring:message code="common.button.cancel" />
			</button>
		</div>
	</form>

</div>


<script>
	hideSpinner("#tab_redes_new");
	hideSpinner("#tab_redes_edit");

	$(".select-select2").select2({
		language : "es",
		containerCssClass : "single-line"
	});

	var selectedValues = new Array();
	<x:forEach select="$editarred_datos_red_xml/Red/redunidadnegocios/Redunidadnegocio/unidadnegocio" var="item">
	selectedValues.push('<x:out select="$item/idunidadnegocio" />');
	</x:forEach>
	$('#selector_unidades').val(selectedValues);
	$('#selector_unidades').trigger('change.select2');

	showSpinner("#principal");
	cargar_usuarios();

	$("#form_red_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFormData();
		}
	});
	
	//********************************************************************************
	function cargar_usuarios()
	{
		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/seguridad/seguridad/select_list_usuario.do'/>",
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				$.each(data.ArrayList.LabelValue, function(i, item) {
				    $("#selector_usuario").append('<option value="'+item.value+'">'+item.label+'</option>');
				});				
			    
				$('#selector_usuario option[value="<x:out select = "$editarred_datos_red_xml/Red/usuario/idusuario" />"]').attr("selected", "selected");
				
				//$('#selector_usuario').trigger('change.select2');
			    					
		    	hideSpinner("#principal");
			}
		});
	}

	//********************************************************************************	
	function saveFormData() {
		var data = $("#form_red_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/seguridad/seguridad/save_red.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				dt_listredes.ajax.reload(null,false);			
				$("#modal-dialog-form").modal('hide');
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
	}
</script>

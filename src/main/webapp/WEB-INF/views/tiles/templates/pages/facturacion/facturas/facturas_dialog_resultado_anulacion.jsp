<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${motivo_anulacion}" var="motivo_anulacion_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	 <h4 class="modal-title">	
		<spring:message code="facturacion.facturas.tabs.facturas.dialog.anular_factura_resultado.title" />				
	</h4>	
</div>

<div class="modal-body">
	<form id="form_factura_anular_datas_listado" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	<div id="tabla_rersultados" hidden>
	
		<table id="resultadosAnulacion_table" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
							  <th id="id"></th>
							  <th id="numero"><spring:message code="facturacion.facturas.tabs.factura_resultado.field.numero"/></th>
							  <th id="fechainiciovigencia"><spring:message code="facturacion.facturas.tabs.factura_resultado.field.cliente" /></th>
							  <th id="fechafinvigencia"><spring:message code="facturacion.facturas.tabs.factura_resultado.field.fecha_generacion" /></th>							 
							  <th id="fechafinvigencia"><spring:message code="facturacion.facturas.tabs.factura_resultado.field.importe" /></th>
							  <th id="fechafinvigencia"><spring:message code="facturacion.facturas.tabs.factura_resultado.field.tipo" /></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
		</table>
	</div>	
	</form>
</div>

<script>
var dtfacturasResultados;

$(document).ready(function() {
	
	dtfacturasResultados=$('#resultadosAnulacion_table').DataTable( {
		"paging": false,
		"info": false,
		"searching": false,
		select: { style: 'os'},
		language: dataTableLanguage,
		 "columnDefs": [
		                { "visible": false, "targets": [0,1]}
		              ]
		});
})
</script>
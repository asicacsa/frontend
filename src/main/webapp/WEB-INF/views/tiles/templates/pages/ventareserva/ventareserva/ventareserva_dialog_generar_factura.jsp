<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${tipos_serie_generacion_factura}" var="tipos_serie_generacion_factura_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="venta.ventareserva.generacionfactura.title" />
	</h4>	
</div>

<div class="modal-body">
	<form id="form_generar_factura_venta_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input id="idcliente" name="idcliente" type="hidden" value="" />
		<div class="col-md-12 col-sm-12 col-xs-12">		
		  	<div class="form-group button-dialog">
           		<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.descuentospromo.field.cliente" /></label>
           		<div class="col-md-9 col-sm-9 col-xs-9">
               		<input name="cliente" id="cliente" type="text" class="form-control" readonly value="">
              	</div>
            </div>			
			<div class="form-group">
				<div class="checkbox col-md-12 col-sm-12 col-xs-12 col-md-offset-3 col-sd-offset-3 col-xs-offset-3">
					<input type="checkbox" name="selector" id="checkserie" value="" class="flat" style="position: absolute; opacity: 0;">
					<strong><spring:message code="venta.ventareserva.generacionfactura.field.checkserie" /></strong>
				</div>
			</div>            
	  		<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.rappels.field.serie" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="idserie" id="selector_serie" class="form-control" disabled="disabled" required="required">
						<option value=""></option>
						<x:forEach select="$tipos_serie_generacion_factura_xml/ArrayList/Tipofactura" var="item">
							<option value="<x:out select="$item/idtipofactura" />"><x:out select="$item/nombre" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
		</div>
	</form>
	<div class="modal-footer">
		<button id="save_generar_factura_button" type="button" class="btn btn-primary save_generar_venta">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>	

<script>
hideSpinner("#checkin_button");
hideSpinner("#tab_busqueda_facturar");

var idcliente="${idCliente}";

if (idcliente!="") {
	$("#idcliente").val("${idCliente}");
}
else {
	$('#cliente').removeAttr('disabled');
	$("#cliente").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>", "#cliente", "#idcliente");
}

if ($("input.flat")[0]) {
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });
}

$("#checkserie").on("ifChanged", function(e) {
	if ($("#selector_serie").attr('disabled')) 
    	$("#selector_serie").removeAttr("disabled");
	else
		$("#selector_serie").attr("disabled", "disabled");
});

$("#save_generar_factura_button").on("click", function(e) {
	showSpinner("#modal-dialog-form-2 .modal-content");
	$("#form_generar_factura_venta_data").submit();
});

$("#form_generar_factura_venta_data").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		saveGenerarFactura();		
	}
});

function saveGenerarFactura() {
	var id_cliente_asociado = $("#idcliente").val();
	if(id_cliente_asociado=="")
		generarFactura();
	else
		asociarClienteYFacturar(id_cliente_asociado);
	
}

function  asociarClienteYFacturar(id_cliente_asociado){
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/modificarClienteVenta.do'/>",
		timeout : 100000,
		data: {
				idCliente: id_cliente_asociado,		
				idVenta: "${idVenta}"
			  }, 
		success : function(data) {
			generarFactura();			
		},
		error : function(exception) {
				hideSpinner("#modal-dialog-form-2 .modal-content");
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",
					  buttons: { sticker: false }				  
				   });		
		}
	});	
}


function  generarFactura(){
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/generar_factura.do'/>",
		timeout : 100000,
		data: {
				idventa: "${idVenta}",
				idtipofactura: $("#selector_serie").val()
			  }, 
		success : function(data) {
			hideSpinner("#modal-dialog-form-2 .modal-content");
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="venta.ventareserva.generacionfactura.alert.generada" />',
				type : "success",
				delay : 5000,
				buttons : { sticker : false	}
			});			
			$("#modal-dialog-form-2").modal('hide');
		}
	});	
}





</script>
	
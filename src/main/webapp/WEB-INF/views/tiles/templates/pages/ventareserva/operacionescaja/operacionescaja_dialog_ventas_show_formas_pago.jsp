<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${selector_motivos}" var="motivosModificacion_xml" />
<x:parse xml="${selector_formas_pago}" var="selectorFormasdePagoPorCanal_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="venta.numerada.tabs.venta_edit.editar.formas.title" />				
	</h4>	
</div>

<div class="modal-body">
	<form id="form_formas_pago_venta_guardar" data-parsley-validate="" class="form-horizontal form-label-left" >
		<input type="hidden" id="xmlPagos" name="xmlPagos" />
		<input type="hidden" id="strMotivo" name="strMotivo" />
		
		<table id="datatable-list-edit_forma_pago_venta" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th><spring:message code="venta.numerada.tabs.venta_formaPago.list.header.num_operacion" /></th>
					<th><spring:message code="venta.numerada.tabs.venta_formaPago.list.header.importe" /></th>
					<th><spring:message code="venta.numerada.tabs.venta_formaPago.list.header.formaPago" /></th>
					<th><spring:message code="venta.numerada.tabs.venta_formaPago.list.header.num_caja" /></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>	
		<div class="modal-footer">			
			<button id="aceptar_formas_button_venta"  class="btn btn-primary">
				<spring:message code="common.button.accept" />
			</button>
			<button type="button" class="btn btn-cancel close_dialog2" data-dismiss="modal">
				<spring:message code="common.button.cancel"/>
			</button>
		</div>		
	</form>
</div>

<script>
hideSpinner("#ver_formas_pago_button");

var json = ${datos_venta};

var dt_listformaspago=$('#datatable-list-edit_forma_pago_venta').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	  initComplete: function( settings, json ) {
	    	window.setTimeout(CargarFormasPago, 100);  
	    },
	scrollCollapse: true,
	ordering:  false,
	paging: false,	
    select: { style: 'os' },
    columns: [
              {},   
              {},
              {},
              {},
              {},
              {}      
              
        ],
        "columnDefs": [
                       { "visible": false, "targets": [0,1]}
                     ],
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('datatable-list-edit_forma_pago_venta');
   	}
    
} ); 

function CargarFormasPago()
{
	var importes = ""+json.ArrayList.Importeparcial;
	if(importes!="undefined")
		{
		if(json.ArrayList.Importeparcial.length>0)
			{
			$.each( json.ArrayList.Importeparcial, function( index, Importeparcial ){
				var idcaja="";
				var idoperacioncaja = "";
				if (Importeparcial.venta !=""){
					idoperacioncaja=Importeparcial.venta.operacioncaja.idoperacioncaja;		
					
					if (Importeparcial.venta.operacioncaja.caja !="")
						idcaja=Importeparcial.venta.operacioncaja.caja.idcaja;
				}else
				{
					idoperacioncaja=	Importeparcial.modificacionimporteparcials.Modificacionimporteparcial.modificacion.operacioncaja.idoperacioncaja
					idcaja=Importeparcial.modificacionimporteparcials.Modificacionimporteparcial.modificacion.operacioncaja.caja.idcaja;
				}
				dt_listformaspago.row.add([
					   						Importeparcial,          
					   						"",		
					   						idoperacioncaja,
					   						Importeparcial.importe, 
					   						Importeparcial.formapago.nombre,         
					   						idcaja			    
					   					]).draw();
			})
			}
		else
			{
			
			var idcaja="";
			if (json.ArrayList.Importeparcial.venta.operacioncaja !="")
				idcaja=json.ArrayList.Importeparcial.venta.operacioncaja.caja.idcaja;
			
			
			dt_listformaspago.row.add([
			   						json.ArrayList.Importeparcial,          
			   						"",	
			   						json.ArrayList.Importeparcial.venta.operacioncaja.idoperacioncaja,
			   						json.ArrayList.Importeparcial.importe, 
			   						json.ArrayList.Importeparcial.formapago.nombre,         
			   					    idcaja									    
			   					]).draw();
			}
		}
}




$("#aceptar_formas_button_venta").on("click", function(e) {	
	$("#form_formas_pago_venta_guardar").validate();
	//El bot�n en la aplicaci�n de Valencia no hace nada
})

$("#form_formas_pago_venta_guardar").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {			
			guardarFormasPagoVenta()			
		}
	}
)

//***************************************************
var xml="";

function guardarFormasPagoVenta()
{
	
	var idVenta = $("#idVentaEditar").val();
	
	xml = "<Modificacion><idmodificacion/><observaciones/>";
	xml+="<motivomodificacion>"+$("#idmotivo_formapago option:selected").text()+"</motivomodificacion>";
	xml+="<venta><idventa>"+idVenta+"</idventa><entradasimpresas>false</entradasimpresas><reciboimpreso>false</reciboimpreso><imprimirSoloEntradasXDefecto/></venta>";
	xml+="<reserva><idreserva/></reserva><anulaciondesdecaja/>";
	xml+="<modificacionlineadetalles><Modificacionlineadetalle><lineadetalle><idlineadetalle/></lineadetalle></Modificacionlineadetalle></modificacionlineadetalles>";
	xml+="<modificacionimporteparcials>";
	
	
	var pagos = dt_listformaspago.rows().data();
	var numero = pagos.length;
	if(numero>0)
		for(i=0;i<numero;i++)
			{
			procesarPago(pagos[i]);		
			}
	else
		procesarPago(pagos)
	
	xml+="</modificacionimporteparcials>";	
	xml+="</Modificacion>";
	$("#xmlPagos").val(xml);
	
	var data = $("#form_formas_pago_venta_guardar").serializeObject();
	
	$.ajax({
		type : "post",
		url : "<c:url value='/ajax/ventareserva/busqueda/save_formas_de_pago_venta.do'/>",
		timeout : 100000,
		data : data,
		success : function(data) {		
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
			$("#modal-dialog-form-2").modal('hide');
		},
		error : function(exception) {
			

			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : exception.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		}
	}); 

}


 function procesarPago(fila)
{

	if(fila[1]=="")
	{
		var jsonFila = fila[0];
		
		
		xml+="<Modificacionimporteparcial>";
		xml+="<tipomodificacion>0</tipomodificacion>";
		xml+="<importeparcial isnew=\"false\" anulado=\"0\">";
		xml+="<idimporteparcial>"+jsonFila.idimporteparcial+"</idimporteparcial>";
		xml+="<formapago>"+json2xml(jsonFila.formapago)+"</formapago>";
		xml+="<importe>"+jsonFila.importe+"</importe>";
		xml+="<modificacionimporteparcials>"+json2xml(jsonFila.modificacionimporteparcials)+"</modificacionimporteparcials>";
		
		
		if (jsonFila.venta!=""){
			xml+="<venta>";
			xml+="<idventa>"+ $("#idVentaEditar").val()+"</idventa>";
			xml+="<operacioncaja>";
			xml+="<idoperacioncaja>"+jsonFila.venta.operacioncaja.idoperacioncaja+"</idoperacioncaja>";
			xml+="<caja>";
			xml+="<idcaja>"+jsonFila.venta.operacioncaja.caja.idcaja+"</idcaja>";
			xml+="</caja>";
			xml+="</operacioncaja>";
			xml+="</venta>";
		}else{
			xml+="<venta/>";
		}	
		
		xml+="<bono/><rts/><numpedido/>";
		xml+="</importeparcial>";			
		xml+="</Modificacionimporteparcial>";
	}
else
	{
		xml+="<Modificacionimporteparcial>";
		xml+="<tipomodificacion>0</tipomodificacion>";
		xml+="<importeparcial isnew=\"true\" anulado=\"0\">";
		xml+="<formapago>";
		xml+="<idformapago>"+fila[1]+"</idformapago>";
		xml+="<nombre></nombre>";
		xml+="</formapago>";
		xml+="<importe>"+fila[3]+"</importe>";
		xml+="<bono><idbono/></bono><rts/><numpedido/><anulado>0</anulado>";
		xml+="</importeparcial>";
		xml+="</Modificacionimporteparcial>";
	}
}
 

//****************************************************
$("#button_new_forma").on("click", function(e) {
	 $("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/venta/numerada/editarVenta/FormasPagoAnadir.do'/>", function() {
			$("#modal-dialog-form-3").modal('show');
			setModalDialogSize("#modal-dialog-form-3", "xs");
		});
});



</script>

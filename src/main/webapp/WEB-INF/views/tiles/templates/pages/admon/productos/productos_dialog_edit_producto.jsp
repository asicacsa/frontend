<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_producto}" var="editar_producto_xml" />
<x:parse xml="${editarproducto_selector_iva}" var="editarproducto_selector_iva_xml" />
<x:parse xml="${editarproducto_selector_tipo_producto}" var="editarproducto_selector_tipo_producto_xml" />
<x:parse xml="${editarproducto_selector_tarifa}" var="editarproducto_selector_tarifa_xml" />
<x:parse xml="${editarproducto_tarifas_no_vigente}" var="editarproducto_tarifas_no_vigente_xml" />

<c:set var="fechainiciovigenciaproducto"><x:out select="$editar_producto_xml/Producto/fechainiciovigencia"/></c:set>
<fmt:parseDate value="${fechainiciovigenciaproducto}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadainiciovigenciaproducto"/>
<c:set var="fechafinvigenciaproducto"><x:out select="$editar_producto_xml/Producto/fechafinvigencia"/></c:set>
<fmt:parseDate value="${fechafinvigenciaproducto}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadafinvigenciaproducto"/>
<c:set var="fechainicioventaproducto"><x:out select="$editar_producto_xml/Producto/fechainicioventa"/></c:set>
<fmt:parseDate value="${fechainicioventaproducto}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadainiciventaproducto"/>
<c:set var="fechafinventaproducto"><x:out select="$editar_producto_xml/Producto/fechafinventa"/></c:set>
<fmt:parseDate value="${fechafinventaproducto}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadafinventaproducto"/>


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<x:choose>
			<x:when select="not($editar_producto_xml/Producto)">
				<spring:message code="administracion.productos.tabs.productos.dialog.crear_producto.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.productos.tabs.productos.dialog.editar_producto.title" />
			</x:otherwise>
		</x:choose>		
	</h4>	
</div>


<div class="modal-body" style="overflow-y:scroll;max-height:80vh;">
	<form id="form_producto_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	   	    <input id="idproducto" name="idproducto" type="hidden" value="<x:out select = "$editar_producto_xml/Producto/idproducto" />" />
			<input id="i18nFieldNombre_producto" name="i18nFieldNombre_producto" type="hidden" value="<x:forEach select="$editar_producto_xml/Producto/i18ns/I18n" var="item"><x:out select="$item/idi18n" />~<x:out select="$item/idioma/ididioma" />~<x:out select="$item/nombre" />^</x:forEach>" />
			<input id="tarifasSeleccionadas" name="tarifasSeleccionadas" type="hidden"/>
			<input id="tarifasBonoSeleccionadas" name="tarifasBonoSeleccionadas" type="hidden"/>

	    <div class="col-md-6 col-sm-6 col-xs-6">
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.productos.field.nombre" />*</label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<input name="nombre" id="nombre" type="text" class="form-control required" required="required" value="<x:out select = "$editar_producto_xml/Producto/nombre" />">
				</div>
			</div>	
			
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.productos.field.descripcion" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<textarea name="descripcion" id="descripcion" class="form-control"  ><x:out select = "$editar_producto_xml/Producto/descripcion" /></textarea>
					</div>
				</div>	


				
				
				
				<div class="form-group date-picker">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.productos.field.vigencia" />*</label>
					<div class="col-md-4 col-sm-4 col-xs-4">
						  <a type="button" class="btn btn-default btn-clear-date" id="button_vigencia_inicio_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="ventareserva.operacionescaja.operacionescaja.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
						  </a>			
                        <div class="input-prepend input-group">
                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                          <input type="text" required="required" class="form-control" name="fechainiciovigencia" value="<fmt:formatDate value="${fechaformateadainiciovigenciaproducto}" pattern="dd/MM/yyyy" type="DATE"/>" readonly/>                          
                        </div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4">
						  <a type="button" class="btn btn-default btn-clear-date" id="button_vigencia_fin_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="ventareserva.operacionescaja.operacionescaja.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
						  </a>			
                        <div class="input-prepend input-group">
                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                          <input type="text" name="fechafinvigencia" class="form-control" value="<fmt:formatDate value="${fechaformateadafinvigenciaproducto}" pattern="dd/MM/yyyy" type="DATE"/>" readonly/>
                        </div>
					</div>
				</div>
			
			
				<div class="form-group date-picker">
						<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.productos.field.venta" />*</label>
						<div class="col-md-4 col-sm-4 col-xs-4">
							  <a type="button" class="btn btn-default btn-clear-date" id="button_venta_inicio_clear">
									<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="ventareserva.operacionescaja.operacionescaja.list.button.clear" />"> <span class="fa fa-trash"></span>
									</span>
							  </a>			
							                     			     
							
	                        <div class="input-prepend input-group">
	                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	                          <input type="text" required="required" name="fechainicioventa" class="form-control" value="<fmt:formatDate value="${fechaformateadainiciventaproducto}" pattern="dd/MM/yyyy" type="DATE"/>" readonly/>	                          
	                        </div>	                        
	                        
						</div>
						
						<div class="col-md-4 col-sm-4 col-xs-4">
							  <a type="button" class="btn btn-default btn-clear-date" id="button_venta_fin_clear">
									<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="ventareserva.operacionescaja.operacionescaja.list.button.clear" />"> <span class="fa fa-trash"></span>
									</span>
							  </a>			
							
	                        	                        
	                        <div class="input-prepend input-group">
	                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	                          <input type="text" name="fechafinventa" class="form-control" value="<fmt:formatDate value="${fechaformateadafinventaproducto}" pattern="dd/MM/yyyy" type="DATE"/>" readonly/>
	                        </div>
						</div>
						
					</div>
				
			
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.productos.field.iva" />*</label>
						<div class="col-md-5 col-sm-5 col-xs-5">
							<select name="idiva" id="selector_iva" class="form-control" required="required">
								<option value=""></option>
								<x:forEach select="$editarproducto_selector_iva_xml/ArrayList/Iva" var="item">
									<option value="<x:out select="$item/idiva" />"><x:out select="$item/nombre" /></option>
								</x:forEach>
							</select>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-3"></div>						
					</div>	

						
			</div>	
			<div class="col-md-6 col-sm-6 col-xs-6">
			
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.productos.field.orden" /></label>
						<div class="col-md-2 col-sm-2 col-xs-2">
							<input type="text" name="orden" id="orden" data-inputmask="'mask': '9{0,10}'" class="form-control"  value="<x:out select = "$editar_producto_xml/Producto/orden" />">
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6"></div>
					</div>			

					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.productos.field.tipo.producto" />*</label>
						<div class="col-md-8 col-sm-8 col-xs-8">
							<select name="tipoproducto[]" id="selector_tipoproducto" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%" required="required">
								<x:forEach select="$editarproducto_selector_tipo_producto_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
							</select>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.productos.field.disponible.bono" /></label>
						<div class="col-md-2 col-sm-2 col-xs-2">	
						   	<input name="disponibleparabono" type="checkbox" class="js-switch" <x:if select="$editar_producto_xml/Producto/disponibleparabono='1'">checked=""</x:if> data-switchery="true" style="display: none;">
						</div>
						<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="administracion.productos.tabs.productos.field.principal" /></label>
						<div class="col-md-2 col-sm-2 col-xs-2">	
						   	<input name="principal" type="checkbox" class="js-switch" <x:if select="$editar_producto_xml/Producto/principal='1'">checked=""</x:if> data-switchery="true" style="display: none;">
						</div>				
						<div class="col-md-2 col-sm-2 col-xs-2"></div>		
					</div>	
					
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.productos.field.imprimirEntradasXDefecto" /></label>
						<div class="col-md-8 col-sm-8 col-xs-8">	
						   	<input name="imprimirEntradasXDefecto" type="checkbox" class="js-switch" <x:if select="$editar_producto_xml/Producto/imprimirEntradasXDefecto='1'">checked=""</x:if> data-switchery="true" style="display: none;">
						</div>
					</div>				
			</div>
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				   <div class="col-md-6 col-sm-6 col-xs-6">
						<select class="form-control" name="tipoTarifas" id="tipoTarifas">
							<option value="0" selected="selected"><spring:message
									code="administracion.productos.tabs.contenidos.field.tarifasvigentes" /></option>
							<option value="1"><spring:message
									code="administracion.productos.tabs.contenidos.list.tarifasnovigentes" /></option>
						</select>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="btn-group pull-right btn-datatable">
							<a type="button" class="btn btn-info" id="tab_tarifas_productos_asignar">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.productos.tarifas.button.asignar" />"> <span class="fa fa-euro"></span>
								</span>
							</a>
							<a type="button" class="btn btn-info" id="tab_tarifas_productos_new">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.productos.tarifas.button.nuevo" />"> <span class="fa fa-plus"></span>
								</span>
							</a>
							<a type="button" class="btn btn-info" id="tab_tarifas_productos_edit">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.productos.tarifas.button.editar" />"> <span class="fa fa-pencil"></span>
								</span>
							</a>
							<a type="button" class="btn btn-info" id="tab_tarifas_productos_remove">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.productos.tarifas.button.eliminar" />"> <span class="fa fa-trash"></span>
								</span>
							</a>		
						</div>							
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 vigente" hidden>
						<table id="table-listaProductostarifas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
							<thead>
								<tr>	
									<th class="id_tarifa_producto" ></th>
									<th class="id_tarifa"></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.nombretarifa" /></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.fechainicio" /></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.fechafin" /></th>				
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.importe" /></th>							
								</tr>
							</thead>
							<tbody>
								<x:forEach select="$editar_producto_xml/Producto/tarifaproductos/Tarifaproducto" var="item">
									<tr>
										<td><x:out select="$item/idtarifaproducto" /></td>
										<td><x:out select="$item/tarifa/idtarifa" /></td>
										<td><x:out select="$item/tarifa/nombre" /></td> 
										<td>
											<c:set var="fechainicionovigencia"><x:out select="$item/fechainiciovigencia"/></c:set>
											<fmt:parseDate value="${fechainicionovigencia}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadainicionovigencia"/>
											<fmt:formatDate value="${fechaformateadainicionovigencia}" pattern="dd/MM/yyyy" type="DATE"/>                      			     
	                          			</td>  										 
										<td>
											<c:set var="fechafinnovigencia"><x:out select="$item/fechafinvigencia"/></c:set>
											<fmt:parseDate value="${fechafinnovigencia}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadafinovigencia"/>
											<fmt:formatDate value="${fechaformateadafinovigencia}" pattern="dd/MM/yyyy" type="DATE"/>                      			     
	                          			</td>										
										<td><x:out select="$item/importe" /></td>
									</tr>
								</x:forEach>
							</tbody>
						</table>
					</div>			
					<div class="col-md-12 col-sm-12 col-xs-12 noVigente" hidden>
						<table id="table-listaProductostarifas-novigentes" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%" >
							<thead>
								<tr>	
									<th class="id_tarifa_producto" ></th>
									<th class="id_tarifa"></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.nombretarifa" /></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.fechainicio" /></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.fechafin" /></th>				
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.importe" /></th>							
								</tr>
							</thead>
							<tbody>								
								<x:forEach select="$editarproducto_tarifas_no_vigente_xml/ArrayList/Tarifaproducto" var="item">
									<tr>
										<td><x:out select="$item/idtarifaproducto" /></td>
										<td><x:out select="$item/tarifa/idtarifa" /></td>
										<td><x:out select="$item/tarifa/nombre" /></td> 
										<td>
											<c:set var="fechainicionovigencia"><x:out select="$item/fechainiciovigencia"/></c:set>
											<fmt:parseDate value="${fechainicionovigencia}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadainicionovigencia"/>
											<fmt:formatDate value="${fechaformateadainicionovigencia}" pattern="dd/MM/yyyy" type="DATE"/>                      			     
	                          			</td>  										 
										<td>
											<c:set var="fechafinnovigencia"><x:out select="$item/fechafinvigencia"/></c:set>
											<fmt:parseDate value="${fechafinnovigencia}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadafinovigencia"/>
											<fmt:formatDate value="${fechaformateadafinovigencia}" pattern="dd/MM/yyyy" type="DATE"/>                      			     
	                          			</td>										
										<td><x:out select="$item/importe" /></td>
									</tr>
								</x:forEach>										
																							
							</tbody>
						</table>
					</div>
											
			</div>
			
			<div class="col-md-12 col-sm-12 col-xs-12">
			<br/>
				<div class="form-group">	
				
					<div class="col-md-6 col-sm-6 col-xs-6">
						<select class="form-control" name="tipoBonoTarifas" id="tipoBonoTarifas">
							<option value="0" selected="selected"><spring:message
									code="administracion.productos.tabs.productos.field.tarifasbono" /></option>
							<option value="1"><spring:message
									code="administracion.productos.tabs.productos.list.tarifasbononovigentes" /></option>
						</select>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="btn-group pull-right btn-datatable">
							<a type="button" class="btn btn-info" id="tab_tarifas_bono_productos_asignar">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.productos.tarifas.button.asignar" />"> <span class="fa fa-euro"></span>
								</span>
							</a>
							<a type="button" class="btn btn-info" id="tab_tarifas_bono_productos_new">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.productos.tarifas.button.nuevo" />"> <span class="fa fa-plus"></span>
								</span>
							</a>
							<a type="button" class="btn btn-info" id="tab_tarifas_bono_productos_edit">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.productos.tarifas.button.editar" />"> <span class="fa fa-pencil"></span>
								</span>
							</a>
							<a type="button" class="btn btn-info" id="tab_tarifas_bono_productos_remove">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.productos.tarifas.button.eliminar" />"> <span class="fa fa-trash"></span>
								</span>
							</a>		
						</div>							
					</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 Bonovigente" hidden>
						<table id="table-listaProductostarifasBono" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
							<thead>
								<tr>	
									<th class="id_tarifa_producto" ></th>
									<th class="id_tarifa"></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.nombretarifa" /></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.fechainicio" /></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.fechafin" /></th>				
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.importe" /></th>							
								</tr>
							</thead>
							<tbody>
								<x:forEach select="$editar_producto_xml/Producto/tarifaproductosbono/Tarifaproducto" var="item">
									<tr>
										<td><x:out select="$item/idtarifaproducto" /></td>
										<td><x:out select="$item/tarifa/idtarifa" /></td>
										<td><x:out select="$item/tarifa/nombre" /></td> 
										<td>
											<c:set var="fechainicionovigencia"><x:out select="$item/fechainiciovigencia"/></c:set>
											<fmt:parseDate value="${fechainicionovigencia}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadainicionovigencia"/>
											<fmt:formatDate value="${fechaformateadainicionovigencia}" pattern="dd/MM/yyyy" type="DATE"/>                      			     
	                          			</td>  										 
										<td>
											<c:set var="fechafinnovigencia"><x:out select="$item/fechafinvigencia"/></c:set>
											<fmt:parseDate value="${fechafinnovigencia}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadafinovigencia"/>
											<fmt:formatDate value="${fechaformateadafinovigencia}" pattern="dd/MM/yyyy" type="DATE"/>                      			     
	                          			</td>										
										<td><x:out select="$item/importe" /></td>
									</tr>
								</x:forEach>
							</tbody>
						</table>
					</div>	
					<div class="col-md-12 col-sm-12 col-xs-12 noVigentesBono" hidden>
						<table id="table-listaProductostarifasBono-novigentes" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%" >
							<thead>
								<tr>	
									<th class="id_tarifa_producto" ></th>
									<th class="id_tarifa"></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.nombretarifa" /></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.fechainicio" /></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.fechafin" /></th>				
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.importe" /></th>							
								</tr>
							</thead>
							<tbody>								
								<x:forEach select="$editarproducto_tarifas_no_vigente_xml/ArrayList/Tarifaproducto" var="item">
									<tr>
										<td><x:out select="$item/idtarifaproducto" /></td>
										<td><x:out select="$item/tarifa/idtarifa" /></td>
										<td><x:out select="$item/tarifa/nombre" /></td> 
										<td>
											<c:set var="fechainicionovigencia"><x:out select="$item/fechainiciovigencia"/></c:set>
											<fmt:parseDate value="${fechainicionovigencia}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadainicionovigencia"/>
											<fmt:formatDate value="${fechaformateadainicionovigencia}" pattern="dd/MM/yyyy" type="DATE"/>                      			     
	                          			</td>  										 
										<td>
											<c:set var="fechafinnovigencia"><x:out select="$item/fechafinvigencia"/></c:set>
											<fmt:parseDate value="${fechafinnovigencia}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadafinovigencia"/>
											<fmt:formatDate value="${fechaformateadafinovigencia}" pattern="dd/MM/yyyy" type="DATE"/>                      			     
	                          			</td>										
										<td><x:out select="$item/importe" /></td>
									</tr>
								</x:forEach>										
																							
							</tbody>
						</table>
					</div>
							
				
			</div>		
		
	
	</form>

	<div class="modal-footer">

		<button id="save_ubicacion_button" type="button" class="btn btn-primary save_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>	
</div>	

<div class="modal fade" id="modal_atributos" tabindex="-1"
	role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static">
	<div class="modal-dialog">
		

			<div class="modal-header">
				<button type="button" class="close close_dialog" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">�</span>
				</button>
				<h4 class="modal-title">
					<spring:message code="administracion.productos.tabs.productos.atributos.title" />			
				</h4>
			</div>
			
			<div class="modal-body">
				<form id="producto_tarifa">
				    <input type="hidden" id="idTipoProductoAtributo" />
				    
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.tipoproductos.field.nombre" />*</label>
						<div class="col-md-8 col-sm-8 col-xs-8">
							<input type="text" name="nombreAtributo" id="nombreAtributo" class="form-control" value=""/>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.tipoproductos.field.tipo" />*</label>
						<div class="col-md-8 col-sm-8 col-xs-8">
							<select name="idatributo" id="selector_atributo" class="form-control" required="required">
															
							</select>
						</div>
					</div>				    
				</form>
				<div class="modal-footer">
					<button type="button" class="btn btn-success close_atributo"
						data-dismiss="modal">
						<spring:message code="common.button.ok" />
					</button>
					<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
						<spring:message code="common.button.cancel" />
					</button>					
				</div>				
			</div>		
	</div>
</div>

<script>
$(":input").inputmask();

//*********************************************
hideSpinner("#tab_productos_new");
hideSpinner("#tab_productos_edit");

//********************************************


//Para saber cuantos tipos de productos hay seleccionados 
var contadorProductos=0;	
	var selectedValues = new Array();
	<x:forEach select="$editar_producto_xml/Producto/tipoprodproductos/Tipoprodproducto/tipoproducto" var="item">
	if (contadorProductos==0)
		tipos_productos="<x:out select="$item/nombre"/>";
	else	
		tipos_productos=tipos_productos+"-<x:out select="$item/nombre"/>";
		contadorProductos++;
	</x:forEach>
	
	if (contadorProductos > 1 ){
		$("#tab_tarifas_productos_asignar").show();
		$("#tab_tarifas_bono_productos_asignar").show();
		
	}
	else{
		$("#tab_tarifas_productos_asignar").hide();
		$("#tab_tarifas_bono_productos_asignar").hide();
	}



//***********************************************
function limpiarModalAtributo(datatableNew)
{
	datatableAtributo = datatableNew;
	$("#idTipoProductoAtributo").val("");
	$("#idatributo").val("");
	$("#nombreAtributo").val("");
	$("#modal_atributos").modal('show');
	setModalDialogSize("#modal_atributos", "sm");	
}

function editarModalAtributo(datatableNew)
{
	datatableAtributo = datatableNew;
	var datos = datatableAtributo.rows( '.selected' ).data()[0];
	var creando = false;
	$("#idTipoProductoAtributo").val(datos[0]);
	$("#selector_atributo").val(datos[1]);
	$("#nombreAtributo").val(datos[2]);
	$("#modal_atributos").modal('show');
	setModalDialogSize("#modal_atributos", "sm");	
}

$(".close_atributo").on("click", function(e) { 
	
	var idTipoProductoAtributo = $("#idTipoProductoAtributo").val();
	
	if(idTipoProductoAtributo!='')
		datatableAtributo.rows( '.selected' ).remove(); 
		
	datatableAtributo.row.add( [  $("#idTipoProductoAtributo").val(),$("#selector_atributo").val(),$("#nombreAtributo").val(), $("#selector_atributo option:selected").text()] )
	    	.draw();
	$("#modal_atributos").modal('hide');
		
})





$("#nombre").i18nField("#i18nFieldNombre_producto",<c:out value="${sessionScope.i18nlangs}"  escapeXml="false" />);


$('#selector_iva option[value="<x:out select = "$editar_producto_xml/Producto/iva/idiva" />"]').attr("selected", "selected");

$(".select-select2").select2({
	language : "es",
	containerCssClass : "single-line"
});

$(".js-switch").each(function(index) {
	var s = new Switchery(this, {
		size : 'small'
	});
});


/*Para convertir la tabla a DataTable*/
var dttarifas,dttarifasbono,dtproductostarifasNoVigentes,dtproductostarifasBonoNoVigentes;

function autoajuste() {
	$(".vigente").show();
	dtproductostarifas.columns.adjust().draw();
	$(".Bonovigente").show();
	dttarifasbono.columns.adjust().draw();	
}




$(document).ready(function() {
	dtproductostarifas=$('#table-listaProductostarifas').DataTable( {
		"scrollY":        "150px",
        "scrollCollapse": true,
        "paging":         false, 	
         "info": false,
         "searching": false,
         select: { style: 'os'},
         language: dataTableLanguage,
         initComplete: function( settings, json ) {
            	window.setTimeout(autoajuste, 1500);
        	 	
       		},
         "columnDefs": [
                         { "visible": false, "targets": [0,1]}
                       ]
         });
	
	
	dttarifasbono=$('#table-listaProductostarifasBono').DataTable( {
		"scrollY":        "150px",
        "scrollCollapse": true,
        "paging":         false, 	
         "info": false,
         "searching": false,
         select: { style: 'os'},
         language: dataTableLanguage,
                
         "columnDefs": [
                         { "visible": false, "targets": [0,1]}
                       ]
         });	
	
	dtproductostarifasNoVigentes=$('#table-listaProductostarifas-novigentes').DataTable( {
		"scrollY":        "150px",
        "scrollCollapse": true,
        "paging":         false, 	
         "info": false,
         "searching": false,
         select: { style: 'os'},
         language: dataTableLanguage,          
         "columnDefs": [
                         { "visible": false, "targets": [0,1]}
                       ]
         });
	
	dtproductostarifasBonoNoVigentes=$('#table-listaProductostarifasBono-novigentes').DataTable( {
		"scrollY":        "150px",
        "scrollCollapse": true,
        "paging":         false, 	
         "info": false,
         "searching": false,
         select: { style: 'os'},
         language: dataTableLanguage,          
         "columnDefs": [
                         { "visible": false, "targets": [0,1]}
                       ]
         });
	
	
} );

var selectedValues = new Array();
<x:forEach select="$editar_producto_xml/Producto/tipoprodproductos/Tipoprodproducto/tipoproducto" var="item">
selectedValues.push('<x:out select="$item/idtipoproducto" />');
</x:forEach>

<x:forEach select="$editarproducto_selector_tarifa_xml/ArrayList/LabelValue" var="item">
$("#selector_tarifa").append('<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>');
</x:forEach>


$('#selector_tipoproducto').val(selectedValues);
$('#selector_tipoproducto').trigger('change.select2');


$today= moment().format("DD/MM/YYYY");
$tomorrow = moment().add("days",1).format("DD/MM/YYYY");




$("#button_vigencia_inicio_clear").on("click", function(e) {
    $('input[name="fechainiciovigencia"]').val('');   
});

$("#button_vigencia_fin_clear").on("click", function(e) {
    $('input[name="fechafinvigencia"]').val('');   
});


$("#button_venta_inicio_clear").on("click", function(e) {
    $('input[name="fechainicioventa"]').val('');   
});

$("#button_venta_fin_clear").on("click", function(e) {
    $('input[name="fechafinventa"]').val('');   
});




 $('input[name="fechainiciovigencia"]').daterangepicker({
	    singleDatePicker: true,		
	    showDropdowns: true,
	  	locale: $daterangepicker_sp
	});	 
 
 $('input[name="fechafinvigencia"]').daterangepicker({
	    singleDatePicker: true,	
	    showDropdowns: true,
	  	locale: $daterangepicker_sp
	});	

	 
 $('input[name="fechainicioventa"]').daterangepicker({
	    singleDatePicker: true,		
	    showDropdowns: true,
	  	locale: $daterangepicker_sp
	});	 
 
 $('input[name="fechafinventa"]').daterangepicker({
	    singleDatePicker: true,		
	    showDropdowns: true,
	  	locale: $daterangepicker_sp
	});	
 
 
 $('input[name="fechainicioventa"]').val("<fmt:formatDate value="${fechaformateadainiciventaproducto}" pattern="dd/MM/yyyy" type="DATE"/> "); 
 $('input[name="fechafinventa"]').val("<fmt:formatDate value="${fechaformateadafinventaproducto}" pattern="dd/MM/yyyy" type="DATE"/> ");
 $('input[name="fechainiciovigencia"]').val("<fmt:formatDate value="${fechaformateadainiciovigenciaproducto}" pattern="dd/MM/yyyy" type="DATE"/> "); 
 $('input[name="fechafinvigencia"]').val("<fmt:formatDate value="${fechaformateadafinvigenciaproducto}" pattern="dd/MM/yyyy" type="DATE"/> ");
	

$("#tab_tarifas_productos_new").on("click", function(e) { 	
	limpiarModaltarifas(dtproductostarifas);
})

$("#tab_tarifas_bono_productos_new").on("click", function(e) { 	
	limpiarModaltarifas(dttarifasbono);
})


$("#tab_tarifas_bono_productos_edit").on("click", function(e) { 
	
	if(dttarifasbono.rows( { selected: true } ).data().length==1)		
    {
		editarModaltarifas(dttarifasbono);
    }
    else
    	{
    	if(dttarifasbono.rows( { selected: true } ).data().length==0)
	    	{
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="administracion.productos.tabs.productos.tarifas.list.alert.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
	    	}
	    	
		else
			{
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="administracion.productos.tabs.productos.tarifas.list.alert.seleccion_multiple" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			}
		}

	
	
})	


$("#tab_tarifas_productos_edit").on("click", function(e) { 
	
	if(dtproductostarifas.rows( { selected: true } ).data().length==1)		
    {
		editarModaltarifas(dtproductostarifas);
    }
    else
    	{
    	if(dtproductostarifas.rows( { selected: true } ).data().length==0)
	    	{
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="administracion.productos.tabs.productos.tarifas.list.alert.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
	    	}
	    	
		else
			{
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="administracion.productos.tabs.productos.tarifas.list.alert.seleccion_multiple" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			}
		}

	
	
})	


$("#tab_tarifas_productos_remove").on("click", function(e) { 
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="administracion.productos.tabs.productos.tarifas.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
		  buttons: { closer: false, sticker: false	},
		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
			dtproductostarifas
		    .rows( '.selected' )
		    .remove()
		    .draw();
	   })
	
	})

$("#tab_tarifas_bono_productos_remove").on("click", function(e) { 
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="administracion.productos.tabs.productos.tarifas.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
		  buttons: { closer: false, sticker: false	},
		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
		   dttarifasbono
		    .rows( '.selected' )
		    .remove()
		    .draw();
	   })
	
	})


	$( "#tipoTarifas").change(function() {
					if($("#tipoTarifas").val()==0){
                    	$(".vigente").show();
                    	$(".noVigente").hide();
                    	                    	
                    	if (contadorProductos > 1 )
                    		$("#tab_tarifas_productos_asignar").show();                    	
                    	else
                    		$("#tab_tarifas_productos_asignar").hide();
                    	
                    	$("#tab_tarifas_productos_new").show();
	                    $("#tab_tarifas_productos_edit").show();
	                    $("#tab_tarifas_productos_remove").show();	                	
                    }
                    
                    else{
                    	$(".vigente").hide();
                    	$(".noVigente").show();
                    	dtproductostarifasNoVigentes.columns.adjust().draw();
                    	$("#tab_tarifas_productos_asignar").hide();
                        $("#tab_tarifas_productos_new").hide();
	                    $("#tab_tarifas_productos_edit").hide();
	                    $("#tab_tarifas_productos_remove").hide();                  	
                    }
	})
	
	
	
	$( "#tipoBonoTarifas").change(function() {
					if($("#tipoBonoTarifas").val()==0){
                    	$(".Bonovigente").show();
                    	$(".noVigentesBono").hide();
                    	
                    	
                    	if (contadorProductos > 1 )
                    		$("#tab_tarifas_bono_productos_asignar").show();                    	
                    	else
                    		$("#tab_tarifas_bono_productos_asignar").hide();
                    	
                        $("#tab_tarifas_bono_productos_new").show();
	                    $("#tab_tarifas_bono_productos_edit").show();
	                    $("#tab_tarifas_bono_productos_remove").show();	                	
                    }
                    
                    else{
                    	$(".Bonovigente").hide();
                    	$(".noVigentesBono").show();
                    	dtproductostarifasBonoNoVigentes.columns.adjust().draw();
                    	$("#tab_tarifas_bono_productos_asignar").hide();
                        $("#tab_tarifas_bono_productos_new").hide();
	                    $("#tab_tarifas_bono_productos_edit").hide();
	                    $("#tab_tarifas_bono_productos_remove").hide();                  	
                    }
	})
		
	
	$(".save_dialog").on("click", function(e) { 
		
		 // Procesamos las tarifas
		 var data = dtproductostarifas.rows().data();
		 var tarifas = "";
		 data.each(function (value, index) {
			var linea = ""+dtproductostarifas.rows( index ).data()[0][0]+"|";
			linea = linea + dtproductostarifas.rows( index ).data()[0][1]+"|";
			linea = linea + dtproductostarifas.rows( index ).data()[0][3]+"|";
			linea = linea + dtproductostarifas.rows( index ).data()[0][4]+"|";
			linea = linea + dtproductostarifas.rows( index ).data()[0][5];
			if(tarifas!="")
				tarifas = tarifas + "$";
			tarifas = tarifas + linea;				
		 });
		 $("#tarifasSeleccionadas").val(tarifas);
		 
		 
		 // Procesamos las tarifas bono
		 var data = dttarifasbono.rows().data();
		 tarifas = "";
		 data.each(function (value, index) {
			var linea = ""+dttarifasbono.rows( index ).data()[0][0]+"|";
			linea = linea + dttarifasbono.rows( index ).data()[0][1]+"|";
			linea = linea + dttarifasbono.rows( index ).data()[0][3]+"|";
			linea = linea + dttarifasbono.rows( index ).data()[0][4]+"|";
			linea = linea + dttarifasbono.rows( index ).data()[0][5];
			if(tarifas!="")
				tarifas = tarifas + "$";
			tarifas = tarifas + linea;				
		 });
		 $("#tarifasBonoSeleccionadas").val(tarifas);		 
		 
		 
		 $("#form_producto_data").submit();			 
				
	})
	
		$("#form_producto_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			guardarProducto()
		}
	}
	);
	
	
	function guardarProducto()
	{
		var data = $("#form_producto_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/productos/productos/save_producto.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				
				$("#modal-dialog-form").modal('hide');
				
				dt_listproductos.ajax.reload(null,false);
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});

	}
		
//*******************************************************************

$("#tab_tarifas_productos_asignar").on("click", function(e) {
	showButtonSpinner("#tab_tarifas_productos_asignar");
	var idbono="0";
	var idproducto=$("#idproducto").val();
						
	var url = "<c:url value='/ajax/admon/productos/productos/asignar_tarifas.do'/>?"+encodeURI("idbono="+idbono+"&idproducto="+idproducto+"&cabecera="+tipos_productos);
		
	$("#modal-dialog-form-2 .modal-content:first").load(url, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "lg");
	});
	
});

//*******************************************************************

$("#tab_tarifas_bono_productos_asignar").on("click", function(e) {
	showButtonSpinner("#tab_tarifas_bono_productos_asignar");
	var idbono="1";
	var idproducto=$("#idproducto").val();
						
	var url = "<c:url value='/ajax/admon/productos/productos/asignar_tarifas.do'/>?"+encodeURI("idbono="+idbono+"&idproducto="+idproducto+"&cabecera="+tipos_productos);
		
	$("#modal-dialog-form-2 .modal-content:first").load(url, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "lg");
	});
	
});

	
</script>



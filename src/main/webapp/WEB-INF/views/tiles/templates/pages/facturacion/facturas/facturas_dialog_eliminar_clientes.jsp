<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${motivos_bloqueo}" var="motivos_bloqueo_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">
	     	<spring:message code="facturacion.facturas.tabs.clientes.dialog.eliminar.title" />		 		 
	</h4>	
</div>


<div class="modal-body">
			<div class="col-md-12 col-sm-12 col-xs-12">
			<form id="form-eliminar-cliente" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
			   <input type="hidden" id="ids" name="ids" value="${ids}"/>
			   <input type="hidden" id="motivo" name="motivo" value=""/>			
			
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.bloqueo.field.motivos" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="motivos_bloqueo" id="motivos_bloqueo" class="form-control">
						<x:forEach select="$motivos_bloqueo_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>					
				</div>				
			</div>
			</form>
			<br/><br/>
		</div>
		
		
		<div class="modal-footer">	
			<button id="eliminar_cliente" type="button" class="btn btn-primary eliminar_cliente">
				<spring:message code="common.button.accept" />
			</button>	
			<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
				<spring:message code="common.button.cancel"/>
			</button>
		</div>	
</div>
<script>

$( "#eliminar_cliente" ).on("click", function(e) {	
	var motivo =  $("#motivos_bloqueo option:selected").text();

	$("#motivo").val(motivo);

	var data = $("#form-eliminar-cliente").serializeObject();

	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/facturacion/facturas/cliente/eliminar.do'/>",
		timeout : 100000,
		data : data,
		success : function(data) {
	
			dt_listclientes.ajax.reload();	
			$("#modal-dialog-form").modal('hide');	
		},
		error : function(exception) {
			dt_listclientes.processing(false);
			
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });		
			$("#modal-dialog-form").modal('hide');	
		}
	});
})
</script>
	
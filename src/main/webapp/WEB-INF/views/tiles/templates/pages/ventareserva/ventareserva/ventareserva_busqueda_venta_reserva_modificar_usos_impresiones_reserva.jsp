<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${usos_impresiones_reserva}" var="usos_impresiones_reserva_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal"
		aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_reserva.title" /> ${id}				
	</h4>
</div>	

	<form id="form_usos" class="form-horizontal form-label-left">
		<input type="hidden" name="idReserva"  id="idReserva" value="${id}"/>
		<input type="hidden" name="start"  id="start" value="0"/>
		<input type="hidden" name="length" value="25"/>
	</form>

	<div class="modal-body">				
			<div class="btn-group pull-right btn-datatable">
					<a type="button" class="btn btn-info" id="tab_entradas_reserva_impresiones">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.button.impresiones" />"> <span class="fa fa-mail-reply"></span>
						</span>
					</a>	
					<a type="button" class="btn btn-info" id="tab_entradas_reserva_modificar_usos">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.button.modificar" />"> <span class="fa fa-pencil"></span>
						</span>
					</a>
					<a type="button" class="btn btn-info" id="tab_entradas_reserva_imprimir">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.button.imprimir" />"> <span class="fa fa-print"></span>
						</span>
					</a>	
					<a type="button" class="btn btn-info" id="tab_entradas_reserva_reimprimir">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.button.reimprimir" />"> <span class="fa fa-paint-brush"></span>
						</span>
					</a>
			</div>
						
			
			<table id="tab_entradasReserva" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th></th>
						<th id="identrada"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.ref_entrada" /></th>
						<th id="idventa"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.ref_venta" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.ref_reserva" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.ref_bono" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.producto" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.contenido" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.fecha" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.hora" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.recinto" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.usos" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.impr" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.inhab" /></th>
					</tr>
				</thead>
				<tbody>					
				</tbody>				
			</table>
		
		
		
			
		<div class="modal-footer">
		
			<button id="actualizar_usos" type="button" class="btn btn-success">
					<spring:message code="common.button.accept" />
			</button>
		
			<button type="button" class="btn btn-success close_dialog"
				data-dismiss="modal">
				<spring:message code="common.button.cancel" />
			</button>
		</div>

	</div>	
	
	<script>
var dtentradasReserva;
//*****************************************************
hideSpinner("#tab_busqueda_usos_impresiones_reserva");
//*****************************************************
$(document).ready(function() {
	dtentradasReserva=$('#tab_entradasReserva').DataTable( {
			serverSide: true,
			ordering: false,
			pagingType: "simple",
			"pageLength": 25,
			info: false,
		    ajax: {
		        url: "<c:url value='/ajax/ventareserva/busqueda/ventareservas/list_entrada_por_reserva.do'/>",
		        rowId: 'identrada',
		        type: 'POST',
		        dataSrc: function (json) { 
		        	json_usos =json;
		        	if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Entrada)); return(""); },
		        data: function (params) {
		        	$('#form_usos input[name="start"]').val(params.start);
		
	       	     	return($("#form_usos").serializeObject()); },
		       error: function (xhr, error, code)
		            {
		               	new PNotify({
							title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
							text : xhr.responseText,
							type : "error",
							delay : 5000,
							buttons : {
								closer : true,
								sticker : false
							}					
						});
		            }
		    },
			initComplete: function( settings, json ) {
				$("#tab_entradasReserva_length").hide();	    	
			},
			columns: [
			         { data: null, type: "spanish-string", defaultContent: "", render: function ( data, type, row, meta ) {return data; }}, 
			         { data: "identrada", type: "spanish-string", defaultContent: ""},
			         { data: "lineadetalle.venta.idventa", type: "spanish-string", defaultContent: ""},
			         { data: "idReserva", type: "spanish-string" ,  defaultContent:"${id}"} ,
			         { data: "idBono", type: "spanish-string" ,  defaultContent:""} ,
			         { data: "lineadetalle.producto.nombre", type: "spanish-string" ,  defaultContent:""} ,
			         { data: "entradatornoses.Entradatornos", className: "cell_centered",
			          	  render: function ( data, type, row, meta ) { 
			                  var contenidos="";
			                  //Si esta anulada cogenos la eliminada
			                  if(""+data=="undefined")
			                	  {
			                	  data = row.entradatornoseseliminados.Entradatornos;
			                	  }
			                	  
			                  if(data.length>0)
			                	  {
				                	  for (var j=0; j<data.length; j++)
				                	  {
				                	  var entradaTornos = data[j];
				                	  if(j>0)
				                		  contenidos += "<br/>";
				                	  contenidos+=""+entradaTornos.sesion.contenido.nombre;
				                	  }
			                	  }
			                  else		                	  
			                	  contenidos = data.sesion.contenido.nombre;
			                	  
			                 
			          		   return contenidos; 
			          		  }	
			           }, 
			           { data: "entradatornoses.Entradatornos", className: "cell_centered",
				          	  render: function ( data, type, row, meta ) { 
				                  var contenidos="";
				                //Si esta anulada cogenos la eliminada
				                  if(""+data=="undefined")
				                	  {
				                	  data = row.entradatornoseseliminados.Entradatornos;
				                	  }
				                  if(data.length>0)
			                	  {
				                	  for (var j=0; j<data.length; j++)
				                	  {
				                	  var entradaTornos = data[j];
				                	  if(j>0)
				                		  contenidos += "<br/>";
				                	  contenidos+=""+entradaTornos.sesion.fecha.split("-")[0];
				                	  }  
			                	  }
				                  else
				                	  contenidos+=""+data.sesion.fecha.split("-")[0];
				                  
				          		   return contenidos; 
				          		  }	
				           }, 
				           { data: "entradatornoses.Entradatornos", className: "cell_centered",
					          	  render: function ( data, type, row, meta ) { 
					                  var contenidos="";
					                //Si esta anulada cogenos la eliminada
					                  if(""+data=="undefined")
					                	  {
					                	  data = row.entradatornoseseliminados.Entradatornos;
					                	  }
					                  if(data.length>0)
				                	  {
					                  for (var j=0; j<data.length; j++)
					                	  {
					                	  var entradaTornos = data[j];
					                	  if(j>0)
					                		  contenidos += "<br/>";
					                	  contenidos+=""+entradaTornos.sesion.horainicio;
					                	  }
				                	  }
					                  else
					                	  contenidos+=""+data.sesion.horainicio;
					          		  return contenidos; 
					          		 
					                	  
					          	  }
				           
					           } , 
			           { data: "entradatornoses.Entradatornos", className: "cell_centered",
				          	  render: function ( data, type, row, meta ) { 
				                  var contenidos="";
				                //Si esta anulada cogenos la eliminada
				                  if(""+data=="undefined")
				                	  {
				                	  data = row.entradatornoseseliminados.Entradatornos;
				                	  }
				                  if(data.length>0)
			                	  {
				                  for (var j=0; j<data.length; j++)
				                	  {
				                	  var entradaTornos = data[j];
				                	  if(j>0)
				                		  contenidos += "<br/>";
				                	  contenidos+=""+entradaTornos.sesion.contenido.tipoproducto.recinto.nombre;
				                	  }
			                	  }
				                  else
				                	  contenidos+=""+data.sesion.contenido.tipoproducto.recinto.nombre;
				          		   return contenidos; 
				          		  }	
				           }, 
		           { data: "entradatornoses.Entradatornos", className: "cell_centered",
			          	  render: function ( data, type, row, meta ) { 
			                  var contenidos="";
			                  if(""+data=="undefined")
			                	  return "";
			                  if(data.length>0)
			                	  {
			                	  for (var j=0; j<data.length; j++)
				                	  {
				                	  var entradaTornos = data[j];
				                	  if(j>0)
				                		  contenidos += "<br/>";
				                	  contenidos+='<input class="usos_impresiones" length="3" type="text" id="entrada'+entradaTornos.identradasvalidas+'" value="'+entradaTornos.usos+'"/>';
				                	  }  
			                	  }
			                  else
			                	  {
			                	  contenidos+='<input class="usos_impresiones" length="3" type="text" id="entrada'+data.identradasvalidas+'" value="'+data.usos+'"/>';
			                	  }
			                  
			          		   return contenidos; 
			          		  }	
			           }, 
			           { data: "impresionentradases", className: "cell_centered",
				          	  render: function ( data, type, row, meta ) { 
				          		 var clase_impresion="";
				          		if(data!="")
					        		{
					        		clase_impresion = "fa-check";
					        		}  
				          		else
				          			{
				          			clase_impresion = "fa-close";
				          			}
				          		return '<i class="fa '+clase_impresion+'" aria-hidden="true"></i>';
				          	  }
			           },
			           { data: "anulada", className: "cell_centered",
				          	  render: function ( data, type, row, meta ) { 
				          		 var clase_inh;
				          		if(data!="")
					        		{
				          			clase_inh = "fa-check";
					        		}  
				          		else
				          			{
				          			clase_inh = "fa-close";
				          			}
				          		return '<i class="fa '+clase_inh+'" aria-hidden="true"></i>';
				          	  }
			           },
			         ],
	         select: { style: 'os'},
	         columnDefs: [
	                      { "targets": 0, "visible": false }             
	                  ],
			language: dataTableLanguage
		});
		
		})
//*******************************************************

$("#tab_entradas_reserva_impresiones").on("click", function(e) {	
	
    var data = dtentradasReserva.rows( { selected: true } ).data();   
		if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	showButtonSpinner("#tab_entradas_reserva_impresiones");
	var identrada = dtentradasReserva.rows( { selected: true } ).data()[0].identrada

	
	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/ventareserva/busqueda/show_historico_impresiones_por_entrada.do'/>?identrada="+identrada, function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "lg");
	});		
})
//************************************************************************
$("#tab_entradas_reserva_modificar_usos").on("click", function(e) {
	dtentradasVenta = dtentradasReserva;
	showButtonSpinner("#tab_entradas_reserva_modificar_usos");
	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/ventareserva/busqueda/entrada/modificarUsosMasivo.do'/>", function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "xs");
	});
	
})	


//***********************************************************
$("#tab_entradas_reserva_imprimir").on("click", function(e) {
		
		var data = dtentradasReserva.rows( { selected: true } ).data(); 
		if (data.length<=0) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.alert.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}	
		
		dtentradasReserva.processing(true);
		
		var entradasSeleccionadas = dtentradasReserva.rows(".selected").data();
   
		var data=[];
		var impresa=false;
		
		entradasSeleccionadas.each(function (value, index) {
			imp=""+value.impresionentradases.Impresionentradas;
			impresa = impresa || (imp!="undefined");				
			data.push(""+value.identrada);	
		 })
		
		 
		if(impresa)
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.alert.impresas" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
		else
		 	$.ajax({
				contenttype: "application/json; charset=utf-8",
				type : "post",
				url : "<c:url value='/ajax/ventareserva/busqueda/entrada/imprimir.do'/>",
				timeout : 100000,
				data: { data: data.toString() }, 
				success : function(data) {				
				},
				error : function(exception) {
					dtentradasReserva.processing(false);
					
					new PNotify({
					      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					      text: exception.responseText,
						  type: "error",		     
						  delay: 5000,
						  buttons: { sticker: false }
					   });			
				}
			});		 
	})
	
//***************************************************************
$("#tab_entradas_reserva_reimprimir").on("click", function(e) {
		
	var data = dtentradasReserva.rows( { selected: true } ).data(); 
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
		dtentradasReserva.processing(true);
		
		var entradasSeleccionadas = dtentradasReserva.rows(".selected").data();
   
		var data=[];
		
		entradasSeleccionadas.each(function (value, index) {
			data.push(""+value.identrada);
		 })
		
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/entrada/reimprimir.do'/>",
			timeout : 100000,
			data: { data: data.toString() }, 
			success : function(data) {				
			},
			error : function(exception) {
				dtentradasReserva.processing(false);
				
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",		     
					  delay: 5000,
					  buttons: { sticker: false }
				   });			
			}
		});
		 
	})
	
	
	//***************************************************************
$("#actualizar_usos").on("click", function(e) {
		
	var entradas = json_usos.ArrayList.Entrada;
	if(entradas.length>0)	
	{
	for (var i=0; i<entradas.length; i++) 
	   {
		entrada = entradas[i];
		actualizarEntrada(entrada);	
	   }
	}
	else
	{
	actualizarEntrada(entradas);
	}	
	
	var xml_entradas = "<ArrayList>"+json2xml(json_usos.ArrayList)+"</ArrayList>";
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/actualizarUsosTabla.do'/>",
		timeout : 100000,
		data: {
			xml: xml_entradas
		},
		success : function(data) {
			
			$("#modal-dialog-form-2").modal('hide');
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
			
			
		},
		error : function(exception) {
			hideSpinner("#modal-dialog-form .modal-content");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});		
})

function actualizarEntrada(entrada)
{
	var entradasTornos = entrada.entradatornoses.Entradatornos;
	 if(""+entradasTornos=="undefined")
   	  return "";
	
	
	if(entradasTornos.length>0)
	{
	for (var j=0; j<entradasTornos.length; j++) 
	   {
		var entradaTornos = entradasTornos[j];
		var id_uso = "#entrada"+entradaTornos.identradasvalidas;
		var valorUso = $(id_uso).val();
		entradaTornos.usos = valorUso;		
	   }
	}
	else
		{
		var id_uso = "#entrada"+entradasTornos.identradasvalidas;
		var valorUso = $(id_uso).val();
		entradasTornos.usos = valorUso;	
		}
}

</script>
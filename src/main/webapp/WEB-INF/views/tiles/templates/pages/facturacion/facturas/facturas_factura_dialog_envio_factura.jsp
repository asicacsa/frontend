<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="modal fade" id="modal-dialog-envio-entradas"
	role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close close_dialog" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">�</span>
				</button>
				<h4 class="modal-title">
					<spring:message code="facturacion.facturas.tabs.gestionfacturas.dialog.enviar.title" />			
				</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="control-label"> <input type="radio" class="flat" checked name="tipoenvio" value="1"> 
						<spring:message code="facturacion.facturas.tabs.gestionfacturas.dialog.enviar.email.option" />
					</label>
					<br/>
					 <label class="control-label"> <input type="radio" class="flat" name="tipoenvio" value="2">
						<spring:message code="facturacion.facturas.tabs.gestionfacturas.dialog.enviar.fax.option" />
					</label>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success close_dialog" id="tab_factura_dialog_enviar"
						data-dismiss="modal">
						<spring:message code="common.button.ok" />
					</button>
					<button type="button" class="btn btn-default close_dialog"
						data-dismiss="modal">
						<spring:message code="common.button.cancel" />
					</button>
				</div>

			</div>
			
		</div>
	</div>
</div>

<!--  -->

<script>
//********************************************************************************
$("#tab_factura_dialog_enviar").on("click", function(e) { 

	if ($("input[name='tipoenvio']:checked").val()=="1") {
		enviarFactura("email");
	}
	else {
		enviarFactura("fax");
	}
});	

//********************************************************************************
function enviarFactura(tipo) {
	
	showButtonSpinner("#tab_facturas_enviar_factura");
	
	var data = dt_listgestionfacturas.rows( { selected: true } ).data();
		
	
	var xml="<parametro><list>";
	for(i=0;i<data.length;i++)
		{
		var datosFactura = json2xml(data[i],'');
		xml=xml+"<Factura idfactura=\""+data[i].idfactura[0]+"\">"+datosFactura+"</Factura>";
		}
	xml=xml+"</list><medioenvio>"+tipo+"</medioenvio></parametro>";
	
	

	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/factura/factura/factura_envio.do'/>",
		timeout : 100000,
		data: {
				xml: xml
			  }, 
		success : function(data) {
			hideSpinner("#tab_facturas_enviar_factura");
			dt_listgestionfacturas.ajax.reload();	

			new PNotify({
					title : '<spring:message code="facturacion.facturas.tabs.gestionfacturas.envio_realizada" />',
					text : '<spring:message code="facturacion.facturas.tabs.gestionfacturas.dialog.envio.realizado" />',
					type : "info",
					delay : 5000,
					buttons : {
						sticker : false
					}
				});
			},
			error : function(exception) {
				hideSpinner("#tab_facturas_enviar_factura");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						sticker : false
					}
				});
			}
		});

	}

</script>


<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="modal fade" id="modal_porcentajes" tabindex="-1"
	role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close close_dialog" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">�</span>
				</button>
				<h4 class="modal-title titulo_atributos"></h4>
			</div>
			<div class="modal-body">
				<form id="form_porcentajes" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
					<input type="hidden" id="idPorcentaje" />
					<div class="col-md-12 col-sm-12 col-xs-12">
					    
					<div class="form-group date-picker">
						<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.productos.field.tarifas.fecha" />*</label>
						<div class="col-md-8 col-sm-8 col-xs-8">
							  <a type="button" class="btn btn-default btn-clear-date" id="button_apertura_clear">
									<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="ventareserva.operacionescaja.operacionescaja.list.button.clear" />"> <span class="fa fa-trash"></span>
									</span>
							  </a>			
		                       <div class="input-prepend input-group">
		                         <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
		                         <input type="text" name="fecha" id="fecha" class="form-control" value="" readonly/>
		                         <input type="hidden" required="required" id="fechainicioPorcentaje" name="fechainicioPorcentaje" />
		                         <input type="hidden" id="fechafinPorcentaje" name="fechafinPorcentaje" />
		                       </div>
						</div>
					</div>						    
					
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.tipo_clientes.field.porcentaje" />*</label>
						<div class="col-md-3 col-sm-3 col-xs-3">
							<input type="text" name="porcentaje" id="porcentaje" class="form-control" required="required"/>								
						</div>
						<label class="control-label col-md-1 col-sm-1 col-xs-1">%</label>	
						<div class="col-md-4 col-sm-4 col-xs-4"></div>				
					</div>
					
					
				</form>
				
				<div class="modal-footer">
					<button id="save_cliente_button" type="button" class="btn btn-primary close_porcentajes">
						<spring:message code="common.button.accept" />
					</button>
					<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
						<spring:message code="common.button.cancel" />
					</button>
				</div>

			</div>
			
		</div>
	</div>
</div>

<script>
$('#porcentaje').inputmask({alias: 'numeric', 
    allowMinus: false,  
    digits: 2, 
    max: 99.99});


$today= moment().format("DD/MM/YYYY");
//Ahora venta
dia_inicio = $today;
dia_fin = $today;

var datatableModalPorcentajes;
var creandoPorcentaje = true;

$('input[name="fecha"]').val( dia_inicio + ' - ' + dia_fin);


$('input[name="fecha"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
  	  	 $('input[name="fecha"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('input[name="fechainicioPorcentaje"]').val(start.format('DD/MM/YYYY'));
  	  	 $('input[name="fechafinPorcentaje"]').val(end.format('DD/MM/YYYY'));
	 });
	 
function limpiarModaltPorcentajes(datatableNew)
{
	datatableModalPorcentajes = datatableNew;
	$("#idPorcentaje").val("");
	$("#fechainicioPorcentaje").val($today);
	$("#fechafinPorcentaje").val($today);
	$("#porcentaje").val("");
	$("#modal_porcentajes").modal('show');
	$('input[name="fecha"]').val(  $today+ ' - ' +  $today);
	setModalDialogSize("#modal_porcentajes", "sm");	
	creandoPorcentaje = true;
}	 

function editarModalPorcentajes(datatableNew)
{
	datatableModalPorcentajes = datatableNew;
	var datos = datatableModalPorcentajes.rows( '.selected' ).data()[0];
	$("#idPorcentaje").val(datos[0]);
	$("#fechainicioPorcentaje").val(datos[1]);
	$("#fechafinPorcentaje").val(datos[2]);
	$("#porcentaje").val(datos[3]);
	$('input[name="fecha"]').val( datos[1] + ' - ' + datos[2]);
	$("#modal_porcentajes").modal('show');
	setModalDialogSize("#modal_porcentajes", "sm");	
	creandoPorcentaje = false;
}


$(".close_porcentajes").on("click", function(e) { 
	$("#form_porcentajes").submit();						
})



$("#form_porcentajes").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		guardarPorcentaje()
	}
}
);		


function guardarPorcentaje()
{
	
	if(!creandoPorcentaje)
		datatableModalPorcentajes
	    .rows( '.selected' )
	    .remove();	
	
	datatableModalPorcentajes.row.add( [  $("#idPorcentaje").val(), ''+$("#fechainicioPorcentaje").val()+'',
	                        ''+$("#fechafinPorcentaje").val()+'',$("#porcentaje").val()] )
	    	.draw();
	$("#modal_porcentajes").modal('hide');

}




</script>

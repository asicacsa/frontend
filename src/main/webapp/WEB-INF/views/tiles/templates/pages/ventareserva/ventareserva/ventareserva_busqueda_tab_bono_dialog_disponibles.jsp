<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${disponibles_datos_sesiones}" var="disponibles_datos_sesiones_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.disponibles.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_establecer_disponibles" class="form-horizontal form-label-left">
		<input type="hidden" name="xmlCanje" id="xmlCanje"/>
		<div class="form-group">
			<div id="tabla-disponibles" class="col-md-12 col-sm-12 col-xs-12 disponibles">
					<div class="col-md-10 col-sm-10 col-xs-12">
						<div class="form-group encabezado-disponibilidad">
							<label class="control-label col-md-8 col-sm-8 col-xs-12 label-disponibilidad">Disponibilidad <span class="nombre-producto"><x:out select = "$producto" /></span></label>
							<label class="control-label col-md-1 col-sm-1 col-xs-4""><spring:message code="venta.ventareserva.dialog.disponibles.field.fecha" /></label>
							<div class="col-md-3 col-sm-3 col-xs-8" id="div-fecha-disponibles">
			                      <div class="input-prepend input-group">
			                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
			                        <input type="text" name="fecha_disponibles" id="fecha_disponibles" class="form-control" readonly value="<x:out select = "$fecha" />"/>
			                      </div>
							</div>
						</div>					
					</div>
					<div class="btn-group pull-right btn-datatable">
						<a type="button" class="btn btn-info" id="button_disponibles_lock">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.bloquear" />"> <span class="fa fa-lock"></span>
							</span>
						</a>
						<a type="button" class="btn btn-info" id="button_disponibles_unlock">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.desbloquear" />"> <span class="fa fa-unlock"></span>
							</span>
						</a>
					</div>
					
					<div class="col-md-12 col-sm-12 col-xs-12">
						<table id="datatable-list-disponibles" class="table table-striped table-bordered dt-responsive sesiones" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.idsesion" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.fecha" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.hora" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.contenido" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.idzona" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.zona" /></th>
									<th class="disponibles"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.disponibles" /></th>
									<th class="bloqueadas"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.bloqueadas" /></th>
									<th class="reservadas"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.reservadas" /></th>
									<th class="vendidas"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.vendidas" /></th>
								</tr>
							</thead>
							<tbody>								
							</tbody>
						</table>
						<span>&nbsp;</span>
					</div>
			</div>
		
		</div>
		
		<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3">Observaciones</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<textarea name="observacionesCanje" id="observacionesCanje"  class="form-control" ></textarea>
				</div>
		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_disponibles_button" type="button" class="btn btn-primary save_dialog">
					<spring:message code="venta.ventareserva.dialog.disponibles.list.canjear.bono" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
	</form>
</div>

<script>
$('input[name="fecha_disponibles"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
	minDate: moment(),
  	locale: $daterangepicker_sp
});

var dt_listdisponibles=$('#datatable-list-disponibles').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	scrollY: "200px",
	scrollCollapse: true,
	paging: false,
    select: { style: 'single' },
    initComplete: function( settings, json ) {
    	window.setTimeout(cargaInicialSesiones, 100);
	},
	columnDefs: [
        { "targets": 0, "visible": false },
        { "targets": 1, "visible": false },
        { "targets": 2, "visible": false },
        { "targets": 5, "visible": false }
    ],
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('datatable-list-disponibles');
   	}
} );


function cargaInicialSesiones()
{
var json_disponibles = ${disponibles_datos_sesiones_json};

if (json_disponibles.ArrayList!="") 
	{
	var item= json_disponibles.ArrayList.sesiones.Sesion;
	cargarSesiones(item);
	}
}
	
	
function cargarSesiones(item)
{
	dt_listdisponibles.rows().remove().draw();
	if (typeof item!="undefined")
	{
	if (item.length>0)
		{
		$.each(item, function(key, sesion){
	    	dt_listdisponibles.row.add([
				 sesion,								                                                                           
		         sesion.idsesion, 
		         sesion.fecha, 
		         sesion.horainicio, 
		         sesion.contenido.nombre, 
		         sesion.zonasesions.Zonasesion.zona.idzona, 
		         sesion.zonasesions.Zonasesion.zona.nombre, 
		         sesion.zonasesions.Zonasesion.numlibres, 
		         sesion.zonasesions.Zonasesion.numbloqueadas, 
		         sesion.zonasesions.Zonasesion.numreservadas, 
		         sesion.zonasesions.Zonasesion.numvendidas 
		    ]).draw();
	    });
		}
	else
		dt_listdisponibles.row.add([
                item,								                                                                           
                item.idsesion, 
                item.fecha, 
                item.horainicio, 
                item.contenido.nombre, 
                item.zonasesions.Zonasesion.zona.idzona, 
                item.zonasesions.Zonasesion.zona.nombre, 
                item.zonasesions.Zonasesion.numlibres, 
                item.zonasesions.Zonasesion.numbloqueadas, 
                item.zonasesions.Zonasesion.numreservadas, 
                item.zonasesions.Zonasesion.numvendidas 
	    ]).draw();
	}
}


$('input[name="fecha_disponibles"]').on("change", function() { 
	showFieldSpinner("#div-fecha-disponibles");
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/list_tipos_producto.do'/>",
		timeout : 100000,
		data: {
  			   idtipoproducto: "<x:out select = "$idtipoproducto" />",
	    	   fecha: $('input[name="fecha_disponibles"]').val()
			  }, 
		success : function(data) {
			if (data.ArrayList!="") {
				var item= data.ArrayList.sesiones.Sesion;
				cargarSesiones(item);
				hideSpinner("#div-fecha-disponibles");
			    dt_listdisponibles.draw();
			}
		},
		error : function(exception) {
			
			hideSpinner("#div-fecha-disponibles");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});	
	
});

$("#save_disponibles_button").on("click", function(e) {
	var observaciones = $("#observacionesCanje").val();
	var xmlCanje="<list>";	
	xmlCanje+="<Canjebono><observaciones>"+observaciones+"</observaciones>";
	xmlCanje+="<bono><idbono>"+idBono+"</idbono></bono>";
	xmlCanje+="<lineadetallezonasesion>";
	xmlCanje+="<lineadetalle><idlineadetalle>"+idLineaDetalle+"</idlineadetalle><canjebono/></lineadetalle>";
	var zonaSesion = dt_listdisponibles.rows(".selected").data()[0][0];
	xmlCanje+="<zonasesion>";
	xmlCanje+="<idzonasesion>"+zonaSesion.zonasesions.Zonasesion.idzonasesion+"</idzonasesion>";
	xmlCanje+="<numvendidas>"+zonaSesion.zonasesions.Zonasesion.numvendidas+"</numvendidas>";
	xmlCanje+="<numreservadas>"+zonaSesion.zonasesions.Zonasesion.numreservadas+"</numreservadas>";
	xmlCanje+="<numlibres>"+zonaSesion.zonasesions.Zonasesion.numlibres+"</numlibres>";
	xmlCanje+="<numbloqueadas>"+zonaSesion.zonasesions.Zonasesion.numbloqueadas+"</numbloqueadas>";
	xmlCanje+="<zona>"+ json2xml(zonaSesion.zonasesions.Zonasesion.zona,"")+"</zona>";
	xmlCanje+="<sesion>";
	xmlCanje+="<idsesion>"+zonaSesion.idsesion+"</idsesion>";
	xmlCanje+="<horainicio>"+zonaSesion.horainicio+"</horainicio>";
	xmlCanje+="<fecha>"+zonaSesion.fecha+"</fecha>";
	xmlCanje+="<contenido>"+json2xml(zonaSesion.contenido,"")+"</contenido>";
	xmlCanje+="<overbookingventa>"+zonaSesion.overbookingventa+"</overbookingventa>";
	xmlCanje+="<bloqueado>"+zonaSesion.bloqueado+"</bloqueado>";
	xmlCanje+="</sesion>";
	xmlCanje+="</zonasesion>";	
	xmlCanje+="</lineadetallezonasesion>";
	xmlCanje+="</Canjebono>";
	xmlCanje+="</list>";
	
	$("#xmlCanje").val(xmlCanje);
	
	var data = $("#form_establecer_disponibles").serializeObject();

	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/busqueda/bono/asignarSesion.do'/>",
		timeout : 100000,
		data: data, 
		success : function(data) {
			$("#modal-dialog-form-2").modal('hide');
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		   						
			$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/ventareserva/busqueda/bono/editar.do'/>?id="+idBono, function() {
			});
			
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
	
	
})

</script>

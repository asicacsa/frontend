<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_puntos_recogida_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.puntos.recogida.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_puntos_recogida_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.puntos.recogida.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_puntos_recogida_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.puntos.recogida.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>		
	</div>
	
	<table id="datatable-lista-puntos-recogida" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="administracion.recintos.tabs.puntos.recogida.list.header.nombre" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>	
</div>

<script>

var dt_listpuntos=$('#datatable-lista-puntos-recogida').DataTable( {
    ajax: {
        url: "<c:url value='/ajax/admon/recintos/puntos_recogida/list_puntos.do'/>",
        rowId: 'idpuntosrecogida',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Puntosrecogida)); return(""); },
        error: function (xhr, error, thrown) {
            if (xhr.responseText=="403") {
                  $("#tab_puntos_recogida").hide();
            }  
        
        },
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listpuntos.data().count()>0) dt_listpuntos.columns.adjust().draw(); });
	},
    columns: [
        { data: "nombre", type: "spanish-string" ,  defaultContent:""}  
       
        
    ],    
    drawCallback: function( settings ) {
    	$('[data-toggle="tooltip"]').tooltip();
    },
    select: { style: 'os'},
	language: dataTableLanguage,
	processing: true,
} );
	

insertSmallSpinner("#datatable-lista-puntos-recogida_processing");
//********************************************************************************
$("#tab_puntos_recogida_new").on("click", function(e) { 
	showButtonSpinner("#tab_puntos_recogida_new");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/recintos/puntos_recogida/show_puntos_recogida.do'/>", function() {
		setModalDialogSize("#modal-dialog-form", "sm");	
		$("#modal-dialog-form").modal('show');
	});
})
//********************************************************************************
$("#tab_puntos_recogida_edit").on("click", function(e) {
	showButtonSpinner("#tab_puntos_recogida_edit");
	var data = sanitizeArray(dt_listpuntos.rows( { selected: true } ).data(),"idpuntosrecogida");
	
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.recintos.tabs.puntos.recogida.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		hideSpinner("#tab_puntos_recogida_edit");
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.recintos.tabs.puntos.recogida.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		hideSpinner("#tab_puntos_recogida_edit");
		return;
	}	
		
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/recintos/puntos_recogida/show_puntos_recogida.do'/>?id="+data[0], function() {
	
		setModalDialogSize("#modal-dialog-form", "sm");
		$("#modal-dialog-form").modal('show');
	});
});

//********************************************************************************
$("#tab_puntos_recogida_remove").on("click", function(e) { 
	showButtonSpinner("#tab_puntos_recogida_remove");	
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="administracion.recintos.tabs.puntos.recogida.list.confirm.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
  		  buttons: { closer: false, sticker: false	},
  		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
	
		   dt_listpuntos.processing(true);
			
			var data = sanitizeArray(dt_listpuntos.rows( { selected: true } ).data(),"idpuntosrecogida");
		   
			$.ajax({
				contenttype: "application/json; charset=utf-8",
				type : "post",
				url : "<c:url value='/ajax/admon/recintos/puntos_recogida/remove_puntos_recogida.do'/>",
				timeout : 100000,
				data: { data: data.toString() }, 
				success : function(data) {
					dt_listpuntos.processing(false);	
					dt_listpuntos.rows( { selected: true } ).remove().draw();
				},
				error : function(exception) {
					dt_listpuntos.processing(false);
					
					new PNotify({
					      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					      text: exception.responseText,
						  type: "error",		     
						  delay: 5000,
						  buttons: { sticker: false }
					   });			
				}
			});

	   }).on('pnotify.cancel', function() {
	   });		 
	hideSpinner("#tab_puntos_recogida_remove");
}); 



</script>


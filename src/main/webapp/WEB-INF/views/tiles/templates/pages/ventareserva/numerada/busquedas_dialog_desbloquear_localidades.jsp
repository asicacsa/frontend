<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="modal fade" id="modal-dialog-localidades-desbloquear"
	role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close close_dialog" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">�</span>
				</button>
				<h4 class="modal-title">
					<spring:message code="venta.numerada_busquedas.dialog.desbloquear_localidades.title" />			
				</h4>
			</div>
			<div class="modal-body">
				<p class="modal-text"><spring:message code="venta.numerada_busquedas.dialog.desbloquear_localidades.text" /></p>
				<div class="modal-footer">
					<button type="button" class="btn btn-success close_dialog" id="tab_localidades_dialog_desbloquear_si_vender"
						data-dismiss="modal">
						<spring:message code="common.text.yes" />
					</button>
					<button type="button" class="btn btn-success close_dialog" id="tab_localidades_dialog_desbloquear_no_vender"
						data-dismiss="modal">
						<spring:message code="common.text.no" />
					</button>
					<button type="button" class="btn btn-default close_dialog"
						data-dismiss="modal">
						<spring:message code="common.button.cancel" />
					</button>
				</div>

			</div>
			
		</div>
	</div>
</div>

<!--  -->

<script>
//********************************************************************************
$("#tab_localidades_dialog_desbloquear_no_vender").on("click", function(e) { 
	
	var xml="<list>";
	var data = dt_listlocalidades.rows( { selected: true } ).data();
	
	for (var i = 0; i < data.length; i++) {
		xml=xml+"<Estadolocalidad>";
		xml=xml+"<idestadolocalidad>"+data[i].idestadolocalidad+"</idestadolocalidad>";
		xml=xml+"<localidad>";
		xml=xml+"<idlocalidad>"+data[i].localidad.idlocalidad+"</idlocalidad>";
		xml=xml+"<nombrearea>"+data[i].localidad.nombrearea+"</nombrearea>";
		xml=xml+"<zona>";
		xml=xml+"<idzona>"+data[i].localidad.zona.idzona+"</idzona>";
		xml=xml+"<nombre>"+data[i].localidad.zona.nombre+"</nombre>";
		xml=xml+"</zona>";
		xml=xml+"<fila>"+data[i].localidad.fila+"</fila>";
		xml=xml+"<butaca>"+data[i].localidad.butaca+"</butaca>";
		xml=xml+"</localidad>";
		xml=xml+"<sesion>";
		xml=xml+"<idsesion>"+data[i].sesion.idsesion+"</idsesion>";
		xml=xml+"<idzona>"+data[i].sesion.idzona+"</idzona>";
		xml=xml+"<contenido>";
		xml=xml+"<nombre>"+data[i].sesion.contenido.nombre+"</nombre>";
		xml=xml+"</contenido>";
		xml=xml+"<fecha>"+data[i].sesion.fecha+"</fecha>";
		xml=xml+"<horainicio>"+data[i].sesion.horainicio+"</horainicio>";
		xml=xml+"</sesion>";
		xml=xml+"<estado>";
		xml=xml+"<idestado>"+data[i].estado.idestado+"</idestado>";
		xml=xml+"<nombre>"+data[i].estado.nombre+"</nombre>";
		xml=xml+"</estado>";
		xml=xml+"<observaciones>"+data[i].observaciones+"</observaciones>";		
		xml=xml+"</Estadolocalidad>";
	}
	xml=xml+"</list>";
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/desbloquear_localidades.do'/>",
		timeout : 100000,
		data: {
				xml: xml
			  }, 
		success : function(data) {
			dt_listlocalidades.ajax.reload(null,false);
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,				  
				  buttons: { sticker: false }				  
			   });		
		}
	});
		
	
});


//********************************************************************************
$("#tab_localidades_dialog_desbloquear_si_vender").on("click", function(e) { 
	
	var xml="<list>";
	var data = dt_listlocalidades.rows( { selected: true } ).data();
	
	for (var i = 0; i < data.length; i++) {
		xml=xml+"<Estadolocalidad>";
		xml=xml+"<idestadolocalidad>"+data[i].idestadolocalidad+"</idestadolocalidad>";
		xml=xml+"<localidad>";
		xml=xml+"<idlocalidad>"+data[i].localidad.idlocalidad+"</idlocalidad>";
		xml=xml+"<nombrearea>"+data[i].localidad.nombrearea+"</nombrearea>";
		xml=xml+"<zona>";
		xml=xml+"<idzona>"+data[i].localidad.zona.idzona+"</idzona>";
		xml=xml+"<nombre>"+data[i].localidad.zona.nombre+"</nombre>";
		xml=xml+"</zona>";
		xml=xml+"<fila>"+data[i].localidad.fila+"</fila>";
		xml=xml+"<butaca>"+data[i].localidad.butaca+"</butaca>";
		xml=xml+"</localidad>";
		xml=xml+"<sesion>";
		xml=xml+"<idsesion>"+data[i].sesion.idsesion+"</idsesion>";
		xml=xml+"<idzona>"+data[i].sesion.idzona+"</idzona>";
		xml=xml+"<contenido>";
		xml=xml+"<nombre>"+data[i].sesion.contenido.nombre+"</nombre>";
		xml=xml+"</contenido>";
		xml=xml+"<fecha>"+data[i].sesion.fecha+"</fecha>";
		xml=xml+"<horainicio>"+data[i].sesion.horainicio+"</horainicio>";
		xml=xml+"</sesion>";
		xml=xml+"<estado>";
		xml=xml+"<idestado>"+data[i].estado.idestado+"</idestado>";
		xml=xml+"<nombre>"+data[i].estado.nombre+"</nombre>";
		xml=xml+"</estado>";
		xml=xml+"<observaciones>"+data[i].observaciones+"</observaciones>";		
		xml=xml+"</Estadolocalidad>";
	}
	xml=xml+"</list>";
	
	
	$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='//ajax/venta/numerada/vender_localidades_bloqueadas.do'/>?"+encodeURI("xml="+xml), function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "lg");
	});	
	
		
		
});	

</script>


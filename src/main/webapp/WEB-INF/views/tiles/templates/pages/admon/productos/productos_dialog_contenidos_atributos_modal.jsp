<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<x:parse xml="${editar_tipo_producto_selector_atributo}" var="editar_tipo_producto_selector_atributo_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="administracion.productos.tabs.productos.atributos.title" />
	</h4>	
</div>

	<div class="modal-body">

		<form id="contenido_atributos" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input type="hidden" id="idTipoProductoAtributo" value="${idatributoscontenido}"/>
		<div class="col-md-12 col-sm-12 col-xs-12">

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.tipoproductos.field.tipo" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="idatributo" id="selector_atributo" class="form-control" required="required">
						<x:forEach select="$editar_tipo_producto_selector_atributo_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.contenidos.field.valor" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" name="valorAtributo" id="valorAtributo" class="form-control" value="${valor}"/>
				</div>
			</div>

		</div>

	</form>
	



</div>
</div>
<div class="modal-footer">

		<button id="save_cliente_button" type="button" class="btn btn-primary close_atributo" data-dismiss="modal">
			<spring:message code="common.button.accept" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>
<script>
	if("${idatributo}"!="")
	{
		$("#selector_atributo").append('<option value="${idatributo}">${nombre}${nombreTipo}</option>');	
	}




$(".close_atributo").on("click", function(e) { 
	
	var idTipoProductoAtributo = $("#idTipoProductoAtributo").val();
	
	if(idTipoProductoAtributo!='')
		dtatributos.rows( '.selected' ).remove();
	

		
	dtatributos.row.add( [  $("#idTipoProductoAtributo").val(),$("#selector_atributo").val(), $("#selector_atributo option:selected").text(),$("#valorAtributo").val()] )
	    	.draw();
	$("#modal_atributos").modal('hide');
		
})

</script>

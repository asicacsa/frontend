<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>




<div class="modal-body">
	<form id="form_anular_lineas" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input type="hidden" name="idVenta" id="idVenta" value="${idVenta} }" />
		

		<table id="datatable_list_venta_individual" class="table table-striped table-bordered dt-responsive ventareserva" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.producto" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.fecha" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.contenido" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.disponible" />.</th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.cnd" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.perfil" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.descuento" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.impUnitario" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.importe" /></th>
					<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.sesiones" /></th>
					<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.bono" /></th>
					<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.idtarifa" /></th>
					<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.iddescuento" /></th>
					<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.idperfil" /></th>
					<th><spring:message code="venta.ventareserva.dialog.venta_resumen.field.anulada" /></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>


	<div class="modal-footer">
		<button id="boton_anular_lineas" type="button" class="btn btn-primary">
			<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.lineas.title" />
		</button>	
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>		
	</form>
</div>
<script>
 hideSpinner("#button_ver_lineas");
 var jsonLineas = ${lineasDetalle};


 var dt_listventaindividual=$('#datatable_list_venta_individual').DataTable( {
 	language: dataTableLanguage,
 	info: false,
 	searching: false,
 	scrollCollapse: true,
 	paging: false,
 	initComplete: function( settings, json ) {
    	 window.setTimeout(cargarLineasParaVenta, 100);    	 
 	},
     select: { style: 'os' },
     columnDefs: [
         { "targets": 0, "visible": false },
         { "targets": 1, "visible": false },
         { "targets": 5, "visible": false },
         { "targets": 9, "visible": false },
         { "targets": 11, "visible": false },
         { "targets": 12, "visible": false },
         { "targets": 13, "visible": false },
         { "targets": 14, "visible": false },
         { "targets": 15, "visible": false }
     ],
     drawCallback: function( settings ) { 
     	activateTooltipsInTable('datatable_list_venta_individual');
    	}
 } );
 
 
 
 var total_anulacion = "0.0";
 
 $("#boton_anular_lineas").on("click", function(e) {		
		var idVenta=""+${idVenta};
		
		var lineas_sel = dt_listventaindividual.rows( { selected: true }).data();
		
		if(lineas_sel.length>0)
		{
			total_anulacion = dt_listventaindividual.rows( { selected: true }).data()[0][0].importe;
			
			
	
		 	$("#modal-dialog-form-4 .modal-content").load("<c:url value='/ajax/ventareserva/operacionescaja/show_anular_venta.do'/>?id="+idVenta+"&parcial=1", function() {
		 		$("#modal-dialog-form-4").modal('show');
		 		setModalDialogSize("#modal-dialog-form-3", "md");
		 	});
		}
	 
	 })

 
 

 function cargarLineaParaVenta(lineadetalle)
 {
	var fechas= "", contenidos= "", disponibles= "",retorno= false;
	var impreso = lineadetalle.entradasimpresas+"/"+lineadetalle.entradasnoanuladas;
	var sesiones = lineadetalle.lineadetallezonasesions.Lineadetallezonasesion;
	var descuento_nombre=""+lineadetalle.descuentopromocional.nombre;
	var numbonoagencia= lineadetalle.numerobonoagencia;
	
	if(descuento_nombre=="undefined")
		descuento_nombre =" ";
	
	var contenidos;
	
	 if (sesiones.length>0)		 
	 {
		 for (var i=0; i<sesiones.length; i++) {
				if (retorno) {
					fechas+="</br>";
					contenidos+= "</br>";
					disponibles+="</br>";
				}				
				var f= sesiones[i].zonasesion.sesion.fecha.split("-")[0];
				fechas+= f;					
				var hi=sesiones[i].zonasesion.sesion.horainicio;
				var  no=sesiones[i].zonasesion.sesion.contenido.nombre;
				contenidos+= hi+" "+no;
				var d=sesiones[i].zonasesion.numlibres;
				disponibles+=d;
				retorno= true;
			}	 
			
	 }
	 else
	 {
		fechas= lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.fecha.split("-")[0];
		contenidos= lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.horainicio+" "+lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.contenido.nombre;
		disponibles= ""+ lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.numlibres;
	}
	 
	 var anulada = "";
	 
	 if(lineadetalle.anulada == 0)
		 anulada = "<spring:message code="venta.ventareserva.dialog.venta_resumen.field.vigente" />";
	 else
		 anulada = "<spring:message code="venta.ventareserva.dialog.venta_resumen.field.anulada" />";
			
	 dt_listventaindividual.row.add([
			     lineadetalle,
			     lineadetalle.producto.idproducto, 
			     lineadetalle.producto.nombre,
			   	 fechas,
				 contenidos,
			 		disponibles,
			 		  lineadetalle.cantidad, 
			 		 lineadetalle.perfilvisitante.nombre, 
			 		 descuento_nombre, 
			 		 "",
			 		lineadetalle.importe,  // Importe
					                            "", // Datos de las sesiones
					                            "", // Bono
					                            "", // idtarifa
					                            "",	// iddescuento
						"", 	// idperfil,
						""+anulada
						
				                               ]).draw();
 }
 
 
 function cargarLineasParaVenta()
 {
	var lineas_detalle = jsonLineas.ArrayList.Lineadetalle;
	
	if(lineas_detalle.length>0)
		{
		for( k=0; k<lineas_detalle.length;k++)
			{
			var lineaDetalle = lineas_detalle[k];
			cargarLineaParaVenta(lineaDetalle)
			
			}
		}
	else
		{
		var lineaDetalle = lineas_detalle;
		cargarLineaParaVenta(lineaDetalle)
		}
 }

</script>
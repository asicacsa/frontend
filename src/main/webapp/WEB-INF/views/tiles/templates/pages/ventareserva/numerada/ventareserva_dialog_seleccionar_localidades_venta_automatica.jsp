<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${zonasPorRecinto}" var="zonasPorRecinto_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">x</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.numerada.tabs.venta.automatica.title" />
	</h4>
</div>
<div class="modal-body" id="principal">
	<form id="form_venta_automatica" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	<input id="idsesion" name="idsesion" type="hidden" value="<x:out select = "$idSesion" />" />
	
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-3">
			<spring:message code="venta.numerada.tabs.venta.automatica.zona" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
				<select name="idzona" id="selector_zona" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$zonasPorRecinto_xml/ArrayList/zona" var="item">
							<option value="<x:out select="$item/idzona" />"><x:out select="$item/nombre" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.numerada.tabs.venta.automatica.numero" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="numeroLocalidades" id="selector_numLocalidades" class="form-control" required="required">
					</select>
				</div>
			</div>	
			
					<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_automatico_button" type="button" class="btn btn-primary save_automatico_button">
					<spring:message code="common.button.ok" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
	</form>

	

</div>

<script>
var $select= $("#selector_numLocalidades");
var numLoc = ${maxLoc};
var intNum=numLoc.int;



for(i=1;i<=intNum;i++)
	{
	$select.append('<option value=' + i +'>' + i + '</option>');
	}

//**********************************************
function generarXmlLocalidades(data,sesion)
{
	
	var xml="<list>";
	if(data.length>1)
		{
		$.each(data, function(key, Localidad){
			xml+="<Estadolocalidad><sesion><idsesion>"+sesion+"</idsesion></sesion><localidad><idlocalidad>"+Localidad.idlocalidad+"</idlocalidad></localidad></Estadolocalidad>";
		})
		}
	else
		xml+="<Estadolocalidad><sesion><idsesion>"+sesion+"</idsesion></sesion><localidad><idlocalidad>"+data.idlocalidad+"</idlocalidad></localidad></Estadolocalidad>";
	

	xml=xml+"</list>";
	
	return xml;
}

//********************************************************************
$("#save_automatico_button").on("click", function(e) 
{ 
	var 	zona= $("#selector_zona").val(),
			sesion= dtsesionesVentas.rows( { selected: true } ).data()[0][0].idsesion ,
			numero= ""+$("#selector_numLocalidades").val();
	
	$.ajax({
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/prereservaAutomaticaLocalidades.do'/>",
		timeout : 100000,
		data: {
			zona: zona,
			sesion: sesion,
			numero: numero
		  }, 
		success : function(data) {
			var xml=generarXmlLocalidades(data.ArrayList.Temp_resultado_butaca,sesion);	
			xml+="";
			$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='//ajax/venta/numerada/vender_localidades_bloqueadas.do'/>?"+encodeURI("xml="+xml), function() {
				$("#modal-dialog-form-2").modal('show');
				setModalDialogSize("#modal-dialog-form-2", "lg");
			});
				
				
			
			
			
		},
		error : function(exception) {
			hideSpinner("#modal-dialog-form .modal-content");

			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : exception.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		}
	});	
})

</script>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_selector_descuentos}" var="ventareserva_selector_descuentos_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.edit_sesion.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_edit_sesion" class="form-horizontal form-label-left">

		<div class="col-md-12 col-sm-12 col-xs-12">
		
			

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.dialog.edit_sesion.field.tarifa" /></label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<select class="form-control" name="idtarifa" id="idtarifa">
						<option value=""></option>						
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.dialog.edit_sesion.field.descuentos" /></label>
				<div id="descuentos_div" class="col-md-9 col-sm-9 col-xs-12">
					<select class="form-control" name="iddescuento" id="iddescuento">
						<option value=""></option>
						<x:forEach select="$ventareserva_selector_descuentos_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>

			
			
		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_edit_sesion" type="button" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
	</form>
</div>


<script>



hideSpinner("#button_${buttonEdit}_editar");
//*******************************************
 var sesion_data=$.extend(true,[],dt_listventaabono.rows( { selected: true } ).data()[0][0]), 
	cerrar_dialogo= false, actualizando= false;
//**********************************************
$('#iddescuento option[value="'+ dt_listventaabono.rows( { selected: true } ).data()[0][6] +'"]').attr("selected", "selected"); 
//******************************************************************

 
 var item = sesion_data.LineadetalleVentaPasesDTO.perfiles.Perfilvisitante;
 
if (typeof item!="undefined") {
	var $select= $("#idtarifa");
	$select.html('');
	$select.prepend("<option value='' selected='selected'></option>");
	if (item.length>0)
	    $.each(item, function(key, val){
	      $select.append('<option value="' + val.idperfilvisitante + '" idtarifa="' + val.tarifa.idtarifa + '" importe="' + val.tarifa.Tarifaproducto.importe + '">' + val.nombre + '</option>');
	})
	else
		$select.append('<option value="' + item.idperfilvisitante + '" idtarifa="' + item.tarifa.idtarifa + '" importe="' + item.tarifa.Tarifaproducto.importe + '">' + item.nombre + '</option>');
	 
	if (typeof sesion_data.LineadetalleVentaPasesDTO.perfilvisitante.idperfilvisitante!="undefined") {
			$('#idtarifa option[value="'+ sesion_data.LineadetalleVentaPasesDTO.perfilvisitante.idperfilvisitante +'"]').attr("selected", "selected");
			CargarDescuentos();
		} 
}
//*****************************************************************
function CargarDescuentos()
{
	showFieldSpinner("#descuentos_div");
	$select=$("#iddescuento");
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/list_descuentos.do'/>",
		timeout : 100000,
		data: {
				idproducto: "${idProducto}",
				idcanal: "${sessionScope.idcanal}",
				idtarifa: $("#idtarifa :selected").attr("idtarifa")
			  }, 
		success : function(data) {
			hideSpinner("#descuentos_div");
			$select.html('');
			$select.prepend("<option value='' selected='selected'></option>");
			if (data.ArrayList!="") {
				var item=data.ArrayList.LabelValue;
				if (item.length>0)
				    $.each(item, function(key, val){
				      $select.append('<option value="' + val.value + '">' + val.label + '</option>');
				    });
				else
			      	$select.append('<option value="' + item.value + '">' + item.label + '</option>');
				if (typeof sesion_data.LineadetalleVentaPasesDTO.descuento.iddescuento!="undefined") {
					$('#iddescuento option[value="'+ sesion_data.LineadetalleVentaPasesDTO.descuento.iddescuento +'"]').attr("selected", "selected");
					
				} 
			}
		},
		error : function(exception) {
			hideSpinner("#descuentos_div");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});	
}

 
//******************************************************************
$("#idtarifa").on("change", function(e) {
	
	var perfilVisitante= obtenerPerfilVisitante(item,$("#idtarifa").val());
	sesion_data.LineadetalleVentaPasesDTO.perfilvisitante.nombre= perfilVisitante.nombre;
	sesion_data.LineadetalleVentaPasesDTO.perfilvisitante.idperfilvisitante= perfilVisitante.idperfilvisitante;
	//sesion_data.perfilvisitante.tarifa.nombre= perfilVisitante.tarifa.nombre;
	var importeLinea = calcularImporteTotal();
	sesion_data.LineadetalleVentaPasesDTO.importe= importeLinea;
	
	CargarDescuentos();
		
}); 

//******************************************************************
 function cerrarActualizarTabla() {
	
	dt_listventaabono.rows( { selected: true } ).data()[0][0]= sesion_data;
	if ($("#idtarifa :selected").val()!="") {
		dt_listventaabono.rows( { selected: true } ).data()[0][5]= $("#idtarifa :selected").text();
		
		var descuento_parcial = 0;
		/*dt_listventaabono.rows( { selected: true } ).data()[0][6]= $("#iddescuento :selected").text();
		
		if(!isNaN(parseFloat($("#iddescuento :selected").text())))
			{
			descuento_parcial =  sesion_data.LineadetalleVentaPasesDTO.importe * parseFloat(sesion_data.LineadetalleVentaPasesDTO.descuento.porcentajedescuento)/100.0;
			
			dt_listventaabono.rows( { selected: true } ).data()[0][6]= descuento_parcial;
			}
		else
			{
			dt_listventaabono.rows( { selected: true } ).data()[0][6]= "";
			}*/
		
		dt_listventaabono.rows( { selected: true } ).data()[0][6]= $("#iddescuento :selected").text();
		
		dt_listventaabono.rows( { selected: true } ).data()[0][7]= sesion_data.LineadetalleVentaPasesDTO.tarifaproducto.importe;
	}
	dt_listventaabono.rows().invalidate().draw();
	
	
	obtener_totales_venta_temporal();
	$("#modal-dialog-form-3").modal('hide');	
    
	
} 

function calcularImporteTotal() {
	var total;	
	
	var importe=$("#idtarifa :selected").attr("importe");
	if (""+importe!="undefined") {		
		total= Number($("#idtarifa :selected").attr("importe"));
		return total.toFixed(2);	
	}else{		
		new PNotify({
		      title: '<spring:message code="venta.ventareserva.dialog.edit_sesion.field.numero_incorrecto" />',
		      text: "",
			  type: "error",
			  buttons: { sticker: false }				  
		   });		
		return "";
	}
	
}

function actualizarLineaDetalle() {
	actualizando= true;
	if ($("#iddescuento").val()!="") {
		showFieldSpinner("#descuentos_div");
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/venta/ventareserva/aplicar_descuentos.do'/>",
			timeout : 100000,
			data: {
					idcliente: "${idCliente}",
					lineadetalle: "<Lineadetalle>"+json2xml(sesion_data,"")+"</Lineadetalle>"
				  }, 
			success : function(data) {
				hideSpinner("#descuentos_div");
			
				sesion_data.importe= data.importe;
				if (cerrar_dialogo) cerrarActualizarTabla();
				actualizando= false;
			},
			error : function(exception) {
				hideSpinner("#descuentos_div");
				actualizando= false;
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",
					  buttons: { sticker: false }				  
				   });		
			}
		});
	}
	else {
		sesion_data.importe= calcularImporteTotal();
		actualizando= false;
	}
}

/* $("#cantidad").on("change", function(e) {
	sesion_data.cantidad= $("#cantidad").val();
	actualizarLineaDetalle();
});
 */
function obtenerPerfilVisitante(tarifas,idperfilvisitante) {
	for (var i=0; i<tarifas.length; i++) 
		if (tarifas[i].idperfilvisitante==idperfilvisitante) return(tarifas[i]); 
	return("");
}



$("#iddescuento").on("change", function(e) {
	sesion_data.LineadetalleVentaPasesDTO.descuento.iddescuento= $("#iddescuento").val();
	sesion_data.LineadetalleVentaPasesDTO.descuento.porcentajedescuento= $("#iddescuento :selected").text();
});



$("#save_edit_sesion").on("click", function(e) {
	$("#form_edit_sesion").submit();
});

$("#form_edit_sesion").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		saveSesionData();		
	}
});

function saveSesionData() {
	cerrar_dialogo= true;
	
	cerrarActualizarTabla();
	
}
	 


</script>
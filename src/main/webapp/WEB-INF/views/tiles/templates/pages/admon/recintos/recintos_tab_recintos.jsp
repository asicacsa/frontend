<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${tabrecintos_selector_unidades}" var="tabrecintos_selector_unidades_xml" />

<style>
td.details-control {
    background: url('../resources/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('../resources/details_close.png') no-repeat center center;
}
</style>


<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2><spring:message code="common.text.filter_list" /></h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">

		<form id="form_filter_list_recintos" class="form-horizontal form-label-left">
		
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="administracion.recintos.tabs.recintos.field.unidades" /></label>
					<div class="col-md-10 col-sm-10 col-xs-10">
						
						<select class="form-control" name="idunidadnegocio">
							<option value=""></option>
							<x:forEach select="$tabrecintos_selector_unidades_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
						</select>
					</div>
				</div>				
			</div>
			
			<div class="clearfix"></div>
			<div class="ln_solid"></div>
			<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
				<button id="button_filter_list_recintos" type="button" class="btn btn-success pull-right">
					<spring:message code="common.button.filter" />
				</button>
			</div>
				
		</form>
	</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_recintos_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.recintos.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_recintos_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.recintos.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_recintos_lock">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.recintos.list.button.bloquear" />"> <span class="fa fa-lock"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_recintos_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.recintos.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>
	</div>


	<table id="datatable-lista-recintos" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="administracion.recintos.tabs.recintos.list.header.recintos_asociados" /></th>
				<th><spring:message code="administracion.recintos.tabs.recintos.list.header.horario" /></th>
				<th><spring:message code="administracion.recintos.tabs.recintos.list.header.capacidad" /></th>
				<th><spring:message code="administracion.recintos.tabs.recintos.list.header.varias_zonas" /></th>
				<th><spring:message code="administracion.recintos.tabs.recintos.list.header.taquillas" /></th>
				<th><spring:message code="administracion.recintos.tabs.recintos.list.header.subrecintos" /></th>
				<th><spring:message code="administracion.recintos.tabs.recintos.list.header.bloqueado" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>
var dt_listrecintos=$('#datatable-lista-recintos').DataTable( {
    ajax: {
        url: "<c:url value='/ajax/admon/recintos/recintos/list_recintos.do'/>",
        rowId: 'idrecinto',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Recinto)); return(""); },
        error: function (xhr, error, thrown) {
                    if (xhr.responseText=="403") {
                          $("#tab_recintos").hide();
                    }     
                    else
       	      	 {
                    	$("#datatable-lista-recintos_processing").hide();
       	      		new PNotify({
       						title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
       						text : xhr.responseText,
       						type : "error",
       						delay : 5000,
       						buttons : {
       							closer : true,
       							sticker : false
       						}					
       					});
       	      	 }
             },
        data: function (params) { return($("#form_filter_list_recintos").serializeObject()); }
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listrecintos.data().count()>0) dt_listrecintos.columns.adjust().draw(); });
	},
    columns: [
              { data: "nombre", type: "spanish-string" ,  defaultContent:""},
              { data: "bloqueado", type: "spanish-string" ,
                  render: function ( data, type, row, meta ) {
                	  	  if (row.horario.from!='') return '<spring:message code="administracion.recintos.tabs.recintos.list.text.horario.de" /> '+row.horario.from+' <spring:message code="administracion.recintos.tabs.recintos.list.text.horario.hasta" /> '+row.horario.to; else return '';	
                  }},
              { data: "capacidad", type: "spanish-string" ,  defaultContent:""} , 
              { data: "zonas", type: "spanish-string" ,  defaultContent:""} ,
              { data: "taquillas.Taquilla", className: "cell_centered",
                      	  render: function ( data, type, row, meta ) { return showListInCell(data,"<spring:message code="administracion.recintos.tabs.recintos.list.text.taquillas" />","nombre"); }	
                            }, 
              { data: "recintos.Recinto", className: "cell_centered",
                           render: function ( data, type, row, meta ) { return showListInCell(data,"<spring:message code="administracion.recintos.tabs.recintos.list.text.subrecintos" />","nombre"); }	
              }, 
              { data: "bloqueado", className: "text_icon cell_centered",
                render: function ( data, type, row, meta ) {
              	  	  if (data==1) return '<i class="fa fa-check-circle" aria-hidden="true"></i>'; else return '';	
                }}
              
    ],    
    drawCallback: function( settings ) {
    	activateTooltipsInTable('datatable-lista-recintos');
    	//activarTr();
    },
    select: { style: 'os', selector:'td:not(:nth-child(6),:nth-child(5))'},
	language: dataTableLanguage,
	processing: true,
    
} );



/* Formatting function for row details - modify as you need */
function datos ( row ) {
    // `d` is the original data object for the row
    
    d = row.data();
    str = ''
    
    subrecintos = d.recintos.Recinto;
   
    if (subrecintos != null && typeof subrecintos != "undefined") {
    if (Array.isArray(subrecintos)) {
		if (subrecintos.length > 0) {
			str += '<table width="100%" border="1">';
			str +="<tr><td>Recintos</td><td>Opciones</td></tr>";
			for (var i = 0; i < subrecintos.length; i++) {
				str +="<tr><td>"+subrecintos[i].nombre+"</td><td>2</td></tr>";
			}	
			str +=  '</table>';
			row.child( str ).show();
		}
    }
    else
    	   if (subrecintos != "")
		    	{
		    	str = '<table width="100%" border="1">';
		    	str +="<tr><td>Recintos</td><td>Opciones</td></tr>";
		    	str +="<tr><td>"+subrecintos.nombre+"</td><td>1</td></tr>";
		    	str +=  '</table>';
		    	row.child( str ).show();
		    	}
	}
       
    return str;
   
}


function activarTr()
{
//Add event listener for opening and closing details
$('#datatable-lista-recintos tbody tr td').on('click', function () {
    var tr = $(this).parent();
	var row = dt_listrecintos.row(tr);
        
    
    if ( row.child.isShown() ) {
    	// This row is already open - close it
        row.child.hide();       
    }
    else {
    	// Open this row
        
        var detalle = datos(row);
    	
        
       
        
    }
} );
}






insertSmallSpinner("#datatable-lista-recintos_processing");





//********************************************************************************
$("#button_filter_list_recintos").on("click", function(e) {
dt_listrecintos.ajax.reload();
})




//********************************************************************************
$("#tab_recintos_remove").on("click", function(e) { 
	showButtonSpinner("#tab_recintos_remove");	
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="administracion.recintos.tabs.recintos.list.confirm.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
  		  buttons: { closer: false, sticker: false	},
  		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
	
		   dt_listrecintos.processing(true);
			
			var data = sanitizeArray(dt_listrecintos.rows( { selected: true } ).data(),"idrecinto");
		   
			$.ajax({
				contenttype: "application/json; charset=utf-8",
				type : "post",
				url : "<c:url value='/ajax/admon/recintos/recintos/remove_recintos.do'/>",
				timeout : 100000,
				data: { data: data.toString() }, 
				success : function(data) {
					dt_listrecintos.ajax.reload();					
				},
				error : function(exception) {
					dt_listrecintos.processing(false);
					
					new PNotify({
					      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					      text: exception.responseText,
						  type: "error",		     
						  delay: 5000,
						  buttons: { sticker: false }
					   });			
				}
			});

	   }).on('pnotify.cancel', function() {
	   });		 
	hideSpinner("#tab_recintos_remove")
}); 


//********************************************************************************
$("#tab_recintos_lock").on("click", function(e) { 
	showButtonSpinner("#tab_recintos_lock");
	dt_listrecintos.processing(true);
	
	var data = sanitizeArray(dt_listrecintos.rows( { selected: true } ).data(),"idrecinto");
	var estado = sanitizeArray(dt_listrecintos.rows( { selected: true } ).data(),"bloqueado");
	
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.recintos.tabs.recintos.list.alert.seleccion_multiple.bloquear" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		dt_listrecintos.processing(false);
		hideSpinner("#tab_recintos_lock");
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.recintos.tabs.recintos.list.alert.ninguna_seleccion.bloquear" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		dt_listrecintos.processing(false);
		hideSpinner("#tab_recintos_lock");
		return;
	}	
	
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/admon/recintos/recintos/bloquear_recintos.do'/>",
		timeout : 100000,
		data: { data: data.toString(),
			estado: estado.toString()}, 
		success : function(data) {
			dt_listrecintos.ajax.reload();					
		},
		error : function(exception) {
			dt_listrecintos.processing(false);
			
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
	
	hideSpinner("#tab_recintos_lock");
}); 
//********************************************************************
$("#tab_recintos_new").on("click", function(e) { 
	showButtonSpinner("#tab_recintos_new");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/recintos/recintos/show_recinto.do'/>", function() {										  
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
})

$("#tab_recintos_edit").on("click", function(e) { 
	showButtonSpinner("#tab_recintos_edit");
	var data = sanitizeArray(dt_listrecintos.rows( { selected: true } ).data(),"idrecinto");
	
		if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.recintos.tabs.recintos.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		hideSpinner("#tab_recintos_edit");
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.recintos.tabs.recintos.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		hideSpinner("#tab_recintos_edit");
		return;
	}	
		
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/recintos/recintos/show_recinto.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
})


</script>


<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${tiposBono}" var="tiposBono_xml" />
<x:parse xml="${tarifas}" var="tarifas_xml" />
<x:parse xml="${productos}" var="productos_xml" />

<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2><spring:message code="common.text.filter_list" /></h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">
	      
	<form id="form_busqueda_venta_bonos" class="form-horizontal form-label-left">     
	    	<!--  <input type="hidden" id="idcliente_venta_bono" name="idcliente_venta_bono" value=""/>-->
	    	<div class="row">
	    		<div class="col-md-6 col-sm-6 col-xs-12">
	    			<div class="form-group date-picker">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.venta_bono.field.fecha" /></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<a type="button" class="btn btn-default btn-clear-date" id="button_fechabuscadorbono_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.canje_bono.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
						  	</a>			
                        	<div class="input-prepend input-group">
                          		<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                          		<input type="text" name="fechabuscadorbono" id="fechabuscadorbono" class="form-control" value="" readonly/>
                          		<input type="hidden" required="required" name="fechabuscadorbonodesde" value=""/>
                          		<input type="hidden" required="required" name="fechabuscadorbonohasta" value=""/>
                        	</div>
						</div>
					</div>	    
	    		</div>	
		    	<div class="col-md-6 col-sm-6 col-xs-12">	
					<div class="form-group">
						<fieldset id="ventas_bonos_ventas">
							<div class="form-group">
								<div class="checkbox col-md-4 col-sm-4 col-xs-12">
									<input type="radio" name="anulada" id="ventas_bonos_todas" checked value="" data-parsley-multiple="ventas" class="flat" style="position: absolute; opacity: 0;">
									<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.todas" /></strong>
								</div>
								<div class="checkbox col-md-4 col-sm-4 col-xs-12">
									<input type="radio" name="anulada" id="ventas_bonos_anuladas"  value="1" data-parsley-multiple="reservas" class="flat" style="position: absolute; opacity: 0;">
									<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.anuladas" /></strong>
								</div>
								<div class="checkbox col-md-4 col-sm-3 col-xs-12">
									<input type="radio" name="anulada" id="ventas_bonos_vigentes" value="0" data-parsley-multiple="ambas" class="flat" style="position: absolute; opacity: 0;">
									<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.vigentes" /></strong>
								</div>							
							</div>				
						</fieldset>
					
					 <fieldset id="ventas-bonos">
							<div class="form-group">
								<div class="checkbox col-md-4 col-sm-4 col-xs-12">
									<input type="radio" name="facturados" id="ventas-bonos-todos" checked value="" data-parsley-multiple="ventas" class="flat" style="position: absolute; opacity: 0;">
									<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.todas" /></strong>
								</div>
								<div class="checkbox col-md-4 col-sm-4 col-xs-12">
									<input type="radio" name="facturados" id="ventas-bonos-fecturado"  value="1" data-parsley-multiple="reservas" class="flat" style="position: absolute; opacity: 0;">
									<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.facturado" /></strong>
								</div>
								<div class="checkbox col-md-4 col-sm-4 col-xs-12">
									<input type="radio" name="facturados" id="ventas-bonos-nofacturado" value="0" data-parsley-multiple="ambas" class="flat" style="position: absolute; opacity: 0;">
									<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.nofacturado" /></strong>
								</div>							
							</div>				
						</fieldset>								
					</div>
		      	</div>
		     </div>
	      	<div id="grupo-datos2">	
				<div class="x_title">
					<div class="clearfix"></div>
				</div>				
				<div class="col-md-5 col-sm-5 col-xs-12">	
					<div class="form-group">
					<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.refventa" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input name="refventa" id="refventa_bono" type="text" class="form-control" >
						</div>
					</div>
					<div class="form-group">
					<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.refbono" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input name="refbono" id="refbono" type="text" class="form-control" >
						</div>
					</div>
					<div class="form-group">
						<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.canje_bono.field.cliente" /></label>
						<div class="col-md-2 col-sm-2 col-xs-12">
								<input name="idcliente_venta_bono" id="idcliente_venta_bono" style="height:10" type="text" class="form-control"><!--  <button id="buscarClienteIndividual">v</button>-->
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input name="cliente_venta_bono" id="cliente_venta_bono" type="text" class="form-control" readonly>
						</div>
					</div>
					
				</div>
				<div class="col-md-7 col-sm-7 col-xs-12">	
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.producto" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<select name="selector_producto" id="selector_producto_venta_bonos" class="form-control" required="required">
						   		<option value="" ></option>
								<x:forEach select="$productos_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select = "$item/value" />" ><x:out select = "$item/label" /></option>
								</x:forEach>						   		
							</select>
						</div>						
					</div>
										
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.busquedas.tabs.field.tipo_bono" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<select name="selector_tipo_bono" id="tipo_bono_venta_bonos" class="form-control" required="required">
						   		<option value="" ></option>
								<x:forEach select="$tiposBono_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select = "$item/value" />" ><x:out select = "$item/label" /></option>
								</x:forEach>						   		
							</select>
						</div>						
					</div>

					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.busquedas.tabs.field.tarifa" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<select name="selector_tarifa" id="selector_tarifa" class="form-control" required="required">
						   		<option value="" ></option>
								<x:forEach select="$tarifas_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select = "$item/value" />" ><x:out select = "$item/label" /></option>
								</x:forEach>						   		
							</select>
						</div>						
					</div>
				</div>
				
			</div>	
	      <input type="hidden" name="start" value=""/>
			<input type="hidden" name="length" value=""/>
	      
	      </form>
	      <div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
			<button id="button_search_venta_bono" type="button" class="btn btn-success pull-right">
				<spring:message code="common.button.filter" />
			</button>
			<button id="button_clean_venta_bono" type="button" class="btn btn-success pull-right">
				<spring:message code="venta.ventareserva.tabs.canje-bono.button.clean" />
			</button>
		</div>
	</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		
		<a type="button" class="btn btn-info" id="tab_venta_bono_editar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.venta.bono.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>	
		<a type="button" class="btn btn-info" id="tab_venta_bono_anular">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.venta.bono.list.button.anular" />"> <span class="fa fa-times"></span>
			</span>
		</a>		
	</div>

	<table id="datatable-list-venta-bono" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="venta.ventareserva.busqueda.tabs.venta-bono.list.header.refVenta" /></th>
				<th><spring:message code="venta.ventareserva.busqueda.tabs.venta-bono.list.header.fechaHora" /></th>
				<th><spring:message code="venta.ventareserva.busqueda.tabs.venta-bono.list.header.cliente" /></th>
				<th><spring:message code="venta.ventareserva.busqueda.tabs.venta-bono.list.header.tipo" /></th>
				<th><spring:message code="venta.ventareserva.busqueda.tabs.venta-bono.list.header.impTotal" /></th>
				<th><spring:message code="venta.ventareserva.busqueda.tabs.venta-bono.list.header.impPagado" /></th>
				<th><spring:message code="venta.ventareserva.busqueda.tabs.venta-bono.list.header.impPendiente" /></th>
				<th><spring:message code="venta.ventareserva.busqueda.tabs.venta-bono.list.header.anulado" /></th>
				<th><spring:message code="venta.ventareserva.busqueda.tabs.venta-bono.list.header.impreso" /></th>	
				
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>
$("#cliente_venta_bono").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>?editable=1", "#cliente_venta_bono", "#idcliente_venta_bono");

$('input[name="fechabuscadorbono"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
  	  	 $('input[name="fechabuscadorbono"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('input[name="fechabuscadorbonodesde"]').val(start.format('DD/MM/YYYY'));  	  	 
  	  	 $('input[name="fechabuscadorbonohasta"]').val(end.format('DD/MM/YYYY'));
	 });
	 
$("#button_fechabuscadorbono_clear").on("click", function(e) {
    $('input[name="fechabuscadorbono"]').val('');
    $('input[name="fechabuscadorbonodesde"]').val('');
    $('input[name="fechabuscadorbonohasta"]').val('');
});

$("#idcliente_venta_bono").blur(function() {
	var idCliente =""+$("#idcliente_venta_bono").val(); 
	
	if(idCliente!="")
	{
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/facturacion/buscarClientePorId.do'/>",
		timeout : 100000,
		data: {
			idcliente: idCliente,    			
		},
		success : function(data) {    		     
				$("#cliente_venta_bono").val(data.Cliente.nombrecompleto); 
				
				
				
		},
		error : function(exception) {    	
			$("#idcliente_venta_bono").val("");
	    	$("#cliente_venta_bono").val("");
	    	

			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});
	}
	else
		$("#cliente_venta_bono").val("");
})
	
/***********************************************BOT�N EDITAR VENTA/RESERVA  *********************************/
$("#button_clean_venta_bono").on("click", function(e) {
	
	$("#refventa_bono").val("");
	$("#refbono").val("");
	$("#idcliente_venta_bono").val("");
	$("#cliente_venta_bono").val("");
	
    $('input[name="fechabuscadorbono"]').val('');
    $('input[name="fechabuscadorbonodesde"]').val('');
    $('input[name="fechabuscadorbonohasta"]').val('');
	
    
    $("#selector_producto_venta_bonos option:first").prop("selected", "selected");
    $("#tipo_bono_venta_bonos option:first").prop("selected", "selected");
    $("#selector_tarifa option:first").prop("selected", "selected");
})

//***************************************************************
$("#button_search_venta_bono").on("click", function(e) {
	dt_listventabono.ajax.reload();
	})
	
/***********************************************BOT�N EDITAR VENTA/RESERVA  *********************************/
$("#tab_venta_bono_editar").on("click", function(e) {
	
	var data = sanitizeArray(dt_listventabono.rows( { selected: true } ).data(),"idventa");
	
		
	if (data.length<=0 ||data.length>1)  {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: ' <spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.alert.editar.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
			return;
		}else
		{	
			showButtonSpinner("#tab_venta_bono_editar");
			$("#modal-dialog-form .modal-content").load("<c:url value='/venta/ventareserva/busqueda/ventareserva/editar_venta_bono.do'/>?id="+data, function() {
				$("#modal-dialog-form").modal('show');
				setModalDialogSize("#modal-dialog-form", "md");
			});
			
		}
});
	
	

//***************************************************************
$("#tab_venta_bono_anular").on("click", function(e) {
	var data = sanitizeArray(dt_listventabono.rows( { selected: true } ).data(),"idventa");
	
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.seleccion_multiple.editar" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.ninguna_seleccion.editar" />',		      
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	$("#modal-dialog-form .modal-content").load("<c:url value='/venta/ventareserva/busqueda/ventaBonos/pantalla_anular.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "sm");
	});	
	})

	
var dt_listventabono =$('#datatable-list-venta-bono').DataTable( {
	serverSide: true,
	searching: false,
	ordering: false,
	lengthMenu:[[25,50,100],[25,50,100]],
	"pageLength":25,
	pagingType: "simple",
	info: false,
	deferLoading: 0,
    ajax: {
        url: "<c:url value='/ajax/venta/ventareserva/venta_abono/list_venta_bono.do'/>",
        rowId: 'idventa',
        type: 'POST',
        dataSrc: function (json) { 
        	
        	if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Venta)); return(""); },
        data: function (params) { 
        	$('#form_busqueda_venta_bonos input[name="start"]').val(params.start);
        	$('#form_busqueda_venta_bonos input[name="length"]').val(params.length);        	
        	return($("#form_busqueda_venta_bonos").serializeObject()); }
        	,
            error: function (xhr, error, code)
                 {
                    	new PNotify({
     					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
     					text : xhr.responseText,
     					type : "error",
     					delay : 5000,
     					buttons : {
     						closer : true,
     						sticker : false
     					}					
     				});
                 }
    },
    initComplete: function( settings, json ) {
    	$('#datatable-list-venta-bono_length').hide();
    	
        //$('a#menu_toggle').on("click", function () {dt_listventabono.columns.adjust().draw(); });
	},
    columns: [
          	{ data: "idventa", type: "spanish-string", defaultContent: ""},
          	{ data: "fechayhoraventa", type: "spanish-string", defaultContent: ""}, 
          	{ data: "cliente.nombre", type: "spanish-string", defaultContent: "" },		 
          	{ data: "tipobonoventa.nombre", type: "spanish-string", defaultContent: "" },
          	{ data: "importetotalventa", type: "spanish-string", defaultContent: ""},
          	{ data: "importeparcialtotalventa", type: "spanish-string", defaultContent: ""},	
          	{ data: "importeparcialtotalventa", className: "spanish-string",
          		render: function ( data, type, row, meta ) {
          		  var importe = row.importetotalventa-row.importeparcialtotalventa;
          		  
          		  var valor = importe.toFixed(2); 
          		  
          		  return (valor);
          		}
          	},
          	{ data: "estadooperacion.idestadooperacion", className: "text_icon cell_centered",
          		render: function ( data, type, row, meta ) {
          	  	  	if (data==4) return '<i class="fa fa-check" aria-hidden="true"></i>'; else return '<i class="fa fa-close" aria-hidden="true"></i>';	
          	}},
          	{ data: "importeparcialtotalventa", className: "spanish-string",
          		render: function ( data, type, row, meta ) {
          			var item=row.lineadetalles;
          			
          			
          			if(item=="")
                      	{
                      	return "0/0";
                      	}  
          			var itemLinea = item.Lineadetalle;
          			
          			
          			if(itemLinea.length>0)
          			{
          			
          				var i = 0;
          				var nBonos = 0;
          				var nimpresos = 0;
          				 $.each(itemLinea, function(key, val){
          					 var item = itemLinea[i].bonosForIdlineadetalle.Bono;
          					 i++;
          					 if (item.length>0)
          						 nBonos = nBonos + item.length;
          					 else
          						 nBonos = nBonos + 1;
          					 
          					 var j = 0;
          					 if(item.length>0)
          					 {
          						 $.each(item, function(key, val){
          						    var impresiones = item[j].impresiones;
          		                	j = j +1;
          							if(impresiones>0)
          								 nimpresos++;                   
          		                })
          					 }
          					 else
          					 {
          						 var impresiones = item.impresiones;
          						 if(impresiones>0)
          							 nimpresos++; 
          					 }
          				 })
          				 return ""+ nimpresos+"/"+nBonos;
          			}
          			else
          			{
          			
          			item = itemLinea.bonosForIdlineadetalle.Bono;
          			if (item.length>0)
                      	{			
                      	var nBonos = item.length;
                          var nimpresos = 0;
                      	var k = 0;
                          $.each(item, function(key, val){
                          	var impresiones = item[k].impresiones;
          					 if(impresiones>0)
          						 nimpresos++;   
          					 k++;
                          })
                          return ""+ nimpresos+"/"+nBonos;
                      	}            
                      else
                      	{
                      	var nimpresos = 0;
                      	if(item.impresiones>0)
          					 nimpresos=1; 
                      	return ""+ nimpresos+"/1";
                      	}
          			}
          	}}],
	select: { style: 'os'},
	language: dataTableLanguage,
	processing: true,
});



insertSmallSpinner("#datatable-list-venta-bono_processing");



</script>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_productos_principales}" var="ventareserva_productos_principales_xml" />
<x:parse xml="${ventareserva_canales_indirectos}" var="ventareserva_canales_indirectos_xml" />

<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2><spring:message code="venta.ventareserva.selector" /></h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">
	
		<form id="form_selector_venta_individual" class="form-horizontal form-label-left">
		<!-- 	<input type="hidden" id="idcliente_venta_individual" name="idcliente_venta_individual" value=""/> -->
			<input type="hidden" id="cifcliente_venta_individual" value=""/>
			<input type="hidden" id="ventaParcial" value="0"/>
			<input type="hidden" id="paisventaParcial" value="0"/>
			<input type="hidden" id="cpVentaParcial" value="0"/>
			<input type="hidden" id="cpcliente_venta_individual" value=""/>
			<input type="hidden" id="emailcliente_venta_individual" value=""/>
			<input type="hidden" id="telefonocliente_venta_individual" value=""/>
			<input type="hidden" id="telefonomovilcliente_venta_individual" value=""/>
			<input type="hidden" id="producto_venta_individual_directa_valor" value=""/>
			<input type="hidden" id="producto_venta_individual_directa_texto" value=""/>

		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.emision_bono.field.cliente" /></label>
					<div class="col-md-2 col-sm-2 col-xs-12">
						<input name="idcliente_venta_individual" id="idcliente_venta_individual" style="height:10" type="text" class="form-control"><!--  <button id="buscarClienteIndividual">v</button>-->
					</div>					
					<div class="col-md-7 col-sm-7 col-xs-12">
						<input name="cliente_venta_individual" id="cliente_venta_individual" type="text" class="form-control" readonly>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.emision_bono.field.canal" /></label>
					<div class="col-md-8 col-sm-8 col-xs-12">
						<select class="form-control" name="idcanal_venta_individual" id="idcanal_venta_individual">
							<option value=""></option>
							<x:forEach select="$ventareserva_canales_indirectos_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
						</select>
					</div>
				</div>
			</div>
			
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.emision_bono.field.fecha" /></label>
					<div class="col-md-9 col-sm-9 col-xs-12">
	                      <div class="input-prepend input-group">
	                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	                        <input type="text" name="fecha_venta_individual" id="fecha_venta_individual" class="form-control" readonly/>
	                      </div>
					</div>
				</div>
			</div>
		</div>	
		
		<div class="row botones-venta-directa">		
			<x:forEach select="$ventareserva_productos_principales_xml/ArrayList/Entry" var="item">
				<x:if select="not($item/unidadnegocio/idunidadnegocio='')">
					&nbsp;
					<x:forEach select="$item/productos/Producto" var="producto">
						<button type="button" id="venta_<x:out select="$producto/value" />" value="<x:out select="$producto/value" />" class="btn btn-primary btn-venta-directa"><x:out select="$producto/label" /></button>					
					</x:forEach>
					&nbsp;
				</x:if>
			</x:forEach>
		</div>
			
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label class="control-label"><spring:message code="venta.ventareserva.tabs.emision_bono.field.unidad" /></label>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<select class="form-control ventareserva" name="idunidad_venta_individual" id="idunidad_venta_individual" size="5" required="required">
							<x:forEach select="$ventareserva_productos_principales_xml/ArrayList/Entry/unidadnegocio" var="item">
								<option value="<x:out select="$item/idunidadnegocio" />"><x:out select="$item/nombre" /></option>
							</x:forEach>
						</select>
					</div>
				</div>
			</div>
				
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label class="control-label"><spring:message code="venta.ventareserva.tabs.emision_bono.field.producto" /></label>
					<div class="col-md-12 col-sm-12 col-xs-12" id="div_producto_venta_individual">
						<select class="form-control ventareserva" name="idproducto_venta_individual" id="idproducto_venta_individual" size="5" required="required">
							<option value=""></option>
						</select>
					</div>
				</div>				
			</div>
		</div>
		</form>
		<div class="clearfix"></div>
		<div class="ln_solid"></div>
		<div class="col-md-12 col-sm-12 col-xs-12">
			<button id="button_disp_venta_individual" type="button" class="btn pull-right">
				<spring:message code="venta.ventareserva.tabs.emision_bono.button.disp" />
			</button>
		</div>
	</div>
</div>

	
<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="totales-group totales_venta_individual">
		Total: <span class="total-lbl"><span id="total-val">0.00</span>&euro;</span>&nbsp;<span class="descuento-lbl">Descuento: <span id="total-desc">0.00</span>&euro;&nbsp;(<span id="total-prc">0.00</span>%)</span>
	</div>

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="button_venta_individual_sesiones">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.sesiones" />"> <span class="fa fa-history"></span>
			</span>
		</a>
		 <a type="button" class="btn btn-info" id="button_venta_individual_multisesion">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.multisesion" />"> <span class="fa fa-flag"></span>
			</span>
		</a> 
		<a type="button" class="btn btn-info" id="button_venta_individual_horas">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.horas" />"> <span class="fa fa-clock-o"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="button_venta_individual_descombinar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.descombinar" />"> <span class="fa fa-object-ungroup"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="button_venta_individual_combinar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.combinar" />"> <span class="fa fa-object-group"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="button_venta_individual_prerreservar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.prerreservar" />"> <span class="fa fa-bookmark-o"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="button_venta_individual_vender_sel">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.emision_bono.list.button.venderSel" />"> <span class="fa fa-mouse-pointer"></span>
			</span>
		</a> 
		<a type="button" class="btn btn-info" id="button_venta_individual_vender">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.venta_individual.list.button.vender" />"> <span class="fa fa-shopping-cart"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="button_venta_individual_duplicar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.emision_bono.list.button.duplicar" />"> <span class="fa fa-clone"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="button_venta_individual_eliminar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.emision_bono.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="button_venta_individual_cancelar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.venta_individual.list.button.cancelar" />"> <span class="fa fa-close"></span>
			</span>
		</a>
	</div>

	<table id="datatable_list_venta_individual" class="table table-striped table-bordered dt-responsive ventareserva" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.idproducto" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.producto" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.fecha" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.contenido" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.disp" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.etd" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.perfil" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.descuentos" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.importeunitario" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.importe" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.sesiones" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.bono" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.idtarifa" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.iddescuento" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.idperfil" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>

var ventaParcial = false;

var conAbono= false;

var boton_venta_directa="";

$('input[name="fecha_venta_individual"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
	minDate: moment(),
  	locale: $daterangepicker_sp
});

var dt_listventaindividual=$('#datatable_list_venta_individual').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	scrollCollapse: true,
	paging: false,
    select: { style: 'os' },
	columnDefs: [
        { "targets": 0, "visible": false },
        { "targets": 1, "visible": false },
        { "targets": 11, "visible": false },
        { "targets": 12, "visible": false },
        { "targets": 13, "visible": false },
        { "targets": 14, "visible": false },
        { "targets": 15, "visible": false }
    ],
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('datatable_list_venta_individual');
   	}
} );

$("#cliente_venta_individual").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>?editable=1", "#cliente_venta_individual", "#idcliente_venta_individual", "#cifcliente_venta_individual", "#cpcliente_venta_individual", "#emailcliente_venta_individual", "#telefonocliente_venta_individual");

$("#cliente_venta_individual").on("change", function(e) {
	obtener_totales_venta_temporal("venta_individual","venta_individual",$("#idcliente_venta_individual").val(),4,false);
});

$("#idunidad_venta_individual").on("change", function(e) {
	showFieldSpinner("#div_producto_venta_individual");
	
	var data = $("#idunidad_venta_individual :selected").val();
	$select=$("#idproducto_venta_individual");
	
	/* Primero se a�aden los productos principales */
	
	$select.html('');
	unidadesnegocio[$("#idunidad_venta_individual :selected").text()].forEach( function(element) { 
	      $select.append('<option class="option-main-product" value="' + element.value + '">' + element.label + '</option>');
	});
	
	/* Luego se a�aden los subproductos por ajax */
	
	if (data!="") { // La unidad de negocio COMBINADAS no tiene id
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/venta/ventareserva/list_subproductos.do'/>",
			timeout : 100000,
			data: {
		    	   id: data.toString()
				  }, 
			success : function(data) {
				hideSpinner("#div_producto_venta_individual");
				if (data.ArrayList!="") {
					var item= data.ArrayList.LabelValue;
					if (item.length>0){
						item=sortJSON(item,"label",true);
					    $.each(item, function(key, val){
					      $select.append('<option value="' + val.value + '">' + val.label + '</option>');
					    });
					}
					else
					      $select.append('<option value="' + item.value + '">' + item.label + '</option>');
				}
			},
			error : function(exception) {
				hideSpinner("#div_producto_venta_individual");
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",
					  buttons: { sticker: false }				  
				   });		
			}
		});	
	}
	else hideSpinner("#div_producto_venta_individual");

}); 	


//********************************************************************************
function cargarProductoVentaIndividualEnTabla(idBoton, esPrincipal, idProducto, textoProducto) {

	var formdata = $("#form_selector_venta_individual").serializeObject();
	
	showButtonSpinner(idBoton);
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/get_producto.do'/>",
		timeout : 100000,
		data: {
			idcliente: formdata.idcliente_venta_individual,
			idcanal: "${sessionScope.idcanal}",
			idtipoventa: 4, // Tipo Venta no numerada
			idproducto: idProducto,
			fecha: formdata.fecha_venta_individual,
			principal: esPrincipal,
			autosesion: "true"
		},
		success : function(data) {
			var contenido="",
				fecha_sesion= "",
				disponibles= "";
			item= data.Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion;
			if (item.length>0) {
				contenido= item[0].sesion.horainicio+" "+item[0].sesion.contenido.nombre;
				fecha_sesion= item[0].sesion.fecha;
				disponibles= item[0].numlibres;
			}
			else {
				contenido= item.sesion.horainicio+" "+item.sesion.contenido.nombre;
				fecha_sesion= item.sesion.fecha;
				disponibles= item.numlibres;
			}
			var tarifa= data.Lineadetalle.perfilvisitante.nombre;
			var importe= data.Lineadetalle.tarifaproducto.importe;
			var row=dt_listventaindividual.row.add([data,
	                                	idProducto.trim(), 
			                            textoProducto.trim(),
			                            fecha_sesion.split("-")[0],
			                            contenido,
			                            disponibles,
			                            1,  // N� Etd
			                            typeof tarifa!="undefined"?tarifa:"", // Perfil/Tarifa
			                            "", // Descuentos
			                            importe, // I. Unitario
			                            importe,  // Importe
			                            "", // Datos de las sesiones
			                            "", // Bono
			                            get_idtarifa(data), // idtarifa
			                            "",	// iddescuento
			                            "" 	// idperfil
		                               ]).draw();
			hideSpinner(idBoton);
			obtener_totales_venta_temporal("venta_individual","venta_individual",$("#idcliente_venta_individual").val(),4,false);
			row.select();
			editarLineaDetalleVentaIndividual();
			
		},
		error : function(exception) {
			hideSpinner(idBoton);
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});	
}


//********************************************************************************
$(".btn-venta-directa").on("click", function(e) {
	var formdata = $("#form_selector_venta_individual").serializeObject();
	
	showButtonSpinner(e.currentTarget);
	
	boton_venta_directa= "#"+this.id;
	
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_disponibles.do'/>?idproducto="+$(e.currentTarget).val()+"&idcliente="+formdata.idcliente_venta_individual+"&idtipoventa=4&producto="+encodeURIComponent($(e.currentTarget).text())+"&fecha="+formdata.fecha_venta_individual+"&button=venta_individual&list=dt_listventaindividual", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
	
});

//********************************************************************************
$( "#idproducto_venta_individual" ).on("click", function(e) { 
	
	if ($("#idproducto_venta_individual option:selected").length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	dt_listventaindividual.rows().deselect();
	
	var formdata = $("#form_selector_venta_individual").serializeObject();
	
	showSpinner("#tab_venta_individual");	
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_disponibles.do'/>?idproducto="+formdata.idproducto_venta_individual+"&idcliente="+formdata.idcliente_venta_individual+"&idtipoventa=4&producto="+encodeURIComponent($("#idproducto_venta_individual option:selected").text())+"&fecha="+formdata.fecha_venta_individual+"&button=venta_individual&list=dt_listventaindividual", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
	/*	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/getDatosProducto.do'/>",
		timeout : 100000,
		data: {
			  idProducto: $("#idproducto_venta_individual :selected").val()
			  }, 
		success : function(data) {
			var tipoProductos = data.Producto.tipoprodproductos.Tipoprodproducto;
			if(tipoProductos.length>=0)	
				{
				$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_disponibles.do'/>?idproducto="+formdata.idproducto_venta_individual+"&idcliente="+formdata.idcliente_venta_individual+"&idtipoventa=4&producto="+encodeURIComponent($("#idproducto_venta_individual option:selected").text())+"&fecha="+formdata.fecha_venta_individual+"&button=venta_individual&list=dt_listventaindividual", function() {
					$("#modal-dialog-form").modal('show');
					setModalDialogSize("#modal-dialog-form", "md");
				});
				}
			else
				cargarProductoVentaIndividualEnTabla("", $("#idproducto_venta_individual :selected").hasClass("option-main-product"), $("#idproducto_venta_individual :selected").val(),  $("#idproducto_venta_individual :selected").text());
			
		},
		error : function(exception) {
			hideSpinner("#div_producto_venta_individual");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});	*/
	
	
	
}); 

//********************************************************************************
$("#button_venta_individual_sesiones").on("click", function(e) {
	
	
	if (dt_listventaindividual.rows( { selected: true }).data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	if (dt_listventaindividual.rows( { selected: true }).data().length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#button_venta_individual_sesiones");	
	
	var formdata = $("#form_selector_venta_individual").serializeObject(),
		rowdata= dt_listventaindividual.rows( { selected: true }).data();
	
	var sesiones = rowdata[0][0].Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion;
	var map = "<tipoProducto>";
	if (sesiones.length>0)
		{
		for(i=0;i<sesiones.length;i++)
			{
			var session = sesiones[i].zonasesion.sesion;
			var id_producto_sesion = ""+session.contenido.tipoproducto.idtipoproducto;
			var fecha_sesion = session.fecha;
			map +="<entry><int>"+id_producto_sesion+"</int><date>"+fecha_sesion+"</date></entry>"			
			}
		}
	else
		{
		var session = sesiones.zonasesion.sesion;
		var id_producto_sesion = ""+session.contenido.tipoproducto.idtipoproducto;
		var fecha_sesion = session.fecha;
		map +="<entry><int>"+id_producto_sesion+"</int><date>"+fecha_sesion+"</date></entry>"			

		}
	
	map +="</tipoProducto>";
	
	

	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_disponibles.do'/>?idproducto="+rowdata[0][1]+"&idcliente="+formdata.idcliente_venta_individual+"&producto="+encodeURIComponent(rowdata[0][2])+"&fecha="+formdata.fecha_venta_individual+"&map="+map+"&button=venta_individual&list=dt_listventaindividual&edicion=1", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
});

//********************************************************************************
$("#button_venta_individual_duplicar").on("click", function(e) { 
		
	var data= $.extend(true,[],dt_listventaindividual.rows( { selected: true } ).data());

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	dt_listventaindividual.rows.add(data).draw();
	obtener_totales_venta_temporal("venta_individual","venta_individual",$("#idcliente_venta_individual").val(),4,false);

}); 

//********************************************************************************
$("#button_venta_individual_eliminar").on("click", function(e) { 
		
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.confirm.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
		  buttons: { closer: false, sticker: false	},
		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
		   dt_listventaindividual.rows( { selected: true } ).remove().draw();
		   obtener_totales_venta_temporal("venta_individual","venta_individual",$("#idcliente_venta_individual").val(),4,false);
	   }).on('pnotify.cancel', function() {
	   });		 

}); 

//********************************************************************************
$("#button_venta_individual_cancelar").on("click", function(e) { 
		
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="venta.ventareserva.tabs.venta_individual.list.confirm.cancelar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
		  buttons: { closer: false, sticker: false	},
		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
			$.ajax({
				contenttype: "application/json; charset=utf-8",
				type : "post",
				url : "<c:url value='/ajax/venta/ventareserva/cancelar_prerreserva.do'/>",
				timeout : 100000,
				data: null,
				success : function(data) {
				   dt_listventaindividual.rows().remove().draw();
				   $("#idcliente_venta_individual").val("");
			    	$("#cliente_venta_individual").val("");
			    	$("#cpcliente_venta_individual").val(""); 
					$("#emailcliente_venta_individual").val(""); 
					$("#telefonocliente_venta_individual").val(""); 
					$("#telefonomovilcliente_venta_individual").val("");
					$("#paisventaParcial").val("");
					$("#cpVentaParcial").val("");
				   obtener_totales_venta_temporal("venta_individual","venta_individual",$("#idcliente_venta_individual").val(),4,false);
				},
				error : function(exception) {
					new PNotify({
					      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					      text: exception.responseText,
						  type: "error",
						  delay: 5000,
						  buttons: { closer:true, sticker: false }			  
					   });		
				}
			});		   
	   }).on('pnotify.cancel', function() {
	   });		 

}); 

//********************************************************************************
$("#button_venta_individual_prerreservar").on("click", function(e) {
	
	showButtonSpinner("#button_venta_individual_prerreservar");
	
	var lineasdetalle= dt_listventaindividual.rows().data();
	if (lineasdetalle.length <= 0) {
		hideSpinner("#button_venta_individual_prerreservar");
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.prerreserva_vacia" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/actualizar_prerreserva.do'/>",
		timeout : 100000,
		data: {
			xml: construir_xml_lineasdetalle(lineasdetalle)
		},
		success : function(data) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.prerreserva_realizada" />',
			      text: '<spring:message code="venta.ventareserva.list.alert.productos.prerreserva_realizada" />',
				  type: "info",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			
			hideSpinner("#button_venta_individual_prerreservar");
		},
		error : function(exception) {
			hideSpinner("#button_venta_individual_prerreservar");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});
});

//********************************************************************************
$("#button_venta_individual_vender").on("click", function(e) {
	ventaParcial = false;
	
	$("#paisventaParcial").val("");
	$("#cpVentaParcial").val("");
	

	
	if (dt_listventaindividual.rows().data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.venta_vacia" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	showButtonSpinner("#button_venta_individual_vender");	

	var data = $("#form_selector_venta_individual").serializeObject();

	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_vender_venta_individual.do'/>?idcanal=${sessionScope.idcanal}", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
});

//********************************************************************************
$("#button_venta_individual_combinar").on("click", function(e) {
	
	if (dt_listventaindividual.rows( { selected: true }).data().length<=1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.combinados.no_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#button_venta_individual_combinar");	
	
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_list_combinados.do'/>?tabla=venta_individual&list=dt_listventaindividual", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "xs");
	});
});

//********************************************************************************
$("#button_venta_individual_descombinar").on("click", function(e) {
	
	if (dt_listventaindividual.rows( { selected: true }).data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	if (dt_listventaindividual.rows( { selected: true }).data()[0][0].Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.length>0)
	{
		showButtonSpinner("#button_venta_individual_descombinar");	
		
		$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_list_productos.do'/>?tabla=venta_individual&list=dt_listventaindividual", function() {
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "xs");
		});
	}else
	{
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.producto_no_combinado" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	
});

//********************************************************************************
$("#button_disp_venta_individual").on("click", function(e) {
	showButtonSpinner("#button_disp_venta_individual");	

	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_tabla_disponibilidad.do'/>?button=disp_venta_individual", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
});

//********************************************************************************
$("#button_venta_individual_horas").on("click", function(e) {
	showButtonSpinner("#button_venta_individual_horas");	

	var lineasdetalle= dt_listventaindividual.rows().data();
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/show_hora_presentacion.do'/>",
		timeout : 100000,
		data: {
			button: "venta_individual_horas",
			list: "dt_listventaindividual",
			xml: construir_xml_lineasdetalle(lineasdetalle)
		},
		success : function(data) {
			$("#modal-dialog-form .modal-content").html(data);
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "sm");
		},
		error : function(exception) {
			hideSpinner("#button_venta_individual_horas");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});
});
//**********************************************
    var celda;
    $("#datatable_list_venta_individual tbody").delegate("td", "click", function() {
    	celda=this;
    	var posicionCelda=0;    	
    	posicionCelda=$("td", celda).context.cellIndex;
    	if (posicionCelda=="4" || posicionCelda=="5" || posicionCelda=="6")    		
    		window.setTimeout(editarLineaDetalleVentaIndividual, 100);
    });   
  
    //***************************************************************************************
    
    function editarLineaDetalleVentaIndividual()
    {
    	
    	if (dt_listventaindividual.rows( { selected: true }).data().length<=0) {
    		return;
    	}	
    	
    	if (dt_listventaindividual.rows( { selected: true }).data().length>1) {
    		new PNotify({
    		      title: '<spring:message code="common.dialog.text.error" />',
    		      text: '<spring:message code="venta.ventareserva.list.alert.productos.seleccion_multiple" />',
    			  type: "error",
    			  delay: 5000,
    			  buttons: { sticker: false }
    		   });
    		return;
    	}	
    	
    	var data = $("#form_selector_venta_individual").serializeObject(),
		rowdata= dt_listventaindividual.rows( { selected: true }).data();
    	
    	var formdata = $("#form_selector_venta_individual").serializeObject(),
		rowdata= dt_listventaindividual.rows( { selected: true }).data();
	
		var sesiones = rowdata[0][0].Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion;
		var map = "<tipoProducto>";
		if (sesiones.length>0)
			{
			for(i=0;i<sesiones.length;i++)
				{
				var session = sesiones[i].zonasesion.sesion;
				var id_producto_sesion = ""+session.contenido.tipoproducto.idtipoproducto;
				var fecha_sesion = session.fecha;
				map +="<entry><int>"+id_producto_sesion+"</int><date>"+fecha_sesion+"</date></entry>"			
				}
			}
		else
			{
			var session = sesiones.zonasesion.sesion;
			var id_producto_sesion = ""+session.contenido.tipoproducto.idtipoproducto;
			var fecha_sesion = session.fecha;
			map +="<entry><int>"+id_producto_sesion+"</int><date>"+fecha_sesion+"</date></entry>"			
			}
		
		map +="</tipoProducto>";
    	
    	
		showSpinner("#tab_venta_individual");
		$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_edit_sesion.do'/>?idproducto="+rowdata[0][1]+"&idcliente="+data.idcliente_venta_individual+"&producto="+encodeURIComponent(rowdata[0][2])+"&fecha="+data.fecha_venta_individual+"&idtarifa="+dt_listventaindividual.rows( { selected: true } ).data()[0][13]+"&map="+map+"&idcanal=${sessionScope.idcanal}&idxnumero=6&idxunitario=9&idxtotal=10&idxbono=12&&idxtarifa_txt=7&idxtarifa_id=13&idxdescuento_txt=8&idxdescuento_id=14&idxperfil=15&button=venta_individual&list=dt_listventaindividual", function() {
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "lg");
		});
		
    }
    
    
  //********************************************************************************
    $("#button_venta_individual_vender_sel").on("click", function(e) {
       ventaParcial = true;
       
       if (dt_listventaindividual.rows({ selected: true }).data().length<=0) {
   		new PNotify({
   		      title: '<spring:message code="common.dialog.text.error" />',
   		      text: '<spring:message code="venta.ventareserva.list.alert.productos.venta_vacia" />',
   			  type: "error",
   			  delay: 5000,
   			  buttons: { sticker: false }
   		   });
   		return;
   	}
   	
   	showButtonSpinner("#button_venta_individual_vender_sel");
       
	   	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_vender_venta_individual.do'/>?idcanal=${sessionScope.idcanal}", function() {
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "md");
		});
    })
    
    //***************************************************************************************    
    $("#idcliente_venta_individual").blur(function() {
    	var idCliente =""+$("#idcliente_venta_individual").val(); 
    	
    	if(idCliente!="")
    	{
    	$.ajax({
    		contenttype: "application/json; charset=utf-8",
    		type : "post",
    		url : "<c:url value='/ajax/facturacion/buscarClientePorId.do'/>",
    		timeout : 100000,
    		data: {
    			idcliente: idCliente,    			
    		},
    		success : function(data) {    		     
    				$("#cliente_venta_individual").val(data.Cliente.nombrecompleto); 
    				$("#cifcliente_venta_individual").val(data.Cliente.identificador); 
    				$("#cpcliente_venta_individual").val(data.Cliente.direccionorigen.cp.cp); 
    				$("#emailcliente_venta_individual").val(data.Cliente.direccionorigen.correoelectronico); 
    				$("#telefonocliente_venta_individual").val(data.Cliente.direccionorigen.telefonofijo); 
    				$("#telefonomovilcliente_venta_individual").val(data.Cliente.direccionorigen.telefonomovil);
    				obtener_totales_venta_temporal("venta_individual","venta_individual",$("#idcliente_venta_individual").val(),4,false);
    				
    		},
    		error : function(exception) {    	
    			$("#idcliente_venta_individual").val("");
		    	$("#cliente_venta_individual").val("");
		    	$("#cpcliente_venta_individual").val(""); 
				$("#emailcliente_venta_individual").val(""); 
				$("#telefonocliente_venta_individual").val(""); 
				$("#telefonomovilcliente_venta_individual").val("");
				obtener_totales_venta_temporal("venta_individual","venta_individual",$("#idcliente_venta_individual").val(),4,false);

    			new PNotify({
    			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
    			      text: exception.responseText,
    				  type: "error",
    				  delay: 5000,
    				  buttons: { closer:true, sticker: false }			  
    			   });		
    		}
    	});
    	}
    	else
    		{
    		$("#idcliente_venta_individual").val("");
	    	$("#cliente_venta_individual").val("");
	    	$("#cpcliente_venta_individual").val(""); 
			$("#emailcliente_venta_individual").val(""); 
			$("#telefonocliente_venta_individual").val(""); 
			$("#telefonomovilcliente_venta_individual").val("");
			obtener_totales_venta_temporal("venta_individual","venta_individual",$("#idcliente_venta_individual").val(),4,false);
    		}
    		
    })
    
    
	 //********************************************************************************
	$("#button_venta_individual_multisesion").on("click", function(e) {
		
		tabla = dt_listventaindividual;
		var filas = tabla.rows().data();
		var xml = "<parametro><arrayProductoTipoProductoYFechas>";
		
		
		if(filas.length>0)
			{
			for(i=0;i<filas.length;i++)
				{
				
				var lineaDetalle = filas[i][0].Lineadetalle;				
				xml+="<ProductoTipoProductoYFechas>";
				xml+="<idProducto>"+lineaDetalle.producto.idproducto+"</idProducto>";
				var sesiones = lineaDetalle.lineadetallezonasesions.Lineadetallezonasesion;
				xml += "<tipoProducto>";
				if (sesiones.length>0)
					{
					for(j=0;j<sesiones.length;j++)
						{
						var session = sesiones[j].zonasesion.sesion;
						var id_producto_sesion = ""+session.contenido.tipoproducto.idtipoproducto;
						var fecha_sesion = session.fecha;
						xml +="<entry><int>"+id_producto_sesion+"</int><date>"+fecha_sesion+"</date></entry>"			
						}
					}
				else
					{
					var session = sesiones.zonasesion.sesion;
					var id_producto_sesion = ""+session.contenido.tipoproducto.idtipoproducto;
					var fecha_sesion = session.fecha;
					xml +="<entry><int>"+id_producto_sesion+"</int><date>"+fecha_sesion+"</date></entry>"			
					}
				
				xml +="</tipoProducto>";
				xml+="</ProductoTipoProductoYFechas>";				
				}			
			}
			
		else
			{			
			lineaDetalle = filas[0][0].Lineadetalle;
			xml+="<ProductoTipoProductoYFechas>";
			xml+="<idProducto>"+lineaDetalle.producto.idproducto+"</idProducto>";
			var sesiones = lineaDetalle.lineadetallezonasesions.Lineadetallezonasesion;
			xml+="<tipoProducto>";
			if (sesiones.length>0)
				{
				for(i=0;i<sesiones.length;i++)
					{
					var session = sesiones[i].zonasesion.sesion;
					var id_producto_sesion = ""+session.contenido.tipoproducto.idtipoproducto;
					var fecha_sesion = session.fecha;
					xml +="<entry><int>"+id_producto_sesion+"</int><date>"+fecha_sesion+"</date></entry>"			
					}
				}
			else
				{
				var session = sesiones.zonasesion.sesion;
				var id_producto_sesion = ""+session.contenido.tipoproducto.idtipoproducto;
				var fecha_sesion = session.fecha;
				xml +="<entry><int>"+id_producto_sesion+"</int><date>"+fecha_sesion+"</date></entry>"			
				}
			
			xml +="</tipoProducto>";			
			xml+="</ProductoTipoProductoYFechas>";
			}
			
		xml+="</arrayProductoTipoProductoYFechas>";
		xml+="<fecha>"+$("#fecha_venta_individual").val()+"</fecha>";
		xml +="</parametro>";
		
		var formdata = $("#form_selector_venta_individual").serializeObject();
				
		$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_multisesion.do'/>?idproducto=1&idcliente="+formdata.idcliente_venta_individual+"&producto=1&fecha="+formdata.fecha_venta_individual+"&xml="+xml+"&button=venta_individual&list=dt_listventaindividual&edicion=1", function() {
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "md");
		});
	});
</script>

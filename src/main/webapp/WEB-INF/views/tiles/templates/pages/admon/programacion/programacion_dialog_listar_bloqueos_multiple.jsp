<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="administracion.programacion.programacion.dialog.bloqueos.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_establecer_bloqueos" class="form-horizontal form-label-left">
	
		<div class="form-group">
	
			<div id="tabla-bloqueos" class="col-md-12 col-sm-12 col-xs-12">
			
				<div class="btn-group pull-right btn-datatable">
					<a type="button" class="btn btn-info" id="button_bloqueos_new">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.dialog.bloqueos.list.button.nuevo" />"> <span class="fa fa-plus"></span>
						</span>
					</a>
				</div>
			
				<div class="col-md-12 col-sm-12 col-xs-12">
					<table id="datatable-list-bloqueos" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th><spring:message code="administracion.programacion.programacion.dialog.bloqueos.list.header.nrolocalidadesbloqueadas" /></th>
								<th><spring:message code="administracion.programacion.programacion.dialog.bloqueos.list.header.motivobloqueo" /></th>
								<th><spring:message code="administracion.programacion.programacion.dialog.bloqueos.list.header.usuario" /></th>
								<th><spring:message code="administracion.programacion.programacion.dialog.bloqueos.list.header.observaciones" /></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					<span>&nbsp;</span>
				</div>
			</div>
		</div>

	</form>

	<div class="modal-footer">
		<button type="button" class="btn btn-cancel close_dialog" id="button_close">
			<spring:message code="common.button.close" />
		</button>
	</div>
	
</div>

<script>

hideSpinner("#button_programacion_bloqueos");
showSpinner('#tabla-bloqueos');

var dt_listbloqueos=$('#datatable-list-bloqueos').DataTable( {
	language: dataTableLanguage,
	processing: false,
	scrollY: "200px",
	scrollCollapse: true,
	paging: false,
	info: false,
	searching: false,
    select: { style: 'single'},
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_verafluencia.data().count()>0) dt_verafluencia.columns.adjust().draw(); });
        hideSpinner('#tabla-bloqueos');
	},
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-bloqueos') }
} );

function ajustar_cabeceras_datatable()
{
	$('#datatable-list-bloqueos').DataTable().columns.adjust().draw();
}

setTimeout(ajustar_cabeceras_datatable, 200);

//********************************************************************************
$("#button_bloqueos_new").on("click", function(e) {
	
	var button_bloqueos= "button_bloqueos_new",
		lista_bloqueos= "dt_listbloqueos";
	showButtonSpinner("#"+button_bloqueos);
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/show_new_bloqueo.do'/>?id="+"${idSesion}"+"&idmultiple="+"${idSesionMultiple}"+"&button="+button_bloqueos+"&lista="+lista_bloqueos, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "sm");
	});
});

 
//********************************************************************************
$("#button_close").on("click", function(e) {
	
	$("#modal-dialog-form").modal('hide');
});

</script>


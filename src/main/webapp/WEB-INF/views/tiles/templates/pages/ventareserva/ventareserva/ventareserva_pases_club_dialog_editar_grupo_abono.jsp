<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${listadogrupo_pasesclub}" var="listadogrupo_pasesclub_xml" />
<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.tabs.gestion_pases_club-dialog.listado-pases.title" />
	</h4>
</div>
<div class="modal-body">

<div class="btn-group pull-right btn-datatable">
			<a type="button" class="btn btn-info" id="tab_pases_club_listado_add">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.gestion_pases_club.button.add" />">
					<span class="fa fa-plus"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="tab_pases_club_listado_edit">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.gestion_pases_club.button.editar" />"> <span class="fa fa-pencil"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="tab_pases_club_listado_establecertitutar">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.gestion_pases_club.button.establecertitutal" />"> <span class="fa fa-male"></span>
				</span>
			</a>	
			<a type="button" class="btn btn-info" id="tab_pases_club_listado_bloquear_desbloquear">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.gestion_pases_club.button.bloquear_desbloquear" />"> <span class="fa fa-lock"></span>
				</span>
			</a>	
			<a type="button" class="btn btn-info" id="tab_pases_club_listado_renovar">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.gestion_pases_club.button.renovar" />"> <span class="fa fa-check"></span>
				</span>
			</a>	
			<a type="button" class="btn btn-info" id="tab_pases_club_listado_cancelar">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.gestion_pases_club.button.cancelar" />"> <span class="fa fa-times"></span>
				</span>
			</a>	
			<a type="button" class="btn btn-info" id="tab_pases_club_listado_imprimir">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.gestion_pases_club.button.imprimir" />"> <span class="fa fa-print"></span>
				</span>
			</a>			
		</div>

	<table id="listado_pases_club_table" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
			  <th></th>
			  <th id="idabono"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.listadogrupo.field.idabono"/></th>
			  <th id="titular"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.listadogrupo.field.titular"/></th>
			  <th id="nombre"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.listadogrupo.field.nombre" /></th>
			  <th id="tipopase"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.listadogrupo.field.tipopase" /></th>							  						 
			  <th id="nif"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.listadogrupo.field.nif" /></th>
			  <th id="rol"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.listadogrupo.field.rol" /></th>
			  <th id="falta"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.listadogrupo.field.fechaalta" /></th>
			  <th id="vigencia"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.listadogrupo.field.vigencia" /></th>
			  <th id="estado"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.listadogrupo.field.estado" /></th>
			   <th></th>
			</tr>
		</thead>
		<tbody>				
			
		</tbody>				
	</table>			

	<div class="modal-footer">
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>		

</div>
<script>
//*****************************************
hideSpinner("#tab_pases_club_edit");

var listadogrupo_pasesclub_json=${listadogrupo_pasesclub_json};


//*****************************************
 var dt_listpaseclubeditar =$('#listado_pases_club_table').DataTable( {
	ordering: false,
	pagingType: "simple",
	info: false,
	deferLoading: 0,	   
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-pases_club') },
    //GGL Multiseleccion select: { style: 'single'},
    select: { style: 'os'},//, selector:'td:not(:nth-child(3)):not(:nth-child(4))' GGL quito esto para que el clic funcione en el tercer y cuarto campo tb
	language: dataTableLanguage,
	columnDefs: [
	             { "visible": false, "targets": [0,10]}
	         ],
	processing: true,
} );


 var json= ${listadogrupo_pasesclub_json};
 CargarMiembros();
 
//****************************************************************
 function CargarMiembros()
 	{
 		var miembros = json.TreeSet.PaseDtoSalida; 	 		
 		if (miembros.length>0)
          $.each(miembros, function(key, miembro){
            	CargarMiembro(miembro)
            });        	
        else        	
        	CargarMiembro(miembros) 	
 	
 	}
 		
 	
	//*****************************************************************************************
	function CargarMiembro(miembro)
	{


 		var fecha_inicio = miembro.fechaAlta.substring(0,10); 		 
 		var fecha_fin = miembro.finVigencia.substring(0,10);
 		
 		var titular = miembro.titular;
 		var img_titular="";
 		var id_pase = miembro.identificador.idpase;
		if(titular==1)
			{
			if(miembro.estado=="ACTIVO")
				str_titular = "<titularGrupo><idpase>"+id_pase+"</idpase><esAbono>1</esAbono></titularGrupo>";
			img_titular='<p align="center"> <i class="fa fa-check" aria-hidden="true"> </p>' ;
			}
		else
			img_titular='<p align="center"><i class="fa fa-close" aria-hidden="true"> </p>';	

		
		
		dt_listpaseclubeditar.row.add([
						miembro.grupo,
						id_pase,
						img_titular,
						miembro.nombreTitular,
						miembro.tipoPase,
						miembro.nif,
						miembro.parentesco,
						fecha_inicio,
						fecha_fin,
						miembro.estado,
						miembro
					]).draw();
	}

		

	//***************************************************************
	$("#tab_pases_club_listado_add").on("click", function(e) { 
		
		 list_abonos= []; 
		
		$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/pases_club/new_abono_miembro.do'/>", function() {
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "md");
		});	
	})
	
	
//***************************************************************
$("#tab_pases_club_listado_establecertitutar").on("click", function(e) {  
  
  	var data = dt_listpaseclubeditar.rows( { selected: true } ).data();
	
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.list.titular.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.list.titular.alert.multiple_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	var id_pase = dt_listpaseclubeditar.rows( { selected: true } ).data()[0][1];
	var miembro = dt_listpaseclubeditar.rows( { selected: true } ).data()[0][10];
		
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/abono/establecerTitular.do'/>",
		timeout : 100000,
		data: {
			idpase: id_pase,
			abono:miembro.identificador.esAbono
		},
		success : function(data) {
			insertSmallSpinner("#listado_pases_club_table_processing");
			var id_grupo = dt_listpaseclubeditar.rows( { selected: true } ).data()[0][0];
			
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.list.titular.alert.success" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
			
			$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/pases_club/editar_grupo_abono.do'/>?id="+id_grupo, function() {
				$("#modal-dialog-form").modal('show');
				setModalDialogSize("#modal-dialog-form", "md");
			});			
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
});



//***************************************************************
$("#tab_pases_club_listado_bloquear_desbloquear").on("click", function(e) {  
  
  	var data = dt_listpaseclubeditar.rows( { selected: true } ).data();
	
	
	var xml = "<list>";
	for(i=0;i<data.length;i++)
		{
		var id_pase = dt_listpaseclubeditar.rows( { selected: true } ).data()[i][1];
		xml += "<IdentificadorPasesDto><idpase>"+id_pase+"</idpase><esAbono>1</esAbono></IdentificadorPasesDto>";
		}
	xml +="</list>";
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/abono/bloquearPases.do'/>",
		timeout : 100000,
		data: {
			seleccion: xml
		},
		success : function(data) {
			insertSmallSpinner("#listado_pases_club_table_processing");
			var id_grupo = dt_listpaseclubeditar.rows( { selected: true } ).data()[0][0];
			
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.list.bloquear.alert.success" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
			
			$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/pases_club/editar_grupo_abono.do'/>?id="+id_grupo, function() {
				$("#modal-dialog-form").modal('show');
				setModalDialogSize("#modal-dialog-form", "md");
			});			
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
});

//***************************************************************
$("#tab_pases_club_listado_cancelar").on("click", function(e) {  
  
  	var data = dt_listpaseclubeditar.rows( { selected: true } ).data();
	
	
	var xml = "<list>";
	for(i=0;i<data.length;i++)
		{
		var id_pase = dt_listpaseclubeditar.rows( { selected: true } ).data()[i][1];
		xml += "<IdentificadorPasesDto><idpase>"+id_pase+"</idpase><esAbono>1</esAbono></IdentificadorPasesDto>";
		}
	xml +="</list>";
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/abono/cancelarPases.do'/>",
		timeout : 100000,
		data: {
			seleccion: xml
		},
		success : function(data) {
			insertSmallSpinner("#listado_pases_club_table_processing");
			var id_grupo = dt_listpaseclubeditar.rows( { selected: true } ).data()[0][0];
			
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.list.cancelar.alert.success" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
			
			$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/pases_club/editar_grupo_abono.do'/>?id="+id_grupo, function() {
				$("#modal-dialog-form").modal('show');
				setModalDialogSize("#modal-dialog-form", "md");
			});			
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
});

//***************************************************************
$("#tab_pases_club_listado_imprimir").on("click", function(e) { 
	var data = dt_listpaseclubeditar.rows( { selected: true } ).data();	
	
	
	
	var xml = "<ParamSolicitudDatosImpresion><identificadores>";
	for(i=0;i<data.length;i++)
		{	
		var id_pase = dt_listpaseclubeditar.rows( { selected: true } ).data()[i][1];
		var estado = dt_listpaseclubeditar.rows( { selected: true } ).data()[i][9];		
		xml += "<identificador><esAbono>1</esAbono><idpase>"+id_pase+"</idpase></identificador>";
		}
	xml +="</identificadores></ParamSolicitudDatosImpresion>";
	
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/pases_club/listado_impresion.do'/>",
		timeout : 100000,
		data: {
			xml: xml
		},
		success : function(data) {
			
			json= data;
			
			$("#modal-dialog-form-4 .modal-content").load("<c:url value='/ajax/venta/ventareserva/pases_club/pantalla_impresion.do'/>?xml="+xml, function() {
				$("#modal-dialog-form-4").modal('show');
				setModalDialogSize("#modal-dialog-form-4", "xs");
			});			
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
	
	
	
		
})


//***************************************************************
	$("#tab_pases_club_listado_renovar").on("click", function(e) { 		
	
		var data = dt_listpaseclubeditar.rows( { selected: true } ).data();
		
		
		if (data.length<=0) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.list.renovar.alert.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}
		/* TODO Comprobar que esten caducados o directamente le sumamos un año si es MH ¿?¿? SACAR AVISO*/
		for(i=0;i<data.length;i++){
			var caducidad = dt_listpaseclubeditar.rows( { selected: true } ).data()[i][8];
			//alert("caducidad "+caducidad+ " new Date "+new Date());
			var parts = caducidad.split("/");
			caducidad = new Date(parts[2], parts[1] - 1, parts[0]);
			//alert("caducidad "+caducidad+ " new Date "+new Date());
			if (caducidad > new Date()) {
				alert("Compara bien las fechas ");
				new PNotify({
				      title: '<spring:message code="common.dialog.text.atencion" />',
				      text: '<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.list.renovar.alert.no_caducado" />',
					  type: "notice",
					  delay: 10000,
					  buttons: { sticker: false }
				   });				
			}
		}
		
		/*if (data.length>1) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.list.renovar.alert.multiple_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}*/
		
		
							
				$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/venta/ventareserva/pases_club/pantalla_renovacion.do'/>", function() {
					$("#modal-dialog-form-2").modal('show');
					setModalDialogSize("#modal-dialog-form-2", "xs");
				});			
			
	});	

//***************************************************************
$("#tab_pases_club_listado_edit").on("click", function(e) { 		

		var data = dt_listpaseclubeditar.rows( { selected: true } ).data();
		
		
		if (data.length<=0) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.list.renovar.alert.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}	
		
		if (data.length>1) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.list.renovar.alert.multiple_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}
		
		var id_pase = dt_listpaseclubeditar.rows( { selected: true } ).data()[0][1];
		var xml = "<IdentificadorPasesDto><idpase>"+id_pase+"</idpase><esAbono>1</esAbono></IdentificadorPasesDto>";
			
	
		$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/venta/ventareserva/pases_club/editar_pase.do'/>?xml="+xml, function() {
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "md");
		});			
		
	});	

</script>
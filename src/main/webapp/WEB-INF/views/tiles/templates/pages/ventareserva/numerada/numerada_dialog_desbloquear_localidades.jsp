<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="modal fade" id="modal-dialog-localidades-desbloquear"
	role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close close_dialog" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">�</span>
				</button>
				<h4 class="modal-title">
					<spring:message code="venta.numerada_busquedas.dialog.desbloquear_localidades.numerada.title" />			
				</h4>
			</div>
			<div class="modal-body">
				<p class="modal-text"><spring:message code="venta.numerada_busquedas.dialog.desbloquear_localidades.numerada.text" /></p>
				<div class="modal-footer">
					<button type="button" class="btn btn-success close_dialog" id="tab_localidades_dialog_desbloquear_si_vender"
						data-dismiss="modal">
						<spring:message code="venta.numerada_busquedas.dialog.desbloquear_localidades.button.vender" />
					</button>
					<button type="button" class="btn btn-success close_dialog" id="tab_localidades_dialog_desbloquear_no_vender"
						data-dismiss="modal">
						<spring:message code="venta.numerada_busquedas.dialog.desbloquear_localidades.button.desbloquear" />
					</button>
					<button type="button" class="btn btn-success close_dialog" id="tab_localidades_dialog_desbloquear_liberar"
						data-dismiss="modal">
						<spring:message code="venta.numerada_busquedas.dialog.desbloquear_localidades.button.liberar" />
					</button>
				</div>

			</div>
			
		</div>
	</div>
</div>

<!--  -->

<script>
//********************************************************************************
$("#tab_localidades_dialog_desbloquear_no_vender").on("click", function(e) { 

});


//********************************************************************************
$("#tab_localidades_dialog_desbloquear_si_vender").on("click", function(e) { 
	xml = generarXmlLocalidades();
	
	$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/venta/numerada/vender_localidades_bloqueadas.do'/>?"+encodeURI("xml="+xml), function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "lg");
	});			
		
});	


//********************************************************************************
$("#tab_localidades_dialog_desbloquear_liberar").on("click", function(e) { 
	xml = generarXmlLocalidades();
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/desbloquear_localidades.do'/>",
		timeout : 100000,
		data: {
				xml: xml
			  }, 
		success : function(data) {
			cambiarZona();
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,				  
				  buttons: { sticker: false }				  
			   });		
		}
	});	
	
	
	
});	



</script>


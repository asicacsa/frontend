<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">x</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="administracion.programacion.programacion.dialog.remove_bloqueos.title" />
	</h4>
</div>

<div class="modal-body" id="principal">

	<form id="form_remove_bloqueo_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		
		<input name="idestadolocalidad" id="idestadolocalidad" type="hidden">
		
		<fieldset id="check-borrado">
		
			<div class="form-group">
				<div class="checkbox col-md-12 col-sm-12 col-xs-12">
					<input type="radio" name="check-borrado" id="borrado-todo" checked value="" data-parsley-multiple="inicio_sesion" class="flat" style="position: absolute; opacity: 0;">
					<strong><spring:message code="administracion.programacion.programacion.dialog.remove_bloqueos.field.borrar-todos" /></strong>
				</div>
			</div>

			<div class="form-group">
				<div class="checkbox col-md-6 col-sm-6 col-xs-12">
					<input type="radio" name="check-borrado" id="borrado-parte" value="" data-parsley-multiple="inicio_sesion" class="flat" style="position: absolute; opacity: 0;">
					<strong><spring:message code="administracion.programacion.programacion.dialog.remove_bloqueos.field.borrar-parte" /></strong>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<label class="control-label"><spring:message code="administracion.programacion.programacion.dialog.remove_bloqueos.field.label-borrar-parte" /></label>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6">
					<input name="cantidad-borrado" id="cantidad-borrado" disabled="disabled" type="text" class="form-control" value="">
				</div>
			</div>
		
		</fieldset>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="delete_button" type="submit" class="btn btn-primary save_dialog">
					<spring:message code="common.button.delete" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
		
	</form>

</div>

<script>
	hideSpinner("#button_bloqueos_remove");

	$(":input").inputmask();
	
	if ($("input.flat")[0]) {
        $(document).ready(function () {
            $('input.flat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
    }
	
	$('input[name="idestadolocalidad"]').val("${idEstadoLocalidad}");

    $("#borrado-parte").on("ifChanged", function(e) {
    	if ($('input[name="cantidad-borrado"]').attr('disabled')) 
    		$('input[name="cantidad-borrado"]').removeAttr('disabled');
        else 
        	$('input[name="cantidad-borrado"]').attr('disabled', 'disabled');
    });	

	$("#form_remove_bloqueo_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			removeBloqueo();
		}
	});

	//********************************************************************************	
	function removeBloqueo() {
		
		var data = $("#form_remove_bloqueo_data").serializeObject();

		showSpinner("#modal-dialog-form-3 .modal-content");

		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/admon/programacion/remove_bloqueos.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form-3 .modal-content");
				dt_listbloqueos.ajax.reload(null,false);						
				$("#modal-dialog-form-3").modal('hide');
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form-3 .modal-content");
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
	}
</script>

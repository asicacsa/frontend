<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<c:choose>
			<c:when test="${esProgramacion}">
				<spring:message code="administracion.programacion.programacion.dialog.bloqueos.title" />
			</c:when>
			<c:otherwise>
				<spring:message code="venta.ventareserva.dialog.bloqueos.title" />
			</c:otherwise>
		</c:choose>
	</h4>
</div>

<div class="modal-body">

	<form id="form_establecer_bloqueos" class="form-horizontal form-label-left">
	
		<div class="form-group">
	
			<div id="tabla-bloqueos" class="col-md-12 col-sm-12 col-xs-12">
			
				<div class="btn-group pull-right btn-datatable">
					<a type="button" class="btn btn-info" id="button_bloqueos_new">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.dialog.bloqueos.list.button.nuevo" />"> <span class="fa fa-plus"></span>
						</span>
					</a>
					<a type="button" class="btn btn-info" id="button_bloqueos_remove">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.dialog.bloqueos.list.button.eliminar" />"> <span class="fa fa-trash"></span>
						</span>
					</a>
					<a type="button" class="btn btn-info" id="button_bloqueos_info">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.dialog.bloqueos.list.button.info" />"> <span class="fa fa-question"></span>
						</span>
					</a>
				</div>
			
				<div class="col-md-12 col-sm-12 col-xs-12">
					<table id="datatable-list-bloqueos" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th><spring:message code="administracion.programacion.programacion.dialog.bloqueos.list.header.idestadolocalidad" /></th>
								<th><spring:message code="administracion.programacion.programacion.dialog.bloqueos.list.header.motivobloqueo" /></th>
								<th><spring:message code="administracion.programacion.programacion.dialog.bloqueos.list.header.observaciones" /></th>
								<th><spring:message code="administracion.programacion.programacion.dialog.crear_bloqueo.field.zona" /></th>
								<th><spring:message code="administracion.programacion.programacion.dialog.bloqueos.list.header.nrolocalidadesbloqueadas" /></th>
								<th><spring:message code="ventareserva.operacionescaja.editarcaja.list.header.fecha" /></th>
								<th><spring:message code="administracion.programacion.programacion.dialog.bloqueos.list.header.usuario" /></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					<span>&nbsp;</span>
				</div>
			</div>
		</div>

	</form>

	<div class="modal-footer">
		<button type="button" class="btn btn-cancel close_dialog" id="button_close">
			<spring:message code="common.button.close" />
		</button>
	</div>
	
</div>

<script>
if (!${esProgramacion}) {
	$("#button_bloqueos_new").hide();
	$("#button_bloqueos_info").hide();
}

hideSpinner("#button_programacion_bloqueos");
showSpinner('#tabla-bloqueos');

var dt_listbloqueos=$('#datatable-list-bloqueos').DataTable( {
	language: dataTableLanguage,
	processing: false,
	scrollY: "200px",
	scrollCollapse: true,
	paging: false,
	info: false,
	searching: false,
    select: { style: 'single'},
    ajax: {
        url: "<c:url value='/ajax/admon/programacion/list_bloqueos.do'/>",
        rowId: 'idestadolocalidad',
        type: 'POST',
        data: {
        	id: "${idSesion}"
        },
        dataSrc: function (json) { console.log(json);if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.Altasesionparam.sesionPlantilla.estadolocalidads.Estadolocalidad)); return(""); }
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_verafluencia.data().count()>0) dt_verafluencia.columns.adjust().draw(); });
        hideSpinner('#tabla-bloqueos');
	},
	columnDefs: [
	             { "targets": 0, "visible": false }
	         ],
    columns: [
        { data: "idestadolocalidad", className: "cell_centered", type: "spanish-string", defaultContent: "" },
        { data: "motivobloqueo", type: "spanish-string", defaultContent: "" },
        { data: "observaciones", type: "spanish-string", defaultContent: "" },
        { data: "zona.nombre", type: "spanish-string", defaultContent: "" },
        { data: "nrolocalidadesbloqueadas", className: "cell_centered", type: "spanish-string", defaultContent: "" },
        { data: "fecha", type: "spanish-string", defaultContent: "" },      
        { data: "usuario.login", type: "spanish-string", defaultContent: "" }        
     ],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-bloqueos') }
} );

function ajustar_cabeceras_datatable()
{
	$('#datatable-list-bloqueos').DataTable().columns.adjust().draw();
}

setTimeout(ajustar_cabeceras_datatable, 200);

insertSmallSpinner("#datatable-list-bloqueos_processing");

//********************************************************************************
$("#button_bloqueos_new").on("click", function(e) {
	
	var button_bloqueos= "button_bloqueos_new",
		lista_bloqueos= "dt_listbloqueos";
	showButtonSpinner("#"+button_bloqueos);
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/show_new_bloqueo.do'/>?id="+"${idSesion}"+"&button="+button_bloqueos+"&lista="+lista_bloqueos, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "sm");
	});
});

//********************************************************************************
$("#button_bloqueos_remove").on("click", function(e) {
	
	var data = sanitizeArray(dt_listbloqueos.rows( { selected: true } ).data(),"idestadolocalidad");
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.programacion.programacion.dialog.remove_bloqueos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	

	showButtonSpinner("#button_bloqueos_remove");
	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/admon/programacion/show_remove_bloqueos.do'/>?id="+data[0], function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "xs");
	});
	
}); 

//********************************************************************************
$("#button_bloqueos_info").on("click", function(e) {
	
	showButtonSpinner("#button_bloqueos_info");
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/show_info_bloqueo.do'/>?id="+"${idSesion}", function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "sm");
	});
});


//********************************************************************************
$("#button_close").on("click", function(e) {
	
	if (${esProgramacion}) /* Se llama desde programación */ 
		$("#modal-dialog-form").modal('hide');
	else { /* Se llama desde Venta y Reserva */
		var bloqueadas=0;
		$.each(dt_listbloqueos.column(1).data(),function() { bloqueadas+=this; });
		var desbloqueadas= parseInt(${listaBloqueos}.rows( { selected: true } ).data()[0][${idxBloqueadas}])-parseInt(bloqueadas) 
		${listaBloqueos}.rows( { selected: true } ).data()[0][${idxLibres}]=parseInt(${listaBloqueos}.rows( { selected: true } ).data()[0][${idxLibres}])+parseInt(desbloqueadas);
		${listaBloqueos}.rows( { selected: true } ).data()[0][${idxBloqueadas}]=parseInt(${listaBloqueos}.rows( { selected: true } ).data()[0][${idxBloqueadas}])-parseInt(desbloqueadas);
		${listaBloqueos}.rows().invalidate().draw();
		$("#modal-dialog-form-2").modal('hide');
	}

});

</script>


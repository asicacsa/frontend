<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div id="principal" class="row x_panel">
	<div class="x_content" style="display: block;">

		<form id="form_aportacioncaja_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		    <input id="idCaja" name="idcaja" type="hidden" value="" />
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="ventareserva.operacionescaja.aportacioncaja.field.importe" /></label>
				<div class="col-md-3 col-sm-3 col-xs-3">
					<input name="importe" id="importe" required="required" type="text" class="form-control required" required="required" value="" disabled="disabled">
                       <span class="fa fa-euro form-control-feedback right" aria-hidden="true"></span>
				</div>
			</div>
		</form>

		<div class="modal-footer">
			<button id="save_aportacioncaja_button" type="button" class="btn btn-primary" disabled="disabled">
				<spring:message code="common.button.save" />
			</button>
		</div>

	</div>
</div>

<script>

showSpinner("#principal");

$.ajax({
	contenttype: "application/json; charset=utf-8",
	type : "post",
	url : "<c:url value='/ajax/ventareserva/operacionescaja/obtener_caja_abierta.do'/>",
	timeout : 100000,
	data: null, 
	success : function(data) {
		hideSpinner("#principal");
		if (data!=null && typeof data.Caja!=("undefined"))
			{
				$("#importe").removeAttr('disabled');
				$("#save_aportacioncaja_button").removeAttr('disabled');
				$('input[name="idcaja"]').val(data.Caja.idcaja);
			}
		else
			{
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : '<spring:message code="ventareserva.operacionescaja.aportacioncaja.alert.no_abierta" />',
					type : "info",
					delay : 5000,
					buttons : { sticker : false	}
				});
			}
		
	},
	error : function(exception) {
		hideSpinner("#principal");
		new PNotify({
		      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
		      text: exception.responseText,
			  type: "error",
			  buttons: { sticker: false }				  
		   });		
	}
});	

$("#save_aportacioncaja_button").on("click", function(e) {
	$("#form_aportacioncaja_data").submit();
})


$("#form_aportacioncaja_data").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		saveFormData();		
	}
});

//********************************************************************************	
function saveFormData() {
	var data = $("#form_aportacioncaja_data").serializeObject();
	
	showSpinner("#principal");

	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/operacionescaja/aportar_caja.do'/>",
		timeout : 100000,
		data: data, 
		success : function(data) {
			hideSpinner("#principal");
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="ventareserva.operacionescaja.aportacioncaja.alert.operacion_realizada" />',
				type : "success",
				delay : 5000,
				buttons : { sticker : false	}
			});			
			$("#importe").attr('disabled',true);
			$("#save_aportacioncaja_button").attr('disabled',true);
		},
		error : function(exception) {
			hideSpinner("#principal");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});	
}
 	
</script>

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form"/>
<tiles:insertAttribute name="modal_dialog" />

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="modal-body">
	<div class="tarjeta" id="tarjeta">	
		<div class ="fotoTarjeta">
			<img id="foto" class="imprimir"></img>
		</div>
		<div class="codBarras" id="barcode1D"></div>			  
		<div class="col-md-6 col-sm-6 col-xs-12">
    		<div id="nombreCompoleto" class="nombrePase"></div> <br/>
    		<div id="fechaCaducidad" class="caducidadPase"></div> <br/>
		</div>    
   </div> 	 

</div>
  	
<div class="modal-footer">
	<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
		<spring:message code="common.button.cancel"/>
	</button>
</div>	      
  	
  	
<script>
var imageObj;// imageObject.src = URL
mostrarPase();


function cargarImagen()
{
	 $("#tarjeta").print();
     //$("#modal-dialog-form-5").modal('hide');
}




function mostrarPase()
{
	
	
	var settings = {
	        output:"bmp",
	        bgColor: '#FFFFFF',
	        color: '#000000',
	        barWidth: '1',
	        barHeight:'20',
	        moduleSize: '5',
	        posX: '10',
	        posY: '20',
	        addQuietZone: '1'
	      };
	
	$("#barcode1D").barcode(pase.codigobarras, "code128",settings); 
	$("#barcode1D").src = $("#barcode1D").html()
	
	//GGL EN QR SERÍA
	
	/* {
	    // render method: 'canvas', 'image' or 'div'
	    render: 'canvas',

	    // version range somewhere in 1 .. 40
	    minVersion: 1,
	    maxVersion: 40,

	    // error correction level: 'L', 'M', 'Q' or 'H'
	    ecLevel: 'L',

	    // offset in pixel if drawn onto existing canvas
	    left: 0,
	    top: 0,

	    // size in pixel
	    size: 200,

	    // code color or image element
	    fill: '#000',

	    // background color or image element, null for transparent background
	    background: null,

	    // content
	    text: 'no text',

	    // corner radius relative to module width: 0.0 .. 0.5
	    radius: 0,

	    // quiet zone in modules
	    quiet: 0,

	    // modes
	    // 0: normal
	    // 1: label strip
	    // 2: label box
	    // 3: image strip
	    // 4: image box
	    mode: 0,

	    mSize: 0.1,
	    mPosX: 0.5,
	    mPosY: 0.5,

	    label: 'no label',
	    fontname: 'sans',
	    fontcolor: '#000',

	    image: null
	} */
 // O ASí SIN NADA
	/* $("#barcode1D").qrcode({'text':pase.codigobarras});
 
	$("#barcode1D").qrcode({
		 'render': 'canvas',
		 'size': 100,
		 'fill': '#1D82AF',
		 'radius': 0.5,
		 'background': '#FFFFFF',
		 'text': pase.codigobarras
		});
		 */
	
	
	
	

	setTimeout(cargarImagen, 1000);
	
	
	
	
	$("#nombreCompoleto").text(pase.nombreCompleto);
	$("#fechaCaducidad").text(pase.fechaCaducidad);
	alert("nombreFoto "+pase.nombreFoto);
	var nombre_foto = pase.nombreFoto;	
	//alert("Ruta fotos 2 "+nombre_foto);
	//Actualmente por tema de permisos no se puede acceder ni local ni al servidor 172.29.132.35
	//nombre_foto = "https://lucasleonsimon.files.wordpress.com/2015/03/alfonso-alonso.jpg";	
	//nombre_foto = ruta_fotos + nombre_foto;
	//El src espera una URL absoluta, o relativa a la página web
	 $('#foto').attr('src',nombre_foto);
	 $('#foto').width("100%"); 
	 $('#foto').height("100%");
	
	
}



</script>
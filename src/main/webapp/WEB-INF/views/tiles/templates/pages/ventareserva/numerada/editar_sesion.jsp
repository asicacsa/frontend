<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<x:parse xml="${perfiles}" var="perfiles_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>		
</div>

<div class="modal-body">
	<form id="editar_sesion" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<div class="col-md-12 col-sm-12 col-xs-12">

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada.tabs.lineas.list.perfil" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">

						<select class="form-control" name="idperfil" id="idperfil">
							<option value=""></option>
							<x:forEach select="$perfiles_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada.tabs.lineas.list.descuento" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">

						<select class="form-control" name="iddescuento" id="iddescuento">
							<option value=""></option>																
						</select>
					</div>
			  </div>			  
		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_edit_sesion" type="button" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
		
		
	</form>
</div>
<script>
var id_perfil="${idperfil}";

id_perfil="183100";

if(id_perfil!="")
	{
	$('#idperfil option[value="'+id_perfil+'"]').attr("selected", "selected");
	}

$("#save_edit_sesion").on("click", function(e) { 
	dt_listlineas.rows( { selected: true }).data()[0][0].perfilvisitante.idperfilvisitante = $("#idperfil").val();
	dt_listlineas.rows( { selected: true }).data()[0][5] = $("#idperfil option:selected").text();
	dt_listlineas.rows( { selected: true }).data()[0][0].descuentopromocional.iddescuentopromocional = $("#iddescuento").val();
	dt_listlineas.rows( { selected: true }).data()[0][6] = $("#iddescuento option:selected").text();
	dt_listlineas.rows().invalidate().draw();
	dt_listlineas.rows().deselect();	
	$("#modal-dialog-form-3").modal('hide');	
})

</script>
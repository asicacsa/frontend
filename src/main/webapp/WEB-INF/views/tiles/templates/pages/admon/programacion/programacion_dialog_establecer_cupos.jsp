<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${establecercupos_datos_cupo}" var="establecercupos_datos_cupo_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="administracion.programacion.programacion.dialog.cupos.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_establecer_cupos" class="form-horizontal form-label-left">
	
		<div class="form-group">
		
			<input name="idsesion" id="idsesion" type="hidden" />
			<input name="cupos_asignados" id="cupos_asignados" type="hidden" />
	
			<div class="col-md-12 col-sm-12 col-xs-12">
			
				<div class="btn-group pull-right btn-datatable">
					<a type="button" class="btn btn-info" id="button_cupos_new">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.dialog.cupos.list.button.nuevo" />"> <span class="fa fa-plus"></span>
						</span>
					</a>
					<a type="button" class="btn btn-info" id="button_cupos_edit">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.dialog.cupos.list.button.editar" />"> <span class="fa fa-pencil"></span>
						</span>
					</a>
					<a type="button" class="btn btn-info" id="button_cupos_remove">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.dialog.cupos.list.button.eliminar" />"> <span class="fa fa-trash"></span>
						</span>
					</a>
				</div>
			
				<div class="col-md-12 col-sm-12 col-xs-12">
					<table id="datatable-list-cupos" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th><spring:message code="administracion.programacion.programacion.dialog.cupos.list.header.idcanal" /></th>
								<th><spring:message code="administracion.programacion.programacion.dialog.cupos.list.header.canal" /></th>
								<th><spring:message code="administracion.programacion.programacion.dialog.cupos.list.header.cupo" /></th>
							</tr>
						</thead>
						<tbody>
							<x:forEach select="$establecercupos_datos_cupo_xml/ArrayList/Cupocanalsesion" var="item">
								<tr>
									<td><x:out select="$item/canal/idcanal" /></td>
									<td><x:out select="$item/canal/nombre" /></td>
									<td><x:out select="$item/cupo" /></td>
								</tr>
							</x:forEach>										
						</tbody>
					</table>
					<span>&nbsp;</span>
				</div>
			</div>
		</div>
		
	</form>

	<div class="modal-footer">
		<button id="save_establecer_cupos_button" class="btn btn-primary save_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>
	
</div>

<script>
$('input[name="idsesion"]').val("${idSesion}");
hideSpinner("#button_programacion_cupos");

var dt_listcupos=$('#datatable-list-cupos').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	scrollY: "200px",
	scrollCollapse: true,
	paging: false,
    select: { style: 'os' },
	columnDefs: [
        { "targets": 0, "visible": false },
        { "targets": 2, "className": "text-center" }
    ],
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('datatable-list-cupos');
   	}
} );

function ajustar_cabeceras_datatable()
{
	$('#datatable-list-cupos').DataTable().columns.adjust().draw();
}

setTimeout(ajustar_cabeceras_datatable, 200);

insertSmallSpinner("#datatable-list-cupos_processing");

//********************************************************************************
$("#button_cupos_new").on("click", function(e) {
	
	showButtonSpinner("#button_cupos_new");
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/show_edit_cupo.do'/>", function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "xs");
	});
});

//********************************************************************************
$("#button_cupos_edit").on("click", function(e) {
	
	var data = sanitizeArray(dt_listcupos.rows( { selected: true } ).data(),"idcanal");
	
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.programacion.programacion.dialog.cupos.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.programacion.programacion.dialog.cupos.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	

	showButtonSpinner("#button_cupos_edit");
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/show_edit_cupo.do'/>?id="+data[0], function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "xs");
	});
});

//********************************************************************************
$("#button_cupos_remove").on("click", function(e) { 
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="administracion.programacion.programacion.dialog.cupos.list.confirm.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
		  buttons: { closer: false, sticker: false	},
		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
		   dt_listcupos
		    .rows( '.selected' )
		    .remove()
		    .draw();
     });
	
});

//********************************************************************************

$("#save_establecer_cupos_button").on("click", function(e) {
	$("#form_establecer_cupos").submit();
})

$("#form_establecer_cupos").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		saveCuposData();		
	}
});	
    
function saveCuposData() {
	
	// Se genera el xml de asignaciones de cupo
	var asignaciones_cupo= dt_listcupos.rows().data();
	var cupos_xml= "<list>";
	asignaciones_cupo.each(function (value, index) {
		cupos_xml+= "<Cupocanalsesion><canal>";
		cupos_xml+= "<idcanal>"+dt_listcupos.rows(index).data()[0][0]+"</idcanal>";
		cupos_xml+= "<nombre>"+dt_listcupos.rows(index).data()[0][1]+"</nombre>";
		cupos_xml+= "</canal>";
		cupos_xml+= "<sesion><idsesion>"+$("#idsesion").val()+"</idsesion></sesion>";
		cupos_xml+= "<cupo>"+dt_listcupos.rows(index).data()[0][2]+"</cupo></Cupocanalsesion>";
	});
	cupos_xml+= "</list>";
	$("#cupos_asignados").val(cupos_xml);
	
	var data = $("#form_establecer_cupos").serializeObject();

	showSpinner("#modal-dialog-form .modal-content");

	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/admon/programacion/save_cupos.do'/>",
		timeout : 100000,
		data : data,
		success : function(data) {
			hideSpinner("#modal-dialog-form .modal-content");
			$("#modal-dialog-form").modal('hide');
			
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
			
		},
		error : function(exception) {
			hideSpinner("#modal-dialog-form .modal-content");

			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : exception.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		}
	});
}	

</script>


<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${idioma_carta}" var="idioma_carta_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	     
			<spring:message code="venta.ventareserva.tabs.ventareserva.dialog.opciones_carta_confirmacion.title" />		
	</h4>	
</div>

<div class="modal-body">
		<form id="form-carta_confirmacion" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
			<div class="col-md-12 col-sm-12 col-xs-12">		 
				<div class="form-group">
					<label class="control-label col-md-6 col-sm-6 col-xs-12"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.opciones_carta_confirmacion.field.forma_envio" /></label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select name="forma_envio" id="forma_envio" class="form-control">
								<option value="1"><spring:message code="common.dialog.text.forma_envi.pdf" /></option>
								<option value="2"><spring:message code="common.dialog.text.forma_envi.email" /></option>
								<option value="3"><spring:message code="common.dialog.text.forma_envi.fax" /></option>																
							</select>					
					</div>				
				</div>
				<div class="form-group">
					<label class="control-label col-md-6 col-sm-6 col-xs-12"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.opciones_carta_confirmacion.field.idioma_carta" /></label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select class="form-control" name="idioma_carta" id="idioma_carta">
							<x:forEach select="$idioma_carta_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
						</select>			
					</div>				
				</div>					
			<br/><br/>		
			</div>
			
			
</form>
				


	<div class="modal-footer">
	
		<button id="carta_confirmacion_button" type="button" class="btn btn-primary close_carta_confirmacion">
			<spring:message code="common.button.accept" />
		</button>	
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>		

</div>

<script>
hideSpinner("#tab_busqueda_carta_confirmacion");
//*****************************************************
$( "#carta_confirmacion_button" ).on("click", function(e) {		
	//Falla su parte
})
//**************************************************************************

</script>

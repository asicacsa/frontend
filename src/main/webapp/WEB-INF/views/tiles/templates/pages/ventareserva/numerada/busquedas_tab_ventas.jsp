<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${tabventas_selector_temporada}" var="tabventas_selector_temporada_xml" />
<x:parse xml="${tabventas_selector_canales}" var="tabventas_selector_canales_xml" />

<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2>
			<spring:message code="common.text.filter_list" />
		</h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">

		<form id="form_filter_list_ventas" class="form-horizontal form-label-left">

			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="form-group date-picker">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.numerada_busquedas.tabs.ventas.field.fechas" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<a type="button" class="btn btn-default btn-clear-date" id="button_fechas_clear">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada_busquedas.tabs.ventas.field.fechas.button.clear" />"> <span class="fa fa-trash"></span>
							</span>
						</a>
						<div class="input-prepend input-group">
							<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span> 
							<input type="text" name="fechas" id="fechas" class="form-control" value="" readonly /> 
							<input type="hidden" id="fechaini" name="fechaini" value="" /> 
							<input type="hidden" id="fechafin" name="fechafin" value="" />
						</div>
					</div>
				</div>

				<div class="form-group">
						<div class="col-md-3 col-sm-3 col-xs-3"></div>
						<label class="col-md-9 col-sm-9 col-xs-9"><input type="checkbox" name="anulada" class="flat" /> <span class="control-label"><spring:message code="venta.numerada_busquedas.tabs.ventas.field.anulada" /></span>
						</label>
				</div>
				<br/>
				<div class="form-group">
					<label class="control-label"> <input type="radio" class="flat" checked name="idtipoventa" value=""> <spring:message code="venta.numerada_busquedas.tabs.ventas.field.radio.todas" />
					</label> <label class="control-label"> <input type="radio" class="flat" name="idtipoventa" value="3">
					<spring:message code="venta.numerada_busquedas.tabs.ventas.field.radio.localidades" />
					</label> <label class="control-label"> <input type="radio" class="flat" name="idtipoventa" value="1">
					<spring:message code="venta.numerada_busquedas.tabs.ventas.field.radio.abonos" />
					</label>
				</div>

				<div class="form-group">
					<label class="control-label"> <input type="radio" class="flat" checked name="facturada" value=""> <spring:message code="venta.numerada_busquedas.tabs.ventas.field.radio.todas" />
					</label> <label class="control-label"> <input type="radio" class="flat" name="facturada" value="1">
					<spring:message code="venta.numerada_busquedas.tabs.ventas.field.radio.facturadas" />
					</label> <label class="control-label"> <input type="radio" class="flat" name="facturada" value="0">
					<spring:message code="venta.numerada_busquedas.tabs.ventas.field.radio.no_facturadas" />
					</label>
				</div>

				<br/>
				<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.numerada_busquedas.tabs.ventas.field.ref_venta_desde" /></label>
						<div class="col-md-3 col-sm-3 col-xs-3">
	                          <input type="text" name="refventadesde" id="refventadesde" class="form-control" value=""/>
						</div>
						<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="venta.numerada_busquedas.tabs.ventas.field.ref_venta_hasta" /></label>
						<div class="col-md-3 col-sm-3 col-xs-3">
	                          <input type="text" name="refventahasta" id="refventahasta" class="form-control" value=""/>
						</div>
				</div>


				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.numerada_busquedas.tabs.ventas.field.ref_venta_entrada" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="refentrada" id="refentrada" type="text" class="form-control">
					</div>
				</div>

			</div>


			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.ventas.field.canal" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						
						<select class="form-control" name="idcanal" id="idcanal">
							<option value=""></option>
							<x:forEach select="$tabventas_selector_canales_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>							
						</select>
					</div>
				</div>				

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.ventas.field.temporada" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						
						<select class="form-control" name="idtemporada" id="idtemporada_ventas">
							<option value=""></option>
							<x:forEach select="$tabventas_selector_temporada_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>							
						</select>
					</div>
				</div>				

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.ventas.field.espectaculo" /></label>
					<div id="espectaculo_ventas_div" class="col-md-8 col-sm-8 col-xs-8">
						
						<select class="form-control" name="idespectaculo" id="idespectaculo_ventas">
							<option value=""></option>
						</select>
					</div>
				</div>				

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.ventas.field.tipo_abono" /></label>
					<div id="tipoabono_div"  class="col-md-8 col-sm-8 col-xs-8">
						
						<select class="form-control" name="idtipoabono" id="idtipoabono">
							<option value=""></option>
						</select>
					</div>
				</div>				

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.ventas.field.cliente" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<input name="cliente" id="cliente" type="text" class="form-control" readonly>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.ventas.field.nombre_abonado" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<input name="nombreabonado" id="nombreabonado" type="text" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.ventas.field.numero_abonado_abono" /></label>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<input name="numabonado" id="numabonado" type="text" class="form-control">
					</div>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<input name="numabono" id="numabono" type="text" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="venta.numerada_busquedas.tabs.ventas.field.dni_cif" /></label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<input name="dni" id="dni" type="text" class="form-control">
					</div>
				</div>


			</div>

			<div class="clearfix"></div>
			<div class="ln_solid"></div>
			<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
				<button id="button_filter_list_ventas" type="button" class="btn btn-success pull-right">
					<spring:message code="common.button.filter" />
				</button>
				<button id="button_clean_venta_numerada" type="button" class="btn btn-success pull-right">
					<spring:message code="venta.ventareserva.tabs.canje-bono.button.clean" />
				</button>
			</div>


			<input type="hidden" name="idcliente_ventas" value=""/>
			<input type="hidden" name="start" value=""/>
			<input type="hidden" name="length" value=""/>

		</form>


	</div>
</div>

<!--  -->


<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_ventas_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada_busquedas.tabs.ventas.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_ventas_print_ticket">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada_busquedas.tabs.ventas.list.button.imprimir_entrada" />"> <span class="fa fa-ticket"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_ventas_print_receipt">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada_busquedas.tabs.ventas.list.button.imprimir_recibo" />"> <span class="fa fa-print"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_ventas_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada_busquedas.tabs.ventas.list.button.anular" />"> <span class="fa fa-ban"></span>
			</span>
		</a>
	</div>

	<table id="datatable-list-ventas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.ventas.list.header.ref_venta" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.ventas.list.header.fecha" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.ventas.list.header.estado" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.ventas.list.header.facturada" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.ventas.list.header.venta" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.ventas.list.header.dni" /></th>
				<th><spring:message code="venta.numerada_busquedas.tabs.ventas.list.header.importe" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>
var id_anulacion;
$("#cliente").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>", "#cliente", "#idcliente_ventas");

$("#button_filter_list_ventas").on("click", function(e) { 
	
	if (!isFormEmpty("#form_filter_list_ventas")) {
		dt_listventas.ajax.reload(); 
	}
	else {
		new PNotify({
			  title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
		      text: '<spring:message code="common.dialog.text.empty_filter" />',
			  type: "error",
			  buttons: { sticker: false }				  
		   });
	}
	
});

$('input[name="fechas"]').daterangepicker({
		autoUpdateInput: false,
		linkedCalendars: false,
		autoApply: true,
      	locale: $daterangepicker_sp
		}, function(start,end) {
			 $('#form_filter_list_ventas input[name="fechas"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
      	  	 $('#form_filter_list_ventas input[name="fechaini"]').val(start.format('DD/MM/YYYY'));
      	  	 $('#form_filter_list_ventas input[name="fechafin"]').val(end.format('DD/MM/YYYY'));
    	 });
    	 
$("#button_fechas_clear").on("click", function(e) {
    
    $('input[name="fechas"]').val('');
    $('input[name="fechaini"]').val('');
    $('input[name="fechafin"]').val('');
    
    
});


$("#idtemporada_ventas").on("change", function(e) {
	showFieldSpinner("#espectaculo_ventas_div");	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/list_espectaculos.do'/>",
		timeout : 100000,
		data: {
				idtemporada: $("#idtemporada_ventas").val()
			  }, 
		success : function(data) {
			hideSpinner("#espectaculo_ventas_div");
			var $select=$("#idespectaculo_ventas");
			$select.html('');
			$select.prepend("<option value='' selected='selected'></option>");
		    $.each(data.ArrayList, function(key, val){
		      $select.append('<option value="' + val.value + '">' + val.label + '</option>');
		    });
		},
		error : function(exception) {
			hideSpinner("#espectaculo_ventas_div");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});
		
	showFieldSpinner("#tipoabono_div");	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/list_tipos_abono_ventas.do'/>",
		timeout : 100000,
		data: {
				idtemporada: $("#idtemporada_ventas").val()
			  }, 
		success : function(data) {
			hideSpinner("#tipoabono_div");
			var $select=$("#idtipoabono");
			$select.html('');
			$select.prepend("<option value='' selected='selected'></option>");
		    $.each(data.ArrayList, function(key, val){
		      $select.append('<option value="' + val.value + '">' + val.label + '</option>');
		    });
		},
		error : function(exception) {
			hideSpinner("#tipoabono_div");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});	
	
});

var dt_listventas=$('#datatable-list-ventas').DataTable( {
	serverSide: true,
	searching: false,
	ordering: false,
	deferLoading: 0,
	pagingType: "simple",
	info: false,
    ajax: {
        url: "<c:url value='/ajax/venta/numerada/list_ventas.do'/>",
        rowId: 'idventa',
        type: 'POST',
        dataSrc: function (json) { 
        	if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Venta)); return(""); 
        	},
        data: function (params) {
        	$('#form_filter_list_ventas input[name="start"]').val(params.start);
        	$('#form_filter_list_ventas input[name="length"]').val(params.length);
        	return ($("#form_filter_list_ventas").serializeObject());
       	}
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listventas.data().count()>0)  dt_listventas.columns.adjust().draw(); });
	},
    columns: [
        { data: "lineadetalles"}, 
        { data: "idventa", type: "numeric"}, 
        { data: "fechayhoraventa", type: "date", defaultContent: ""}, 
        { data: "estadooperacion.nombre", type: "spanish-string", defaultContent: ""}, 
        { data: "facturada", className: "text_icon cell_centered",
            render: function ( data, type, row, meta ) {     
          	  if (data==0) return '<i class="fa fa-times-circle" aria-hidden="true"></i>'; else return '<i class="fa fa-check-circle" aria-hidden="true"></i>';	
            }
          }, 
        { data: "nombre", type: "spanish-string", defaultContent: ""}, 
        { data: "nif", type: "spanish-string", defaultContent: ""}, 
        { data: "importetotalventa", type: "numeric"},         
    ],    
	columnDefs: [
	             { "targets": 0, "visible": false },
	         ],
    select: { style: 'single'},
	language: dataTableLanguage,
	processing: true
} );


insertSmallSpinner("#datatable-list-ventas_processing");


//********************************************************************************
$("#tab_ventas_print_ticket").on("click", function(e) { 

	var data = dt_listventas.rows( { selected: true } ).data();

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.numerada_busquedas.tabs.ventas.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	var entradas=data[0].lineadetalles.Lineadetalle;
	var xml="<list>";
	for (var i = 0; i < entradas.length; i++) {
		xml=xml+"<identrada>"+entradas[i].entradas.Entrada.identrada+"</identrada>";
	}
	xml=xml+"</list>";
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/mostrar_dialogo_imprimir_entradas.do'/>",
		timeout : 100000,
		data: {
				xml: xml
			  }, 
		success : function(data) {
			if (data.Integer==1) {
				$("#modal-dialog-ventas-imprimir-entradas").modal('show');
				setModalDialogSize("#modal-dialog-ventas-imprimir-entradas", "md");
			}
			else {
				busquedasNumeradaVentasImprimirEntradas();			
			}
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,				  
				  buttons: { sticker: false }				  
			   });		
		}
	});
					
}); 


//********************************************************************************
function busquedasNumeradaVentasImprimirEntradas() {

	showButtonSpinner("#tab_ventas_print_ticket");
	
	var data = dt_listventas.rows( { selected: true } ).data();
		
	var entradas=data[0].lineadetalles.Lineadetalle;
	var xml="<list>";
	for (var i = 0; i < entradas.length; i++) {
		xml=xml+"<identrada>"+entradas[i].entradas.Entrada.identrada+"</identrada>";
	}
	xml=xml+"</list>";

	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/imprimir_entradas.do'/>",
		timeout : 100000,
		data: {
				xml: xml
			  }, 
		success : function(data) {
			hideSpinner("#tab_ventas_print_ticket");

			new PNotify({
			      title: '<spring:message code="common.dialog.text.impresion_realizada" />',
			      text: '<spring:message code="venta.numerada_busquedas.tabs.ventas.list.alert.impresion_realizada" />',
				  type: "info",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
		},
		error : function(exception) {
			hideSpinner("#tab_ventas_print_ticket");
			
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,				  
				  buttons: { sticker: false }				  
			   });		
		}
	});
	
}			




//********************************************************************************
function busquedasNumeradaVentasRegenerarImprimirEntradas() {
	
	showButtonSpinner("#tab_ventas_print_ticket");
	
	var data = dt_listventas.rows( { selected: true } ).data();
	
	var entradas=data[0].lineadetalles.Lineadetalle;
	var xml="<list>";
	for (var i = 0; i < entradas.length; i++) {
		xml=xml+"<identrada>"+entradas[i].entradas.Entrada.identrada+"</identrada>";
	}
	xml=xml+"</list>";

	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/regenerar_imprimir_entradas.do'/>",
		timeout : 100000,
		data: {
				xml: xml
			  }, 
		success : function(data) {
			hideSpinner("#tab_ventas_print_ticket");
			

			new PNotify({
					title : '<spring:message code="common.dialog.text.impresion_realizada" />',
					text : '<spring:message code="venta.numerada_busquedas.tabs.ventas.list.alert.impresion_realizada" />',
					type : "info",
					delay : 5000,
					buttons : {
						sticker : false
					}
				});
			},
			error : function(exception) {
				hideSpinner("#tab_ventas_print_ticket");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						sticker : false
					}
				});
			}
		});

	}
	
//********************************************************************************
$("#tab_ventas_print_receipt").on("click", function(e) { 

	var data = sanitizeArray(dt_listventas.rows( { selected: true } ).data(),"idventa");
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.numerada_busquedas.tabs.ventas.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
			
	showButtonSpinner("#tab_ventas_print_receipt");
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/imprimir_recibos.do'/>",
		timeout : 100000,
		data: {
				id: data[0]
			  }, 
		success : function(data) {
			hideSpinner("#tab_ventas_print_receipt");

			new PNotify({
			      title: '<spring:message code="common.dialog.text.impresion_realizada" />',
			      text: '<spring:message code="venta.numerada_busquedas.tabs.ventas.list.alert.impresion_realizada" />',
				  type: "info",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
		},
		error : function(exception) {
			hideSpinner("#tab_ventas_print_receipt");
			
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,				  
				  buttons: { sticker: false }				  
			   });		
		}
	});
	
});			
	
//********************************************************************************
$("#tab_ventas_remove").on("click", function(e) { 

	var data = sanitizeArray(dt_listventas.rows( { selected: true } ).data(),"idventa");
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.numerada_busquedas.tabs.ventas.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
			
	showButtonSpinner("#tab_ventas_remove");
	
	id_anulacion = data[0];
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/numerada/anular_venta.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});	
	
});			
	

//********************************************************************************
$("#tab_ventas_edit").on("click", function(e) { 

	var data = sanitizeArray(dt_listventas.rows( { selected: true } ).data(),"idventa");
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.numerada_busquedas.tabs.ventas.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
			
	showButtonSpinner("#tab_ventas_edit");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/numerada/editar_venta.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});	
	
});		

/***********************************************BOT�N LIMPIAR   *********************************/
$("#button_clean_venta_numerada").on("click", function(e) {
	$("#fechas").val("");
	$("#refventadesde").val("");
	$("#refventahasta").val("");
	$("#refentrada").val("");
	$("#cliente").val("");
	$("#nombreabonado").val("");
	$("#numabonado").val("");
	$("#numabono").val("");
	$("#dni").val("");
	
	
	 $("#idcanal option:first").prop("selected", "selected");
	 $("#idtemporada_ventas option:first").prop("selected", "selected");
	 $("#idespectaculo_ventas option:first").prop("selected", "selected");
	 $("#idtipoabono option:first").prop("selected", "selected");
	 $("#idtipoabono option:first").prop("selected", "selected");
	
})


</script>

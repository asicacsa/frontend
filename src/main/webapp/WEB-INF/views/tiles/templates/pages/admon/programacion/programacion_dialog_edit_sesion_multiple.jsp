<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="administracion.programacion.programacion.dialog.editar_sesion_multiple.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_sesion_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

		<input id="idsesiones" name="idsesiones" type="hidden" value="${editarsesionmultiples_datos_sesiones}" />
		<input id="idcontenido_dialogo" name="idcontenido_dialogo" type="hidden" value="" />

		<div id="columna-izquierda" class="col-md-12 col-sm-12 col-xs-12">
		
			<div id="grupo-principal">
			
				<div class="form-group button-dialog">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.dialog.editar_sesion.field.contenido" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
			  			<a type="button" class="btn btn-default btn-dialog" id="button-select-contenido" >
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.dialog.editar_sesion.list.button.select_contenido" />"> <span class="fa fa-pencil"></span></span>
				  		</a>		
                        <div class="input-prepend">
							<input name="contenido" id="contenido" type="text" class="form-control" readonly value="">
						</div>
					</div>
				</div>
							
			</div>

			<div id="grupo-programacion">
		
				<div class="x_title">
					<h5><spring:message code="administracion.programacion.dialog.editar_sesion.title.programacion" /></h5>
					<div class="clearfix"></div>
				</div>
			
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.dialog.editar_sesion.field.horainicio" /></label>
					<div class="col-md-2 col-sm-2 col-xs-2">
	                	<input type="text" name="horainicio" id="horainicio" class="form-control" data-inputmask="'mask': '99:99'" value=""/>
					</div>
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.dialog.editar_sesion.field.horafin" /></label>
					<div class="col-md-2 col-sm-2 col-xs-2">
	                	<input type="text" name="horafin" id="horafin" class="form-control" data-inputmask="'mask': '99:99'" value=""/>
					</div>
				  	<a type="button" class="btn btn-default btn-search-hour" id="button-select-franja">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.dialog.editar_sesion.list.button.clear" />"> <span class="fa fa-binoculars"></span>
						</span>
				  	</a>			
				</div>
				
				<div id="fecha-programacion-edicion">
				
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.programacion.dialog.editar_sesion.field.fecha" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
	                       <div class="input-prepend input-group">
	                         <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	                         <input type="text" name="fecha_sesion" id="fecha_sesion" class="form-control" value="" readonly/>
	                       </div>
						</div>
					</div>
				
				</div>
			
					
			</div>	

		</div>

		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_sesion_button" type="submit" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
		
	</form>

</div>


<script>
//Al calcular el aforo-total-calculado se suma 0 al principio del parseInt para evitar que el resultado de la operación de NaN porque uno de ellos es null
function calcular_aforo_total() {
	var aforo_total_calculado= parseInt(0+$('#aforo-total').val());
	if ($("#oaforo:checked").length) aforo_total_calculado+= parseInt(0+$('#overbookingaforo').val());
		return(aforo_total_calculado);
}

$(document).ready(function() {
	hideSpinner("#button_programacion_new");
	hideSpinner("#button_programacion_edit");
		
	$(".select-select2").select2({
		language : "es",
	});

		 
	$(":input").inputmask();
	
		
    $('input[name="fecha_sesion"]').daterangepicker({
        singleDatePicker: true,
        autoUpdateInput: false,
        showDropdowns: true,
      	locale: $daterangepicker_sp},
      	function(start) {
     	  	 $('input[name="fecha_sesion"]').val(start.format('DD/MM/YYYY'));     	  	 
   	 });      	


    /* Selector de contenido en un diálogo adicional */

    $("#button-select-contenido").on("click", function(e) {
    	
    	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/select_contenido.do'/>?id="+$("#idrecinto :selected").val(), function() {
        	   
            $("#modal-dialog-form-2").modal('show');
            setModalDialogSize("#modal-dialog-form-2", "xs");
      });
    	
    });
    
    /* Selector de franjas horarias */

    $("#button-select-franja").on("click", function(e) {
    	
    	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/select_franja.do'/>", function() {
        	   
            $("#modal-dialog-form-2").modal('show');
            setModalDialogSize("#modal-dialog-form-2", "xs");
      });
    	
    });
    
        
    /* Procesado de datos */
    
	$("#form_sesion_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {		
				saveSesionData();
		}
	});
    
	function saveSesionData() {
		var data = $("#form_sesion_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/programacion/save_sesion_multiple.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				dt_listprogramacion.ajax.reload(null,false);			
				$("#modal-dialog-form").modal('hide');
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
	}
    
});

</script>

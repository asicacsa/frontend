<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<x:parse xml="${tabgeneracionfacturas_selector_series}" var="tabgeneracionfacturas_selector_series_xml" />

<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2><spring:message code="common.text.filter_list" /></h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">

		<form id="form_filter_list_generacionfacturas" class="form-horizontal form-label-left">
		<input id="idcliente" name="idcliente" type="hidden" value="" />
		<input id="idclienteasociado" name="idclienteasociado" type="hidden" value="" />
		<input id="clienteasociado" name="clienteasociado" type="hidden" value="" />
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="form-group button-dialog">
           			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.geberacionfacturas.field.cliente" /></label>
           			<div class="col-md-9 col-sm-9 col-xs-9">
               			<input name="cliente" id="cliente" type="text" class="form-control"  value="">
              		</div>
            	</div>        		           				
				<div class="form-group">
					<label class="control-label col-md-9 col-sm-9 col-xs-9"><spring:message code="facturacion.facturas.tabs.geberacionfacturas.field.principales" /></label>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<input id="soloprin-switch" name="soloprin" type="checkbox" class="js-switch-activacion"  data-switchery="true" style="display: none;">
					</div>
				</div>        	
            		
            		
            	<div class="form-group date-picker" id="fechaventageneracion">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.generacionfacturas.field.fechaventa" />:</label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<a type="button" class="btn btn-default btn-clear-date" id="button_fechaventageneracion_clear">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.generacionfacturas.list.button.clear" />"> <span class="fa fa-trash"></span>
							</span>
						</a>			
                     	<div class="input-prepend input-group">
                        	<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                         	<input type="text" name="ventageneracion" id="ventageneracion" class="form-control" value="" readonly/>
                          	<input type="hidden" required="required" name="fechainicioventageneracion" value=""/>
                          	<input type="hidden" required="required" name="fechafinventageneracion" value=""/>
                        </div>
					</div>
				</div>
				<div class="form-group date-picker" id="fechacangegeneracion">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.generacionfacturas.field.fechacange" />:</label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<a type="button" class="btn btn-default btn-clear-date" id="button_fechacangegeneracion_clear">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.generacionfacturas.list.button.clear" />"> <span class="fa fa-trash"></span>
							</span>
						</a>			
                     	<div class="input-prepend input-group">
                        	<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                         	<input type="text" name="cangegeneracion" id="cangegeneracion" class="form-control" value="" readonly/>
                          	<input type="hidden" required="required" name="fechainiciocangegeneracion" value=""/>
                          	<input type="hidden" required="required" name="fechafincangegeneracion" value=""/>
                        </div>
					</div>
				</div>
				
				<div class="form-group date-picker" id="fechareservageneracion">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.generacionfacturas.field.fechareserva" />:</label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<a type="button" class="btn btn-default btn-clear-date" id="button_fechareservageneracion_clear">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.generacionfacturas.list.button.clear" />"> <span class="fa fa-trash"></span>
							</span>
						</a>			
                     	<div class="input-prepend input-group">
                        	<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                         	<input type="text" name="reservageneracion" id="reservageneracion" class="form-control" value="" readonly/>
                          	<input type="hidden" required="required" name="fechainicioreservageneracion" value=""/>
                          	<input type="hidden" required="required" name="fechafinreservageneracion" value=""/>
                        </div>
					</div>
				</div>
				
				<div class="form-group date-picker" id="fechaespectaculos">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.generacionfacturas.field.fechaespectaculos" />:</label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<a type="button" class="btn btn-default btn-clear-date" id="button_fechaespectaculos_clear">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.generacionfacturas.list.button.clear" />"> <span class="fa fa-trash"></span>
							</span>
						</a>			
                     	<div class="input-prepend input-group">
                        	<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                         	<input type="text" name="espectaculos" id="espectaculos" class="form-control" value="" readonly/>
                          	<input type="hidden" required="required" name="fechainicioespectaculos" value=""/>
                          	<input type="hidden" required="required" name="fechafinespectaculos" value=""/>
                        </div>
					</div>
				</div>
			</div>
					

			<div class="col-md-6 col-sm-6 col-xs-12">
				
				 
				 <div class="form-group" id="refventageneracion">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.generacionfacturas.field.refventa" />:</label>					
					<div class="col-md-4 col-sm-4 col-xs-4">
	                	<input type="text" name="refventageneracioninicial" id="refventageneracioninicial" class="form-control" value=""/>
					</div>
					<label class="control-label col-md-1 col-sm-1 col-xs-1"><spring:message code="facturacion.facturas.tabs.generacionfacturas.field.a" /></label>
					<div class="col-md-4 col-sm-4 col-xs-4">
	                	<input type="text" name="refventageneracionfinal" id="refventageneracionfinal" class="form-control" value=""/>
					</div>
				</div>
				
				<div class="form-group" id="refreservageneracion">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.generacionfacturas.field.refreserva" />:</label>					
					<div class="col-md-4 col-sm-4 col-xs-4">
	                	<input type="text" name="refreservageneracioninicial" id="refreservageneracioninicial" class="form-control" value=""/>
					</div>
					<label class="control-label col-md-1 col-sm-1 col-xs-1"><spring:message code="facturacion.facturas.tabs.generacionfacturas.field.a" /></label>
					<div class="col-md-4 col-sm-4 col-xs-4">
	                	<input type="text" name="refreservageneracionfinal" id="refreservageneracionfinal" class="form-control" value=""/>
					</div>				
				</div>
				
				<div class="form-group" id="localizadorag">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.generacionfacturas.field.localizadorag" />:</label>					
					<div class="col-md-9 col-sm-9 col-xs-9">
	                	<input type="text" name="localizadorgeneracion" id="localizadorgeneracion" class="form-control" value=""/>
					</div>					
				</div>
				
			
				
				<fieldset id="tipoventas">
					<div class="form-group">
						<div class="checkbox col-md-12 col-sm-12 col-xs-12">
							<input type="radio" name="tipo-ventas" id="ventaproductos" checked value="1" data-parsley-multiple="ventas" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.tabs.generacionfacturas.field.ventaproductos" /></strong>
						</div>
						<div class="checkbox col-md-12 col-sm-12 col-xs-12">
							<input type="radio" name="tipo-ventas" id="ventabonos"  value="2" data-parsley-multiple="ventas" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.tabs.generacionfacturas.field.ventabonos" /></strong>
						</div>
						<div class="checkbox col-md-12 col-sm-12 col-xs-12">
							<input type="radio" name="tipo-ventas" id="bonos" value="3" data-parsley-multiple="ventas" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.tabs.generacionfacturas.field.bonos" /></strong>
						</div>							
					</div>				
				</fieldset>			
											
				 
			</div>

			<div class="clearfix"></div>
			<div class="ln_solid"></div>
			<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
				<button id="button_clean_gen_facturas" type="button" class="btn btn-success pull-right">
						<spring:message code="venta.ventareserva.tabs.canje-bono.button.clean" />
				</button>
				<button id="button_filter_list_generacionfacturas" type="button" class="btn btn-success pull-right">
					<spring:message code="common.button.filter" />
				</button>
			</div>
			<input type="hidden" name="start" value=""/>
			<input type="hidden" name="length" value=""/>
		</form>


	</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="form-group generacionfacturas">
		<label class="control-label col-md-2 col-sm-2 col-xs-12"><spring:message code="facturacion.facturas.tabs.generacionfacturas.field.seriaafacturar" /></label>					
		<div class="col-md-4 col-sm-4 col-xs-12">						
			<select name="idseriefacturar" id="buscador_generacionfacturas_selector_serie" class="form-control"">
				<option value=""></option>
				<x:forEach select="$tabgeneracionfacturas_selector_series_xml/ArrayList/Tipofactura" var="item">
					<option value="<x:out select="$item/idtipofactura" />"><x:out select="$item/nombre" /></option>
				</x:forEach>
			</select>
		</div>
	</div>
	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_generacionfacturas_asociar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.generacionfacturas.list.button.asociar" />"> <span class="fa fa-search"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_generacionfacturas_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.generacionfacturas.list.button.eliminar" />"> <span class="fa fa-remove"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_generacionfacturas_generar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.generacionfacturas.list.button.generar" />"> <span class="fa fa-euro"></span>
			</span>
		</a>
	</div>

	<table id="datatable-list-generacionfacturas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="facturacion.facturas.tabs.generacionfacturas.list.header.refventa" /></th>
				<th><spring:message code="facturacion.facturas.tabs.generacionfacturas.list.header.codcliente" /></th>
				<th><spring:message code="facturacion.facturas.tabs.generacionfacturas.list.header.cliente" /></th>
				<th><spring:message code="facturacion.facturas.tabs.generacionfacturas.list.header.fechaventa" /></th>
				<th><spring:message code="facturacion.facturas.tabs.generacionfacturas.list.header.importe" /></th>
				
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>

$(document).ready(function() {		
	
	var dt_listgeneracionfacturas=$('#datatable-list-generacionfacturas').DataTable( {
		serverSide: true,
		searching: false,
		ordering: false,
		pagingType: "simple",
		info: false,
		deferLoading: 0,		
		
		 ajax: {
		        url: "<c:url value='/ajax/facturacion/facturas/generacionfacturas/list_generacionfacturas.do'/>",
		        rowId: 'idventa',
		        type: 'POST',
		        dataSrc: function (json) {
		        
		        	if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Venta)); return(""); },
		        data: function (params) {
		        	$('#form_filter_list_generacionfacturas input[name="start"]').val(params.start);
		        	$('#form_filter_list_generacionfacturas input[name="length"]').val(params.length);
		        	return($("#form_filter_list_generacionfacturas").serializeObject());
		    	
		        },
		        
		        error: function (xhr, error, thrown) {
		            if (xhr.responseText=="403") {
		                  $("#generacionfacturas-tab").hide();
		            }  
		            else
		            	{
		            	$("#datatable-list-generacionfacturas_processing").hide();
		            	new PNotify({
	     					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
	     					text : xhr.responseText,
	     					type : "error",
	     					delay : 5000,
	     					buttons : {
	     						closer : true,
	     						sticker : false
	     					}					
	     				});
		            	}
		     },
		    },
		    initComplete: function( settings, json ) {
		        $('a#menu_toggle').on("click", function () {if (dt_listgeneracionfacturas.data().count()>0) dt_listgeneracionfacturas.columns.adjust().draw(); });
			}  
		    ,
		    columns: [
				
				{ data: "idventa", type: "spanish-string", defaultContent: ""}, 
				{ data: "cliente.idcliente", type: "spanish-string", defaultContent: ""}, 
				{ data: "cliente.nombrecompleto", type: "spanish-string", defaultContent: "" },		 
				{ data: "fechayhoraventa", type: "spanish-string" ,  defaultContent:"",
					 render: function ( data, type, row, meta ) {			 
			    	  	  return data.substr(0, 10);}	 
				 } ,
				{ data: "importetotalventa", type: "spanish-string", defaultContent: ""},
		    ],    
		    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-generacionfacturas') },
		    select: { style: 'os'},
			language: dataTableLanguage,
			processing: true,
} );
	
//********************************************************************************	
	$("#cliente").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>", "#cliente", "#idcliente");
//*******************************************************************************
	insertSmallSpinner("#datatable-list-generacionfacturas_processing");
//********************************************************************************
$("#soloprin-switch").each(function(index) {
	var s = new Switchery(this, {
		size : 'small'
	});
});
//********************************************************************************
$("#button_filter_list_generacionfacturas").on("click", function(e) {	
	dt_listgeneracionfacturas.ajax.reload();
	})
//*******************************************************************************

$today= moment().format("DD/MM/YYYY");
var dia_inicio = $today;
var dia_fin = $today;

//Fecha venta:
$('input[name="fechainicioventageneracion"]').val(dia_inicio);
$('input[name="fechafinventageneracion"]').val(dia_fin);
$('input[name="ventageneracion"]').val( dia_inicio + ' - ' + dia_fin);


$('input[name="ventageneracion"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
  	  	 $('input[name="ventageneracion"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('input[name="fechainicioventageneracion"]').val(start.format('DD/MM/YYYY'));
  	  	 $('input[name="fechafinventageneracion"]').val(end.format('DD/MM/YYYY'));
	 });

$("#button_fechaventageneracion_clear").on("click", function(e) {
    $('input[name="ventageneracion"]').val('');
    $('input[name="fechainicioventageneracion"]').val('');
    $('input[name="fechafinventageneracion"]').val('');
});

//**************************************************************
//Fecha cange:
$('input[name="fechainiciocangegeneracion"]').val(dia_inicio);
$('input[name="fechafincangegeneracion"]').val(dia_fin);
$('input[name="cangegeneracion"]').val( dia_inicio + ' - ' + dia_fin);


$('input[name="cangegeneracion"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
  	  	 $('input[name="cangegeneracion"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('input[name="fechainiciocangegeneracion"]').val(start.format('DD/MM/YYYY'));
  	  	 $('input[name="fechafincangegeneracion"]').val(end.format('DD/MM/YYYY'));
	 });

$("#button_fechacangegeneracion_clear").on("click", function(e) {	
    $('input[name="cangegeneracion"]').val('');
    $('input[name="fechainiciocangegeneracion"]').val('');
    $('input[name="fechafincangegeneracion"]').val('');
});

//**************************************************************
//Fecha reserva:
$('input[name="fechainicioreservageneracion"]').val(dia_inicio);
$('input[name="fechafinreservageneracion"]').val(dia_fin);
$('input[name="reservageneracion"]').val( dia_inicio + ' - ' + dia_fin);


$('input[name="reservageneracion"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
	locale: $daterangepicker_sp
	}, function(start,end) {
	  	 $('input[name="reservageneracion"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
	  	 $('input[name="fechainicioreservageneracion"]').val(start.format('DD/MM/YYYY'));
	  	 $('input[name="fechafinreservageneracion"]').val(end.format('DD/MM/YYYY'));
	 });

$("#button_fechareservageneracion_clear").on("click", function(e) {
  $('input[name="reservageneracion"]').val('');
  $('input[name="fechainicioreservageneracion"]').val('');
  $('input[name="fechafinreservageneracion"]').val('');
});
//**************************************************************
//Fecha espectaculos:
$('input[name="fechainicioespectaculos"]').val(dia_inicio);
$('input[name="fechafinespectaculos"]').val(dia_fin);
$('input[name="espectaculos"]').val( dia_inicio + ' - ' + dia_fin);


$('input[name="espectaculos"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
	locale: $daterangepicker_sp
	}, function(start,end) {
	  	 $('input[name="espectaculos"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
	  	 $('input[name="fechainicioespectaculos"]').val(start.format('DD/MM/YYYY'));
	  	 $('input[name="fechafinespectaculos"]').val(end.format('DD/MM/YYYY'));
	 });

$("#button_fechaespectaculos_clear").on("click", function(e) {
	$('input[name="espectaculos"]').val('');
	$('input[name="fechainicioespectaculos"]').val('');
	$('input[name="fechafinespectaculo"]').val('');
});

//*****************************************************************************************
	$("#fechacangegeneracion").hide();
//*****************************************************************************************
$("#ventaproductos").on("ifChanged", function(e) {	
	if($("input[name='tipo-ventas']:checked").val() == 1 )
	{

		$("#fechaventageneracion").show();
		$("#fechareservageneracion").show();
		$("#fechaespectaculos").show();		
		$("#fechacangegeneracion").hide();		
		$("#refreservageneracion").show();
		$("#localizadorag").show();
	}else
	{		
		$("#fechacangegeneracion").show();
		$("#fechaventageneracion").hide();
		$("#fechareservageneracion").hide();
		$("#fechaespectaculos").show();		
		$("#refreservageneracion").hide();
		$("#localizadorag").hide();
	}
});


$("#ventabonos").on("ifChanged", function(e) {	
	
	if($("input[name='tipo-ventas']:checked").val() == 2 )
	{
		$("#fechaventageneracion").show();
		$("#fechareservageneracion").show();
		$("#fechaespectaculos").show();		
		$("#fechacangegeneracion").hide();		
		$("#refreservageneracion").show();
		$("#localizadorag").show();
	}else
	{		
		$("#fechacangegeneracion").show();
		$("#fechaventageneracion").hide();
		$("#fechareservageneracion").hide();
		$("#fechaespectaculos").show();		
		$("#refreservageneracion").hide();
		$("#localizadorag").hide();
	}
	
});
vaciarFechas();	
//*****************************************************************************************
function vaciarFechas()
{	
	 $('input[name="ventageneracion"]').val('');
	 $('input[name="fechainicioventageneracion"]').val('');
	 $('input[name="fechafinventageneracion"]').val('');
	 $('input[name="cangegeneracion"]').val('');
	 $('input[name="fechainiciocangegeneracion"]').val('');
	 $('input[name="fechafincangegeneracion"]').val('');
	 $('input[name="reservageneracion"]').val('');
	 $('input[name="fechainicioreservageneracion"]').val('');
	 $('input[name="fechafinreservageneracion"]').val('');	
	 $('input[name="espectaculos"]').val('');
	 $('input[name="fechainicioespectaculos"]').val('');
	 $('input[name="fechafinoespectaculos"]').val(''); 
	}
//***********************************************BOT�N ASOCIAR CON CLIENTE**********************
 	$("#tab_generacionfacturas_asociar").on("click", function(e) { 
 		showButtonSpinner("#tab_generacionfacturas_asociar");
	var data = sanitizeArray(dt_listgeneracionfacturas.rows( { selected: true } ).data(),"idventa");
		
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="facturacion.facturas.tabs.generacionfacturas.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		hideSpinner("#tab_generacionfacturas_asociar");
		return;
	}	
	
	hideSpinner("#tab_generacionfacturas_asociar");
	$("#modal-dialog-form-buscar-cliente .modal-content").load("<c:url value='/ajax/show_buscadorcliente.do'/>", function() {                                                                  
        $("#modal-dialog-form-buscar-cliente").modal('show');
	       	$("#modal-dialog-form-buscar-cliente input#campoBuscadorClienteDevueltoNombre").val("#clienteasociado");
	       	$("#modal-dialog-form-buscar-cliente input#campoBuscadorClienteDevueltoId").val("#idclienteasociado");	       	
        setModalDialogSize("#modal-dialog-form-buscar-cliente", "lg");
 });
	
	hideSpinner("#tab_generacionfacturas_asociar");
	
})

//****************************************************
$( "#clienteasociado" ).change(function() {
		
		var data = sanitizeArray(dt_listgeneracionfacturas.rows( { selected: true } ).data(),"idventa");
		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/facturacion/facturas/generacionfacturas/asociarcliente.do'/>?idclienteasociado="+$("#idclienteasociado").val(),				  
			timeout : 100000,
			data: { data: data.toString() },		
			success : function(data) {				
				dt_listgeneracionfacturas.ajax.reload();			        
			},
			error : function(exception) {				
				dt_listgeneracionfacturas.processing(false);
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",		     
					  delay: 5000,
					  buttons: { sticker: false }
				   });			
			}
	});
});

 	//***********************************************BOT�N ELIMINAR CLIENTE DE LA VENTA***********************************
 	$("#tab_generacionfacturas_remove").on("click", function(e) { 
 		showButtonSpinner("#tab_generacionfacturas_remove");
 		
 		var data = sanitizeArray(dt_listgeneracionfacturas.rows( { selected: true } ).data(),"idventa");
 		if (data.length<=0) {
 			new PNotify({
 			      title: '<spring:message code="common.dialog.text.error" />',
 			      text: ' <spring:message code="facturacion.facturas.tabs.generacionfacturas.list.alert.ninguna_seleccion" />',
 				  type: "error",
 				  delay: 5000,
 				  buttons: { sticker: false }
 			   });
 			hideSpinner("#tab_generacionfacturas_remove");
 			return;
 		}			
 		
 		
 			new PNotify({
 			      title: '<spring:message code="common.dialog.text.atencion" />',
 			      text: '<spring:message code="facturacion.facturas.tabs.generacionfacturas.list.confirm.eliminar" />',
 				  hide: false,
 				  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
 		  		  buttons: { closer: false, sticker: false	},
 		  		  history: { history: false	}
 			   }).get().on('pnotify.confirm', function() {
 					
 				   	dt_listgeneracionfacturas.processing(true);
 					
 				   	$.ajax({
 						contenttype: "application/json; charset=utf-8",
 						type : "post",
 						url : "<c:url value='/ajax/facturacion/facturas/generacionfacturas/remove_clienteventa.do'/>",
 						timeout : 100000,
 						data: { data: data.toString() }, 
 						success : function(data) {
 							
 							dt_listgeneracionfacturas.ajax.reload();					
 						},
 						error : function(exception) {
 							
 							dt_listgeneracionfacturas.processing(false); 						
 							new PNotify({
 							      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
 							      text: exception.responseText,
 								  type: "error",		     
 								  delay: 5000,
 								  buttons: { sticker: false }
 							   });			
 						}
 					});
 			   }).on('pnotify.cancel', function() {
 			   });		 
 			hideSpinner("#tab_generacionfacturas_remove");
 		});
 	//***********************************************BOT�N GENERAR FACTURA***********************************
 	$("#tab_generacionfacturas_generar").on("click", function(e) { 
 		
 		
 		var data = sanitizeArray(dt_listgeneracionfacturas.rows( { selected: true } ).data(),"idventa");

 		if (data.length<=0) {
 			new PNotify({
 			      title: '<spring:message code="common.dialog.text.error" />',
 			      text: ' <spring:message code="facturacion.facturas.tabs.generacionfacturas.list.alert.ninguna_seleccion" />',
 				  type: "error",
 				  delay: 5000,
 				  buttons: { sticker: false }
 			   });
 			return;
 		}			
 		
 		new PNotify({
 		      title: '<spring:message code="common.dialog.text.atencion" />',
 		      text: '<spring:message code="facturacion.facturas.tabs.generacionfacturas.list.confirm.generar" />',
 			  hide: false,
 			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
 	  		  buttons: { closer: false, sticker: false	},
 	  		  history: { history: false	}
 		   }).get().on('pnotify.confirm', function() {
 		
 			  dt_listgeneracionfacturas.processing(true);
 			   
 				$.ajax({
 					contenttype: "application/json; charset=utf-8",
 					type : "post",
 					url : "<c:url value='/ajax/facturacion/facturas/generacionfacturas/generar_factura.do'/>?idtipofactura="+$("#buscador_generacionfacturas_selector_serie").val(),
 					timeout : 100000,
 					data: { data: data.toString() }, 
 					success : function(data) {
 						dt_listgeneracionfacturas.processing(false);	
 						dt_listgeneracionfacturas.rows( { selected: true } ).remove().draw();
 						
 						 						
 						var xml = json2xml(data.ArrayList,''); 						
 						 						
 						showButtonSpinner("#tab_generacionfacturas_generar");
 						
 						var url = "<c:url value='/ajax/facturacion/facturas/generacionfacturas/show_listadofacturas.do'/>?"+encodeURI("facturas="+xml);
 						
 						$("#modal-dialog-form .modal-content").load(url, function() {
 							$("#modal-dialog-form").modal('show');
 							setModalDialogSize("#modal-dialog-form", "md");
 						});
 												
 						
 						
 					},
 					error : function(exception) {
 						dt_listgeneracionfacturas.processing(false);
 						
 						new PNotify({
 						      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
 						      text: exception.responseText,
 							  type: "error",		     
 							  delay: 5000,
 							  buttons: { sticker: false }
 						   });			
 					}
 				});

 		   }).on('pnotify.cancel', function() {
 		   });		 
	});
//**********************************************

});


/***********************************************BOT�N EDITAR VENTA/RESERVA  *********************************/
$("#button_clean_gen_facturas").on("click", function(e) {
	$('input[name="cliente"]').val('');
	$('input[name="refventageneracioninicial"]').val('');
	$('input[name="refventageneracionfinal"]').val('');
	$('input[name="refreservageneracioninicial"]').val('');
	$('input[name="refreservageneracionfinal"]').val('');
	$('input[name="localizadorgeneracion"]').val('');
	$('input[name="ventageneracion"]').val('');
	$('input[name="reservageneracion"]').val('');
	$('input[name="espectaculos"]').val('');

})
</script>

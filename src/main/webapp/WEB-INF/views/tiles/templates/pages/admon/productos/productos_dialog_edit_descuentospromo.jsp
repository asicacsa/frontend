<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<x:parse xml="${editardescuento_datos_descuento}" var="editardescuento_datos_descuento_xml" />
<x:parse xml="${editardescuento_selector_tipospromocion}" var="editardescuento_selector_tipospromocion_xml" />
<x:parse xml="${editardescuento_selector_unidadesnegocio}" var="editardescuento_selector_unidadesnegocio_xml" />
<x:parse xml="${editardescuento_selector_productos}" var="editardescuento_selector_productos_xml" />
<x:parse xml="${editardescuento_selector_tarifas}" var="editardescuento_selector_tarifas_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">	
		<x:choose>
			<x:when select="not($editardescuento_datos_descuento_xml/Descuentopromocional)">
				<spring:message code="administracion.productos.tabs.descuentospromo.dialog.crear_descuento.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.productos.tabs.descuentospromo.dialog.editar_descuento.title" />
			</x:otherwise>
		</x:choose>		
	</h4>	
</div>


<div class="modal-body">
	<form id="form_descuento_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	<input id="iddescuentopromocional" name="iddescuentopromocional" type="hidden" value="<x:out select = "$editardescuento_datos_descuento_xml/Descuentopromocional/iddescuentopromocional" />" />
	<input id="idcliente" name="idcliente" type="hidden" value="<x:out select = "$editardescuento_datos_descuento_xml/Descuentopromocional/cliente/idcliente" />" />
	<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.descuentospromo.field.nombre" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editardescuento_datos_descuento_xml/Descuentopromocional/nombre" />">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.descuentospromo.field.descripcion" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">					
					<textarea name="descripcion" id="descripcion" class="form-control"  ><x:out select = "$editardescuento_datos_descuento_xml/Descuentopromocional/descripcion" /></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.descuentospromo.field.tipopromocion" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="idtipopromocion" id="selector_tipopromocion" class="form-control" required="required" disabled="disabled">
						<option value=""></option>
						<x:forEach select="$editardescuento_selector_tipospromocion_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			
		 	<div class="form-group" id="uno">
				<label class="control-label col-md-3 col-sm-3 col-xs-3" id="tipopromocion1_1"><spring:message code="administracion.productos.tabs.descuentospromo.field.n" />*</label>
				<label class="control-label col-md-3 col-sm-3 col-xs-3" id="tipopromocion1_2"><spring:message code="administracion.productos.tabs.descuentospromo.field.descuento" />*</label>
				<label class="control-label col-md-3 col-sm-3 col-xs-3" id="tipopromocion1_3"><spring:message code="administracion.productos.tabs.descuentospromo.field.preciofijo" />*</label>
				<label class="control-label col-md-3 col-sm-3 col-xs-3" id="tipopromocion1_4"><spring:message code="administracion.productos.tabs.descuentospromo.field.descuentofijo" />*</label>
				
				<div class="col-md-9 col-sm-9 col-xs-9">					
					<input name="descuento" id="descuento"  type="text" class="form-control" required="required" value="<x:out select = "$editardescuento_datos_descuento_xml/Descuentopromocional/descuento" />">
				</div>
			</div>
			
			<div class="form-group" id="dos">
				<label class="control-label col-md-3 col-sm-3 col-xs-3" id="tipopromocion1_1_1"><spring:message code="administracion.productos.tabs.descuentospromo.field.m" />*</label>
				
				<div class="col-md-9 col-sm-9 col-xs-9">					
					<input name="descuento2" id="descuento2"  type="text" class="form-control" required="required" value="<x:out select = "$editardescuento_datos_descuento_xml/Descuentopromocional/descuento2" />">
				</div>
			</div>				
			
				<div class="form-group" id="tres">
				<label class="control-label col-md-3 col-sm-3 col-xs-3" id="tipopromocion1_5"><spring:message code="administracion.productos.tabs.descuentospromo.field.productopromocionado" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
				<select name="idproductopromo" id="selector_productospromo" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$editardescuento_selector_productos_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>				
				</div>			
				</div> 
			
			<div class="form-group" id="cuatro">
				<label class="control-label col-md-3 col-sm-3 col-xs-3" id="tipopromocion1_6"><spring:message code="administracion.productos.tabs.descuentospromo.field.tarifaorigen" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="idtarifaorigenpromo" id="selector_tarifasorigenpromo" class="form-control" required="required" >
						<option value=""></option>
						<x:forEach select="$editardescuento_selector_tarifas_xml/ArrayList/Tarifa" var="item">
							<option value="<x:out select="$item/idtarifa" />"><x:out select="$item/nombre" /></option>
						</x:forEach>
					</select>		
				</div>			
				</div>
				
				<div class="form-group" id="cinco">
				<label class="control-label col-md-3 col-sm-3 col-xs-3" id="tipopromocion1_6_1"><spring:message code="administracion.productos.tabs.descuentospromo.field.tarifadestino" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="idtarifadestinopromo" id="selector_tarifasdestinopromo" class="form-control" required="required" >
						<option value=""></option>
						<x:forEach select="$editardescuento_selector_tarifas_xml/ArrayList/Tarifa" var="item">
							<option value="<x:out select="$item/idtarifa" />"><x:out select="$item/nombre" /></option>
						</x:forEach>
					</select>		
				</div>			
				</div>		
			
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.descuentospromo.field.unidadnegocio" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="idunidadnegocio" id="selector_unidadnegocio" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$editardescuento_selector_unidadesnegocio_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			
			<div class="form-group" id="selector_tarifasdescuentos_group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.descuentospromo.field.tarifas" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="tarifasdescuentos[]" id="selector_tarifasdescuentos" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
						<x:forEach select="$editardescuento_selector_tarifas_xml/ArrayList/Tarifa" var="item">
							<option value="<x:out select="$item/idtarifa" />"><x:out select="$item/nombre" /></option>
						</x:forEach>
					</select>
				</div>
			</div>

			</div>			
			
			<!-- ********************************************************************************************* -->
			<div class="col-md-6 col-sm-6 col-xs-12">
			
		   <div class="form-group button-dialog">
           <label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.descuentospromo.field.cliente" /></label>
           	<div class="col-md-9 col-sm-9 col-xs-9">
               	<input name="cliente" id="cliente" type="text" class="form-control" readonly value="<x:out select = "$editardescuento_datos_descuento_xml/Descuentopromocional/cliente/nombre" />">
              </div>
            </div>
		
			<div class="form-group date-picker">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.descuentospromo.field.vigencia" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						  <a type="button" class="btn btn-default btn-clear-date" id="button_vigencia_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.descuentospromo.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
						  </a>			
                        <div class="input-prepend input-group">
                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                          <input type="text" name="vigencia" id="apertura" class="form-control"  value="" readonly/>
                          <input type="hidden" required="required" name="fechainiciovigencia" value="<x:out select = "$editardescuento_datos_descuento_xml/Descuentopromocional/fechainiciovigencia" />"/>
                          <input type="hidden" required="required" name="fechafinvigencia" value="<x:out select = "$editardescuento_datos_descuento_xml/Descuentopromocional/fechafinvigencia" />"/>
                        </div>
					</div>
				</div>
				
				<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.descuentospromo.field.orden" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">					
					<input name="orden" id="orden" data-inputmask="'mask': '9{0,10}'" type="text" class="form-control" value="<x:out select = "$editardescuento_datos_descuento_xml/Descuentopromocional/orden" />">
				</div>
			</div>
			
			<div class="form-group" id="selector_productosdescuentos_form_group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.descuentospromo.field.productos" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="productosdescuentos[]" id="selector_productosdescuentos" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
						<x:forEach select="$editardescuento_selector_productos_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
	
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_descuento_button" type="submit" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
			
 	</form>
	

</div>

<script >




$(":input").inputmask();
//*********************************************
$("#cliente").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>", "#cliente", "#idcliente");

//********************************************************************************
hideSpinner("#tab_descuentos_new");
hideSpinner("#tab_descuentos_edit");
//********************************************************************************


$(".select-select2").select2({
	language : "es",
	containerCssClass : "single-line"
});
//********************************************************************************
var selectedValues = new Array();
<x:forEach select="$editardescuento_datos_descuento_xml/Descuentopromocional/descuentoproductos/Descuentoproducto/producto" var="item">	
selectedValues.push('<x:out select="$item/idproducto" />');
</x:forEach>
$('#selector_productosdescuentos').val(selectedValues);
$('#selector_productosdescuentos').trigger('change.select2');	
//********************************************************************************
var selectedValues = new Array();
<x:forEach select="$editardescuento_datos_descuento_xml/Descuentopromocional/descuentotarifas/Descuentotarifa/tarifa" var="item">	
selectedValues.push('<x:out select="$item/idtarifa" />');
</x:forEach>
$('#selector_tarifasdescuentos').val(selectedValues);
$('#selector_tarifasdescuentos').trigger('change.select2');	
//********************************************************************************

if("<x:out select = '$editardescuento_datos_descuento_xml/Descuentopromocional/tipopromocion/idtipopromocion' />"=="")
	$("#selector_tipopromocion").attr("disabled", false);

	
$('#selector_tipopromocion option[value="<x:out select = "$editardescuento_datos_descuento_xml/Descuentopromocional/tipopromocion/idtipopromocion" />"]').attr("selected", "selected");

//********************************************************************************
$('#selector_unidadnegocio option[value="<x:out select = "$editardescuento_datos_descuento_xml/Descuentopromocional/unidadnegocio/idunidadnegocio" />"]').attr("selected", "selected");
//********************************************************************************
$('#selector_tipopromocion option[value="<x:out select = "$editardescuento_datos_descuento_xml/Descuentopromocional/tipopromocion/idtipopromocion" />"]').attr("selected", "selected");

if( $("#selector_tipopromocion").val()=="")
	{
		//Si no esta seleccionada ningun tipo de promoción los campos se mantienen ocultos		
   		$("#uno").hide();
   		$("#dos").hide();
   		$("#tres").hide();
   		$("#cuatro").hide();
   		$("#cinco").hide();
	} else
		habilitarCamposPromocion();
//********************************************************************************
$('#selector_tarifasorigenpromo option[value="<x:out select = "$editardescuento_datos_descuento_xml/Descuentopromocional/tarifaorigen/idtarifa" />"]').attr("selected", "selected");
//********************************************************************************
$('#selector_tarifasdestinopromo option[value="<x:out select = "$editardescuento_datos_descuento_xml/Descuentopromocional/tarifadestino/idtarifa" />"]').attr("selected", "selected");
//********************************************************************************
$('#selector_productospromo option[value="<x:out select = "$editardescuento_datos_descuento_xml/Descuentopromocional/descprodpromocionados/Descprodpromocionado/producto/idproducto" />"]').attr("selected", "selected");
//********************************************************************************

$today= moment().format("DD/MM/YYYY");

//Fecha de vigencia
var dia_inicio = $today;
var dia_fin = $today;

if($('input[name="fechainiciovigencia"]').val()!='')
{	
dia_inicio = $('input[name="fechainiciovigencia"]').val().substring(0, 10);
}
if($('input[name="fechafinvigencia"]').val()!='')
{	
dia_fin = $('input[name="fechafinvigencia"]').val().substring(0, 10);
}


$('input[name="vigencia"]').val( dia_inicio + ' - ' + dia_fin);
$('input[name="fechainiciovigencia"]').val( dia_inicio);
$('input[name="fechafinvigencia"]').val( dia_fin);
/*
if('<x:out select = "$editardescuento_datos_descuento_xml/Descuentopromocional/fechainiciovigencia" />'=='')
{
	$('input[name="vigencia"]').val('');
	$('input[name="fechainiciovigencia"]').val('');
	$('input[name="fechafinvigencia"]').val('');
}
if('<x:out select = "$editardescuento_datos_descuento_xml/Descuentopromocional/fechafinvigencia" />'=='')
	$('input[name="fechafinvigencia"]').val('');*/



$('input[name="vigencia"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
  	  	 $('input[name="vigencia"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('input[name="fechainiciovigencia"]').val(start.format('DD/MM/YYYY'));  	  	 
  	  	 $('input[name="fechafinvigencia"]').val(end.format('DD/MM/YYYY'));
	 });

$("#button_vigencia_clear").on("click", function(e) {
    $('input[name="vigencia"]').val('');
    $('input[name="fechainiciovigencia"]').val('');
    $('input[name="fechafinvigencia"]').val('');
});

//***************************************************************************************
$("#form_descuento_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFormDataDescuento();
		}
	}
	);
	//********************************************************************************	
	function saveFormDataDescuento() 
	{
	if( $("#apertura").val() == "")
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="administracion.productos.tabs.descuentospromo.list.confirm.guardarsinfecha" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
	  		  buttons: { closer: false, sticker: false	},
	  		  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
			   grabarDescuento();
		   }).on('pnotify.cancel', function() {   
			});
	else
		grabarDescuento();
		
		//************
		
	}
//***********************************
function grabarDescuento() 
	{
	   $("#selector_tipopromocion").attr("disabled", false);
		var data = $("#form_descuento_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/productos/descuentos/save_descuentospromocionales.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
			
				
				$("#modal-dialog-form").modal('hide');
				
				dt_listdescuentospromo.ajax.reload(null,false);
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});	
	}
	//**************************************************************
	$( "#selector_tipopromocion" ).change(function() {
		habilitarCamposPromocion();
	})	
//*********************************************
function habilitarCamposPromocion(){		
		seleccion= $("#selector_tipopromocion").val();		
		
		switch (seleccion) { 
	   	case "1":	   		
	   		$("#uno").show();
	   		$("#dos").show();
	   		$("#tres").hide();
	   		$("#cuatro").hide();
	   		$("#cinco").hide();
	   		//Para el grupo uno habilito la label 1_1
	   		$("#tipopromocion1_1").show();
	   		$("#tipopromocion1_2").hide();
	   		$("#tipopromocion1_3").hide();
	   		$("#tipopromocion1_4").hide();	   		
	   		$("#selector_tarifasdescuentos_group").show();		
	   		break ;
	   	case "2": 
	   		$("#uno").show();
	   		$("#dos").hide();
	   		$("#tres").hide();
	   		$("#cuatro").hide();
	   		$("#cinco").hide();
	   	//Para el grupo uno habilito la label 1_2
	   		$("#tipopromocion1_1").hide();
	   		$("#tipopromocion1_2").show();
	   		$("#tipopromocion1_3").hide();
	   		$("#tipopromocion1_4").hide();	   		
	   		$("#selector_tarifasdescuentos_group").show();
	   		break;	      	
	   	case "3": 
	   		$("#uno").show();
	   		$("#dos").hide();
	   		$("#tres").hide();
	   		$("#cuatro").hide();
	   		$("#cinco").hide();
	   	//Para el grupo uno habilito la label 1_3
	   		$("#tipopromocion1_1").hide();
	   		$("#tipopromocion1_2").hide();
	   		$("#tipopromocion1_3").show();
	   		$("#tipopromocion1_4").hide();	   		
	   		$("#selector_tarifasdescuentos_group").show();
	      	break; 
	   	case "4": 	   		
	   		$("#uno").show();
	   		$("#dos").hide();
	   		$("#tres").hide();
	   		$("#cuatro").hide();
	   		$("#cinco").hide();
	   	//Para el grupo uno habilito la label 1_4
	   		$("#tipopromocion1_1").hide();
	   		$("#tipopromocion1_2").hide();
	   		$("#tipopromocion1_3").hide();
	   		$("#tipopromocion1_4").show();	   		
	   		$("#selector_tarifasdescuentos_group").show();
	      	break;
	   	case "5": 	   			   		
	   		$("#uno").hide();
	   		$("#dos").hide();
	   		$("#tres").show();
	   		$("#cuatro").hide();
	   		$("#cinco").hide();	   		
	   		$("#selector_tarifasdescuentos_group").show();
	      	break;
	   	case "6": 
	   		$("#uno").hide();
	   		$("#dos").hide();
	   		$("#tres").hide();
	   		$("#cuatro").show();
	   		$("#cinco").show();	   		
	   		$("#selector_tarifasdescuentos_group").hide();
	      	break;
	   	default: 	     		
	   		$("#uno").hide();	   	
   			$("#dos").hide();
   			$("#tres").hide();
   			$("#cuatro").hide();
   			$("#cinco").hide();   		
   			$("#selector_tarifasdescuentos_group").show();
	}
		
	}
	
	//**************************************************************
	
	
	
	
	
	
	
	
</script>

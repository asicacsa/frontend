<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_ruta_web}" var="ventareserva_ruta_web_xml" />




<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.tabs.gestion_pases_club-dialog.listado-pases-imprimir.title" />
	</h4>
</div>
<div class="modal-body">
<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_pases_club_imprimir_pase">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.gestion_pases_club.button.imprimir_pase" />"> <span class="fa fa-print"></span>
				</span>
		</a>	
</div>


<table id="listado_pases_club_table_imprimir" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th></th>
				<th><spring:message code="venta.ventareserva.tabs.gestion_pases_club-dialog.listado-pases-imprimir.nombre"/></th>
			</tr>
		</thead>
		<tbody>	
		</tbody>
</table>


          

          
   <div class="modal-footer">
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>	      

			
</div>
<script>

//GGL string ¿?¿?¿?¿ var ruta_fotos="<x:out select = "$ventareserva_ruta_web_xml/string" />";
var ruta_fotos="<x:out select = "$ventareserva_ruta_web_xml" />";
//alert("Ruta fotos "+ruta_fotos);
var pase;



var pases;

var dt_listpaseclubeditar_imprimir =$('#listado_pases_club_table_imprimir').DataTable( {
	ordering: false,
	"paging":false, 	
	"searching": false,
	info: false,
	initComplete: function( settings, json ) {
   	 window.setTimeout(cargarTablaImpresion, 100);  
	},
	deferLoading: 0,	   
    drawCallback: function( settings ) { activateTooltipsInTable('listado_pases_club_table_imprimir') },
    select: { style: 'simple'},
	language: dataTableLanguage,
	processing: true,
	"columnDefs": [
                   { "visible": false, "targets": [0]}
                 ]
} );



function cargarTablaImpresion()
{
	pases = json.ArrayList.DatosImpresionPase;
	if (pases.length>0)
		$.each(pases, function(key, pase){
 			
			 dt_listpaseclubeditar_imprimir.row.add([
			                     					pase,
			                     					pase.nombreCompleto, 
			                     				]).draw();
	     });
	else
		{
		
	
		dt_listpaseclubeditar_imprimir.row.add([
		                     					pases,
		                     					pases.nombreCompleto, 
		                     				]).draw();
		}
	
	
}

//***************************************************************
$("#tab_pases_club_imprimir_pase").on("click", function(e) {
	
	
	pase = dt_listpaseclubeditar_imprimir.rows( { selected: true } ).data()[0][0];
	
	var xml = "<java.util.List><IdentificadorPasesDtoActualizado>";
	xml+="<idpase>"+pase.IdentificadorPasesDto.idpase+"</idpase>";
	xml+="<esAbono>1</esAbono>";
	xml+="<codigobarras>"+pase.codigobarras+"</codigobarras>";
	xml+="</IdentificadorPasesDtoActualizado></java.util.List>";
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/abono/actualizarImpresion.do'/>",
		timeout : 100000,
		data: {
			xml: xml
		},
		success : function(data) {	
			$("#modal-dialog-form-5 .modal-content").load("<c:url value='/ajax/venta/ventareserva/pases_club/pantalla_imprimir_pase.do'/>", function() {
				$("#modal-dialog-form-5").modal('show');
				setModalDialogSize("#modal-dialog-form-5", "xs");
			});			
		}
	})
	
})

</script>
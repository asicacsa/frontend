<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


    <div class="col-md-12 col-sm-12 col-xs-12">
    
    <div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_perfilesvisitante_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.perfilesvisitante.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_perfilesvisitante_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.perfilesvisitante.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_perfilesvisitante_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.perfilesvisitante.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>		
	</div>
    
	<table id="datatable-lista-perfilesvisitante" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>			
				<th><spring:message code="administracion.productos.tabs.perfilesvisitante.list.header.nombre" /></th>
				<th><spring:message code="administracion.productos.tabs.perfilesvisitante.list.header.descripcion" /></th>
				<th><spring:message code="administracion.productos.tabs.perfilesvisitante.list.header.orden" /></th>	
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>
var dt_listperfilesvisitante=$('#datatable-lista-perfilesvisitante').DataTable( {	
    ajax: {
        url: "<c:url value='/ajax/admon/productos/perfilesvisitante/list_perfilesvisitante.do'/>",
        rowId: 'idperfilvisitante',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.perfilvisitante)); return(""); 
        						},
        						error: function (xhr, error, thrown) {
        					         if (xhr.responseText=="403") {
        					               $("#perfiles-tab").hide();
        					         }    
        					         else
        					      	 {
        					      	 	$("#datatable-lista-perfilesvisitante_processing").hide();
        					      		new PNotify({
        										title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
        										text : xhr.responseText,
        										type : "error",
        										delay : 5000,
        										buttons : {
        											closer : true,
        											sticker : false
        										}					
        									});
        					      	 }
        					  },       
       
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listperfilesvisitante.data().count()>0) dt_listperfilesvisitante.columns.adjust().draw(); });
	},
    columns: [             
              { data: "nombre", type: "spanish-string" ,  defaultContent:""} ,
              { data: "descripcion", type: "spanish-string" ,  defaultContent:""},
              { data: "orden", type: "spanish-string" ,  defaultContent:""}                      
              
    ],    
    drawCallback: function( settings ) {
    	$('[data-toggle="tooltip"]').tooltip();
    },
    select: { style: 'os'},
	language: dataTableLanguage,
	processing: true,
} );

insertSmallSpinner("#datatable-lista-perfilesvisitante_processing");

/***********************************************BOT�N NUEVO***********************************/

$("#tab_perfilesvisitante_new").on("click", function(e) { 
	showButtonSpinner("#tab_perfilesvisitante_new");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/productos/perfilesvisitante/show_perfilesvisitante.do'/>", function() {
		setModalDialogSize("#modal-dialog-form", "sm");	
		$("#modal-dialog-form").modal('show');
	});
})
/***********************************************BOT�N EDITAR***********************************/
$("#tab_perfilesvisitante_edit").on("click", function(e) {
	
	var data = sanitizeArray(dt_listperfilesvisitante.rows( { selected: true } ).data(),"idperfilvisitante");
	
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.productos.tabs.formasdepago.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.productos.tabs.perfilesvisitante.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	showButtonSpinner("#tab_perfilesvisitante_edit");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/productos/perfilesvisitante/show_perfilesvisitante.do'/>?id="+data[0], function() {
		setModalDialogSize("#modal-dialog-form", "sm");
		$("#modal-dialog-form").modal('show');
	});
});
/***********************************************BOT�N ELIMIMAR***********************************/
$("#tab_perfilesvisitante_remove").on("click", function(e) { 
		
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="administracion.productos.tabs.perfilesvisitante.list.confirm.eliminar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
	  		  buttons: { closer: false, sticker: false	},
	  		  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
		
			   dt_listperfilesvisitante.processing(true);
				
				var data = sanitizeArray(dt_listperfilesvisitante.rows( { selected: true } ).data(),"idperfilvisitante");			   
				$.ajax({
					contenttype: "application/json; charset=utf-8",
					type : "post",
					url : "<c:url value='/ajax/admon/productos/perfilesvisitante/remove_perfilesvisitante.do'/>",
					timeout : 100000,
					data: { data: data.toString() }, 
					success : function(data) {
						dt_listperfilesvisitante.ajax.reload();					
					},
					error : function(exception) {
						dt_listperfilesvisitante.processing(false);
						
						new PNotify({
						      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						      text: exception.responseText,
							  type: "error",		     
							  delay: 5000,
							  buttons: { sticker: false }
						   });			
					}
				});

		   }).on('pnotify.cancel', function() {
		   });		 

	});  


</script>


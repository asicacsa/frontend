<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="ventareserva.operacionescaja.cerrarcaja.dialog.cerrar.title" />
	</h4>	
</div>

<div class="modal-body">
	<form id="form_cerrarcaja_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	    <input id="idCaja" name="idcaja" type="hidden" value="" />
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="ventareserva.operacionescaja.cerrarcaja.field.importe" /></label>
			<div class="col-md-9 col-sm-9 col-xs-9">
				<input name="importe" id="importe" required="required" type="text" class="form-control required" value="">
                <span class="fa fa-euro form-control-feedback right" aria-hidden="true"></span>
			</div>
			<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <div class="checkbox">
                  <label>
                    <input id="informe" type="checkbox" value="" checked> <spring:message code="ventareserva.operacionescaja.cerrarcaja.field.informe" />
                  </label>
                </div>
            </div>	
		</div>
			
	</form>

	<div class="modal-footer">
		<button id="save_cerrarcaja_button" type="button" class="btn btn-primary save_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>

</div>

<script>

$("#save_cerrarcaja_button").on("click", function(e) {
	$("#form_cerrarcaja_data").submit();
})

$("#form_cerrarcaja_data").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
    rules: {
	    importe: {
			required: true,
			number: true
	    }
	},
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		saveFormData();		
	}
});

//********************************************************************************	
function saveFormData() {
	$('input[name="idcaja"]').val("${idCaja}");
	var data = $("#form_cerrarcaja_data").serializeObject();

	showSpinner("#modal-dialog-form .modal-content");

	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/operacionescaja/cerrar_caja.do'/>",
		timeout : 100000,
		data: data, 
		success : function(data) {
			hideSpinner("#modal-dialog-form .modal-content");
			dt_listcajas.ajax.reload(null,false);
			if (document.getElementById("informe").checked) {
				window.open("../../verInformeCierreCaja.action?idcaja="+"${idCaja}"+"&nombreinforme=VR_CJ_001", "_blank");
			}
			$("#modal-dialog-form").modal('hide');
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : { sticker : false	}
			});		
		},
		error : function(exception) {
			hideSpinner("#modal-dialog-form .modal-content");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});		
}	

</script>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${numerada_tipos_producto}" var="numerada_tipos_producto_xml" />


<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2><spring:message code="venta.numerada.tabs.bloquear_localidades.representacion_bloquear" /></h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">
	
		<form id="form_selector_bloquear_localidades" class="form-horizontal form-label-left">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.numerada.tabs.bloquear_localidades.field.temporada" /></label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<select class="form-control" name="idtemporada" id="idtemporada">										
						</select>
					</div>					
				</div>
			</div>
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.numerada.tabs.bloquear_localidades.field.tipo_producto" /></label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<select class="form-control" name="idtipo_producto" id="idtipo_producto">
							<option value=""></option>
							<x:forEach select="$numerada_tipos_producto_xml/ArrayList/tipoproducto" var="item">
								<option value="<x:out select="$item/idtipoproducto" />"><x:out select="$item/nombre" /></option>
							</x:forEach>							
						</select>
					</div>					
				</div>
			</div>
			
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.numerada.tabs.bloquear_localidades.field.contenido" /></label>
					<div id="espectaculo_programacion_div" class="col-md-4 col-sm-4 col-xs-12">
						<select class="form-control" name="idcontenido" id="idcontenido">													
						</select>
					</div>					
				</div>
			</div>
		</form>
		<div class="clearfix"></div>
		<div class="ln_solid"></div>		
		
		
	</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_bloquear_localidades_siguiente">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada.tabs.bloquear_localidades.list.button.siguiente" />"> <span class="fa fa-angle-double-right"></span>
			</span>
		</a>				
	</div>

	<table id="datatable-lista-sesiones" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th></th>
				<th><spring:message code="venta.numerada.tabs.bloquear_localidades.list.header.contenido" /></th>
				<th><spring:message code="venta.numerada.tabs.bloquear_localidades.list.header.fecha" /></th>
				<th><spring:message code="venta.numerada.tabs.bloquear_localidades.list.header.hora" /></th>				
				<th><spring:message code="venta.numerada.tabs.bloquear_localidades.list.header.bloq" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>


<script>

var temporadas_json = ${numerada_temporadas_programacion};
var temporadas_json_array = temporadas_json.ArrayList.LabelValue;
var contenidos;
var temporadas = $("#idtemporada"); 
var temporadasVentas = $("#idtemporadaVenta"); 

if(temporadas_json_array.length>1)
	{
	temporadas.append("<option value=''></option>");
	for (k = 0; k < temporadas_json_array.length; k++)
		{
		temporadas.append("<option value='" + temporadas_json_array[k].value+ "'>" + temporadas_json_array[k].label + "</option>");
		temporadasVentas.append("<option value='" + temporadas_json_array[k].value+ "'>" + temporadas_json_array[k].label + "</option>");
		}		
	}
else
	{
	temporadas.append("<option value='" + temporadas_json_array.value+ "'>" + temporadas_json_array.label + "</option>");	
	temporadasVentas.append("<option value='" + temporadas_json_array.value+ "'>" + temporadas_json_array.label + "</option>");
	}
	

	

dtsesiones=$('#datatable-lista-sesiones').DataTable( {
	"paging": true,
	"pageLength": 10,
	"info": false,
	"searching": false,
	select: { style: 'single'},
	columnDefs: [
	             { "targets": [0], "visible": false }
	             ],
	language: dataTableLanguage
	});


//********************************************************************************
$("#idcontenido").on("change", function(e) {
	dtsesiones.clear();
	var indice =$("#idcontenido").prop('selectedIndex');
	var contenido = contenidos;
	
	if(contenidos.length>0)
	  contenido = contenido[indice-1];	
	
	$("#id_recinto").val(contenido.tipoproducto.recinto.idrecinto);
	
	var sesiones = contenido.sesions.Sesion;	
	

	$.each(sesiones, function(key, sesion){					
		var bloqueado = "";
		if (sesion.bloqueado==1) 
			bloqueado = '<i class="fa fa-check-circle" aria-hidden="true"></i>';

		
		dtsesiones.row.add( [ sesion, contenido.nombre, sesion.fecha.split("-")[0], sesion.horainicio, bloqueado ] )
	    .draw()
	    });
    	
	
})

//**********************************************************************
$("#idtipo_producto").on("change", function(e) {
	showFieldSpinner("#espectaculo_programacion_div");	
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/list_contenidos_by_tipo.do'/>",
		timeout : 100000,
		data: {
				idtipo_producto: $("#idtipo_producto").val(),
				idtemporada: $("#idtemporada").val()
			  }, 
		success : function(data) {
			hideSpinner("#espectaculo_programacion_div");
			var $select=$("#idcontenido");
			$select.html('');
			$select.prepend("<option value='' selected='selected'></option>");
			contenidos = data.ArrayList.Contenido;
			if(contenidos!=undefined)
				{
				if(contenidos.length>0)
				{
						$.each(contenidos, function(key, val){					
					      $select.append('<option value="' + val.idcontenido + '">' + val.nombre + '</option>');
					    });
				}	
				else
					$select.append('<option value="' + contenidos.idcontenido + '">' + contenidos.nombre + '</option>');
				}
				
		},
		error : function(exception) {
			hideSpinner("#espectaculo_localidades_div");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,				  
				  buttons: { sticker: false }				  
			   });		
		}
	});
	
});

//**********************************************************************
$("#idtemporada").on("change", function(e) {
	
	showFieldSpinner("#espectaculo_programacion_div");	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/list_contenidos_temporada.do'/>",
		timeout : 100000,
		data: {
				idtemporada: $("#idtemporada").val()
			  }, 
		success : function(data) {
			hideSpinner("#espectaculo_programacion_div");
			var $select=$("#idcontenido");
			$select.html('');
			$select.prepend("<option value=''></option>");
			contenidos = data.ArrayList.Contenido;
			if(contenidos!=undefined)
				{
				if(contenidos.length>0)
				{
						$.each(contenidos, function(key, val){					
					      $select.append('<option value="' + val.idcontenido + '">' + val.nombre + '</option>');
					    });
				}	
				else
					$select.append('<option value="' + contenidos.idcontenido + '">' + contenidos.nombre + '</option>');
				}
				
		},
		error : function(exception) {
			hideSpinner("#espectaculo_localidades_div");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,				  
				  buttons: { sticker: false }				  
			   });		
		}
	});
	
});


//********************************************************************
$("#tab_bloquear_localidades_siguiente").on("click", function(e) 
{ 
	
	var id_recinto = $("#id_recinto").val();
	var id_sesion = dtsesiones.rows( { selected: true } ).data()[0][0].idsesion;
	
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_seleccion_localidadeds_venta_numerada.do'/>?idSesion="+id_sesion+"&idrecinto="+id_recinto+"&vender=0", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});	
	
	
})


</script>


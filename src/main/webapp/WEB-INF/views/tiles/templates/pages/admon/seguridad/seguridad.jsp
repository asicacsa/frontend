<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!-- Pestañas ------------------------------------------------------------->
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 maintab">
		<div class="" role="tabpanel" data-example-id="togglable-tabs">
			<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
				<li role="presentation" class="active"><a href="#tab_usuarios" id="usuarios-tab" role="tab" data-toggle="tab" aria-expanded="true"><spring:message code="administracion.seguridad.tabs.usuarios.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_perfiles" role="tab" id="perfiles-tab" data-toggle="tab" aria-expanded="false"><spring:message code="administracion.seguridad.tabs.perfiles.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_grupos" role="tab" id="grupos-tab" data-toggle="tab" aria-expanded="false"><spring:message code="administracion.seguridad.tabs.grupos.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_redes" role="tab" id="redes-tab" data-toggle="tab" aria-expanded="false"><spring:message code="administracion.seguridad.tabs.redes.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_restricciones" role="tab" id="restricciones-tab" data-toggle="tab" aria-expanded="false"><spring:message code="administracion.seguridad.tabs.restricciones.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_auditoria" role="tab" id="auditoria-tab" data-toggle="tab" aria-expanded="false"><spring:message code="administracion.seguridad.tabs.auditoria.title" /></a></li>
			</ul>
		</div>
	</div>
</div>

<!-- Area de trabajo ------------------------------------------------------>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel thin_padding">

			<div id="myTabContent" class="tab-content">
				<div role="tabpanel" class="tab-pane fade active in" id="tab_usuarios" aria-labelledby="usuarios-tab">
					<tiles:insertAttribute name="tab_usuarios" />
				</div>

				<div role="tabpanel" class="tab-pane fade" id="tab_perfiles" aria-labelledby="perfiles-tab">
					<tiles:insertAttribute name="tab_perfiles" />
				</div>

				<div role="tabpanel" class="tab-pane fade" id="tab_grupos" aria-labelledby="grupos-tab">
					<tiles:insertAttribute name="tab_grupos" />
				</div>

				<div role="tabpanel" class="tab-pane fade" id="tab_redes" aria-labelledby="redes-tab">
					<tiles:insertAttribute name="tab_redes" />
				</div>

				<div role="tabpanel" class="tab-pane fade" id="tab_restricciones" aria-labelledby="restricciones-tab">
					<tiles:insertAttribute name="tab_restricciones" />
				</div>

				<div role="tabpanel" class="tab-pane fade" id="tab_auditoria" aria-labelledby="auditoria-tab">
					<tiles:insertAttribute name="tab_auditoria" />
				</div>
			</div>

			<div class="clearfix"></div>
			
		
		</div>
	</div>
</div>

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form"/>
<tiles:insertAttribute name="modal_dialog" />

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${impresion_entradas}" var="impresion_entradas_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal"
		aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.busqueda.tabs.bono.entradas.impresion.title" />					
	</h4>
</div>	

<div class="modal-body">
		<table id="impresiones_entradas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th id="fecha"><spring:message code="venta.ventareserva.busqueda.tabs.bono.entradas.impresion.field.fecha"/></th>
				<th id="usuario"><spring:message code="venta.ventareserva.busqueda.tabs.bono.entradas.impresion.field.usuario" /></th>
				<th id="reimpresion"><spring:message code="venta.ventareserva.busqueda.tabs.bono.entradas.impresion.field.reimpresion" /></th>							  						 
			</tr>
		</thead>
		<tbody>				
			<x:forEach select="$impresion_entradas_xml/ArrayList/Impresionentradas" var="item">
				<tr class="seleccionable">
					<td id="fecha" >
								<c:set var="fecha"><x:out select="$item/fecha" /></c:set>
								<fmt:parseDate value="${fecha}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateada"/>
								<fmt:formatDate value="${fechaformateada}" pattern="dd/MM/yyyy" type="DATE"/>                      			     
					</td>
					<td id="usuario" ><x:out select="$item/usuario/nombrecompleto" /></td>
					<td id="reimpresion" >
						<c:set var="reimpresion"><x:out select="$item/reimpresion" /></c:set>
                        <c:choose>
                         <c:when test="${impresion>0}"><i class="fa fa-check" aria-hidden="true"></i></c:when>
                         <c:otherwise><i class="fa fa-close" aria-hidden="true"></i></c:otherwise>
                        </c:choose>	
					</td>																						
				</tr>																
			</x:forEach>
		</tbody>				
	</table>	



		<div class="modal-footer">
			<button type="button" class="btn btn-success close_dialog"
				data-dismiss="modal">
				<spring:message code="common.button.accept" />
			</button>
		</div>

</div>	
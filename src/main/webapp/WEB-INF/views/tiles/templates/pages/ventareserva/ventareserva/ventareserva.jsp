<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_productos_principales}" var="ventareserva_productos_principales_xml" />
<x:parse xml="${ventareserva_puntos_recogida}" var="ventareserva_puntos_recogida_xml" />
<x:parse xml="${ventareserva_canales_indirectos}" var="ventareserva_canales_indirectos_xml" />
<x:parse xml="${ventareserva_idiomas}" var="ventareserva_idiomas_xml" />
<x:parse xml="${ventareserva_operaciones_canal}" var="ventareserva_operaciones_canal_xml" />
<x:parse xml="${ventareserva_formas_pago}" var="ventareserva_formas_pago_xml" />
<x:parse xml="${ventareserva_pago_canal}" var="ventareserva_pago_canal_xml" />

<!-- Pestañas ------------------------------------------------------------->
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 maintab">
		<div class="" role="tabpanel" data-example-id="togglable-tabs">
			<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
				<li role="presentation" class="active"><a href="#tab_venta_individual" role="tab" id="venta-individual-tab" data-toggle="tab" aria-expanded="true"><spring:message code="venta.ventareserva.tabs.venta_individual.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_reserva_grupo" role="tab" id="reserva-grupo-tab" data-toggle="tab" aria-expanded="false"><spring:message code="venta.ventareserva.tabs.grupo.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_reserva_individual" role="tab" id="reserva-individual-tab" data-toggle="tab" aria-expanded="false"><spring:message code="venta.ventareserva.tabs.reserva_individual.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_emision_bono" id="emision-bono-tab" role="tab" data-toggle="tab" aria-expanded="false"><spring:message code="venta.ventareserva.tabs.emision_bono.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_canje_bono" id="canje-bono-tab" role="tab" data-toggle="tab" aria-expanded="false"><spring:message code="venta.ventareserva.tabs.canje_bono.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_gestion_pases_club" role="tab" id="gestion-pases-club-tab" data-toggle="tab" aria-expanded="false"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.title" /></a></li>
			</ul>
		</div>
	</div>
</div>

<!-- Area de trabajo ------------------------------------------------------>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div id="main-ventareserva" class="x_panel thin_padding">

			<div id="myTabContent" class="tab-content">
				<div role="tabpanel" class="tab-pane fade active in" id="tab_venta_individual" aria-labelledby="venta-individual-tab">
					<tiles:insertAttribute name="tab_venta_individual" />
				</div>

				<div role="tabpanel" class="tab-pane fade" id="tab_reserva_grupo" aria-labelledby="reserva-grupo-tab">
					<tiles:insertAttribute name="tab_reserva_grupo" />
				</div>

				<div role="tabpanel" class="tab-pane fade" id="tab_reserva_individual" aria-labelledby="reserva-individual-tab">
					<tiles:insertAttribute name="tab_reserva_individual" />
				</div>

				<div role="tabpanel" class="tab-pane fade" id="tab_emision_bono" aria-labelledby="emision-bono-tab">
					<tiles:insertAttribute name="tab_emision_bono" />
				</div>

				<div role="tabpanel" class="tab-pane fade" id="tab_canje_bono" aria-labelledby="canje-bono-tab">
					<tiles:insertAttribute name="tab_canje_bono" />
				</div>
				
				<div role="tabpanel" class="tab-pane fade" id="tab_gestion_pases_club" aria-labelledby="gestion-pases-club-tab">
					<tiles:insertAttribute name="tab_gestion_pases_club" />
				</div>
			</div>

			<div class="clearfix"></div>
			
		
		</div>
	</div>
</div>

<script>

/* Se cargan los productos principales en un Array */

showSpinner("#main-ventareserva");
var unidadesnegocio = new Array();
var tabla;

var productos= new Array();
<x:forEach select="$ventareserva_productos_principales_xml/ArrayList/Entry" var="item">
	productos=[];
	<x:forEach select="$item/productos/Producto" var="producto">
		productos.push({value: '<x:out select="$producto/value" />', label: '<x:out select="$producto/label" />'});
	</x:forEach>
	unidadesnegocio['<x:out select="$item/unidadnegocio/nombre" />']= productos;
</x:forEach>
hideSpinner("#main-ventareserva");

/* Función genérica para obtener el xml de las lineas de detalle en las operaciones de Venta y Reserva */
function construir_xml_lineasdetalle(lineas_detalle_sel) {
	var xml_lineasdetalle= "<lineadetalles>";
	for (var i=0; i<lineas_detalle_sel.length; i++) { 
		xml_lineasdetalle+="<Lineadetalle>";
		xml_lineasdetalle+=json2xml(lineas_detalle_sel[i][0].Lineadetalle,"");
		xml_lineasdetalle+="</Lineadetalle>";
	}
	xml_lineasdetalle+="</lineadetalles>";
	return(xml_lineasdetalle);
}

/* 
 * En el json de get_producto no se indica el idtarifa del Perfil/Tarifa preseleccionado. Hay que 
 * buscarlo entre todos los perfiles.
 */

function get_idtarifa(json) {
	var idtarifa="",
		nombre= json.Lineadetalle.perfilvisitante.tarifa.nombre;
	if (nombre!="") {
		var item= json.Lineadetalle.perfiles.Perfilvisitante;
		if (item.length>0)
		    $.each(item, function(key, val){
		    	if (val.tarifa.nombre==nombre) idtarifa= val.tarifa.idtarifa;
		    });
		else
			if (item.tarifa.nombre==nombre) idtarifa= item.tarifa.idtarifa;
	}
	return(idtarifa);
}

var actualizado_linea = false;



function obtener_totales_venta_temporal(tabname,tablename,idcliente,idtipoventa,seleccion) {
	var tabla_set= {};
	
	if (seleccion && $("#datatable_list_"+tablename).DataTable().rows('.selected').any())
		tabla_set= { selected: true };
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/obtener_totales_ventatemporal.do'/>",
		timeout : 100000,
		data: {
			idcliente: idcliente,
			idtipoventa: idtipoventa,
			xml: construir_xml_lineasdetalle($("#datatable_list_"+tablename).DataTable().rows(tabla_set).data())
		},
		success : function(data) {
			if(seleccion && (tabname==="venta_individual"))
				{
				importe_total = Number(data.TotalVentaDTO.importeventa);
				importe_devolver= -importe_total;
				if(importe_total<0.01)
					{
					$('#selector_importe1').attr('disabled', 'disabled');
					$('#selector_importe2').attr('disabled', 'disabled');
					$('#selector_importe3').attr('disabled', 'disabled');	
					}
				$("#total-cobrar").text(importe_total.toFixed(2));
				$("#total-devolver").text(importe_devolver.toFixed(2));
				}
			else
				{
				$(".totales_"+tabname+" #total-val").text(data.TotalVentaDTO.importeventa);
				$(".totales_"+tabname+" #total-desc").text(data.TotalVentaDTO.importedc);
				$(".totales_"+tabname+" #total-prc").text(data.TotalVentaDTO.porcentaje);
				if(actualizado_linea) 
				    invalidar_linea();
					
				actualizado_linea = false;
				}
		},
		error : function(exception) {
			if(actualizado_linea)
				{
				volverLinea();
				actualizado_linea = false;
				}
			else
				{				
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",
					  delay: 5000,
					  buttons: { closer:true, sticker: false }			  
				   });	
				}
		}
	});	
}
//********************************************************************************	
function enviar_carta_confirmacion(id, nombre_id) {
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/enviar_carta_confirmacion.do'/>",
		timeout : 100000,
		data: {
			id: id,
			nombre_id:nombre_id,
			format: $("#carta_confirmacion_select").val(),
			idioma: $("#selector_idiomas").val()
		},
		success : function(data) {
		// en la aplicación no da respuesta en caso de éxito
		},
		error : function(exception) {
		// en la aplicación no da respuesta en caso de fracaso
		}
	});		
}	

//******************CARGAR SELECTOR IDIOMAS------------------------
function cargarSelectorIdiomas(nombre_selector){
	var i18nlangs= ${sessionScope.i18nlangs};
	for (var i = 0; i < i18nlangs.ArrayList.LabelValue.length; i++){
	   var obj = i18nlangs.ArrayList.LabelValue[i];
	   
	   $("#"+nombre_selector).append('<option value='+obj.value+'>'+obj.label+'</option>');		
	}
}

//********************************************************************************	

</script>

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-2"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-3"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-4"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-5"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-buscar-cliente"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-editar-cliente"/>
<tiles:insertAttribute name="modal_dialog" />


<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_cliente_pais}" var="editar_cliente_pais_xml" />

<div class="modal fade" id="modal_nuevo_cp" tabindex="-1"
	role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close close_dialog" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">�</span>
				</button>
				<h4 class="modal-title">
						<spring:message code="facturacion.facturas.tabs.clientes.nuevo.cp" />			
				</h4>	
			</div>
			<div class="modal-body">

				<form id="form_new_cp" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
				<input type="hidden" id="idTipoProductoAtributo" />
				<div class="col-md-12 col-sm-12 col-xs-12">
		
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.field.pais" />*</label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<select name="new_pais" id="selector_new_pais" class="form-control" required="required">
								<option selected="selected" value="472">Espa�a</option>
								
							</select>					
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.field.provincia" />*</label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<input id="filter_new_provincia" type="text" class="form-control" value=""/>
							<select name="new_provincia" id="selector_new_provincia" class="form-control" required="required">
							   <option value="" ></option>
							</select>					
						</div>
					</div>	
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.field.poblacion" />*</label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<input id="filter_new_poblacion" type="text" class="form-control" value="" disabled/>
							<select name="new_poblacion" id="selector_new_poblacion" class="form-control" required="required">
							    <option value="" ></option>
							</select>					
						</div>
					</div>	
					
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.field.cp" />*</label>
						<div class="col-md-7 col-sm-7 col-xs-7">
							<input id="new_cp" name="new_cp" type="text" class="form-control" value="" disabled/>
						</div>
					</div>
				
					    
		
		
				</div>
		
			</form>
	
				<div class="modal-footer">
				
						<button id="save_cliente_button" type="button" class="btn btn-primary close_nuevo_cp">
							<spring:message code="common.button.accept" />
						</button>
						<button type="button" class="btn btn-cancel close_dialog_cp">
							<spring:message code="common.button.cancel" />
						</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
$("#selector_new_pais").change(function() {
	$( "#filter_new_provincia" ).prop( "disabled", false );
});

$( "#filter_new_provincia" ).keyup(function() {
	var nombre_provincia = $("#filter_new_provincia").val();
	var idPais = $("#selector_new_pais").val();
	var provincia = $('#selector_new_provincia');
	filtrarProvincia(nombre_provincia,idPais,provincia);
});

$("#selector_new_provincia").change(function() {
	$("#filter_new_provincia").val($("#selector_new_provincia option:selected").text());	
	$("#filter_new_provincia").show();
	$("#selector_new_provincia").hide();
	$("#filter_new_poblacion" ).prop( "disabled", false );
});


$( "#filter_new_poblacion" ).keyup(function() {
	var nombre_poblacion = $("#filter_new_poblacion").val();
	var idPais= $("#selector_new_pais").val();
	var idProvincia= $("#selector_new_provincia").val();
	var poblacion = $('#selector_new_poblacion');
	
	filtrarPoblacion(nombre_poblacion,idPais,idProvincia,poblacion);
});

$("#selector_new_poblacion").change(function() {
	$("#filter_new_poblacion").val($("#selector_new_poblacion option:selected").text());	
	$("#filter_new_poblacion").show();	
	$("#selector_new_poblacion").hide();	
	$("#new_cp").prop( "disabled", false );
});

$(".close_nuevo_cp").on("click", function(e) {	
	$("#form_new_cp").submit();	
});

$(".close_dialog_cp").on("click", function(e) {	
	$("#modal_nuevo_cp").modal('hide');
});

$("#form_new_cp").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		guardarCP()
	}
})

	function guardarCP()
	{
		var data = $("#form_new_cp").serializeObject();

		showSpinner("#modal_nuevo_cp .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/facturacion/facturas/clientes/alta_CP.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal_nuevo_cp .modal-content");
				
				$("#modal_nuevo_cp").modal('hide');
				var provincia = $("#filter_new_provincia").val();
				var poblacion = $("#filter_new_poblacion").val();
				returnfilterprovincia.val(provincia)
				returnfilterpoblacion.val(poblacion)
				retunfiltercp.val(provincia+"|"+poblacion+"|"+$("#new_cp").val());
				var idProvincia =  $("#selector_new_provincia").val();
				returnprovincia.append('<option selected value="'+idProvincia+'">'+provincia+'</option>');
				var idPoblacion =  $("#selector_new_poblacion").val();
				returnpoblacion.append('<option selected value="'+idPoblacion+'">'+poblacion+'</option>');

				var idcp = data.Cpmunicipio.cp.idcp;

				returncp.append('<option selected value="'+idcp+'">'+$("#filter_cp").val()+'</option>');
		    
				try
					{
					returnidprovincia.val(idProvincia);
					returnidpoblacion.val(idPoblacion);
					retunidcp.val(idcp);	
					}
				catch(err) {}				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal_nuevo_cp .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});

	}

</script>
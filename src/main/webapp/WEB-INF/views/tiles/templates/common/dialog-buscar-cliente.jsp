<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${buscarcliente_selector_categoriasactividad}" var="buscarcliente_selector_categoriasactividad_xml" />

<div class="filter_list thin_padding">
<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="administracion.productos.tabs.descuentospromo.dialog.buscador_clientes.title" />
	</h4>	
</div>

	<div class="modal-body">

		<form id="form_buscador_cliente" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

		<input type="hidden" id="campoBuscadorClienteDevueltoNombre" value=""/>
		<input type="hidden" id="campoBuscadorClienteDevueltoId" value=""/>
		<input type="hidden" id="campoBuscadorClienteDevueltoCif" value=""/>
		<input type="hidden" id="campoBuscadorClienteDevueltoCp" value=""/>
		<input type="hidden" id="campoBuscadorClienteDevueltoEmail" value=""/>
		<input type="hidden" id="campoBuscadorClienteDevueltoTelefono" value=""/>

		<div class="col-md-6 col-sm-6 col-xs-12">

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.codigo" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="codigo" id="codigo" type="text" class="form-control" required="required" value="">
				</div>
			</div>
			
			
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.nif_cif" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="dni" id="dni" type="text" class="form-control" required="required" value="">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.nombre_razon" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="nombre_razon" id="nombre_razon" type="text" class="form-control" required="required" value="">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.apellido1" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="apellido1"  id="apellido1" type="text" class="form-control" value="">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.apellido2" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="apellido2"  id="apellido2" type="text" class="form-control" value="">
				</div>
			</div>

			
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.categoria" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="idcategoria" id="buscador_cliente_selector_categoria" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$buscarcliente_selector_categoriasactividad_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.actividad" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="idactividad" id="buscador_cliente_selector_actividad" class="form-control" required="required">
						<option value=""></option>						
					</select>
				</div>
			</div>
		</div>


		<div class="col-md-6 col-sm-6 col-xs-12">

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.provincia" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="provincia" id="provincia" type="text" class="form-control" required="required" value="">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.poblacion" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="poblacion" id="poblacion" type="text" class="form-control" required="required" value="">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.direccion" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="direccion" id="direccion" type="text" class="form-control" required="required" value="">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.telefono" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="telefono" id="telefono" type="text" class="form-control" required="required" value="">
				</div>
			</div>
				
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.pais" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="pais"  id="pais" type="text" class="form-control" value="">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.cp" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="cp"  id="cp" type="text" class="form-control" value="">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.fax" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="fax"  id="fax" type="text" class="form-control" value="">
				</div>
			</div>

		</div>
		
		<div class="clearfix"></div>
			<div class="ln_solid"></div>
			<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
				<button id="buscador_cliente_button_filter_list_clientes" type="button" class="btn btn-success pull-right">
					<spring:message code="common.button.filter" />
				</button>
			</div>
			
			<input type="hidden" name="start" value=""/>
			<input type="hidden" name="length" value=""/>
			
	</form>
	
<c:if test = "${editable > 0}">
<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="btn-group pull-right btn-datatable">
			<a type="button" class="btn btn-info" id="tab_clientes_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.clientes.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
			</a>
			<a type="button" class="btn btn-info" id="tab_cliente_editar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.formasdepago.cliente.editar" />"> <span class="fa fa-pencil"></span>
			</span>
			</a>			
		</div>
</div>
</c:if>
<div class="row">

	<table id="datatable-lista-clientes-buscador" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="administracion.productos.tabs.formasdepago.list.header.idcliente" /></th>
				<th><spring:message code="administracion.productos.tabs.formasdepago.list.header.cif" /></th>
				<th><spring:message code="administracion.productos.tabs.formasdepago.list.header.nombre" /></th>				
				<th><spring:message code="administracion.productos.tabs.formasdepago.list.header.provincia" /></th>
				<th><spring:message code="administracion.productos.tabs.formasdepago.list.header.municipio" /></th>
				<th><spring:message code="administracion.productos.tabs.formasdepago.list.header.pais" /></th>
				<th><spring:message code="administracion.productos.tabs.formasdepago.list.header.cp" /></th>
				<th><spring:message code="administracion.productos.tabs.formasdepago.list.header.direccion" /></th>
				<th><spring:message code="administracion.productos.tabs.formasdepago.list.header.telefono" /></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
</div>
</div>
<div class="modal-footer">

		<button id="buscador_cliente_save_cliente_button" type="button" class="btn btn-primary save_dialog">
			<spring:message code="common.button.accept" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>
<script>
//**************************************************************

var dt_listclientes_buscador=null;



//**************************************************************
function buscadorClientesReloadTabla() {

	if (isFormEmpty("#form_buscador_cliente")) {	
		new PNotify({
			  title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
		      text: '<spring:message code="common.dialog.text.empty_filter" />',
			  type: "error",
			  buttons: { sticker: false }				  
		   });
		
		return;
	}	
	
	if (dt_listclientes_buscador==null) {
		dt_listclientes_buscador=$('#datatable-lista-clientes-buscador').DataTable( {
			serverSide: true,
			searching: false,
			ordering: false,
			pagingType: "simple",
			info: false,
		    ajax: {
		        url: "<c:url value='/ajax/list_clientes.do'/>",
		        rowId: 'idcliente',
		        type: 'POST',
		        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Cliente)); return(""); },
		        data: function (params) {
		        	$('input[name="start"]').val(params.start);
		        	$('input[name="length"]').val(params.length);
		        	return ($("#form_buscador_cliente").serializeObject());
		       	},
		       	error: function (xhr, error, code)
	            {
	               	new PNotify({
						title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						text : xhr.responseText,
						type : "error",
						delay : 5000,
						buttons : {
							closer : true,
							sticker : false
						}					
					});
	            }
		    },
		    initComplete: function( settings, json ) {
	    	    $('a#menu_toggle').on("click", function () {if (dt_listclientes_buscador.data().count()>0) dt_listclientes_buscador.columns.adjust().draw(); });
			},
		columnDefs: [
		             { "targets": 9, "visible": false }
        ],
	    columns: [	              
	              { data: "idcliente" , type: "spanish-string" ,  defaultContent:""} ,
	              { data: "identificador", type: "spanish-string" ,  defaultContent:""} ,
	              { data: "nombre", type: "spanish-string" ,  defaultContent:""} ,			
	              { data: "direccionorigen.provincia.nombreprovincia", type: "spanish-string" ,  defaultContent:""} ,
	              { data: "direccionorigen.municipio.nombre" , type: "spanish-string" ,  defaultContent:""} ,
	              { data: "direccionorigen.pais.nombre" , type: "spanish-string" ,  defaultContent:""} ,
	              { data: "direccionorigen.cp.cp" , type: "spanish-string" ,  defaultContent:""} ,
	              { data: "direccionorigen.direccion" , type: "spanish-string" ,  defaultContent:""} ,
	              { data: "direccionorigen.telefonofijo" , type: "spanish-string" ,  defaultContent:""} ,
	              { data: "direccionfiscal.correoelectronico" , type: "spanish-string" ,  defaultContent:""}
	    ],    
	    select: { style: 'single'},
		language: dataTableLanguage,
		processing: true,
		} );

		insertSmallSpinner("#datatable-lista-clientes-buscador_processing");		
	}
	else {
		dt_listclientes_buscador.ajax.reload();
	}
}




//**************************************************************************************
$("#buscador_cliente_button_filter_list_clientes").on("click", function(e) {
	buscadorClientesReloadTabla();
})

/**********************************************BOT�N FILTRAR*************************************/
function buscadorClientesCargarActividad()
{	
	var categoria = $("#buscador_cliente_selector_categoria").val();
	$("#buscador_cliente_selector_actividad").find("option").remove();
		
	$.ajax({
		type : "post",
		url : "<c:url value='/ajax/select_list_actividades.do'/>?id="+categoria,
		timeout : 100000,
		data : null,
		dataType : 'json',
		success : function(data) {
			if (data.ArrayList.LabelValue!=null && typeof data.ArrayList.LabelValue!="undefined") {
			$.each(data.ArrayList.LabelValue, function(i, item) {
			    $("#buscador_cliente_selector_actividad").append('<option value="'+item.value+'">'+item.label+'</option>');
			});
			}
		}
	});
}

//**************************************************************
$( "#buscador_cliente_selector_categoria" ).change(function() {
	buscadorClientesCargarActividad();
})	

//**************************************************************
$("#buscador_cliente_save_cliente_button").on("click", function(e) {
	
	if (dt_listclientes_buscador==null) {
		$(campoBuscadorClienteDevueltoId).val("");
		$(campoBuscadorClienteDevueltoNombre).val("");
		$("#modal-dialog-form-buscar-cliente").modal('hide');
		return;
	}		
	
	var data=dt_listclientes_buscador.rows( { selected: true } ).data();
		
	if (dt_listclientes_buscador==null || data.length<=0) {
		$(campoBuscadorClienteDevueltoId).val("");
		$(campoBuscadorClienteDevueltoNombre).val("");
		$("#modal-dialog-form-buscar-cliente").modal('hide');
		return;
	}		
	
	var idCliente = dt_listclientes_buscador.rows('.selected').data()[0].idcliente;
	var nombre = dt_listclientes_buscador.rows('.selected').data()[0].nombre;    
	var cif = dt_listclientes_buscador.rows('.selected').data()[0].identificador;    
	var cp = dt_listclientes_buscador.rows('.selected').data()[0].direccionorigen.cp.cp;
	var email = dt_listclientes_buscador.rows('.selected').data()[0].direccionorigen.correoelectronico;
	var telefono = dt_listclientes_buscador.rows('.selected').data()[0].direccionorigen.telefonofijo;

 	var campoBuscadorClienteDevueltoNombre=$("#modal-dialog-form-buscar-cliente input#campoBuscadorClienteDevueltoNombre").val();
 	var campoBuscadorClienteDevueltoId=$("#modal-dialog-form-buscar-cliente input#campoBuscadorClienteDevueltoId").val();
 	var campoBuscadorClienteDevueltoCif=$("#modal-dialog-form-buscar-cliente input#campoBuscadorClienteDevueltoCif").val();        	 	
 	var campoBuscadorClienteDevueltoCp=$("#modal-dialog-form-buscar-cliente input#campoBuscadorClienteDevueltoCp").val();        	 	
 	var campoBuscadorClienteDevueltoEmail=$("#modal-dialog-form-buscar-cliente input#campoBuscadorClienteDevueltoEmail").val();        	 	
 	var campoBuscadorClienteDevueltoTelefono=$("#modal-dialog-form-buscar-cliente input#campoBuscadorClienteDevueltoTelefono").val();        	 	
		
 	$(campoBuscadorClienteDevueltoId).val(idCliente).trigger("change");
	$(campoBuscadorClienteDevueltoNombre).val(nombre).trigger("change");
	$(campoBuscadorClienteDevueltoCif).val(cif).trigger("change");
	$(campoBuscadorClienteDevueltoCp).val(cp).trigger("change");
	$(campoBuscadorClienteDevueltoEmail).val(email).trigger("change");
	$(campoBuscadorClienteDevueltoTelefono).val(telefono).trigger("change");
	$("#modal-dialog-form-buscar-cliente").modal('hide');
});


//********************************************************************************
$("#tab_clientes_new").on("click", function(e) {
	 showButtonSpinner("#tab_clientes_new");
	 $("#modal-dialog-form-editar-cliente .modal-content:first").load("<c:url value='/ajax/facturacion/facturas/clientes/show_cliente.do?tipo=1'/>", function() {
		 
		$("#modal-dialog-form-editar-cliente").modal('show');
		setModalDialogSize("#modal-dialog-form-editar-cliente", "lg");
	});
})




//**************************************************************
$("#tab_cliente_editar").on("click", function(e) {
		
	if (dt_listclientes_buscador==null)
		{
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.productos.tabs.formasdepago.cliente.editar.ninguna_seleccion.editar" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
		}
	var data=dt_listclientes_buscador.rows( { selected: true } ).data();
	
	
	
		
		if (data.length>1) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="administracion.productos.tabs.formasdepago.cliente.editar.seleccion_multiple.editar" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}
		
		
		if (data.length<=0) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="administracion.productos.tabs.formasdepago.cliente.editar.ninguna_seleccion.editar" />',		      
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}
		
		
		var idCliente = dt_listclientes_buscador.rows('.selected').data()[0].idcliente;
		
		showButtonSpinner("#tab_cliente_editar");
		
		$("#modal-dialog-form-editar-cliente .modal-content:first").load("<c:url value='/ajax/facturacion/facturas/clientes/show_cliente.do'/>?tipo=1&id="+idCliente, function() {
			$("#modal-dialog-form-editar-cliente").modal('show');
			setModalDialogSize("#modal-dialog-form-editar-cliente", "md");
		});	
		
		
		
		return;
		
})



	
</script>
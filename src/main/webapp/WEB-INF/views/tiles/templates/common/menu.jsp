<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<script type="text/javascript">
	jQuery(document).ready(function($) {

		var id_organization = "${user.organization.id}";
		
		$("#menu_list_admin_internet").click(function(event) {
			window.location.href = "/frontend/admon/configuracioninternet/configuracion.do";
		});	
		
		$("#menu_list_admin_seguridad").click(function(event) {
			window.location.href = "/frontend/admon/seguridad/seguridad.do";
		});
		
		$("#menu_list_admin_productos").click(function(event) {
			window.location.href = "/frontend/admon/productos/productos.do";
		});	

		$("#menu_list_admin_recintos").click(function(event) {
			window.location.href = "/frontend/admon/recintos/recintos.do";
		});

		$("#menu_list_admin_programacion").click(function(event) {
			window.location.href = "/frontend/admon/programacion/programacion.do";
		});

		$("#menu_list_cajas_supervision").click(function(event) {
			window.location.href = "/frontend/ventareserva/operacionescaja/operacionescaja.do";
		});
		
		$("#menu_list_opcaja_abrir").click(function(event) {
			window.location.href = "/frontend/ventareserva/operacionescaja/abrircaja.do";
		});
		
		$("#menu_list_opcaja_cerrar").click(function(event) {
			window.location.href = "/frontend/ventareserva/operacionescaja/cerrarcaja.do";
		});
			
		$("#menu_list_opcaja_aportacion").click(function(event) {
			window.location.href = "/frontend/ventareserva/operacionescaja/aportacioncaja.do";
		});
		
		$("#menu_list_opcaja_retirada").click(function(event) {
			window.location.href = "/frontend/ventareserva/operacionescaja/retiradacaja.do";
		});

		$("#menu_list_facturacion_facturas").click(function(event) {
			window.location.href = "/frontend/facturacion/facturas/facturas.do";
		});	
		
		$("#menu_list_venta_reserva_busquedas").click(function(event) {
			window.location.href = "/frontend/venta/ventareserva/busquedas.do";
		});			

		$("#menu_list_venta_reserva_operaciones").click(function(event) {
			window.location.href = "/frontend/venta/ventareserva/ventareserva.do";
		});			

		$("#menu_list_venta_numerada_busquedas").click(function(event) {
			window.location.href = "/frontend/venta/numerada/busquedas.do";
		});			

		$("#menu_list_venta_numerada_operaciones").click(function(event) {
			window.location.href = "/frontend/venta/numerada/operaciones.do";
		});			
		
		$("#menu_list_informes_gestion").click(function(event) {
			window.open("/frontend/documentacion/informes/gestion.do","_blank");
		});			

		$("#menu_list_informes_documentacion").click(function(event) {
			window.location.href = "/frontend/documentacion/documentos/gestion.do";
		});			
		
		$("#menu_list_informes_consulta").click(function(event) {
			window.location.href = "/frontend/documentacion/documentos/consulta.do";
		});			
		
		 $("#menu_list_encuestas_mantenimiento").click(function(event) {
			window.location.href = "/frontend/gestionencuestas/encuestas/mantenimientoencuestas.do";
		});
		
		 $("#menu_list_encuestas_responder").click(function(event) {
			window.location.href = "/frontend/gestionencuestas/encuestas/responderencuestas.do";
		}); 	
		
	});
</script>

<div class="navbar nav_title" style="border: 0; background-image:url(<c:url value='/resources/images/logo_colossus_dashboard.png'/>)" >
	<span class="site_title"></span>
</div>

<div class="clearfix"></div>


<c:if test="${user.organization.id>1}">
	<div class="profile">
		<div class="profile_pic">
			<c:choose>
				<c:when test="${empty user.organization.icon}">
					<img src="./resources/images/user.png" class="img-circle profile_img">
				</c:when>
				<c:otherwise>
					<img src="./resources/images/organizations/${user.organization.icon}" class="img-circle profile_img">
				</c:otherwise>
			</c:choose>

		</div>
		<div class="profile_info">
			<span><spring:message code="dashboard.menu.your_organization" /></span> <br /> <a id="organization_data" href="#" class="menuTextOrganizacion">${user.organization.name}</a>
		</div>
	</div>
</c:if>

<c:if test="${not empty(sessionScope.taquilla)}">
<div class="taquilla_info">
<spring:message code="dashboard.taquilla.text" />: ${sessionScope.taquilla}
</div>
</c:if>
<c:if test="${empty(sessionScope.taquilla)}">
<div class="taquilla_info">
<spring:message code="dashboard.sin_taquilla.text" />
</div>
</c:if>

<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
	<div class="menu_section">
		<h3>
			<spring:message code="dashboard.menu.main_section" />	
			
		</h3>
		
		<ul class="nav side-menu">

			<li class="menu_option"><a><i class="fa fa-tasks"></i> <spring:message code="dashboard.menu.administration" /><span class="fa fa-chevron-down"></span></a>
			    <ul class="nav child_menu">
			    	<c:if test="${fn:contains(menu.xml__ArrayList__String, 'mn_adm_op_internet')}">
			    		<li id="admin-internet"><a href="#" id="menu_list_admin_internet"><i class="fa fa-signal"></i><div class="option-text"><spring:message code="dashboard.menu.administration_internet" /></div></a></li>
			    	</c:if>
			    	<c:if test="${fn:contains(menu.xml__ArrayList__String, 'mn_adm_op_recintos')}">
			    		<li id="admin-recintos"><a href="#" id="menu_list_admin_recintos"><i class="fa fa-building-o"></i><div class="option-text"><spring:message code="dashboard.menu.administration_recintos" /></div></a></li>
			    	</c:if>
			    	<c:if test="${fn:contains(menu.xml__ArrayList__String, 'mn_adm_op_seguridad')}">
			    		<li id="admin-seguridad"><a href="<c:url value='/admon/seguridad/seguridad.do'/>" id="menu_list_admin_seguridad"><i class="fa fa-unlock-alt"></i><div class="option-text"><spring:message code="dashboard.menu.administration_seguridad" /></div></a></li>			    		
			    	</c:if>
			    	<c:if test="${fn:contains(menu.xml__ArrayList__String, 'mn_adm_op_productostarifas')}">
			    		<li id="admin-productos"><a href="#" id="menu_list_admin_productos"><i class="fa fa-puzzle-piece"></i><div class="option-text"><spring:message code="dashboard.menu.administration_productos" /></div></a></li>
			    	</c:if>
			    	<c:if test="${fn:contains(menu.xml__ArrayList__String, 'mn_adm_op_programacion')}">
			    		<li id="admin-programacion"><a href="#" id="menu_list_admin_programacion"><i class="fa fa-clock-o"></i><div class="option-text"><spring:message code="dashboard.menu.administration_programacion" /></div></a></li>
			    	</c:if>
			    	
			    	<!--  FALTA: TURNOS ABONO - mn_adm_op_turnosabono -->
			    	
			    </ul>
			</li>
               
			<li class="menu_option"><a><i class="fa fa-bank"></i> <spring:message code="dashboard.menu.cajas" /><span class="fa fa-chevron-down"></span></a>
			    <ul class="nav child_menu">
			    	<c:if test="${fn:contains(menu.xml__ArrayList__String, 'mn_cja_op_caja_sup')}">
			    		<li id="cajas_supervision"><a href="#" id="menu_list_cajas_supervision"><i class="fa fa-thumbs-up"></i><div class="option-text"><spring:message code="dashboard.menu.cajas_supervision" /></div></a></li>
			    	</c:if>
			    </ul>
			</li>

			<li class="menu_option"><a><i class="fa fa-money"></i> <spring:message code="dashboard.menu.opcaja" /><span class="fa fa-chevron-down"></span></a>
			    <ul class="nav child_menu">
			    	<c:if test="${fn:contains(menu.xml__ArrayList__String, 'mn_ope') or fn:contains(menu.xml__ArrayList__String, 'mn_cja_op_caja_sup')}">
			    		<li id="opcaja_abrir"><a href="#" id="menu_list_opcaja_abrir"><i class="fa fa-inbox"></i><div class="option-text"><spring:message code="dashboard.menu.opcaja_abrir" /></div></a></li>
			    	</c:if>
			    	<c:if test="${fn:contains(menu.xml__ArrayList__String, 'mn_ope') or fn:contains(menu.xml__ArrayList__String, 'mn_cja_op_caja_sup')}">
			    		<li id="opcaja_cerrar"><a href="#" id="menu_list_opcaja_cerrar"><i class="fa fa-archive"></i><div class="option-text"><spring:message code="dashboard.menu.opcaja_cerrar" /></div></a></li>
			    	</c:if>
			    	<c:if test="${fn:contains(menu.xml__ArrayList__String, 'mn_ope') or fn:contains(menu.xml__ArrayList__String, 'mn_cja_op_caja_sup')}">
			    		<li id="opcaja_aportacion"><a href="#" id="menu_list_opcaja_aportacion"><i class="fa fa-plus-circle"></i><div class="option-text"><spring:message code="dashboard.menu.opcaja_aportacion" /></div></a></li>
			    	</c:if>
			    	<c:if test="${fn:contains(menu.xml__ArrayList__String, 'mn_ope') or fn:contains(menu.xml__ArrayList__String, 'mn_cja_op_caja_sup')}">
			    		<li id="opcaja_retirada"><a href="#" id="menu_list_opcaja_retirada"><i class="fa fa-minus-circle"></i><div class="option-text"><spring:message code="dashboard.menu.opcaja_retirada" /></div></a></li>
			    	</c:if>
			    </ul>
			</li>

			<li class="menu_option"><a><i class="fa fa-cart-plus"></i> <spring:message code="dashboard.menu.venta" /><span class="fa fa-chevron-down"></span></a>
			    <ul class="nav child_menu">
			    	<c:if test="${fn:contains(menu.xml__ArrayList__String, 'mn_ven_op_venta')}">
			    		<li id="venta_reserva_operaciones"><a href="#" id="menu_list_venta_reserva_operaciones"><i class="fa fa-tag"></i><div class="option-text"><spring:message code="dashboard.menu.operaciones_venta_reserva" /></div></a></li>
			    		<li id="venta_reserva_busquedas"><a href="#" id="menu_list_venta_reserva_busquedas"><i class="fa fa-bookmark"></i><div class="option-text"><spring:message code="dashboard.menu.busquedas_venta_reserva" /></div></a></li>
			    	</c:if>
			    	<c:if test="${fn:contains(menu.xml__ArrayList__String, 'mn_ven_op_ventanumerada')}">
			    		<li id="venta_numerada_operaciones"><a href="#" id="menu_list_venta_numerada_operaciones"><i class="fa fa-ticket"></i><div class="option-text"><spring:message code="dashboard.menu.operaciones_numerada" /></div></a></li>
			    		<li id="venta_numerada_busquedas"><a href="#" id="menu_list_venta_numerada_busquedas"><i class="fa fa-bookmark-o"></i><div class="option-text"><spring:message code="dashboard.menu.busquedas_numerada" /></div></a></li>
			    	</c:if>
			    	<!--  
			    	<c:if test="${fn:contains(menu.xml__ArrayList__String, 'mn_con')}">
			    		<li id="venta_cobros"><a href="#" id="menu_list_venta_cobros"><i class="fa fa-exchange"></i><div class="option-text"><spring:message code="dashboard.menu.venta_cobros" /></div></a></li>
			    	</c:if>
			    	-->
			    </ul>
			</li>
			
			<!--  FALTA: MEN� PALAU - mn_pal_op_ventapalau_ven -->

			<li class="menu_option"><a><i class="fa fa-eur"></i> <spring:message code="dashboard.menu.facturacion" /><span class="fa fa-chevron-down"></span></a>
			    <ul class="nav child_menu">
			    	<c:if test="${fn:contains(menu.xml__ArrayList__String, 'mn_fac_op_fac_faccli')}">
			    		<li id="facturacion_facturas"><a href="#" id="menu_list_facturacion_facturas"><i class="fa fa-file"></i><div class="option-text"><spring:message code="dashboard.menu.facturacion_facturas" /></div></a></li>
			    	</c:if>
			    </ul>
			</li>

			<li class="menu_option"><a><i class="fa fa-files-o"></i> <spring:message code="dashboard.menu.informes" /><span class="fa fa-chevron-down"></span></a>
			    <ul class="nav child_menu">
			    	<c:if test="true"> <!-- SIEMPRE ACTIVO ${fn:contains(menu.xml__ArrayList__String, 'mn_inf_op_informes_ges')}"> -->
			    		<li id="informes_gestion"><a href="#" id="menu_list_informes_gestion"><i class="fa fa-file-text-o"></i><div class="option-text"><spring:message code="dashboard.menu.informes_gestion" /></div></a></li>
			    	</c:if>
			    	<c:if test="${fn:contains(menu.xml__ArrayList__String, 'mn_inf_op_documentacion_ges')}">
			    		<li id="informes_documentacion"><a href="#" id="menu_list_informes_documentacion"><i class="fa fa-book"></i><div class="option-text"><spring:message code="dashboard.menu.informes_documentacion" /></div></a></li>
			    	</c:if>
			    	<c:if test="${fn:contains(menu.xml__ArrayList__String, 'mn_inf_op_documentacion_con')}">
			    		<li id="informes_consulta"><a href="#" id="menu_list_informes_consulta"><i class="fa fa-eye"></i><div class="option-text"><spring:message code="dashboard.menu.informes_consulta" /></div></a></li>
			    	</c:if>
			    </ul>
			</li>

			<li class="menu_option"><a><i class="fa fa-binoculars"></i> <spring:message code="dashboard.menu.encuestas" /><span class="fa fa-chevron-down"></span></a>
			    <ul class="nav child_menu">
			    	<c:if test="true"> <!-- SIEMPRE ACTIVO "${fn:contains(menu.xml__ArrayList__String, 'mn_enc_mant_enc')}"> -->
			    		<li id="encuestas_mantenimiento"><a href="#" id="menu_list_encuestas_mantenimiento"><i class="fa fa-gear"></i><div class="option-text"><spring:message code="dashboard.menu.encuestas_mantenimiento" /></div></a></li>
			    	</c:if>
			    	<c:if test="true"> <!-- SIEMPRE ACTIVO "${fn:contains(menu.xml__ArrayList__String, 'mn_enc_responder_enc')}"> -->
			    		<li id="encuestas_responder"><a href="#" id="menu_list_encuestas_responder"><i class="fa fa-comment"></i><div class="option-text"><spring:message code="dashboard.menu.encuestas_responder" /></div></a></li>
			    	</c:if>
			    </ul>
			</li>
			
			<c:if test="${user.role==1}">

				<li class="menu_option" id="admin_options"><a><i class="fa fa-folder"></i>&nbsp;<spring:message code="dashboard.menu.administration" /><span class="fa fa-chevron-down"></span></a>
					<ul class="nav child_menu">

						<c:if test="${user.organization.id==1}">
							<li id="admin_organizations"><a href="#" id="menu_list_organizations"><i class="fa fa-users"></i><div class="option-text"><spring:message code="dashboard.menu.organizations" /></div></a></li>
							<li id="stadistics"><a href="#" id="menu_list_stadistics"><i class="fa fa-bar-chart"></i><div class="option-text"><spring:message code="dashboard.menu.stadistics" /></div></a></li>
							<li id="admin_products"><a href="#" id="menu_list_products"><i class="fa fa-cutlery"></i><div class="option-text"><spring:message code="dashboard.menu.products" /></div></a></li>
						</c:if>

						<li id="admin_requests"><a href="#" id="menu_list_requests"><i class="fa fa-hand-o-up"></i><div class="option-text"> <spring:message code="dashboard.menu.request" /></div></a></li>

						<li><a href="#" id="menu_list_users"><i class="fa fa-user"></i><div class="option-text"><spring:message code="dashboard.menu.users" /></div></a></li>

						<li><a href="#" id="menu_list_workcenters"><i class="fa fa-industry"></i><div class="option-text"><spring:message code="dashboard.menu.workcenter" /></div></a></li>
					</ul></li>

			</c:if>

		</ul>
	</div>
</div>


<!-- /sidebar menu -->

<!-- /menu footer buttons -->
<!--
<div class="sidebar-footer hidden-small">
	<a data-toggle="tooltip" data-placement="top" title="Settings"> <span
		class="glyphicon glyphicon-cog" aria-hidden="true"></span>
	</a> <a data-toggle="tooltip" data-placement="top" title="FullScreen">
		<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
	</a> <a data-toggle="tooltip" data-placement="top" title="Lock"> <span
		class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
	</a> <a data-toggle="tooltip" data-placement="top" title="Logout"> <span
		class="glyphicon glyphicon-off" aria-hidden="true"></span>
	</a>
</div>
-->
<!-- /menu footer buttons -->


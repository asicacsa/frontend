importPackage(Packages.org.springframework.web.servlet);
var forward = 'obtenerListadoComun.action';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('obtenerListadoComun.js');
function handle(request, response) {
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : Llamada a obtenerListadoTaquillasSinRecintoFiltrado.action "+this.getClass().getName());	
	}
	//request = comun.setServicio(request);
	// en este caso, el nombre de servicio lo establecemos fijo, y 
	// no en funcion del nombre del dataset, ya que varios datasets
	// atacan a este action.
	request.setAttribute('servicio','obtenerListadoTaquillasSinRecintoFiltrado');
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : to this service : "+request+'SOAWrapper'+' : '+this.getClass().getName());
		log.info("LOG(INFO) : forward to "+forward);
	}
	var modelAndView = new ModelAndView(forward);
	return modelAndView;
}
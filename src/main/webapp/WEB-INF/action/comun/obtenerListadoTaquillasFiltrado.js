importPackage(Packages.org.springframework.web.servlet);
var forward = 'obtenerListadoComun.action';

function handle(request, response) {
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('obtenerListadoComun.js');
	request = comun.setServicio(request);
	var modelAndView = new ModelAndView(forward);
	return modelAndView;
}

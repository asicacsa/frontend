/** Funciones comunes de Match para references **/
meta.addMethod(
	'totalMatch',  // param1: nombre de la funcion
	'String,String', // param2: string con los tipos de los parametros separados por coma
	'String' // param3: string con el tipo de retorno
);
function totalMatch(xmlparam,xrefen) {
	//log.info("----Antes de tratarlo : "+xmlparam.toXMLString());
	if (xmlparam != null) {
		var xmlparam = new XML(xmlparam);
		var aux_references = this.searchReferences(xmlparam,xrefen);
		var aux_dict = {};// Esta variable es la que usaremos para guardar las referencias guardadas.
		for (i=0;i<aux_references.length();i++) {
			var aux_attribute = aux_references[i];
			//log.info('El valor de reference : '+i+" "+aux_attribute);
			if (typeof aux_dict[aux_attribute] == 'undefined') {
				//log.info("Antes del For : ");
				var tamanyo = eval('xmlparam..*.('+xrefen+' == '+aux_attribute+').length()');
				//log.info("----TAMANYO: " + tamanyo);
				var toAsign = this.returnIdReference(xmlparam,aux_attribute);
				for (x=0;x<tamanyo;x++) {
					eval('xmlparam..*.('+xrefen+' == '+aux_attribute+')['+x+'].setChildren(toAsign.children())');
				//log.info("El valor que asignamos : "+x+" "+xmlparam..*.(@reference == aux_attribute)[x]);
				}
				aux_dict[aux_attribute]=true;
				continue;
			}
		}
	}
	deleteReferencesAndId(xmlparam,xrefen);
	return xmlparam.toXMLString();
}
function deleteReferencesAndId(xmlparam,refen) {
	eval('delete xmlparam..'+refen);
	if (log.isInfoEnabled()) {
		log.info(xmlparam);
	}
	delete xmlparam..@id;
	return xmlparam;
}
function searchReferences(xmlparam,refen) {

	//log.info('Este es el refer que utilizaremos : '+refen);
	return eval('xmlparam..'+refen);
}
function returnIdReference(xmlparam,referenceparam) {
	return xmlparam..*.(@id == referenceparam);
}

meta.addMethod(
	'deleteTrash',  // param1: nombre de la funcion
	'String', // param2: string con los tipos de los parametros separados por coma
	'String' // param3: string con el tipo de retorno
);
/** Elimina la basura de los Set **/
function deleteTrash(elem) {
	if (elem != null) {
		elem= new XML(elem);
		delete elem..initialized;
		delete elem..owner;
		delete elem..cachedSize;
		delete elem..role;
		delete elem..key;
		delete elem..dirty;
		delete elem..callbacks;
		delete elem..hibernateLazyInitializer;
	}
	return elem.toXMLString();
}


meta.addMethod(
	'borraElementosSinHijos', 
	'String',
	'Object'
);

function borraElementosSinHijos(parametro){
	//primero obtenemos todos los elementos en una lista
	var lista;
	var nombre = "";
	var salir = false;
	var xml = new XML( parametro );
	
	while (!salir) {
		var lista = xml.descendants();
		salir=true;
		for each(var posible in lista) {
			if (posible.nodeKind()=='element') {
				if (posible.children().length()==0) {
					delete posible.parent().*[posible.childIndex()]
					//var nombre = rutaCompletaANodo(posible);
					//eval("delete xml." + nombre );
					salir=false;
				}
			}
		}
	}
	return xml;
}

function rutaCompletaANodo(nodo) {
	var nombre= "";
	while (nodo.parent()!=null) {
		if (nombre.length>0) {
			nombre = "." + nombre;
		}
		nombre = "*[" + nodo.childIndex() + "]" + nombre;
		nodo=nodo.parent();
	}
	return nombre;
}

meta.addMethod(
	'deleteSelectedTag', 
	'String',
	'String'
);

function eliminarElementosNull(xmlparam) {
	var lista;
	var nombre = "";
	var salir = false;
	var xml = new XML( xmlparam );
	
	while (!salir) {
		var lista = xml.descendants();
		salir=true;
		for each(var posible in lista) {
			if (posible.nodeKind()=='element') {
				if (posible.text()=='null') {
					delete posible.parent().*[posible.childIndex()]
					//var nombre = rutaCompletaANodo(posible);
					//eval("delete xml." + nombre );
					salir=false;
				}
			}
		}
	}
	return xml;
}



function deleteSelectedTag(xmlparam) {
	if (xmlparam != null) {
		var aux = new XML(xmlparam);
		delete aux..selected;
		var xmlparam = aux.toXMLString();
	}
	return xmlparam;
}


function sustituyeLiteralesBooleanosEnVenta(xmlSinHijos) {
	//API 29 Diciembre 2010 Remedio de emergencia pues anularVenta.js no se llama desde donde debiera.
	if (log.isInfoEnabled()) {
		log.info( "\n\nEl xml que llega a sustituyeLiteralesBooleanosEnVenta es\n" + xmlSinHijos);
	}
	if ( xmlSinHijos.name()=='venta' || xmlSinHijos.name()=='Venta' ){
		xmlSinHijos = sustituyeBooleanosXML( xmlSinHijos, false );
	}
	else {
		var lista = xmlSinHijos.descendants();
		for each( var posible in lista ) {
			if ( posible.name()=='venta' || posible.name()=='Venta' ) {
				xmlSinHijos = sustituyeBooleanosXML( xmlSinHijos, true );
				break;
			}
		}
	}
	if (log.isInfoEnabled()) {
		log.info( "\n\nEl xml que va a devolver sustituyeLiteralesBooleanosEnVenta es\n" + xmlSinHijos);
	}
	return xmlSinHijos;
}


function sustituyeBooleanosXML( xmlparam, hijo ) {
	//Sustitucion de trues y falses en financiada, entradasimpresas, reciboimpreso e imprimirSoloEntradasXDefecto.
	if ( hijo == true ) {
		if (xmlparam.venta.financiada.text() == 'true') {
			xmlparam.venta.financiada = <financiada>1</financiada>;
		} else if (xmlparam.venta.financiada.text() == 'false'){
			xmlparam.venta.financiada = <financiada>0</financiada>;
		}
		if (xmlparam.venta.entradasimpresas.text() == 'true') {
			xmlparam.venta.entradasimpresas = <entradasimpresas>1</entradasimpresas>;
		} else if (xmlparam.venta.entradasimpresas.text() == 'false'){
			xmlparam.venta.entradasimpresas = <entradasimpresas>0</entradasimpresas>;
		}
		if (xmlparam.venta.reciboimpreso.text() == 'true') {
			xmlparam.venta.reciboimpreso = <reciboimpreso>1</reciboimpreso>;
		} else if (xmlparam.venta.reciboimpreso.text() == 'false'){
			xmlparam.venta.reciboimpreso = <reciboimpreso>0</reciboimpreso>;
		}
		if (xmlparam.venta.imprimirSoloEntradasXDefecto.text() == 'true') {
			xmlparam.venta.imprimirSoloEntradasXDefecto = <imprimirSoloEntradasXDefecto>1</imprimirSoloEntradasXDefecto>;
		} else if (xmlparam.venta.imprimirSoloEntradasXDefecto.text() == 'false'){
			xmlparam.venta.imprimirSoloEntradasXDefecto = <imprimirSoloEntradasXDefecto>0</imprimirSoloEntradasXDefecto>;
		}
	}
	else {
		if (xmlparam.financiada.text() == 'true') {
			xmlparam.financiada = <financiada>1</financiada>;
		} else if (xmlparam.financiada.text() == 'false'){
			xmlparam.financiada = <financiada>0</financiada>;
		}
		if (xmlparam.entradasimpresas.text() == 'true') {
			xmlparam.entradasimpresas = <entradasimpresas>1</entradasimpresas>;
		} else if (xmlparam.entradasimpresas.text() == 'false'){
			xmlparam.entradasimpresas = <entradasimpresas>0</entradasimpresas>;
		}
		if (xmlparam.reciboimpreso.text() == 'true') {
			xmlparam.reciboimpreso = <reciboimpreso>1</reciboimpreso>;
		} else if (xmlparam.reciboimpreso.text() == 'false'){
			xmlparam.reciboimpreso = <reciboimpreso>0</reciboimpreso>;
		}
		if (xmlparam.imprimirSoloEntradasXDefecto.text() == 'true') {
			xmlparam.imprimirSoloEntradasXDefecto = <imprimirSoloEntradasXDefecto>1</imprimirSoloEntradasXDefecto>;
		} else if (xmlparam.imprimirSoloEntradasXDefecto.text() == 'false'){
			xmlparam.imprimirSoloEntradasXDefecto = <imprimirSoloEntradasXDefecto>0</imprimirSoloEntradasXDefecto>;
		}
	}
	return xmlparam;
}
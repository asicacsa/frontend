importClass(java.util.concurrent.locks.ReentrantLock);
var services = context.beans.getBean('httpServiceSOA');
this.lock = new ReentrantLock();

function handle(request, response) {
	try {
		this.lock.lock();
		this._handle(request, response);
	} finally {
		this.lock.unlock();
	}	
}

function _handle(request, response) {

	var level = request.getParameter('level');
	var msg = request.getParameter('msg');
	
	if (level.toLowerCase()=="debug" && log.isDebugEnabled()) {
		log.debug("lzxLogger " + msg);
	}
	
	if (level.toLowerCase()=="info" && log.isInfoEnabled()) {
		log.info("lzxLogger " + msg);
	}
	
	if (level.toLowerCase()=="warn" && log.isWarnEnabled()) {
		log.warn("lzxLogger " + msg);
	}
	
	if (level.toLowerCase()=="error" && log.isErrorEnabled()) {
		log.error("lzxLogger " + msg);
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');	
	response.writer.println('<ok/>');
}

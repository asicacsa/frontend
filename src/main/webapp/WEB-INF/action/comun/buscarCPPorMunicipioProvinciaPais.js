var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
		
	if (log.isInfoEnabled()) {
		log.info('Entrada a buscarCPPorMunicipioProvinciaPais.');
	}
	
	var methodpost = "buscarCPPorMunicipioProvinciaPais";
	var nombre = request.getParameter('nombrecp');
	if(nombre == null)
		nombre='';
	var idpais = request.getParameter('idPais');
	if(idpais == null)
		idpais = '';
	
	var idprovincia = request.getParameter('idProvincia');
	if(idprovincia == null)
		idprovincia = '';
	
	var idmunicipio = request.getParameter('idMunicipio');
	//log.info('------------------ idmunicipio'+idmunicipio);
	if(idmunicipio == null)
		idmunicipio = '';
		
	var envio = <servicio>
					<parametro>
						<BuscarMunicipioParam>
							<nombrecp>{nombre}</nombrecp>
							<idpais>{idpais}</idpais>
							<idprovincia>{idprovincia}</idprovincia>
							<idmunicipio>{idmunicipio}</idmunicipio>
						</BuscarMunicipioParam>
					</parametro>
				</servicio >;
				
	envio = comun.borraElementosSinHijos(envio);
	//No tratamos el xml de entrada...
	if (log.isInfoEnabled()) {
		log.info("XML de envio del buscarCPPorMunicipioProvinciaPais: " + envio.toXMLString());
	}
	var respuesta;
	
	try{
		respuesta = services.process(methodpost+'SOAWrapper',envio.toXMLString());
	} catch (ex) {
 		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	 }
	
	//log.info('------------- respuesta='+respuesta);
	if (log.isInfoEnabled()) {
		log.info("respuesta del buscarCPPorMunicipioProvinciaPais \r\n " + respuesta);
	}
	
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	
	response.writer.println(respuesta);
}
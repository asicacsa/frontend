importPackage(Packages.org.springframework.web.servlet);
var forward = 'obtenerComun.action';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('obtenerComun.js');
function handle(request, response) {
	if (log.isInfoEnabled()) {
		log.info("Entrada a obtenerListadoTiposProducto.");
	}
	request = comun.setServicio(request);
	var modelAndView = new ModelAndView(forward);
	return modelAndView;
}

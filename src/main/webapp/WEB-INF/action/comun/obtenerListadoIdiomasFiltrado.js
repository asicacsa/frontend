importPackage(Packages.org.springframework.web.servlet);
var forward = 'obtenerListadoComun.action';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('obtenerListadoComun.js');

function handle(request, response) {
	request = comun.setServicio(request);
	var modelAndView = new ModelAndView(forward);
	return modelAndView;
}

var services = context.beans.getBean('httpServiceSOA');
var servicio_OLTPP = "obtenerListadoTarifasPorProducto";
var servicio_OTPPP = "obtenerTarifasProductoPorProducto";

function handle(request, response) {

	if (log.isInfoEnabled()) {
		log.info("CONTEXT is a special variable >>> "+context);
	}

	var xml = request.getParameter('xml');

	if(xml == null ) {
		var envio = '<servicio />';
	} else {
		var envio = 
			<servicio>
				<parametro>
					<idProducto>{xml}</idProducto>
				</parametro>
			</servicio>
	}
	//Obtenemos el producto, o su id???
	//No tratamos el xml de entrada...

	//print("The input XML : "+xml);
	//log.info('The input XML : '+xml);	

	var respuesta = new XML(services.process(servicio_OLTPP+'SOAWrapper',envio));

	//log.info('The response of first service XML : '+respuesta);

	if (respuesta != null) {
		// Aki posiblemente deberiamos lanzar una excepcion, ya que si no existen Tarifas para nuestro Producto
		// o evitar la segunda llamada, ya que al no existir Tarifas, ES IMPOSIBLE QUE EXISTAN TARIFAS_PRODUCTOS!!!,
		// bueno es lo que vamos a hacer...
		var llamada_servicio2 = 
			<servicio>
				<parametro>
					<idProducto>{xml}</idProducto>
				</parametro>
				<parametro>
					<java.util.List>{respuesta.tarifa}</java.util.List>
				</parametro>
			</servicio>
			
		// En respuesta debemos obtener las tarifas que pertenecen a este producto...

		var respuesta_tarifas_productos = new XML(services.process(servicio_OTPPP+'SOAWrapper',llamada_servicio2.toXMLString()));

		// En respuesta_tarifas_productos debemos obtener una lista de listas de tarifas_productos...
		
		var x=0;
		var hoy = new java.util.Date();
		var date_Format = new java.text.SimpleDateFormat('dd/MM/yyyy');
		for each (i in respuesta.tarifa) {
			//for each(u in i.tarifasProducto.tarifaProducto) {
				//u.@selected=false;
			//}
			//log.info("Metodo GET : "+respuesta_tarifas_productos.list[x]);

			//Para los combo....
			i.@selected=false;

			i.tarifasProducto=respuesta_tarifas_productos.list[x];

			// Tratamos las tarifas productos, para seleccionar la adecuada,
			// y agregarles el @selected=false para los combo...
			//var pos_tarifa_Producto = 0;
			//var count = 0;
			var importe_aux=0;
			for each (v in i.list.tarifaProducto) {
				v.@selected=false;
				//log.info("Fecha de hoy en miliseconds : "+hoy.getTime());
				var dat_inicio = date_Format.parse(v.fechaInicioVigencia.toString());
				var dat_fin = date_Format.parse(v.fechaFinVigencia.toString());
				//log.info("Fecha de tarifaProducto en miliseconds : "+((java.util.Date)date_Format.parse(v.fechaInicioVigencia.toString())).getTime());			
				//log.info("Fecha de tarifaProducto en miliseconds : "+((java.util.Date)date_Format.parse(v.fechaFinVigencia.toString())).getTime());			
				if (hoy.getTime() > dat_inicio.getTime() && hoy.getTime() < dat_fin.getTime()) {
					importe_aux=v.importe;
				}
				//count++;
			}
		
			//i.tarifaProductoActual=i.list.tarifaProducto[pos_tarifa_Producto];
			i.importe=importe_aux;
			x++;
		}
	} else {
		//Aki una repuesta vacia, no hay tarifas para el producto!!!
		respuesta= <list/>
	}

	//log.info('Resultado de montar la llamada a servicio 2 : '+llamada_servicio2);
	//print("The response after the transformation : "+llamada_servicio2);

	//log.info("The Xml of response: " + respuesta_tarifas_productos);

	//log.info("The Xml of response after the transformation : " + respuesta);
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
}
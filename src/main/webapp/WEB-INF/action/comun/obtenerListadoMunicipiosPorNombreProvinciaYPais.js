var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
		
	if (log.isInfoEnabled()) {
		log.info('Entrada a obtenerListadoMunicipiosPorNombreProvinciaYPais.');
	}
	
	var methodpost = "obtenerListadoMunicipiosPorNombreProvinciaYPais";
	var nombre = request.getParameter('nombre');
	if(nombre == null || nombre == 'null'){
		nombre='';
	}

	var idpais = request.getParameter('idPais');
	if(idpais == null || idpais == 'null')
		idpais = '';
	var idprovincia = request.getParameter('idProvincia');

	if(idprovincia == null || idprovincia == 'null')
		idprovincia = '';
	
	var envio = <servicio>
					<parametro>
						<BuscarMunicipioParam>
							<nombremunicipio>{nombre}</nombremunicipio>
							<idpais>{idpais}</idpais>
							<idprovincia>{idprovincia}</idprovincia>
						</BuscarMunicipioParam>
					</parametro>
				</servicio >;
				
	envio = comun.borraElementosSinHijos(envio);
	//No tratamos el xml de entrada...
	if (log.isInfoEnabled()) {
		log.info("XML de envio del obtenerListadoMunicipiosPorNombreProvinciaYPais: " + envio.toXMLString());
	}
	var respuesta;
	
	try{
		respuesta = services.process(methodpost+'SOAWrapper',envio.toXMLString());
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	if (log.isInfoEnabled()) {
		log.info("respuesta del obtenerListadoMunicipiosPorNombreProvinciaYPais \r\n " + respuesta);
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta);
}
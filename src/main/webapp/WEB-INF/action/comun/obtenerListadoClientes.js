var services = context.beans.getBean('httpServiceSOA');
//var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {

	//print("CONTEXT is a special variable >>> "+context);	
	
	var methodpost = request.getParameter('servicio');
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	var envio = '<servicio />';
	
	//No tratamos el xml de entrada...
	var respuesta;
	
	try{
		respuesta = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
 		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	 }
	 
	
	if (log.isInfoEnabled()) {
		log.info("Este es el xml antes de la transformacion : "+new XML(respuesta).toXMLString());
	}
	if (respuesta != null) {
		respuesta = this.postProcessXML(xml,respuesta);
	}
	if (log.isInfoEnabled()) {
		log.info("Este es el xml despues de la transformacion : "+new XML(respuesta).toXMLString());
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
	
}
/**
???Cuidado!!! Un XMLObject nunca es nulo.
classXML : 
			<labelValue>
				<label/>
				<value/>
		   </labelValue>
Los parametros no deben ser XML o en el caso de serlo deben tener valor.
Retorna el valor del parametro xmlresponse como un XML con el esquema expuesto antes.
xmlparam debe ser nulo o en el caso de no serlo ser el valor de idRecinto a seleccionar.			
**/
function postProcessXML(xmlparam,xmlresponse) {
	if (xmlresponse != null) {
		xmlresponse = new XML(xmlresponse);
		if (xmlparam != null) {
			for each (i in xmlresponse.cliente) {
				//print("El valor de idcliente : "+i.idcliente );
				if ( i.idcliente == xmlparam) {
					i.selected=true;
				} else {
					i.selected=false;
				}
				i.setName('labelValue');
				i.idcliente.setName('value');
				i.nombre.setName('label');				
			}
		} else {
			var x =0;
			for each (i in xmlresponse.cliente ) {
				if (x==1) {
					i.selected=true;
				} else {
					i.selected=false;
				}
				i.setName('labelValue');
				i.idcliente.setName('value');
				i.nombre.setName('label');
				x++;
			}
		}
	}
	return xmlresponse;
}
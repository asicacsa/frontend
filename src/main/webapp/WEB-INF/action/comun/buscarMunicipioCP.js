var services = context.beans.getBean('httpServiceSOA');
//var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {
	
	if (log.isInfoEnabled()) {
		log.info('Entrada a buscarMunicipioCP.');
	}
	
	var methodpost = "buscarMunicipioCP";
	var texto = request.getParameter('texto');
	var idpais = request.getParameter('idpais');
	
	var envio = <servicio>
					<parametro>
						<int>{idpais}</int>
						<texto>{texto}</texto>
					</parametro>
				</servicio >;
	
	//No tratamos el xml de entrada...
	if (log.isInfoEnabled()) {
		log.info("XML de envio del buscarMunicipioCP: " + envio.toXMLString());
	}
	
	var respuesta;
	
	try{
		respuesta = services.process(methodpost+'SOAWrapper',envio.toXMLString());
	} catch (ex) {
 		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	 }
	
	if (log.isInfoEnabled()) {
		log.info("respuesta del buscarMunicipioCP \r\n " + respuesta);
	}
	
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	
	response.writer.println(respuesta);
}
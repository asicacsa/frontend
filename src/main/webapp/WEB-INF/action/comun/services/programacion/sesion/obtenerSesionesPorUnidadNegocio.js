var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {	
	
	var methodpost;
	if (methodpost == null) {
		methodpost = request.getParameter('servicio');
	}		
	if (methodpost == null) {
		methodpost = request.getParameter('__request_service');
	}
	// si xml no se envia, llama al metodo sin parametros	
	var selecciona = null;
	
	enviar_id = request.getParameter('xml');
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : Call to obtenerSesionesPorUnidadNegocio : "+this.getClass().getName());
	}
	
	// En este tipo de servicio no van a haber parametros de Entrada.
	var envio = <servicio>
					<parametro>
						<int>{enviar_id}</int>
					</parametro>
				</servicio>;

	if (log.isInfoEnabled()) {
		log.info("Metodo a llamar : "+methodpost);
		log.info("Envio : "+envio);
	}
	
	var result;
	
	try{
		result = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
	 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	 }	
	
	if (log.isInfoEnabled()) {
		log.info(result);
	}
	
	if (result !=  null) {
		result = new XML(result);
		result = this.postProcessXML(result);		
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());	
}

function postProcessXML(param) {
	//log.info("Estamos postProcesando : ");
	for each (i in param.Sesion) {
		//log.info("Entramos en el for : "+i)
		var aux = i.fecha.text();
		aux = aux.substr(0,aux.indexOf('-'));
		i.fecha = <fecha>{aux}</fecha>;
	}	
	return param;
//Modificaremos la fecha y hora de inicio venta. 
}

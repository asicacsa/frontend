var services = context.beans.getBean('httpServiceSOA');
var methodpost = 'obtenerSesionesPorUnidadNegocioYMesYAnyo';
function handle(request, response) {
	
	var mes = request.getParameter('mes');
	var anyo = request.getParameter('anyo');
	var idunidadnegocio = request.getParameter('idUnidadNegocio');
	
	if (mes == null || anyo == null || idunidadnegocio == null) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result does not mes, anyo o idunidadnegocio : '+ex);
		}
		var ex = new java.lang.Exception('Exception calling this service : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		throw ex;
	}
	
	var envio = <servicio>
					<parametro>
						<int>{idunidadnegocio}</int>
						<int>{mes}</int>
						<int>{anyo}</int>						
					</parametro>
				</servicio>;

	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : with this parameters : "+envio+' : '+this.getClass().getName());
	}
	var aux;
	if (log.isErrorEnabled()) {
		try {
			aux = services.process(methodpost+'SOAWrapper',envio);				
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
	} else {
		aux = services.process(methodpost+'SOAWrapper',envio);				
	}
	
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : resultado de la llamada : "+aux);
	}
	
	if (aux !=  null) {
		aux = new XML(aux);
		aux = this.postProcessXML(aux,anyo,mes,idunidadnegocio);		
	}
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : resultado de la llamada despues de la trasnformacion : "+aux);
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(aux.toXMLString());   
}

function postProcessXML(result,anyo,mes,idunidadnegocio) {
	var month = <month{mes}/>				
	if (result!=null) {
		result= new XML(result);
		var day;
/**
	XML de referencia : 
		<dayXXX>
			<event/>
		</dayXXX>
**/
		var day_flag;
		for each (i in result.Sesion) {
			if (day == null) {
				var aux = i.fecha.text();
				day_flag = aux.substr(0,aux.indexOf('/'));
				if (day_flag.substr(0,1) == '0') {
					day_flag=day_flag.substr(1,1);
				}
				day = <day{day_flag}>							
						</day{day_flag}>
				var aux_idsesion = i.idsesion.text();
				var aux_horinicio_hora = i.horainicio.text().substr(0,i.horainicio.text().indexOf(':'));
				var aux_horinicio_min = i.horainicio.text().substr(i.horainicio.text().indexOf(':')+1,2);				

				var aux_horfin_hora = i.horafin.text().substr(0,i.horafin.text().indexOf(':'));
				var aux_horfin_min = i.horafin.text().substr(i.horafin.text().indexOf(':')+1,2);				
				var event = <event idsesion={aux_idsesion} idUnidadNegocio={idunidadnegocio}>
								<summary value={i.contenido.nombre}/>
								<comment value=""/>
								<start year={anyo} month={mes} day={day_flag} hour={aux_horinicio_hora} minute={aux_horinicio_min}/>
								<end year={anyo} month={mes} day={day_flag} hour={aux_horfin_hora} minute={aux_horfin_min}/>
								<category value="milestone"/>
								<uid value={aux_idsesion}/>
							</event>
				day.appendChild(event);
			} else {
				var aux = i.fecha.text();
				var aux_day = aux.substr(0,aux.indexOf('/'));
				if (aux_day.substr(0,1) == '0') {
					aux_day=aux_day.substr(1,1);
				}
				if (aux_day != day_flag) {
					month.appendChild(day);
					day_flag = aux_day;
					day = <day{day_flag}/>
				}
				var aux_idsesion = i.idsesion.text();
				var aux_horinicio_hora = i.horainicio.text().substr(0,i.horainicio.text().indexOf(':'));
				var aux_horinicio_min = i.horainicio.text().substr(i.horainicio.text().indexOf(':')+1,2);				

				var aux_horfin_hora = i.horafin.text().substr(0,i.horafin.text().indexOf(':'));
				var aux_horfin_min = i.horafin.text().substr(i.horafin.text().indexOf(':')+1,2);				
				var event= <event idsesion={aux_idsesion} idUnidadNegocio={idunidadnegocio}>
								<summary value={i.contenido.nombre}/>
								<comment value=""/>
								<start year={anyo} month={mes} day={day_flag} hour={aux_horinicio_hora} minute={aux_horinicio_min}/>
								<end year={anyo} month={mes} day={day_flag} hour={aux_horfin_hora} minute={aux_horfin_min}/>
								<category value="milestone"/>
								<uid value={aux_idsesion}/>
							</event>
				day.appendChild(event);				
			}
		}
		if (day!=null) {
			month.appendChild(day);	
		}
		
	}	
	var resultado = <vcalendar>
						<year{anyo}>																						
							{month}
						</year{anyo}>
					</vcalendar>
	
	return resultado;	
}
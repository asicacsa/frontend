var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');
function handle(request, response) {
	
	var methodpost = request.getAttribute('servicio');
	if (methodpost == null) {
		methodpost = request.getParameter('servicio');
	}
	if (methodpost == null) {
		methodpost = request.getParameter('__request_service');
	}
	// si xml no se envia, llama al metodo sin parametros
	var selecciona = null;
	
	selecciona = request.getParameter('xml');
	if (log.isInfoEnabled()) {
		log.info("obtenerComun: Id para seleccionar " + selecciona);
	}
	
	// En este tipo de servicio no van a haber parametros de Entrada.
	var envio = '<servicio />';

	if (log.isInfoEnabled()) {
		log.info("Metodo a llamar : "+methodpost);
		log.info("Envio : "+envio);
	}
	
	var result = services.process(methodpost+'SOAWrapper',envio);
	
	if (log.isInfoEnabled()) {
		log.info(result);
	}
	
	result= result.trim();
	
	if (result !=  null) {
		result = new XML(result);
		//result = this.postProcessXML(result, selecciona);
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
	
}
function postProcessXML(xmlparam, selecciona) {
	var aux_item = <LabelValue>
				<value></value>
				<label></label>
				<selected>false</selected>
				</LabelValue>
	var aux_param;
	if (xmlparam != null) {
		xmlparam = new XML(xmlparam);

		if (selecciona!=null && selecciona != '') {
			xmlparam.LabelValue.(value==selecciona).selected=true;
		} else {
			aux_item.selected=true;
		}
		xmlparam.prependChild(aux_item);
	}		
	return xmlparam;
}

/** Funciones comun de Asignacion del servicio a llamar **/
meta.addMethod(
	'setServicio', // param1: nombre de la funcion
	'javax.servlet.ServletRequest', // param2: ServletRequest
	'javax.servlet.ServletRequest' // param3: ServletRequest
);
function setServicio(request) {

	var methodPost = request.getParameter('servicio');
	if (methodPost == null) {
		methodPost = request.getAttribute('__request_service');	
	}
	request.setAttribute('servicio',methodPost);
	return request;
}
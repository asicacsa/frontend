var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {	
	
	var methodpost = request.getAttribute('servicio');		
	if (methodpost == null) {
		methodpost = request.getParameter('servicio');
	}
	var id_aux = request.getParameter('xml');
	
		
	var envio;
	if (id_aux != null) {
		envio = <servicio>
					<parametro><int>{id_aux}</int></parametro>
				</servicio>
	} else {
		envio = <servicio>
					<parametro/>
				</servicio>
	}
	//log.info("Metodo a llamar : "+methodpost);
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : Call from obtenerListado."+this.getClass().getName());
		log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());
		log.info("LOG(INFO) : with this parameters : "+envio+' : '+this.getClass().getName());
	}
	//var result= resultado_comun;
	var result;
	if (log.isErrorEnabled()) {
		try {
			result = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
	} else {
		result = services.process(methodpost+'SOAWrapper',envio);
	}
	
	if (log.isInfoEnabled()) {
		log.info('LOG(INFO) : Sucess in the call from : '+this.getClass().getName());
		log.info('LOG(INFO) : result in : '+result);
	}
				

	//No tratamos el xml de entrada...
	
	//print("The input XML : "+xml);
	
	//log.info(result);
	
	if (result !=  null) {
		result = new XML(result);
		//Post procesamos con selected...
		log.info('LOG(INFO) : Result after the transformation : '+this.getClass().getName());
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
	
}

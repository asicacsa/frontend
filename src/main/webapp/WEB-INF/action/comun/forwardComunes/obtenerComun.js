var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');
function handle(request, response) {	
	
	var methodpost = request.getAttribute('servicio');
	if (methodpost == null) {
		methodpost = request.getParameter('servicio');
	}		
	if (methodpost == null) {
		methodpost = request.getParameter('__request_service');
	}
	// si xml no se envia, llama al metodo sin parametros	
	var selecciona = null;
	
	selecciona = request.getParameter('xml');
	if (log.isInfoEnabled()) {
		log.info("obtenerComun: Id para seleccionar " + selecciona);
	}
	
	// En este tipo de servicio no van a haber parametros de Entrada.
	var envio = '<servicio />';

	if (log.isInfoEnabled()) {
		log.info("Metodo a llamar : "+methodpost);
		log.info("Envio : "+envio);
	}
	
	var result = services.process(methodpost+'SOAWrapper',envio);
	
	//modificamos esta linea de log porque puede ser pesada.
	if (log.isInfoEnabled()) {
		log.info('Respuesta del servicio: ' + result.substr(0,50) + '...');
	}
	
	if (result !=  null) {
		result = new XML(result);
		result = this.postProcessXML(result, selecciona);		
	} else {
		result = new XML();
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
	
}
function postProcessXML(xmlparam, selecciona) {
	if (xmlparam != null) {
		xmlparam = new XML(xmlparam);	
		if (selecciona!=null && selecciona != '') {
			if (xmlparam.LabelValue.length() != 0) {
				//Si encontramos el elemento seleccionado
				if (xmlparam.LabelValue.(value==selecciona).length() != 0) {
					xmlparam.LabelValue.(value==selecciona).selected=true;
				}
			}
		}
	}
	
	return xmlparam;
}

/** Funciones comun de Asignacion del servicio a llamar **/
meta.addMethod(
	'setServicio', // param1: nombre de la funcion
	'javax.servlet.ServletRequest', // param2: ServletRequest
	'javax.servlet.ServletRequest' // param3: ServletRequest
);
function setServicio(request) {
	var methodPost = request.getParameter('servicio');
	if (methodPost == null) {
		methodPost = request.getAttribute('__request_service');	
	}
	request.setAttribute('servicio',methodPost);
	return request;
}
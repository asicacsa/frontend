var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	var xml = request.getParameter('xml');
	
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var resultado;
		
	if (xml!=null) {
		
		var envio = <servicio>
						<parametro>
							<int>
								{xml}
							</int>
						</parametro>
					</servicio>
		
		if (log.isInfoEnabled()) {		
			log.info("Este es el xml que enviamos para el get: "+envio.toXMLString());
		}
		var aux = services.process(methodpost+'SOAWrapper',envio);					
		if (log.isInfoEnabled()) {
			log.info('Este es el valor que retorna : '+new XML(aux).toXMLString())
		}
		if (aux != null) {
			resultado = new XML(aux);
		}		
	} else {
	
		modelstring = request.getAttribute('__modelo');

		var envio = <servicio>
						<parametro>
							<{modelstring}/>							
						</parametro>
					</servicio>
		
		if (log.isInfoEnabled()) {
			log.info("Este es el xml que enviamos para el get modelo: "+envio.toXMLString());		
		}
		resultado = services.process('getXMLModelSOAWrapper',envio);
		if (log.isInfoEnabled()) {
			log.info('Devuelto el modelo  : '+new XML(resultado).toXMLString())
		}
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado);
}

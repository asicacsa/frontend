var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {	
	
	var methodpost = request.getAttribute('servicio');		
	if (methodpost == null) {
		methodpost = request.getParameter('servicio');
	}
	// si xml no se envia, llama al metodo sin parametros
	// Parametros posibles a usar en lso listados.
	// selected : Nodos que deben aparecer con un elemento selected=true
	// filter : Id que deben ser filtrados.	
	var selected = request.getParameter('selected');
	// Forma del XML de selected 
	/**
	*	<envio>
	*		<parametro id="0">id[0]</parametro>
	*		...
	*		<parametro id="i">id[i]</parametro> 
	*	</envio>
	*
	**/		
	var filter = request.getParameter('filter');
	if (log.isInfoEnabled()) {
		log.info("Esto es el filtro : "+filter);
	}
	filter = this.postProcessFilter(filter);
	if (log.isInfoEnabled()) {
		log.info("Despues de la trasnformacion : "+filter);
	}
	var combo = request.getParameter("combo");
	
	// Forma del XML de filter 
	/**
	*	<envio>
	*		<parametro id="0">id[0]</parametro>
	*		...
	*		<parametro id="i">id[i]</parametro>
	*	</envio>
	*
	**/	
	// En este tipo de servicio no van a haber parametros de Entrada.
	
	if (log.isInfoEnabled()) {
		log.info("el parametro que nos viene en el combo: " + combo);
	}
	
	var envio;	
	if(combo == null || combo == 'undefined' ) {
		envio = <servicio>
					<parametro>
						{filter}						
					</parametro>
				</servicio>
	} else {
		envio = <servicio>
					<parametro>
						{filter}
						<int>{combo}</int>
					</parametro>
				</servicio>;
	}
	//log.info("Metodo a llamar : "+methodpost);
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : Call from obtenerListadoComun."+this.getClass().getName());
		log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());
		log.info("LOG(INFO) : with this parameters : "+envio+' : '+this.getClass().getName());
	}
	//var result= resultado_comun;
	var result;
	if (log.isErrorEnabled()) {
		try {
			result = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
	} else {
		result = services.process(methodpost+'SOAWrapper',envio);
	}
	
	if (log.isInfoEnabled()) {
		log.info('LOG(INFO) : Sucess in the call from : '+this.getClass().getName());
		log.info('LOG(INFO) : result in : '+result);
	}
				

	//No tratamos el xml de entrada...
	
	//print("The input XML : "+xml);
	
	//log.info(result);
	
	result= result.trim();
	
	if (result !=  null) {
		result = new XML(result);
		//Post procesamos con selected...
		result = this.postProcessXML(result,selected);
		if (log.isInfoEnabled()) {
			log.info('LOG(INFO) : Result after the transformation : '+this.getClass().getName());
		}
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
	
}
//Function para transformar los Filter de Laszlo a Services...
function postProcessFilter(xmlparam) {
	var result = <list />;
	if (xmlparam != null) {
		xmlparam = new XML(xmlparam);	
		
		for each (i in xmlparam..parametro) {
			var child = i.text();
			result.appendChild(<int>{child}</int>);
			if (log.isInfoEnabled()) {
				log.info("Result :"+result+' mas el texto : '+i.text());
			}
		}
	}
	return result;
}
//Function para transformar la salida.
function postProcessXML(xmlparam,xml) {
	//var aux = comun.deleteTrash(xmlparam.toXMLString());
	//aux = comun.totalMatch(aux,'@reference');
	if (xml != null) {
		xml = new XML(xml);
		
		if (xml..*.text() != -1) {
			for each(i in xmlparam.LabelValue) {			
				if (eval('xml..*.text() == '+i.value)) {
					i.selected = true;
				}
			}
		}
	} 
	return xmlparam;
}

/** Funciones comun de Asignacion del servicio a llamar **/
meta.addMethod(
	'setServicio',  // param1: nombre de la funcion
	'javax.servlet.ServletRequest',		// param2: ServletRequest
	'javax.servlet.ServletRequest'		 // param3: ServletRequest
);
function setServicio(request) {
	var methodPost = request.getParameter('servicio');
	if (methodPost == null) {
		methodPost = request.getAttribute('__request_service');	
	}
	request.setAttribute('servicio',methodPost);
	return request;
}
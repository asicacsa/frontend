var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}

	// si xml no se envia, llama al metodo sin parametros	
	var xml = request.getParameter('xml');
	var envio;
	if (log.isInfoEnabled()) {
		log.info('Este es el xml que nos llega para dar de Baja : '+xml);
	}
	if(xml == null ) {
		envio = '<servicio />';
	} else {
		xml = new XML(xml);
		envio = 
			<servicio>
				<parametro>
					{xml}	
				</parametro>
			</servicio>
	}
	if (log.isInfoEnabled()) {
		log.info('Lo que le pasamos al servicio : '+envio);
	}
	services.process(methodpost+'SOAWrapper',envio);
	
	var result = <OK/>
	//log.info("Esto es lo que vamos a enviar : ");
	response.writer.println(result.toXMLString());
}
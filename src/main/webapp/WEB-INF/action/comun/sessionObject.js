function handle(request, response) {
	
	var resultado = <sessionObject>
						<nombreApellidos></nombreApellidos>
						<nombre></nombre>
						<apellidos></apellidos>
						<id></id>
						<idUsuario></idUsuario>
						<taquilla></taquilla>
						<idtaquilla></idtaquilla>
						<canal>
							<idcanal></idcanal>
							<nombre></nombre>
							<tipocanal>
								<idtipocanal/>
								<nombre/>
							</tipocanal>
						</canal>
						<login></login>
					</sessionObject>;
	
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : Entrada en el action de peticion de Datos del usuario.");
	}			
	
	var session = request.getSession(false);
	
	if (session != null) {
		//Aki crearemos el XML que le devolveremos a laszlo.
		var nombreApellidos;
		var nombre = session.getAttribute("nombre");		
		var apellidos = session.getAttribute("apellidos");
		
		//log.info("Accedemos a los nombres y los apellidos : ");
		if (nombre != null && nombre.length() > 0) {
			resultado.nombre.appendChild(nombre);
			nombreApellidos=nombre;
		} else {
			//resultado.nombre.appendChild('');
			nombreApellidos='';
		}
		
		//log.info("El tama??o de los apellidos : "+apellidos.length());
		//log.info("El typeof apellidos : "+typeof(apellidos));
		
		if (apellidos != null && apellidos.length() > 0) {			
			resultado.apellidos.appendChild(apellidos);
			if (nombreApellidos.length() > 0 ) {
				//log.info("por el if");
				//nombreApellidos.concat(' ');
				//nombreApellidos.concat(apellidos);
				nombreApellidos=nombreApellidos+" "+apellidos;
			} else {
				//log.info("por el else : "+nombreApellidos.length);			
				nombreApellidos=apellidos;
			}
		}
		//log.info("El typeof de nombreApellidos : "+typeof(nombreApellidos));
		
		//log.info("Intentando asignar los nombreApellidos : "+nombreApellidos+" el length : "+nombreApellidos.length);
		
		if (nombreApellidos != null && nombreApellidos.length > 0) {
			resultado.nombreApellidos.appendChild(nombreApellidos);
		}
		
		//log.info("Despues de asignar los nombreApellidos : ");
		
		var id = session.getId();
		//donde se ha visto una session sin id, :P
		resultado.id.appendChild(id);
		
		var idUsuario= session.getAttribute("idUsuario");
		
		//log.info("Intentando asignar los idUsuario : ");
		
		if (idUsuario != null && idUsuario.length() > 0) {
			resultado.idUsuario.appendChild(idUsuario);
		}
				
		var taquilla= session.getAttribute("taquilla");
		
		//log.info("Intentando asignar la taquilla : ");
		
		if (taquilla != null && taquilla.length() > 0) {
			resultado.taquilla.appendChild(taquilla);
		}
		
		var idtaquilla= session.getAttribute("idtaquilla");
		
		//log.info("Intentando asignar idtaquilla : ");
		
		if (idtaquilla != null && idtaquilla.length() > 0) {
			resultado.idtaquilla.appendChild(idtaquilla);
		}
		
		var idcanal= session.getAttribute("idcanal");
		
		//log.info("Intentando asignar la idcanal : ");
		
		if (idcanal != null && idcanal.length() > 0) {
			resultado.canal.idcanal.appendChild(idcanal);
		}
		
		var nombreCanal = session.getAttribute("nombreCanal");
		
		if (nombreCanal != null && nombreCanal.length() > 0) {
			resultado.canal.nombre.appendChild(nombreCanal);
		}
		
		var nombreTipoCanal = session.getAttribute("nombreTipocanal");
		
		if (nombreTipoCanal != null && nombreTipoCanal.length() > 0) {
			resultado.canal.tipocanal.nombre.appendChild(nombreTipoCanal);
		}
		
		var idTipoCanal = session.getAttribute("idTipocanal");
		
		if (idTipoCanal != null && idTipoCanal.length() > 0) {
			resultado.canal.tipocanal.idtipocanal.appendChild(idTipoCanal);
		}
		
		var login = session.getAttribute("login");
		
		if (login != null && login.length() > 0) {
			resultado.login.appendChild(login);
		}
		
		session=null;
	}
		
	if (log.isInfoEnabled()) {
		log.info("  ## Resultado Objeto sesion: " + resultado.toXMLString() );
	}

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
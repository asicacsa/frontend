//var service = context.beans.getBean('httpServiceSOA');
var services = context.beans.getBean('httpServiceSOA');

//var service = context.beans.getBean('httpServiceSOA');
var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {

	//log.info("Entrada a obtenerListadoZonasDeRecintoPorSesion: ");
	
	var methodpost = request.getParameter('servicio');
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	var envio = new XML();
	
	var zonaactual = request.getParameter('zonaactual');
	
	if (log.isInfoEnabled()) {
		log.info("Este es el xml de entrada (obtenerListadoZonasDeRecintoPorSesion): " + xml);
	}
	
	if(xml == null ) {
		envio = <servicio/>;
	} else {
		xml = new XML(xml);
		
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>	
	}
	
	//No tratamos el xml de entrada...
	
	if (log.isInfoEnabled()) {
		log.info("Este es el xml a enviar al servicio (obtenerListadoZonasDeRecintoPorSesion) : " + xml.toXMLString());
	}
	
	var respuesta = services.process(methodpost+'SOAWrapper',envio.toXMLString());
	//var respuesta = service.process(methodpost,envio);
	
	respuesta= respuesta.trim(); // AVC. 09-11-2017. El valor que deveulve es erróneo si no se hace trim.
	
	if (log.isInfoEnabled()) {
		log.info("Este es el xml de respuesta antes del postprocesado (obtenerListadoZonasDeRecintoPorSesion): " + respuesta)
	}
	
	if (respuesta != null) {
		respuesta = this.postProcessXML(zonaactual,respuesta);
	}
	
	if (log.isInfoEnabled()) {
		log.info("Este es el xml de respuesta despues del postprocesado (obtenerListadoZonasDeRecintoPorSesion): "+ respuesta )
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
	
}
/**
???Cuidado!!! Un XMLObject nunca es nulo.
classXML : 
			<labelValue>
				<label/>
				<value/>
		   </labelValue>
Los parametros no deben ser XML o en el caso de serlo deben tener valor.
Retorna el valor del parametro xmlresponse como un XML con el esquema expuesto antes.
xmlparam debe ser nulo o en el caso de no serlo ser el valor de idRecinto a seleccionar.			
**/
function postProcessXML(xmlparam,xmlresponse) {
	if (xmlresponse != null) {
		xmlresponse = new XML(xmlresponse);
		if (xmlparam != null) {
			for each (i in xmlresponse.LabelValue) {
				if ( i.value == xmlparam ) {
					i.selected=true;
				} else {
					i.selected=false;
				}												
			}
		} 
	}
	return xmlresponse;
}
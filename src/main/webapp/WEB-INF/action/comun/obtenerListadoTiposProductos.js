var service = context.beans.getBean('httpServiceSOA');
//var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {

	//print("CONTEXT is a special variable >>> "+context);	
	
	var methodpost = request.getParameter('servicio');
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		var envio = '<servicio />';
	}
	
	//No tratamos el xml de entrada...
	
	//print("The input XML : "+xml);
	
	//var result = services.process(methodpost+'SOAWrapper',envio);
	var respuesta = new XML(service.process(methodpost,envio));
	
	//print("The Xml of response: " + respuesta);
	
	for each (i in respuesta.tipoProducto) {
		i.@selected=false;
	}
	
	//print("The xml of response after transform : "+ respuesta);
response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
	
}
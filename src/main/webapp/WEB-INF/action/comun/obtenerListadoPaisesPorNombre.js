var services = context.beans.getBean('httpServiceSOA');
//var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	if (log.isInfoEnabled()) {
		log.info('Entrada a obtenerListadoPaisesPorNombre');
	}
	
	var methodpost = "obtenerListadoPaisesPorNombre";
	var nombre = request.getParameter('nombre');
	var envio = <servicio>
					<parametro>
						<nombre>{nombre}</nombre>
					</parametro>
				</servicio >;
	
	envio = comun.borraElementosSinHijos(envio);
	//No tratamos el xml de entrada...
	if (log.isInfoEnabled()) {
		log.info("XML de envio del obtenerListadoPaisesPorNombre: " + envio.toXMLString());
	}
	var respuesta;
	
	try {
		respuesta = services.process(methodpost+'SOAWrapper',envio.toXMLString());
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
		
	if (log.isInfoEnabled()) {
		log.info("respuesta del obtenerListadoPaisesPorNombre \r\n " + respuesta);
	}
	//if (respuesta!=null) {
	//
	//	log.info("respuesta no es nula");
	//	respuestaxml = new XML(respuesta);
	//	
	//	procesar = new XML();		
	//
	//	respuestaxml.setName("list");
	//	
	//	for each (var cli in respuestaxml.Cliente) {
	//		cli.nombrecompleto = cli.nombre.text() + ' ' + cli.apellido1.text() + ' ' + cli.apellido2.text();			
	//	}
	//	
	//	procesar = respuestaxml;
	//	
	//	log.info(procesar);
	//	
	//	log.info("resultado despues de transformar:\r\n" + procesar.toXMLString());
	//}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	
	response.writer.println(respuesta);
}
//var service = context.beans.getBean('httpServiceSOA');
var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {

	//print("CONTEXT is a special variable >>> "+context);	
	
	var methodpost = request.getParameter('servicio');
	var listaIdsZonas = request.getParameter('listaIdsZonas');
	listaIdsZonas = new XML (listaIdsZonas);
	var envio;
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		envio = <servicio />;
	} else {
		envio = <servicio />;
	}
	
	//No tratamos el xml de entrada...
	
	//print("The input XML : "+xml);
	
	var respuesta;
	
	try{
		respuesta = services.process(methodpost+'SOAWrapper',envio.toXMLString());
	} catch (ex) {
 		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	 }
	
	//var respuesta = service.process(methodpost,envio);
	
	if (log.isInfoEnabled()) {
		log.info("Este es el xml antes de la transformacion : "+new XML(respuesta).toXMLString());
	}
	if (respuesta != null) {
		respuesta = this.postProcessXML(respuesta, listaIdsZonas);
	}
	
	if (log.isInfoEnabled()) {
		log.info("Este es el xml despues de la transformacion : "+new XML(respuesta).toXMLString());
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
	
}

function postProcessXML(xmlresponse, listaIdsZonas) {
	if (xmlresponse != null) {
		xmlresponse = new XML(xmlresponse);
 
 		//Si no hay ninguna zona definida para internet, simplemente le ponemos a todas las zonas el atributo recinto_zona
 		if(listaIdsZonas.idzona.toString() == ""){
			var sub_recinto_zona = "";
			var recinto_zona = "";
			var nom_subrecinto = "";
			var nom_recinto = "";
			for each (recinto in xmlresponse.Recinto){
				nom_recinto = recinto.nombre.text();
				//Si buscamos en las zonas del recinto
				for each (zona in recinto.zonas.Zona){
					//Establezco el nombre del recinto asociado a cada zona
					recinto_zona = nom_recinto + " - " + zona.nombre.text();
					zona.recinto_zona = <recinto_zona>{recinto_zona}</recinto_zona>
					//Buscamos en las zonas de los subrecintos
					for each (subrecinto in recinto.Recinto){
						nom_subrecinto = subrecinto.nombre.text();
						for each (subzona in subrecinto.zonas.Zona){
							sub_recinto_zona = nom_subrecinto + " - " + subzona.nombre.text();
							subzona.recinto_zona = <recinto_zona>{sub_recinto_zona}</recinto_zona>
						}
					}
				}
			}
		}
		else{
			//Busco las zonas que ya está seleccionadas y las elimino para que no se muestren
			for each(zonaEliminar in listaIdsZonas.idzona){
				for each (recinto in xmlresponse.Recinto){
					nom_recinto = recinto.nombre.text();
					//Si buscamos en las zonas del recinto
					for ( var i = 0; i < recinto.zonas.Zona.length(); i++){
						if(recinto.zonas.Zona[i].idzona.text() == zonaEliminar){
							delete recinto.zonas.Zona[i];
							i--;
						}
						else{
							//Establezco el nombre del recinto asociado a cada zona
							recinto_zona = nom_recinto + " - " + recinto.zonas.Zona[i].nombre.text();
							recinto.zonas.Zona[i].recinto_zona = <recinto_zona>{recinto_zona}</recinto_zona>
						}
						
						//Buscamos en las zonas de los subrecintos
						for each (subrecinto in recinto.Recinto){
							nom_subrecinto = subrecinto.nombre.text();
							for ( var j = 0; j < subrecinto.zonas.Zona.length(); j++){
								if(subrecinto.zonas.Zona[j].idzona.text() == zonaEliminar){
									delete subrecinto.zonas.Zona[j];
									j--;
								}
								else{
									sub_recinto_zona = nom_subrecinto + " - " + subrecinto.zonas.Zona[j].nombre.text();
									subrecinto.zonas.Zona[j].recinto_zona = <recinto_zona>{sub_recinto_zona}</recinto_zona>
								} //else
							}// for j
						}//for each subrecinto
					}// for i
				} //for each recinto
			} //for each listIdsZonas
		}
	}
	return xmlresponse;
}
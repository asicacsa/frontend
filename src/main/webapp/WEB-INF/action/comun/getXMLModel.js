var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');	
		
function handle(request, response) {
		
	//log.info("LOG(INFO) : Entrada a getXMLModel.");
	
	//Aceptaremos como parametro un xml de entrada, por ahora no haremos nada con el.
	var xml =  request.getParameter('xml');
	//var methodpost = request.getParameter('service');
	var methodpost = 'getXMLModel';
	var resultado;
	
	
	if (xml != null){
		xml = new XML(xml);
		if (log.isInfoEnabled()) {
			log.info("Serializacion de xml que llega a getXMLModel: " + xml.toXMLString());
		}
		
		var envio = <servicio>
						<parametro>
							<{xml}/>
						</parametro>
					</servicio>;
		
		try {
			//log.info("Este es el metodo al que vamos a llamar : "+methodpost);
			if (log.isInfoEnabled()) {
				log.info('Este es el metodo al que vamos a llamar : ' + methodpost + " con envio=" + envio);
			}
			var resultado = services.process(methodpost+'SOAWrapper',envio);
			if (log.isInfoEnabled()) {
				log.info('Esto es lo que devuelve el servicio getXMLModel: ' + resultado);
			}
			resultado = new XML(resultado);
		} catch (ex) {
			if (log.isErrorEnabled()) {
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost);
				log.error("LOG(ERROR) : con esta llamada: " + envio);
				log.error('LOG(ERROR) : con este tipo de error : ' + ex);
				log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
			}
			throw ex.javaException;
		}
	} else {
		resultado = <error>XML nulo!</error>;
	}
	
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : result : "+resultado+' : '+this.getClass().getName());		
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
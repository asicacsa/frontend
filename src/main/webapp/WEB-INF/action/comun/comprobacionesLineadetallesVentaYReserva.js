var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');	
		
function handle(request, response) {
	//Aceptaremos como parametro un xml de entrada, por ahora no haremos nada con el.
	var xml =  request.getParameter('xml');
	
	if (xml != null){
		xml = new XML(xml);
		
		var sErrores = "";
		
		//Comprobamos si la venta esta facturada
		if (xml.facturada.text() == 1) {
			/*[SSVG - 08/03/2011 - Fase 5 - FA-15] Se comenta esta comprobacion a la se hace referencia en ventareserva.lzx 
			 * a traves de las windowModalDialog modalConfirmacionModificarVentaFacturada 
			 * par que no tenga efecto sobre ventas facturadas*/
			//sErrores += "No puede modificar una venta que ha sido facturada <br/>";
		}
		
		//Comprobamos si la venta esta anulada
		if (xml.estadooperacion.idestadooperacion.text() == 4) {
			sErrores += "No puede modificar una venta que ha sido anulada <br/>";
		}
		
		//Comprobaciones en los bonos
		var tbAnterior = "";
		var numLD = 1;
		
		for each (var ld in xml.lineadetalles.Lineadetalle) {			
			//Comprobamos que la línea de detalle tiene seleccionado el perfil de visitante
			if (ld.perfilvisitante.idperfilvisitante.text().length() == 0) {
				sErrores += "Debe de indicar el perfil de la linea de detalle numero " + numLD + "<br/>";
			}
			
			//Comprobamos que el importe tenga un valor numérico
			if (isNaN(ld.tarifaproducto.importe.text()) || ld.tarifaproducto.importe.text().toString() == '') {
				sErrores += "El importe unitario de la linea de detalle numero " + numLD + " no es correcto <br/>";
			}
			
			//Comprobamos que el importe tenga un valor numérico
			if (isNaN(ld.importe.text()) || ld.importe.text().toString() == '') {
				sErrores += "El importe de la linea de detalle numero " + numLD + " no es correcto <br/>";
			}
			
			numLD ++;
		}
		
		//Comprobamos que no exista ningún bono facturado
		var numBonosFacturados = xml..Bono.(facturado.text() == 1).length();
		if (numBonosFacturados != 0) {
			if (numBonosFacturados == 1) {
				sErrores += "Hay " + numBonosFacturados + " bono que esta facturado y por tanto no puede modificar la venta.<br/>";
			} else {
				sErrores += "Hay " + numBonosFacturados + " bonos que estan facturados y por tanto no puede modificar la venta.<br/>";
			}
		}
		
		
		if (sErrores.length > 0) {
			throw new java.lang.Exception(sErrores);
		}
	} else {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : ' + this.getClass().getName());
		}
		throw new java.lang.Exception("Debe indicar un XML para poder comprobar si las líneas de detalle tienen los datos correctos");
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println("<ok />");
}
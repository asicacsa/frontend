var services = context.beans.getBean('httpServiceSOA');
//var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	if (log.isInfoEnabled()) {
		log.info('Entrada a obtenerListadoProvinciasPorNombreYPais');
	}
	
	var methodpost = "obtenerListadoProvinciasPorNombreYPais";
	var nombre = request.getParameter('nombre');
	var idPais = request.getParameter('idPais');
	
	var envio = <servicio>
					<parametro>
						<nombre>{nombre}</nombre>
						<int>{idPais}</int>
					</parametro>
				</servicio >;
				
	envio = comun.borraElementosSinHijos(envio);
	
	//No tratamos el xml de entrada...
	if (log.isInfoEnabled()) {
		log.info("obtenerListadoProvinciasPorNombreYPais, envio: " + envio.toXMLString());
	}
	
	var respuesta;
	
	try {
		respuesta = services.process(methodpost+'SOAWrapper',envio.toXMLString());
	} catch (ex) {
		if (log.isErrorEnabled()) {	
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : con esta llamada : ' + envio);
			log.error('LOG(ERROR) : result in this Exception : '+ex);
		}
		
		throw ex.javaException;
	}
	
	if (log.isInfoEnabled()) {
		log.info("obtenerListadoProvinciasPorNombreYPais, respuesta: " + respuesta);
	}
	
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta);
}
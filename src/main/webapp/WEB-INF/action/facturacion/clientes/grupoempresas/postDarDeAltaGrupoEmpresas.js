var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaGrupoEmpresas';
var servicio_edicion ='actualizarGrupoEmpresas';


function handle(request, response) {

	var xml = request.getParameter('xml');		
	
	var resultado;
	
	var methodpost;
	
	var modoEdicion = request.getParameter('_modo');
	
	var envio = new XML();
	
	if (xml!=null) {
		xml = new XML(xml);
		
		if (log.isInfoEnabled()) {
			log.info("XML original "  +  xml);
		}
		
		xml = xml.children();
		

		if (modoEdicion=="alta") {
			methodpost = servicio_alta;
			envio = 
				<servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>
			
		} else {
			methodpost = servicio_edicion;
			envio = 
				<servicio>
					<parametro>
						<list>
							{xml}
						</list>
					</parametro>
				</servicio>				
		}
		
		if (log.isInfoEnabled()) {
			log.info("Este es el envio antes de preprocesado: "+envio);
		}
		
		envio = preProcessXML(envio);	
		
		if (log.isInfoEnabled()) {
			log.info("Este es el envio despues de preprocesado: "+envio);
		}
		
		if(envio == "" || envio == null){
			var ex = new java.lang.Exception('el nombre del grupo de empresas es requerido');
			throw ex;
		}
		
		var aux;
		try{
			aux = services.process(methodpost+'SOAWrapper',envio.toXMLString());
		} catch (ex) {
			if (log.isErrorEnabled()) {	
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				log.error('LOG(ERROR) : con esta llamada : ' + envio);
				log.error('LOG(ERROR) : result in this Exception : '+ex);
			}
			
			throw ex.javaException;
		}
		
		
		resultado = 
				<ok>
					{aux}
				</ok>
				
	} else {
		if (log.isInfoEnabled()) {
			log.info("No hay dato en el post: ");
		}
		resultado = <error/>
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {

	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('transformerXML.js');
		
	delete param..selected; //Eliminamos todos los tags selected
	
	delete param..nombrecompleto;
		
	for each (cliente in param..Cliente){
		cliente.setName('cliente');
	}
	
	param = comun.borraElementosSinHijos(param); //Eliminamos todos los elementos que se han quedado sin hijos
		

	return param;
	
}
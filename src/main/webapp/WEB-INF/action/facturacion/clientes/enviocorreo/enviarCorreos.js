var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	var resultado;
	var methodpost = 'enviarCorreos';
	var envio;
		
	var xml = request.getParameter('xml');
	var idmodelo = request.getParameter('idmodelo');
	
	xml = new XML(xml);
	
	if( xml != null && idmodelo != null){

		envio = 
			<servicio>
				<parametro>
					{xml}		
					<int>{idmodelo}</int>
				</parametro>
			</servicio>
		
	}
	
	//Y llamamos al servicio...
	var aux;
	
	try{
		aux = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	

	if(aux != null){
		resultado = new XML(aux);
		if (log.isInfoEnabled()) {
			log.info('## es el resultado de enviarCorreos '+resultado);	
		}
	} else {
		resultado = new XML();
		if (log.isInfoEnabled()) {
			log.info("El resultado es null.");
		}
		throw new java.lang.Exception("Error.")
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
	
}

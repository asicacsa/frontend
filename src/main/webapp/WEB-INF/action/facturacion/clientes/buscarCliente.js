var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');
	

function handle(request, response) {

	if (log.isInfoEnabled()){
		log.info("entrada a buscarCliente.action");
	}
	
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}

	// si xml no se envia, llama al metodo sin parametros	
	var parametro = request.getParameter('xml');
	var xml;
		 	
	if(parametro == null ) {
		xml = '<servicio><list/></servicio>';
	} else {
		//Se realiza con concatenacion de cadenas porque sino mete bloques CDATA
		// obtengo el js externo
		xml = comun.borraElementosSinHijos(parametro);		
	}
	
	xml = new XML(xml);
	xml = this.preProcessXML(xml);
	
	if (log.isDebugEnabled()) {
		log.debug("buscarCliente.action, envio:" + xml.toXMLString());
	}
	
	var result;
	
	try{
		result = services.process(methodpost+'SOAWrapper', "<servicio><parametro>" + xml.toXMLString()+ "</parametro></servicio>");
	} catch (ex) {
	 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	 }	
	 
	if (result != null) 
	{
		result = new XML(result);	
		result = this.postProcessXML(result);
	}
	
	
	if (log.isDebugEnabled()) {
		log.debug("buscarCliente.action, resultado servicio:" + result);
	}
	
	result = comun.deleteTrash(result);
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result);
}


function preProcessXML(param) 
{
	
	if(param.principal.text() == "true" || param.principal.text()=="1")
	{
		param.principal = <principal>1</principal>;
	} 
	else 
	{
		delete param.principal;
	}
		
	return param;
}

function postProcessXML(xmlparam)
{
	for each (cliente in xmlparam.Cliente)
	{
		if (cliente.facturable.text() == "2")
		{
			delete cliente.facturable;
			
			cliente.facturable = <facturable>1</facturable>;
			cliente.facturable_electronico = <facturable_electronico>1</facturable_electronico>;
		}
		else 
		{
			cliente.facturable_electronico = <facturable_electronico>0</facturable_electronico>;
		}
	}
	return xmlparam;
}



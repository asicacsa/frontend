// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	//log.info("Entrada a darDeBajaClientes.action");
	
	var methodpost = request.getParameter('servicio');
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	var motivo = request.getParameter('motivo');
	var envio = <servicio/>;
	
	if (xml != null && motivo != null){
		envio.appendChild(<parametro/>);
		envio.parametro.appendChild(<BloqueoBajaClienteParam/>);
		
		if (xml != null){
			xml = new XML(xml);
			envio.parametro.BloqueoBajaClienteParam.appendChild(xml);
		}
		
		if (motivo != null){
			motivo = new XML(motivo);
			envio.parametro.BloqueoBajaClienteParam.appendChild(motivo);
		}
	}
	
	if (log.isInfoEnabled()) {
		log.info("Envio al servicio darDeBajaClientes: " + envio.toXMLString());
	}
	var result;
	
	try{
		result = services.process(methodpost+'SOAWrapper',envio.toXMLString());
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
		
	if (log.isInfoEnabled()) {
		log.info("Resultado del servicio darDeBajaClientes: " + result);
	}
	
	if(result != '<null/>'){
		result = new XML(result);
	} else {
		result = new XML('<ok/>');
	}
	//var respuesta = new XML(service.process(methodpost,envio));
	//var respuesta = <ok/>;
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
}
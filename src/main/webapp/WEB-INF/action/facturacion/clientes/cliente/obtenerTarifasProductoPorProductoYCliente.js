// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {

	//log.info("Entrada a obtenerTarifasProductoPorProductoYCliente.");	
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	var methodpost = request.getParameter('servicio');
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var idproducto = request.getParameter('idproducto');
	var idcliente = request.getParameter('idcliente');
	
	if (log.isInfoEnabled()) {
		log.info("Valor de parametros idproducto, idcliente, servicio: " + idproducto +", "+ idcliente + ", " + methodpost);
	}
	
	var envio = new XML();
	if(idproducto == null  || idcliente == null) {
		envio = <servicio/>;
	} else {
		//xml = new XML(xml);
		
		envio = <servicio>
					<parametro>
						<int>{idproducto}</int>
						<int>{idcliente}</int>
					</parametro>
				</servicio>;
	}
	
	var respuesta;
	
	try{
		respuesta = services.process(methodpost+'SOAWrapper',envio.toXMLString());
	} catch (ex) {
	 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	 }	
	 
	if (log.isInfoEnabled()) {
		log.info("Resultado de llamar a obtenerTarifasProductoPorProductoYCliente antes de postprocesado: "+respuesta);
	}
	
	if (respuesta!=null) {
		respuesta = new XML(respuesta);
		//respuesta = this.postProcessXML(respuesta);	
		//log.info("Resultado de llamar a obtenerTarifasProductoPorProductoYCliente tras postprocesado: "+respuesta.toXMLString());
	}

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
	
}


/**
 * Ajustamos el XML a la estructura y campos de la vista.
 */
function postProcessXML(xmlparam) {
	
	if (xmlparam != null){
	
		for each (bloqueocliente in xmlparam.Bloqueocliente){
			//log.info("postprocesado en obtenerTarifasProductoPorProductoYCliente. Bloqueocliente a procesar: " + bloqueocliente.toXMLString());
			var nombrecompleto = bloqueocliente.usuario.nombre.text() + " " + 
		 						 bloqueocliente.usuario.apellidos.text();
		 						 
			var fechabloqueoyresp = bloqueocliente.fechabloqueo.text() + " - " + nombrecompleto;
		 						 
			bloqueocliente.nombre = <nombre>{nombrecompleto}</nombre>;
			bloqueocliente.fechabloqueoyresp = <fechabloqueoyresp>{fechabloqueoyresp}</fechabloqueoyresp>;
			
			// Fuera elementos innecesarios
			//delete reserva.cliente;	
		}					
	}
	
	return xmlparam;
}

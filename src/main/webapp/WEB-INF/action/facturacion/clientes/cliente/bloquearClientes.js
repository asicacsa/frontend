var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	//log.info("Entra en el action bloquearCliente");
	
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}

	var xml = request.getParameter('xml');
	var envio;
	var result;
	
	if (log.isInfoEnabled()) {
		log.info('Este es el xml que nos llega para bloquearCliente: '+xml);
	}
	
	if(xml != null ) {

		xml = new XML(xml);
		xml = xml.BloqueoBajaClienteParam;
		
		envio = 
			<servicio>
				<parametro>
					{xml}	
				</parametro>
			</servicio>
		
		if (log.isInfoEnabled()) {
			log.info('Lo que le pasamos al servicio bloquearCliente: ' + envio);
		}
		var aux;
		
		try{
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
		
		result = <ok>
					{aux}
				 </ok>;
					 		
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a bloquearCliente es nulo!!!");
		}
		result = <error/>
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
}

// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {

	//log.info("Entrada a obtenerHistoricoBloqueosCliente.");	
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	var methodpost = request.getParameter('servicio');
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	if (log.isInfoEnabled()) {
		log.info("Valor de par?metro xml: " + xml);
		log.info("Valor de par?metro servicio: " + methodpost);
	}
	
	var envio = new XML();
	if(xml == null ) {
		envio = <servicio/>;
	} else {
		xml = new XML(xml);
		
		envio = <servicio>
					<parametro>
						<int>{xml}</int>
					</parametro>
				</servicio>;
	}
	
	var respuesta;
	
	try{
		respuesta = services.process(methodpost+'SOAWrapper',envio.toXMLString());
	} catch (ex) {
	 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	 }	
		 
	if (log.isInfoEnabled()) {
		log.info("Resultado de llamar a obtenerHistoricoBloqueosCliente antes de postprocesado: "+respuesta);
	}
	
	if (respuesta!=null) {
		respuesta = new XML(respuesta);
		respuesta = this.postProcessXML(respuesta);	
		if (log.isInfoEnabled()) {
			log.info("Resultado de llamar a obtenerHistoricoBloqueosCliente tras postprocesado: "+respuesta.toXMLString());
		}
	}

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
	
}

/**
 * Ajustamos el XML a la estructura y campos de la vista.
 */
function postProcessXML(xmlparam) {
	
	if (xmlparam != null){
	
		for each (bloqueocliente in xmlparam.Bloqueocliente){
			//log.info("postprocesado en obtenerHistoricoBloqueosCliente. Bloqueocliente a procesar: " + bloqueocliente.toXMLString());
			var nombrecompleto = bloqueocliente.usuario.nombre.text() + " " + 
		 						 bloqueocliente.usuario.apellidos.text();
		 						 
			var fechabloqueoyresp = bloqueocliente.fechabloqueo.text() + " - " + nombrecompleto;

			bloqueocliente.nombre = <nombre>{nombrecompleto}</nombre>;
			bloqueocliente.fechabloqueoyresp = <fechabloqueoyresp>{fechabloqueoyresp}</fechabloqueoyresp>;
			
			// Fuera elementos innecesarios
			//delete reserva.cliente;	
		}
	}
	
	return xmlparam;
}



	// fake provisional
	/*var respuesta = <ArrayList>
					<Reserva>
						<idreserva>1</idreserva>
						<nombre>Spiderman</nombre>
						<localizador>L-00000001</localizador>
						<fecha>10/10/2006</fecha>
						<importe>100</importe>
					</Reserva>
					<Reserva>
						<idreserva>2</idreserva>
						<nombre>Spiderman2</nombre>
						<localizador>L-00000002</localizador>
						<fecha>10/10/2006</fecha>
						<importe>100</importe>
					</Reserva>
					<Reserva>
						<idreserva>3</idreserva>
						<nombre>Spiderman3</nombre>
						<localizador>L-00000003</localizador>
						<fecha>10/10/2006</fecha>
						<importe>100</importe>
					</Reserva>
					<Reserva>
						<idreserva>4</idreserva>
						<nombre>Spiderman4</nombre>
						<localizador>L-00000003</localizador>
						<fecha>10/10/2006</fecha>
						<importe>100</importe>
					</Reserva>
					<Reserva>
						<idreserva>5</idreserva>
						<nombre>Spiderman5</nombre>
						<localizador>L-00000003</localizador>
						<fecha>10/10/2006</fecha>
						<importe>100</importe>
					</Reserva>
				</ArrayList>;*/
	//fFake provisional
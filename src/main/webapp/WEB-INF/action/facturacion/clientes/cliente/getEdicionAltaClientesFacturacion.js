var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');			
			
			
function handle(request, response) {

	//log.info("Entrada a getEdicionAltaClientesFacturacion.");
		
	var xml = request.getParameter('xml');
	var methodpost = request.getParameter('servicio');
	var envio;
	var cliente;

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	//print("Este es el xml que llega a getEdicionAltaClientesFacturacion: " + xml);	
	
	xml = new XML(xml);	
	
	if (xml != null && xml != '') 
	{	
		envio = <servicio>
					<parametro>
						<int>
							{xml}
						</int>
					</parametro>
				</servicio>;	
		
	} else {
		envio  = <servicio>
					<parametro>
						<Cliente/>
					</parametro>
				</servicio>;
		methodpost = 'getXMLModel';
	}
	
	if (log.isInfoEnabled()) {
		log.info("Este es el metodo al que vamos a llamar : "+methodpost);
	}
	var aux;
	
	try{
		aux = services.process(methodpost+'SOAWrapper',envio);					
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
		
	if (log.isInfoEnabled()) {
		log.info('Esto es lo que devuelve el servicio : ' + aux);
	}
	
	if (aux != null) {
		// hemos recibido cliente.
		cliente = new XML(aux.trim());		
		
		// si es edicion, obtenemos las sucursales para el cliente.
		// Obtenenerlas directamente mapeadas en betwixt de Cliente da error de redundancia ciclica.
		if (xml != null && xml != ''){
			//log.info("Obtenemos las sucursales.");
			methodpost = "obtenerListadoSucursalesPorCliente";
			aux = services.process(methodpost+'SOAWrapper',envio);	
			if (log.isInfoEnabled()) {
				log.info("Sucursales obtenidas: " + aux);
			}
			
			if (aux != null) {
				aux = new XML(aux.trim());	
				cliente = fusionaClientesYSucursales(cliente, aux);
			}
		}					
	}	
	
	if (log.isDebugEnabled())
	{
		log.debug("############### getEdicionAltaClienteFacturacion.js (handle): cliente antes de su procesado..." + cliente.toXMLString());
	}
	cliente = this.postProcessXML(cliente);
	
//	if (log.isInfoEnabled()) 
//	{
//		log.info("Esto es lo que devuelve getEdicionAltaClientesFacturacion.action: " + cliente.toXMLString());
//	}
	if (log.isDebugEnabled())
	{
		log.debug("############### getEdicionAltaClienteFacturacion.js (handle): cliente despues de su procesado..." + cliente.toXMLString());
	}

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(cliente.toXMLString());
}


function postProcessXML(param) 
{
//	if (log.isDebugEnabled())
//	{
//		log.debug("############### getEdicionAltaClienteFacturacion.js (postProcessXML): param antes de su procesado..." + param.toXMLString());
//		log.debug("############### getEdicionAltaClienteFacturacion.js (postProcessXML): xmlparam:" + xmlparam.toXMLString());
//	}
	
//	xmlparam = comun.deleteTrash(xmlparam.toXMLString());
//	xmlparam = comun.totalMatch(xmlparam,'@idref');
	xmlparam = comun.deleteTrash(param.toXMLString());
	xmlparam = comun.totalMatch(xmlparam,'@idref');
	
	if (log.isDebugEnabled())
	{
		log.debug("############### getEdicionAltaClienteFacturacion.js (postProcessXML): param antes de su procesado..." + param);
	}
	
//	if ((param.facturable.text() != null) && (param.facturable.text() == "1"))
//	{
//		delete param.facturable;
//		param.facturable = <facturable>1</facturable>;
//	} 
	// JMH 16.07.2013: Facturación electrónica
//	else if (param.facturable.text() == "2")
	if (param.facturable.text() == '2')
	{
		delete param.facturable;
		param.facturable = <facturable>1</facturable>;
		param.facturable_electronico = <facturable_electronico>1</facturable_electronico>;
	}
	else if (param.facturable.text() == '1')
	{
		param.facturable_electronico = <facturable_electronico>0</facturable_electronico>;
	}
	
	//log.info("Tratamos aparecepersonaenfactura");
	if (param.aparecepersonaenfactura.text()!=null && param.aparecepersonaenfactura.text() == "1")
	{
		param.aparecepersonaenfactura = <aparecepersonaenfactura>1</aparecepersonaenfactura>;
	} 
	else 
	{
		param.aparecepersonaenfactura = <aparecepersonaenfactura>0</aparecepersonaenfactura>;
	}
	
	return param;	
}


function fusionaClientesYSucursales(cliente, aux) {

	cliente.clientes = <clientes/>;
	if (log.isInfoEnabled()) {
		log.info("xml de sucursales: " + aux.toXMLString());
		log.info("cliente tras anyadir tag clientes vacio: " + cliente.toXMLString());
	}
	
	for each (var sucursal in aux.LabelValue){
		//log.info("sucursal a procesar: " + sucursal.toXMLString());
		sucursal = <Cliente><nombrecompleto>{sucursal.label.text().toXMLString()}</nombrecompleto></Cliente>;
		//log.info("sucursal procesada: " + sucursal.toXMLString());
		cliente.clientes.appendChild(sucursal);
		//log.info("cliente tras anyadir sucursal: " + cliente.toXMLString());
	}
	
	return cliente;
}
var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaCliente';
var servicio_edicion ='actualizarClientes';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) 
{	
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost;
	var envio;
	
	if (log.isInfoEnabled()) {
		log.info("Entrada a postEdicionAltaClientesFacturacion.action.");
	}
	
	if (xml!=null) {
		xml = new XML(xml);
		if (log.isInfoEnabled()) {
			log.info("postEdicionAltaClientesFacturacion, xml: " + xml.toXMLString());
		}
		
		if (xml.name()!="Cliente"){
			xml = xml.Cliente;
		}
		

		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicion de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.
		
		if(methodpost == null) {
			//Si es un alta no poseera la propiedad id
			// ALTA
			if (xml.idcliente.text().length() == 0) {
				methodpost = servicio_alta;
				xml = this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							{xml}
						</parametro>
					</servicio>
			// EDICION	
			} else {
				methodpost = servicio_edicion;
				xml= this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							<list>
								{xml}
							</list>
						</parametro>
					</servicio>			
			}
		}		
		
		if (log.isInfoEnabled()) {
			log.info("postEdicionAltaClientesFacturacion, envio: " + envio);
			log.info("postEdicionAltaClientesFacturacion, metodo post : " + methodpost);
		}
		
		//Y llamamos al servicio...
		var aux;
		
		try{
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
		
		aux = new XML(aux);
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		resultado = 
				<ok>
					{aux}
				</ok>
		//if (methodpost == servicio_alta){
		//	resultado = <ok>alta</ok>
		//} else {
		//	resultado = <ok>edicion</ok>
		//}
		if (log.isInfoEnabled()) {
			log.info("postEdicionAltaClientesFacturacion, resultado: " + resultado.toXMLString());
		}
					
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a postEdicionAltaClientesFacturacion es nulo!!!");
		}
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) 
{
	//Datos obligatorios que no pueden estar vacios ...
	if (log.isInfoEnabled()) 
	{
		log.info("postEdicionAltaClientesFacturacion, param pasado a preprocesar : " + param);
	}
	
	param = new XML(param);
	param = comun.borraElementosSinHijos(param.toXMLString());
	
	if (log.isInfoEnabled()) 
	{
		log.info("postEdicionAltaClientesFacturacion, param despues del borrado de elementos sin hijos: " + param);
	}
	
	
	// renombramos clienteprincipal a cliente
	//var clienteprincipal = param.clienteprincipal;
	//log.info("Valor de clienteprincipal: " + clienteprincipal);
	//if (clienteprincipal.descendants().length() != 0){
	//	clienteprincipal.setName('cliente');
	//}
	
	// cambiamos los true por 1 y los false por 0 (checkboxes)
	//log.info("Tratamos facturable");
	
	// JMH 16.07.2013: Facturación electrónica
	if (param.facturable.text() == '1' || param.facturable.text() == 'true')
	{
		if (param.facturable_electronico.text() == '1' || param.facturable_electronico.text() == 'true')
		{
			param.facturable = <facturable>2</facturable>;
		}
		else
		{
			param.facturable = <facturable>1</facturable>;
		}
	}
	else
	{
		param.facturable = <facturable>0</facturable>;
	}
	
	delete param.facturable_electronico;
	
	//log.info("Tratamos generarrappelporsucursal");
	if (param.generarrappelporsucursal.text()=='1' || param.generarrappelporsucursal.text()=='true'){
		param.generarrappelporsucursal = <generarrappelporsucursal>1</generarrappelporsucursal>;
	} else {
		param.generarrappelporsucursal = <generarrappelporsucursal>0</generarrappelporsucursal>;
	}
	
	//log.info("Tratamos rappelautomatico");
	if (param.rappelautomatico.text()=='1' || param.rappelautomatico.text()=='true'){
		param.rappelautomatico = <rappelautomatico>1</rappelautomatico>;
	} else {
		param.rappelautomatico = <rappelautomatico>0</rappelautomatico>;
	}
	
	//log.info("Tratamos aparecepersonaenfactura");
	if (param.aparecepersonaenfactura.text()=='1' || param.aparecepersonaenfactura.text()=='true'){
		param.aparecepersonaenfactura = <aparecepersonaenfactura>1</aparecepersonaenfactura>;
	} else {
		param.aparecepersonaenfactura = <aparecepersonaenfactura>0</aparecepersonaenfactura>;
	}
	
	// comprobamos siempre si es sucursal de alguien, si no lo es,
	// es principal.
	if (param.cliente.idcliente.text().toXMLString() != ""){
		if (log.isInfoEnabled()) {
			log.info("Establecemos el cliente como NO principal.");
			log.info("El cliente es sucursal de " + param.cliente.idcliente.text());
		}
		
		param.principal = <principal>0</principal>;
	} else {
		if (log.isInfoEnabled()) {
			log.info("Establecemos el cliente como principal.");
		}
		
		param.principal = <principal>1</principal>;
	}
	
	// quitamos la propiedad nombrecompleto, que al ser de solo lectura
	// no le gusta a XStream
	delete param..nombrecompleto;
	
	
	// en caso de ser facturable, y ya que temporalmente no pueden establecerse
	// las condiciones de cliente desde la interfaz, marcamos estas condiciones
	// a pi??n para que puedan crear clientes facturables.
	//if (param.facturable.text() == '1'){
	//	log.info("El cliente es facturable, establecemos las condiciones de cliente.");
	//	//param.tipoclienteactivo.idtipocliente = <tipoclienteactivo><idtipocliente>1</idtipocliente></tipoclienteactivo>;
	//	//param.formapago.idformapago = <formapago><idformapago>3</idformapago></formapago>
	//	//param.direccionenvio.iddireccionenvio = <direccionenvio><iddireccionenvio>1</iddireccionenvio></direccionenvio>;
	//	
	//	param.tipoclienteactivo = <tipoclienteactivo><idtipocliente>1</idtipocliente></tipoclienteactivo>;
	//	param.formapago = <formapago><idformapago>3</idformapago></formapago>
	//	param.direccionenvio = <direccionenvio><iddireccionenvio>1</iddireccionenvio></direccionenvio>;
	//}
	
	procesaDirecciones(param);
	//this.borrarDirecciones(param);
	
	return param;
	
}


function procesaDirecciones(param){
	borrarDirecciones(param);
	
	// si el cliente es sucursal, la direccion pasada (dentro de direccion fiscal)
	// es realmente direccion de origen, y la fiscal es la fiscal de su principal.
	// Si el cliente es principal, la direccion es la fiscal (origen en blanco).
	
	if (param.principal.text()=='0' && !param.hasOwnProperty("direccionorigen")){
		param.direccionfiscal.setName("direccionorigen");
	}
}


function borrarDirecciones(param) {	
	//Preguntamos si tiene 0 o un nodo hijo.
	if (param.direccionorigen.children().length() < 2) {
		//Tiene 0 o 1 hijo
		if (param.direccionorigen.children().length() == 0) {
			//Lo eliminamos directamente.
			delete param.direccionorigen;
		} else {
			//Miramos que el hijo no se el idDireccion, si es el idDireccion eliminamos el nodo, si no es asi
			// dejamos el nodo.
			if (param.direccionorigen.child("iddireccion").length()>0) {
				delete param.direccionorigen;
			}
		}
	
		
	}
	//Preguntamos si tiene 0 o un nodo hijo.	
	if (param.direccionfiscal.children().length() < 2) {
		//Tiene 0 o 1 hijo
		if (param.direccionfiscal.children().length() == 0) {
			//Lo eliminamos directamente.
			delete param.direccionfiscal;
		} else {
			//Miramos que el hijo no se el idDireccion, si es el idDireccion eliminamos el nodo, si no es asi
			// dejamos el nodo.
			if (param.direccionfiscal.child("iddireccion").length()>0) {
				delete param.direccionfiscal;
			}
		}
	
		
	}
	//Preguntamos si tiene 0 o un nodo hijo.	
	if (param.direccionauxiliar.children().length() < 2) {
		//Tiene 0 o 1 hijo
		if (param.direccionauxiliar.children().length() == 0) {
			//Lo eliminamos directamente.
			delete param.direccionauxiliar;
		} else {
			//Miramos que el hijo no se el idDireccion, si es el idDireccion eliminamos el nodo, si no es asi
			// dejamos el nodo.
			if (param.direccionauxiliar.child("iddireccion").length()>0) {
				delete param.direccionauxiliar;
			}
		}
	
		
	}
}

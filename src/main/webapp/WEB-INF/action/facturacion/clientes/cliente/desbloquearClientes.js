var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	//log.info("Entra en el action desbloquearCliente");
	
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}

	var xml = request.getParameter('xml');
	var envio;
	var result;
	
	if (log.isInfoEnabled()) {
		log.info('Este es el xml que nos llega para desbloquearCliente: '+xml);
	}
	
	if(xml != null ) {
		
		// Pasamos solo el id del cliente a desbloquear
		xml = new XML(xml);
		
		envio = 
			<servicio>
				<parametro>
					<list>
						<int>{xml}</int>
					</list>	
				</parametro>
			</servicio>
	
		if (log.isInfoEnabled()) {
			log.info('Lo que le pasamos al servicio desbloquearCliente: ' + envio);
		}
		var aux;
		
		try{
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
		
		result = <ok>
					{aux}
				 </ok>;
					 		
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a desbloquearCliente es nulo!!!");
		}
		result = <error/>
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
}

var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaTipoRappel';
var servicio_edicion ='actualizarTiposRappel';


function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	
	var resultado;
	
	var methodpost;
	
	var modoEdicion = request.getParameter('_modo');
	
	var envio;
	
	if (xml!=null) {
		xml = new XML(xml);
		
		if (log.isInfoEnabled()) {
			log.info("XML original "  +  xml);
		}
		
		xml = xml.children();
		
		xml = this.preProcessXML(xml);
		
		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicio de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.

		//Si es un alta no poseera la propiedad idproducto
		if (modoEdicion=="alta") {
			methodpost = servicio_alta;
			envio = 
				<servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>
			
		} else {
			methodpost = servicio_edicion;
			envio = 
				<servicio>
					<parametro>
						<list>
							{xml}
						</list>
					</parametro>
				</servicio>				
		}
		
		if (log.isInfoEnabled()) {
			log.info("Este es el envio : "+envio);
			log.info("Este es el metodo post : "+methodpost);
		}
		
		//Y llamamos al servicio...		
		var aux;
		
		try{
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
		 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		 }
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		
		resultado = 
				<ok>
					{aux}
				</ok>
		//En aux tenemos la respuesta			
	} else {
		if (log.isInfoEnabled()) {
			log.info("No hay dato en el post : ");
		}
		resultado = <error/>
	}
	
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...

	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('transformerXML.js');
	
	delete param..selected; //Eliminamos todos los tags selected
	
		//Obligatorio : Campo dado de baja, por defecto lo ponemos a 0 ...
	if (param.dadodebaja.text().length() == 0) {
			param.dadodebaja=0;
	}
	
	if (param.acumulativo.text() == false) {
			param.acumulativo=<acumulativo>0</acumulativo>
		} else if (param.acumulativo.text() == true) {
			param.acumulativo=<acumulativo>1</acumulativo>
		} else {
			if (log.isWarnEnabled()) {
				log.warn("LOG(WARN) : postRealizarVent. El dato activada llega con valor distinto a 0 ? 1.")
			}
		}
	
	param = comun.borraElementosSinHijos(param); //Eliminamos todos los elementos que se han quedado sin hijos
		

	return param;
	
}


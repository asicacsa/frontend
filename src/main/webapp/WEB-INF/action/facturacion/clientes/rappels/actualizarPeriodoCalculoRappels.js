var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {
		
	var fechaCalculo = request.getParameter('fechaCalculo');		
	var fechaDesde = request.getParameter('fechaDesde');		
	var fechaHasta = request.getParameter('fechaHasta');		

	var methodpost ='actualizarPeriodoCalculoRappels';
	var resultado;
	
	var methodpost;
	
	var envio;
	

	if (fechaCalculo != null && fechaDesde != null && fechaHasta != null ) {
		
		
		envio = 
			<servicio>
				<parametro>
					<PeriodosRappel>
						<diacalculorappel>{fechaCalculo}</diacalculorappel>
						<fechainicioperiodo>{fechaDesde}</fechainicioperiodo>
						<fechafinperiodo>{fechaHasta}</fechafinperiodo>					
					</PeriodosRappel>
				</parametro>
			</servicio>			
		
		
		if (log.isInfoEnabled()) {		
			log.info("Este es el envio : "+envio);
			log.info("Este es el metodo post : "+methodpost);
		}
		
		var aux=null;
		try{
			//Y llamamos al servicio...		
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			if (log.isErrorEnabled()) {
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost);
				log.error('LOG(ERROR) : con este tipo de error : ' + ex);
				log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
			}
		throw ex.javaException;
		}
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		
		resultado = 
				<ok>
					{aux}
				</ok>
		//En aux tenemos la respuesta			
	} else {
		resultado = <error/>
	}
	
	response.writer.println(resultado.toXMLString());
}
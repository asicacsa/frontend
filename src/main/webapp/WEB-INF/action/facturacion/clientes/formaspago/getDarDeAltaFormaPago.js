var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');			
			
			
function handle(request, response) {

	//log.info("Entrada a getDarDeAltaFormaPago.");
		
	var xml = request.getParameter('xml');
	var methodpost = request.getParameter('servicio');
	var envio;
	var result;

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	//print("Este es el xml que llega a getDarDeAltaFormaPago: " + xml);	
	
	xml = new XML(xml);	
	
	if (xml != null && xml != '') {
		
		envio = <servicio>
					<parametro>
						<int>
							{xml}
						</int>
					</parametro>
				</servicio>;	
		
	} else {
		envio  = <servicio>
					<parametro>
						<Formapago/>
					</parametro>
				</servicio>;
		methodpost = 'getXMLModel';
	}
	
	if (log.isInfoEnabled()) {
		log.info("Este es el metodo al que vamos a llamar : "+methodpost);
	}
	var aux;
	
	try{
		aux = services.process(methodpost+'SOAWrapper',envio);	
	} catch (ex) {
	 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	 }	
	
				
	if (log.isInfoEnabled()) {
		log.info('Esto es lo que devuelve el servicio : ' + aux);	
	}
	
	aux = new XML(aux);
	result = this.postProcessXML(aux);
	
	if (log.isInfoEnabled()) {
		log.info("Esto es lo que devuelve getDarDeAltaFormaPago.action: " + result.toXMLString());
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
}


function postProcessXML(param) {
	//param = comun.deleteTrash(param.toXMLString());
	//param = comun.totalMatch(param,'@idref');
	
	//log.info("Tratamos distintaformapagodevolucion");
	if (param.distintaformapagodevolucion.text()!=null && param.distintaformapagodevolucion.text()=='1'){
		param.distintaformapagodevolucion = <distintaformapagodevolucion>true</distintaformapagodevolucion>;
	} else {
		param.distintaformapagodevolucion = <distintaformapagodevolucion>false</distintaformapagodevolucion>;
	}
	
	if (param.credito.text()!=null && param.credito.text()=='1'){
		param.credito = <credito>true</credito>;
	} else {
		param.credito = <credito>false</credito>;
	}
	
	return param;	
}
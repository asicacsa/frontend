var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaCanal';
var servicio_edicion ='actualizarCanales';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost;
	var envio;
	
	//log.info("Entra en el handle de postDarDeAltaCanal");
	
	if (xml!=null) {
		xml = new XML(xml);
		xml = xml.Canal;
		if (log.isInfoEnabled()) {
			log.info("Este es el xml que llega a postDarDeAltaCanal: "+xml.toXMLString());
		}
		

		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicion de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.
		
		if(methodpost == null) {
			//Si es un alta no poseera la propiedad idperfilvisitante
			// ALTA
			if (xml.idcanal.text().length() == 0) {
				methodpost = servicio_alta;
				xml = this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							{xml}
						</parametro>
					</servicio>
			// EDICI?N	
			} else {
				methodpost = servicio_edicion;
				xml= this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							<list>
								{xml}
							</list>
						</parametro>
					</servicio>			
			}
		}		
		
		if (log.isInfoEnabled()) {
			log.info("Este es el envio : "+envio);
			//log.info("Este es el metodo post : "+methodpost);
		}
		
		//Y llamamos al servicio...
		
		var aux;
		
		try{
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		resultado = 
				<ok>
					{aux}
				</ok>
					
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a postDarDeAltaCanal es nulo!!!");
		}
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	
	param = new XML(param);
	param = comun.borraElementosSinHijos(param.toXMLString());
	
	for each(i in param.usuarios.Usuario){
		i.nombrecompleto.setName('nombre');
	}
	delete param..selected; //Eliminamos todos los tags selected
	
	return param;
	
}
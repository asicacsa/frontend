var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');			
			
			
function handle(request, response) {

	//log.info("Entrada a getDarDeAltaCanal.action");
		
	var xml = request.getParameter('xml');
	var methodpost = request.getParameter('servicio');
	var envio;
	var result;

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	//print("Este es el xml que llega a getDarDeAltaCanal: " + xml);	
	
	xml = new XML(xml);	
	
	if (xml != null && xml != '') {
		
		envio = <servicio>
					<parametro>
						<int>
							{xml}
						</int>
					</parametro>
				</servicio>;	
		
	} else {
		envio  = <servicio>
					<parametro>
						<Canal/>
					</parametro>
				</servicio>;
		methodpost = 'getXMLModel';
	}
	
	//log.info("Este es el metodo al que vamos a llamar : "+methodpost);
	var aux;
	
	try{
		aux = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
	 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	 }	
	 
	if (log.isInfoEnabled()) {					
		log.info('Esto es lo que devuelve el servicio : ' + aux);	
	}
	
	aux = new XML(aux);
	result = this.postProcessXML(aux);
	
	if (log.isInfoEnabled()) {
		log.info("Esto es lo que devuelve getDarDeAltaCanal.action: " + result.toXMLString());
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
}


function postProcessXML(param) {
	//param = comun.deleteTrash(param.toXMLString());
	//param = comun.totalMatch(param,'@idref');
	
	// a??adimos Cliente.idcliente, en caso de que esta propiedad este vacia.
	// Por tratarse de un set, si esta vacio, no podemos hacer esto facilmente
	// desde la capa de servicios, en la transformacion a xml.

	//log.info("Tratamos clientes.Cliente. Longitud: " + param.clientes.Cliente.length());
	
	if (param.clientes.Cliente.length() == 0){
		param.clientes.Cliente = <Cliente><idcliente/></Cliente>;
	}
	
	return param;	
}
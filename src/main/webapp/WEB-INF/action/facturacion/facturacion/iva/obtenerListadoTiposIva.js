importPackage(Packages.org.springframework.web.servlet);
var forward = 'obtenerComun.action';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('obtenerComun.js');
function handle(request, response) {
	
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : Llamada a obtenerListadoTiposIva."+this.getClass().getName());		
		log.info("LOG(INFO) : to this service : "+request+'SOAWrapper'+' : '+this.getClass().getName());
		log.info("LOG(INFO) : forward to "+forward);
	}
	
	request = comun.setServicio(request);
	var modelAndView = new ModelAndView(forward);
	return modelAndView;
}

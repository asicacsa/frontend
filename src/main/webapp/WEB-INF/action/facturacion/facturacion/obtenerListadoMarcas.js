importPackage(Packages.org.springframework.web.servlet);
var forward = 'obtenerComun.action';

function handle(request, response) {
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('obtenerComun.js');
	request = comun.setServicio(request);
	var modelAndView = new ModelAndView(forward);
	return modelAndView;
}
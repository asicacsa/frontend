var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaTipoFactura';
var servicio_edicion ='actualizarTiposFactura';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost;
	var envio;
	
	//log.info("Entra en el handle de postDarDeAltaTipoFactura");
	
	if (xml!=null) {

		xml = new XML(xml);
		xml = xml.Tipofactura;

		//log.debug("Este es el xml que llega a postDarDeAltaTipoFactura: "+xml.toXMLString());
		

		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicion de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.
		
		if(methodpost == null) {
			//Si es un alta no poseera la propiedad idperfilvisitante
			// ALTA
			if (xml.idtipofactura.text().length() == 0) {
				methodpost = servicio_alta;
				xml = this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							{xml}
						</parametro>
					</servicio>
			// EDICI?N	
			} else {
				methodpost = servicio_edicion;
				xml= this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							<list>
								{xml}
							</list>
						</parametro>
					</servicio>			
			}
		}		
		
		//log.info("Este es el envio : "+envio);
		//log.info("Este es el metodo post : "+methodpost);

		var aux;
		//Y llamamos al servicio...
		
		try {
		 	aux = new XML(services.process(methodpost+'SOAWrapper',envio));
		 } catch (ex) {
		 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		 }
		
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		resultado = 
				<ok>
					{aux}
				</ok>
					
	} else {
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	
	//log.info("Lo que vamos a pasar por preProcessXML : "+param);
	param = new XML(param);

	
	if (param.hasOwnProperty('activa')) {
		if (param.activa.text() == 'true') {
			param.activa = <activa>1</activa>
		}else{
			if (param.activa.text() != '1') {
				param.activa = <activa>0</activa>
			}
		}
	}	

	if (param.hasOwnProperty('pordefecto')) {
		if (param.pordefecto.text() == 'true') {
			param.pordefecto = <pordefecto>1</pordefecto>
		}else{
			if (param.pordefecto.text() != '1') {
				param.pordefecto = <pordefecto>0</pordefecto>
			}
		}
	}	


	if (param.hasOwnProperty('rectificativa')) {
		if (param.rectificativa.text() == 'true') {
			param.rectificativa = <rectificativa>1</rectificativa>
		}else{
			if (param.rectificativa.text() != '1') {
				param.rectificativa = <rectificativa>0</rectificativa>
			}
		}
	}	


	param = comun.borraElementosSinHijos(param.toXMLString());

	return param;
	
}

var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	var xml = request.getParameter('xml');	
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var resultado;
		
	if (xml!=null) {
		
		var envio = <servicio>
						<parametro>
							<int>
								{xml}
							</int>
						</parametro>
					</servicio>
					
		//log.info("Este es el xml que enviamos para el get: "+envio.toXMLString());
		var aux = services.process(methodpost+'SOAWrapper',envio);					
		//log.info('Este es el valor que retorna : '+new XML(aux).toXMLString())
		if (aux != null) {
			resultado = new XML(aux);
			resultado = this.postProcessXML(resultado);
		}		
	} else {
	
		modelstring = request.getAttribute('__modelo');

		var envio = <servicio>
						<parametro>
							<Tipofactura/>							
						</parametro>
					</servicio>

		//log.info("Este es el xml que enviamos para el get modelo: "+envio.toXMLString());		
		resultado = services.process('getXMLModelSOAWrapper',envio);
		//log.info('Devuelto el modelo  : '+new XML(resultado).toXMLString())
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado);
}

function postProcessXML(xmlparam) {
	//Tenemos que tratar el overbooking para pasarlo a un boolean..

	if (xmlparam != null) {

		if (!xmlparam.entidadgestora.hasOwnProperty('identidadgestora')){
			xmlparam.entidadgestora= <entidadgestora><identidadgestora/></entidadgestora>
		}

		if (!xmlparam.hasOwnProperty('idtipofacturarectificativa')){
			xmlparam.idtipofacturarectificativa= <idtipofacturarectificativa></idtipofacturarectificativa>
		}


	}

	//log.info("##### Este es el xml que enviamos para el get modelo: "+xmlparam.toXMLString());		

	return xmlparam;	
}



//var service = context.beans.getBean('httpServiceSOA');

// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = request.getParameter('servicio');
	
	if (log.isInfoEnabled()) {
		log.info("Entrada a buscarBonosCreditoNoFacturados.action");
	}
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		var envio = '<servicio><parametro><BuscarBonosCreditoNoFacturadosParam/></parametro></servicio>'
	} else {
		xml = new XML(xml);
		if (log.isInfoEnabled()) {
			log.info("LOG(INFO) : buscarBonosCreditoNoFacturados params before the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		}
		xml = this.preProcessXML(xml);
		if (log.isInfoEnabled()) {
			log.info("LOG(INFO) : buscarBonosCreditoNoFacturados params after the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		}
		
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>	
	}
	
	var respuesta;
	
	try{
		respuesta = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	//var respuesta = new XML(service.process(methodpost,envio));
	
	if (respuesta!=null) {
		respuesta = new XML(respuesta);
		if (log.isInfoEnabled()) {
			log.info("LOG(INFO): valor de la respuesta antes de ser postprocesada: " + respuesta);
		}
		respuesta = this.postProcessXML(respuesta);	
		if (log.isInfoEnabled()) {
			log.info("Resultado de llamar a buscarBonosCreditoNoFacturados: "+respuesta.toXMLString());
		}

	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
	
}


function preProcessXML(param) {
	//log.debug("LOG(INFO) : params before the borraElementosSinHijos : "+this.getClass().getName()+" "+param.toXMLString());
	param = comun.borraElementosSinHijos(param.toXMLString());
	//log.debug("LOG(INFO) : params after the borraElementosSinHijos : "+this.getClass().getName()+" "+param.toXMLString());	
	
	param = comun.deleteSelectedTag(param.toXMLString());
	param = new XML(param);
	//log.debug("LOG(INFO) : params after the deleteSelectedTag : "+this.getClass().getName()+" "+param.toXMLString());	
	
	// cambiamos los true por 1 y los false por 0 (checkboxes)
	if (param.soloPrincipal.text()!=null && param.soloPrincipal.text()=='true'){
		param.soloPrincipal = <soloPrincipal>1</soloPrincipal>;
	} else {
		param.soloPrincipal = <soloPrincipal>0</soloPrincipal>;
	}	
	
	return param;
	
}


/**
 * Ajustamos el XML a la estructura y campos de la vista.
 */
function postProcessXML(xmlparam) {
	
	var importe = 0;
	
	if (xmlparam != null){
		xmlparam = new XML(xmlparam);
		
		for each (bono in xmlparam.Bono){
			
			// el importe total de la venta sera la suma de los importes de sus lineasdetalle
			importe = 0;
			importe = bono.lineacanje.importe.text() / bono.lineacanje.cantidad.text();
			bono.importebono = <importebono>{importe}</importebono>;
			
			// quitamos los canjebonos anulados
			var canjesanulados = bono..Canjebono.(@anulado=='1');
			for (var i = 0; i < canjesanulados.length(); i++){
				delete canjesanulados[i];
			}
			
		}	
								
	}
	
	return xmlparam;
}
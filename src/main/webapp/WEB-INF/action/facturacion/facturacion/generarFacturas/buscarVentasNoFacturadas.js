//var service = context.beans.getBean('httpServiceSOA');

// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');
var busquedamethod = 'buscarVentasNoFacturadas';
var busquedafiltradamethod = 'buscarVentasNoFacturadasFiltradas';

function handle(request, response) {
	
	if (log.isInfoEnabled()){
		log.info("Entrada a buscarVentasNoFacturadas.action");
	}
	
	
	var xml = request.getParameter('xml');
	var filter = request.getParameter('filter');
	var envio;
	var methodpost;
	
	//var methodpost = request.getParameter('servicio');
	// si servicio no se envia, intenta obtenerlo del nombre del action
	//if(methodpost == null) {
	//	methodpost = request.getAttribute('__request_service');
	//}
	
	
	if(xml == null ) {
		envio = '<servicio><parametro><Buscarventanofacturadaparam/></parametro></servicio>'
	} else {
		xml = new XML(xml);
		if (log.isInfoEnabled()) {
			log.info("LOG(INFO) : buscarVentasNoFacturadas params before the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		}
		xml = this.preProcessXML(xml);
		if (log.isInfoEnabled()) {
			log.info("LOG(INFO) : buscarVentasNoFacturadas params after the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		}
		
		// el filtro es una lista de ids de venta. Si no lo pasamos, el servicio a invocar
		// es el de busqueda normal. Si lo pasamos, el servicio es el de búsqueda filtrada.
		
		if (filter == null){
			envio = <servicio>
						<parametro>
							{xml}
						</parametro>
					</servicio>;
					
			methodpost = busquedamethod;	
		} else {
			filter = new XML(filter);
			envio = <servicio>
						<parametro>
							{xml}
							{filter}
						</parametro>
					</servicio>;
					
			methodpost = busquedafiltradamethod;
		}
	}
	
	
	if (log.isInfoEnabled()){
		log.info("buscarVentasNoFacturadas.action, accediendo a servicio " + methodpost + " con datos: " + envio);
	}
	
	var respuesta;
	
	try{
		respuesta = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	if (respuesta!=null) {
		//if (log.isInfoEnabled()) {
		//	log.info("LOG(INFO): valor de la respuesta antes de ser postprocesada: " + respuesta.substr(0,300) + "[...]");
		//}
		
		//respuesta = new XML(respuesta);
		//respuesta = this.postProcessXML(respuesta);	
		
		if (log.isInfoEnabled()) {
			log.info("buscarVentasNoFacturadas, resultado final: " + respuesta.substr(0,300) + "[...]");
		}

	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta);
	
}


function preProcessXML(param) {
	//log.debug("LOG(INFO) : params before the borraElementosSinHijos : "+this.getClass().getName()+" "+param.toXMLString());
	param = comun.borraElementosSinHijos(param.toXMLString());
	//log.debug("LOG(INFO) : params after the borraElementosSinHijos : "+this.getClass().getName()+" "+param.toXMLString());	
	
	param = comun.deleteSelectedTag(param.toXMLString());
	param = new XML(param);
	//log.debug("LOG(INFO) : params after the deleteSelectedTag : "+this.getClass().getName()+" "+param.toXMLString());	
	
	// cambiamos los true por 1 y los false por 0 (checkboxes)
	if (param.soloPrincipal.text()!=null && param.soloPrincipal.text()=='true'){
		param.soloPrincipal = <soloPrincipal>1</soloPrincipal>;
	} else {
		param.soloPrincipal = <soloPrincipal>0</soloPrincipal>;
	}
	
	if (param.buscarProductosYBonosAgencia.text() == 'true'){
		param.buscarProductosYBonosAgencia = <buscarProductosYBonosAgencia>1</buscarProductosYBonosAgencia>; 
	} else if (param.buscarProductosYBonosAgencia.text() == 'false'){
		param.buscarProductosYBonosAgencia = <buscarProductosYBonosAgencia>0</buscarProductosYBonosAgencia>; 
	}
	
	if (param.buscarBonoPrepago.text() == 'true'){
		param.buscarBonoPrepago = <buscarBonoPrepago>1</buscarBonoPrepago>; 
	} else if (param.buscarBonoPrepago.text() == 'false'){
		param.buscarBonoPrepago = <buscarBonoPrepago>0</buscarBonoPrepago>; 
	}
	
	return param;
	
}


/**
 * Ajustamos el XML a la estructura y campos de la vista.
 */
/*function postProcessXML(xmlparam) {
	
	var importe = 0;
	var nombrecompleto = "";
	
	if (xmlparam != null){
		xmlparam = new XML(xmlparam);
		
		for each (venta in xmlparam.Venta){
			
			// el importe total de la venta ser? la suma de los importes de sus lineasdetalle
			importe = 0; 
			for each (lineadetalle in venta.lineadetalles.Lineadetalle){
				importe = importe + (lineadetalle.importe.text() - 0); // toma typecast de fresa!
			}
			//log.info("Importe: " + importe);
			venta.importetotalventa = <importetotalventa>{importe}</importetotalventa>;

		}	
								
	}
	
	return xmlparam;
}*/
//var service = context.beans.getBean('httpServiceSOA');

// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	if (log.isInfoEnabled()) {
		log.info("Entrada a generarFactura.action");
	}
	
	var xml = request.getParameter('xml');
	var methodpost = request.getParameter('servicio');
	var respuesta;
	
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	if(xml == null ) {
		respuesta = new XML('<error>xml nulo!</error>');
	} else {
		xml = new XML(xml);
		xml = this.preProcessXML(xml);
		
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>
				
		if (log.isInfoEnabled()) {
			log.info("generarFactura, envio: " + envio.toXMLString());
		}
				
		respuesta = services.process(methodpost+'SOAWrapper',envio.toXMLString());
		
		if (log.isInfoEnabled()) {
			log.info("generarFactura, respuesta del servicio: " + respuesta);
		}
	
		if (respuesta!=null) {
			respuesta = new XML(respuesta);
			respuesta = this.postProcessXML(respuesta);	
		}
	}
	
	if (log.isInfoEnabled()) {
		log.info("generarFactura, resultado final: " + respuesta.toXMLString().substr(0,300) + "[...]");
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
	
}


function preProcessXML(param) {

	//log.info("LOG(INFO) : params before the borraElementosSinHijos : "+this.getClass().getName()+" "+param.toXMLString());
	param = comun.borraElementosSinHijos(param.toXMLString());
	//log.info("LOG(INFO) : params after the borraElementosSinHijos : "+this.getClass().getName()+" "+param.toXMLString());	
	
	param = comun.deleteSelectedTag(param.toXMLString());
	param = new XML(param);
	//log.info("LOG(INFO) : params after the deleteSelectedTag : "+this.getClass().getName()+" "+param.toXMLString());		
	
	return param;
	
}


/**
 * Ajustamos el XML a la estructura y campos de la vista.
 */
function postProcessXML(xmlparam) {
	
	var importe = 0;
	var nombrecompleto = "";
	var erroresFacturacion ="";
	
	if (xmlparam != null){
		xmlparam = new XML(xmlparam);

		//Facturas		
		for each (factura in xmlparam.ResultadoFacturacion.Factura){
			
			// construimos el nombre del cliente
			nombrecompleto = factura.cliente.nombre.text() + ' ' 
							+ factura.cliente.apellido1.text() + ' '
							+ factura.cliente.apellido2.text();
			factura.cliente.nombrecompleto =  <nombrecompleto>{nombrecompleto}</nombrecompleto>;

		}
		
								
	}
	
	return xmlparam;

}
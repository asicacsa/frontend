importPackage(Packages.org.springframework.web.servlet);
//var forward = 'http://localhost:8080/services/JasperController?report=FacturaEmitida&format=pdf&idFactura=1';
function handle(request, response) {
	//log.info("LOG(INFO) : Llamada a emitirFacturas.");		
	
	//var forward = 'jasper.post?report=FacturaEmitida&format=pdf';
	//var forward = 'jasper.post?report=FacturaNormalYRectificativa&format=pdf';
	
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO): Entrada a emitirFacturas.action.");	
	}
	
	var idfactura = request.getParameter('idFactura');
	var viewOnly = request.getParameter('viewOnly');
	var nombreinforme = request.getParameter('nombreinforme');
	var forward =  'jasper.post?report='+nombreinforme+'&format=pdf';
	
	
	forward = forward + '&idFactura=' + idfactura;
	
	if (typeof(viewOnly)!='null' && typeof(viewOnly)!='undefined' && viewOnly!=''){
		forward = forward + '&viewOnly=' + viewOnly;
	}
	
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : emitirFacturas.action, forward to url: " + forward);	
	}
	
	response.sendRedirect(forward);
	return false;
}

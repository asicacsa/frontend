var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
	
	
	//log.info('Hemos entrado en actualizarFactura');	
	//var xml = request.getParameter('xml');		
	var resultado;
	var methodpost = 'actualizarFactura';
	var envio;
		

	
	var idFactura = request.getParameter('idFactura');	
	var idFormapago = request.getParameter('idFormapago');
	var idFormaenvio = request.getParameter('idFormaenvio');
	var fechaGeneracion = request.getParameter('fechaGeneracion');
	var numImpresiones = request.getParameter('numImpresiones');
	var observaciones = request.getParameter('observaciones');

	if (log.isInfoEnabled()) {
		log.info('Los datos q me llegan al JS....'+idFactura+' ' +idFormapago+' '+idFormaenvio);
	}

	if ((numImpresiones == null) || (numImpresiones =="")) {
		throw new java.lang.Exception("El numero de impresiones no puede ser nulo.");
	}

	if (idFormapago=="null"){
		idFormapago="";
	}
	
	var xml = <dto>
	 			<idfactura>{idFactura}</idfactura>
	 			<idformapago>{idFormapago}</idformapago>
	 			<iddireccionenvio>{idFormaenvio}</iddireccionenvio>
	 			<fechageneracion>{fechaGeneracion}</fechageneracion>
	 			<numimpresiones>{numImpresiones}</numimpresiones>
	 			<observaciones>{observaciones}</observaciones>
			  </dto>;

		
		
	xml = this.preProcessXML(xml);
	envio = 
		<servicio>
			<parametro>
				{xml}
			</parametro>
		</servicio>

	if (log.isInfoEnabled()) {
		log.info("Este es el envio : "+envio);	
	}
		
	
		var aux;
		
		try{
			//Y llamamos al servicio...
			aux = services.process(methodpost+'SOAWrapper',envio);
			
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
		
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		if(aux != null){
		//log.info('aux es distinto de nulo');
			aux = new XML(aux);
			resultado = aux;
				
			if (log.isInfoEnabled()) {	
				log.info("Este es el resultado : "+resultado);
			}
		} else {
			resultado = new XML();
			if (log.isInfoEnabled()) {
				log.info("El aux es null");
			}
		}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	//log.info("Lo que vamos a pasar por preProcessXML : "+param);
	
	param = new XML(param);
	
	param = comun.borraElementosSinHijos(param.toXMLString());

	//log.info("Lo que enviamos preparado : "+param);

	return param;
	
}
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

var TIPOVENTA_ABONOS = '1';
var TIPOVENTA_BONOS = '2';
var TIPOVENTA_NUMERADAS = '3';
var TIPOVENTA_NONUMERADAS = '4';

function handle(request, response) {
	
	// pasaremos la lista de ventas en su estado final, el servicio
	// se encargara del procesado.
	
	var listaventas = request.getParameter('listaventas');
	var motivo = request.getParameter('motivo');
	
	var methodpost = 'rectificarFacturasVenta';
	
	var envio;
	var resultado;
	
	if (log.isInfoEnabled()){
		log.info("Entrada a rectificarFactura.");
	}
	
	if (log.isDebugEnabled()){
		log.debug("rectificarFactura, listaventas: " + listaventas);
	}
	
	listaventas = new XML(listaventas);
	motivo = new XML(motivo);
	
	envio = <servicio>
				<parametro>
					{listaventas}
					{motivo}
				</parametro>
			</servicio>
		
	
	envio = new XML(envio);
	if (log.isDebugEnabled()){
		log.debug("rectificarFactura, envio antes preProcessXML: " + envio.toXMLString());
	}
	envio = preProcessXML(envio);
	
	if (log.isDebugEnabled()){
		log.debug("rectificarFactura, envio: " + envio.toXMLString());
	}

	resultado = services.process(methodpost+'SOAWrapper',envio);

	if (resultado == null){
		if (log.isInfoEnabled()) {
			log.info("La factura es nula!");
		}
		resultado = '<error/>';
	}
	
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado);
}



function preProcessXML(param) {

	delete param..importetotalventa;
	delete param..nombrecompleto;
	delete param..perfiles;
	delete param..importeTarifaSeleccionada;
	delete param..recintounidadnegocios;
	delete param..tipoprodproductos;
	delete param..sesion.nombre;
	//delete param.parametro.list.Venta.lineadetalles.Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.producto;
	delete param..zonasesion.producto;
	delete param..Lineadetalle.detalles;
	/* [15/03/2011 - SSVG - FA07-1] - Se eliminan los nodos de la modificacion que no interesan*/
	delete param..modificacions.Modificacion.venta
	delete param..modificacions.Modificacion.modificacionlineadetalles
	/* [15/03/2011 - SSVG - FA07-1] - Fin modificacion*/


	for each (venta in param.parametro.list.Venta){
		for each (lineadetalle in venta..Lineadetalle){
			// los NaN se sustituyen en la interfaz por "--", que limpiamos
			if (lineadetalle.importe.text() == "--"){
				delete lineadetalle.importe;
			}
			
			if (venta.tipoventa.idtipoventa.text() == TIPOVENTA_BONOS){
				// renombro Lineadetalle a Lineadetallebonoparam para cada venta,
				// aunque todas las ventas lanzadas juntas deberían ser del mismo tipo.
				lineadetalle.setName("Lineadetallebonoparam");
			}
		}
	}
	
	
	//param = comun.deleteSelectedTag(param.toXMLString());
	var xmlSinHijos= comun.borraElementosSinHijos(param);

	return xmlSinHijos;
	
}
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

var TIPO_MODIFICACION_ANULACION = 0;
var TIPO_MODIFICACION_INSERCION_NUEVA = 1;
var TIPO_MODIFICACION_CAMBIO_CANJE_BONOS = 2;
	
function handle(request, response) {
	
	// pasaremos la lista de ventas en su estado final, el servicio
	// se encargara del procesado.
	
	if (log.isInfoEnabled()){
		log.info("JMARIN-->Entrada a rectificarFacturaBonosCredito.action");
	}
	
	var idfactura = request.getParameter('idfactura');
	var motivo = request.getParameter('motivo');
	var listabonosoriginal = request.getParameter('listabonosoriginal');
	var listabonosfinal = request.getParameter('listabonosfinal');
	var listabonosmodificados = request.getParameter('listabonosmodificados');
	
	var methodpost = 'rectificarFacturaBonosCredito';
	
	var envio;
	var resultado;
	
	
	idfactura = new XML(idfactura);
	motivo = new XML(motivo);
	listabonosoriginal = new XML(listabonosoriginal);
	listabonosfinal = new XML(listabonosfinal);
	listabonosmodificados = new XML(listabonosmodificados);
	
	
	if (log.isDebugEnabled()){
		log.debug("JMARIN-->rectificarFacturaBonosCredito, idfactura: " + idfactura);
		log.debug("JMARIN-->rectificarFacturaBonosCredito, motivo: " + motivo);
		log.debug("JMARIN-->rectificarFacturaBonosCredito, listabonosoriginal: " + listabonosoriginal);
		log.debug("JMARIN-->rectificarFacturaBonosCredito, listabonosfinal: " + listabonosfinal);
		log.debug("JMARIN-->rectificarFacturaBonosCredito, listabonosmodificados: " + listabonosmodificados);
	}
	
	
	var arrayIdcanjebonosToAdd = obtenerListaCanjebonosToAdd(listabonosfinal, listabonosoriginal);
	var arrayIdcanjebonosToRemove = obtenerListaCanjebonosToRemove(listabonosfinal, listabonosoriginal);
	
	
	var Rectificarfacturabonoscreditoparam = new XML();
	Rectificarfacturabonoscreditoparam = <Rectificarfacturabonoscreditoparam>
												<idfactura>{idfactura}</idfactura>
												<motivorectificacion>{motivo}</motivorectificacion>
												<modificacionfacturabonocreditos/>
										 </Rectificarfacturabonoscreditoparam>;
	
		
	
	// bonos a añadir
	if (arrayIdcanjebonosToAdd.length>0){
		var mfbc = crearModificacionFacturaBonoCreditos_Add_Rem(arrayIdcanjebonosToAdd,TIPO_MODIFICACION_INSERCION_NUEVA);
		Rectificarfacturabonoscreditoparam.modificacionfacturabonocreditos.* += mfbc;
	}
	
	// bonos a eliminar
	if (arrayIdcanjebonosToRemove.length>0){
		var mfbc = crearModificacionFacturaBonoCreditos_Add_Rem(arrayIdcanjebonosToRemove,TIPO_MODIFICACION_ANULACION);
		Rectificarfacturabonoscreditoparam.modificacionfacturabonocreditos.* += mfbc;
	}
	
	// bonos a modificar
	// (length() funcion y no propiedad por ser lista xml)
	if (listabonosmodificados.*.length()>0){
		var mfbc = crearModificacionFacturaBonoCreditos_Mod(listabonosmodificados);
		Rectificarfacturabonoscreditoparam.modificacionfacturabonocreditos.* += mfbc;
	}
	
	
	envio = <servicio>
				<parametro>
					{Rectificarfacturabonoscreditoparam}
				</parametro>
			</servicio>;
			
	envio = new XML(envio);
	envio = preProcessXML(envio);
	
	if (log.isInfoEnabled()){
		log.info("rectificarFacturaBonosCredito, envio: " + envio.toXMLString());
	}


	resultado = services.process(methodpost+'SOAWrapper',envio);
					

	if (resultado == null){
		if (log.isInfoEnabled()) {
			log.info("La factura es nula!");
		}
		resultado = '<error/>';
	}


	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado);
}



function preProcessXML(param) {

	delete param..importetotalventa;
	delete param..nombrecompleto;
	delete param..perfiles;
	delete param..importeTarifaSeleccionada;
	delete param..recintounidadnegocios;
	delete param..tipoprodproductos;
	delete param..sesion.nombre;
	//delete param.parametro.list.Venta.lineadetalles.Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.producto;
	delete param..zonasesion.producto;
	delete param..Lineadetalle.detalles;
	delete param..Entradatornos;
	delete param..Lineafactura;
	delete param..zonasesions.Zonasesion;
	
	//param = comun.deleteSelectedTag(param.toXMLString());
	var xmlSinHijos= comun.borraElementosSinHijos(param);

	return xmlSinHijos;
	
}



// **
// * Para añadir y quitar
// *
function crearModificacionFacturaBonoCreditos_Add_Rem(listacanjebonos, tipomodificacion){

	if (log.isInfoEnabled()){
		log.info("entrada a crearModificacionFacturaBonoCreditos con listaidcanjebonos, tipomodificacion.");
	}
	
	var Modificacionfacturabonocredito = new XML();
	
	Modificacionfacturabonocredito = 
		<Modificacionfacturabonocredito>
			<canjebonos/>
			<tipomodificacion>{tipomodificacion}</tipomodificacion>
		</Modificacionfacturabonocredito>;
		
	for (var i=0; i<listacanjebonos.length; i++){
		Modificacionfacturabonocredito.canjebonos.* += listacanjebonos[i];
	}
	
	return Modificacionfacturabonocredito;
}



// **
// * Para cambios de canje
// *
function crearModificacionFacturaBonoCreditos_Mod(listabonos){

	if (log.isInfoEnabled()){
		log.info("entrada a crearModificacionFacturaBonoCreditos con listabonos.");
	}
	
	var Modificacionfacturabonocredito = new XML();
	
	Modificacionfacturabonocredito = 
		<Modificacionfacturabonocredito>
			<canjebonos/>
			<tipomodificacion>{TIPO_MODIFICACION_CAMBIO_CANJE_BONOS}</tipomodificacion>
		</Modificacionfacturabonocredito>;
		
	for each (var bono in listabonos.Bono){
		Modificacionfacturabonocredito.canjebonos = bono.canjebonos;
	}
	
	return Modificacionfacturabonocredito;
}



function obtenerListaCanjebonosToAdd(listabonosfinal, listabonosoriginal){
	var arrayCanjebonosToAdd = [];
	
	for each (var bonofinal in listabonosfinal.Bono){
		var encontrado = false;
		for each (var bonooriginal in listabonosoriginal.Bono){
			if (bonofinal.idbono.text() == bonooriginal.idbono.text()){
				encontrado = true;
			}
		}
		
		if (!encontrado) arrayCanjebonosToAdd.push(bonofinal.canjebonos[0].Canjebono);
	}
	
	return arrayCanjebonosToAdd;
}



function obtenerListaCanjebonosToRemove(listabonosfinal, listabonosoriginal){
	var arrayCanjebonosToRemove = [];
	
	for each (var bonooriginal in listabonosoriginal.Bono){
		var encontrado = false;
		for each (var bonofinal in listabonosfinal.Bono){
			if (bonooriginal.idbono.text() == bonofinal.idbono.text()){
				encontrado = true;
			}
		}
		
		if (!encontrado) arrayCanjebonosToRemove.push(bonooriginal.canjebonos[0].Canjebono);
	}
	
	return arrayCanjebonosToRemove;
}
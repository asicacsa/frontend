var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}

	// si xml no se envia, llama al metodo sin parametros	
	var xml = request.getParameter('xml');
	if(xml == null ) {
		xml = '<servicio />';
	} else {
		var xmlSinHijos;
		var sbf = context.beans.getBean('scriptBeanFactory');
		var comun = sbf.getBean('transformerXML.js');
		
		xmlSinHijos = comun.borraElementosSinHijos(xml);
		xmlSinHijos = this.preProcess(xmlSinHijos);
		
		if (xmlSinHijos.name() == "parametro") {
			xml = <servicio>
					{xmlSinHijos}
				  </servicio>
		} else {
			xml = <servicio>
					<parametro>
						{xmlSinHijos}
					</parametro>
				  </servicio>
		}
		
	}
	
	var result = null;
	try {
		result = services.process(methodpost+'SOAWrapper',xml);
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost);
			log.error("LOG(ERROR) : con esta llamada: " + xml);
			log.error('LOG(ERROR) : con este tipo de error : ' + ex);
			log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
		}
		
		throw ex.javaException;
	
	}
	
	if (result == null) {
		result = "<ArrayList />";
	}
	
	if (log.isDebugEnabled()) {
		log.debug("defult.action accesing " + methodpost);
		log.debug("		result: " + result.substr(0,300) + "[...]");
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	
	response.writer.println(result);
}

function preProcess(param){
	//Elimino las propiedades calculadas que no existen en la base de datos
	delete param.list.Factura..nombrecompleto;
	delete param.list.Factura..importetotal;
	delete param.list.Factura..anulada;
	delete param.list.Factura..serieynumero;
	
	delete param.list.Facturarappel..nombrecompleto;
	delete param.list.Facturarappel..importetotal;
	delete param.list.Facturarappel..anulada;
	delete param.list.Facturarappel..serieynumero;
	
	return param;
}
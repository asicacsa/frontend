var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
	if (log.isInfoEnabled()) {
		log.info('Entrada a rectificarFacturaPorCliente.action');
	}	
	
	var idfactura = request.getParameter('idfactura');	
	var idcliente = request.getParameter('idcliente');
	var motivo = request.getParameter('motivo');
	
	if (log.isInfoEnabled()) {
		log.info('rectificarFacturaPorCliente.action, idfactura :' + idfactura);
		log.info('rectificarFacturaPorCliente.action, idcliente: ' + idcliente);
	}
	
	var resultado;
	var methodpost = request.getParameter('servicio');
	var envio;
		
	
	envio = 
		<servicio>
			<parametro>
				<long>{idfactura}</long>
				<int>{idcliente}</int>
				<motivo>{motivo}</motivo>
			</parametro>
		</servicio>

	if (log.isInfoEnabled()) {
		log.info("rectificarFacturaPorCliente, envio: " + envio);	
	}
		
	
	var aux;
	
	try{
		aux = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	
	
	if(aux != null){
		//aux = new XML(aux);
		resultado = aux;
			
		if (log.isInfoEnabled()) {	
			log.info("rectificarFacturaPorCliente, resultado: " + resultado);
		}
	} else {
		if (log.isInfoEnabled()) {
			log.info("rectificarFacturaPorCliente, resultado nulo.");
		}
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado);
}



function preProcessXML(param) {
	param = new XML(param);
	param = comun.borraElementosSinHijos(param.toXMLString());

	return param;
}
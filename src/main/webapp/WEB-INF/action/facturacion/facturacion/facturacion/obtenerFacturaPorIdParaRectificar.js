var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');
var dates = sbf.getBean('date.js');

var TIPOVENTA_ABONOS = '1';
var TIPOVENTA_BONOS = '2';
var TIPOVENTA_NUMERADAS = '3';
var TIPOVENTA_NONUMERADAS = '4';

function handle(request, response) {

	// el action hará una llamada a dos servicios: por un lado, obtiene datos de la factura.
	// Por otro, obtiene las ventas (o bonos, en función del tipo de venta) para esa factura. 
	// Combinará los dos resultados a la salida.

	
	var idFactura = request.getParameter('idFactura');
	
	var mthodObtencionfactura = 'obtenerFacturaPorIdParaRectificar';
	var mthodObtencionventas = 'obtenerVentasPorIdFactura';
	var mthodObtencionbonos = 'obtenerBonosPorIdFactura';
	
	var envio;
	var factura;
	var ventas;
	var bonos;
	var resultado;
	
	if (log.isInfoEnabled()){
		log.info("Entrada a obtenerFacturaPorIdParaRectificar, idFactura: " + idFactura);
	}
	
	
	if (idFactura!=null) {
		idFactura = new XML(idFactura);

		envio = 
			<servicio>
				<parametro>
					<java.lang.Long>{idFactura}</java.lang.Long>
				</parametro>
			</servicio>			
		
		if (log.isDebugEnabled()) {
			log.debug("obtenerFacturaPorIdParaRectificar, envio: " + envio);
		}
		
		// llamada al servicio de obtención de factura
		factura = services.process(mthodObtencionfactura+'SOAWrapper',envio).trim();
		
		// @TODO: puede llamarse al servicio de obtencion de ventas o bonos
		// condicionalmente, en funcion de los datos de factura obtenidos.
		if (log.isDebugEnabled())
		{
			log.debug("JMARIN-->mthodObtencionfactura: llamada al servicio de obtencion de ventas");
		}
		
		// llamada al servicio de obtención de ventas
		ventas = services.process(mthodObtencionventas+'SOAWrapper',envio).trim();
		
		
		// llamada al servicio de obtención de bonos
		if (log.isDebugEnabled())
		{
			log.debug("JMARIN-->mthodObtencionfactura: llamada al servicio de obtencion de bonos");
		}
		bonos = services.process(mthodObtencionbonos+'SOAWrapper',envio);
		
		
		
		if(factura != null){
			factura = new XML(factura);
			
			// @TODO: idem, podemos llamar a un metodo u otro condicionalmente
			// para optimizar.
			if (log.isDebugEnabled())
			{
				log.debug("JMARIN-->mthodObtencionfactura: llamada al combinaFacturaYVentas");
			}

			ventas = new XML(ventas);
			resultado = combinaFacturaYVentas(factura, ventas);
			
			if (log.isDebugEnabled())
			{
				log.debug("JMARIN-->mthodObtencionfactura: llamada al combinaFacturaYBonos");
			}
			
			bonos = new XML(bonos);
						
			resultado = combinaFacturaYBonos(factura, bonos);
			
			if (log.isDebugEnabled()) {
				//log.info("obtenerFacturaPorIdParaRectificar, resultado final: " + resultado.toXMLString().substr(0,500) + '[...]');
				log.debug("obtenerFacturaPorIdParaRectificar, resultado final: " + resultado.toXMLString());
			}
			
		} else {
			log.error("La factura es nula!");
			resultado = <error/>;
		}
					
	} else {
		log.error("El xml que llega a obtenerFacturaPorId es nulo!!!");
		resultado = <error/>;
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}


function preProcessXML(param) {		
	param = comun.deleteSelectedTag(param.toXMLString());
	var xmlSinHijos= comun.borraElementosSinHijos(param);

	return xmlSinHijos;
}


function combinaFacturaYVentas(factura, ventas){
	// en pantalla de rectificación mostramos lineas de detalle tanto de
	// las ventas de la factura, como de las ventas asociadas a las facturas rectificativas
	// asociadas a la factura principal. Para facilitar el mostrado en pantalla,
	// agrupamos todas las ventas en un mismo tag.

	if (typeof(factura.ventasagrupadas)=='null' || typeof(factura.ventasagrupadas)=='undefined'){
		factura.ventasagrupadas = <ventasagrupadas/>;
	}

	if (typeof(factura.ventasdetalladasagrupadas)=='null' || typeof(factura.ventasdetalladasagrupadas)=='undefined'){
		factura.ventasdetalladasagrupadas = <ventasdetalladasagrupadas/>;
	}

	for each (var venta in ventas.Venta){
;
		// el detalle a mostrar para cada linea depende del tipo de venta.
		// Añadimos campo "detalles" a cada linea con el contenido oportuno.
		var idtipoventa = venta.tipoventa.idtipoventa.text();
		for each (var lineadetalle in venta.lineadetalles.Lineadetalle){
			lineadetalle = insertarCampoDetalles(lineadetalle, idtipoventa);
		}

		//postproceso
		if (log.isInfoEnabled()) {
			log.info("obtenerFacturaPorIdParaRectificar, venta antes de postproceso: " + venta);
		}
		venta = postProceso(venta);

		
		// ventas con detalles
		factura.ventasdetalladasagrupadas.* += venta;
		

		// ventas sin detalles
		delete venta..lineadetalles;
		factura.ventasagrupadas.* += venta;

		
	}

	return factura;
}



function combinaFacturaYBonos(factura, bonos){
	// en pantalla de rectificación mostramos lineas de detalle tanto de
	// los bonos de la factura, como de los bonos asociados a las facturas rectificativas
	// asociadas a la factura principal. Para facilitar el mostrado en pantalla,
	// agrupamos todos los bonos en un mismo tag.
	
	//var BB = sbf.getBean('buscarBonos.js');
	
	if (typeof(factura.bonosagrupados)=='null' || typeof(factura.bonosagrupados)=='undefined'){
		factura.bonosagrupados = <bonosagrupados/>;
	}
	
	for each (var bono in bonos.Bono){
		//BB.evaluateWichCanjebonoIsShowable(bono);
		evaluateWichCanjebonoIsShowable(bono);
		
		factura.bonosagrupados.* += bono;
	}
	
	return factura;
}


	
function evaluateWichCanjebonoIsShowable(bono){

	// Habra que mostrar el canje con fecha de canje <= fecha facturacion > fecha anulacion, 
	// es decir, los canjes que existian cuando se genero la factura. 
	// Si hay varios (anulados o sin anular) habra que mostrar el de menor fecha de canje.
	// Lo marcaremos como 'showable'.

	if (log.isDebugEnabled()){
		log.debug("->evaluateWichCanjebonoIsShowable, bono: " + bono.toXMLString());
	}
	
	var fechageneracion = bono.lineacanje.lineafacturas.Lineafactura.(@anulada=='0').factura.fechageneracion.text();
	var fechafacturacion = dates.stringToDate(fechageneracion, "dd/MM/y-HH:mm:ss");
	
	if (log.isDebugEnabled()){
		log.debug("evaluateWichCanjebonoIsShowable, fechafacturacion: " + fechafacturacion);
	}
	
	var canjecandidato;
	var fechacanje;
	var fechaanulacion;
	
	for each (var canjebono in bono.canjebonos.Canjebono){
	
		// no consideramos la hora, ya que no aparece en la fecha de facturación.
		
		fechacanje = canjebono.fechayhoracanje.text().toXMLString();
		fechaanulacion = canjebono.fechayhoraanulacion.text().toXMLString();
		
		if (fechacanje!=null && fechacanje!=""){
			fechacanje = fechacanje.split("-")[0] + "-00:00:00";
			fechacanje = dates.stringToDate(fechacanje, "dd/MM/y-HH:mm:ss");
		}
		
		if (fechaanulacion!=null && fechaanulacion!=""){
			fechaanulacion = fechaanulacion.split("-")[0] + "-00:00:00";
			fechaanulacion = dates.stringToDate(fechaanulacion, "dd/MM/y-HH:mm:ss");
		}
		
		if (log.isDebugEnabled()){
			log.debug("evaluateWichCanjebonoIsShowable, idcanjebono: " + canjebono.idcanjebono.text());
			log.debug("evaluateWichCanjebonoIsShowable, fechacanje: " + fechacanje);
			log.debug("evaluateWichCanjebonoIsShowable, fechaanulacion: " + fechaanulacion);
		}
		
		// NOTA: si no esta anulado, canjebono.fechayhoraanulacion es nulo, y el Date 
		// fechaanulacion es 1-1-1970 (por definicion), por lo que en ese caso 
		// fechafacturacion > fechaanulacion siempre sera true.
		
		if (fechacanje <= fechafacturacion && fechafacturacion > fechaanulacion){
		
			if (canjecandidato == null){
				canjecandidato = canjebono;
				
				if (log.isDebugEnabled()){
					log.debug("evaluateWichCanjebonoIsShowable, canjecandidato: " + canjecandidato.idcanjebono.text());
				}
			} else {
				// el canje es el nuevo candidato, si su fecha es anterior al del candidato anterior.
				var fechacanjecandidato = dates.stringToDate(canjecandidato.fechayhoracanje.text(), "dd/MM/y-HH:mm:ss");
				
				if (log.isDebugEnabled()){
					log.debug("evaluateWichCanjebonoIsShowable, fechacanjecandidato: " + fechacanjecandidato);
				}
		
				if (fechacanje < fechacanjecandidato){
					canjecandidato = canjebono;
					
					if (log.isDebugEnabled()){
						log.debug("evaluateWichCanjebonoIsShowable, nuevo canjecandidato: " + canjecandidato.idcanjebono.text());
					}
				}
			}
			
		}
		
	}
	
	canjecandidato.@showable = 1;

}



function insertarCampoDetalles(lineadetalle, idtipoventa){
	// el detalle a mostrar para cada linea depende del tipo de venta.
	// Añadimos campo "detalles" a cada linea con el contenido oportuno.
	lineadetalle.detalles = <detalles/>;
	
	//var tipoventa = lineadetalle.venta.tipoventa.idtipoventa.text();
	var tipoventa = idtipoventa;
	
	if (tipoventa == TIPOVENTA_ABONOS){
		var tipoabono = lineadetalle.abonos[0].Abono.tipoabono.nombre.text();
		lineadetalle.detalles.* += <Detalle>{tipoabono}</Detalle>;
	}
	
	if (tipoventa == TIPOVENTA_BONOS){
		var bonos;
		if ( lineadetalle.bonosForLineacanje.bonosForLineacanje[0] != null ) {
			bonos = lineadetalle.bonosForLineacanje[0];
			if ( log.isInfoEnabled() ) log.info('Asignando el bono desde bonosForLineacanje con el contendo ' + lineadetalle.bonosForLineacanje.bonosForLineacanje[0] );
		} else {
			bonos = lineadetalle.bonosForIdlineadetalle[0];
			if ( log.isInfoEnabled() ) log.info('Asignando el bono desde bonosForIdlineadetalle con el contendo ' + lineadetalle.bonosForLineacanje.bonosForIdlineadetalle[0] );
		}
		//for each(var bono in lineadetalle.bonosForLineacanje[0].Bono){
		//for each(var bono in lineadetalle.bonosForIdlineadetalle[0].Bono){
		if (bonos != null) {
			for each(var bono in bonos.Bono){
				var nombreproducto = bono.producto.nombre.text();
				lineadetalle.detalles.* += <Detalle>{nombreproducto}</Detalle>;
			}
		}
	}
	
	if (tipoventa == TIPOVENTA_NUMERADAS){
		var contenidoysesion = lineadetalle.lineadetallezonasesions[0].Lineadetallezonasesion.zonasesion.sesion.nombre.text();
		lineadetalle.detalles.* += <Detalle>{contenidoysesion}</Detalle>;
	}
	
	if (tipoventa == TIPOVENTA_NONUMERADAS){
		for each(var lineadetallezonasesion in lineadetalle.lineadetallezonasesions.Lineadetallezonasesion){
			var contenidoysesion = lineadetallezonasesion.zonasesion.sesion.nombre.text();
			lineadetalle.detalles.* += <Detalle>{contenidoysesion}</Detalle>;
		}
	}
	
	
	return lineadetalle;
}


function postProceso(xmlventa) {
	var ovpicp = sbf.getBean('obtenerVentaPorIdConPerfiles.js');
	var ovpipbcp = sbf.getBean('obtenerVentaPorIdPopupBonoConPerfiles.js');
	
	var idtipoventa = xmlventa.tipoventa.idtipoventa.text();
	var result;
	if (idtipoventa == TIPOVENTA_BONOS){
		// procesado de ventas de bonos prepago
		if (log.isInfoEnabled()) {
			log.info("obtenerFacturaPorIdParaRectificar, postprocesado de venta de bonos.");
		}
		result = ovpipbcp.postProceso(xmlventa);
	} else {
		// procesado normal
		if (log.isInfoEnabled()) {
			log.info("obtenerFacturaPorIdParaRectificar, postprocesado de venta de productos.");
		}
		result = ovpicp.postProceso(xmlventa);
	}
	
	return result;
}
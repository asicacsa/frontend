var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {

	if (log.isInfoEnabled()) {
		log.info('Entrada a crearRectificativaPorIncluirExcluirVentas.action');	
	}
	
	var idfactura = request.getParameter('idfactura');	
	var ventas = request.getParameter('ventas');
	var motivo = request.getParameter('motivo');
	
	if (log.isInfoEnabled()) {
		log.info('crearRectificativaPorIncluirExcluirVentas.action, idfactura :' + idfactura);
		log.info('crearRectificativaPorIncluirExcluirVentas.action, ventas: ' + ventas);
	}
	
	var resultado;
	var methodpost = request.getParameter('servicio');
	var envio;
			
		
	ventas = this.preProcessXML(ventas);
	
	envio = 
		<servicio>
			<parametro>
				<long>{idfactura}</long>
				{ventas}
				<motivo>{motivo}</motivo>
			</parametro>
		</servicio>

	if (log.isInfoEnabled()) {
		log.info("crearRectificativaPorIncluirExcluirVentas, envio: " + envio);	
	}
		
	var aux;
	
	try{
		aux = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	
	if(aux != null){
		//aux = new XML(aux);
		resultado = aux;
			
		if (log.isInfoEnabled()) {	
			log.info("crearRectificativaPorIncluirExcluirVentas, resultado: " + resultado);
		}
	} else {
		if (log.isInfoEnabled()) {
			log.info("crearRectificativaPorIncluirExcluirVentas, resultado nulo.");
		}
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado);
}



function preProcessXML(param) {
	
	param = new XML(param);
	
	param = comun.borraElementosSinHijos(param.toXMLString());

	for each (var venta in param.Venta){
		delete venta.importetotalventa;
		delete venta.cliente;
		delete venta.lineadetalles;
		//venta.setName("Venta");
	}

	return param;
	
}
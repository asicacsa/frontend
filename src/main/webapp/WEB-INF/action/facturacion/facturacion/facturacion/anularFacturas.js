// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	//log.info("Entrada a anularFacturas.action");
	//GGL AVANQUA 11.09.2015
	//Hemos creado un nuevo servicio para anular las facturas y sus gemelas de entidades gestoras distintas 
	//var methodpost = request.getParameter('servicio');
	var methodpost = "anularFacturasYSusGemelas";
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	var motivo = request.getParameter('motivo');
	var envio = <servicio/>;
	
	if (xml != null && motivo != null){
		envio.appendChild(<parametro/>);
		
		if (xml != null){
			xml = new XML(xml);
			envio.parametro.appendChild(xml);
		}
		
		if (motivo != null){
			motivo = new XML(motivo);
			envio.parametro.appendChild(motivo);
		}
	}
	
	if (log.isInfoEnabled()) {
		log.info("Envio al servicio anularFacturasYSusGemelas: " + envio.toXMLString());
	}
	
	var result =null;
	
	try{
		result = services.process(methodpost+'SOAWrapper',envio.toXMLString());
	} catch (ex) {
			if (log.isErrorEnabled()) {	
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				log.error('LOG(ERROR) : con esta llamada : ' + envio);
				log.error('LOG(ERROR) : result in this Exception : '+ex);
			}
			
			throw ex.javaException;
	}
		
	if (log.isInfoEnabled()) {
		log.info("Resultado del servicio anularFacturasYSusGemelas: " + result);
	}
	//var respuesta = new XML(service.process(methodpost,envio));
	//var respuesta = <ok/>;
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result);
}
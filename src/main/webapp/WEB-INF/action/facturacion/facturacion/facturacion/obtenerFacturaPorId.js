var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
		
	if (log.isInfoEnabled()) {
		log.info("Entrada a obtenerFacturaPorId.action.");
	}
	
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost = 'obtenerFacturaPorId';
	var envio;
	
	var idFactura = request.getParameter('idFactura');
	
	if (idFactura!=null) {
		idFactura = new XML(idFactura);
		
		if (log.isInfoEnabled()) {
			log.info("obtenerFacturaPorId, idfactura de entrada: " + idFactura.toXMLString());	
		}

		envio = 
			<servicio>
				<parametro>
					<java.lang.Long>{idFactura}</java.lang.Long>
				</parametro>
			</servicio>			
		
		if (log.isInfoEnabled()) {
			log.info("obtenerFacturaPorId, envio: " + envio);
		}
		
		var aux;
		
		try{
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
		
		if (log.isInfoEnabled()) {
			log.info("obtenerFacturaPorId, respuesta del servicio: " + aux);
		}
				
		if(aux != null){
			resultado = new XML(aux);
		}			
		
					
	} else {
		if (log.isInfoEnabled()) {
			log.info("obtenerFacturaPorId, xml de entrada es nulo!!!");
		}
		resultado = <error/>
	}
	
	
	if (log.isInfoEnabled()) {
		log.info("obtenerFacturaPorId, resultado final: " + resultado.toXMLString());
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}



function preProcessXML(param) {		
	param = comun.deleteSelectedTag(param.toXMLString());
	var xmlSinHijos= comun.borraElementosSinHijos(param);

	return xmlSinHijos;
}
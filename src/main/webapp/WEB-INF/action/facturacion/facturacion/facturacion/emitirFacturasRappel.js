importPackage(Packages.org.springframework.web.servlet);
//var forward = 'http://localhost:8080/services/JasperController?report=FacturaEmitida&format=pdf&idFactura=1';
function handle(request, response) {
	//log.info("LOG(INFO) : Llamada a emitirFacturas.");		
	
	var forward = 'jasper.post?report=factura_rappel&format=pdf';
	//var forward = 'jasper.post?report=FacturaEmitida&format=pdf';
	
	//var idfactura = request.getParameter('idFactura');
	
	var idFacturaRappel = request.getParameter('idfacturarappel');
	
	var viewOnly = request.getParameter('viewOnly');
	
	
	log.info('YA en el JS de EMITIR FACTURA RAPPEL: '+idfacturarappel);
	
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : parametro idFactura:" + request.getParameter('idfacturarappel'));	
	}
	
	//forward = forward + '&idFactura=' + idfactura;
	forward = forward + '&idfacturarappel=' + idFacturaRappel;
	
	if (typeof(viewOnly)!='null' && typeof(viewOnly)!='undefined' && viewOnly!=''){
		forward = forward + '&viewOnly=' + viewOnly;
	}
	
	response.sendRedirect(forward);
	return false;
}

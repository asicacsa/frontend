var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaDistribuidora';
//var servicio_edicion ='actualizarFormasPago';


function handle(request, response) {

		
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost;
	var envio;
	
	if (xml!=null) {
		xml = new XML(xml);
		
		if (log.isInfoEnabled()) {
			log.info("XML de entrada a postdarDeAltaDistribuidora.action: "  +  xml);
		}
		
		xml = this.preProcessXML(xml);
		
		methodpost = servicio_alta;
		envio = 
			<servicio>
				<parametro>
					{xml}
				</parametro>
			</servicio>
		
		
		if (log.isInfoEnabled()) {
			log.info("Este es el envio darDeAltaDistribuidora: "+envio);
			log.info("Este es el metodo post : "+methodpost);
		}
		
		//Y llamamos al servicio...		
		var aux;
		
		try{
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
		 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		 }
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		
		resultado = 
				<ok>
					{aux}
				</ok>
		//En aux tenemos la respuesta			
	} else {
		if (log.isInfoEnabled()) {
			log.info("No hay datos que procesar en postdarDeAltaDistribuidora.action");
		}
		resultado = <error/>
	}
	
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...

	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('transformerXML.js');
	
	if(param.Distribuidora.nombre.text().length() == 0){
		delete param.Distribuidora.nombre;
	}
	
	return param.Distribuidora;
	
}
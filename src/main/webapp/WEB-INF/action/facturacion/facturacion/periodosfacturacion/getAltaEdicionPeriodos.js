var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	var xml = request.getParameter('xml');	
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var resultado;
		
	if (xml!=null) {
		
		var envio = <servicio>
						<parametro>
							<int>
								{xml}
							</int>
						</parametro>
					</servicio>
		
		try {			
			aux = services.process(methodpost+'SOAWrapper',envio);					
		} catch (ex) {
		 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		 }
		 
		//log.info('Este es el valor que retorna : '+new XML(aux).toXMLString())
		if (aux != null) {
			resultado = new XML(aux);
			resultado = this.postProcessXML(resultado);
		}	
	} else {
	
		modelstring = request.getAttribute('__modelo');

		var envio = <servicio>
						<parametro>
							<Periodofacturacion/>							
						</parametro>
					</servicio>

		//log.info("Este es el xml que enviamos para el get modelo: "+envio.toXMLString());		
		resultado = services.process('getXMLModelSOAWrapper',envio);
		//log.info('Devuelto el modelo  : '+new XML(resultado).toXMLString())
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado);
}


function postProcessXML(xmlparam) {	
	//Coloco las etiquetas que necesitamos y que no vienen de la base de datos	
	xmlparam.diamensual=<diamensual/>
	xmlparam.mesmensual=<mesmensual/>
	xmlparam.cadameses=<cadameses/>
	//si el tipo de periodo de facturación es "mensual" los campos dia y nummeses pasarán a ser diamensual y mesmensual respectivamente
	if (xmlparam.tipoperiodofacturacion.idtipoperiodofacturacion == 3){
		xmlparam.diamensual=<diamensual>{xmlparam.dia.text()}</diamensual>
		xmlparam.dia = <dia/>
		xmlparam.mesmensual=<mesmensual>{xmlparam.nummeses.text()}</mesmensual>
		xmlparam.nummeses = <nummeses/>
	}
	else{
		if (xmlparam.tipoperiodofacturacion.idtipoperiodofacturacion == 4){
			xmlparam.cadameses=<cadameses>{xmlparam.nummeses.text()}</cadameses>
			xmlparam.nummeses = <nummeses/>
		}
	}
	return xmlparam;	
}
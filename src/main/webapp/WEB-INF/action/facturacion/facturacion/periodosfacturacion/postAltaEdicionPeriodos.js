var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaPeriodoFacturacion';
var servicio_edicion ='actualizarPeriodosFacturacion';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost;
	var envio;
	
	if (log.isInfoEnabled()){
		log.info("Entrada a postAltaEdicionPeriodos.action");
	}
	
	if (xml!=null) {

		xml = new XML(xml);
		xml = xml.Periodofacturacion;
		
		if(methodpost == null) {
			// ALTA
			if (xml.idperiodofacturacion.text().length() == 0) {
				methodpost = servicio_alta;
				xml = this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							{xml}
						</parametro>
					</servicio>
					
			// EDICION	
			} else {
				methodpost = servicio_edicion;
				xml= this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							<list>
								{xml}
							</list>
						</parametro>
					</servicio>			
			}
		}		
		
		if (log.isInfoEnabled()){
			log.info("Este es el envio : "+envio);
		}

		var aux;
		//Y llamamos al servicio...
		
		try {
		 	aux = new XML(services.process(methodpost+'SOAWrapper',envio));
		 } catch (ex) {
		 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		 }
	
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		resultado = 
				<ok>
					{aux}
				</ok>
					
	} else {
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {

	//Datos obligatorios que no pueden estar vacios ...
	if (log.isInfoEnabled()){
		//log.info("Lo que vamos a pasar por preProcessXML : "+param.toXMLString());
	}
	if(param.diamensual != null && param.diamensual.toString() != ''){
		param.dia = <dia>{param.diamensual.text()}</dia>
		delete param.diamensual;
	}
	if(param.mesmensual != null && param.mesmensual.toString() != ''){
		param.nummeses = <nummeses>{param.mesmensual.text()}</nummeses>
		delete param.mesmensual;
	}
	if(param.cadameses != null && param.cadameses.toString() != ''){
		param.nummeses = <nummeses>{param.cadameses.text()}</nummeses>
		delete param.cadameses;
	}
	param = comun.borraElementosSinHijos(param.toXMLString());
	
	return param;
	
}

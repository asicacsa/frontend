//var service = context.beans.getBean('httpServiceSOA');

// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = request.getParameter('servicio');
	
	
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	if(xml != null ) {
		xml = new XML(xml);
		xml = this.preProcessXML(xml);
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>	
	}
	
	var respuesta = services.process(methodpost+'SOAWrapper',envio);
	
	
	if (respuesta!=null) {
		respuesta = new XML(respuesta);
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
	
}
function preProcessXML(param) {
	
	param = comun.borraElementosSinHijos(param.toXMLString());
	
	param = comun.deleteSelectedTag(param.toXMLString());
	param = new XML(param);
	
	return param;
	
}
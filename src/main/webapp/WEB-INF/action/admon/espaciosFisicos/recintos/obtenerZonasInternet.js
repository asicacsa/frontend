var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {

	if (log.isInfoEnabled()) {
		log.info("entrada a obtenerZonasInternet.action");
	}
	
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}

	// si xml no se envia, llama al metodo sin parametros	
	var xml = request.getParameter('xml');
	
	if (log.isInfoEnabled()) {
		log.info("obtenerZonasInternet.action, method: " + methodpost);
		log.info("obtenerZonasInternet.action, xml: " + xml);
	}
	
	if(xml == null ) {
		xml = '<servicio/>';
	} else {
		var xmlSinHijos;
		var sbf = context.beans.getBean('scriptBeanFactory');
		var comun = sbf.getBean('transformerXML.js');
		
		xmlSinHijos = comun.borraElementosSinHijos(xml);
		
		if (xmlSinHijos.name() == "parametro") {
			xml = <servicio>
					{xmlSinHijos}
				  </servicio>
		} else {
			xml = <servicio>
					<parametro>
						{xmlSinHijos}
					</parametro>
				  </servicio>
		}
		
	}
	
	if (log.isInfoEnabled()) {
		log.info("obtenerZonasInternet.action, service parameters: " + xml);
	}
	
	var result = null;
	
	try {
		result = new XML(services.process(methodpost+'SOAWrapper',xml));
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost);
			log.error("LOG(ERROR) : con esta llamada: " + xml);
			log.error('LOG(ERROR) : con este tipo de error : ' + ex);
			log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
		}
		
		throw ex.javaException;
	}
	
	if (result != null) {
		if(log.isInfoEnabled()){
			log.info("obtenerZonasInternet.action, service response: " + result.toXMLString().substr(0,300) + "[...]");
		}
	}
	
	result = postProcessXML(result);
	
	if (log.isDebugEnabled()) {
		log.debug("obtenerZonasInternet.action accesing " + methodpost);
		//log.debug("obtenerZonasInternet.action result: " + result.substr(0,300) + "[...]");
		log.debug("obtenerZonasInternet.action result: " + result.toXMLString().substr(0,300) + "[...]");
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	
	response.writer.println(result.toXMLString());
}

function postProcessXML(xml) {
	for each (zona in xml.Zona){
		//Establezco el nombre del recinto asociado a cada zona
		var recinto_zona = zona.recinto.nombre.text() + " - " + zona.nombre.text();
		zona.recinto_zona = <recinto_zona>{recinto_zona}</recinto_zona>		
	} 
	return xml;
}

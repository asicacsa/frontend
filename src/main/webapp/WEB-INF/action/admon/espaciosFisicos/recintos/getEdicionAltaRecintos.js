var services = context.beans.getBean('httpServiceSOA');
var model = <Recinto>
				<nombre></nombre>
				<dadodebaja>false</dadodebaja>
				<maxreimpresiones/>
				<bloqueado/>
				<admitesolapamiento/>
				<horaapertura/>
				<horacierre/>
				<overbooking>false</overbooking>																		
				<recintos/>
				<recintounidadnegocios/>
				<taquillas/>
				<zonas/>
				<orden></orden>
			</Recinto>
			
/**
nombre : String
orden : BigDecimal
dadodebaja : Integer
descripcion : String
idrecinto : Integer
maxreimpresiones : BigDecimal
horaapertura : Date
horacierre : Date
overbooking : BigDecimal
plano : String
recinto : Recinto
contenidos : Set
recintos : Set
recintounidadnegocios : Set
taquillas : Set
zonas : Set
**/
			
function handle(request, response) {
		
	var xml = request.getParameter('xml');	
	var methodpost = request.getParameter('servicio');
	

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var resultado;
	

	
	if (xml!=null) {
		
		var envio = <servicio>
						<parametro>
							<int>
								{xml}
							</int>
						</parametro>
					</servicio>
		
		var aux;
		
		try {
			aux = services.process(methodpost+'SOAWrapper',envio);					
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
		
		aux = aux.trim();
				
		if (aux != null) {
			resultado = new XML(aux);
			if (resultado.hasOwnProperty('Recinto')) {
				resultado = resultado.Recinto;
			} else {
				if (!(resultado.name() == 'Recinto')) {
					log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
					log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
					log.error('LOG(ERROR) : result does not Recinto : '+ex);
					var ex = new java.lang.Exception('Exception calling this service : '+methodpost+'SOAWrapper : '+this.getClass().getName());
					throw ex;
				}
			}
			//resultado = resultado.Recinto;			
			resultado = this.postProcessXML(resultado);
			
		}		
	} else {
		
		resultado = model;
		//resultado = this.postProcessXML(resultado);		
	}

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
function postProcessXML(xmlparam) {
	//Tenemos que tratar el overbooking para pasarlo a un boolean..
	
	if (xmlparam != null) {
		xmlparam = new XML(xmlparam);
		if (xmlparam.hasOwnProperty('overbooking')) {
			if (xmlparam.overbooking.text() == 0) {
				xmlparam.overbooking = <overbooking>false</overbooking>
			} else if (xmlparam.overbooking.text() == 1) {
				xmlparam.overbooking = <overbooking>true</overbooking>				
			} else {
				log.warn("LOG(WARN) : getEdicionAltaRecintos. El dato overbooking llega con valor distinto a 0 ? 1.")
			}					
		} else {
			xmlparam.overbooking=<overbooking>false</overbooking>
		}
		if (xmlparam.hasOwnProperty('bloqueado')) {
			if (xmlparam.bloqueado.text() == 0) {
				xmlparam.bloqueado = <bloqueado>false</bloqueado>
			} else if (xmlparam.bloqueado.text() == 1) {
				xmlparam.bloqueado = <bloqueado>true</bloqueado>				
			} else {
				log.warn("LOG(WARN) : getEdicionAltaRecintos. El dato bloqueado llega con valor distinto a 0 ? 1.")
			}					
		} else {
			xmlparam.bloqueado=<bloqueado>false</bloqueado>
		}
		if (xmlparam.hasOwnProperty('dadodebaja')) {
			if (xmlparam.dadodebaja.text() == 0) {
				xmlparam.dadodebaja = <dadodebaja>false</dadodebaja>
			} else if (xmlparam.dadodebaja.text() == 1) {
				xmlparam.dadodebaja = <dadodebaja>true</dadodebaja>				
			} else {
				log.warn("LOG(WARN) : getEdicionAltaRecintos. El dato dadodebaja llega con valor distinto a 0 ? 1.")
			}
		} else {
			xmlparam.dadodebaja=<dadodebaja>false</dadodebaja>
		}			
		if (xmlparam.hasOwnProperty('zonas')) {			
			for each (i in xmlparam.zonas.Zona) {					
				if (i.hasOwnProperty('numerada')) {
					if (i.numerada.text() == 0) {
						i.numerada=<numerada>false</numerada>
					} else if (i.numerada.text() == 1) {
						i.numerada=<numerada>true</numerada>
					} else {
						log.warn("LOG(WARN) : getEdicionAltaRecintos. El dato numerada llega con valor distinto a 0 ? 1.")
					}
				} else {
					log.warn("LOG(WARN) : getEdicionAltaRecintos. El dato numerada no existe.");
					i.numerada=<numerada>false</numerada>
				}
				if (i.hasOwnProperty('dadodebaja')) {
					if (i.dadodebaja.text() == 0) {
						i.dadodebaja = <dadodebaja>false</dadodebaja>
					} else if (i.dadodebaja.text() == 1) {
						i.dadodebaja = <dadodebaja>true</dadodebaja>				
					} else {
						log.warn("LOG(WARN) : getEdicionAltaRecintos. El dato dadodebaja llega con valor distinto a 0 ? 1.")
					}
				} else {
					log.warn("LOG(WARN) : getEdicionAltaRecintos. El dato dadodebaja no existe.");
					i.dadodebaja=<dadodebaja>false</dadodebaja>
				}								
			}
		} else {
			log.warn("LOG(WARN) : getEdicionAltaRecintos. No existe la propiedad zonas.")
		}
		if (xmlparam.hasOwnProperty('recintos')) {
			for each (i in xmlparam.recintos.Recinto) {
				// Solo dejamos el nombre, el id y la etiqueta especial selected
				delete i.horarioapertura;
				delete i.horariocierre;
				delete i.maxreimpresiones;
				delete i.overbooking;
				delete i.taquillas;
				delete i.dadodebaja;
				delete i.plano;
				delete i.recinto;
				delete i.recintos;
				delete i.recintounidadnegocios;
				delete i.taquillas;
				delete i.tipoproductos;
				delete i.zonas;
				i.selected=<selected>false</selected>				
			}
		} else {
			log.warn("LOG(WARN) : getEdicionAltaRecintos. No existe la propiedad recintos.")
		}
	}
	return xmlparam;	
}
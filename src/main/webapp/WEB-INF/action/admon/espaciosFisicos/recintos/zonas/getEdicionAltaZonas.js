var services = context.beans.getBean('httpServiceSOA');
var model = <Zona>
				<recinto>
					<nombre></nombre>
				</recinto>								
				<dadodebaja>false</dadodebaja>
				<nombre/>
				<descripcion/>
				<numerada>false</numerada>
				<aforo/>
				<color/>			
				<orden/>			
				<localidads/>
				<i18ns/>							
			</Zona>
			
function handle(request, response) {
		
	var xml = request.getParameter('xml');
	var recinto_param = request.getParameter('recinto_param');
	
	//log.info("LOG(INFO) : Call to getEdicionAltaZonas."+this.getClass().getName());
		
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var resultado;
	

	
	if (xml != null) {
		//log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());	
		
		//en este caso no hay servicio al que llamar,
		//si nos encontramos en la edicion de una zona reenviamos el parametro que nos ha llegado
		// que es la zona a editar...				
		resultado = new XML(this.postProcessXML(xml,recinto_param));
		//log.info("LOG(INFO) : result : "+resultado+' : '+this.getClass().getName());		
	} else {
		resultado = model;
		resultado = new XML(this.postProcessXML(resultado,recinto_param));
		//log.info("LOG(INFO) : result : "+resultado+' : '+this.getClass().getName());		
	}
	
	//log.info("LOG(INFO) : final response : "+resultado+' : '+this.getClass().getName());
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}


function preProcessXML(xmlparam) {
	return xmlparam;
}


function postProcessXML(xmlparam,nombre_aux) {	
	//log.info("LOG(INFO) : nombre_aux : "+nombre_aux+' : '+this.getClass().getName());
	if (xmlparam != null) {
		xmlparam = new XML(xmlparam);
		if (xmlparam.hasOwnProperty('recinto')) {
			xmlparam.recinto.nombre = <nombre>{nombre_aux}</nombre>
		} else {
			xmlparam.recinto.nombre = <nombre>{nombre_aux}</nombre>				
		}		
	}
	return xmlparam;	
}
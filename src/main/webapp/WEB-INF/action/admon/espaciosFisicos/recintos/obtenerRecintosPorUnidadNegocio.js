//var service = context.beans.getBean('httpServiceSOA');
var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {
	
	//print("CONTEXT is a special variable >>> "+context);	

	
	var methodpost = request.getParameter('servicio');
	var envio;
	var resultado;
			
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	if(xml != null ) {
		envio = <servicio>
					<parametro>
						<int>
							{xml}
						</int>
					</parametro>
				</servicio>

	
		//No tratamos el xml de entrada...
	
		//print("The input XML : "+xml);
	
		var respuesta;
		

		try {
			respuesta = services.process(methodpost+'SOAWrapper',envio);					
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}	
		
		//var respuesta = service.process(methodpost,envio);
	
		if (respuesta != null) {
			resultado = new XML(respuesta);			
			resultado = this.postProcessXML(resultado);
		}
	} else {
		//var ex = new java.lang.Exception('Exception in obtenerRecintosPorUnidadNegocio : '+this.getClass().getName());
		log.error('LOG(ERROR) : Estamos haciendo una peticion vacia... ');			
		//throw ex;
		resultado=new XML("<ArrayList></ArrayList>");
	}	
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
	
}
/**
???Cuidado!!! Un XMLObject nunca es nulo.			
**/
function postProcessXML(xmlresponse) {
	
	if (xmlresponse != null) {
		
		xmlresponse=xmlresponse.toXMLString().replace(/&apos;/g, "'").replace(/&quot;/g, '"').replace(/&gt;/g, '>').replace(/&lt;/g, '<').replace(/&amp;/g, '&');
		xmlresponse = new XML(xmlresponse);

		for each (i in xmlresponse.Recinto) {				

			if (i.hasOwnProperty('descripcion')) {
				delete i.descripcion;								
			}
			if (i.hasOwnProperty('recinto')) {
				delete i.recinto;
			}
			if (i.hasOwnProperty('plano')) {
				delete i.plano;
			}
			if (i.hasOwnProperty('maxreimpresiones')) {
				delete i.maxreimpresiones;
			}
			if (i.hasOwnProperty('overbooking')) {
				delete i.overbooking;
			}
			if (i.hasOwnProperty('orden')) {
				delete i.orden;
			}
			if (i.hasOwnProperty('dadodebaja')) {
				delete i.dadodebaja;
			}
			if (i.hasOwnProperty('zonas')) {
				var capacity = new Number(0);
				for each (v in i.zonas.Zona) {
					capacity=capacity+new Number(v.aforo);
				}
				if (i.zonas.Zona.length() > 1) {
					i.zonas=<zonas>Si</zonas>					
				} else {
					i.zonas=<zonas>No</zonas>
				}
				i.capacidad= <capacidad>{capacity}</capacidad>
			} else {
				i.capacidad = <capacidad>0</capacidad>
			}
			if (i.hasOwnProperty('recintounidadnegocios')) {
				delete i.recintounidadnegocios;
			}
			if (i.hasOwnProperty('contenidos')) {
				delete i.contenidos;
			}
			i.horario=
					<horario>
						<from></from>
						<to></to>
					</horario>
			if (i.hasOwnProperty('horaapertura')) {
				i.horario.from=""+i.horaapertura.text();
				delete i.horaapertura;				
			}
			if (i.hasOwnProperty('horacierre')) {
				i.horario.to=""+i.horacierre.text();
				delete i.horacierre;
			}


			envio2=
			<servicio>
					<parametro>
						<int>{i.idrecinto.text()}</int>
					</parametro>
				</servicio>
			
			var respuesta;
			try{
				respuesta = services.process('obtenerRecintosHijosSOAWrapper',envio2);
			} catch (ex) {
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				log.error('LOG(ERROR) : result in this Exception : '+ex);
				throw ex.javaException;
			}	

			respuestaXML = new XML(respuesta.trim());
			i.recintos = <recintos>{respuestaXML.Recinto}</recintos>
			
		}
	}
	return xmlresponse;
}
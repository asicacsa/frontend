var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	//log.info("Entra en el action bloquearRecintos");
	
	var idrecinto = request.getParameter('idrecinto');
	var bloqueado = request.getParameter('bloqueado');
	
	var methodpost = 'bloquearRecintos';
	var result;
	var envio;

	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	if(idrecinto != null && bloqueado != null ) {
		
		envio = 
			<servicio>
				<parametro>
					<int>{idrecinto}</int>
					<int>{bloqueado}</int>
				</parametro>
			</servicio>

		//log.info('Lo que le pasamos al servicio bloquearRecintos: ' + envio);
		
		var aux;
		try{	
			aux = services.process(methodpost+'SOAWrapper',envio);		
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
		result = <ok>
					{aux}
				 </ok>;
					 		
	} else {
		//log.info("El xml que llega a bloquearRecintos es nulo!!!");
		result = <error/>
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
}
var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {
	//log.info("LOG(INFO) : Call to getEdicionAltaSubRecintos."+this.getClass().getName());
	
	var xml = request.getParameter('xml');
	
	var resultado = new XML();
	
	//log.info("LOG(INFO) : with this parameters : "+xml+' : '+this.getClass().getName());
	
	if (xml != null) {
		var idsubrecinto = request.getParameter('idsubrecinto');
		
		if (idsubrecinto != "null") {
			var envio = <servicio>
							<parametro>
								<int>
									{idsubrecinto}
								</int>
							</parametro>
						</servicio>
						
			//log.info("Este es el xml que enviamos para el get: "+envio.toXMLString());
			
			var aux;
			
			try {
				aux = services.process('obtenerRecintoPorIdSOAWrapper',envio);					
			} catch (ex) {
		 		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				log.error('LOG(ERROR) : result in this Exception : '+ex);
				throw ex.javaException;
		 	}
			
			//log.info('Este es el valor que retorna : '+new XML(aux).toXMLString())
			
			if (aux != null) {
				resultado = new XML(aux);
			}
		} else {
			var model;
			
			model = 
				<Recinto>
					<nombre></nombre>
					<dadodebaja>false</dadodebaja>
					<maxreimpresiones/>
					<horaapertura/>
					<horacierre/>
					<overbooking>false</overbooking>																			
					<recintos/>
					<recintounidadnegocios/>
					<taquillas/>
					<recinto><idrecinto>{xml}</idrecinto></recinto>
					<zonas/>
				</Recinto>
				
			resultado = model;
		}
	} else {
		var ex = new java.lang.Exception('Is not allowed call with this function without parameters.');
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex;
	}
	
	//log.info("LOG(INFO) : final response : "+resultado+' : '+this.getClass().getName());
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());	
}
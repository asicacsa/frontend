var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaRecinto';
var servicio_edicion ='actualizarRecintos';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');
/**
dadodebaja : Integer
idtaquilla : Integer
nombre : String
orden : BigDecimal
recinto : Recinto
ubicacion : Ubicacion

**/
function handle(request, response) {
		
	var xml = request.getParameter('xml');
	var recinto_constant= 'Recinto';
	var id_constant = 'idrecinto';		
	
	var resultado;
	
	var methodpost;
		
	var envio;
	
	//log.info("LOG(INFO) : Call to postEdicionAltaRecinto : "+this.getClass().getName());
	
	if (xml!=null) {
		xml = new XML(xml);
		
		if (xml.hasOwnProperty(recinto_constant)) {
			//Preparamos el xml para enviarlo con <list/>, <arrayList/>, ya vorem...
			xml = xml.Recinto;
		} else if (!(xml.name() == recinto_constant)) {

			var ex = new java.lang.Exception('Exception in postEdicionAltaRecinto : '+this.getClass().getName());
			log.error('LOG(ERROR) : does not exist Recinto in the parameters of the call.',ex);			
			throw ex;
		}// Si su nombre es Unidadnegocio es que 

		
		//log.info("LOG(INFO) : params before the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		xml = this.preProcessXML(xml);
		//log.info("LOG(INFO) : params after the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		
		//xml = this.preProcessXML(xml);
		
		
		//Ahora creamos el envio...
		//var methodpost = request.getParameter('servicio');
		

		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicio de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.		
		if(methodpost == null) {
			//Si es un alta no poseera la propiedad idproducto
			if (!xml.hasOwnProperty('idrecinto')) {
				methodpost = servicio_alta;				
				envio = 
					<servicio>
						<parametro>
							{xml}
						</parametro>
					</servicio>
				
			} else {
				methodpost = servicio_edicion;
				envio = 
					<servicio>
						<parametro>
							<list>
								{xml}
							</list>
						</parametro>
					</servicio>				
			}
		}		
		//log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());			
		//log.info("LOG(INFO) : with this parameters : "+envio+' : '+this.getClass().getName());
		//Y llamamos al servicio...
		var aux;
		
		
		try {
		 	aux = new XML(services.process(methodpost+'SOAWrapper',envio));
		 } catch (ex) {
	 		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		 }
	
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		
		resultado = 
				<ok>
					{aux}
				</ok>
		//log.info("LOG(INFO) : result : "+resultado+' : '+this.getClass().getName());				
		//En aux tenemos la respuesta			
	} else {
		var ex = new java.lang.Exception('Exception in postEdicionAltaRecinto : '+this.getClass().getName());
		log.error('LOG(ERROR) : does not exist params in the call.',ex);			
		throw ex;
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Preproceso de un Recinto que se va a dar de alta o editar...
	if (param != null) {
		param = new XML(param);
		if (param.hasOwnProperty('overbooking')) {
			if (param.overbooking.text() == 'false') {
				param.overbooking = <overbooking>0</overbooking>
			} else if (param.overbooking.text() == 'true') {
				param.overbooking = <overbooking>1</overbooking>				
			} else {
				log.warn("LOG (WARN) : Warning in postEdicionAltaRecinto, value not expected in overbooking : "+this.getClass().getName())
			}
		}
		
		if (param.hasOwnProperty('dadodebaja')) {
			if (param.dadodebaja.text() == 'false') {
				param.dadodebaja = <dadodebaja>0</dadodebaja>
			} else if (param.dadodebaja.text() == 'true') {
				param.dadodebaja = <dadodebaja>1</dadodebaja>				
			} else {
				log.warn("LOG (WARN) : Warning in postEdicionAltaRecinto, value not expected in dadodebaja : "+this.getClass().getName())
			}
		}
		
		if (param.hasOwnProperty('bloqueado')) {
			if (param.bloqueado.text() == 'false') {
				param.bloqueado = <bloqueado>0</bloqueado>
			} else if (param.bloqueado.text() == 'true') {
				param.bloqueado = <bloqueado>1</bloqueado>				
			} else {
				log.warn("LOG (WARN) : Warning in postEdicionAltaRecinto, value not expected in bloqueado : "+this.getClass().getName())
			}
		}
		
		if (param.hasOwnProperty('admitesolapamiento')) {
			if (param.admitesolapamiento.text() == 'false') {
				param.admitesolapamiento = <admitesolapamiento>0</admitesolapamiento>
			} else if (param.admitesolapamiento.text() == 'true') {
				param.admitesolapamiento = <admitesolapamiento>1</admitesolapamiento>				
			} else {
				log.warn("LOG (WARN) : Warning in postEdicionAltaRecinto, value not expected in admitesolapamiento : "+this.getClass().getName())
			}
		}
		param = this.processTaquillas(param);
		param = this.processZonas(param);
		param = this.processUnidadesNegocio(param);
		//Elimino los subRecintos, total no voy a hacer nada con ellos.
		delete param.recintos;
		
		//Ahora eliminamos los elementos sin hijos
		param = comun.borraElementosSinHijos(param);
	}
	
	return param;	
}
function processTaquillas(param) {
	if (param.hasOwnProperty('taquillas')) {
		for each (i in param.taquillas.Taquilla) {
			if (i.hasOwnProperty('selected')) {
				delete i.selected;
			}
		}
	}
	return param;
}
function processUnidadesNegocio(param) {
	if (param.hasOwnProperty('recintounidadnegocios')) {
		for each (i in param.recintounidadnegocios.Recintounidadnegocio.unidadnegocio) {
			if (i.hasOwnProperty('selected')) {
				delete i.selected;
			}
		}
	}
	return param;
}

function processZonas(param) {
	if (param.hasOwnProperty('zonas')) {
		for each (i in param.zonas.Zona) {
			if (i.hasOwnProperty('dadodebaja')) {
				if (i.dadodebaja.text() == 'false') {
					i.dadodebaja = <dadodebaja>0</dadodebaja>
				} else if (i.dadodebaja.text() == 'true') {
					i.dadodebaja = <dadodebaja>1</dadodebaja>				
				} else {
					log.warn("LOG (WARN) : Warning in postEdicionAltaRecinto, value not expected in Zona.dadodebaja : "+this.getClass().getName())
				}				
			}
			if (i.hasOwnProperty('numerada')) {
				if (i.numerada.text() == 'false') {
					i.numerada = <numerada>0</numerada>
				} else if (i.numerada.text() == 'true') {
					i.numerada = <numerada>1</numerada>				
				} else {
					log.warn("LOG (WARN) : Warning in postEdicionAltaRecinto, value not expected in Zona.numerada : "+this.getClass().getName())
				}				
			}			
		}
	}
	return param;
}
var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaPuntosrecogida';
var servicio_edicion ='actualizarPuntosrecogida';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost;
	var envio;
	
	if (log.isInfoEnabled()) {
		log.info("Entrada a postEdicionAltaPuntosrecogida.action");
	}
	
	if (xml!=null) {
		xml = new XML(xml);
		if (log.isInfoEnabled()) {
			log.info("postEdicionAltaPuntosrecogida, xml: " + xml.toXMLString());
		}
		
		if (xml.name()!="Puntosrecogida"){
			xml = xml.Puntosrecogida;
		}
		

		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicion de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.
		
		if(methodpost == null) {
			//Si es un alta no poseera la propiedad id
			// ALTA
			if (xml.idpuntosrecogida.text().length() == 0) {
				methodpost = servicio_alta;
				xml = this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							{xml}
						</parametro>
					</servicio>
			// EDICION	
			} else {
				methodpost = servicio_edicion;
				xml= this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							<list>
								{xml}
							</list>
						</parametro>
					</servicio>			
			}
		}		
		
		if (log.isInfoEnabled()) {
			log.info("postEdicionAltaPuntosrecogida, envio: " + envio);
			log.info("postEdicionAltaPuntosrecogida, metodo post : " + methodpost);
		}
		
		//Y llamamos al servicio...
		var aux;
		
		try{
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
		
		aux = new XML(aux);
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		resultado = <ok>{aux}</ok>

		if (log.isInfoEnabled()) {
			log.info("postEdicionAltaPuntosrecogida, resultado: " + resultado.toXMLString());
		}
					
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a postEdicionAltaPuntosrecogida es nulo!!!");
		}
		resultado = <error/>
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}


function preProcessXML(param) {
	param = new XML(param);
	param = comun.borraElementosSinHijos(param.toXMLString());
	
	return param;	
}
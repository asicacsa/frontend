var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {

	if (log.isInfoEnabled()){
		log.info("Entrada a getEdicionAltaPuntosrecogida.action");
	}
		
	var xml = request.getParameter('xml');
	var methodpost = request.getParameter('servicio');
	var envio;
	var result;

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	
	xml = new XML(xml);	
	
	if (xml != null && xml != '') {
		envio = <servicio>
					<parametro>
						<int>
							{xml}
						</int>
					</parametro>
				</servicio>;
	} else {
		envio  = <servicio>
					<parametro>
						<Puntosrecogida/>
					</parametro>
				</servicio>;
				
		methodpost = 'getXMLModel';
	}
	
	
	try{
		result = services.process(methodpost+'SOAWrapper',envio);		
	} catch (ex) {
 		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}			
	
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result);
}
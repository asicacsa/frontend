var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {

	//log.info("Entrada a getEdicionAltaTaquillas.action");
		
	var xml = request.getParameter('xml');
	var methodpost = request.getParameter('servicio');
	var envio;
	var result;

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	//print("Este es el xml que llega a getEdicionAltaTaquillas: " + xml);	
	
	xml = new XML(xml);	
	
	if (xml != null && xml != '') {
		
		envio = <servicio>
					<parametro>
						<int>
							{xml}
						</int>
					</parametro>
				</servicio>;	
		
	} else {
		envio  = <servicio>
					<parametro>
						<Taquilla/>
					</parametro>
				</servicio>;
		methodpost = 'getXMLModel';
	}
	
	//log.info("Este es el metodo al que vamos a llamar : "+methodpost);
	
	try{
		result = services.process(methodpost+'SOAWrapper',envio);		
	} catch (ex) {
 		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}			
	
	//log.info('Esto es lo que devuelve el servicio : ' + result);
	
	//log.info("Esto es lo que devuelve getEdicionAltaTaquillas.action: " + result);
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result);
}

/*function postProcessXML(xmlparam) {
	//xmlparam = comun.deleteTrash(xmlparam.toXMLString());
	//xmlparam = comun.totalMatch(xmlparam,'@idref');
	//if (xmlparam != null) {
		//xmlparam = new XML(xmlparam);
	//}	
	if (xmlparam != null) {
		xmlparam = new XML(xmlparam);
		if (xmlparam.name() != 'Taquilla') {
			if (xmlparam.hasOwnProperty('Taquilla')) {
				 xmlparam = xmlparam.Taquilla;		 
			}
		}
	}
	return xmlparam;	
}*/
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');
var model = <Ubicacion>
				<nombre></nombre>
				<orden></orden>
				<dadodebaja>0</dadodebaja>
				<descripcion/>			
				<taquillas/>
			</Ubicacion>
			
/**
dadodebaja : Integer
descripcion : String
idubicacion : BigDecimal
nombre : String
orden : BigDecimal
taquillas : Set
**/
			
function handle(request, response) {
		
	var xml = request.getParameter('xml');
	
	
	var methodpost = request.getParameter('servicio');
	


	//log.info("LOG(INFO) : Call to getEdicionAltaUbicaciones."+this.getClass().getName());
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var resultado;
	

	
	if (xml!=null) {
		//log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());
		
		var envio = <servicio>
						<parametro>
							<int>
								{xml}
							</int>
						</parametro>
					</servicio>
		
				//log.info("LOG(INFO) : with this parameters : "+envio+' : '+this.getClass().getName());					
		var aux;
		
		try {
			aux = services.process(methodpost+'SOAWrapper',envio);				
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
		
	    aux = aux.trim();

		//log.info("LOG(INFO) : result : "+aux+' : '+this.getClass().getName());
		if (aux != null) {
			resultado = new XML(aux);
			if (resultado.hasOwnProperty('Ubicacion')) {
				resultado = resultado.Ubicacion;
			} else {
				if (!(resultado.name() == 'Ubicacion')) {
					log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
					log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
					log.error('LOG(ERROR) : result does not Ubicacion : '+ex);
					var ex = new java.lang.Exception('Exception calling this service : '+methodpost+'SOAWrapper : '+this.getClass().getName());
					throw ex;
				}
			}			
			resultado = this.postProcessXML(resultado);
			//log.info("LOG(INFO) : result post processed : "+resultado+' : '+this.getClass().getName());	
					
			//log.info('Fin de la edicion del valor de retorno : '+resultado.toXMLString());								
		}		
	} else {
		//log.info("LOG(INFO) : to this service : "+methodpost+' : '+this.getClass().getName());	
		resultado = model;		
	}
	//log.info("LOG(INFO) : final response : "+resultado+' : '+this.getClass().getName());
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
function postProcessXML(xmlparam) {

	if (xmlparam != null) {
		//Vamos a a meterle los selected en las colecciones.
		if (xmlparam.hasOwnProperty('taquillas')) {
			for each (i in xmlparam.taquillas.Taquilla) {
				i.selected=false;
			}
		}		
	}
	return xmlparam;	
}
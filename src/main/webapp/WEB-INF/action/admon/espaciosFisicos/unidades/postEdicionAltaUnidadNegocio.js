var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaUnidadNegocio';
var servicio_edicion ='actualizarUnidadesNegocio';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');
/**
dadodebaja : Integer
idunidadnegocio : BigDecimal
nombre : String
orden : BigDecimal
recintounidadnegocios : Set
redunidadnegocios : Set
restriccionredeses : Set
restriccionredeses_1 : Set
usuariounidadnegocios : Set
**/
function handle(request, response) {
		
	var xml = request.getParameter('xml');
	var unidad_constant= 'Unidadnegocio';
	var id_constant = 'idunidadnegocio';
	
	var resultado;
	
	var methodpost;
		
	var envio;

	//log.info("LOG(INFO) : Call to postEdicionAltaUnidadNegocio : "+this.getClass().getName());
	
	if (xml!=null) {
		xml = new XML(xml);

		if (xml.hasOwnProperty(unidad_constant)) {
			//Preparamos el xml para enviarlo con <list/>, <arrayList/>, ya vorem...
			xml = xml.Unidadnegocio;
		} else if (!(xml.name() == unidad_constant)) {

			var ex = new Exception('Exception in postEdicionAltaUnidadNegocio : '+this.getClass().getName());
			log.error('LOG(ERROR) : does not exist Unidadnegocio in the parameters of the call.',ex);			
			throw ex;
		}// Si su nombre es Unidadnegocio es que 
				
		//log.info("LOG(INFO) : params before the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		xml = this.preProcessXML(xml);
		//log.info("LOG(INFO) : params after the transformation : "+this.getClass().getName()+" "+xml.toXMLString());

		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicio de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.
		
		//Si es un alta no poseera la propiedad idproducto
		if (!xml.hasOwnProperty(id_constant)) {
			methodpost = servicio_alta;
			envio = 
				<servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>				
		} else {
			methodpost = servicio_edicion;
			envio = 
				<servicio>
					<parametro>
						<list>
							{xml}
						</list>
					</parametro>
				</servicio>				
		}		

		//log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());			
		//log.info("LOG(INFO) : with this parameters : "+envio+' : '+this.getClass().getName());
		//Y llamamos al servicio...
		var aux;
		
		try {
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
		
		
									
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		
		resultado = 
				<ok>
					{aux}
				</ok>
		//log.info("LOG(INFO) : result : "+resultado+' : '+this.getClass().getName());				
		//En aux tenemos la respuesta			
	} else {
		var ex = new java.lang.Exception('Exception in postEdicionAltaUnidadNegocio : '+this.getClass().getName());
		log.error('LOG(ERROR) : does not exist params in the call.',ex);			
		throw ex;
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	//if (param.orden.text().length() == 0) {
	//		delete param.orden;
	//}
	
	if (param.mostrar.text() == 'false') {
		param.mostrar = <mostrar>0</mostrar>
	} else if (param.mostrar.text() == 'true') {
		param.mostrar = <mostrar>1</mostrar>				
	} 

	//Obligatorio : Campo dado de baja, por defecto lo ponemos a 0 ...
	/**if (param.dadodebaja.text().length() == 0) {
			param.dadodebaja=0;
	}**/
	//log.info("LOG(INFO) : params before the borraElementosSinHijos : "+this.getClass().getName()+" "+param.toXMLString());
	param = comun.borraElementosSinHijos(param.toXMLString());
	//log.info("LOG(INFO) : params after the borraElementosSinHijos : "+this.getClass().getName()+" "+param.toXMLString());	
	param = comun.deleteSelectedTag(param.toXMLString());
	param = new XML(param);
	//log.info("LOG(INFO) : params after the deleteSelectedTag : "+this.getClass().getName()+" "+param.toXMLString());		
	
	return param;
	
}
var services = context.beans.getBean('httpServiceSOA');
var model = <Unidadnegocio>
				<nombre></nombre>
				<orden></orden><mostrar>1</mostrar>
				<dadodebaja>0</dadodebaja>
				<redunidadnegocios/>			
				<restriccionredeses/>
				<entidadgestora>
					<identidadgestora/>
				</entidadgestora>
				<restriccionredeses_1/>
				<usuariounidadnegocios/>
				<recintounidadnegocios/>
			</Unidadnegocio>
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');			
function handle(request, response) {
		
	var xml = request.getParameter('xml');	
	var methodpost = request.getParameter('servicio');


	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var resultado;
	

	
	if (xml!=null) {

		var envio = <servicio>
						<parametro>
							<int>
								{xml}
							</int>
						</parametro>
					</servicio>

		var aux;
		
		try {
			aux = services.process(methodpost+'SOAWrapper',envio);					
		} catch (ex) {
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;		
		}
		

		aux = aux.trim();
		
		if (aux != null) {
			resultado = new XML(aux);
			if (resultado.hasOwnProperty('Unidadnegocio')) {
				resultado = resultado.Unidadnegocio;
			} else {
				if (!(resultado.name() == 'Unidadnegocio')) {
					log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
					log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
					log.error('LOG(ERROR) : result does not Unidadnegocio : '+ex);
					var ex = new java.lang.Exception('Exception calling this service : '+methodpost+'SOAWrapper : '+this.getClass().getName());
					throw ex;
				}
			}
			//resultado = this.postProcessXML(resultado);
			//log.info('Fin de la edicion del valor de retorno : '+resultado.toXMLString());								
		}		
	} else {
		resultado = model;		
	}

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
function postProcessXML(xmlparam) {
	if (xmlparam != null) {
		//Vamos a a meterle los selected en las colecciones.
		if (xmlparam.hasOwnProperty('recintounidadnegocios')) {
			for each (i in xmlparam.recintounidadnegocios.Recintounidadnegocio) {
				i.selected=false;
			}
		}
		if (xmlparam.hasOwnProperty('redunidadnegocios')) {
			for each (i in xmlparam.redunidadnegocios.Redunidadnegocio) {
				i.selected=false;
			}
		}
		if (xmlparam.hasOwnProperty('usuariounidadnegocios')) {
			for each (i in xmlparam.usuariounidadnegocios.Usuariounidadnegocio) {
				i.selected=false;
			}
		}
		if (xmlparam.mostrar.text() == 0) {
				xmlparam.mostrar = <mostrar>false</mostrar>
		} else if (xmlparam.mostrar.text() == 1) {
				xmlparam.mostrar = <mostrar>true</mostrar>				
		}		


	}
	return xmlparam;	
}

//var service = context.beans.getBean('httpServiceSOA');

// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	log.info('GMVMICG entro en el handler de obtenerTarifas');

	//print("CONTEXT is a special variable >>> "+context);	
	
	var methodpost = request.getParameter('servicio');
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		var envio = '<servicio />';
	}
	
	//No tratamos el xml de entrada...
	
	//print("The input XML : "+xml);
	
	var respuesta;
	
	try{
		respuesta = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
 		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		log.info('GMVMICG Se ha producido una excepcion' + ex);
		throw ex.javaException;
	 }
	/*Modificado por SADIM para limpiar el XML y poder visualizarlo*/	
	respuesta=respuesta.trim();
	
	//var respuesta = new XML(service.process(methodpost,envio));
	
	if (respuesta!=null) {
		log.info('GMVMICG respuesta es != de null');
		respuesta = new XML(respuesta);
		respuesta = this.postProcessXML(respuesta);	
		//log.info("Resultado de llamar a ObtenerTarifas: "+respuesta.toXMLString());

	}

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());

	log.info('GMVMICG respuesta == ' + respuesta.toXMLString());

}

function postProcessXML(xmlparam) {
	var aux = comun.deleteTrash(xmlparam.toXMLString());
	aux = comun.totalMatch(aux, "@idref");
	if (aux != null) {
		xmlparam = new XML(aux);		
	} else {
		xmlparam = aux;
	}
	return xmlparam;			
}
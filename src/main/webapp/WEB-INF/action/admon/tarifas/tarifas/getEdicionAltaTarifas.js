var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	var xml = request.getParameter('xml');
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var resultado;
	
	if (xml!=null && xml!='') {
		//print("Este es el xml que llega a getEdicionAltaTarifas: "+new XML(xml).toXMLString());	
		var envio = <servicio>
						<parametro>
							<int>
								{xml}
							</int>
						</parametro>
					</servicio>		
	} else {
		envio  = <servicio>
					<parametro>
						<Tarifa/>
					</parametro>
				</servicio>;
		methodpost = 'getXMLModel';
	}
	
	//log.info("Este es el metodo al que vamos a llamar : "+methodpost);
	var aux;
	
	try{
		aux = services.process(methodpost+'SOAWrapper',envio).trim();					
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	//log.info('Este es el valor que retorna: '+new XML(aux).toXMLString())
	
	if (aux != null) {
		resultado = new XML(aux);	
		//if (resultado.name() != "perfilvisitante")		
		//resultado = resultado.perfilvisitante;			
		//resultado = this.postProcessXML(resultado);							
	}
		
	//log.info("Esto es lo que retornamos de getEdicionAltaTarifas: " + resultado.toXMLString());
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}


function postProcessXML(xmlparam) {
	xmlparam = comun.deleteTrash(xmlparam.toXMLString());
	xmlparam = comun.totalMatch(xmlparam,'@idref');

	if (xmlparam.basesapinvitacion.text() == 0) {
			xmlparam.basesapinvitacion = <basesapinvitacion>false</basesapinvitacion>
	} else if (xmlparam.basesapinvitacion.text() == 1) {
			xmlparam.basesapinvitacion = <basesapinvitacion>true</basesapinvitacion>				
	}	
	return xmlparam;	
}
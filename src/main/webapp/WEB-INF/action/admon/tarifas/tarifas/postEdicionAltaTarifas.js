var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaTarifa';
var servicio_edicion ='actualizarTarifas';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

/**
Integer idtarifa
Perfilvisitante perfilvisitante
String nombre
String descripcion
Integer dadodebaja
Integer orden
**/

function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var cPerfiles = request.getParameter('cPerfiles');		
	var resultado;
	var methodpost;
	var envio;
	
	//log.info("Entra en el handle de postEdicionAltaTarifas: "+xml);
	//log.info("Entra en el handle de postEdicionAltaTarifas: "+cPerfiles);
	
	if (xml!=null) {
		xml = new XML(xml);
		xml = xml;
		//log.info("Este es el xml que llega a postEdicionAltaTarifas: "+xml.toXMLString());
		

		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicion de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.
		
		if(methodpost == null) {
			//Si es un alta no poseera la propiedad idperfilvisitante
			// ALTA
			if (xml.idtarifa.text().length() == 0) {
				methodpost = servicio_alta;
				xml = this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							{xml}
							<controlarPerfiles>{cPerfiles}</controlarPerfiles>
						</parametro>
					</servicio>
			// EDICION	
			} else {
				methodpost = servicio_edicion;
				//log.info('Antes de llamar a preProcess....'+xml);
				xml= this.preProcessXML(xml);
				//log.info('Despues de llamar a preProcess....');
				
				envio = 
					<servicio>
						<parametro>
							<list>
								{xml}
							</list>
							<controlarPerfiles>{cPerfiles}</controlarPerfiles>
						</parametro>
					</servicio>			
			}
		}		
		if (log.isDebugEnabled()) {
			log.info("Este es el envio : "+envio);
			log.info("Este es el metodo post : "+methodpost);
		}
		
		var aux;
		
		try {
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			if (log.isErrorEnabled()) {
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost);
				log.error("LOG(ERROR) : con esta llamada: " + envio);
				log.error('LOG(ERROR) : con este tipo de error : ' + ex);
				log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
			}			
			throw ex.javaException;
		}
	
		
		
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		//log.info('Lo que me devuelve el servicio: '+aux);
	

		if (aux != null) {
			resultado = new XML(aux);							
		}
	
		resultado = this.postProcessXML(resultado);
		
		
		//resultado = <ok>{aux}</ok>
					
	} else {
		//log.info("El xml que llega a postEdicionAltaTarifa es nulo!!!");
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	//log.info("Lo que vamos a pasar por preProcessXML postEdicionAltaTarifa : "+param);
	
	param = comun.borraElementosSinHijos(param.toXMLString());
	
	if (param.orden.text().length() == 0) {
			delete param.orden;
	}
	
	if (param.nombre.text().length() == 0) {
			delete param.nombre;
	}
	
	if (param.descripcion.text().length() == 0) {
			delete param.descripcion;
	}
		
	if (param.abono.text() == true) {
		param.abono=<abono>1</abono>
	} else if (param.abono.text() == false || param.abono.text() == 0) {
		param.abono=<abono>0</abono>
	} 

	if (param.bono.text() == true) {
		param.bono=<bono>1</bono>
	} else if (param.bono.text() == false || param.bono.text() == 0) {
		param.bono=<bono>0</bono>
	} 

	if (param.basesapinvitacion.text() == 'false') {
		param.basesapinvitacion = <basesapinvitacion>0</basesapinvitacion>
	} else if (param.basesapinvitacion.text() == 'true') {
		param.basesapinvitacion = <basesapinvitacion>1</basesapinvitacion>				
	} 


	//Obligatorio : Campo dado de baja, por defecto lo ponemos a 0 ...
	if (param.dadodebaja.text().length() == 0) {
			param.dadodebaja=0;
	}
	
	param = comun.deleteSelectedTag(param.toXMLString());
	
	param = new XML(param);

	
	return param;
	
}
function postProcessXML(param) {

	if(param.mensajes.length() == 0){
		param.setName('mensajes');
		var tarifa = <tarifa/>;
		tarifa.appendChild(param);
		
		param = tarifa;
	}else{
		param.setName('tarifa');
	}
	
	
	return param;
}
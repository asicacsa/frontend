var services = context.beans.getBean('httpServiceSOA');
var model = <perfilvisitante>
				<nombre></nombre>
				<descripcion></descripcion>
				<orden></orden><sapinvitacion>0</sapinvitacion>
				<activo>1</activo>
				<dadodebaja/>
				<i18ns/>				
			 </perfilvisitante>
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');			
			
/**
Integer idperfilvisitante
Tarifa tarifa
String nombre
String descripcion
Integer dadodebaja
Integer orden
**/

			
function handle(request, response) {
		
	var xml = request.getParameter('xml');
	
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var resultado;
	
	if (xml!=null) {
		//print("Este es el xml que llega : "+new XML(xml).toXMLString());	
		var envio = <servicio>
						<parametro>
							<int>
								{xml}
							</int>
						</parametro>
					</servicio>
		//log.info("Este es el metodo al que vamos a llamar : "+methodpost);
		var aux;
	
		try{	
			aux = services.process(methodpost+'SOAWrapper',envio);		
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}				
		//log.info('Este es el valor que retorna : '+new XML(aux).toXMLString())
		
		if (aux != null) {
			resultado = new XML(aux);	
			//if (resultado.name() != "perfilvisitante")		
			//resultado = resultado.perfilvisitante;			
			//resultado = this.postProcessXML(resultado);							
		}		
	} else {
		resultado = model;
	}
	
	//log.info("Esto es lo que retornamos : " + resultado.toXMLString());
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}


function postProcessXML(xmlparam) {
	xmlparam = comun.deleteTrash(xmlparam.toXMLString());
	xmlparam = comun.totalMatch(xmlparam,'@idref');


	if (xmlparam.sapinvitacion.text() == 0) {
				xmlparam.sapinvitacion = <sapinvitacion>false</sapinvitacion>
	} else if (xmlparam.sapinvitacion.text() == 1) {
				xmlparam.sapinvitacion = <sapinvitacion>true</sapinvitacion>				
	}
	
	//GMV PR05. Nuevo campo para activar o desactivar el perfil de visitante
	if (xmlparam.activo.text() == 0) {
		xmlparam.activo = <activo>false</activo>
	} else if (xmlparam.activo.text() == 1) {
		xmlparam.activo = <activo>true</activo>				
	}	
	return xmlparam;	
}
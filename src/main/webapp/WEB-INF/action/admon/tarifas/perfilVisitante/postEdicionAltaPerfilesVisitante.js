var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaPerfilVisitante';
var servicio_edicion ='actualizarPerfilesVisitante';
/**
Integer idperfilvisitante
Tarifa tarifa
String nombre
String descripcion
Integer dadodebaja
Integer orden
**/
function handle(request, response) {
		
	var xml = request.getParameter('xml');
	var resultado;
	var methodpost;
	var envio;
	
	//log.info("Entra en el handle de postEdicionAltaPerfilesVisitante");
	
	if (xml!=null) {
		xml = new XML(xml);
		xml = xml.perfilvisitante;
		//log.info("Este es el xml que llega a postEdicionAltaPerfilesVisitante: "+xml.toXMLString());
		
		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicion de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.
		
		if(methodpost == null) {
			//Si es un alta no poseera la propiedad idperfilvisitante
			// ALTA
			if (!xml.hasOwnProperty('idperfilvisitante')) {
				methodpost = servicio_alta;
				xml = this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							{xml}
						</parametro>
					</servicio>
			// EDICI?N	
			} else {
				methodpost = servicio_edicion;
				xml= this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							<list>
								{xml}
							</list>
						</parametro>
					</servicio>
			}
		}
		
		//log.info("Este es el envio : "+envio);
		//log.info("Este es el metodo post : "+methodpost);
		
		//Y llamamos al servicio...
		var aux;
		try{
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}	
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.
		//En aux tenemos la respuesta.
		resultado = 
				<ok>
					{aux}
				</ok>
	} else {
		//log.info("El xml que llega a postEdicionAltaPerfilesVisitante es nulo!!!");
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	//log.info("Lo que vamos a pasar por el servicio : "+param);
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('transformerXML.js');
	
	if (param.orden.text().length() == 0) {
			delete param.orden;
	}
	
	if (param.nombre.text().length() == 0) {
			delete param.nombre;
	}
	
	if (param.descripcion.text().length() == 0) {
			delete param.descripcion;
	}
	
	//Obligatorio : Campo dado de baja, por defecto lo ponemos a 0 ...
	if (param.dadodebaja.text().length() == 0) {
			param.dadodebaja=0;
	}
	
	if (param.sapinvitacion.text() == 'false') {
		param.sapinvitacion = <sapinvitacion>0</sapinvitacion>
	} else if (param.sapinvitacion.text() == 'true') {
		param.sapinvitacion = <sapinvitacion>1</sapinvitacion>
	}
	
	//GMV PR05. Nuevo campo para activar o desactivar el perfil de visitante
	if (param.activo.text() == 'false') {
		param.activo = <activo>0</activo>
	} else if (param.activo.text() == 'true') {
		param.activo = <activo>1</activo>
	}

	if (param.requiereAcreditacion.text() == 'false') {
		log.info( "\n\n\nEl texto del param requiereAcreditacion es false\n\n\n" );
		param.requiereAcreditacion = <requiereAcreditacion>0</requiereAcreditacion>
	} else if (param.requiereAcreditacion.text() == 'true') {
		log.info( "\n\n\nEl texto del param requiereAcreditacion es true\n\n\n" );
		param.requiereAcreditacion = <requiereAcreditacion>1</requiereAcreditacion>
	}
	
	param = comun.borraElementosSinHijos(param); //Eliminamos todos los elementos que se han quedado sin hijos
	
	return param;
	
}
var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaDescuentoPromocional';
var servicio_edicion ='actualizarDescuentosPromocionales';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

/**
Integer idtarifa
Perfilvisitante perfilvisitante
String nombre
String descripcion
Integer dadodebaja
Integer orden
**/

function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost;
	var envio;
	
	log.error("Nuevo");
	//log.info("Entra en el handle de postEdicionAltaDescuentosPromocionales");
	
	if (xml!=null) {
		xml = new XML(xml);
		xml = xml.Descuentopromocional;
		//log.info("Este es el xml que llega a postEdicionAltaDescuentosPromocionales: "+xml.toXMLString());
		

		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicion de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.
		
		if(methodpost == null) {
			//Si es un alta no poseera la propiedad id
			// ALTA
			if (!xml.hasOwnProperty('iddescuentopromocional')) {
				methodpost = servicio_alta;
				xml = this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							{xml}
						</parametro>
					</servicio>
			// EDICI?N	
			} else {
			
			
				methodpost = servicio_edicion;
				xml= this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							<list>
								{xml}
							</list>
						</parametro>
					</servicio>			
			}
		}		
		
		//log.info("Este es el envio : "+envio);
		//log.info("Este es el metodo post : "+methodpost);
		
		//Y llamamos al servicio...
		var aux;
		
		try{
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		resultado = 
				<ok>
					{aux}
				</ok>
					
	} else {
		//log.info("El xml que llega a postEdicionAltaDescuentosPromocionales es nulo!!!");
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	// fuera nombrecompleto de Cliente
	
	delete param..nombrecompleto;
	for each (i in param.descuentosesiones.Descuentosesion.sesion){
		delete i.nombre;		
		}
	//Obligatorio : Campo dado de baja, por defecto lo ponemos a 0 ...
	if (param.dadodebaja.text().length() == 0) {
			param.dadodebaja=0;
	}
	
	param = comun.deleteSelectedTag(param.toXMLString());
	
	var xmlSinHijos= comun.borraElementosSinHijos(param);
	
	return xmlSinHijos;
	
}
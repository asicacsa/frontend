var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = request.getParameter('servicio');
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');

	if(xml == null ) {
		//GGL Por defecto que devuelva los vigentes
		var envio = '<servicio><parametro><obtenerDescuentosCaducadosParam><caducados>0</caducados></obtenerDescuentosCaducadosParam></parametro></servicio>'
	} else {
		xml = new XML(xml);
		
		xml = this.preProcessXML(xml);
		
		
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>	
		if (log.isInfoEnabled()){
			log.info("Este es el envio obtenerDescuentosFiltrandoCaducados.js: "+envio);	
		}
	}
	
	var respuesta;
	
	try {
		respuesta = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}	
	
	//var respuesta = new XML(service.process(methodpost,envio));
	
	if (respuesta!=null) {
		respuesta = new XML(respuesta);
	}
	if (log.isInfoEnabled()){
		log.info("Esta es la respuesta obtenerDescuentosFiltrandoCaducados.js: "+respuesta.toXMLString());	
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
	
}


function preProcessXML(param) {
	
	if(param.caducados.text().length() == 0){
		param.caducados = <caducados>0</caducados>
	}
	param = comun.borraElementosSinHijos(param.toXMLString());

	param = comun.deleteSelectedTag(param.toXMLString());
	param = new XML(param);
		
	return param;
	
}
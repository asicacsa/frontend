var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {

	if (log.isInfoEnabled()){
		log.info("Entrada a getEdicionAltaDescuentosPromocionales.action");
	}
		
	var xml = request.getParameter('xml');
	var methodpost = request.getParameter('servicio');
	var envio;

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	if (log.isInfoEnabled()){
		log.info("getEdicionAltaDescuentosPromocionales, xml de entrada: " + xml);
	}	
	
	xml = new XML(xml);	
	
	if (xml != null && xml != '') {
		
		envio = <servicio>
					<parametro>
						<int>
							{xml}
						</int>
					</parametro>
				</servicio>;	
		
	} else {
		envio  = <servicio>
					<parametro>
						<Descuentopromocional/>
					</parametro>
				</servicio>;
		methodpost = 'getXMLModel';
	}
	
	if (log.isInfoEnabled()){
		log.info("getEdicionAltaDescuentosPromocionales, envio: " + envio);
	}
	
	var result;
	
	try{
		result = services.process(methodpost+'SOAWrapper',envio);					
	} catch (ex) {
	 		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
	 }
		
		
	
	if (log.isInfoEnabled()){
		log.info("getEdicionAltaDescuentosPromocionales, respuesta del servicio: " + result);
	}
	
	if (result !=  null) {
		result = new XML(result);
		result = this.postProcessXML(result);		
	}
	
	
	if (log.isInfoEnabled()){
		log.info("getEdicionAltaDescuentosPromocionales, resultado final: " + result.toXMLString());
	}
	
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
}



function postProcessXML(xmlparam) {

	// nos aseguramos que la colección de productos promocionados
	// tiene al menos un objeto vacío...
	//log.info('xmlparam: ' + xmlparam);
	//log.info('xmlparam.descprodpromocionados.Descprodpromocionado: ' + xmlparam.descprodpromocionados.Descprodpromocionado);
	//log.info('xmlparam.descprodpromocionados.Descprodpromocionado.length(): ' + xmlparam.descprodpromocionados.Descprodpromocionado.length());
	
	if (xmlparam.descprodpromocionados.Descprodpromocionado.length() == 0){
		xmlparam.descprodpromocionados = <descprodpromocionados>
											<Descprodpromocionado>
												<producto>
													<idproducto/>
												</producto>
											</Descprodpromocionado>
										</descprodpromocionados>;
	}
	
	// si el tipo de promoción es "NxM", los valores de descuento han de ser enteros
	if(xmlparam.tipopromocion.idtipopromocion.text() == 1){
		xmlparam.descuento = <descuento>{Math.floor(xmlparam.descuento.text())}</descuento>;
		xmlparam.descuento2 = <descuento2>{Math.floor(xmlparam.descuento2.text())}</descuento2>;
	}
	
	return xmlparam;	
}
var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {
		
	var xml = request.getParameter('xml');
	
	var methodpost = request.getParameter('servicio');


	//log.info("LOG(INFO) : Call to getEdicionAltaPerfil."+this.getClass().getName());
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var resultado;
	var envio;
	if (xml!=null) {
		envio  = <servicio>
					<parametro>
						<int>
							{xml}
						</int>
					</parametro>
				</servicio> 
	} else {
		envio  = <servicio>
					<parametro>
						<Perfilusuario/>
					</parametro>
				</servicio>;
		methodpost = 'getXMLModel';
	}
	
	//log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());
		
	
	//log.info("LOG(INFO) : with this parameters : "+envio+' : '+this.getClass().getName());					
	var aux;
	
	try {
		aux = services.process(methodpost+'SOAWrapper',envio);				
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	

	//log.info("LOG(INFO) : result : "+aux+' : '+this.getClass().getName());
	aux= aux.trim(); // AVC. Modificado por error al devolver el XML. 05-10-2017
	if (aux != null) {
		resultado = new XML(aux);
		if (resultado.hasOwnProperty('Perfilusuario')) {
			resultado = resultado.Perfilusuario;
		} else {
			if (!(resultado.name() == 'Perfilusuario')) {
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				log.error('LOG(ERROR) : result does not Perfilusuario : '+ex);
				var ex = new java.lang.Exception('Exception calling this service : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				throw ex;
			}
		}			
		resultado = this.postProcessXML(resultado);
		//log.info("LOG(INFO) : result post processed : "+resultado+' : '+this.getClass().getName());	
				
		//log.info('Fin de la edicion del valor de retorno : '+resultado.toXMLString());								
	}		
	//log.info("LOG(INFO) : final response : "+resultado+' : '+this.getClass().getName());
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
function postProcessXML(xmlparam) {
	
	return xmlparam;	
}
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
			
/**
dadodebaja : Integer
idtaquilla : Integer
nombre : String
orden : BigDecimal
recinto : Recinto
ubicacion : Ubicacion
**/
			
function handle(request, response) {
		
	var xml = request.getParameter('xml');	
	var methodpost = request.getParameter('servicio');


	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var resultado;
	var envio;
	
	if (xml!=null) {
		envio  = <servicio>
					<parametro>
						<int>
							{xml}
						</int>
					</parametro>
				</servicio> 
	} else {
		envio  = <servicio>
					<parametro>
						<Grupo/>
					</parametro>
				</servicio>;
		methodpost = 'getXMLModel';
	}
					
	var aux;

	try {
		aux = services.process(methodpost+'SOAWrapper',envio);				
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	aux= aux.trim(); // AVC. Modificado por error al devolver el XML. 10-10-2017
	if (aux != null) {
		resultado = new XML(aux);
		if (resultado.hasOwnProperty('Grupo')) {
			resultado = resultado.Grupo;
		} else {
			if (!(resultado.name() == 'Grupo')) {
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				log.error('LOG(ERROR) : result does not Grupo : '+ex);
				var ex = new java.lang.Exception('Exception calling this service : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				throw ex;
			}
		}			
		resultado = this.postProcessXML(resultado);
		
				
		//log.info('Fin de la edicion del valor de retorno : '+resultado.toXMLString());								
	}		

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
function postProcessXML(xmlparam) {
	
	return xmlparam;	
}
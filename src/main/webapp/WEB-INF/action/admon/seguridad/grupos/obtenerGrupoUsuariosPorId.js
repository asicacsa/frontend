var services = context.beans.getBean('httpServiceSOA');			
var modelService = 'getXMLModel';
		
function handle(request, response) {
		
	
	//Aceptaremos como parametro un xml de entrada, por ahora no haremos nada con el.
	var xml =  request.getParameter('xml');
		
	var methodpost = 'obtenerGrupoUsuariosPorId';

	// si servicio no se envia, intenta obtenerlo del nombre del action
	
	var envio = <servicio>
					<parametro>
						<int>2</int>
					</parametro>
				</servicio>;
	var resultado;
	//Este sera un servicio compuesto. Es decir, los parametros de la busqueda y el contenido de los combo.
	
	try {
		resultado = services.process(methodpost+'SOAWrapper',envio);					
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
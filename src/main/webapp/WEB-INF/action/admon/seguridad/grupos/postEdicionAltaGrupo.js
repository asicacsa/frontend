var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var servicio_alta ='darDeAltaGrupo';
var servicio_edicion ='actualizarGrupos';
var comun = sbf.getBean('transformerXML.js');

/**
dadodebaja : Integer
idtaquilla : Integer
nombre : String
orden : BigDecimal
recinto : Recinto
ubicacion : Ubicacion

**/
function handle(request, response) {
		
	var xml = request.getParameter('xml');
	var grupo_constant= 'Grupo';
	var id_constant = 'idgrupo';		
	
	var resultado;
	var methodpost;		
	var envio;

	
	if (xml!=null) {
		xml = new XML(xml);
		
		if (xml.hasOwnProperty(grupo_constant)) {
			//Preparamos el xml para enviarlo con <list/>, <arrayList/>, ya vorem...
			xml = xml.Grupo;
		} else if (!(xml.name() == grupo_constant)) {

			var ex = new java.lang.Exception('Exception in postEdicionAltaGrupo : '+this.getClass().getName());
			log.error('LOG(ERROR) : does not exist Grupo in the parameters of the call.',ex);			
			throw ex;
		}// Si su nombre es Unidadnegocio es que 

		
		xml = this.preProcessXML(xml);
		
		//Ahora creamos el envio...
		//var methodpost = request.getParameter('servicio');
		

		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicio de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.		
		if(methodpost == null) {
			//Si es un alta no poseera la propiedad idproducto
			if (!xml.hasOwnProperty('idgrupo')) {
				methodpost = servicio_alta;				
				envio = 
					<servicio>
						<parametro>
							{xml}
						</parametro>
					</servicio>
				
			} else {
				methodpost = servicio_edicion;
				envio = 
					<servicio>
						<parametro>
							<list>
								{xml}
							</list>
						</parametro>
					</servicio>				
			}
		}		
		
		//Y llamamos al servicio...
		var aux;
	
		try {
		 	aux = services.process(methodpost+'SOAWrapper',envio);
		 } catch (ex) {
		 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		 }
	
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		
		resultado = 
				<ok>
					{aux}
				</ok>
		//log.info("LOG(INFO) : result : "+resultado+' : '+this.getClass().getName());				
		//En aux tenemos la respuesta			
	} else {
		var ex = new java.lang.Exception('Exception in postEdicionAltaGrupo : '+this.getClass().getName());
		log.error('LOG(ERROR) : does not exist params in the call.',ex);			
		throw ex;
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {	
	
	
	if (param != null) {
		param = new XML(param);
		param = comun.borraElementosSinHijos(param.toXMLString());
		param = this.preProcessUsuarios(param);
	}
	return param;
	
}
function preProcessUsuarios(param) {
	for each (i in param.grupousuarios.Grupousuario.usuario) {
		delete i.selected;
		//borramos el nombre completo ya que no es un campo en sí de cliente.
		//Sólo se recuperó como campo combinado para el listado de usuarios de grupos.
		delete i.nombrecompleto;
	}
	return param;
}
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	if (log.isInfoEnabled()){
		log.info("entrada a postBuscarUsuarios.action");
	}
	
	var xml = request.getParameter('xml');
	var methodpost = request.getParameter('servicio');
	var resultado;	
	var envio;
	
	if (log.isInfoEnabled()){
		log.info("postBuscarUsuarios, xml entrada: " + xml);
		log.info("postBuscarUsuarios, methodpost: " + methodpost);
	}
	
	
	if (xml != null) {
		
		xml = new XML(xml);
		xml = xml.Buscarusuarioparam;
		xml = this.preProcessXML(xml);
		
		var envio =
				<servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>
		
		var aux;

		try {
		 	aux = services.process(methodpost+'SOAWrapper',envio);
		 } catch (ex) {
		 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		 }
		
		
		if (aux != null) {
			resultado = new XML(aux.trim());
		} else {
			resultado = <ArrayList/>
		}
		
	} else {
		// XML NULL
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : input xml is null');
		throw ex.javaException;
	}
	
	
	resultado = this.postProcessXML(resultado);
	resultado = resultado.toXMLString();
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado);
}


function preProcessXML(param) {	
	
	if (param != null) {
		param = new XML(param);
		param = comun.borraElementosSinHijos(param.toXMLString());
	}
	
	// si buscamos activos, obviamos 
	// el resto de parametros
	if (param.activo.text()==1){
		param = <Buscarusuarioparam>
					<activo>1</activo>
					<numeroprimerregistro>{param.numeroprimerregistro.text()}</numeroprimerregistro>
				</Buscarusuarioparam>;
	}
	
	return param;	
}


function postProcessXML(param) {
	if (param != null) {
		param = new XML(param);
		param = this.postProcessAuditados(param);
		param = this.postProcessBloqueados(param);		
	}
	return param;
}


function postProcessAuditados(xmlparam) {
	for each (i in xmlparam.Usuario) {
		if (i.auditado.text() == 0) {
			i.auditado=<auditado>false</auditado>
		} else if (i.auditado.text() == 1) {
			i.auditado=<auditado>true</auditado>
		} else {
			log.warn("LOG(WARN) : postBuscarUsuarios. El dato auditado llega con valor distinto a 0 ? 1.")
		}
	}
	return xmlparam;
}


function postProcessBloqueados(xmlparam) {
	for each (i in xmlparam.Usuario) {
		if (i.cuentabloqueada.text() == 0) {
			i.cuentabloqueada=<cuentabloqueada>false</cuentabloqueada>
		} else if (i.cuentabloqueada.text() == 1) {
			i.cuentabloqueada=<cuentabloqueada>true</cuentabloqueada>
		} else {
			log.warn("LOG(WARN) : postBuscarUsuarios. El dato cuentabloqueada llega con valor distinto a 0 ? 1.")
		}
	}
	return xmlparam;
}
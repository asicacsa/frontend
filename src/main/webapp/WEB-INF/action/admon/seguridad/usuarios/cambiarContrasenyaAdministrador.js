var services = context.beans.getBean('httpServiceSOA');
//var servicio_contr ='cambiarContrasenyaAdministrador';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {


	var idusuario = request.getParameter('idusuario');
	var newPass = request.getParameter('newPass');
	var confPass = request.getParameter('confPass');	
	
	var methodpost = request.getParameter('servicio');		
	
	var envio;
		
	var resultado;
	
	//log.info("LOG(INFO) : Call to cambiarContrasenyaUsuarios : "+this.getClass().getName());
				
	envio = 
		<servicio>
			<parametro>
					<idUsuario>{idusuario}</idUsuario>
					<contrasenyaNueva>{newPass}</contrasenyaNueva>
					<confirmacionContrasenyaNueva>{confPass}</confirmacionContrasenyaNueva>
			</parametro>
		</servicio>

	//envio = preProcessXML(envio);
	
	//log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());			
	//log.info("LOG(INFO) : with this parameters : "+envio+' : '+this.getClass().getName());
	
	//Y llamamos al servicio...
	var aux;

	try {
	 	aux = services.process(methodpost+'SOAWrapper',envio);
	 } catch (ex) {
	 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	 }
		
	//En esta accion no hay que realizar ningun postProceso del XML devuelto.		
	resultado = 
			<ok>
				{aux}
			</ok>
	//log.info("LOG(INFO) : result : "+resultado+' : '+this.getClass().getName());				
	//En aux tenemos la respuesta			
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	// necesario ya que pasamos un Integer que podría ser null
	if (param != null) {
		param = new XML(param);
		param = comun.borraElementosSinHijos(param.toXMLString());
	}
	return param;	
}

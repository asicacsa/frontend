var services = context.beans.getBean('httpServiceSOA');			
var modelService = 'getXMLModel';
		
function handle(request, response) {
		
	//log.info("LOG(INFO) : Call to getBuscarUsuarios."+this.getClass().getName());
	
	//Aceptaremos como parametro un xml de entrada, por ahora no haremos nada con el.
	var xml =  request.getParameter('xml');
		
	var methodpost = 'getXMLModel';
	
	
	var envio = <servicio>
					<parametro>
						<Buscarusuarioparam/>
					</parametro>
				</servicio>;
	var resultado;
	//Este sera un servicio compuesto. Es decir, los parametros de la busqueda y el contenido de los combo.
	
	//log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());	

	try {
		resultado = services.process(methodpost+'SOAWrapper',envio);					
		resultado = new XML(resultado);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	
	//log.info("LOG(INFO) : result : "+resultado+' : '+this.getClass().getName());		
	//log.info("LOG(INFO) : final response : "+resultado+' : '+this.getClass().getName());
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
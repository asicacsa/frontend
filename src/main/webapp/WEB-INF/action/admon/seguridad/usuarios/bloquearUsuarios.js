// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
	
	//log.info("Entrada a bloquearUsuarios.action");
	
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}

	var xml = request.getParameter('xml');
	var envio;
	var result;
	
	//log.info('Este es el xml que nos llega para bloquearUsuarios: '+xml);
	
	if(xml != null ) {

		xml = new XML(xml);
		//xml = xml.BloqueoBajaClienteParam;
		
		envio = 
			<servicio>
				{xml}
			</servicio>
	
		var aux;
		
		try {
		 	aux = services.process(methodpost+'SOAWrapper',envio);
		 } catch (ex) {
	 		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		 }
		 
		
		result = <ok>
					{aux}
				 </ok>;
					 		
	} else {
		//log.info("El xml que llega a bloquearUsuarios es nulo!!!");
		result = <error/>
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
}
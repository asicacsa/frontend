var services = context.beans.getBean('httpServiceSOA');				
function handle(request, response) {
		
	var xml = request.getParameter('xml');
	
	//log.info("LOG(INFO) : Call to getEdicionAltaUsuarios."+this.getClass().getName());
		
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var resultado;
	var envio;
	if (xml!=null) {
		envio  = <servicio>
					<parametro>
						<int>
							{xml}
						</int>
					</parametro>
				</servicio> 
	} else {
		envio  = <servicio>
					<parametro>
						<Usuario/>
					</parametro>
				</servicio>;
		methodpost = 'getXMLModel';
	}
	
	
	//log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());
			
	//log.info("LOG(INFO) : with this parameters : "+envio+' : '+this.getClass().getName());					
	var aux;

	try {
		aux = services.process(methodpost+'SOAWrapper',envio);				
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}

	
	if (aux!=null) {
		resultado = new XML(aux);
		resultado = this.postProcessXML(resultado);
	} else {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result does not Usuario : '+ex);
		var ex = new java.lang.Exception('Exception calling this service : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		throw ex;	
	}
	
	
	//log.info("LOG(INFO) : final response : "+resultado+' : '+this.getClass().getName());
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
function postProcessXML(param) {	
	
	if (param != null) {
		param = new XML(param);		
		if (param.cuentabloqueada.text() == 0) {
			param.cuentabloqueada=<cuentabloqueada>false</cuentabloqueada>
		} else if (param.cuentabloqueada.text() == 1) {
			param.cuentabloqueada=<cuentabloqueada>true</cuentabloqueada>
		} else {
			log.warn("LOG(WARN) : getEdicionAltaUsuarios. El dato activada llega con valor distinto a 0 ? 1.")
		}		
		if (param.auditado.text() == 0) {
			param.auditado=<auditado>false</auditado>
		} else if (param.auditado.text() == 1) {
			param.auditado=<auditado>true</auditado>
		} else {
			log.warn("LOG(WARN) : getEdicionAltaUsuarios. El dato activada llega con valor distinto a 0 ? 1.")
		}				
		//param = comun.borraElementosSinHijos(param.toXMLString());
		//param = this.preProcessRedunidadnegocio(param);
	}
	return param;
	
}
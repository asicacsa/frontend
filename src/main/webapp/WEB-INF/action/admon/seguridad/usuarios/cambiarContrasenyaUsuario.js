var services = context.beans.getBean('httpServiceSOA');
//var servicio_contr ='cambiarContrasenyaUsuario';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {

	
	var contrasenyaactual = request.getParameter('contrasenyaactual');
	var confirmacioncontrasenyanueva = request.getParameter('confirmacioncontrasenyanueva');
	var contrasenyanueva = request.getParameter('contrasenyanueva');			
	
	var methodpost = request.getParameter('servicio');
	
	var envio;
	var resultado;
		
	//log.info("LOG(INFO) : Call to cambiarContrasenyaUsuarios : "+this.getClass().getName());			
	
	envio = 
		<servicio>
			<parametro>
				<contrasenyaActual>{contrasenyaactual}</contrasenyaActual>
				<contrasenyaNueva>{contrasenyanueva}</contrasenyaNueva>
				<confirmacionContrasenyaNueva>{confirmacioncontrasenyanueva}</confirmacionContrasenyaNueva>
			</parametro>
		</servicio>

	//log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());			
	//log.info("LOG(INFO) : with this parameters : "+envio+' : '+this.getClass().getName());
	
	//Y llamamos al servicio...
	var aux;

	try {
	 	aux = services.process(methodpost+'SOAWrapper',envio);
	 } catch (ex) {
	 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	 }
		
	//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		
	resultado = 
			<ok>
				{aux}
			</ok>
	//log.info("LOG(INFO) : result : "+resultado+' : '+this.getClass().getName());				
	//En aux tenemos la respuesta			
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}


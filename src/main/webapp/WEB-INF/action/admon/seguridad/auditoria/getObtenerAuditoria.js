var services = context.beans.getBean('httpServiceSOA');
var service_estadoauditoria = 'esAuditoriaActivada';
var service_funcionalidades = 'obtenerListadoFuncionalidadesAuditadas';
var service_usuarios = 'obtenerListadoUsuariosAuditados';
function handle(request, response) {	
	
	//log.info("LOG(INFO) : Call to obtenerAuditoria."+this.getClass().getName());
	
	var envio = <servicio>
					<parametro/>
				</servicio>
					
	
	//Este es un js compuesto, tendremos que recuperar los usuarios auditados
	// y las funcionalidades auditadas.
	var auditoria_activada = false;
	
	try {
		auditoria_activada = services.process(service_estadoauditoria+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+service_usuarios+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	
	var usuarios_auditados;

	try {
		usuarios_auditados = services.process(service_usuarios+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+service_usuarios+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	
	var funcionalidades_auditadas;
	
	try {
		funcionalidades_auditadas = services.process(service_funcionalidades+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+service_funcionalidades+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex;
	}
	
	
	
	var resul;
	
	resul = this.postProcess(usuarios_auditados,auditoria_activada,funcionalidades_auditadas);
	//log.info("LOG(INFO) : final response : "+resul+' : '+this.getClass().getName());
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resul.toXMLString());
}
function postProcess(usuarios,auditoria,funcionalidades) {
	var model = <Auditoria>												
				</Auditoria>;
				
	if (usuarios != null) {
		usuarios = new XML(usuarios);
		usuarios.setName('usuarios');
	}
	model.usuarios = usuarios;
	
	if (funcionalidades != null) {
		funcionalidades = new XML(funcionalidades);
		funcionalidades.setName('funcionalidades');
	}
	model.funcionalidades = funcionalidades;
	
	if (auditoria != null) {
		auditoriaActivada = new XML(auditoria);
		auditoriaActivada.setName('auditoriaActivada');
	}
	model.auditoriaActivada = auditoriaActivada;
	
	return model;	
}
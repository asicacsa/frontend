var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
/**
dadodebaja : Integer
idtaquilla : Integer
nombre : String
orden : BigDecimal
recinto : Recinto
ubicacion : Ubicacion

**/
function handle(request, response) {
		
	//log.info("LOG(INFO) : Call to postObtenerAuditoria : "+this.getClass().getName());
	
	var xml = request.getParameter('xml');
	var methodpost = 'actualizarAuditoria';
	
	var envio;
	var resultado;
	
	//log.info("LOG(INFO) : El xml de entrada : "+xml);
	
	if (xml!=null) {
		xml = new XML(xml);
		xml = xml.Auditoria;	
		
		var bool;
		//log.info("LOG(INFO) : params before the transformation, auditoria : "+xml.auditoriaActivada.toXMLString());	
		if (xml.hasOwnProperty('auditoriaActivada')) {
			bool = xml.auditoriaActivada.text();
		} else {
			bool='';
		}
		
		var users;
		//log.info("LOG(INFO) : params before the transformation, users : "+xml.usuarios.toXMLString());
		if (xml.hasOwnProperty('usuarios')) {
			users = this.preProcessUsers(xml.usuarios);
		} else {
			users ='';
		}
		
		var funcionalidades;
		//log.info("LOG(INFO) : params before the transformation, funcionalidades : "+xml.funcionalidades.toXMLString());		
		if (xml.hasOwnProperty('funcionalidades')) {
			funcionalidades = this.preProcessFuncionalidades(xml.funcionalidades);
		} else {
			funcionalidades = '';
		}
		
		//log.info("LOG(INFO) : params after the transformation, auditoria : "+bool);
		//log.info("LOG(INFO) : params after the transformation, users : "+users);
		//log.info("LOG(INFO) : params after the transformation, funcionalidades : "+funcionalidades);
				
		var envio = <servicio>
						<parametro>
							<java.lang.Boolean>{bool}</java.lang.Boolean>
							{funcionalidades}
							{users}
						</parametro>
					</servicio>		
		
		//log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());			
		//log.info("LOG(INFO) : with this parameters : "+envio+' : '+this.getClass().getName());
		
		//Y llamamos al servicio...
		var aux;
	
		try {
		 	aux = services.process(methodpost+'SOAWrapper',envio);
		 } catch (ex) {
		 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		 }
		 
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		
		resultado = 
				<ok>
					{aux}
				</ok>
		//log.info("LOG(INFO) : result : "+resultado+' : '+this.getClass().getName());				
		//En aux tenemos la respuesta			
	} else {
		var ex = new java.lang.Exception('Exception in postEdicionAltaTaquilla : '+this.getClass().getName());
		log.error('LOG(ERROR) : does not exist params in the call.',ex);			
		throw ex;
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
function preProcessUsers(param) {
	for each (i in param.Usuario) {
		delete i.selected;
	}
	param.setName('java.util.List');
	return param;
}
function preProcessFuncionalidades(param) {	
	for each (i in param.Funcionalidad) {
			if (i.activada.text() == false) {
				i.activada=<activada>0</activada>
			} else if (i.activada.text() == true) {
				i.activada=<activada>1</activada>
			} else {
				log.warn("LOG(WARN) : postActualizarAuditoria. El dato activada llega con valor distinto a 0 ? 1.")
			}
			delete i.selected;
		}
	param.setName('java.util.List');		
	return param;	
}
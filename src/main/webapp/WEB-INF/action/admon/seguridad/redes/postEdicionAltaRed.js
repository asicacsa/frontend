var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaRed';
var servicio_edicion ='actualizarRedes';

/**
dadodebaja : Integer
idtaquilla : Integer
nombre : String
orden : BigDecimal
recinto : Recinto
ubicacion : Ubicacion

**/
function handle(request, response) {
		
	var xml = request.getParameter('xml');
	var red_constant= 'Red';
	var id_constant = 'idred';		
	
	var resultado;
	
	var methodpost;
		
	var envio;
	
	//log.info("LOG(INFO) : Call to postEdicionAltaRed : "+this.getClass().getName());
	
	if (xml!=null) {
		xml = new XML(xml);
		
		if (xml.hasOwnProperty(red_constant)) {
			//Preparamos el xml para enviarlo con <list/>, <arrayList/>, ya vorem...
			xml = xml.Red;
		} else if (!(xml.name() == red_constant)) {

			var ex = new java.lang.Exception('Exception in postEdicionAltaRed : '+this.getClass().getName());
			log.error('LOG(ERROR) : does not exist Red in the parameters of the call.',ex);			
			throw ex;
		}// Si su nombre es Unidadnegocio es que 

		//log.info("LOG(INFO) : params before the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		xml = this.preProcessXML(xml);
		//log.info("LOG(INFO) : params after the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		//xml = this.preProcessXML(xml);
		
		
		//Ahora creamos el envio...
		//var methodpost = request.getParameter('servicio');
		

		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicio de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.		
		if(methodpost == null) {
			//Si es un alta no poseera la propiedad idproducto
			if (!xml.hasOwnProperty('idred')) {
				methodpost = servicio_alta;				
				envio = 
					<servicio>
						<parametro>
							{xml}
						</parametro>
					</servicio>
				
			} else {
				methodpost = servicio_edicion;
				envio = 
					<servicio>
						<parametro>
							<list>
								{xml}
							</list>
						</parametro>
					</servicio>				
			}
			
		}		
		
		//log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());			
		//log.info("LOG(INFO) : with this parameters : "+envio+' : '+this.getClass().getName());
		//Y llamamos al servicio...
		var aux;

		try {
		 	aux = services.process(methodpost+'SOAWrapper',envio);
		 } catch (ex) {
		 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex;
		 }
	
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		
		resultado = 
				<ok>
					{aux}
				</ok>
		//log.info("LOG(INFO) : result : "+resultado+' : '+this.getClass().getName());				
		//En aux tenemos la respuesta			
	} else {
		var ex = new java.lang.Exception('Exception in postEdicionAltaRed : '+this.getClass().getName());
		log.error('LOG(ERROR) : does not exist params in the call.',ex);			
		throw ex;
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {	
	
	if (param != null) {
	
		param = new XML(param);
		param = this.preProcessRedunidadnegocio(param);
	}
	if(param.nombre==""){
	 throw new java.lang.Exception("El nombre es obligatorio.")
	}
	
	if(param.usuario.idusuario==""){
	 throw new java.lang.Exception("El administrador es obligatorio.")
	}

	return param;
	
}
function preProcessRedunidadnegocio(param) {
	for each (i in param.redunidadnegocios.Redunidadnegocio.unidadnegocio) {
		delete i.selected;
	}
	return param;
}
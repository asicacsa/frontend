var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
/**
dadodebaja : Integer
idtaquilla : Integer
nombre : String
orden : BigDecimal
recinto : Recinto
ubicacion : Ubicacion

**/
function handle(request, response) {
		
	var xml = request.getParameter('xml');
	
	var resultado;
	
	var methodpost = 'actualizarFuncionalidades';
		
	var envio;
	
	//log.info("LOG(INFO) : Call to postActualizarFuncionalidades : "+this.getClass().getName());
	//log.info("LOG(INFO) : El xml de entrada : "+xml);
	if (xml!=null) {
		xml = new XML(xml);
		var aux;
		aux = <list>{xml.descendants('Funcionalidad')}</list>;
		aux = this.preProcessXML(aux);
		var envio = <servicio>
						<parametro>
							{aux}
						</parametro>
					</servicio>		
		
		//log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());			
		//log.info("LOG(INFO) : with this parameters : "+envio+' : '+this.getClass().getName());
		//Y llamamos al servicio...
		var aux;
	
		try {
		 	aux = services.process(methodpost+'SOAWrapper',envio);
		 } catch (ex) {
		 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		 }
		
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		
		resultado = 
				<ok>
					{aux}
				</ok>
		//log.info("LOG(INFO) : result : "+resultado+' : '+this.getClass().getName());				
		//En aux tenemos la respuesta			
	} else {
		var ex = new java.lang.Exception('Exception in postActualizarFuncionalidades : '+this.getClass().getName());
		log.error('LOG(ERROR) : does not exist params in the call.',ex);			
		throw ex;
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
function preProcessXML(param) {
	if (param != null) {
		for each (i in param.Funcionalidad) {
			if (i.activada.text() == false) {
				i.activada=<activada>0</activada>
			} else if (i.activada.text() == true) {
				i.activada=<activada>1</activada>
			} else {
				log.warn("LOG(WARN) : obtenerClasificacionesFuncionalidades. El dato activada llega con valor distinto a 0 ? 1.")
			}		
			delete i.selected;
		}
	}
	return param;
}
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');
function handle(request, response) {	
	
	//var methodpost = 'obtenerClasificacionesFuncionalidades';		
	var methodpost = request.getParameter('servicio');
	var listaIdsFuncionalidades = request.getParameter('listaIdsFuncionalidades');
	
	
	// si xml no se envia, llama al metodo sin parametros	
	var selecciona = null;
	
	// En este tipo de servicio no van a haber parametros de Entrada.
	if(listaIdsFuncionalidades != ''){
		listaIdsFuncionalidades = new XML(listaIdsFuncionalidades);
		
		for each (var i in listaIdsFuncionalidades.idfuncionalidad) {
			i.setName('int');
		}
	
		var envio = <servicio>
						<parametro>
							{listaIdsFuncionalidades}
						</parametro>
					</servicio>		
	}else{
		var envio = <servicio>
						<parametro>							
						</parametro>
					</servicio>		
	
	}
	
	var result;
	
	try {
		result = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
	 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}

	
	if (result !=  null) {
		result = new XML(result);
		result = this.postProcessXML(result);		
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
	
}
function postProcessXML(xmlparam) {
	if (xmlparam != null) {
		xmlparam = new XML(xmlparam);	
		for each (i in xmlparam..*.Funcionalidad) {
			if (i.activada.text() == 0) {
				i.activada=<activada>false</activada>
			} else if (i.activada.text() == 1) {
				i.activada=<activada>true</activada>
			} else {
				log.warn("LOG(WARN) : obtenerClasificacionesFuncionalidades. El dato activada llega con valor distinto a 0 ? 1.")
			}
		}
	}	
	return xmlparam;
}
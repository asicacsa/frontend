var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaTurno';
var servicio_edicion ='actualizarTurnos';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost;
	var envio;
	
	//log.info("Entra en el action postEdicionAltaTurnos");
	
	if (xml!=null) {
		xml = new XML(xml);
		if (log.isInfoEnabled()) {
			log.info("Este es el xml que llega a postEdicionAltaTurnos: "+xml.toXMLString());
		}
		xml = xml.Turno;
		

		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicion de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.
		
		if(methodpost == null) {
			//Si es un alta no poseera la propiedad id
			// ALTA
			if (xml.idturno.text().length() == 0) {
				methodpost = servicio_alta;
				xml = this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							{xml}
						</parametro>
					</servicio>
			// EDICION	
			} else {
				methodpost = servicio_edicion;
				xml= this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							<list>
								{xml}
							</list>
						</parametro>
					</servicio>			
			}
		}		
		
		if (log.isInfoEnabled()) {
			//log.info("Metodo post de postEdicionAltaTurnos: "+methodpost);
			log.info("Envio a postEdicionAltaTurnos: "+envio);
		}
		
		//Y llamamos al servicio...
		var aux = services.process(methodpost+'SOAWrapper',envio);
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		resultado = 
				<ok>
					{aux}
				</ok>
		if (log.isInfoEnabled()) {
			log.info("Resultado de postEdicionAltaTurnos: " + resultado.toXMLString());
		}
					
	} else {
		//log.info("El xml que llega a postEdicionAltaTurnos es nulo!!!");
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	//log.info("Lo que vamos a pasar por preProcessXML : "+param);
	
	param = new XML(param);
	param = comun.borraElementosSinHijos(param.toXMLString());
	
	//log.info("Despues del borrado de elementos sin hijos: "+param);
	
	return param;
	
}

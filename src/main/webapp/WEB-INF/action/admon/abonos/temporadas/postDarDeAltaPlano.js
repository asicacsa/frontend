var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaPlano';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost;
	var envio;
	
	//log.info("Entrada a postDarDeAltaPlano.action");
	
	if (xml!=null) {
		//log.info("Este es el xml que llega a postDarDeAltaPlano: " + xml);
		xml = new XML(xml);
		xml = xml.AltaPlanoParam;
			
		methodpost = servicio_alta;
		xml = this.preProcessXML(xml);
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>
		
		//log.info("Este es el envio : "+envio);
		//log.info("Este es el metodo post : "+methodpost);
		
		//Y llamamos al servicio...
		var aux = services.process(methodpost+'SOAWrapper',envio);
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		resultado = <ok>{aux}</ok>
					
	} else {
		//log.info("El xml que llega a postDarDeAltaPlano es nulo!!!");
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}


function preProcessXML(param) {
	param = new XML(param);
	param = comun.borraElementosSinHijos(param.toXMLString());

	delete param..selected; //Eliminamos todos los tags selected
	
	return param;	
}
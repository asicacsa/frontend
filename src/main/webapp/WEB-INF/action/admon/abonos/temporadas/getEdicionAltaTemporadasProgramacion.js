var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');
var model = <Temporadaprogramacion>
				<idtemporadaprogramacion></idtemporadaprogramacion>
				<nombre></nombre>
				<fechainicio></fechainicio>
				<fechafin></fechafin>
			</Temporadaprogramacion>;


function handle(request, response) {

	//log.info("Entrada a getEdicionAltaTemporadasProgramacion.action");
		
	var xml = request.getParameter('xml');
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var resultado;
	
	//log.info("Este es el xml que llega a getEdicionAltaTemporadasProgramacion.action: " + xml);	
	
	xml = new XML(xml);
	
	if (xml != null && xml != '') {
		
		var envio = <servicio>
						<parametro>
							<int>
								{xml}
							</int>
						</parametro>
					</servicio>
					
		//log.info("Este es el metodo al que vamos a llamar : "+methodpost);
		var aux = services.process(methodpost+'SOAWrapper',envio);
		if (log.isInfoEnabled()) {					
			log.info('Esto es lo que devuelve el servicio : ' + aux);
		}
		
		if (aux != null) {
			resultado = new XML(aux);							
		}		
		
	} else {
		resultado = model;
	}
	
	resultado = this.postProcessXML(resultado);
	if (log.isInfoEnabled()) {
		log.info("Esto es lo que devuelve getEdicionAltaTemporadasProgramacion.action: " + resultado.toXMLString());
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function postProcessXML(param) {
	//log.info("Estamos postProcesando : " + param.toXMLString());
	
	// formateo de fechainicio
	var aux = param.fechainicio.text();

	if (aux.length()>0){
		aux = aux.substr(0,aux.indexOf('-'));
		param.fechainicio = <fechainicio>{aux}</fechainicio>;
	}
	
	// formateo de fechafin
	aux = param.fechafin.text();
	
	if (aux.length()>0){
		aux = aux.substr(0,aux.indexOf('-'));
		param.fechafin = <fechafin>{aux}</fechafin>;	
	}
	
	return param;
}

/*function postProcessXML(xmlparam) {
	xmlparam = comun.deleteTrash(xmlparam.toXMLString());
	xmlparam = comun.totalMatch(xmlparam,'@idref');
	return xmlparam;
}*/
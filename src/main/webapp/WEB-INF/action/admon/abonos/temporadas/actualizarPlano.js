var services = context.beans.getBean('httpServiceSOA');
var servicio_edicion ='actualizarPlano';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
	
	log.info("Entrada a actualizarPlano.action");
							
	var localidades = request.getParameter('localidades');	
	var newzona = request.getParameter('newzona');	
	
	var methodpost = servicio_edicion;
	var envio;
	var resultado;
	
	
	if (localidades!=null && newzona!=null) {
		
		log.info("		localidades: " + localidades.substr(0,300));
		log.info("		newzona: " + newzona);
		
		localidades = new XML(localidades);
		localidades.setName('java.util.List');
		envio = 
			<servicio>
				<parametro>
					{localidades}
					<int>{newzona}</int>
				</parametro>
			</servicio>	
			
		log.info("Este es el envio : "+envio);
		log.info("Este es el metodo post : "+methodpost);

		//Y llamamos al servicio...
		var aux = services.process(methodpost+'SOAWrapper',envio);

		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		resultado = <ok>{aux}</ok>;			
					
	} else {
		log.info("Alguno de los parámetros obligatorios que llegan a actualizarPlano es nulo!!!");
		resultado = <error/>
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}


/*function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	log.info("Lo que vamos a pasar por preProcessXML : "+param);
	
	param = comun.borraElementosSinHijos(param.toXMLString());
	
	param = comun.deleteSelectedTag(param.toXMLString());
	
	param = new XML(param);
	
	// fuera nombrecompleto
	delete param..nombrecompleto;
	
	// procesamos los true y false
	if (param.anulado.text() == 'true'){
		param.anulado = <anulado>1</anulado>;
	} else {
		param.anulado = <anulado>0</anulado>;
	} 
	
	return param;
	
}*/
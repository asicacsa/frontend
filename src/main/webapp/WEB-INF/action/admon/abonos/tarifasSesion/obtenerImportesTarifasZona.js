var services = context.beans.getBean('httpServiceSOA');

var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
	
	//log.info('Hemos entrado en obtenerImportesTarifasZona');	


	var idContenido = request.getParameter('idContenido');		
	var idSesion = request.getParameter('idSesion');		
	var idTipoventa = request.getParameter('idTipoventa');		
	var idTipoabono = request.getParameter('idTipoabono');		
	
	if (log.isInfoEnabled()) {
		log.info('Los parametros que me llegan...');
		log.info('El idContenido '+idContenido);
		log.info('El idSesion '+idSesion);
		log.info('El idTipoventa '+idTipoventa);
		log.info('El idTipoabono '+idTipoabono);
	}
	
	var resultado;
	var methodpost = 'obtenerImportesTarifasZona';
	var envio;
	
	if(idContenido == null && idSesion == null){
		var xml = <dto>	 				
	 				<idtipoventa>{idTipoventa}</idtipoventa>
	 				<idtipoabono>{idTipoabono}</idtipoabono>
			  	</dto>;
	}else{
		var xml = <dto>
	 				<idcontenido>{idContenido}</idcontenido>
	 				<idsesion>{idSesion}</idsesion>
	 				<idtipoventa>{idTipoventa}</idtipoventa>	 				
			  	</dto>;
		}
	xml = this.preProcessXML(xml);
	envio = 
		<servicio>
			<parametro>
				{xml}
			</parametro>
		</servicio>
		
	if (log.isInfoEnabled()) {
		log.info("Este es el envio : "+envio);	
	}
		
	
		//Y llamamos al servicio...
		var aux = services.process(methodpost+'SOAWrapper',envio);
		
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		if(aux != null){
		//log.info('aux es distinto de nulo');
			aux = new XML(aux);
			resultado = aux;
			
			if (log.isInfoEnabled()) {		
				log.info("Este es el resultado : "+resultado);
			}
		} else {
			resultado = new XML('<ok/>');
			if (log.isInfoEnabled()) {
				log.info("El aux es null");
			}
		}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
	
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	//log.info("Lo que vamos a pasar por preProcessXML : "+param);
	
	param = new XML(param);
	
	
	param = comun.borraElementosSinHijos(param.toXMLString());

	//log.info("Lo que enviamos preparado : "+param);

	return param;	
}
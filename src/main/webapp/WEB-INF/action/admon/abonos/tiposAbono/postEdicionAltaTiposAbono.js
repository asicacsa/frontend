var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaTipoAbono';
var servicio_edicion ='actualizarTipoAbono';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
		
	var xml = request.getParameter('xml');
	//var perfilusuario_constant= 'Perfilusuario';
	//var id_constant = 'idperfilusuario';		
	
	var resultado;
	
	var methodpost;
		
	var envio;
	
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : Call to postEdicionAltaTipoabono : "+this.getClass().getName());
	}
	
	if (xml!=null) {
		xml = new XML(xml);
		
		if (xml.hasOwnProperty('Tipoabono')) {
			//Preparamos el xml para enviarlo con <list/>, <arrayList/>, ya vorem...
			xml = xml.Tipoabono;
		} else if (!(xml.name() == 'Tipoabono')) {

			var ex = new java.lang.Exception('Exception in postEdicionAltaTipoabono : '+this.getClass().getName());
			if (log.isErrorEnabled()) {
				log.error('LOG(ERROR) : does not exist Tipoabono in the parameters of the call.',ex);
			}
			throw ex;
		}

		if (log.isInfoEnabled()) {
			log.info("LOG(INFO) : params before the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		}
		xml = this.preProcessXML(xml);
		if (log.isInfoEnabled()) {
			log.info("LOG(INFO) : params after the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		}
		
		//xml = this.preProcessXML(xml);
		
		
		//Ahora creamos el envio...
		//var methodpost = request.getParameter('servicio');
		

		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicio de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.		
		if(methodpost == null) {
			//Si es un alta no poseera la propiedad id
			if (!xml.hasOwnProperty('idtipoabono')) {
				methodpost = servicio_alta;				
				envio = 
					<servicio>
						<parametro>
							{xml}
						</parametro>
					</servicio>
				
			} else {
				methodpost = servicio_edicion;
				envio = 
					<servicio>
						<parametro>
							<list>
								{xml}
							</list>
						</parametro>
					</servicio>				
			}
		}		
		
		if (log.isInfoEnabled()) {
			log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());			
			log.info("LOG(INFO) : with this parameters : "+envio+' : '+this.getClass().getName());
		}
		//Y llamamos al servicio...
		var aux;
		if (log.isErrorEnabled()) {
			try {
			 	aux = services.process(methodpost+'SOAWrapper',envio);
			 } catch (ex) {
		 		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				log.error('LOG(ERROR) : result in this Exception : '+ex);
				throw ex.javaException;
			 }
		} else {
		 	aux = services.process(methodpost+'SOAWrapper',envio);
		}
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		
		resultado = 
				<ok>
					{aux}
				</ok>
		if (log.isInfoEnabled()) {
			log.info("LOG(INFO) : result : "+resultado+' : '+this.getClass().getName());
		}
		//En aux tenemos la respuesta			
	} else {
		var ex = new java.lang.Exception('Exception in postEdicionAltaTipoabono : '+this.getClass().getName());
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : does not exist params in the call.',ex);			
		}
		throw ex;
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {	
	
	//Datos obligatorios que no pueden estar vacios ...
	//log.info("Lo que vamos a pasar por preProcessXML : "+param);

	if ( isNaN(new Number(param.descuento.text())) ){
		throw new java.lang.Exception("El descuento debe ser numerico.")
	}
	
	param = new XML(param);
	
	if(param.sesiontipoabonos.descendants().length() != 0){
		delete param.sesiontipoabonos.Sesiontipoabono.sesion.nombre;
	}
	param = comun.borraElementosSinHijos(param.toXMLString());
	
	//Obligatorio : Campo dado de baja, por defecto lo ponemos a 0 ...
	if (param.dadodebaja.text().length() == 0) {
		param.dadodebaja=0;
	}

	//Al borrar eliminar el nodo, no tenemos el id, quitamos el nombre
	if ( param.turno.idturno.text().length()==0 ) {
		delete param.turno;
	}
	
	param = comun.deleteSelectedTag(param.toXMLString());
	
	// volvemos a transformar a XML porque comun.deleteSelectedTag devuelve String
	// @TODO esto deber?a normalizarse (siempre XML o siempre String)
	param = new XML(param);
	
	//log.info("Despues del borrado de elementos sin hijos: "+param);
	
	return param;
	
}

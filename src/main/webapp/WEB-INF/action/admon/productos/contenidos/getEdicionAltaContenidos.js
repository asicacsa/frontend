importPackage(Packages.org.springframework.web.servlet);
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');


function handle(request, response) {
	var modelstring= "Contenido";
	
	var xml = request.getParameter('xml');
	
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var resultado;
		
	if (xml!=null) {
		
		var envio = <servicio>
						<parametro>
							<int>
								{xml}
							</int>
						</parametro>
					</servicio>
		
		if (log.isInfoEnabled()) {		
			log.info("Este es el xml que enviamos para el get: "+envio.toXMLString());
		}
		var aux;
		
		try {
			aux = services.process(methodpost+'SOAWrapper',envio);					
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
		
		if (log.isInfoEnabled()) {
			log.info('Este es el valor que retorna : '+new XML(aux).toXMLString())
		}
		if (aux != null) {
			resultado = new XML(aux);
			
			//Necesitamos los datos iddistribuidora e idmarca porque necesitamos saber si tiene valor o no para los checks
			if (resultado.distribuidora.children().length()==0) {
				resultado.distribuidora.iddistribuidora = "";
			}
			if(resultado.marca.children().length()==0){
				resultado.marca.idmarca="";
			}
		}
	} else {
		var envio = <servicio>
						<parametro>
							<{modelstring}/>							
						</parametro>
					</servicio>
		
		if (log.isInfoEnabled()) {
			log.info("Este es el xml que enviamos para el get modelo: "+envio.toXMLString());		
		}

		try{
			resultado = services.process('getXMLModelSOAWrapper',envio);
		} catch (ex) {

			if (log.isErrorEnabled()) {	
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				log.error('LOG(ERROR) : con esta llamada : ' + envio);
				log.error('LOG(ERROR) : result in this Exception : '+ex);
			}
			
			throw ex.javaException;
		}

		if (log.isInfoEnabled()) {
			log.info('Devuelto el modelo  : '+new XML(resultado).toXMLString())
		}
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado);
	
}


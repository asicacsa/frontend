var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {

	//Este servicio seria el default...
	
	//print("CONTEXT is a special variable >>> "+context);	
	
	var methodpost = request.getParameter('servicio');
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	// si xml no se envia, llama al metodo sin parametros	
	var xml = request.getParameter('xml');
	var envio;	
	if(xml == null ) {
		envio = '<servicio />';
	} else {
		envio = '<servicio />';
	}
	
	var result;
	
	try{
		result = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}	
	
	if (result !=  null) {
		result= new XML(result);
		result = this.postProcessXML(result);
	} else {
		result = <list/>
		log.warn("No obtenemos Datos de Tiposproductos ... ");
	}

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
	
}

function postProcessXML(param) {
	deleteTrash(param..clasifprodProds);
	deleteTrash(param..atributos);
	deleteTrash(param..contenidos);
	deleteTrash(param..tipoprodProductos);
	return param;
}

function deleteTrash(elem) {
	delete elem.initialized;
	delete elem.owner;
	delete elem.cachedSize;
	delete elem.role;
	delete elem.key;
	delete elem.dirty;
}
	

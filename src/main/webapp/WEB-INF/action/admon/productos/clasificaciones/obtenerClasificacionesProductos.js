var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {	
	var methodpost = request.getParameter('servicio');
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}

	envio = '<servicio />';
	var result;
	
	try {
		result = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}

	//log.info(result);
	
	result= new XML(result);
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	
	response.writer.println(result.toXMLString());
}
var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaClasificacionProductos';
var servicio_edicion ='actualizarClasificacionesProductos';

function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	
	var resultado;

	var methodpost;
		
	var envio;
	var modoEdicion = request.getParameter('_modo');
	
	if (xml!=null) {
		xml = new XML(xml)..Clasificacionproductos;
		
		
		var sbf = context.beans.getBean('scriptBeanFactory');
	   	var comun = sbf.getBean('transformerXML.js');
	   	
		delete xml..selected; //Eliminamos todos los tags selected
		xml = comun.borraElementosSinHijos(xml); //Eliminamos todos los elementos que se han quedado sin hijos
		
		if (modoEdicion == "alta") {
			methodpost = servicio_alta;
			envio = 
				<servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>
			
		} else {
			methodpost = servicio_edicion;
			envio = 
				<servicio>
					<parametro>
						<list>
							{xml}
						</list>
					</parametro>
				</servicio>				
		}
		
		//Y llamamos al servicio...
		var aux;
		
		try{
			aux = services.process(methodpost+'SOAWrapper',envio);		
		} catch (ex) {

			if (log.isErrorEnabled()) {	
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				log.error('LOG(ERROR) : con esta llamada : ' + envio);
				log.error('LOG(ERROR) : result in this Exception : '+ex);
			}
			
			throw ex.javaException;
		}
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		
		resultado = 
				<ok>
					{aux}
				</ok>
		//En aux tenemos la respuesta
		
		//log.info('Este es el valor que retorna : '+resultado.toXMLString())
	}  else {
		//log.info("No hay dato en el post : ");
		resultado = <error/>
	}	
	
	response.writer.println(resultado.toXMLString());
}
var services = context.beans.getBean('httpServiceSOA');
//var servicio_alta ='darDeAltaTarifa';
var servicio_edicion ='actualizarBonos';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost;
	var envio;
	
	if (xml!=null) {
		
		//log.info("Este es el xml que llega a actualizarBonos: " + xml);
		xml = new XML(xml);
		xml = xml.Bono;
		
		if(methodpost == null) {
			// Solo servicio de edicion, intento de alta provoca error
			if (xml.idbono.text().length() == 0) {
				//log.info("El idbono que llega a actualizarBonos es nulo!!!");
				resultado = <error/>;
			// EDICION	
			} else {
				methodpost = servicio_edicion;
				xml= this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							<list>
								{xml}
							</list>
						</parametro>
					</servicio>	
					
				//log.info("Este es el envio : "+envio);
				//log.info("Este es el metodo post : "+methodpost);
		
				//Y llamamos al servicio...
				
				var aux;
				
				try {
					aux = services.process(methodpost+'SOAWrapper',envio);
				} catch (ex) {
					log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
					log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
					log.error('LOG(ERROR) : result in this Exception : '+ex);
					throw ex.javaException;
				}
		
				//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
				//En aux tenemos la respuesta.
				resultado = <ok>{aux}</ok>;		
			}
		}		
					
	} else {
		//log.info("El xml que llega a actualizarBonos es nulo!!!");
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	//log.info("Lo que vamos a pasar por preProcessXML : "+param);
	
	param = comun.borraElementosSinHijos(param.toXMLString());
	
	param = comun.deleteSelectedTag(param.toXMLString());
	
	param = new XML(param);
	
	// fuera nombrecompleto
	delete param..nombrecompleto;
	
	//elimino idcanjebono de la entradatornos, sólo lo necesitamos para eliminar sesión
	if (param.entradatornoses.Entradatornos.toString() != '')
		if (param.entradatornoses.Entradatornos.idcanjebono.toString() != '')
			delete param.entradatornoses.Entradatornos.idcanjebono;
		if (param.entradatornoses.Entradatornos.sesion.fechayhora.toString() != '')
			delete param.entradatornoses.Entradatornos.sesion.fechayhora;
	
	// procesamos los true y false
	if (param.anulado.text() == 'true'){
		param.anulado = <anulado>1</anulado>;
	} else {
		param.anulado = <anulado>0</anulado>;
	} 
	
	return param;
	
}
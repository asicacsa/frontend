var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {
	
	if (log.isInfoEnabled()){
		log.info("Entrada a buscarClientesPorNombre.action.");
	}
	
	var methodpost = "buscarCliente"
	var xml = request.getParameter('string');
	
	// el servicio esta paginado por defecto, por lo que solo devolveria N registros.
	// Ya que no se especifica desde interfaz el numero de primer registro, asumimos que no se desea paginar
	// y se pasa un -1. Esto tiene sentido ya que este action suele usarse desde componentes
	// editSuggest, que no admiten paginacion.
	
	var envio = <servicio>
					<parametro>
						<consulta>
							<nombrecompleto>{xml}</nombrecompleto>
							<numeroprimerregistro>-1</numeroprimerregistro>
						</consulta>
					</parametro>
				</servicio >;
	
	//No tratamos el xml de entrada...
	if (log.isInfoEnabled()){
		log.info("buscarClientesPorNombre, envio: " + envio.toXMLString());
	}
	
	var respuesta;
	
	try {
		respuesta = services.process(methodpost+'SOAWrapper',envio.toXMLString());
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	if (log.isInfoEnabled()){
		log.info("buscarClientesPorNombre, respuesta: " + respuesta);
	}
	
	var procesar ;
	
	if (respuesta!=null) {

		respuestaxml = new XML(respuesta);
		procesar = new XML();		

		respuestaxml.setName("list");
		
		for each (var cli in respuestaxml.Cliente) {
			cli.nombrecompleto = cli.nombre.text() + ' ' + cli.apellido1.text() + ' ' + cli.apellido2.text();			
		}
		
		procesar = respuestaxml;
	}
	
	if (log.isInfoEnabled()){
		log.info("buscarClientesPorNombre, resultado final: " + procesar.toXMLString());
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(procesar.toXMLString());
}
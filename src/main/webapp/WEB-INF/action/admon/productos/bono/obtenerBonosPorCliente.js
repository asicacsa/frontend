//var services = context.beans.getBean('httpServiceSOA');
var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {

	//Este servicio seria el default....

	//print("CONTEXT is a special variable >>> "+context);	
	
	var methodpost = request.getParameter('servicio');
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	var envio = '<servicio />';
	
	//No tratamos el xml de entrada...
	var respuesta;

	try {
		respuesta = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	//log.info("Este es el xml despues de la transformacion : "+new XML(respuesta).toXMLString())

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	
	response.writer.println(respuesta);
}
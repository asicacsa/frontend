var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = request.getParameter('servicio');
	
	//log.info("Entrada a buscarBonos.action");
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	log.info("buscarBonos.action, params: " + xml);
	
	if(xml == null ) {
		var envio = '<servicio><parametro><Buscarbonoparam/></parametro></servicio>'
	} else {
		xml = new XML(xml);
		// log.info("LOG(INFO) : params before the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		xml = this.preProcessXML(xml);
		//log.info("LOG(INFO) : params after the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>	
	}
	var respuesta;
	
	try {
		respuesta = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	if (respuesta!=null) {
		respuesta = new XML(respuesta);
		respuesta = this.postProcessXML(respuesta);
	} else {
		respuesta = <error/>;
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
	
}



function preProcessXML(param) {

	param = comun.borraElementosSinHijos(param.toXMLString());
	//log.info("LOG(INFO) : params after the borraElementosSinHijos : "+this.getClass().getName()+" "+param.toXMLString());	
	
	param = comun.deleteSelectedTag(param.toXMLString());
	param = new XML(param);
	//log.info("LOG(INFO) : params after the deleteSelectedTag : "+this.getClass().getName()+" "+param.toXMLString());

	//log.info("Tratamos canjeado.");
	// cambiamos los true por 1 y los false por 0 (checkboxes)
	if (param.canjeado.text()=='true'){
		param.canjeado = <canjeado>1</canjeado>;
	} else if (param.canjeado.text()=='false'){
		param.canjeado = <canjeado>0</canjeado>;
	}
	
	//log.info("Tratamos anulado.");
	// cambiamos los true por 1 y los false por 0 (checkboxes)
	if (param.anulado.text()=='true'){
		param.anulado = <anulado>1</anulado>;
	} else if (param.anulado.text()=='false'){
		param.anulado = <anulado>0</anulado>;
	}

	
	return param;
	
}



function postProcessXML(bonos) {
	for each (var bono in bonos.Bono){
		this.evaluateWichCanjebonoIsShowable(bono);
	}
	
	return bonos;
}



function evaluateWichCanjebonoIsShowable(bono){

	// calculamos cual es el canjebono de menor orden, que será
	// el que se represente en pantalla. Lo marcaremos como 'showable'.

	var ordenescanjebonos = new Array();
	
	for each (var canjebono in bono.canjebonos.Canjebono){
		if (canjebono.anulado.text() == "0"){
			ordenescanjebonos.push(canjebono.@orden);
		}
	}
	
	ordenescanjebonos.sort();
	var minorden = ordenescanjebonos[0];
	
	var encontrado = false;
	for each (var canjebono in bono.canjebonos.Canjebono){
		if (canjebono.@orden == minorden && !encontrado && canjebono.anulado.text() == "0"){
			canjebono.@showable = 1;
			encontrado = true;
		}
	}
	
}
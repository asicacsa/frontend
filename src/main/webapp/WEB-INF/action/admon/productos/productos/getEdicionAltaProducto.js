var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');			
			
			
function handle(request, response) {

	//log.info("Entrada a getEdicionAltaProductos.");
		
	var xml = request.getParameter('xml');
	var methodpost = request.getParameter('servicio');
	var envio;
	var producto;

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	//print("Este es el xml que llega a getEdicionAltaProductos: " + xml);	
	
	xml = new XML(xml);	
	
	if (xml != null && xml != '') {
		
		envio = <servicio>
					<parametro>
						<int>
							{xml}
						</int>
					</parametro>
				</servicio>;	
		
	} else {
		envio  = <servicio>
					<parametro>
						<Producto/>
					</parametro>
				</servicio>;
		methodpost = 'getXMLModel';
	}
	
	//log.info("Este es el metodo al que vamos a llamar : "+methodpost);
	
	var aux;
	
	try {
		aux = services.process(methodpost+'SOAWrapper',envio);			
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}		
	
	aux = aux.trim();
	//log.info('Esto es lo que devuelve el servicio : ' + aux);
	
	if (aux != null) {
		// hemos recibido producto
		producto = new XML(aux);				
	}	
	
	producto = this.postProcessXML(producto);
	
	//log.info("Esto es lo que devuelve getEdicionAltaProductos.action tras prostprocesado: " + producto.toXMLString());
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(producto.toXMLString());
}


function postProcessXML(xmlparam) {
	//log.info("Esta es la entrada a postProcessXML: " + xmlparam.toXMLString());
	
	//xmlparam = comun.deleteTrash(xmlparam.toXMLString());
	//xmlparam = comun.totalMatch(xmlparam,'@idref');
	
	// separaremos los tarifaproducto discerniendo si son de bono o no.
	xmlparam.tarifaproductosbono = <tarifaproductosbono>
										{xmlparam.tarifaproductos.Tarifaproducto.(bono == '1')}
									</tarifaproductosbono>;
	xmlparam.tarifaproductos = <tarifaproductos>
									{xmlparam.tarifaproductos.Tarifaproducto.(bono == '0')}
							   </tarifaproductos>;

   if(xmlparam.imprimirEntradasXDefecto == null || xmlparam.imprimirEntradasXDefecto == ''){
		xmlparam.imprimirEntradasXDefecto = <imprimirEntradasXDefecto>true</imprimirEntradasXDefecto>;
   }
   //log.debug("GMV Alta edicion de productos: " + xmlparam.imprimirEntradasXDefecto);
   return xmlparam;	
}
//var service = context.beans.getBean('httpServiceSOA');
var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {

	//print("CONTEXT is a special variable >>> "+context);	
	
	var methodpost = request.getParameter('servicio');
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		var envio = '<servicio />';
	}
	
	//No tratamos el xml de entrada...
	
	//print("The input XML : "+xml);
	
	var respuesta;
	
	try {
		respuesta = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}		
	
	//var respuesta = new XML(service.process(methodpost,envio));
	
	if (respuesta!=null) {
		respuesta = new XML(respuesta);
		//log.info("Resultado de llamar a ObtenerProductos : "+respuesta.toXMLString());

	}
	
	//POST Tratamiento de la respuesta, transformaciones de tipo etc...
	
	/** No es necesario realizar el postProcess por que la pantalla no
		utiliza estos datos.
	for each(v in respuesta..disponibleparaabono) {
		if (v == 0) {
			v=false;
		} else {
			v=true;
		}
	}
	
	for each(v in respuesta..disponibleparabono) {
		if (v == 0) {
			v=false;
		} else {
			v=true;
		}
	}
	
	for each(v in respuesta..principal) {
		if (v == 0) {
			v=false;
		} else {
			v=true;
		}
	}**/
	
	
	//print("The Xml of response: " + respuesta);
	
	//Tratando los tipos Productos...
	//for each (i in respuesta.producto) {
		//for each(u in i.tarifasProducto.tarifaProducto) {
			//u.@selected=false;
		//}
		for each(v in respuesta..tipoProducto) {
			v.@selected=false;
		}
	//}
	
	//print("The xml of response after transform : "+ respuesta);

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	
	response.writer.println(respuesta.toXMLString());
	
}
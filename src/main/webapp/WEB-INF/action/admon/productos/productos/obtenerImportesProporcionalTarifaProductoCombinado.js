var services = context.beans.getBean('httpServiceSOA');

var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
	
	//log.info('Hemos entrado en obtenerImportesProporcionalTarifaProductoCombinado');	


	var idproducto = request.getParameter('idproducto');		
	var idbono = request.getParameter('idbono');		
	
	var resultado;
	var methodpost = 'obtenerImportesProporcionalTarifaProductoCombinado';
	var envio;
	
	if(idproducto != null && idbono != null){
				envio = 
					<servicio>
						<parametro>
							<int>{idproducto}</int>
							<int>{idbono}</int>
						</parametro>
					</servicio>
			}
	
	
	//log.info("Este es el envio : "+envio);	
	
	var aux;
	
	try {
		aux = services.process(methodpost+'SOAWrapper',envio).trim();
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}		
	
	if(aux != null){
		aux = new XML(aux);
		resultado = aux;
				
		//log.info("Este es el resultado de la llamada a obtenerImportesProporcionalTarifaProductoCombinado: "+resultado);
	} else {
		resultado = new XML('<ok/>');
		//log.info("El aux es null");
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
	
}
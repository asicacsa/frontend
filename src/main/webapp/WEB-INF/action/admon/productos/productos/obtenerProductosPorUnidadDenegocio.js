//var service = context.beans.getBean('httpServiceSOA');

// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = request.getParameter('servicio');
	
	if (log.isInfoEnabled()){
		log.info("Entrando en obtenerProductosPorUnidadDenegocio.js llamaremos al servicio "+methodpost);	
	}
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	
	
	if(xml == null ) {
		
		var envio = '<servicio><parametro><obtenerproductosporpnidadpepegocio/></parametro></servicio>';
	} else {
		xml = new XML(xml);
		
		xml = this.preProcessXML(xml);
		
		
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>	
		if (log.isInfoEnabled()){
			log.info("Este es el envio  a obtenerProductosPorUnidadDenegocio: "+envio);	
		}
	}
	
	var respuesta;
	
	try {
		respuesta = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}	
	
	//var respuesta = new XML(service.process(methodpost,envio));
	
	/*Modificado por SADIM para limpiar el XML y poder visualizarlo*/	
	respuesta=respuesta.trim();
	
	if (respuesta!=null) {
		respuesta = new XML(respuesta);
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
	
}


function preProcessXML(param) {
	
	if(param.caducados.text().length() == 0){
		param.caducados = <caducados>0</caducados>
	}
	param = comun.borraElementosSinHijos(param.toXMLString());

	param = comun.deleteSelectedTag(param.toXMLString());
	param = new XML(param);
		
	return param;
	
}
var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaProducto';
var servicio_edicion ='actualizarProductos';

function handle(request, response) {
		
	var xml = request.getParameter('xml');		

	var resultado ;

	var methodpost;
		
	var envio;
	
	var modoEdicion = request.getParameter('_modo');
		
	//print("Este es el xml que llega : "+xml.toXMLString())
	
	if (xml!=null) {
		xml = new XML(xml)..Producto;
		
		//log.info('postEdicionAltaProductos, XML antes de preprocesado: ' + xml.toXMLString());
		xml = this.preProcessXML(xml);
		
		//Eliminamos lo que nos sobra
		// obtengo el js externo
		var sbf = context.beans.getBean('scriptBeanFactory');
	   	var comun = sbf.getBean('transformerXML.js');
	   	
		delete xml..selected; //Eliminamos todos los tags selected
		xml = comun.borraElementosSinHijos(xml); //Eliminamos todos los elementos que se han quedado sin hijos
		
		//Si tenemos tiposProdProducto debemos anyadir el nodo idtipoprodproducto porque en caso contrario xstream da error.
		for (var i = 0; i < xml.tipoprodproductos.tipoprodproducto.length(); i ++) {
			xml.tipoprodproductos.tipoprodproducto[i].idtipoprodproducto = -1;
			//Aprovechamos para anyadir el id del producto
			xml.tipoprodproductos.tipoprodproducto[i].producto.idproducto = xml.idproducto;
		}
		
		if (modoEdicion == "alta") {
			methodpost = servicio_alta;
			envio = 
				<servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>
			
		} else {
			methodpost = servicio_edicion;
			envio = 
				<servicio>
					<parametro>
						<list>
							{xml}
						</list>
					</parametro>
				</servicio>				
		}
		
		if (log.isInfoEnabled()) {	
			log.info("El xml que enviamos a actualizarProductos: "+envio);
		}


		var aux;
	
		try {
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}	
		
		aux = aux.trim();
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		
		resultado = 
				<ok>
					{aux}
				</ok>
		//En aux tenemos la respuesta
		
	} else {
		//log.info("No hay dato en el post : ");
		resultado = <error/>
	}
	
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(xml) {	
	// principal
	
	//log.info('Lo que llega al preProcessXML: '+xml);
	
	if (xml.principal.text() == true || xml.principal.text() == 1){
		xml.principal=<principal>1</principal>
	}else if (xml.principal.text() == false) {
		xml.principal=<principal>0</principal>
	}else{
		//por defecto es 0
		xml.principal=<principal>0</principal>
	}
	
	// disponible para abono

	/*if (xml.disponibleparaabono.text() == true || xml.disponibleparaabono.text() == 1) {
		xml.disponibleparaabono=<disponibleparaabono>1</disponibleparaabono>
	}else if (xml.disponibleparaabono.text() == false) {
		xml.disponibleparaabono=<disponibleparaabono>0</disponibleparaabono>
	}else{
		//por defecto es 0
		xml.disponibleparaabono=<disponibleparaabono>0</disponibleparaabono>
	}*/
	
	
	// disponible para bono
	
	if (xml.disponibleparabono.text() == true || xml.disponibleparabono.text() == 1) {
		xml.disponibleparabono=<disponibleparabono>1</disponibleparabono>
	}else if (xml.disponibleparabono.text() == false) {
		xml.disponibleparabono=<disponibleparabono>0</disponibleparabono>
	}else{
		//por defecto es 0
		xml.disponibleparabono=<disponibleparabono>0</disponibleparabono>
	}
	
	// imprimirentradasXDefecto
	//GMV RG03 4661 añadimos un campo "imprimirEntradasXDefecto" que nos indicará si se imprimen sus entradas o no por defecto -->
	if (xml.imprimirEntradasXDefecto.text() == true || xml.imprimirEntradasXDefecto.text() == 1) {
		xml.imprimirEntradasXDefecto=<imprimirEntradasXDefecto>1</imprimirEntradasXDefecto>
	}else if (xml.imprimirEntradasXDefecto.text() == false) {
		xml.imprimirEntradasXDefecto=<imprimirEntradasXDefecto>0</imprimirEntradasXDefecto>
	}else{
		//por defecto es 1
		xml.imprimirEntradasXDefecto=<imprimirEntradasXDefecto>1</imprimirEntradasXDefecto>
	}

	// fusionamos las listas de tarifaproductos (bonos y vigentes)
	var tarifaproducto;
	for each(tarifaproducto in xml.tarifaproductos.Tarifaproducto){
		tarifaproducto.bono=<bono>0</bono>;
	}
	for each(tarifaproducto in xml.tarifaproductosbono.Tarifaproducto){
		tarifaproducto.bono=<bono>1</bono>;
		xml.tarifaproductos.* += tarifaproducto;
	}
	delete xml.tarifaproductosbono;
	log.info('XML tras la fusi??n de tarifaproductos y tarifaproductosbono: ' + xml.toXMLString());
	
	return xml;	
}	

/**Funciones de TEST ++++++++++**/

function pruebaProductoConTipo() {
	//Datos teoricamente minimos necesarios para realizar un alta segun la B.D.
	var prueba_producto = 
					<producto>
						<bonos></bonos>
						<clasifprodProds></clasifprodProds>
						<combinado>0</combinado>
						<dadodebaja>0</dadodebaja>
						<descripcion>Descripcion de script 2</descripcion>
						<descuentoProductos></descuentoProductos>
						<disponibleparaabono>0</disponibleparaabono>
						<disponibleparabono>0</disponibleparabono>
						<nombre>Prueba Script 2</nombre>
						<principal>0</principal>
						<tarifaproductos></tarifaproductos>
						<tipoprodProductos>
							<tipoprodProducto>
								<id>
									<idtipodeproducto>106</idtipodeproducto>
								</id>  																
							</tipoprodProducto>
						</tipoprodProductos>						
					</producto>
	return prueba_producto;
}
/**
						<fechafinventa></fechafinventa>
						<fechafinvigencia></fechafinvigencia>
						<fechainicioventa></fechainicioventa>
						<fechainiciovigencia></fechainiciovigencia>
**/
function pruebaProducto() {
	//Datos minimos necesarios para realizar un alta segun la B.D.
	var prueba_producto = 
					<producto>
						<bonos></bonos>
						<clasifprodProds></clasifprodProds>
						<combinado>0</combinado>
						<dadodebaja>0</dadodebaja>
						<descripcion>Descripcion de script 1</descripcion>
						<descuentoProductos></descuentoProductos>
						<disponibleparaabono>0</disponibleparaabono>
						<disponibleparabono>0</disponibleparabono>
						<fechafinventa></fechafinventa>
						<fechafinvigencia></fechafinvigencia>
						<fechainicioventa></fechainicioventa>
						<fechainiciovigencia></fechainiciovigencia>
						<nombre>Prueba Script 1</nombre>
						<orden></orden>
						<principal>0</principal>
						<tarifaproductos></tarifaproductos>
						<tipoprodProductos></tipoprodProductos>						
					</producto>
	return prueba_producto;
}
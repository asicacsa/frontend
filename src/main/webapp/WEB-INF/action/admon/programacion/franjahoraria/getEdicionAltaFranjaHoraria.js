var services = context.beans.getBean('httpServiceSOA');				
function handle(request, response) {
		
	var xml = request.getParameter('xml');
	
	//log.info("LOG(INFO) : Call to getEdicionAltaFranjaHoraria."+this.getClass().getName());
		
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var resultado;
	var envio;
	if (xml!=null) {
		envio  = <servicio>
					<parametro>
						<int>
							{xml}
						</int>
					</parametro>
				</servicio>
	} else {
		envio  = <servicio>
					<parametro>
						<Franjahoraria/>
					</parametro>
				</servicio>;
		methodpost = 'getXMLModel';
	}
				
	var aux;
	
	try {
		aux = services.process(methodpost+'SOAWrapper',envio);				
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	if (aux!=null) {
		resultado = new XML(aux);
		//resultado = this.postProcessXML(resultado);
	} else {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result does not Franjahoraria : '+ex);
		var ex = new java.lang.Exception('Exception calling this service : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		throw ex;	
	}
	
	
	//log.info("LOG(INFO) : final response : "+resultado+' : '+this.getClass().getName());
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
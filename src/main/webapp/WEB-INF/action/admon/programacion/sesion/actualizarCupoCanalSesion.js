var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');
function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var idSesion = request.getParameter('idSesion');		
	var resultado;
	var methodpost = 'actualizarCupoCanalSesion';
	var envio;

	
	if (xml!=null) {
		xml = new XML(xml);		
		envio = <servicio>
					<parametro>
						{xml}
						<int>{idSesion}</int>
					</parametro>
				</servicio>
		

		var aux
			
		try{
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {

			if (log.isErrorEnabled()) {	
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				log.error('LOG(ERROR) : con esta llamada : ' + envio);
				log.error('LOG(ERROR) : result in this Exception : '+ex);
			}
			
			throw ex.javaException;
		}
		
		if(aux != null){

			aux = new XML(aux);
			resultado = aux;
		} 
			
	} else {
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
//var service = context.beans.getBean('httpServiceSOA');

// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = request.getParameter('servicio');
	var resultado;
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		var envio = '<servicio><parametro><buscarsesionesparam/></parametro></servicio>'
	} else {
		xml = new XML(xml);		
		var xml_anyo = xml.anyo.text();
		var xml_mes = xml.mes.text();
		
		xml = this.preProcessXML(xml);
		
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>	
	}
	
	var aux;
	
	try{
		aux = services.process(methodpost+'SOAWrapper',envio);
		
	} catch (ex) {

		if (log.isErrorEnabled()) {	
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : con esta llamada : ' + envio);
			log.error('LOG(ERROR) : result in this Exception : '+ex);
		}
		
		throw ex.javaException;
	}
				
	if(aux != null){
		aux = new XML(aux);
		resultado = aux;
		resultado = this.postProcessXML(aux);
	}	
	


	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
	
}


function preProcessXML(param) {

	
	/* Elimino el mes y el a?o por que no los podemos enviar en el dto*/
	
	delete param.anyo;
	delete param.mes;
	
	param = comun.borraElementosSinHijos(param.toXMLString());
	param = comun.deleteSelectedTag(param.toXMLString());
	param = new XML(param);
	
	return param;
	
}
function postProcessXML(param){
	//log.info('en la funcion postProcessXML y lo q llega '+param);
	/*<Sesion>
	<idsesion>1021</idsesion>
	<horainicio>19:30</horainicio>
	<fecha>07/01/2007-00:00:00</fecha>
	<bloqueado/>
	<contenido>
		<idcontenido>1008</idcontenido>
		<nombre>LA BELLE ET LA B?TE</nombre>
		<tipoproducto>
		<recinto>
			<zonas>
			<Zona>
				<idzona>1001</idzona>
				<nombre>Zona 1</nombre>
			</Zona>
			<Zona>
				<idzona>1005</idzona>
				<nombre>Zona 5</nombre>
			</Zona>
			<Zona>
				<idzona>1003</idzona>
				<nombre>Zona 3</nombre>
			</Zona>
			<Zona>
				<idzona>1002</idzona>
				<nombre>Zona 2</nombre>
			</Zona>
			<Zona>
				<idzona>1006</idzona>
				<nombre>Zona 6</nombre>
			</Zona>
			<Zona>
				<idzona>1004</idzona>
				<nombre>Zona 4</nombre>
			</Zona>
			</zonas>
		</recinto>
		</tipoproducto>
	</contenido>
	</Sesion>*/
	
	for each (i in param.Sesion.contenido.tipoproducto.recinto.zonas.Zona){
		//log.info('en el for each');
		//log.info('Esto es i '+i);
		i.setName('zona');
		//log.info('Despues del Change of the Name '+i);
	
	}
	
	return param;
}
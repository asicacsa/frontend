var services = context.beans.getBean('httpServiceSOA');
var servicio_modificarSesion = 'actualizarSerieSesiones';
function handle(request, response) {
	var actualizar = <Actualizarserieparam/>;
	
	var idsesion =request.getParameter('idsesion');
	if (idsesion == null) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : result does not have idSesion : '+ex);
		var ex = new java.lang.Exception('Exception calling this javascriptE4X : '+this.getClass().getName());
		throw ex;		
	} else {
		idsesion=new XML(idsesion);
		actualizar.listaIdSesiones=<listaIdSesiones>
										<int>{idsesion}</int>
									</listaIdSesiones>;
		var anyo = request.getParameter('anyo');
		var mes = request.getParameter('mes');
		var dia = request.getParameter('dia');
		if (dia != null && mes != null && anyo != null) {
			if (dia.length() < 2) {
				dia = '0'+dia;
			}
			if (mes.length() < 2) {
				mes = '0'+mes;
			}
			var fecha = dia+'/'+mes+'/'+anyo;
			fecha = new XML(fecha);
			actualizar.date = <fecha>{fecha}</fecha>;					
		}
		//log.info("XML que nos llega : "+actualizar);
		var hora = request.getParameter('hora');
		var minuto = request.getParameter('minuto');
		if (hora != null && minuto != null) {
			if (hora.length() < 2) {
				hora = '0'+hora;
			}
			if (minuto.length() < 2) {
				minuto = '0'+minuto;
			}
			var horainicio = hora+':'+minuto;
			actualizar.horainicio = <horainicio>{horainicio}</horainicio>
		}				
		//log.info("XML final : "+actualizar);
		methodpost=servicio_modificarSesion;
		var envio =<servicio>
					<parametro>
					{actualizar}
					</parametro>
					</servicio>;
		
		try {
			services.process(methodpost+'SOAWrapper',envio);				
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
	
	}
	var resultado = <ok/>		
	
	//log.info("LOG(INFO) : final response : "+resultado+' : '+this.getClass().getName());
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
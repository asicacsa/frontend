var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaSesion';
var servicio_edicion ='actualizarSerieSesiones';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

//Para los estados localidad.
var estado_ocupada = 4;
var estado_libre = 1;


function handle(request, response) {
		
	if (log.isInfoEnabled()){
		log.info("Entrada a postEdicionAltaSesion.action.");
	}
	
	var xml = request.getParameter('xml');
	var periodo = request.getParameter('periodo'); // periodo o dias concretos?
	var sesion_constant= 'Altasesionparam';
	var id_constant = 'idsesion';		
	
	var resultado;
	
	var methodpost;
		
	var envio;
	
	if (xml!=null) {
		
		xml = new XML(xml);
		if (log.isInfoEnabled()){
			log.info("Entrada a postEdicionAltaSesion.action. Lo que llega "+ xml.toXMLString());
		}
		if (xml.hasOwnProperty(sesion_constant)) {
			//Preparamos el xml para enviarlo con <list/>, <arrayList/>, ya vorem...
			xml = xml.Altasesionparam;
		} else if (!(xml.name() == franja_constant)) {
			var ex = new java.lang.Exception('Exception in postEdicionAltaSesion : '+this.getClass().getName());
			log.error('LOG(ERROR) : does not exist Altasesionparam in the parameters of the call.',ex);			
			throw ex;
		} 
		
		if(methodpost == null) {
			//Si tiene idSesion estaremos editando una sesion
			if (xml.sesionPlantilla.hasOwnProperty('idsesion')) {
				var xml_edicion = preProcessXMLEdicion(xml);
				methodpost = servicio_edicion;
				
				envio = 
					<servicio>
						<parametro>
								{xml_edicion}
						</parametro>
					</servicio>
					
			//Si no tiene idSesion estaremos dando de alta una sesion
			} else {
				xml = this.preProcessXML(xml);
				eliminarCamposSegunPeriodo(xml, periodo);
				methodpost = servicio_alta;
				
				envio = 
					<servicio>
						<parametro>
							{xml}
						</parametro>
					</servicio>
			}
		}		
		
		if (log.isInfoEnabled()){
			log.info("postEdicionAltaSesion, envio: " + envio);
		}
		
		var aux;
		try {
		 	aux = services.process(methodpost+'SOAWrapper',envio);
		 } catch (ex) {
	 		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : with this : ' + envio);
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		 }
		
		
		if (log.isInfoEnabled()){
			log.info("postEdicionAltaSesion, respuesta del servicio: " + aux);
		}
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		
		resultado = <ok>{aux}</ok>
				
	} else {
		var ex = new java.lang.Exception('Exception in postEdicionAltaSesion : '+this.getClass().getName());
		log.error('LOG(ERROR) : does not exist params in the call.',ex);			
		throw ex;
	}
	
	if (log.isInfoEnabled()){
		log.info("postEdicionAltaSesion, resultado final: " + resultado.toXMLString());
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}



function preProcessXML(xml) {
	//log.info("postEdicionAltaSesion, xml a preprocesar: "+xml.toXMLString());
	
	delete xml.sesionPlantilla.esSesionNumerada;
	delete xml.sesionPlantilla.zonasesions;

	if (xml.sesionPlantilla.nonumerada.text() == false) {
		xml.sesionPlantilla.nonumerada=<nonumerada>0</nonumerada>
	} else if (xml.sesionPlantilla.nonumerada.text() == true) {
		xml.sesionPlantilla.nonumerada=<nonumerada>1</nonumerada>
	} else {
		log.warn("LOG(WARN) : postAltaEdicionSesion. El dato sesionPlantilla.nonumerada llega con valor distinto a true/false.")
	}

	
	//GGL 25042012 ICAA	
	xml.codigo=xml.sesionPlantilla.IncidenciaICAA.codigo.text();
	
	
	if (xml.sesionPlantilla.bloqueado.text() == false) {
		xml.sesionPlantilla.bloqueado=<bloqueado>0</bloqueado>
	} else if (xml.sesionPlantilla.bloqueado.text() == true) {
		xml.sesionPlantilla.bloqueado=<bloqueado>1</bloqueado>
	} else {
		log.warn("LOG(WARN) : postAltaEdicionSesion. El dato sesionPlantilla.bloqueado llega con valor distinto a true/false.")
	}

	//Ahora a juntar la hora y fecha de inicio y fin venta..
	
				
	if (xml.hasOwnProperty('panelfechaFin')) {
	
		xml.panelfechaFin.setName('fechaFin');
		xml.panelfechaInicio.setName('fechaInicio');
						
		}
	
	
	var aux_fecha = xml.sesionPlantilla.fechainicioventa.text();
	var aux_hora = xml.sesionPlantilla.horainicioventa.text();	
	if (aux_fecha != null && aux_fecha.length() > 0) {
		if (aux_hora != null && aux_hora.length() > 0) {
			xml.sesionPlantilla.fechayhorainicioventa=<fechayhorainicioventa>{aux_fecha} {aux_hora}:00</fechayhorainicioventa>;
			
		} else {
			xml.sesionPlantilla.fechayhorainicioventa=<fechayhorainicioventa>{aux_fecha} 00:00:00</fechayhorainicioventa>;
		}
	}
	delete xml.sesionPlantilla.fechainicioventa;
	delete xml.sesionPlantilla.horainicioventa;
	var aux_fecha = xml.sesionPlantilla.fechafinventa.text();
	var aux_hora = xml.sesionPlantilla.horafinventa.text();
	if (aux_fecha != null && aux_fecha.length() > 0) {
		if (aux_hora != null && aux_hora.length() > 0) {
			xml.sesionPlantilla.fechayhorafinventa=<fechayhorafinventa>{aux_fecha} {aux_hora}:00</fechayhorafinventa>;
		} else {
			xml.sesionPlantilla.fechayhorafinventa=<fechayhorafinventa>{aux_fecha} 00:00:00</fechayhorafinventa>;
		}
	}
	delete xml.sesionPlantilla.fechafinventa;
	delete xml.sesionPlantilla.horafinventa;
	
	
	var aux_hora = xml.horasAntes.text();
	
	if (aux_hora != null && aux_hora.length() > 0) {
		aux_hora = aux_hora.split(':');
		xml.horasAntes = <horasAntes>{aux_hora[0]}</horasAntes>;
		xml.minutosAntes = <minutosAntes>{aux_hora[1]}</minutosAntes>;
	}
	
	
	var aux_hora = xml.horasDespues.text();
	if (aux_hora != null && aux_hora.length() > 0) {
		aux_hora = aux_hora.split(':');
		xml.horasDespues = <horasDespues>{aux_hora[0]}</horasDespues>;
		xml.minutosDespues = <minutosDespues>{aux_hora[1]}</minutosDespues>;
	}
		
	
	var aux_fecha_session = xml.sesionPlantilla.fecha.text();
	
	//QUITAR ESTE IF CND EL SERVICIO FUNCIONE BIEN		
	/*if (xml.sesionPlantilla.hasOwnProperty('idsesion')) {
		//Estamos en el servicio de actualizacion de sesiones.
		xml= xml.sesionPlantilla;
		xml.setName('Sesion');
	}*/
	
	for each (i in xml.sesionPlantilla.estadolocalidads.Estadolocalidad) { 
		//log.info('Estado localidad '+i);
		i.estado.idestado=<idestado>{estado_ocupada}</idestado>
		i.fecha=<fecha>{aux_fecha_session}</fecha>
		delete i.textoDescriptivo;
		delete i.usuario.textoDescriptivo;
		delete i.zonas;
	}
	//Ahora miraremos la coleccion de libres de sesionplantilla
	//Si fuera una edicion llevaria el dato de idestadolocalidad sino no lo llevara
	//sino lo lleva eliminaremos el elemento de la coleccion.
	//log.info("Estamos en el preProcess de Edicion Alta Sesion : ");
	
	/**for each (i in xml.libres.Estadolocalidad) {
		delete i.textoDescriptivo;
		delete i.usuario.textoDescriptivo;
		delete i.zonas;
		if (i.idestadolocalidad != null && i.idestadolocalidad.text().length() > 0) {			
			i.estado.idestado=<idestado>{estado_libre}</idestado>
			i.fecha=<fecha>{aux_fecha_session}</fecha>
			xml.estadolocalidads.appendChild(i);			
		} 
		//log.info("En el for : "+i);		
	}**/
			
	// Ahora destruimos la coleccion
	delete xml.libres;
	
	delete xml.nombre;
	
	xml = comun.borraElementosSinHijos(xml.toXMLString());
			
	return xml;
}



/**
* Función que crea el xml necesario para la edición de una sesión
**/
function preProcessXMLEdicion(xml) {
	//log.info("postEdicionAltaSesion, xml a preprocesar: "+xml.toXMLString());
	
	var xml_edicion = <Actualizarserieparam />;
	//xml_edicion.setName('Actualizarserieparam');
	xml_edicion.idplantillafranjas = xml.sesionPlantilla.plantillafranjas.idplantillafranjas;
	xml_edicion.fecha = xml.sesionPlantilla.fecha;
	xml_edicion.hora_inicio = xml.sesionPlantilla.horainicio;
	xml_edicion.hora_fin = xml.sesionPlantilla.horafin;
	xml_edicion.idContenido = <idContenido>{xml.sesionPlantilla.contenido.idcontenido.text()}</idContenido>;
	//GGL 25042012 ICAA
	xml_edicion.codigo = <codigo>{xml.sesionPlantilla.IncidenciaICAA.codigo.text()}</codigo>;
	xml_edicion.aperturatornos = xml.sesionPlantilla.aperturatornos;
	xml_edicion.cierretornos = xml.sesionPlantilla.cierretornos;
	xml_edicion.overbookingaforo = xml.sesionPlantilla.overbookingaforo;
	xml_edicion.overbookingventa = xml.sesionPlantilla.overbookingventa;	
	xml_edicion.listaIdSesiones = <listaIdSesiones><int>{xml.sesionPlantilla.idsesion.text()}</int></listaIdSesiones>; 
	
	if (xml.sesionPlantilla.nonumerada.text() == false) {
		xml_edicion.nonumerada=<nonumerada>0</nonumerada>
	} else if (xml.sesionPlantilla.nonumerada.text() == true) {
		xml_edicion.nonumerada=<nonumerada>1</nonumerada>
	} else {
		log.warn("LOG(WARN) : postAltaEdicionSesion. El dato sesionPlantilla.nonumerada llega con valor distinto a true/false.")
	}

	
	if (xml.sesionPlantilla.bloqueado.text() == false) {
		xml_edicion.bloqueado=<bloqueado>0</bloqueado>
	} else if (xml.sesionPlantilla.bloqueado.text() == true) {
		xml_edicion.bloqueado=<bloqueado>1</bloqueado>
	} else {
		log.warn("LOG(WARN) : postAltaEdicionSesion. El dato sesionPlantilla.bloqueado llega con valor distinto a true/false.")
	}

	
	
	var aux_fecha = xml.sesionPlantilla.fechainicioventa.text();
	var aux_hora = xml.sesionPlantilla.horainicioventa.text();	
	if (aux_fecha != null && aux_fecha.length() > 0) {
		if (aux_hora != null && aux_hora.length() > 0) {
			xml_edicion.fechayhorainicioventa=<fechayhorainicioventa>{aux_fecha} {aux_hora}:00</fechayhorainicioventa>;
			
		} else {
			xml_edicion.fechayhorainicioventa=<fechayhorainicioventa>{aux_fecha} 00:00:00</fechayhorainicioventa>;
		}
	}
	
	var aux_fecha = xml.sesionPlantilla.fechafinventa.text();
	var aux_hora = xml.sesionPlantilla.horafinventa.text();
	if (aux_fecha != null && aux_fecha.length() > 0) {
		if (aux_hora != null && aux_hora.length() > 0) {
			xml_edicion.fechayhorafinventa=<fechayhorafinventa>{aux_fecha} {aux_hora}:00</fechayhorafinventa>;
		} else {
			xml_edicion.fechayhorafinventa=<fechayhorafinventa>{aux_fecha} 00:00:00</fechayhorafinventa>;
		}
	}
	
	
	var aux_hora = xml.horasAntes.text();
	
	if (aux_hora != null && aux_hora.length() > 0) {
		aux_hora = aux_hora.split(':');
		xml_edicion.horasAntes = <horasAntes>{aux_hora[0]}</horasAntes>;
		xml_edicion.minutosAntes = <minutosAntes>{aux_hora[1]}</minutosAntes>;
	}
	
	
	var aux_hora = xml.horasDespues.text();
	if (aux_hora != null && aux_hora.length() > 0) {
		aux_hora = aux_hora.split(':');
		xml_edicion.horasDespues = <horasDespues>{aux_hora[0]}</horasDespues>;
		xml_edicion.minutosDespues = <minutosDespues>{aux_hora[1]}</minutosDespues>;
	}
	
	xml_edicion = comun.borraElementosSinHijos(xml_edicion.toXMLString());	
	return xml_edicion;
}



/**
 * Elimina los campos no relacionados con el tipo de periodo, bien
 * periodo, bien dias concretos. Son excluyentes.
 */
function eliminarCamposSegunPeriodo(xml, periodo){
	
	if (log.isInfoEnabled()){
		log.info("postEdicionAltaSesion.eliminarCamposSegunPeriodo, periodo: " + periodo);
		log.info("postEdicionAltaSesion.eliminarCamposSegunPeriodo, entrada: " + xml.toXMLString());
	}
	
	// PERIODO DE TIEMPO
	if (periodo=="true"){
		delete xml.listaFechas;
	// DIAS CONCRETOS
	} else if (periodo=="false"){
		delete xml.fechaInicio;
		delete xml.fechaFin;
		delete xml.diario;
		delete xml.lunes;
		delete xml.martes;
		delete xml.miercoles;
		delete xml.jueves;
		delete xml.viernes;
		delete xml.sabado;
		delete xml.domingo;
	}
	
	if (log.isInfoEnabled()){
		log.info("postEdicionAltaSesion.eliminarCamposSegunPeriodo, salida: " + xml.toXMLString());
	}
}



function comprobarFechas(fecha_inicio, fecha_fin, fecha_inicio_venta, fecha_fin_venta) {
/**
La fecha de inicio de venta tiene que ser  menor o igula a la de sesi??n.
La fecha de fin de venta tiene que ser mayor a la de inicio de venta.
Si la fecha de inicio de venta es igual a la fecha de fin de venta entonces la hora de inicio de venta tiene que ser mayor a la de fin de venta.
La hora de fin de sesi??n tiene que ser mayor que la fecha de inicio de sesi??n.
**/
}
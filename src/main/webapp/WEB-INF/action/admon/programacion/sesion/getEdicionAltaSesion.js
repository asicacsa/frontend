importPackage(Packages.java.util);
importPackage(Packages.java.text);

var services = context.beans.getBean('httpServiceSOA');
var pattern_date_format = "dd/MM/yyyy-HH:mm";
var estado_ocupada = 4;	
var methodpostSinBloqueos = 'esSesionNumerada';	
function handle(request, response) {

	var xml = request.getParameter('xml');
	
	//log.info("LOG(INFO) : Call to getEdicionAltaSesion."+this.getClass().getName());
		
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var resultado;
	var envio;
	if (xml!=null) {
		envio  = <servicio>
					<parametro>
						<int>
							{xml}
						</int>
					</parametro>
				</servicio>
		methodpost = 'obtenerSesionSinBloqueosNumerados';
	} else {
		envio  = <servicio>
					<parametro>
						<Altasesionparam/>
					</parametro>
				</servicio>;
		methodpost = 'getXMLModel';
	}
	
	//log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());
			
	//log.info("LOG(INFO) : with this parameters : "+envio+' : '+this.getClass().getName());					
	var aux;
	
	try {
		aux = services.process(methodpost+'SOAWrapper',envio);				
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	aux= aux.trim(); // 24-10-2017. AVC. Para que funcione correctamente hay que eliminar los blancos de aux.
	if (aux!=null) {
		resultado = new XML(aux);
		log.info("LOG(INFO) : resultado de la operacion : "+resultado);
		if (methodpost == 'obtenerSesionSinBloqueosNumerados') {
			//Edicion...
			resultado = this.postProcessXMLEdicion(resultado);
		} else {
			resultado = this.postProcessXMLAlta(resultado);
		}
		//resultado = this.postProcessXML(resultado);
	} else {
		log.error('VOY A DAR EL ERROR...');
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('YA TÁ');
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result does not Sesion : '+ex);
		var ex = new java.lang.Exception('Exception calling this service : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		throw ex;	
	}
	
	//log.info("LOG(INFO) : final response : "+resultado+' : '+this.getClass().getName());
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function esSesionNumerada(id) {
	//log.info('Dentro de la Function esSesionNumerada..y el id sesion q llega '+id);
	var envio;
		
	if (id!=null) {
		envio  = <servicio>
					<parametro>
						<int>
							{id}
						</int>
					</parametro>
				</servicio>
			}
	
	
	var respuesta;
	
	try {
		respuesta = services.process(methodpostSinBloqueos+'SOAWrapper',envio);
	} catch (ex) {
	 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	if (respuesta != null){
		respuesta = new XML(respuesta);
	}

	return respuesta;
	
}

function postProcessXMLAlta(param) {
	var aux;
	var aux_fech;
	var aux_date = new java.util.Date();
	var date_formater = new java.text.SimpleDateFormat(pattern_date_format);
	
	//log.info('En el postProcessXMLAlta...'+param.toXMLString());
	
	if (param.sesionPlantilla.fecha.text().length() != 0) {
		aux = param.sesionPlantilla.fecha.text();
		aux_fech=aux.substr(0,aux.indexOf('-'));
		param.sesionPlantilla.fecha = <fecha>{aux_fech}</fecha>		
	}
	if (param.sesionPlantilla.fechayhorafinventa.text().length() != 0) {
		aux = param.sesionPlantilla.fechayhorafinventa.text();
		aux_fech=aux.substr(0,aux.indexOf('-'));
		param.sesionPlantilla.fechafinventa = <fechafinventa>{aux_fech}</fechafinventa>
		aux_fech = aux.substr(aux.indexOf('-')+1,5);
		param.sesionPlantilla.horafinventa = <horafinventa>{aux_fech}</horafinventa>
	} else {
		//aux_fech=param.sesionPlantilla.fecha.text();
		//var hora = param.sesionPlantilla.horainicio.text();
		//Se ha seteado en el primer if que formatea las fechas...
		param.sesionPlantilla.fechafinventa = <fechafinventa></fechafinventa>
		param.sesionPlantilla.horafinventa = <horafinventa></horafinventa>
	}
	delete param.sesionPlantilla.fechayhorafinventa;	
	if (param.sesionPlantilla.fechayhorainicioventa.text().length() != 0) {
		aux = param.sesionPlantilla.fechayhorainicioventa.text();
		aux_fech=aux.substr(0,aux.indexOf('-'));
		param.sesionPlantilla.fechainicioventa = <fechainicioventa>{aux_fech}</fechainicioventa>
		aux_fech = aux.substr(aux.indexOf('-')+1,5);
		param.sesionPlantilla.horainicioventa = <horainicioventa>{aux_fech}</horainicioventa>
	} else {				
		var aux_string_date = date_formater.format(aux_date);
		aux_fech=aux_string_date.substr(0,aux_string_date.indexOf('-'));
		param.sesionPlantilla.fechainicioventa = <fechainicioventa>{aux_fech}</fechainicioventa>
		aux_fech = aux_string_date.substr(aux_string_date.indexOf('-')+1,5);
		param.sesionPlantilla.horainicioventa = <horainicioventa>{aux_fech}</horainicioventa>	
	}
	delete param.sesionPlantilla.fechayhorainicioventa;
	
	if (param.sesionPlantilla.bloqueado.text() == 0) {
		param.sesionPlantilla.bloqueado=<bloqueado>false</bloqueado>
	} else if (param.sesionPlantilla.bloqueado.text() == 1) {
		param.sesionPlantilla.bloqueado=<bloqueado>true</bloqueado>
	} else {
		log.warn("LOG(WARN) ??? ?? ?? : postAltaEdicionSesion. El dato bloqueado llega con valor distinto a 0 ? 1.")
	}
	// N? localidades : xxxx en la Zona : xxxx por el motivo : xxxx y el usuario : xxxxx
	for each (var i in param.estadolocalidads.Estadolocalidad) { 
		var aux_texto ='';
		if (i.usuario.nombre.text() != 0) {
			aux_texto = i.usuario.nombre.text();
			if (i.usuario.apellidos.text() != 0) {
				aux_texto = aux_texto+" "+i.usuario.apellidos.text();
			}
		} else if (i.usuario.apellidos.text() != 0) {
			aux_texto = i.usuario.apellidos.text();
		}
		i.usuario.textoDescriptivo=<textoDescriptivo>{aux_texto}</textoDescriptivo>;
		aux_texto = 'N\u00BA localidades : '+i.nrolocalidadesbloqueadas.text();
		if (i.zona.nombre.text() != 0) {
			aux_texto = aux_texto+' Zona : '+i.zona.nombre.text();			
		}		
		aux_texto = aux_texto+' Motivo : '+i.motivobloqueo.text()+' Bloqueado por : '+i.usuario.textoDescriptivo.text();
		i.textoDescriptivo=<textoDescriptivo>{aux_texto}</textoDescriptivo>;		
	}
	return param;
//Modificaremos la fecha y hora de inicio venta. 
}

function postProcessXMLEdicion(param) {
	var aux;
	var aux_fech;
	var aux_date = new java.util.Date();
	var date_formater = new java.text.SimpleDateFormat(pattern_date_format);
	
	var esSesionNumerada = this.esSesionNumerada(param.sesionPlantilla.idsesion.text());
	//log.info('El valor de esSesionNumerada en postProcessXMLEdicion '+esSesionNumerada);
	if(esSesionNumerada == '1'){
		esSesionNumerada = <esSesionNumerada>false</esSesionNumerada>;
	}else{
		esSesionNumerada = <esSesionNumerada>true</esSesionNumerada>;
	}
	param.sesionPlantilla.appendChild(esSesionNumerada);
	
	
	if (param.sesionPlantilla.fecha.text().length() != 0) {
		aux = param.sesionPlantilla.fecha.text();
		aux_fech=aux.substr(0,aux.indexOf('-'));
		param.sesionPlantilla.fecha = <fecha>{aux_fech}</fecha>		
	}
	if (param.sesionPlantilla.fechayhorafinventa.text().length() != 0) {
		aux = param.sesionPlantilla.fechayhorafinventa.text();
		aux_fech=aux.substr(0,aux.indexOf('-'));
		param.sesionPlantilla.fechafinventa = <fechafinventa>{aux_fech}</fechafinventa>
		aux_fech = aux.substr(aux.indexOf('-')+1,5);
		param.sesionPlantilla.horafinventa = <horafinventa>{aux_fech}</horafinventa>
	} else {		
		//Se ha seteado en el primer if que formatea las fechas...
		param.sesionPlantilla.fechafinventa = <fechafinventa></fechafinventa>
		param.sesionPlantilla.horafinventa = <horafinventa></horafinventa>
	}
	delete param.sesionPlantilla.fechayhorafinventa;	
	if (param.sesionPlantilla.fechayhorainicioventa.text().length() != 0) {
		aux = param.sesionPlantilla.fechayhorainicioventa.text();
		aux_fech=aux.substr(0,aux.indexOf('-'));
		param.sesionPlantilla.fechainicioventa = <fechainicioventa>{aux_fech}</fechainicioventa>
		aux_fech = aux.substr(aux.indexOf('-')+1,5);
		param.sesionPlantilla.horainicioventa = <horainicioventa>{aux_fech}</horainicioventa>
	} else {						
		param.sesionPlantilla.fechainicioventa = <fechainicioventa></fechainicioventa>
		param.sesionPlantilla.horainicioventa = <horainicioventa></horainicioventa>	
	}
	delete param.sesionPlantilla.fechayhorainicioventa;
	
	if (param.sesionPlantilla.bloqueado.text() == 0) {
		param.sesionPlantilla.bloqueado=<bloqueado>false</bloqueado>
	} else if (param.sesionPlantilla.bloqueado.text() == 1) {
		param.sesionPlantilla.bloqueado=<bloqueado>true</bloqueado>
	} else {
		log.warn("LOG(WARN) : postAltaEdicionSesion. El dato bloqueado llega con valor distinto a 0 ? 1.")
	}
	
	

	//Formateamos la fecha de inicio y fin de tornos. Le quitamos la parte de la hora
	var date_initornos = param.sesionPlantilla.fechainiciotorno.split("-");
	date_initornos = date_initornos[0];
	var date_fintornos = param.sesionPlantilla.fechafintorno.split("-");
	date_fintornos = date_fintornos[0];
	
	//Formateamos las fechas de inicio, inicio de tornos y fin de tornos concatenando la fecha y la hora
	var fecha_inicio = getFecha(param.sesionPlantilla.fecha, param.sesionPlantilla.horainicio);
	var fecha_initornos = getFecha(date_initornos, param.sesionPlantilla.aperturatornos);
	var fecha_fintornos = getFecha(date_fintornos, param.sesionPlantilla.cierretornos);
	
	//Calculamos las horas relativas al inicio de la sesión y actualizamos el xml con el valor calculado
	var horas_antesini = calculaHorasRelativasInicio (fecha_initornos, fecha_inicio);
	param.sesionPlantilla.aperturatornos = <aperturatornos>{horas_antesini}</aperturatornos>
	
	var horas_despuesini = calculaHorasRelativasInicio (fecha_inicio, fecha_fintornos);
	param.sesionPlantilla.cierretornos = <cierretornos>{horas_despuesini}</cierretornos>;
	
	// N? localidades : xxxx en la Zona : xxxx por el motivo : xxxx y el usuario : xxxxx
	for each (var i in param.sesionPlantilla.estadolocalidads.Estadolocalidad) {
		if ((i.estado.idestado.text()-0) == (estado_ocupada-0)) { 
			//log.info("Por el if, Estamos en el preproceso : "+i.estado.idestado.text()+" y estado_ocupada : "+estado_ocupada);
			var aux_texto ='';
			if (i.usuario.nombre.text() != 0) {
				aux_texto = i.usuario.nombre.text();
				if (i.usuario.apellidos.text() != 0) {
					aux_texto = aux_texto+" "+i.usuario.apellidos.text();
				}
			} else if (i.usuario.apellidos.text() != 0) {
				aux_texto = i.usuario.apellidos.text();
			}
			i.usuario.textoDescriptivo=<textoDescriptivo>{aux_texto}</textoDescriptivo>;
			aux_texto = 'N\u00BA localidades : '+i.nrolocalidadesbloqueadas.text();
			if (i.zona.nombre.text() != 0) {
				aux_texto = aux_texto+' Zona : '+i.zona.nombre.text();			
			}		
			aux_texto = aux_texto+' Motivo : '+i.motivobloqueo.text()+' Bloqueado por : '+i.usuario.textoDescriptivo.text();
			i.textoDescriptivo=<textoDescriptivo>{aux_texto}</textoDescriptivo>;		
		}
	}
	
	/**
		Agregamos esta parte del XML que sera donde guardemos los bloqueos que
		van a ser dados de baja **/
	param.sesionPlantilla.libres = <libres/>
	
	return param;
//Modificaremos la fecha y hora de inicio venta. 
}

/**Función que genera una fecha completa 
* param fecha: fecha en el formato dd/mm/yyyy
* param horaminutos: hora en el formato hh:mm
**/
function getFecha(fecha, horaminutos){
	var partes_fecha = fecha.split("/");
	var partes_hm = horaminutos.split(":");
	
	var f = new Date(partes_fecha[2], partes_fecha[1], partes_fecha[0],partes_hm[0], partes_hm[1]);
	
	return f;
}

/** Función que obtiene el tiempo transcurrido entre dos fechas
* param fecha1: fecha anterior
* param fecha2: fecha posterior
**/
function restaFechas(fecha1, fecha2) {
		
		var lf1=  fecha1.getTime();
//		Le quitamos una hora que es cuando empieza el Date
		var lf2 = fecha2.getTime() - lf1 - 3600* 1000;
		
		var date = new Date(lf2);
		
		return date;
}

/** Función que obtiene las horas transcurridas desde una fecha en función de otra de referencia
* param fantes: fecha anterior
* param fdespues: fecha posterior
**/
function calculaHorasRelativasInicio(fantes, fdespues){
	var f_antesini = restaFechas(fantes, fdespues);
	var horas = f_antesini.getHours();
	var min = f_antesini.getMinutes();

	if(horas < 10 ){
		horas = "0" + horas;
	}
	if(min < 10 ){
		min = "0" + min;
	}
	
	horas = horas + ":" + min;
	
	return horas;
}
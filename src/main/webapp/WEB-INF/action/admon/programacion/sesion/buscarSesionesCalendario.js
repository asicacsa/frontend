//var service = context.beans.getBean('httpServiceSOA');

// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = request.getParameter('servicio');
	
	//log.info("Entrada a buscarSesionesCalendario.js");
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		//log.info('El XML es NULO');
		var envio = '<servicio><parametro><buscarsesionesparam/></parametro></servicio>'
	} else {
		xml = new XML(xml);
		
		
		//log.info("LOG(INFO) : buscarSesionesCalendario.js params before the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		
		var xml_anyo = xml.anyo.text();
		var xml_mes = xml.mes.text();
		
		xml = this.preProcessXML(xml);
		//log.info("LOG(INFO) : buscarSesionesCalendario.js params after the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>	
		//log.info("Este es el envio : "+envio);	
	}
	
	
	var respuesta;
	
	try{
		respuesta = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {

		if (log.isErrorEnabled()) {	
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : con esta llamada : ' + envio);
			log.error('LOG(ERROR) : result in this Exception : '+ex);
		}
		
		throw ex.javaException;
	}
	
	if (respuesta!=null) {
		respuesta = new XML(respuesta);	
		respuesta = this.postProcessXML(respuesta,xml_anyo,xml_mes,xml.idrecinto);	
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
	
}


function preProcessXML(param) {

	
	/* Elimino el mes y el a?o por que no los podemos enviar en el dto*/
	
	delete param.anyo;
	delete param.mes;
	
	//log.info("LOG(INFO) : params before the borraElementosSinHijos : "+this.getClass().getName()+" "+param.toXMLString());
	param = comun.borraElementosSinHijos(param.toXMLString());
	//log.info("LOG(INFO) : params after the borraElementosSinHijos : "+this.getClass().getName()+" "+param.toXMLString());	
	param = comun.deleteSelectedTag(param.toXMLString());
	param = new XML(param);
	//log.info("LOG(INFO) : params after the deleteSelectedTag : "+this.getClass().getName()+" "+param.toXMLString());		
	
	return param;
	
}

function postProcessXML(result,anyo,mes,idrecinto) {
	var month = <month{mes}/>;
	

	if (result!=null) {
		result= new XML(result);
		var day;
/**
	XML de referencia : 
		<dayXXX>
			<event/>
		</dayXXX>
**/
		var day_flag;
		for each (i in result.Sesion) {
			if (day == null) {
				var aux = i.fecha.text();
				day_flag = aux.substr(0,aux.indexOf('/'));
				if (day_flag.substr(0,1) == '0') {
					day_flag=day_flag.substr(1,1);
				}
				day = <day{day_flag}>							
						</day{day_flag}>
				var aux_idsesion = i.idsesion.text();
				var aux_horinicio_hora = i.horainicio.text().substr(0,i.horainicio.text().indexOf(':'));
				var aux_horinicio_min = i.horainicio.text().substr(i.horainicio.text().indexOf(':')+1,2);				

				//veo el si la sesion est? o no bloqueada y en funcion de eso le cambio el color
				var aux_bloqueado = i.bloqueado.text();
				var aux_bloqueado_color;

				if(aux_bloqueado == '1'){
					aux_bloqueado_color = 'astro';
				}else{
					aux_bloqueado_color = 'milestone';
					}

				var aux_horfin_hora = i.horafin.text().substr(0,i.horafin.text().indexOf(':'));
				var aux_horfin_min = i.horafin.text().substr(i.horafin.text().indexOf(':')+1,2);				
				var event = <event idsesion={aux_idsesion} idRecinto={idrecinto}>
								<summary value={i.contenido.nombre}/>
								<comment value=""/>
								<start year={anyo} month={mes} day={day_flag} hour={aux_horinicio_hora} minute={aux_horinicio_min}/>
								<end year={anyo} month={mes} day={day_flag} hour={aux_horfin_hora} minute={aux_horfin_min}/>
								<category value={aux_bloqueado_color}/>
								<uid value={aux_idsesion}/>
							</event>
				day.appendChild(event);
			} else {
				var aux = i.fecha.text();
				var aux_day = aux.substr(0,aux.indexOf('/'));

				//veo el si la sesion est? o no bloqueada y en funcion de eso le cambio el color
				var aux_bloqueado = i.bloqueado.text();
				var aux_bloqueado_color;

				if(aux_bloqueado == '1'){
					aux_bloqueado_color = 'astro';
				}else{
					aux_bloqueado_color = 'milestone';
					}
				
				
				if (aux_day.substr(0,1) == '0') {
					aux_day=aux_day.substr(1,1);
				}
				if (aux_day != day_flag) {
					month.appendChild(day);
					day_flag = aux_day;
					day = <day{day_flag}/>
				}
				var aux_idsesion = i.idsesion.text();
				var aux_horinicio_hora = i.horainicio.text().substr(0,i.horainicio.text().indexOf(':'));
				var aux_horinicio_min = i.horainicio.text().substr(i.horainicio.text().indexOf(':')+1,2);				

				var aux_horfin_hora = i.horafin.text().substr(0,i.horafin.text().indexOf(':'));
				var aux_horfin_min = i.horafin.text().substr(i.horafin.text().indexOf(':')+1,2);				
				var event= <event idsesion={aux_idsesion} idRecinto={idrecinto}>
								<summary value={i.contenido.nombre}/>
								<comment value=""/>
								<start year={anyo} month={mes} day={day_flag} hour={aux_horinicio_hora} minute={aux_horinicio_min}/>
								<end year={anyo} month={mes} day={day_flag} hour={aux_horfin_hora} minute={aux_horfin_min}/>
								<category value={aux_bloqueado_color}/>
								<uid value={aux_idsesion}/>
							</event>
				day.appendChild(event);				
			}
		}
		if (day!=null) {
			month.appendChild(day);	
		}
		
	}	
	var resultado = <vcalendar>
						<year{anyo}>																						
							{month}
						</year{anyo}>
					</vcalendar>
	
	return resultado;	
}
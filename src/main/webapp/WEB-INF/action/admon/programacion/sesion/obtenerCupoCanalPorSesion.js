var services = context.beans.getBean('httpServiceSOA');

var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {

	var idSesion = request.getParameter('idSesion');		
	var resultado;
	var methodpost = 'obtenerCupoCanalPorSesion';
	var envio;
	
	if(idSesion != null){
		idSesion = new XML(idSesion);
		envio = <servicio>
					<parametro>
						<int>{idSesion}</int>
					</parametro>
				</servicio>
	}
	
	
	var aux;
		
	try{
		aux = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {

		if (log.isErrorEnabled()) {	
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : con esta llamada : ' + envio);
			log.error('LOG(ERROR) : result in this Exception : '+ex);
		}
		
		throw ex.javaException;
	}
	
	
	//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
	//En aux tenemos la respuesta.
	if(aux != null){
	//log.info('aux es distinto de nulo');
		aux = new XML(aux);
		resultado = aux;
				
		//log.info("Este es el resultado : "+resultado);
	} else {
		resultado = new XML('<ok/>');
		//log.info("El aux es null");
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
	
}
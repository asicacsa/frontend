var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {

	if (log.isInfoEnabled()) {
		log.info("entrada a actualizarPresentacionsesions.action");
	}
	
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}

	// si xml no se envia, llama al metodo sin parametros	
	var xmlListaAdd = request.getParameter('xmlListaAdd');
	var xmlListaEdit = request.getParameter('xmlListaEdit');
	var xmlListaDelete = request.getParameter('xmlListaDelete');
	
	if (log.isInfoEnabled()) {
		log.info("actualizarPresentacionsesions.action, method: " + methodpost);
		log.info("actualizarPresentacionsesions.action, xml: " + xml);
	}
	
		var sbf = context.beans.getBean('scriptBeanFactory');
		var comun = sbf.getBean('transformerXML.js');
		
		xmlListaAdd = comun.borraElementosSinHijos(xmlListaAdd);
		xmlListaEdit = comun.borraElementosSinHijos(xmlListaEdit);
		xmlListaDelete = comun.borraElementosSinHijos(xmlListaDelete);

		xmlListaAdd = preprocessXML(xmlListaAdd);
		xmlListaEdit = preprocessXML(xmlListaEdit);
		xmlListaDelete = preprocessXML(xmlListaDelete);
		
			var xml = <servicio>
					<parametro>
						{xmlListaAdd}
						{xmlListaEdit}
						{xmlListaDelete}
					</parametro>
				  </servicio>

	
	if (log.isInfoEnabled()) {
		log.info("actualizarPresentacionsesions.action, service parameters: " + xml);
	}
	
	var result = null;
	try {
		result = new XML(services.process(methodpost+'SOAWrapper',xml));
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost);
			log.error("LOG(ERROR) : con esta llamada: " + xml);
			log.error('LOG(ERROR) : con este tipo de error : ' + ex);
			log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
		}
		
		throw ex.javaException;
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
}

function preprocessXML(lista){
	for each (ps in lista.Presentacionsesion){
		delete ps.sesion.nombre;
	}
	return lista;
}

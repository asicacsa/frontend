var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var estado = request.getParameter('estado');		
	var resultado;
	var methodpost = 'bloquearLocalidadesZonaNoNumerada';
	var envio;
	
	
	if (xml!=null) {
	
		xml = new XML(xml);
		estado = new XML(estado);
	
		envio = 
			<servicio>
				<parametro>
					{estado}
					{xml}
				</parametro>
			</servicio>			

		
		envio = this.preProcessXML(envio);
	
		var aux;
		
		try{
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {

			if (log.isErrorEnabled()) {	
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				log.error('LOG(ERROR) : con esta llamada : ' + envio);
				log.error('LOG(ERROR) : result in this Exception : '+ex);
			}
			
			throw ex.javaException;
		}
				
		if(aux != null){
			aux = new XML(aux);
			resultado = aux;
		}			
					
	} else {
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	if (param.parametro.Estadolocalidad.desbloqueasoloresponsable.text() == "false") {
		param.parametro.Estadolocalidad.desbloqueasoloresponsable = <desbloqueasoloresponsable>0</desbloqueasoloresponsable>
	} else if (param.parametro.Estadolocalidad.desbloqueasoloresponsable.text() == "true") {
		param.parametro.Estadolocalidad.desbloqueasoloresponsable = <desbloqueasoloresponsable>1</desbloqueasoloresponsable>
	} else {
	
		log.warn("LOG(WARN) : bloquearLocalidadesZonaNoNumerada. El dato desbloqueasoloresponsable llega con valor distinto a 0 ? 1.")
	}
	param = comun.borraElementosSinHijos(param.toXMLString());

	return param;
	
}
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');

/**
dadodebaja : Integer
idtaquilla : Integer
nombre : String
orden : BigDecimal
recinto : Recinto
ubicacion : Ubicacion
**/

function handle(request, response) {

	var xml = request.getParameter('xml');
	var methodpost = request.getParameter('servicio');
	var resultado;
	var envio;
	var aux;

	log.info("LOG(INFO) : Call to getEdicionAltaIdiomaZZZZZZZZ."+this.getClass().getName());

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}

	if (xml!=null) {
		envio	= <servicio>
				<parametro>
				<java.lang.Long>
					{xml}
				</java.lang.Long>
				</parametro>
			</servicio>
	} else {
		envio	= <servicio>
				<parametro>
				<IdiomaEnc/>
				</parametro>
			</servicio>;
		methodpost = 'getXMLModel';
	}

	log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());
	log.info("LOG(INFO) : with this parametersAAAAAAA : "+envio+' : '+this.getClass().getName());
//	log.info("LOG(INFO) : using service : "+services.toString());
	log.info("LOG(INFO) : service==null"+(services==null));
	if (log.isDebugEnabled()) {
		try {
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
	} else {
		aux = services.process(methodpost+'SOAWrapper',envio);
	}

	log.info("LOG(INFO) : result : "+aux+' : '+this.getClass().getName());
	if (aux != null) {
		resultado = new XML(aux);
		if (resultado.hasOwnProperty('IdiomaEnc')) {
			resultado = resultado.Idioma;
		} else {
			if (!(resultado.name() == 'IdiomaEnc')) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result does not Idioma : '+ex);
			var ex = new java.lang.Exception('Exception calling this service : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			throw ex;
			}
		}
		resultado = this.postProcessXML(resultado);
		log.info("LOG(INFO) : result post processed : "+resultado+' : '+this.getClass().getName());
		//log.info('Fin de la edicion del valor de retorno : '+resultado.toXMLString());
	}
	log.info("LOG(INFO) : final response : "+resultado+' : '+this.getClass().getName());
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}


function postProcessXML(xmlparam) {
	//return xmlparam;
	if (xmlparam != null) {
		xmlparam = new XML(xmlparam);
		log.info("LOG(INFO) : postProcessXML.xmlparam : "+xmlparam);
	/*
		for each (i in param.Idioma) {
			param.Idioma.porDefecto = param.Idioma.porDefecto
			delete i.selected;
			//borramos el nombre completo ya que no es un campo en s� de cliente.
			//S�lo se recuper� como campo combinado para el listado de usuarios de grupos.
			delete i.nombrecompleto;
		}
	*/
	}
	return xmlparam;
}
var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='guardarIdioma';
var servicio_edicion ='guardarIdioma';

function handle(request, response) {

	var xml = request.getParameter('xml');
	var idioma_constant= 'IdiomaEnc';
	var id_constant = 'cod';
	var resultado;
	var methodpost;
	var envio;

	log.info("LOG(INFO) : Call to postEdicionAltaIdioma : "+this.getClass().getName());

	if (xml!=null) {
		xml = new XML(xml);
		if (xml.hasOwnProperty(idioma_constant)) {
			//Preparamos el xml para enviarlo con <list/>, <arrayList/>, ya vorem...
			xml = xml.IdiomaEnc;
		} else if (!(xml.name() == idioma_constant)) {
			var ex = new java.lang.Exception('Exception in postEdicionAltaIdioma : '+this.getClass().getName());
			log.error('LOG(ERROR) : does not exist Idioma in the parameters of the call.',ex);
			throw ex;
		}// Si su nombre es Unidadnegocio es que
	
		log.info("LOG(INFO) : params before the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
	//	xml = this.preProcessXML(xml);
		log.info("LOG(INFO) : params after the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
	
		//xml = this.preProcessXML(xml);
		//Ahora creamos el envio...
		//var methodpost = request.getParameter('servicio');
	
		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicio de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.
		if(methodpost == null) {
			methodpost = servicio_alta;
			envio =
			<servicio>
				<parametro>
				{xml}
				</parametro>
			</servicio>
		}
	
		log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());
		log.info("LOG(INFO) : with this parameters : "+envio+' : '+this.getClass().getName());
		//Y llamamos al servicio...
		var aux;
		if (log.isDebugEnabled()) {
			try {
			aux = services.process(methodpost+'SOAWrapper',envio);
			 } catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
			 }
		} else {
			aux = services.process(methodpost+'SOAWrapper',envio);
		}
	
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.
	
		resultado =
			<ok>
				{aux}
			</ok>
		log.info("LOG(INFO) : result : "+resultado+' : '+this.getClass().getName());
		//En aux tenemos la respuesta
	} else {
		var ex = new java.lang.Exception('Exception in postEdicionAltaIdioma : '+this.getClass().getName());
		log.error('LOG(ERROR) : does not exist params in the call.',ex);
		throw ex;
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	if (param != null) {
		param = new XML(param);
		param = this.preProcessUsuarios(param);
	}
	return param;
}

function preProcessUsuarios(param) {
	for each (i in param.porDefecto) {
		log.info("LOG(INFO) : preProcessUsuarios.i : "+i);
		i=(i==true)?1:0;
		log.info("LOG(INFO) : preProcessUsuarios.i : "+i);
	}
	param.porDefecto=(param.porDefecto==true?1:0);
	return param;
}

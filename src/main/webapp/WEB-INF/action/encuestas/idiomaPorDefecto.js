var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');

/**
dadodebaja : Integer
idtaquilla : Integer
nombre : String
orden : BigDecimal
recinto : Recinto
ubicacion : Ubicacion
**/

function handle(request, response) {

	var xml = request.getParameter('xml');
	var methodpost = request.getParameter('servicio');
	var resultado;
	var envio;
	var aux;
	
	log.info("LOG(INFO) : Call to idiomaPorDefecto."+this.getClass().getName());

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}

	if (xml!=null) {
		envio	= <servicio>
				<parametro>
				<java.lang.Long>
					{xml}
				</java.lang.Long>
				</parametro>
			</servicio>
	} else {
		envio	= <servicio>
				<parametro>
				<Idioma/>
				</parametro>
			</servicio>;
		methodpost = 'getXMLModel';
	}

	log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());
	log.info("LOG(INFO) : with this parameters : "+envio+' : '+this.getClass().getName());
	//log.info("LOG(INFO) : using service : "+services.toString());
	log.info("LOG(INFO) : service==null"+(services==null));
	if (log.isDebugEnabled()) {
		try {
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
	} else {
		aux = services.process(methodpost+'SOAWrapper',envio);
	}

	resultado =
		<ok>
			{aux}
		</ok>;

	log.info("LOG(INFO) : result : "+resultado+' : '+this.getClass().getName());

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}


function postProcessXML(xmlparam) {
	//return xmlparam;
	if (xmlparam != null) {
		xmlparam = new XML(xmlparam);
		log.info("LOG(INFO) : postProcessXML.xmlparam : "+xmlparam);
	/*
		for each (i in param.Idioma) {
			param.Idioma.porDefecto = param.Idioma.porDefecto
			delete i.selected;
			//borramos el nombre completo ya que no es un campo en s� de cliente.
			//S�lo se recuper� como campo combinado para el listado de usuarios de grupos.
			delete i.nombrecompleto;
		}
	*/
	}
	return xmlparam;
}
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');

function handle(request, response) {
	log.info("LOG(INFO)ZZZ : Entrando en obtenerNuevasPreguntasEncuesta.js");
	var xml = request.getParameter('xml');
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var resultado;
	var envio;

	if (xml!=null) {
		envio	= '<servicio>\n'+
				'	<parametro>\n'+
				'	'+xml+'\n'+
				'	</parametro>\n'+
				'</servicio>';
	} else {
		envio	= <servicio>
				<parametro>
				</parametro>
			</servicio>;
	}

	log.info("LOG(INFO)ZZZ : PASO 1");
	var aux;
	if (log.isDebugEnabled()) {
		try {
			log.info("LOG(INFO)ZZZ : PASO 2");
					aux = services.process(methodpost+'SOAWrapper',envio).trim();
			log.info("LOG(INFO)ZZZ : PASO 3");
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
	} else {
		log.info("LOG(INFO)ZZZ : PASO 2B");
		aux = services.process(methodpost+'SOAWrapper',envio).trim();
		log.info("LOG(INFO)ZZZ : PASO 3B");
	}
	log.info("LOG(INFO)ZZZ : PASO 4");
	log.info(aux);
	log.info("LOG(INFO)ZZZ : PASO 5");
	if (aux != null) {
		resultado = new XML(aux);
	/*
		if (resultado.hasOwnProperty('Encuesta')) {
			resultado = resultado.Encuesta;
		} else {
			if (!(resultado.name() == 'Encuesta')) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result does not Encuesta : '+ex);
			var ex = new java.lang.Exception('Exception calling this service : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			throw ex;
			}
		}
	*/
	//	resultado = this.postProcessXML(resultado);
		log.info("LOG(INFO) : result post processed : "+resultado+' : '+this.getClass().getName());
	}
	log.info("LOG(INFO)ZZZ : final response : "+resultado+' : '+this.getClass().getName());
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}


function postProcessXML(param) {
	//log.info("Estamos postProcesando : ");
	for each (i in param) {
		// formateo de fechainicio
		//log.info("Entramos en el for : "+i)
		var aux = i.fecIni.text();
		if (aux.length()>0){
			aux = aux.substr(0,aux.indexOf('-'));
			i.fecIni = <fecIni>{aux}</fecIni>;
		}
		// formateo de fechafin
		aux = i.fecFin.text();
		if (aux.length()>0){
			aux = aux.substr(0,aux.indexOf('-'));
			i.fecFin = <fecFin>{aux}</fecFin>;
		}
	}
	return param;
}
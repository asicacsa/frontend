var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='guardarPregunta';
var servicio_edicion ='guardarPregunta';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {

	var xml = request.getParameter('xml');
	var pregunta_constant= 'Pregunta';
	var id_constant = 'cod';
	var resultado;
	var methodpost;
	var envio;

	log.info("LOG(INFO) : Call to postEdicionAltaPregunta : "+this.getClass().getName());

	if (xml!=null) {
		xml = new XML(xml);
		if (xml.hasOwnProperty(pregunta_constant)) {
			xml = xml.Pregunta;
		} else if (!(xml.name() == pregunta_constant)) {
			var ex = new java.lang.Exception('Exception in postEdicionAltaPregunta : '+this.getClass().getName());
			log.error('LOG(ERROR) : does not exist Pregunta in the parameters of the call.',ex);
			throw ex;
		}
		log.info("LOG(INFO) : params before the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		xml = this.preProcessXML(xml);
		log.info("LOG(INFO) : params after the transformation : "+this.getClass().getName()+" "+xml.toXMLString());

		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicio de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.
		if(methodpost == null) {
			methodpost = servicio_alta;
			envio =
			<servicio>
				<parametro>
				{xml}
				</parametro>
			</servicio>
		}

		log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());
		log.info("LOG(INFO) : with this parameters : "+envio+' : '+this.getClass().getName());
		//Y llamamos al servicio...
		var aux;
		if (log.isDebugEnabled()) {
			try {
				aux = services.process(methodpost+'SOAWrapper',envio);
			} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
				}
		} else {
			aux = services.process(methodpost+'SOAWrapper',envio);
		}
	
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.
	
		resultado =
			<ok>
				{aux}
			</ok>
		log.info("LOG(INFO) : result : "+resultado+' : '+this.getClass().getName());
		//En aux tenemos la respuesta
	} else {
		var ex = new java.lang.Exception('Exception in postEdicionAltaPregunta : '+this.getClass().getName());
		log.error('LOG(ERROR) : does not exist params in the call.',ex);
		throw ex;
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	if (param != null) {
	param = new XML(param);
	param = comun.borraElementosSinHijos(param.toXMLString());
	}
	return param;
}
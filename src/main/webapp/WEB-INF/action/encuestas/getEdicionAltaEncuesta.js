var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');

function handle(request, response) {

	var xml = request.getParameter('xml');
	var methodpost = request.getParameter('servicio');
	var resultado;
	var envio;
	var aux;

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}

	if (xml!=null) {
		envio	= <servicio>
				<parametro>
				<java.lang.Long>
					{xml}
				</java.lang.Long>
				</parametro>
			</servicio>
	} else {
		envio	= <servicio>
				<parametro>
				<Encuesta/>
				</parametro>
			</servicio>;
		methodpost = 'getXMLModel';
	}

	if (log.isDebugEnabled()) {
		try {
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
	} else {
		aux = services.process(methodpost+'SOAWrapper',envio);
	}

	if (aux != null) {
		resultado = new XML(aux.trim());
		if (resultado.hasOwnProperty('Encuesta')) {
			resultado = resultado.Encuesta;
		} else {
			if (!(resultado.name() == 'Encuesta')) {
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				log.error('LOG(ERROR) : result does not Encuesta : '+ex);
				var ex = new java.lang.Exception('Exception calling this service : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				throw ex;
			}
		}
		resultado = this.postProcessXML(resultado);
		log.info("LOG(INFO) : result post processed : "+resultado+' : '+this.getClass().getName());
	}
	log.info("LOG(INFO)ZZZ : final response : "+resultado+' : '+this.getClass().getName());
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}


function postProcessXML(param) {
	//log.info("Estamos postProcesando : ");
	for each (i in param) {
		// formateo de fechainicio
		//log.info("Entramos en el for : "+i)
		var aux = i.fecIni.text();
		if (aux.length()>0){
			aux = aux.substr(0,aux.indexOf('-'));
			i.fecIni = <fecIni>{aux}</fecIni>;
		}
		// formateo de fechafin
		aux = i.fecFin.text();
		if (aux.length()>0){
			aux = aux.substr(0,aux.indexOf('-'));
			i.fecFin = <fecFin>{aux}</fecFin>;
		}
	}
	return param;
}
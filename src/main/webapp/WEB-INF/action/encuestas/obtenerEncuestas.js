var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {

	var methodpost = 'obtenerEncuestas';
	if (methodpost == null) {
		methodpost = request.getParameter('servicio');
	}
	if (methodpost == null) {
		methodpost = request.getParameter('__request_service');
	}
	// si xml no se envia, llama al metodo sin parametros
	var selecciona = null;

	enviar_id = request.getParameter('xml');
	//log.info("LOG(INFO) : Call to obtenerTemporadasProgramacion : "+this.getClass().getName());

	// En este tipo de servicio no van a haber parametros de Entrada.
	var envio = '<servicio />';

	//log.info("Metodo a llamar : "+methodpost);
	if (log.isInfoEnabled()) {
		log.info("Envio : "+envio);
	}

	var result = services.process(methodpost+'SOAWrapper',envio);
	result=result.trim();
	if (log.isInfoEnabled()) {
		log.info(result);
	}

	if (result !=	null) {
		result = new XML(result);
		result = this.postProcessXML(result);
	} else {
		result = new XML();
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
}

function postProcessXML(param) {
	//log.info("Estamos postProcesando : ");
	for each (i in param.Encuesta) {
		// formateo de fechainicio
		//log.info("Entramos en el for : "+i)
		var aux = i.fecIni.text();
		if (aux.length()>0){
			aux = aux.substr(0,aux.indexOf('-'));
			i.fecIni = <fecIni>{aux}</fecIni>;
		}
		// formateo de fechafin
		aux = i.fecFin.text();
		if (aux.length()>0){
			aux = aux.substr(0,aux.indexOf('-'));
			i.fecFin = <fecFin>{aux}</fecFin>;
		}
	}
	return param;
}
var services = context.beans.getBean('inetServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
		
	var xml = request.getParameter('xml');

	if (log.isInfoEnabled()){
		log.info("Entra en el action de obtenerVentaPorIdQuiosco.");
	}

	var resultado;
	var methodpost = '/obtenerVentaPorIdQuiosco';
	var envio;
	
	
	if (xml!=null) {
		xml = new XML(xml);
		
		envio = 
			<servicio>
				{xml}
			</servicio>
		
		
		if (log.isDebugEnabled()){
			log.debug("obtenerVentaPorIdQuiosco, envio : " + envio);
			log.debug("obtenerVentaPorIdQuiosco, metodo post : " + methodpost);
		}
		log.info( 'Vamos a llamar al servicio ' + methodpost +'.invoke' );
		try {
			var aux = services.process( methodpost+'.invoke', envio );
		} catch ( ex ) {
			aux = '<error>' + ex.javaException.getMessage() + '</error>';
			resultado = new XML(aux);
		}
		log.info( 'La respuesta del metodo ' + methodpost + '.invoke es ' + aux );
		if(aux != null){
			resultado = new XML(aux);
		}

	} else {
		if (log.isDebugEnabled()){
			log.info("El xml que llega a obtenerVentaPorId es nulo!!!");
		}
		resultado = <error/>
	}
	log.info('El resultado generado es ' + resultado.toXMLString() );
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
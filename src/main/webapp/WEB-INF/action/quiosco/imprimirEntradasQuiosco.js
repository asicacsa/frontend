var services = context.beans.getBean('inetServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {

	var xml = request.getParameter('xml');
	var resultado;
	var methodpost = '/imprimirEntradasQuiosco';
	var envio;
	var ip;
	//var idusuario;
	
	var secureContextUtils = context.beans.getBean('secureContextUtils').getSecureContext();

	if (secureContextUtils != null) {
		ip = secureContextUtils.getRealIp();
		if (log.isInfoEnabled()) {
			log.info("	#### ip -->" + ip );
		}

		if (ip==null){
			if (log.isErrorEnabled()) {
				log.error("	#### No tenemos IP " );
			}
		}

	}else{
		if (log.isErrorEnabled()) {
			log.error("	#### No tenemos contexto " );
		}
	}

	xml +="<ip>" + ip + "</ip>";
	
	if (xml!=null) {
		
		envio = "<servicio><parametro>" + xml  +  "</parametro></servicio>";
				
		try {
			var aux = services.process(methodpost+'.vpi',envio);
		} catch ( ex ) {
			aux = '<error>' + ex.javaException.getMessage() + '</error>';
			resultado = new XML(aux);
		}
		log.info("## El ResultadoImpresionPR es devuelto como: \n "+ aux + "\n" );
		if( aux != null ){
			resultado = new XML(aux);
		}
	} else {
		if (log.isDebugEnabled()){
			log.info("El xml que llega a imprimirEntradasQuiosco es nulo!!!");
		}
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
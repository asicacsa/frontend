function dameCodBarrasCode128(psCodBarras){
//Funcion que calcula el codigo para un codigo de barras code128
//http://www.adams1.com/128table.html
//http://grandzebu.net/informatique/codbar-en/code128.htm
	print("Valor para el codigo de barras: ",psCodBarras);
	var tmp = 0;
    
    var Src = psCodBarras;
    var StrLen = Src.length;
    var Sum = 105;
    
    // start character (203) for Set C
	//Segun fuente estandar para C es 105 o sea que su ascii sera 137
    //var Message = "" + String.fromCharCode(203);
	//GGL 105 + 105
    var Message = "" + String.fromCharCode(210);

    var Weight = 1;
    var I = 0;
    
    var NextChar = 0;
    var CurrChar = 0;
    
    var ceroCharCode = "0".charCodeAt(0);
    var nueveCharCode = "9".charCodeAt(0);
    
    while (I < StrLen) {
        CurrChar = (Src.substr(I, 1)).charCodeAt(0);
        
        if ((I + 1) < StrLen) {
            NextChar = (Src.substr(I + 1, 1)).charCodeAt(0);
            
            //if (CurrChar >= ceroCharCode && CurrChar <= nueveCharCode && NextChar >= ceroCharCode && NextChar <= nueveCharCode) {
                //2 digits
				//GGL Aqui manejo los valores no los ascii
                tmp = (CurrChar - ceroCharCode) * 10 + (NextChar - ceroCharCode);
                
                //if (tmp == 0) {
                //    Message = Message + String.fromCharCode(192);
                //} else if (tmp > 0 && tmp < 95) {
                //    Message = Message + String.fromCharCode(tmp + 32);
                //} else {
                //    Message = Message + String.fromCharCode(tmp + 98);
                //}
                //trace(Message);
				if (tmp < 95) {
                    Message = Message + String.fromCharCode(tmp + 32);
				} else {
                    Message = Message + String.fromCharCode(tmp + 105);
				}
				//GGL El checksum se calcula con los value no con ascii
                Sum = Sum + Weight * tmp;
                I = I + 2;
           // } else {
           //     Message = Code128Auto(Src)
           //     return Message;
           // }
        }/* else {
            Message = Message + String.fromCharCode(198);
            Sum = Sum + Weight * 100;
            Weight = Weight + 1;
            
            if (CurrChar == 32) {
                Message = Message & String.fromCharCode(192);
            } else if (CurrChar == 127) {
                Message = Message + String.fromCharCode(193);
                Sum = Sum + Weight * 95;
            } else if (CurrChar < 127 && CurrChar > 32) {
                Message = Message + String.fromCharCode(CurrChar);
                Sum = Sum + Weight * (CurrChar - 32);
            } else {
                Message = Code128Auto(Src);
                return Message;
            }
            
            I = I + 1;
        }*/
        
        Weight = Weight + 1;
    }
    
    // add CheckDigit
    Sum = Sum % 103;
    
    //if (Sum == 0) {
    //    Message = Message + String.fromCharCode(192);
    //} else if (Sum <= 94) {    	
    //    Message = Message + String.fromCharCode(Sum + 32);
    //} else {
    //    Message = Message + String.fromCharCode(Sum + 98);
    //}
    if (Sum < 95) {    	
        Message = Message + String.fromCharCode(Sum + 32);
    } else {
        Message = Message + String.fromCharCode(Sum + 105);
	}
    // add stop character (204)
    //Message = Message + String.fromCharCode(204);
	// 106 + 105
	Message = Message + String.fromCharCode(211);   
    //trace(Message);
	//Devuelve todo codificado en ascii para imprimir
    return Message;
}

function Code128Auto(Src) {
    var StrLen = Src.length;
    var Sum = 104;
    
    // 2 indicates Set B
    var CurrSet = 2;
    
    // start character with value 202 for Set B
    Message = "" + String.fromCharCode(202);
    
    var CurrChar = Src.substr(1, 1).charCodeAt(0);
    
    if (CurrChar <= 31 && CurrChar >= 0) {
        // switch to Set A
       	// 1 indicates Set A
        CurrSet = 1;
        
        // start character with value 201 for Set A
        Message = "" + String.fromCharCode(201);
        Sum = 103;
    }
    
    Weight = 1;
    
    Message = GeneralEncode(Src, CurrSet);
    
    return Message;
}

function GeneralEncode(Src, CurrSet) {
    var tmp = 0;
    var CurrDone = false;
    
    var I = 1;
    
    var StrLen = Src.length;
    
    var CurrChar;
    var NextChar;
    
    var Weight = 0;
    var Sum = 0;
    
    while (I <= StrLen) {
        CurrChar = Src.substr(I, 1).charCodeAt(0);
        CurrDone = false;
        
        if ((I + 1) <= StrLen) {
            NextChar = Src.substr(I + 1, 1).charCodeAt(0);
            
            if (CurrChar >= "0".charCodeAt(0) && CurrChar <= "9".charCodeAt(0) && NextChar >= "0".charCodeAt(0) && NextChar <= "9".charCodeAt(0)) {
                tmp = (CurrChar - "0".charCodeAt(0)) * 10 + (NextChar - "0".charCodeAt(0));
                
                // 2 digits
                if (CurrSet != 3) {
                    // the previous set is not Set C
                    Message = Message + String.fromCharCode(99 + 98);
                    Sum = Sum + Weight * 99;
                    Weight = Weight + 1;
                    CurrSet = 3;
                }
                
                if (tmp == 0) {
                    Message = Message & String.fromCharCode(192);
                } else if (tmp > 0 && tmp < 95) {
                    Message = Message + String.fromCharCode(tmp + 32);
                } else {
                    Message = Message + String.fromCharCode(tmp + 98);
                }
                    
                Sum = Sum + Weight * tmp;
                I = I + 2;
                
                CurrDone = True;
            }
        }
        
        if (!CurrDone) {
            if (CurrChar >= 0 && CurrChar <= 31) {
                // choose Set A
                if (CurrSet != 1) {
                    // the previous set is not Set A
                    Message = Message + String.fromCharCode(101 + 98);
                    Sum = Sum + Weight * 101;
                    Weight = Weight + 1;
                    CurrSet = 1;
                }
                
                if (CurrChar == 31) {
                    Message = Message + String.fromCharCode(193);
                    Sum = Sum + Weight * 95;
                } else {
                    Message = Message & String.fromCharCode(CurrChar + 96);
                    Sum = Sum + Weight * (CurrChar + 64);
                }
            } else {
                // choose Set B
                if (CurrSet != 2) {
                    // the previous set is not Set B
                    Message = Message + String.fromCharCode(100 + 98);
                    Sum = Sum + Weight * 100;
                    Weight = Weight + 1;
                    CurrSet = 2;
                }
                
                if (CurrChar == 32) {
                    Message = Message + String.fromCharCode(192);
                } else if (CurrChar == 127) {
                    Message = Message + String.fromCharCode(193);
                    Sum = Sum + Weight * 95;
                } else if (CurrChar < 127 && CurrChar > 32) {
                    Message = Message + String.fromCharCode(CurrChar);
                    Sum = Sum + Weight * (CurrChar - 32);
                }
            }
            
            I = I + 1;
        }
        
        Weight = Weight + 1;
    }
    
    // add CheckDigit
    Sum = Sum % 103;
    
    if (Sum == 0) {
        Message = Message + String.fromCharCode(192);
    } else if (Sum <= 94) {
        Message = Message + String.fromCharCode(Sum + 32);
    } else {
        Message = Message + String.fromCharCode(Sum + 98);
    }
    
    // add stop character (204)
    Message = Message + String.fromCharCode(204);
    
    return Message;
}

//Funcion que calcula el digito de control para un codigo de barras ean13
function dameCodBarrasEan13(psCodBarras){
/*
	Este seudocodigo explica el algoritmo de calculo para el digito de control del ean13
	
	Suma = 0
	Por cada uno de los 12 digitos a codificar
	   Valor corrector = 1 si la posicion del digito es impar si la posicion es par
	   Suma = Suma + Valor del digito * Valor corrector
	Final de los digitos
	Digito de control = 10 - modulo 10 de Suma
	Si Digito de control = 10
	   Digito de control = 0
	Final Si

	Los codigos EAN se forman de la siguiente manera:
	Codigo_Pais + Codigo_Empresa + Codigo_Articulo + Check_Digit
	
	Informacion obtenida de http://www.fpress.com/revista/Num9905/Barras.htm
*/

	return strToEan13(psCodBarras);

	
}

function strToEan13(psCodigo){
    var lnCheckSum = 0;
    var i = 0;
    var lcRet = "";
    var lnAux = "";
    var lnPri = 0;
    var lcIni = "";
    var lcLat = "";
    var lcMed = "";
    var lcResto = "";
    var lcJuego = "";
    var lcCod = "";
    var sCaracterReemplazar = "";
	
	//--- Tabla de Juegos de Caracteres
	//--- segun "lnPri" (�NO CAMBIAR!)
    var laJuego = new Array("","AAAAAACCCCCC", "AABABBCCCCCC", "AABBABCCCCCC", "AABBBACCCCCC", "ABAABBCCCCCC", "ABBAABCCCCCC", "ABBBAACCCCCC", "ABABABCCCCCC", "ABABBACCCCCC", "ABBABACCCCCC");
	var sAux = "";
    
    lcRet = psCodigo;
	
	trace("psCodigo: " + psCodigo);
    
    if (psCodigo.length != 12) {
        //--- Error en parametro
        //--- debe tener un len = 12
        return "";
	}

   	//--- Genero digito de control
   	lnCheckSum = 0;   
   
	for (i = 0; i <= 11; i ++) {
    	if (i % 2 == 0) {
			lnCheckSum = lnCheckSum + parseInt(lcRet.substr(i, 1)) * 3;
	  	} else {
         	lnCheckSum = lnCheckSum + parseInt(lcRet.substr(i, 1)) * 1;
	  	}
   	}
	
	trace("lnCheckSum " + lnCheckSum);
   
   	lnAux = lnCheckSum % 10;
   
    if (lnAux != 0) {
        lnAux = 10 - lnAux;
	}
    
	trace("Digito control: " + lnAux);
	
   	lcRet = lcRet + lnAux.toString();

	trace("Codigo + dc: " + lcRet);
	
   	//--- Para imprimir con fuente True Type EAN13
   	//--- 1er. digito (lnPri)
   	lnPri = parseInt(lcRet.substr(1, 1));
   
   	//--- Caracter inicial (fuera del codigo)
   	lcIni = chr(lnPri + 35);
   
   	//--- Caracteres lateral y central
   	lcLat = chr(33);
   	lcMed = chr(45);

   	//--- Resto de los caracteres
	lcResto = lcRet.substr(1, 12);
   
   trace("digitos a codificar: " + lcResto);
   
   	for (i = 0; i < 12; i ++) {
      	lcJuego = laJuego[lnPri + 1].substr(i, 1);
		
		trace("lcResto.substr(" + i + ", 1): " + lcResto.substr(i, 1));
      
		if (lcJuego == "A") {
			sCaracterReemplazar = chr(parseInt(lcResto.substr(i, 1)) + 48);
		}
		if (lcJuego == "B") {
			sCaracterReemplazar = chr(parseInt(lcResto.substr(i, 1)) + 65);
		}
		if (lcJuego == "C") {
			sCaracterReemplazar = chr(parseInt(lcResto.substr(i, 1)) + 97);
		}
		
		sAux += sCaracterReemplazar;
		
		trace("sAux en pasada " + i + " " + sAux);
      
       	//Mid(lcResto.slice(i, 1) = sCaracterReemplazar
	}

   //--- Armo codigo
   lcCod = lcIni + lcLat + sAux.substr(0, 6) + lcMed + sAux.substr(6, 6) + lcLat;
   
   trace("Resultado " + lcCod);
   return lcCod;

}

/*
 * Funcion que imprimira la tarjeta
*/
function imprimirTarjeta(){
	print(this, "bmovie");
}

function getValue(caracter) {
	switch (caracter) {
		case " ":
            return 0;
        case "!":
            return 1;
        case "\"":
            return 2;
        case "#":
            return 3;
        case "$":
            return 4;
        case "%":
            return 5;
        case "&":
            return 6;
        case "'":
            return 7;
        case "(":
            return 8;
        case ")":
            return 9;
        case "*":
            return 10;
        case "+":
            return 11;
        case ",":
            return 12;
        case "-":
            return 13;
        case ".":
            return 14;
        case "/":
            return 15;
        case "0":
            return 16;
        case "1":
            return 17;
        case "2":
            return 18;
        case "3":
            return 19;
        case "4":
            return 20;
        case "5":
            return 21;
        case "6":
            return 22;
        case "7":
            return 23;
        case "8":
            return 24;
        case "9":
            return 25;
        case ":":
            return 26;
        case ";":
            return 27;
        case "<":
            return 28;
        case "=":
            return 29;
        case ">":
            return 30;
        case "?":
            return 31;
        case "@":
            return 32;
        case "A":
            return 33;
        case "B":
            return 34;
        case "C":
            return 35;
        case "D":
            return 36;
        case "E":
            return 37;
        case "F":
            return 38;
        case "G":
            return 39;
        case "H":
            return 40;
        case "I":
            return 41;
        case "J":
            return 42;
        case "K":
            return 43;
        case "L":
            return 44;
        case "M":
            return 45;
        case "N":
            return 46;
        case "O":
            return 47;
        case "P":
            return 48;
        case "Q":
            return 49;
        case "R":
            return 50;
        case "S":
            return 51;
        case "T":
            return 52;
        case "U":
            return 53;
        case "V":
            return 54;
        case "W":
            return 55;
        case "X":
            return 56;
        case "Y":
            return 57;
        case "Z":
            return 58;
        case "[":
            return 59;
        case "\\":
            return 60;
        case "]":
            return 61;
        case "^":
            return 62;
        case "_":
            return 63;
        case "`":
            return 64;
        case "a":
            return 65;
        case "b":
            return 66;
        case "c":
            return 67;
        case "d":
            return 68;
        case "e":
            return 69;
        case "f":
            return 70;
        case "g":
            return 71;
        case "h":
            return 72;
        case "i":
            return 73;
        case "j":
            return 74;
        case "k":
            return 75;
        case "l":
            return 76;
        case "m":
            return 77;
        case "n":
            return 78;
        case "o":
            return 79;
        case "p":
            return 80;
        case "q":
            return 81;
        case "r":
            return 82;
        case "s":
            return 83;
        case "t":
            return 84;
        case "u":
            return 85;
        case "v":
            return 86;
        case "w":
            return 87;
        case "x":
            return 88;
        case "y":
            return 89;
        case "z":
            return 90;
        case "{":
            return 91;
        case "|":
            return 92;
        case "}":
            return 93;
        case "~":
            return 94;
        default:
            return 0;
			
	}
}

function getChar(valor) {
	switch (valor) {
		case 0:
            return " ";
        case 1:
            return "!";
        case 2:
            return "\"";
        case 3:
            return "#";
        case 4:
            return "$";
        case 5:
            return "%";
        case 6:
            return "&";
        case 7:
            return "'";
        case 8:
            return "(";
        case 9:
            return ")";
        case 10:
            return "*";
        case 11:
            return "+";
        case 12:
            return ",";
        case 13:
            return "-";
        case 14:
            return ".";
        case 15:
            return "/";
        case 16:
            return "0";
        case 17:
            return "1";
        case 18:
            return "2";
        case 19:
            return "3";
        case 20:
            return "4";
        case 21:
            return "5";
        case 22:
            return "6";
        case 23:
            return "7";
        case 24:
            return "8";
        case 25:
            return "9";
        case 26:
            return ":";
        case 27:
            return ";";
        case 28:
            return "<";
        case 29:
            return "=";
        case 30:
            return ">";
        case 31:
            return "?";
        case 32:
            return "@";
        case 33:
            return "A";
        case 34:
            return "B";
        case 35:
            return "C";
        case 36:
            return "D";
        case 37:
            return "E";
        case 38:
            return "F";
        case 39:
            return "G";
        case 40:
            return "H";
        case 41:
            return "I";
        case 42:
            return "J";
        case 43:
            return "K";
        case 44:
            return "L";
        case 45:
            return "M";
        case 46:
            return "N";
        case 47:
            return "O";
        case 48:
            return "P";
        case 49:
            return "Q";
        case 50:
            return "R";
        case 51:
            return "S";
        case 52:
            return "T";
        case 53:
            return "U";
        case 54:
            return "V";
        case 55:
            return "W";
        case 56:
            return "X";
        case 57:
            return "Y";
        case 58:
            return "Z";
        case 59:
            return "[";
        case 60:
            return "\\";
        case 61:
            return "]";
        case 62:
            return "^";
        case 63:
            return "_";
        case 64:
            return "`";
        case 65:
            return "a";
        case 66:
            return "b";
        case 67:
            return "c";
        case 68:
            return "d";
        case 69:
            return "e";
        case 70:
            return "f";
        case 71:
            return "g";
        case 72:
            return "h";
        case 73:
            return "i";
        case 74:
            return "j";
        case 75:
            return "k";
        case 76:
            return "I";
        case 77:
            return "m";
        case 78:
            return "n";
        case 79:
            return "o";
        case 80:
            return "p";
        case 81:
            return "q";
        case 82:
            return "r";
        case 83:
            return "s";
        case 84:
            return "t";
        case 85:
            return "u";
        case 86:
            return "v";
        case 87:
            return "w";
        case 88:
            return "x";
        case 89:
            return "y";
        case 90:
            return "z";
        case 91:
            return "{";
        case 92:
            return "|";
        case 93:
            return "}";
        case 94:
            return "~";
        case 103, 104, 105, 106:
        	return chr(valor);
        default:
            return " ";
	}
}
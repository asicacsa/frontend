var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='darDeAltaDocumento';

function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	
	var resultado;
	
	var methodpost;
	
	var envio;
	//log.info('El XML que llega: '+xml);

	if (xml!=null) {
		xml = new XML(xml);
		
		xml = this.preProcessXML(xml);
		
		if (!(xml.hasOwnProperty('iddocumento'))) {
			
			methodpost = servicio_alta;
			envio = 
				<servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>
			
		} 
		
		if (log.isInfoEnabled()) {		
			log.info("Este es el envio : "+envio);
			log.info("Este es el metodo post : "+methodpost);
		}
		
		//Y llamamos al servicio...		
		var aux;
		
		try{
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
		 	log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		 }	
			
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		
		resultado = 
				<ok>
					{aux}
				</ok>
		//En aux tenemos la respuesta			
	} else {
		resultado = <error/>
	}
	
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	//log.info('Lo que llega a preProcessXML: '+param);
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('transformerXML.js');
	
	delete param..selected; //Eliminamos todos los tags selected
	
		//Obligatorio : Campo dado de baja, por defecto lo ponemos a 0 ...
	if (param.dadodebaja.text().length() == 0) {
			param.dadodebaja=0;
	}
	
	param = comun.borraElementosSinHijos(param); //Eliminamos todos los elementos que se han quedado sin hijos
	
	param = param.Documento;
	return param;
	
}


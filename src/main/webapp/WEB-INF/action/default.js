var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {
	
	if (log.isDebugEnabled()) {
		log.debug("entrando a default.action");
	}
	var methodpost = request.getParameter('servicio');
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	if (log.isDebugEnabled())
	{
		log.debug("default.action: vamos a llamar al metodo " + methodpost);
	}
	//si xml no se envia, llama al metodo sin parametros
	var xml = request.getParameter('xml');
	if (log.isDebugEnabled()) {
		log.debug("default.action: el parametro xml que nos llega es:\n\n" + xml + "\n\n");
	}
	if(xml == null ) {
		xml = '<servicio/>';
	} else {
		var xmlSinHijos;
		var sbf = context.beans.getBean('scriptBeanFactory');
		var comun = sbf.getBean('transformerXML.js');
		xmlSinHijos = comun.borraElementosSinHijos(xml);
		xmlSinHijos = comun.sustituyeLiteralesBooleanosEnVenta(xmlSinHijos);
		if (xmlSinHijos.name() == "servicio") {
			xml = xmlSinHijos;
		} else {
			if (xmlSinHijos.name() == "parametro") {
				xml = <servicio>
						{xmlSinHijos}
					</servicio>
			} else {
				xml = <servicio>
						<parametro>
							{xmlSinHijos}
						</parametro>
					</servicio>
			}
		}
	}
	if (log.isDebugEnabled()) {
		log.debug("default.action: parametro xml formateado para enviar a la capa de servicios :\n\n" + xml + "\n\n" );
	}
	var result = null;
	try {
		result = services.process(methodpost+'SOAWrapper',xml);
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.debug('\n\nLOG(ERROR) : Error in this call: '+this.getClass().getName());
			log.debug('LOG(ERROR) : to this: '+methodpost);
			log.debug("LOG(ERROR) : con estos parametros:\n" + xml);
			log.debug('LOG(ERROR) : con este tipo de error:\n' + ex);
			log.debug('LOG(ERROR) : con esta causa:\n'+ ex.javaException.getMessage() + '\n\n');
		}
		throw ex.javaException;
	}
	if (result == null) {
		result = "<ArrayList />";
	}
	if(log.isDebugEnabled()){
		log.debug("default.action: respuesta del servicio: \n\n" + result + "\n\n");
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result);
}
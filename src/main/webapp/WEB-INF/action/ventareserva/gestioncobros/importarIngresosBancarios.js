//var service = context.beans.getBean('httpServiceSOA');

// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {

	if (log.isInfoEnabled()) {
		log.info("Entrada a importarIngresosBancarios.");
	}
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	var methodpost = request.getParameter('servicio');
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var filename = request.getParameter('filename');
	
	if(filename == null) {
		var envio = '<servicio />';
	} else {
		var envio = <servicio>
						<parametro>
							<nombreFichero>
								{filename}
							</nombreFichero>
						</parametro>
					</servicio>;
	}
	
	if (log.isInfoEnabled()) {
		log.info("importarIngresosBancarios, envio: " + envio);
	}
	var respuesta;
	
	try{
		respuesta = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	
	if (respuesta!=null) {
		respuesta = new XML(respuesta);
		//respuesta = this.postProcessXML(respuesta);	
		if (log.isInfoEnabled()) {
			log.info("importarIngresosBancarios, respuesta: " + respuesta.toXMLString());
		}

	}

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
	
}
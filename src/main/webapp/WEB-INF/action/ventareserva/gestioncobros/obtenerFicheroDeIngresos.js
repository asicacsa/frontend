var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {

	//log.info("Entrada a obtenerFicheroDeIngresos.");	
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	var methodpost = request.getParameter('servicio');
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	//var xml = request.getParameter('xml');
	//if(xml == null ) {
	//	var envio = '<servicio />';
	//}
	
	var filename = request.getParameter('filename');
	
	if(filename == null) {
		var envio = '<servicio />';
	} else {
		var envio = <servicio>
						<parametro>
							<String>
								{filename}
							</String>
						</parametro>
					</servicio>;
	}
	
	if (log.isInfoEnabled()) {
		log.info("xml a pasar al servicio: " + envio);
	}
	
	//var respuesta = services.process(methodpost+'SOAWrapper',envio);
	
	// fake provisional
	var respuesta = <ArrayList>
						<Dtoingreso>
							<nombre>Spiderman</nombre>
							<localizador>L-00000001</localizador>
							<fecha>10/10/2006</fecha>
							<importe>100</importe>
						</Dtoingreso>
						<Dtoingreso>
							<nombre>Spiderman2</nombre>
							<localizador>L-00000002</localizador>
							<fecha>10/10/2006</fecha>
							<importe>100</importe>
						</Dtoingreso>
						<Dtoingreso>
							<nombre>Spiderman3</nombre>
							<localizador>L-00000003</localizador>
							<fecha>10/10/2006</fecha>
							<importe>100</importe>
						</Dtoingreso>
					</ArrayList>;
	//fFake provisional
	
	if (respuesta!=null) {
		respuesta = new XML(respuesta);
		//respuesta = this.postProcessXML(respuesta);	
		if (log.isInfoEnabled()) {
			log.info("Resultado de llamar a obtenerFicheroDeIngresos: "+respuesta.toXMLString());
		}

	}

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
	
}


/**
 * Ajustamos el XML a la estructura y campos de la vista.
 */
/*function postProcessXML(xmlparam) {
	var OPERACION_APERTURA = 1;
	var OPERACION_CIERRE = 2;
	
	if (xmlparam != null){
		xmlparam = new XML(xmlparam);
		var nombrecompleto = xmlparam.usuarioOperador.nombre.text() + " " + 
		                     xmlparam.usuarioOperador.apellidos.text();
		xmlparam.usuarioOperador.nombrecompleto = <nombrecompleto>{nombrecompleto}</nombrecompleto>;
		
		var fechaapertura = "";
		var fechacierre = "";
		var taquillas = new Array();
		for each (i in xmlparam.operacioncajas.Operacioncaja){
			if (i.tipooperacioncaja.text() == OPERACION_APERTURA){
				fechaapertura = i.fechayhoraoperacioncaja;
			} else if (i.tipooperacioncaja.text() == OPERACION_CIERRE){
				fechacierre = i.fechayhoraoperacioncaja;
			}
			
			// Creamos array de taquillas implicadas
			var encontrado = 0;
			for (var aux = 0; aux++; aux<taquillas.length || !encontrado){
				if (taquillas[aux] == i.idtaquilla){
					encontrado = 1;
				} 
			}
			if (encontrado == 0){
				taquillas[taquillas.length] = i.idtaquilla;
			}
		}
			
		// Fuera elementos innecesarios
		delete usuarioOperador.nombre;
		delete usuarioOperador.apellidos;
								
	}
	
	//log.warn("LOG(WARN) : getEdicionAltaRecintos. El dato numerada llega con valor distinto a 0 ? 1.")
	return xmlparam;
}*/
//var service = context.beans.getBean('httpServiceSOA');

// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	if (log.isInfoEnabled()) {
		log.info("Entrada a confirmarPagosReservas");
	}
	
	var methodpost = request.getParameter('servicio');
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		var envio = <servicio/>;
	} else {
	
		xml = new XML(xml);
		
		if (log.isInfoEnabled()) {
			log.info("confirmarPagosReservas, xml antes de preprocesado: " + xml.toXMLString());
		}
		
		xml = preProcessXML(xml);
		
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>	
		
		
		if (log.isInfoEnabled()) {
			log.info("confirmarPagosReservas, envio: " + xml.toXMLString());
		}
		
		var respuesta;
		
		try {
			respuesta = services.process(methodpost+'SOAWrapper',envio.toXMLString());
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
	
		
		resultado = <ok>{respuesta}</ok>;	
		log.info("confirmarPagosReservas, resultado: " + resultado);
		
	}
	
	response.setContentType('text/xml');
	response.setCharacterEncoding('UTF-8');
	response.writer.println(resultado.toXMLString());
}


/**
 * Ajustamos el XML a la estructura y campos de la vista.
 */
meta.addMethod(
	'preProcessXML', // param1: nombre de la funcion
	'String', // param2: string con los tipos de los parametros separados por coma
	'String' // param3: string con el tipo de retorno
); 
function preProcessXML(xmlparam){
	
	var lista = <list/>;
	
	if (xmlparam != null){	
		for each (var conciliacion  in xmlparam.Conciliacion){
			
			if (log.isInfoEnabled()) {
				log.debug("LOG(DEBUG) : valor de conciliacion: " + conciliacion );
			}
			
			var idreserva = conciliacion.ridreserva.text();
			var importepagado = conciliacion.iconsumido.text();
			var numeroreferenciaq57 = conciliacion.inumeroreferenciaq57.text();
			var idcobro = conciliacion.iidcobro.text();
			var referenciacobro = conciliacion.ireferenciacobro.text();
			var fechavalor = conciliacion.ifechavalor.text();
			var importetotalingreso = conciliacion.iimporte.text();
			var observaciones = conciliacion.iobservaciones.text();
			
			var conciliacionparam = <Conciliacionparam>
										<idreserva/>
										<importeparcials>
											<Importeparcial>
												<importe/>
												<ingresobancario>
													<numeroreferenciaq57/>
													<idcobro/>
													<referenciacobro/>
													<fechavalor/>
													<importe/>
													<observaciones/>
												</ingresobancario>
											</Importeparcial>
										</importeparcials>
									</Conciliacionparam>;
  
			if (idreserva!=null) conciliacionparam.idreserva = <idreserva>{idreserva}</idreserva>;
			if (importepagado!=null) conciliacionparam.importeparcials.Importeparcial.importe = <importe>{importepagado}</importe>;
			if (numeroreferenciaq57!=null) conciliacionparam.importeparcials.Importeparcial.ingresobancario.numeroreferenciaq57 = <numeroreferenciaq57>{numeroreferenciaq57}</numeroreferenciaq57>;
			if (idcobro!=null) conciliacionparam.importeparcials.Importeparcial.ingresobancario.idcobro = <idcobro>{idcobro}</idcobro>;
			if (referenciacobro!=null) conciliacionparam.importeparcials.Importeparcial.ingresobancario.referenciacobro = <referenciacobro>{referenciacobro}</referenciacobro>;
			if (fechavalor!=null) conciliacionparam.importeparcials.Importeparcial.ingresobancario.fechavalor = <fechavalor>{fechavalor}</fechavalor>;
			if (importetotalingreso!=null) conciliacionparam.importeparcials.Importeparcial.ingresobancario.importe = <importe>{importetotalingreso}</importe>;
			if (observaciones!=null) conciliacionparam.importeparcials.Importeparcial.ingresobancario.observaciones = <observaciones>{observaciones}</observaciones>;
			

			lista.* += conciliacionparam;		
		}						
	}
	
	lista = comun.borraElementosSinHijos(lista);
	
	return lista;
}

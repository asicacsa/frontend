<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ page import="java.net.*" %> 
<html>

	 <%
	 	 //Se invoca desde modulo de documentacion y cuarderno 57(defecto)
		 String modulo = request.getParameter("modulo"); 
	 
	 	 System.out.println(modulo);
	 %>
    <head>	
        <title>
        	<% if (modulo==null){ %>
					Apertura fichero Cuaderno 57
			<% }else if (modulo.equals("documentacion")) {%>
				    Gestion de Documentos
			<% }%>
        
        </title>
        
        <SCRIPT language="JavaScript" type="text/javascript">
		    <!--
		    if (document.images)
		    {
		      preload_image = new Image(372,252); 
		      preload_image.src="../../librerias/gifs/popUpCaja.gif"; 
		    }
		    //-->
	    </SCRIPT>
	    
	    <script type="text/javascript">
			function validate_required(field,alerttxt){
				with (field){
					if (value==null||value==""){
						alert(alerttxt);
						return false;
					} else {
						return true;
					}
				}
			}
			
			function validate_form(thisform){
				with (thisform){
					if (validate_required(file,"Debe especificar un fichero de ingresos.")==false){
						file.focus();
						return false;
					}
				}
			}
		</script>
	    
        <style>
			<!--
			 /* Style Definitions */
			 body {   
			 	/*background-image : url(../../librerias/gifs/popUpCaja.gif);*/
 				/*background-color : white;*/
 				background-repeat : no-repeat
 			 }
			 p.MsoNormal, li.MsoNormal, div.MsoNormal {
			 	mso-style-parent:"";
				margin:0cm;
				margin-bottom:.0001pt;
				mso-pagination:widow-orphan;
				font-size:10.0pt;
				font-family:"Verdana";
				color:#628099;
				mso-fareast-font-family:"Times New Roman";
			}
			p.MsoFile {
			 	mso-style-parent:"";
				margin:0cm;
				text-align:center;
				margin-bottom:.0001pt;
				mso-pagination:widow-orphan;
				font-size:10.0pt;
				font-family:"Verdana";
				color:#628099;
				mso-fareast-font-family:"Times New Roman";
			}
			p.MsoNaranja, li.MsoNaranja, div.MsoNaranja {
			 	mso-style-parent:"";
				margin:0cm;
				margin-bottom:.0001pt;
				mso-pagination:widow-orphan;
				font-size:11.0pt;
				font-family:"Verdana";
				font-weight:bold;
				color:#DC822F;
				mso-fareast-font-family:"Times New Roman";
			}
			@page Section1 {
				size:595.3pt 841.9pt;
				margin:70.85pt 3.0cm 70.85pt 3.0cm;
				mso-header-margin:35.4pt;
				mso-footer-margin:35.4pt;
				mso-paper-source:0;
			}
			div.Section1
				{page:Section1;}
		-->	
		</style>
		
        <!--<script language="JavaScript">
        	function windowConfig(){
        		self.resizeTo(500,300);
        		self.moveTo((screen.width-500)/2,(screen.height-300)/2);				
        	}   
        </script>-->
        
        
    </head>
    <%
		// parametros
		 String ruta = request.getParameter("ruta");
		 String modo = request.getParameter("modo");
		 String maxSizeFileUpload = request.getParameter("maxSizeFileUpload");
		 
		// out.print(ruta);	 
		// out.print(modo);	 
		
	%>
    <body bgcolor="white" background="../../librerias/gifs/popUpCaja.gif"><!-- onload="javascript:windowConfig();" bgcolor="DDE1E5" -->
        <table border="0" width="350" height="150" >
        	<tr>
        	<td>
	        <div class="Section1">
	        	<form method="post" action="/application/upload.form" enctype="multipart/form-data" onsubmit="return validate_form(this)">
	        		<input name="filerepository" value="<%=ruta%>" type="hidden" size="20"/>
					<input name="modo" value="<%=modo%>" type="hidden" size="20"/>
					<input name="maxSizeFileUpload" value="<%=maxSizeFileUpload%>" type="hidden" size="20"/>
				
					<p class=MsoNormal><o:p>&nbsp;</o:p></p>
					<p class=MsoNormal><o:p>&nbsp;</o:p></p>
					
					<% if (modulo==null){ %>
						<p class=MsoNaranja style='text-indent:32pt'>Apertura fichero Cuaderno 57</p>
					<% }else if (modulo.equals("documentacion")) {%>
						<p class=MsoNaranja style='text-indent:32pt'>Gestion de Documentos </p>
					<% }%>
					
					<p class=MsoNormal><o:p>&nbsp;</o:p></p>
					<p class=MsoNormal style='text-indent:37.4pt'>Por favor, seleccione archivo:</p>
					<p class=MsoNormal><o:p>&nbsp;</o:p></p>
					<p class=MsoFile style='text-indent:35.4pt'><input type="file" name="file"/></p>
					<p class=MsoNormal><o:p>&nbsp;</o:p></p>
					<p class=MsoFile style='text-indent:35.4pt'><input type="submit" value="aceptar"/></p>
				</form>
			</div>
			</td>
		</tr>
		</table>
    </body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<HTML>
	<HEAD>
		<TITLE>Envio de fichero</TITLE>
		<!--<META http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
		
		<% String filepath = (String)request.getAttribute("filepath"); %>
		
	    <SCRIPT language="JavaScript" type="text/javascript">
		    <!--
		    if (document.images)
		    {
		      preload_image = new Image(372,252); 
		      preload_image.src="../../librerias/gifs/popUpCaja.gif"; 
		    }
		    //-->
	    </SCRIPT>
		
		<style>
			<!--
			 /* Style Definitions */
			 body {   
			 	/*background-image : url(../../librerias/gifs/popUpCaja.gif);*/
 				/*background-color : white;*/
 				background-repeat : no-repeat
 			 }
			 p.MsoNormal, li.MsoNormal, div.MsoNormal {
			 	mso-style-parent:"";
				margin:0cm;
				margin-bottom:.0001pt;
				mso-pagination:widow-orphan;
				font-size:10.0pt;
				font-family:"Verdana";
				color:#628099;
				mso-fareast-font-family:"Times New Roman";
			}
			p.MsoFile {
			 	mso-style-parent:"";
				margin:0cm;
				text-align:center;
				margin-bottom:.0001pt;
				mso-pagination:widow-orphan;
				font-size:10.0pt;
				font-family:"Verdana";
				color:#628099;
				mso-fareast-font-family:"Times New Roman";
			}
			p.MsoNaranja, li.MsoNaranja, div.MsoNaranja {
			 	mso-style-parent:"";
				margin:0cm;
				margin-bottom:.0001pt;
				mso-pagination:widow-orphan;
				font-size:11.0pt;
				font-family:"Verdana";
				font-weight:bold;
				color:#DC822F;
				mso-fareast-font-family:"Times New Roman";
			}
			@page Section1 {
				size:595.3pt 841.9pt;
				margin:70.85pt 3.0cm 70.85pt 3.0cm;
				mso-header-margin:35.4pt;
				mso-footer-margin:35.4pt;
				mso-paper-source:0;
			}
			div.Section1
				{page:Section1;}
		-->	
		</style>
		
	</HEAD>
	
	<body bgcolor="white" background="../../librerias/gifs/popUpCaja.gif">
		
		<div class=Section1>
			<p class=MsoNormal><o:p>&nbsp;</o:p></p>
			<p class=MsoNormal><o:p>&nbsp;</o:p></p>
			<p class=MsoNaranja style='text-indent:32pt'>Envio de fichero</p>
			<p class=MsoNormal><o:p>&nbsp;</o:p></p>
			<p class=MsoNormal style='text-indent:35.4pt'>Transferencia de archivo realizada</p>
			<p class=MsoNormal style='text-indent:35.4pt'>correctamente.</p>
			<!--<p class=MsoNormal style='text-indent:35.4pt'>Filepath: <%=filepath%></p>-->
			<p class=MsoNormal><o:p>&nbsp;</o:p></p>
			<p class=MsoNormal style='text-indent:35.4pt'>Pulse Aceptar para continuar.</p>
			<p class=MsoNormal><o:p>&nbsp;</o:p></p>
			<p class=MsoNormal><o:p>&nbsp;</o:p></p>
			<p class=MsoFile style='text-indent:35.4pt'>
				<input type="submit" name="aceptar" value="aceptar" 
			 	 onclick="javascript:window.opener.document.getElementById('lzapp').SetVariable('canvas.filename','<%= filepath %>');window.close()"/>
			</p>
		</div>

	</BODY>
</HTML>
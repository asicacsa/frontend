var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	if (log.isDebugEnabled()) {
		log.debug("entrada a obtenerImportesParcialesPorReserva.action");
	}
	
	var methodpost = 'obtenerImportesParcialesPorReserva';

	// si xml no se envia, llama al metodo sin parametros	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		xml = '<servicio />';
	} else {
		var xmlSinHijos;
		
		xmlSinHijos = comun.borraElementosSinHijos(xml);

		xml = <servicio>
				<parametro>
					{xmlSinHijos}
				</parametro>
			  </servicio>
	}
	
	
	var result = null;
	
	try {
		result = services.process(methodpost+'SOAWrapper',xml);
		
		if (log.isDebugEnabled()) {
			log.debug("obtenerImportesParcialesPorReserva.action, respuesta servicio: " + result);
		}
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost);
			log.error("LOG(ERROR) : con esta llamada: " + xml);
			log.error('LOG(ERROR) : con este tipo de error : ' + ex);
			log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
		}
		
		throw ex.javaException;
	}
	
	if (result == null) {
		result = <ArrayList />;
	} else {
		result = new XML(result);
		result = postProceso(result);
	}
	
	
	if (log.isDebugEnabled()) {
		log.debug("obtenerImportesParcialesPorReserva.action, resultado final: " + result.toXMLString());
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
}


function postProceso(xmlparam) {
	// si el importe no tiene idventa, lo sacamos de su modificacion.
	for each (importe in xmlparam.Importeparcial){
		if (importe.venta.idventa.toXMLString()==""){
			var idventa = importe.modificacionimporteparcials.Modificacionimporteparcial[0].modificacion.venta.idventa.text();
			importe.venta.idventa = <idventa>{idventa}</idventa>;
		}
	}

	return xmlparam;	
}

var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

var OPERACION_CAJA_ABRIR_CAJA = 1;
var OPERACION_CAJA_APORTACION_PARCIAL = 2;
var OPERACION_CAJA_RETIRADA_PARCIAL = 3;
var OPERACION_CAJA_CERRAR_CAJA = 4;
var OPERACION_CAJA_VISTO_BUENO = 5;
var OPERACION_CAJA_VENTA = 6;
var OPERACION_CAJA_MODIFICACION_VENTA = 7;

function handle(request, response) {
	
	var methodpost = request.getParameter('servicio');
	
	//log.info("Entrada a obtenerCajaPorId");
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		var envio = <servicio/>;
	} else {
		xml = new XML(xml);
		
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>	
	}
	
	var respuesta;
	
	try{
		respuesta = services.process(methodpost+'SOAWrapper',envio.toXMLString());
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	//var respuesta = new XML(service.process(methodpost,envio));

	if (respuesta!=null) respuesta= respuesta.trim();
	
	respuesta = new XML(respuesta);	
	
	if (typeof(respuesta)!='null' && typeof(respuesta)!='undefined' && respuesta!=''){
		this.filtrarOperaciones(respuesta);
	}
	
	if (log.isInfoEnabled()) {
		log.info("Resultado de llamar a obtenerCajaPorId: " + respuesta.toXMLString().substr(0,500) + "[...]");
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
}

/**
 * En el listado de operaciones de caja sólo mostraremos las de Apertura,
 * Cierre, Retirada y Aportación.
 */
function filtrarOperaciones(xml){	
	
	for (var i=0; i<xml.operacioncajas.*.length(); i++){
		var tipooperacioncaja = xml.operacioncajas.*[i].tipooperacioncaja.idtipooperacioncaja.text();
		
		//if (log.isInfoEnabled()) {
		//	log.info("filtrado de operaciones, tipooperacioncaja: " + tipooperacioncaja);
		//}
			
		if (tipooperacioncaja != OPERACION_CAJA_ABRIR_CAJA && 
			tipooperacioncaja != OPERACION_CAJA_CERRAR_CAJA && 
			tipooperacioncaja != OPERACION_CAJA_RETIRADA_PARCIAL && 
			tipooperacioncaja != OPERACION_CAJA_APORTACION_PARCIAL && 
			tipooperacioncaja != OPERACION_CAJA_VISTO_BUENO){
			
			delete xml.operacioncajas.*[i];
			i--; // porque modificamos la colección mientras la recorremos
			
		}
	}
}
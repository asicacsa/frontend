importPackage(Packages.org.springframework.web.servlet);

function handle(request, response) {
	var Caja_Ini = request.getParameter('Caja_Ini');
	var Caja_Fin = request.getParameter('Caja_Fin');
	var nombreinforme = request.getParameter('nombreinforme');
	
	var forward =  'jasper.post?report='+nombreinforme+'&format=pdf';
	
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : parametro Caja_Ini:" + request.getParameter('Caja_Ini'));	
	}
	
	forward = forward + '&Caja_Ini=' + Caja_Ini + '&Caja_Fin=' + Caja_Fin;
	
	response.sendRedirect(forward);
	return false;
}

//var service = context.beans.getBean('httpServiceSOA');
// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {
	var operacionCaja = <Operacioncaja>
							<importe></importe>
						</Operacioncaja>
	//Estos son los datos necesarios para abrir una caja,
	// lo demas lo utilizara directamente de la sessionUsuario.
	var methodpost = request.getParameter('servicio');
	if (log.isInfoEnabled()) {
		log.info("Nos disponemos a abrir una Caja.");
	}
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	var xml = request.getParameter('xml');
	var caja = request.getParameter('caja');
	var envio;
	if(xml == null ) {
		if (log.isInfoEnabled()) {
			log.info("El importe no puede ser nulo.");
		}
		throw new java.lang.Exception("El importe no puede ser nulo.")
	} else {
		if (log.isInfoEnabled()) {
			log.info("LOG(INFO) : params before the transformation : "+this.getClass().getName()+" "+operacionCaja.toXMLString());
		}
		operacionCaja.importe=<importe>{xml}</importe>;
		if (caja != null) {
			var aux_caja = new XML(caja);
			if (methodpost == 'rectificarOperacionCaja') {
				operacionCaja.idoperacioncaja=aux_caja.idoperacioncaja;	
			} else {
				operacionCaja.caja.idcaja=aux_caja.idcaja;
			}
			if (log.isInfoEnabled()) {
				log.info("LOG(INFO) : params after the transformation : "+this.getClass().getName()+" "+operacionCaja.toXMLString());
			}
		}
		if (log.isInfoEnabled()) {
			log.info("LOG(INFO) : params after the transformation : "+this.getClass().getName()+" "+operacionCaja.toXMLString());
		}
		envio = <servicio>
					<parametro>
						{operacionCaja}
					</parametro>
				</servicio>
	}
	var respuesta;
	try{
		respuesta = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	//var respuesta = new XML(service.process(methodpost,envio));
	if (respuesta!=null) {
		respuesta = new XML(respuesta);
		if (log.isInfoEnabled()) {
			log.info("LOG(INFO): valor de la respuesta antes de ser postprocesada: " + respuesta);
		}
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
}
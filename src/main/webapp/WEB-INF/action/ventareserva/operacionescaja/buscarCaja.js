//var service = context.beans.getBean('httpServiceSOA');

// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = request.getParameter('servicio');
	
	//log.info("Entrada a buscarCaja");
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		var envio = '<servicio><parametro><dtoBuscarCaja/></parametro></servicio>'
	} else {
		xml = new XML(xml);
		if (log.isDebugEnabled()) {
			log.debug("LOG(debug) : params before the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		}
		xml = this.preProcessXML(xml);
		if (log.isDebugEnabled()) {
			log.debug("LOG(debug) : params after the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		}
		
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>	
	}
	var respuesta;
	
	try{
		respuesta = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	//var respuesta = new XML(service.process(methodpost,envio));
	
	if (respuesta!=null) {
		respuesta = new XML(respuesta);
		if (log.isDebugEnabled()) {
			log.debug("LOG(INFO): valor de la respuesta antes de ser postprocesada: " + respuesta.substr(0,50));
		}
		respuesta = this.postProcessXML(respuesta);	
		if (log.isInfoEnabled()) {
			log.info("Resultado de llamar a buscarCaja: "+respuesta.toXMLString().substr(0,50));
		}

	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
	
}


function preProcessXML(param) {
	param = comun.borraElementosSinHijos(param.toXMLString());
	param = comun.deleteSelectedTag(param.toXMLString());
	param = new XML(param);	
	
	return param;
}


/**
 * Ajustamos el XML a la estructura y campos de la vista.
 */
function postProcessXML(xmlparam) {
	var OPERACION_APERTURA = 1;
	var OPERACION_CIERRE = 4;
	var OPERACION_VISTOBUENO = 5;
	
	if (xmlparam != null){
		xmlparam = new XML(xmlparam);
		
		for each (j in xmlparam.Caja){
		
			var caja = j;
			
			// Averiguamos el nombre completo del operador
			//var nombrecompleto = caja.usuarioOperador.nombre.text() + " " + 
			//					 caja.usuarioOperador.apellidos.text();
			//log.debug("LOG(debug) : Nombrecompleto: " + nombrecompleto);
			//caja.usuarioOperador.nombrecompleto = <nombrecompleto>{nombrecompleto}</nombrecompleto>;
	
			
			// Averiguamos las fechas de apertura, cierre, visto bueno y lista de taquillas implicadas
			var fechaapertura = "";
			var fechacierre = "";
			var vistobueno = false;
			var taquillas = new Array();
			
			for each (i in caja.operacioncajas.Operacioncaja){
			
				//log.debug("LOG(debug): idtipooperacioncaja: " + i.tipooperacioncaja.idtipooperacioncaja.text());
				if (i.tipooperacioncaja.idtipooperacioncaja.text() == OPERACION_APERTURA){
					fechaapertura = i.fechayhoraoperacioncaja.text();
				} else if (i.tipooperacioncaja.idtipooperacioncaja.text() == OPERACION_CIERRE){
					fechacierre = i.fechayhoraoperacioncaja.text();
				} else if (i.tipooperacioncaja.idtipooperacioncaja.text() == OPERACION_VISTOBUENO){
					vistobueno = true;
				}
				
				// Creamos array de taquillas implicadas
				var encontrado = false;
				//log.debug("taquillas.length: " + taquillas.length);
				//log.debug("encontrado: " + encontrado);
				for (var aux = 0; (aux<taquillas.length) && !encontrado; aux++){
					//log.debug("taquillas[aux]: " + taquillas[aux]);
					if (taquillas[aux] == i.taquilla.idtaquilla.text()){
						encontrado = true;
					} 
				}
				if (encontrado == false){
					taquillas[taquillas.length] = i.taquilla.idtaquilla.text();
				}
				
			}
			
			caja.fechaapertura = <fechaapertura>{fechaapertura}</fechaapertura>;
			caja.fechacierre = <fechacierre>{fechacierre}</fechacierre>;
			caja.vistobueno = <vistobueno>{vistobueno}</vistobueno>;
			
			caja.taquillas = <taquillas></taquillas>;
			for (var aux = 0; aux < taquillas.length; aux++){
				//xmlparam.taquillas.taquilla[0] += <taquilla>{taquillas[aux]}</taquilla>;
				caja.taquillas.* += <taquilla><idtaquilla>{taquillas[aux]}</idtaquilla></taquilla>;
			}
			
			//log.debug("LOG(debug): Array taquillas: " + taquillas.toString());
			
			// Fuera elementos innecesarios
			//delete caja.usuarioOperador.nombre;
			//delete caja.usuarioOperador.apellidos;
			delete caja.operacioncajas;
		
		}	
								
	}
	
	//log.warn("LOG(WARN) : getEdicionAltaRecintos. El dato numerada llega con valor distinto a 0 ? 1.")
	return xmlparam;
}
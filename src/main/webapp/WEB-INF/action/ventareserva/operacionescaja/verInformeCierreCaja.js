importPackage(Packages.org.springframework.web.servlet);

function handle(request, response) {
	var idcaja = request.getParameter('idcaja');
	var nombreinforme = request.getParameter('nombreinforme');
	var forward =  'jasper.post?report='+nombreinforme+'&format=pdf';
	
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : parametro idcaja:" + request.getParameter('idcaja'));	
	}
	
	forward = forward + '&CajasDeUsuariosList_2=' + idcaja;
	
	response.sendRedirect(forward);
	return false;
}

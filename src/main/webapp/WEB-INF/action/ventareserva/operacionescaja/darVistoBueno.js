//var service = context.beans.getBean('httpServiceSOA');

// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = request.getParameter('servicio');
	
	//log.info("Entrada a darVistoBueno");
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		var envio = <servicio/>;
	} else {
		xml = new XML(xml);
		
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>	
	}
	var respuesta;
	
	try{
		respuesta = services.process(methodpost+'SOAWrapper',envio.toXMLString());
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	//var respuesta = new XML(service.process(methodpost,envio));
	
	if (log.isInfoEnabled()) {
		log.info("Resultado de llamar a darVistoBueno: "+respuesta);
	}
	response.setContentType('text/xml');
	response.writer.println(respuesta);
}
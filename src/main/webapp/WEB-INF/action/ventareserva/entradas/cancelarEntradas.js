var services = context.beans.getBean('httpServiceSOA');
var methodpost = "cancelarEntradas";
function handle(request, response) {

	if (log.isDebugEnabled()) {
		log.debug("entrada a cancelarEntradas.action");
	}
		
	// si xml no se envia, llama al metodo sin parametros   
	var entradas = request.getParameter('entradas');
	var modificacion = request.getParameter('modificacion');
	var xml;
	
	if (log.isDebugEnabled()) {
		log.debug("cancelarEntradas.action, method: " + methodpost);
		log.debug("cancelarEntradas.action, entradas: " + entradas);
		log.debug("cancelarEntradas.action, modificacion: "+ modificacion);
	}
		
	var xmlSinHijosEntradas;
	var xmlSinHijosModificacion;
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('transformerXML.js');
		
	xmlSinHijosEntradas = comun.borraElementosSinHijos(entradas);
	xmlSinHijosModificacion = comun.borraElementosSinHijos(modificacion);
	xmlSinHijosEntradas = comun.sustituyeLiteralesBooleanosEnVenta(xmlSinHijosEntradas);
	xmlSinHijosModificacion = comun.sustituyeLiteralesBooleanosEnVenta(xmlSinHijosModificacion);
	
	/* Sustitución de trues y falses en entradasimpresas y reciboimpreso
	if (xmlSinHijosModificacion.venta.entradasimpresas.text() == 'true') {
		xmlSinHijosModificacion.venta.entradasimpresas = <entradasimpresas>1</entradasimpresas>;
	} else if (xmlSinHijosModificacion.venta.entradasimpresas.text() == 'false'){
		xmlSinHijosModificacion.venta.entradasimpresas = <entradasimpresas>0</entradasimpresas>;
	}
	
	if (xmlSinHijosModificacion.venta.reciboimpreso.text() == 'true') {
		xmlSinHijosModificacion.venta.reciboimpreso = <reciboimpreso>1</reciboimpreso>;
	} else if (xmlSinHijosModificacion.venta.reciboimpreso.text() == 'false'){
		xmlSinHijosModificacion.venta.reciboimpreso = <reciboimpreso>0</reciboimpreso>;
	}*/
	
	//xmlSinHijos = this.preProcessXML(xmlSinHijos,numeroUsos);
		
	xml = <servicio>
			<parametro>
			   	{xmlSinHijosModificacion}
				{xmlSinHijosEntradas}
			</parametro>
		  </servicio>;
	
	if (log.isDebugEnabled()) {
		log.debug("cancelarEntradas.action, service parameters: " + xml);
	}
	
	var result = null;
	try {
		result = services.process(methodpost+'SOAWrapper',xml);
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost);
			log.error("LOG(ERROR) : con esta llamada: " + xml);
			log.error('LOG(ERROR) : con este tipo de error : ' + ex);
			log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
		}		
		throw ex.javaException;
	}
	
	if (result == null) {
		result = "<ArrayList />";
	}else if(log.isDebugEnabled()){
		log.debug("cancelarEntradas.action, service response: " + result.substr(0,300) + "[...]");
	}
	
	if (log.isDebugEnabled()) {
		log.debug("cancelarEntradas.action accesing " + methodpost);
		log.debug("cancelarEntradas.action result: " + result.substr(0,300) + "[...]");
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');   
	response.writer.println(result);
}

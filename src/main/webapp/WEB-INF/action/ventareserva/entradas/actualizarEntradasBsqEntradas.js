var services = context.beans.getBean('httpServiceSOA');
var methodpost = "actualizarUsosEntradas";
function handle(request, response) {

	if (log.isInfoEnabled()) {
		log.info("entrada a actualizarEntradas.action");
	}
		
	// si xml no se envia, llama al metodo sin parametros   
	var xml = request.getParameter('xml');
	var numeroUsos = request.getParameter('numeroUsos');
		
	
	if (log.isInfoEnabled()) {
		log.info("actualizarEntradas.action, method: " + methodpost);
		log.info("actualizarEntradas.action, xml: " + xml);
		log.info("actualizarEntradas.action, numUsos: "+ numeroUsos);
	}
	
	if(xml == null ) {
		xml = '<servicio/>';
	} else {
		var xmlSinHijos;
		var sbf = context.beans.getBean('scriptBeanFactory');
		var comun = sbf.getBean('transformerXML.js');
		
		xmlSinHijos = comun.borraElementosSinHijos("<ArrayList>"+xml+"</ArrayList>");
		
		xmlSinHijos = this.preProcessXML(xmlSinHijos,numeroUsos);
		
		if (xmlSinHijos.name() == "parametro") {
			xml = <servicio>
					{xmlSinHijos}
				  </servicio>;
		} else {
			xml = <servicio>
					<parametro>
						{xmlSinHijos}
					</parametro>
				  </servicio>;
		}
		
	}
	
	if (log.isInfoEnabled()) {
		log.info("actualizarEntradas.action, service parameters: " + xml);
	}
	
	var result = null;
	try {
		result = services.process(methodpost+'SOAWrapper',xml);
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost);
			log.error("LOG(ERROR) : con esta llamada: " + xml);
			log.error('LOG(ERROR) : con este tipo de error : ' + ex);
			log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
		}		
		throw ex.javaException;
	}
	
	if (result == null) {
		result = "<ArrayList />";
	}else if(log.isInfoEnabled()){
		log.info("actualizarEntradas.action, service response: " + result.substr(0,300) + "[...]");
	}
	
	if (log.isDebugEnabled()) {
		log.debug("actualizarEntradas.action accesing " + methodpost);
		log.debug("actualizarEntradas.action result: " + result.substr(0,300) + "[...]");
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	
	response.writer.println(result);
}

function preProcessXML(xmlParam,numUsos) {
	var result = <ArrayList/>;
	for each (Entrada in xmlParam.Entrada) {
		result.appendChild(this.createEntrada(Entrada,numUsos));
	}
	return result;
}

function createEntrada(xmlParam,numUsos) {
	if (log.isDebugEnabled()) {
		log.debug("El numero de usos : "+numUsos);
	}
	var result = <Entrada/>;
	result.appendChild(<identrada>{xmlParam.identrada.text()}</identrada>);
	result.entradatornoses=<entradatornoses/>;
	for each (Entradatornos in xmlParam.entradatornoses.Entradatornos) {
		result.entradatornoses.appendChild(<Entradatornos><usos>{numUsos}</usos><identradasvalidas>{Entradatornos.identradasvalidas.text()}</identradasvalidas></Entradatornos>);
	}
	return result;
}
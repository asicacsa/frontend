importPackage(Packages.java.lang);
importPackage(Packages.java.text);
var services = context.beans.getBean('httpServiceSOA');
var methodpost = "buscarEntrada";
function handle(request, response) {
	
	if (log.isDebugEnabled()){
		log.debug("Entrada a obtenerEntradas.action.");
	}
		
	// si xml no se envia, llama al metodo sin parametros	
	var xml = request.getParameter('xml');
	
	if (log.isDebugEnabled()){
		log.debug("Los datos de entrada : " + xml);
	}
	
	if(xml == null ) {
		xml = '<servicio />';
	} else {
		var xmlSinHijos;
		var sbf = context.beans.getBean('scriptBeanFactory');
		var comun = sbf.getBean('transformerXML.js');
		
		xmlSinHijos = comun.borraElementosSinHijos(xml);

			
		if (xmlSinHijos.ventas.text()==2){
			methodpost = "buscarEntradaBono";
		}else{
			methodpost = "buscarEntrada";
		}
		 	
		//logica de busqueda de entradas.
		if (xmlSinHijos.ventas.text()!=0) {			
			delete xmlSinHijos.reservagrupo;
		} 	
		delete xmlSinHijos.ventas;
		//Eliminamos datos no necesarios para la busqueda
		delete xmlSinHijos.nombreCliente;
		delete xmlSinHijos.login;
		delete xmlSinHijos.financiada;
		
		if (xmlSinHijos.name() == "parametro") {
			xml = <servicio>
					{xmlSinHijos}
				  </servicio>
		} else {
			xml = <servicio>
					<parametro>
						{xmlSinHijos}
					</parametro>
				  </servicio>
		}
		
	}
	
	if (log.isDebugEnabled()){
		log.debug("obtenerEntradas, envio: " + xml);
	}
	
	var result = null;
	
	try {
		result = services.process(methodpost+'SOAWrapper',xml).trim();
		
		if (log.isDebugEnabled()){
			log.debug("obtenerEntradas, respuesta del servicio: " + result);
		}
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost);
			log.error("LOG(ERROR) : con esta llamada: " + xml);
			log.error('LOG(ERROR) : con este tipo de error : ' + ex);
			log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
		}
		
		throw ex.javaException;
	}
	
	if (result == null) {
		result = "<ArrayList />";
	}
	else{
		result = new XML(result);
		result = this.postProcessXML(result);		
		result = result.toXMLString();
		
	}
	
	if (log.isDebugEnabled()){
		log.debug("obtenerEntradas, resultado final: " + result);
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result);
}

function agruparImportesParcialesPorFormaPago(xmlParam) {	
	for each (venta in xmlParam.Entrada.lineadetalle.venta) {
		var importes = new Array();
		for each (importeparcial in venta.importeparcials.importeparcial) {
			
			var encontrado = importes.length;
			
			for (var i =0; i<importes.length;i++) {
				if (importes[i].formapago.idformapago.text()==importeparcial.formapago.idformapago.text()) {
					encontrado=i;
					break;
				}
			}
			if (encontrado!=importes.length) {
				importes[encontrado].importe=<importe>{Number(importes[encontrado].importe.text())+Number(importeparcial.importe.text())}</importe>;
			} else {
				importes[encontrado]=importeparcial;
			}
		}
		for each (Modificacionimporteparcial in venta.modificacions.Modificacion.modificacionimporteparcials.Modificacionimporteparcial) {			
			var encontrado = importes.length;
			
			for (var i =0; i<importes.length;i++) {
				if (importes[i].formapago.idformapago.text()==Modificacionimporteparcial.importeparcial.formapago.idformapago.text()) {
					encontrado=i;
					break;
				}
			}
			if (encontrado!=importes.length) {
				importes[encontrado].importe=<importe>{Number(importes[encontrado].importe.text())+Number(Modificacionimporteparcial.importeparcial.importe.text())}</importe>;
			} else {
				importes[encontrado]=Modificacionimporteparcial.importeparcial;
			}
		}
		var importesXML = <importeparcials></importeparcials>;
		for (var i = 0; i < importes.length; i++) {			
			importesXML.appendChild(this.formateaFormaDePago(importes[i]));
		}
		venta.importeparcials=importesXML;
		delete venta.modificacions;
	}	
	return xmlParam;
}

function unionImporteParcials(xmlParam) {
	for each (venta in xmlParam.Entrada.lineadetalle.venta) {
		var importes = <importeparcials></importeparcials>;
		
		for each (importeparcial in venta.importeparcials.importeparcial) {			
			importes.appendChild(this.formateaFormaDePago(importeparcial));		
		}
		for each (Modificacionimporteparcial in venta.modificacions.Modificacion.modificacionimporteparcials.Modificacionimporteparcial) {			
			importes.appendChild(this.formateaFormaDePago(Modificacionimporteparcial.importeparcial));
		}
		venta.importeparcials=importes;
		delete venta.modificacions;
	}
	return xmlParam;
}

function calcularImpresiones(xmlParam) {
	var result = xmlParam;
	
	for each (Entrada in result.Entrada) {
		var i = 0;
		for each (idimpresionentradas in Entrada.impresionentradases.Impresionentradas.idimpresionentradas) {
			i++;
		}
		Entrada.numImpresiones = <numImpresiones>{i}</numImpresiones>;
		delete Entrada.impresionentradases;
	}	
	return xmlParam;
}

function formateaFormaDePago(xmlImporteParcial) {
	var result= xmlImporteParcial;
	result=<importeparcial><formapago>{result.formapago.nombre.text()+' '+result.importe.text()+' &#x20AC;'}</formapago></importeparcial>;		
	return result;
}

function formateaContenido(xmlParam) {
	for each (zonasesion in xmlParam.Entrada.lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion) {
		var nombre = zonasesion.sesion.nombre.text();
		var fecha = zonasesion.sesion.fechayhorainiciosesion.text();
		delete zonasesion.sesion;
		zonasesion.appendChild(this.formateaSesion(nombre, fecha));
	}
	return xmlParam;
}

function formateaSesion(nombre, fecha) {
	var result;
	var fecha_aux = new String(fecha);
	result = <sesion>{fecha_aux.substring(0,fecha_aux.lastIndexOf(':'))+" "+nombre}</sesion>;
	return result;
}

function calcularImporteEntrada(xmlParam) {
	
	for each (Entrada in xmlParam.Entrada) {
		var importeTotal = Entrada.lineadetalle.importe.text();
		var cantidad = Entrada.lineadetalle.cantidad.text();
		//log.info("importeTotal : "+importeTotal);
		//log.info("cantidad : "+cantidad);
		var importeEntrada = Math.round(((new Float(importeTotal)/new Float(cantidad))*100))/new Float("100.0f");
		Entrada.importe=<importe>{(new Float(importeEntrada)).toString()}</importe>;
		delete Entrada.lineadetalle.cantidad;
		delete Entrada.lineadetalle.importe;
	}
	return xmlParam;
}

function postProcessXML(xmlParam){
	var result = xmlParam;
	//Ahora existe un campo calculado que nos llega directamente de services.
	//result = this.calcularImpresiones(result);	
 	result = this.agruparImportesParcialesPorFormaPago(result);
	result = this.calcularImporteEntrada(result);
	result = this.formateaContenido(result);
	return result;
}
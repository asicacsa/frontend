var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = 'obtenerDatosParaImpresionPasesNew';
	if ( log.isDebugEnabled() ) {
		log.debug("Entrada a obtenerDatosParaImpresionPases.js");
	}
	
	var xml = request.getParameter('xml');
	//GGL No puedo utilizarlo ya que uso este js cuando cierro el popupde imprimirPases.lzx para dejar el dataset vacio
	if(xml == null || xml == "") { 
	//	throw new java.lang.Exception("Debe seleccionar al menos un pase para imprimir.")
		var envio = <servicio><parametro><ParamSolicitudDatosImpresion/></parametro></servicio>
	} else {	
		xml = new XML(xml);
		/*if (xml.children().length()<=1){
			throw new java.lang.Exception("Debe seleccionar al menos 1 pase para imprimir.")
		}*/
		var envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>
	
	} 
	if (log.isInfoEnabled()) {
		log.info(" #### El envio de obtenerDatosParaImpresionPases.js:" + envio.toString());
	}
	var respuesta = null; 
	try{
		respuesta = services.process( methodpost+'SOAWrapper', envio );
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : ' + this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper');
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			log.error('LOG(ERROR) : con esta descripcion : '+ ex.javaException.getMessage());
			log.error('LOG(ERROR) : con esta llamada : '+ envio);
		}
		throw ex.javaException;
	}
	if (respuesta!=null) { 
		if ( log.isInfoEnabled() ) {
			log.info( 'El servicio obtenerDatosParaImpresionPases.js devuelve ' + respuesta );
		}
	} else {
		respuesta = '</ArrayList>';
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta);
}

function preProcessXML(param) {
	param = comun.borraElementosSinHijos(param.toXMLString());
	return new XML(param);
}

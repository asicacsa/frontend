var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = 'obtenerDatosClienteClubNew';
	if ( log.isDebugEnabled() ) {
		log.debug("Entrada a obtenerDatosClienteClubNew");
	}
	//GGL 10-12-2020 Aquí se manda un xml, no los parámetros en el request
	//var idabono = request.getParameter('idabono');
	//var esabono = request.getParameter('esabono');
	
	//if (log.isInfoEnabled()) {
	//	log.info(" #### El idabono:" + idabono.toString());
	//}
	
	//if(idabono == null || idabono == "") { 
	//	if ( log.isInfoEnabled() ) {
	//		log.info( 'El servicio obtenerDatosClienteClub no recibe los parametros adecuados');
	//	}
	//} else {
	//			var envio = '<servicio><parametro><IdentificadorPasesDto><idpase>'+idabono+'</idpase><esAbono>'+esabono+'</esAbono></IdentificadorPasesDto></parametro></servicio>';
	//} 
	
	var xml = request.getParameter('xml');
	if(xml == null ) {
		if ( log.isInfoEnabled() ) {
			log.info( 'El servicio obtenerDatosClienteClubNew no recibe los parametros adecuados');
		}
	} else {
		xml = new XML(xml);
		if ( log.isInfoEnabled() ) {
			log.info("LOG(INFO) : obtenerDatosClienteClubNew params before the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		}
		xml = this.preProcessXML(xml);
		
		//Comprobamos que tengamos campos de busuqueda
		if (xml.children().length()<=1){
			throw new java.lang.Exception("El servicio obtenerDatosClienteClub no recibe los parametros adecuados")
		}
		if ( log.isInfoEnabled() ) {
			log.info("LOG(INFO) : obtenerDatosClienteClub params after the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		}
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>
	}
	var respuesta = null; 
	try{
		respuesta = services.process( methodpost+'SOAWrapper', envio );
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : ' + this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper');
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			log.error('LOG(ERROR) : con esta descripcion : '+ ex.javaException.getMessage());
			log.error('LOG(ERROR) : con esta llamada : '+ envio);
		}
		throw ex.javaException;
	}
	if (respuesta!=null) { 
		//if ( log.isInfoEnabled() ) {
			log.error( 'El servicio obtenerDatosClienteClubNew devuelve ' + respuesta );
		//}
	} else {
		respuesta = '<DatosClienteClubNew/>';
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta);
}

function preProcessXML(param) {
	param = comun.borraElementosSinHijos(param.toXMLString());
	return new XML(param);
}

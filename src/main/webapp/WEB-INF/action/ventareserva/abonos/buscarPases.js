var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	//API 8 Septiembre 2011 Pasamos a llamar a obtenerPases necesariamente.
	var methodpost = 'obtenerPasesNew';
	//var methodpost = request.getParameter('servicio');
	if ( log.isDebugEnabled() ) {
		log.debug("Entrada a buscarPases");
	}
	
	//API 8 Septiembre 2011 llamamos obtenerPases
	// si servicio no se envia, intenta obtenerlo del nombre del action
	//if(methodpost == null) {
	//	methodpost = request.getAttribute('__request_service');
	//}
	if ( log.isInfoEnabled() ) {
		log.info('El metodo a llamar es ' + methodpost);
	}
	var xml = request.getParameter('xml');
	if(xml == null ) {
		var envio = '<servicio><parametro><buscarpaseparam><todo>true</todo></buscarpaseparam></parametro></servicio>'
	} else {
		xml = new XML(xml);
		if ( log.isInfoEnabled() ) {
			log.info("LOG(INFO) : buscarPases params before the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		}
		xml = this.preProcessXML(xml);
		if (log.isInfoEnabled()) {
			log.info(" #### Numero de Parametros busqueda Pases:" + xml.children().length());
		}
		//Comprobamos que tengamos campos de busqueda
		if (xml.children().length()<=1){
			throw new java.lang.Exception("Debe introducir algun criterio de busqueda.")
		}
		if ( log.isInfoEnabled() ) {
			log.info("LOG(INFO) : buscarPases params after the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		}
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>
	}
	var respuesta = null; 
	try{
		respuesta = services.process( methodpost+'SOAWrapper', envio );
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : ' + this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper');
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			log.error('LOG(ERROR) : con esta descripcion : '+ ex.javaException.getMessage());
			log.error('LOG(ERROR) : con esta llamada : '+ envio);
		}
		throw ex.javaException;
	}
	if (respuesta!=null) { 
		if ( log.isInfoEnabled() ) {
			log.info( 'El servicio buscarPases devuelve ' + respuesta );
		}
	} else {
		respuesta = '</ArrayList>';
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta);
}

function preProcessXML(param) {
	param = comun.borraElementosSinHijos(param.toXMLString());
	return new XML(param);
}
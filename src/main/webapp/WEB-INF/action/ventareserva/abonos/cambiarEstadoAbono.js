var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = 'cambiarEstadoAbonoNew';
	if ( log.isDebugEnabled() ) {
		log.debug("Entrada a cambiarEstadoAbono");
	}
	
	var seleccion = request.getParameter('seleccion');
	var idestado = request.getParameter('idestado');
	if (log.isInfoEnabled()) {
		log.info(" #### El XML:" + seleccion);
	
	}
	
	if(seleccion != null && seleccion != "" && idestado != null && idestado != "" ) {
		var envio = '<servicio><parametro>'+seleccion+'<java.lang.String>'+idestado+'</java.lang.String></parametro></servicio>';
		
	} else {		
		var envio = '<servicio><parametro><IdentificadorPasesDto></IdentificadorPasesDto><idestado></idestado></parametro></servicio>';	
		if (log.isInfoEnabled()) {
			//log.info(" #### El XML:" + xml.toString());
			log.info(" #### El envio:" + envio);
		}
		
	} 
	var respuesta = null; 
	try{
		respuesta = services.process( methodpost+'SOAWrapper', envio );
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : ' + this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper');
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			log.error('LOG(ERROR) : con esta descripcion : '+ ex.javaException.getMessage());
			log.error('LOG(ERROR) : con esta llamada : '+ envio);
		}
		throw ex.javaException;
	}
	if (respuesta!=null) { 
		if ( log.isInfoEnabled() ) {
			log.info( 'El servicio cambiarEstadoAbono devuelve ' + respuesta );
		}
	} else {
		respuesta = '</ArrayList>';
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta);
}

function preProcessXML(param) {
	param = comun.borraElementosSinHijos(param.toXMLString());
	return new XML(param);
}
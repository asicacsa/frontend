var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = 'obtenerPasesDelGrupo';//Esta comentado en services. Este js no se utiliza!!!
	if ( log.isDebugEnabled() ) {
		log.debug("Entrada a obtenerPasesDelGrupo");
	}
	
	var idabono = request.getParameter('idabono');
	var esabono = request.getParameter('esabono');
	
	if (log.isInfoEnabled()) {
		log.info(" #### El idabono:" + idabono.toString());
		//log.info(" #### El envio:" + envio);
	}
	
	if(idabono == null || idabono == "") { 
		var envio = '<servicio><parametro><Buscarpaseparam><todo>true</todo></Buscarpaseparam></parametro></servicio>'
	} else {
		//var envio = '<servicio><parametro>'+xml+'</parametro></servicio>';
		//var envio = '<servicio><parametro><buscarpaseparam><dniabonado>'+xml+'</dniabonado></buscarpaseparam></parametro></servicio>';
		//var envio = '<servicio><parametro><Buscarpaseparam><idabono>'+idabono+'</idabono><esAbono>'+esabono+'</esAbono></Buscarpaseparam></parametro></servicio>';
		var envio = '<servicio><parametro><Buscarpaseparam><identificador><idpase>'+idabono+'</idpase><esAbono>'+esabono+'</esAbono></identificador></Buscarpaseparam></parametro></servicio>';
		
	/*} else {
		xml = new XML(xml);
		if ( log.isInfoEnabled() ) {
			log.info("LOG(INFO) : buscarPases params before the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		}*/
		//xml = this.preProcessXML(xml);
		if (log.isInfoEnabled()) {
			//log.info(" #### El XML:" + xml.toString());
			log.info(" #### El envio:" + envio);
		}
		/*
		//Comprobamos que tengamos campos de busuqueda
		if (xml.children().length()<=1){
			throw new java.lang.Exception("Debe introducir algun criterio de busqueda.")
		}
		if ( log.isInfoEnabled() ) {
			log.info("LOG(INFO) : buscarPases params after the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		}
		envio = <servicio>
					<parametro>
						{xmlenvio}
					</parametro>
				</servicio>
	*/
	} 
	var respuesta = null; 
	try{
		respuesta = services.process( methodpost+'SOAWrapper', envio );
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : ' + this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper');
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			log.error('LOG(ERROR) : con esta descripcion : '+ ex.javaException.getMessage());
			log.error('LOG(ERROR) : con esta llamada : '+ envio);
		}
		throw ex.javaException;
	}
	if (respuesta!=null) { 
		if ( log.isInfoEnabled() ) {
			log.info( 'El servicio obtenerPasesDelGrupo devuelve ' + respuesta );
		}
	} else {
		respuesta = '</ArrayList>';
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta);
}

function preProcessXML(param) {
	param = comun.borraElementosSinHijos(param.toXMLString());
	return new XML(param);
}

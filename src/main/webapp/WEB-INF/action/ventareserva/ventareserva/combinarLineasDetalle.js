var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('obtenerProductoPorIdVentaReserva.js');


function handle(request, response) {
	
	var lineasdetalle = request.getParameter('lineasdetalle');
	var nuevoProducto = request.getParameter('nuevoProducto');
	var idtipoventa = request.getParameter('idtipoventa');
	var idcliente = request.getParameter('idcliente');
	var idcanal = request.getParameter('idcanal');
	
	var result = new XML();

	//Comprobamos que tengamos los datos correctos
	if (lineasdetalle == null || lineasdetalle == "" || nuevoProducto == null || nuevoProducto == "") {
		throw new java.lang.Exception("Debe de especificar una serie de lineas de detalle y el nuevo producto seleccionado");
	}
	
	//Creamos los xml
	lineasdetalle = new XML(lineasdetalle);
	nuevoProducto = new XML(nuevoProducto);
	
	//Cogemos la primera linea de detalle que nos viene para obtener el esqueleto
	var ldVacia = new XML(lineasdetalle.Lineadetalle[0].toXMLString());
	var idlineadetalle = ldVacia.idlineadetalle.text();
	
	//Vaciamos la linea de detalle para que nos quede solo con la estructura
	delete ldVacia.perfiles;
	delete ldVacia.lineadetallezonasesions.Lineadetallezonasesion;
	delete ldVacia.producto.*;
	ldVacia.idlineadetalle = "";
	//ldVacia.importeTarifaSeleccionada = "";
	//ldVacia.perfilvisitante.idperfilvisitante = "";
	//ldVacia.descuentopromocional.iddescuentopromocional = "";
	ldVacia.importe = "";
	
	var cantidadMin = 0;
	
	//Obtenemos la menor cantidad
	for each (cantidad in lineasdetalle.Lineadetalle.cantidad) {
		if (cantidadMin == 0 || cantidad < cantidadMin) {
			cantidadMin = cantidad;
		}
	}
	
	ldVacia.cantidad = cantidadMin;
	
	//Ahora que ya tenemos el esqueleto vamos a rellenarlo
	
	//Empezamos poniendo el nuevo producto
	nuevoProducto.setName("producto");
	ldVacia.producto = nuevoProducto;
	
	//Ahora insertamos los perfiles	
	var fechaMin = null;
	var idsesion = null;
	
	//Recorremos todas las sesiones para obtener la que tiene la fecha mas cercana	
	for each(var session in lineasdetalle.Lineadetalle[0].lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion) {
		
		var fechaString = session.fecha.text();
		if (fechaString.toString() != "") {
			var array = fechaString.split("-");
			var camposFecha = array[0].split("/");
			
			var fechaActual = new java.util.Date(camposFecha[2], camposFecha[1] - 1, camposFecha[0]);
			if(session.idsesion.text().toString() != ""){
				if (fechaMin == null || fechaActual.before(fechaMin)) {
					fechaMin = fechaActual;
					idsesion = session.idsesion;
				}
				else{
					//Si las sesiones tienen la misma fecha, nos quedamos con la de menor id
					if(fechaActual.equals(fechaMin) && (idsesion == null || idsesion > session.idsesion)){ 
						idsesion = session.idsesion;
					}
				}
			}
			else{
				idsesion = null;
				break;
			}
		}
	}
	
	var perfilLD = new XML(comun.retornaPerfilVisitanteYDescuento(ldVacia.producto.idproducto.text(), idcanal, idcliente, idsesion, idtipoventa, idlineadetalle));

	if(isNaN(idtipoventa) || idtipoventa == "")
		idtipoventa = null;
	else
		idtipoventa = parseInt(idtipoventa);
		
	if(isNaN(idcliente) || idcliente == "")
		idcliente = null;
	else
		idcliente = parseInt(idcliente);
		
	if (fechaMin != null) {
		var dia = "0" + fechaMin.getDate();
		var mes = "0" + (Number(fechaMin.getMonth()) + 1);

		fechaMin = dia.substr(dia.length - 2, 2) + "/" + mes.substr(mes.length - 2, 2) + "/" + fechaMin.getYear();
	}	
	ldVacia = comun.establecerPerfilesEImportesLD(ldVacia, perfilLD, idcliente, fechaMin, idtipoventa);
		
	//Ahora insertamos las lineadetallezonasesions de cada tipo de producto que hay en el nuevo producto con
	// los datos de las lineas de detalle que nos pasan.
	for each (tpp in nuevoProducto.tipoprodproductos.Tipoprodproducto) {
		var ldzs = lineasdetalle.Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.(zonasesion.sesion.contenido.tipoproducto.idtipoproducto==tpp.tipoproducto.idtipoproducto);
		
		if (ldzs.toXMLString() == "") {
			ldzs = <Lineadetallezonasesion />;
			ldzs.zonasesion.sesion.horainicio = "";
			ldzs.zonasesion.sesion.fecha = "";
			ldzs.zonasesion.numlibres = "";
			ldzs.zonasesion.sesion.contenido.tipoproducto = tpp.tipoproducto;
		}
		
		ldVacia.lineadetallezonasesions.appendChild(ldzs);
	}
	
	result = ldVacia;
		
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
	
}
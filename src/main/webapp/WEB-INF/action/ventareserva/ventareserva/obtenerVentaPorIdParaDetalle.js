var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {
	var entrada = new Date();
	
	if ( log.isInfoEnabled() ) { 
		log.info("\nEntrando a obtenerVentaPorIdParaDetalle.js en el instante " + entrada.getTime() + " ms desde 1970.\n");
	};
	var methodpost = 'obtenerVentaPorId';

	
	// si xml no se envia, llama al metodo sin parametros	
	var selecciona = null;
	
	selecciona = request.getParameter('xml');
	if (log.isInfoEnabled()) {
		log.info("obtenerComun: Id para seleccionar " + selecciona);
	}
	
	var envio = <servicio></servicio>
	
	// En este tipo de servicio no van a haber parametros de Entrada.
	if(selecciona != null){
		selecciona = new XML(selecciona);
		selecciona.setName('java.lang.Integer');
		envio = <servicio>
					<parametro>
						{selecciona}
					</parametro>
				</servicio>
	}

	if (log.isInfoEnabled()) {
		//log.info("Metodo a llamar : "+methodpost);
		log.info("Envio : "+envio);
	}
	
	var result;
	if (log.isErrorEnabled()) {
		try {
			result = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.info('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.info('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.info('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
	} else {
		result = services.process(methodpost+'SOAWrapper',envio);
	}
	
	
	if (log.isInfoEnabled()) {
		log.info(result);
	}
	
	if (result !=  null) {
		result = new XML(result.trim());
		//result = this.postProcessXML(result, selecciona);		
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
	
	if ( log.isInfoEnabled() ) {
		log.info( "\nSaliendo de obtenerVentaPorIdParaDetalle.js tras " + ( new Date().getTime() - entrada.getTime() ) + " ms de procesamiento.\n" );
	};
	
}
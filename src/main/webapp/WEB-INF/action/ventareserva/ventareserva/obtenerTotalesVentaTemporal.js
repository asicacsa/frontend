var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');
var eliminarBasuraLD = sbf.getBean('eliminarDatosInvalidosLineaDetalle.js');

function handle(request, response) {
	if ( log.isInfoEnabled() ) log.info('Entrada a obtenerTotalesVentaTemporal.');
		
	var resultado;
	var methodpost = 'obtenerTotalesVentaTemporal';
	var envio;
		
	var lineas = request.getParameter('lineas');
	var idcliente = request.getParameter('idcliente');
	var idtipoventa = request.getParameter('idtipoventa');

	lineas = new XML(lineas);
	var lineadetalles = <lineadetalles/>;
	for each(i in lineas..Lineadetalle)
	{
			lineadetalles.appendChild(i);	
	}
	
	
	if ( log.isInfoEnabled() ) {
		log.info("idcliente: " + idcliente);
		log.info("idtipoventa: " + idtipoventa);
		log.info("lineas: " + lineas.toXMLString());
	}
	
	if ( idcliente.isNaN || idcliente == null || idcliente.length == 0 || idcliente == "null" ) {
		var xml = <dto>			
				{lineadetalles}			
			<idtipoventa>{idtipoventa}</idtipoventa>
		</dto>;
	} else {
		var xml = <dto>
				{lineadetalles}
			<idtipoventa>{idtipoventa}</idtipoventa>
			<idcliente>{idcliente}</idcliente>
		</dto>;
	}
		
	xml = this.preProcessXML(xml);
	envio = <servicio>
				<parametro>
					{xml}
				</parametro>
			</servicio>;

	if ( log.isInfoEnabled() ) log.info("Este es el envio : "+envio);
	
	//Y llamamos al servicio...
	var aux = services.process(methodpost+'SOAWrapper',envio);
	
	//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
	//En aux tenemos la respuesta.
	if(aux != null){
		resultado = new XML(aux);
		//resultado = this.postProcessXML(resultado);
	} else {
		resultado = new XML();
		if (log.isInfoEnabled()) log.info("El resultado es null.");
	}
	if (log.isInfoEnabled()) log.info("Este es el resultado de obtenerTotalesVentaTemporal: " + resultado.toXMLString());
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}


function preProcessXML(param) {
	param = new XML(param);
	
	//for each(i in param.lineadetalles.Lineadetalle) {
		//var lalinea = i.Lineadetalle;
		//if (log.isInfoEnabled()) log.info("Lineadetalle a eliminarle basura: " + lalinea.toXMLString());
		//lalinea = eliminarBasuraLD.eliminarDatosInvalidosLineaDetalleDeUnaLinea(lalinea);
	//}
	delete param..Lineadetalle.detalles;
	//GGL 27.02.2015 Cuando quitabamos el DP se quedaba el nodo descripcion y daba error, por no ser nulo y no tener iddescuentopromocional
	delete param..Lineadetalle.descuentopromocional.descripcion;
	
	delete param..Lineadetalle.producto.iva;
	
	param = eliminarBasuraLD.eliminarDatosInvalidosLineaDetalle(param);
	param = comun.borraElementosSinHijos(param.toXMLString());
	return param;
}


function postProcessXML(param) {
	//log.info('en la Function postProcess..'+param);
	param = new XML(param);
	var nd = <iddescuentopromocional/>;
	for each(i in param.Lineadetalle.descuentopromocional){
		//log.info('Hay Nodo descuento??????'+i.descendants('iddescuentopromocional').length());
		if (i.descendants('iddescuentopromocional').length() == 0){
			i.appendChild(nd);	
		}
	}
	//log.info("Lo que enviamos postProcesadoooooo : "+param);
	return param;
}
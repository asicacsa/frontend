var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var ventas = request.getParameter('ventas');
	var motivomodificacion = request.getParameter('motivomodificacion');
	var resultado;
	var methodpost = "actualizarLineadetallesVentasList";
	var envio;
	
	if (ventas != null && ventas != "") {
		ventas = new XML(ventas);
		var listamodificaciones = <list />;
		var modificacion = <Modificacion>
								<motivomodificacion>{motivomodificacion}</motivomodificacion>
								<venta>
									<idventa />
								</venta>
								<modificacionimporteparcials />
							</Modificacion>;
		var errores = "";
		
		for each(var v in ventas.Venta) {
		
			var idventa=v.idventa;
			var operacioncaja=v.operacioncaja;
			var modificacionimporteparcials= <modificacionimporteparcials />;
			var importeparcialparams = <importeparcialparams />;
			
			for each(var ip in v.importeparcials.Importeparcial) {
				
				var ipAux=ip;
				ipAux.setName("importeparcial");
				
				var ventaSinImportesParciales=v;
				ventaSinImportesParciales.setName("venta");
				delete ventaSinImportesParciales.importeparcials;
				ipAux.appendChild(ventaSinImportesParciales);
				var modificacionimporteparcial = <Modificacionimporteparcial>
													<tipomodificacion>0</tipomodificacion>
													{ipAux}
												</Modificacionimporteparcial>;
				modificacionimporteparcials.appendChild(modificacionimporteparcial);
			}
			modificacion.venta.idventa = v.idventa;
			modificacion.modificacionimporteparcials = modificacionimporteparcials;
			listamodificaciones.appendChild(modificacion);
		}
		listamodificaciones = comun.borraElementosSinHijos(listamodificaciones);
		//Aqui vamos a eliminar aquellas modificacionImporteParcial que no tenga hijos
		for each( i in listamodificaciones.Modificacion.modificacionimporteparcials.Modificacionimporteparcial ){
			if(i.importeparcial.importe.toString() == "" && i.importeparcial.formapago.toString() == ""){
				delete i.tipomodificacion;
				delete i.importeparcial.venta;
			}
		}
		var selectedNodes = listamodificaciones..Modificacionimporteparcial.(importeparcial.@isnew == false);
		for(var i = 0; i < selectedNodes.length(); i = 0){
			delete selectedNodes[i];
		}
		
		listamodificaciones = comun.borraElementosSinHijos(listamodificaciones);
		
		var j = 0;
		
		for (var i=0 ; i < listamodificaciones.Modificacion.length();){
			if (!listamodificaciones.Modificacion[i].hasOwnProperty('modificacionimporteparcials')) {
				delete listamodificaciones.Modificacion[i];
			}else{
				i = i+1;
			}
		}
		
		if(!listamodificaciones.hasOwnProperty('Modificacion')){
				throw new java.lang.Exception("No se ha realizado ninguna modificación.");
		}
		
		envio = 
			<servicio>
				<parametro>
					{listamodificaciones}
				</parametro>
			</servicio>
		
		var aux = null;
		
		//Y llamamos al servicio...
		try {
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			if (log.isErrorEnabled()) {	
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				log.error('LOG(ERROR) : con esta llamada : ' + envio);
				log.error('LOG(ERROR) : result in this Exception : '+ex);
			}
			
			throw ex.javaException;
		}
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		if(aux != null){
		//log.info('aux es distinto de nulo');
			aux = new XML(aux);
			resultado = 
					<ok>
						{aux}
					</ok>
			if (log.isInfoEnabled()) {
				log.info("Este es el resultado : "+resultado);
			}
		}
	}else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a realizarreserva es: " + xml);
		}
		throw new java.lang.Exception("Peticion incorrecta.");
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
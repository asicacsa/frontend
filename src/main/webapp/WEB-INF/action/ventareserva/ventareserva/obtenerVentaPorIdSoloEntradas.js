var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {
	var entrada = new Date();
	if (log.isInfoEnabled()){
		log.info("\nEntrada a obtenerVentaPorIdSoloEntradas.js en el instante " + + " ms desde 1970.\n");
	}
	
	var methodpost = 'obtenerVentaPorIdSoloEntradas';

	// si xml no se envia, llama al metodo sin parametros	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		xml = '<servicio />';
	} else {
		var xmlSinHijos;
		var sbf = context.beans.getBean('scriptBeanFactory');
		var comun = sbf.getBean('transformerXML.js');
		
		xmlSinHijos = comun.borraElementosSinHijos(xml);

		xml = <servicio>
				<parametro>
					{xmlSinHijos}
				</parametro>
			  </servicio>
	}
	
	
	var result = null;
	
	try {
		result = services.process(methodpost+'SOAWrapper',xml).trim();
		
		if (log.isDebugEnabled()){
			log.debug("obtenerVentaPorIdSoloEntradas.action, service result: " + result);
		}
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost);
			log.error("LOG(ERROR) : con esta llamada: " + xml);
			log.error('LOG(ERROR) : con este tipo de error : ' + ex);
			log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
		}
		if ( log.isInfoEnabled() ) { 
			log.info("\nSaliendo de obtenerVentaPorIdSoloEntradas.js tras " + ( new Date().getTime() - entrada.getTime() ) + " ms\n");
		};
		throw ex.javaException;
	}
	
	if (result == null) {
		result = "<ArrayList />";
	} else {
		result = new XML(result);
		
		var numimpresiones = 0;
		
		for each (var ld in result.lineadetalles.Lineadetalle) {
			numimpresiones = ld.entradas.Entrada.(impresionentradases.Impresionentradas.length() >= 1 && anulada == 0).length();
			
			delete ld.entradas;
			
			ld.numimpresiones = numimpresiones;
			
			// GMV (Fases I y IV)
			// Introducido por: JMARIN (16/02/2011)
			
			//ld.imprimir = Number(ld.cantidad.text()) - numimpresiones;
			if(ld.producto.imprimirEntradasXDefecto == 1)
			{
				ld.imprimir = Number(ld.cantidad.text()) - numimpresiones;
			}
			else
			{
				ld.imprimir = 0;
			}
		}		
	}
	
	if (log.isDebugEnabled()){
		log.debug("obtenerVentaPorIdSoloEntradas.action, response: " + result.toXMLString());
	}

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');	
	response.writer.println(result);

	if ( log.isInfoEnabled() ) { 
		log.info("\nSaliendo de obtenerVentaPorIdSoloEntradas.js tras " + ( new Date().getTime() - entrada.getTime() ) + " ms\n");
	};

}

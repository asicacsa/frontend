var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {
	
	var methodpost = "getXMLModel";
	
	var xml = request.getParameter('xml');
	
	var envio = <servicio />;
	var resultadoCliente = null;
	
	// Creamos el xml para obtener el modelo de la venta.
	if(xml != null){
		envio = <servicio>
					<parametro>
						<Venta />
					</parametro>
				</servicio>
	} else {
		throw new java.lang.Exception("Debe de seleccionar una reserva para poder venderla.");
	}

	var result;
	
	try {
		//Pedimos el modelo
		result = services.process(methodpost+'SOAWrapper',envio).trim();
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : ');
			log.error("LOG(ERROR) : con esta llamada: " + envio);
			log.error('LOG(ERROR) : con este tipo de error : ' + ex);
			log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
		}
		
		throw ex.javaException;
	}
	
	
	if (result !=  null) {
		result = new XML(result);
		
		xml = new XML(xml);
		var xmlResultado = null;

		var xmlEnvio = 	<servicio>
							<parametro>
								<int>{xml.idreserva.text()}</int>
							</parametro>
						</servicio>;
						
		try {
			xmlResultado = new XML(services.process("obtenerReservaPorIdSOAWrapper", xmlEnvio).trim());
		} catch (ex) {
			if (log.isErrorEnabled()) {
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : aplicarDescuentoPromocionalListaSOAWrapper : ');
				log.error("LOG(ERROR) : con esta llamada: " + xmlEnvio);
				log.error('LOG(ERROR) : con este tipo de error : ' + ex);
				log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
			}
			
			throw ex.javaException;
		}
		
		xmlResultado.setName("reserva");
		
		result.reserva = xmlResultado; 
		
		log.error(result);
		if (log.isInfoEnabled()) {
			log.info(result.toXMLString());
		}
	} else {
		throw new java.lang.Exception("No se ha podido recuperar el xml para realizar la venta de la reserva.");
	}
	
	//Devolvemos los datos
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
	
}
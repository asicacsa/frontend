var services = context.beans.getBean('httpServiceSOA');
//var servicio_alta ='realizarVentaAbonos';
//var servicio_edicion ='actualizarAbonos';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
	
	if (log.isInfoEnabled()){
		log.info('Entrada a aplicarDescuentoPromocionalLDActualizandoLineaDetalle.action.');	
	}	
		
	var resultado;
	var methodpost = 'aplicarDescuentoPromocionalLista';
	var envio;
		
	var lalineaOriginal = request.getParameter('xml');
	var idtipoventa = request.getParameter('idtipoventa');
	
	lalinea = new XML(lalineaOriginal);
	lalineaOriginal = new XML (lalineaOriginal);
	
	var eliminarBasuraLD = sbf.getBean('eliminarDatosInvalidosLineaDetalle.js');
	
	//Eliminamos los datos que son de solo lectura
	delete lalinea..sesion.nombre;
	delete lalinea.detalles;
	
	lalinea = eliminarBasuraLD.eliminarDatosInvalidosLineaDetalleDeUnaLinea(lalinea);
	
	var comun = sbf.getBean('transformerXML.js');

	lalinea = comun.borraElementosSinHijos(lalinea);

	var xml = <dto>
	 			<lineadetalles>
	 				{lalinea}
	 			</lineadetalles>
	 			<idtipoventa>{idtipoventa}</idtipoventa>
			  </dto>;
	
	envio = 
		<servicio>
			<parametro>
				{xml}
			</parametro>
		</servicio>
		
	
	//Y llamamos al servicio...
	var aux = null;
	
	if (log.isInfoEnabled()){
		log.info('aplicarDescuentoPromocionalLDActualizandoLineaDetalle, envio: ' + envio.toXMLString());	
	}	
	
	try {
		aux = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : ' + this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper');
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			log.error('LOG(ERROR) : con esta descripcion : '+ ex.javaException.getMessage());
			log.error('LOG(ERROR) : con esta llamada : '+ envio);
		}
		
		throw ex.javaException;
	}
	
	if (log.isInfoEnabled()){
		log.info('aplicarDescuentoPromocionalLDActualizandoLineaDetalle, respuesta: ' + aux);	
	}	
	
	if(aux != null){
		resultado = new XML(aux);
		lalineaOriginal.importe = resultado.Lineadetalle.importe;
		resultado = lalineaOriginal;
	} else {
		resultado = new XML();
	}
	
	if (log.isInfoEnabled()){
		log.info('aplicarDescuentoPromocionalLDActualizandoLineaDetalle, resultado final: ' + resultado.toXMLString());	
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
	
}
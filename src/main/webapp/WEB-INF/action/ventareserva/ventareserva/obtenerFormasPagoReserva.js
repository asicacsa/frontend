var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {
		
	var idreserva = request.getParameter('xml');
	
	var resultado;
	var methodpostReserva = "obtenerReservaPorId";
	var methodpostListado = "obtenerImportesParcialesPorVenta";
	var envio;
	var datosReserva = null;
	var llamada = new XML();
	
	
	if (idreserva!=null && idreserva != "") {
		idreserva = new XML(idreserva);
		
		try {
			llamada = <servicio><parametro>{idreserva}</parametro></servicio>;
			
			datosReserva = services.process(methodpostReserva + 'SOAWrapper', llamada);
			
			datosReserva = new XML(datosReserva);
		} catch (ex) {
			if (log.isErrorEnabled()) {	
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+ methodpostReserva +'SOAWrapper : '+this.getClass().getName());
				log.error('LOG(ERROR) : con esta llamada : ' + llamada);
				log.error('LOG(ERROR) : result in this Exception : '+ex);
				log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
			}
			
			throw ex.javaException;
		}
		
		var importesparciales = null;
		
		//Obtenemos los importes parciales para cada venta
		for each(var v in datosReserva.ventas.Venta) {
			try {
				llamada = <servicio><parametro><int>{v.idventa.text()}</int></parametro></servicio>;
				
				importesparciales = services.process(methodpostListado + 'SOAWrapper', llamada);
				
				importesparciales = new XML(importesparciales);
				
				importesparciales.setName("importeparcials");
				
				v.appendChild(importesparciales);
			} catch (ex) {
				if (log.isErrorEnabled()) {	
					log.error('LOG(ERROR) : Error in this call : ' + this.getClass().getName());
					log.error('LOG(ERROR) : to this : '+ methodpostListado +'SOAWrapper : '+this.getClass().getName());
					log.error('LOG(ERROR) : con esta llamada : ' + llamada);
					log.error('LOG(ERROR) : result in this Exception : '+ex);
					log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
				}
				
				throw ex.javaException;
			}
		}
	} else {
		throw new java.lang.Exception("El id de la reserva es obligatorio");
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(datosReserva.toXMLString());
}
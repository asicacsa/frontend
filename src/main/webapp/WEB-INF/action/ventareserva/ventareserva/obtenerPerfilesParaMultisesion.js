var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');			
var methodpost = 'obtenerPerfilesAplicablesConUnaTarifaProducto';
		
function handle(request, response) {	
	var xml = request.getParameter('xml');
	
	var idzona = null;
	var idcontenido = null;
	var idsesion = null;
	
	xml = new XML(xml);

	var fechaMin = null;

	//Recorremos todas las sesiones para obtener la que tiene la fecha mas cercana
	for each(var dto in xml.ObtenerPerfilesy1Tarifaparam) {
		for each(var zs in dto.lineadetallezonasesions.Lineadetallezonasesion.zonasesion) {
			var fechaString = zs.sesion.fecha.text();
			var array = fechaString.split("-");
			var camposFecha = array[0].split("/");
			
			var fechaActual = new java.util.Date(camposFecha[2], camposFecha[1] - 1, camposFecha[0]);
			
			if (fechaMin == null || fechaActual.before(fechaMin)) {
				fechaMin = fechaActual;
				
				idsesion = zs.sesion.idsesion;
				idzona = zs.zona.idzona;
				idcontenido = zs.sesion.contenido.idcontenido;
			}
		}
		
		delete dto.lineadetallezonasesions;
		
		dto.idsesion = idsesion;
		dto.idzona = idzona;
		dto.idcontenido = idcontenido;
	}
	
	var xmlEnvio = 	<servicio>
						<parametro>
							{xml}
						</parametro>
					</servicio>;
					
	
	var perfiles;
	
	try {
		//Eliminamos todos los elementos que no tengan valor
		xmlEnvio = comun.borraElementosSinHijos(xmlEnvio.toXMLString());
		
		perfiles = services.process(methodpost + 'SOAWrapper', xmlEnvio);
		
		perfiles = new XML(perfiles);
		
		for each(var arrayPerfiles in perfiles.ArrayList) {
			arrayPerfiles.setName("perfiles");
		}
		
		log.info(perfiles.toXMLString());
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : ');
			log.error('LOG(ERROR) : result in this Exception : '+ex);
		}
		
		throw ex.javaException;
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(perfiles.toXMLString());	
}
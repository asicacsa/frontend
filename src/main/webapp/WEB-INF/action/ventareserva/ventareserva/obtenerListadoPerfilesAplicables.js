var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {	
	
	var methodpost = request.getParameter('servicio');		
	
	//log.info('Si que pasa por su propio JS');
	
	var selecciona = null;
	var selecciona2 = null;

//GGL CLUB 02.06.2016
	//API 24 Agosto 2011
	//He estado repasando las posibles llamadas a este .js y parece que NO SE LLAMA ACTUALMENTE DESDE NINGUN PUNTO, 
	//siempre se acaba utilizando el obtenerListadoPerfilesAplicablesPalau.js
	//El canal se puede obtener en el servicio a partir de la sesionusuario
	//en su lugar vamos a pasarle el tipo de venta que es necesario y no tenemos forma de averiguarla.
	//selecciona = request.getParameter('idcanal');
	selecciona = request.getParameter('idtipoventa');
	selecciona2 = request.getParameter('idproducto');
	
	if (log.isInfoEnabled()) {
		log.info("obtenerComun: Id para seleccionar " + selecciona);
	}
	
	var envio = <servicio></servicio>
	
	// En este tipo de servicio no van a haber parametros de Entrada.
	if(selecciona != null && selecciona2 != null){
	
		selecciona = new XML(selecciona);
		selecciona2 = new XML(selecciona2);
		
		envio = <servicio>
					<parametro>
						<canal>
							<idcanal>{selecciona}</idcanal>
						</canal>
						<producto>
							<idproducto>{selecciona2}</idproducto>
						</producto>
					</parametro>
				</servicio>
	}

	if (log.isInfoEnabled()) {
		//log.info("Metodo a llamar : "+methodpost);
		log.info("Envio : "+envio);
	}
	var result;
	if (log.isErrorEnabled()) {
		try {
			result = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
	} else {
		result = services.process(methodpost+'SOAWrapper',envio);
	}
	
	if (log.isInfoEnabled()) {
		log.info(result);
	}
	
	if (result !=  null) {
		result = new XML(result);
		//result = this.postProcessXML(result, selecciona);		
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
	
}


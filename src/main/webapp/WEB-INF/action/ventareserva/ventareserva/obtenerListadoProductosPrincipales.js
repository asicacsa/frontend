var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {

	if (log.isInfoEnabled()) {
		log.info("entrada a obtenerListadoProductosPrincipales.action");
	}
	
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}

	// si xml no se envia, llama al metodo sin parametros	
	var xml = request.getParameter('xml');
	
	if (log.isInfoEnabled()) {
		log.info("obtenerListadoProductosPrincipales.action, method: " + methodpost);
		log.info("obtenerListadoProductosPrincipales.action, xml: " + xml);
	}
	
	if(xml == null ) {
		xml = '<servicio/>';
	} else {
		var xmlSinHijos;
		var sbf = context.beans.getBean('scriptBeanFactory');
		var comun = sbf.getBean('transformerXML.js');
		
		xmlSinHijos = comun.borraElementosSinHijos(xml);
		
		if (xmlSinHijos.name() == "parametro") {
			xml = <servicio>
					{xmlSinHijos}
				  </servicio>
		} else {
			xml = <servicio>
					<parametro>
						{xmlSinHijos}
					</parametro>
				  </servicio>
		}
		
	}
	
	if (log.isInfoEnabled()) {
		log.info("obtenerListadoProductosPrincipales.action, service parameters: " + xml);
	}
	
	var result = null;
	try {
		result = services.process(methodpost+'SOAWrapper',xml);
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost);
			log.error("LOG(ERROR) : con esta llamada: " + xml);
			log.error('LOG(ERROR) : con este tipo de error : ' + ex);
			log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
		}
		
		throw ex.javaException;
	}
	
	if (result == null) {
		result = <ArrayList />;
	}else {
		result = new XML(result);
		
		if(result.Entry.length() > 0){
			result.Entry[result.Entry.length()-1].setName("Entrycombinada");
		}
	}
	
	if (log.isDebugEnabled()) {
		log.debug("obtenerListadoProductosPrincipales.action accesing " + methodpost);
		log.debug("obtenerListadoProductosPrincipales.action result: " + result.toXMLString().substr(0,300) + "[...]");
	}
	
	
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	
	response.writer.println(result.toXMLString());
}
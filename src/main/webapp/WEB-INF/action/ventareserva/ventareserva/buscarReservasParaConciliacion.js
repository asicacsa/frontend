var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	if (log.isInfoEnabled()){
		log.info("Entrada a buscarReservasParaConciliacion.action.");
	}
	
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}

	// si xml no se envia, llama al metodo sin parametros	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		xml = '<servicio />';
	} else {
		var xmlSinHijos;
		
		xmlSinHijos = comun.borraElementosSinHijos(xml);
		
		//No es necesario mandar el nombre del cliente
		delete xmlSinHijos.nombreCliente;
		
		if (xmlSinHijos.name() == "parametro") {
			xml = <servicio>
					{xmlSinHijos}
				</servicio>
		} else {
			xml = <servicio>
					<parametro>
						{xmlSinHijos}
					</parametro>
				</servicio>
		}
		
	}
	
	if (log.isInfoEnabled()){
		log.info("buscarReservasParaConciliacion, envio: " + xml);
	}
	
	var result = null;
	
	try {
		result = services.process(methodpost+'SOAWrapper',xml);
		
		if (log.isInfoEnabled()){
			log.info("buscarReservasParaConciliacion, respuesta del servicio: " + result.substr(0,300) + "[...]");
		}
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost);
			log.error("LOG(ERROR) : con esta llamada: " + xml);
			log.error('LOG(ERROR) : con este tipo de error : ' + ex);
			log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
		}
		
		throw ex.javaException;
	}
	
	
	if (result == null) {
		result = "<ArrayList />";
	} else {
		result = new XML(result);
		result = this.postProcessXML(result);
		result = result.toXMLString();
	}
	
	
	if (log.isInfoEnabled()){
		log.info("buscarReservasParaConciliacion, resultado final: " + result.substr(0,300) + "[...]");
	}
	
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result);
}



function getFechaMenor(fecha1 , fecha2){
	if(fecha1 < fecha2){
		return fecha1;
	}
	else{
		return fecha2;
	}
}



function getFechaMenorXML(ventaoreserva){
	var fecha_aux = new Date ();
	var fecha="";
	var dia = "";
	var mes = "";
	var anio = "";
	var primeravisita = "";
	
	for each (sesion in ventaoreserva.lineadetalles.Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion){	
		fecha = sesion.fecha.text();
	
		fecha = fecha.substring (0, fecha.indexOf("-"));
		dia = fecha.substring (0, fecha.indexOf("/"));
		
		fecha = fecha.substring (fecha.indexOf("/")+1);
		mes = fecha.substring (0, fecha.indexOf("/"));
		
		fecha = fecha.substring (fecha.indexOf("/")+1);
		anio = fecha.substring (0);
					
		fecha_aux.setFullYear(anio, mes-1, dia);

		if(primeravisita == ""){
			primeravisita = new Date();
			primeravisita.setFullYear(anio, mes-1, dia);	
		}
		else{
			primeravisita = new Date(this.getFechaMenor(fecha_aux, primeravisita));
		}
		
	}
	
	for each (sesion in ventaoreserva.lineadetallessinventa.Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion){	
		fecha = sesion.fecha.text();

		fecha = fecha.substring (0, fecha.indexOf("-"));
		dia = fecha.substring (0, fecha.indexOf("/"));
		
		fecha = fecha.substring (fecha.indexOf("/")+1);
		mes = fecha.substring (0, fecha.indexOf("/"));
		
		fecha = fecha.substring (fecha.indexOf("/")+1);
		anio = fecha.substring (0);
					
		fecha_aux.setFullYear(anio, mes-1, dia);
		
		if(primeravisita == ""){
			primeravisita = new Date();
			primeravisita.setFullYear(anio, mes-1, dia);	
		}
		else{
			primeravisita = new Date(this.getFechaMenor(fecha_aux, primeravisita));
		}
		
	}
	
	if(primeravisita != ""){
		dia = primeravisita.getDate();
		if(dia < 10)
			dia = "0" + dia;
	
		mes = primeravisita.getMonth()+1;
		if(mes < 10)
			mes = "0" + mes;
	
		primeravisita = dia + "/" + mes + "/" + primeravisita.getFullYear();
	}
	return primeravisita;
	
}



function postProcessXML(xml){
	
	var primeravisita = "";
	
	for each (r in xml.Reserva){
	
		
		if (r.estadooperacion.idestadooperacion.text() == "4") {
			r.lineadetalles.* = r.lineadetallesanuladas.*;
			r.importe=calcularImporteTotal(r);
		}
		primeravisita = getFechaMenorXML(r);
		r.primeravisita = <primeravisita>{primeravisita}</primeravisita>;
		r.referencia = <referencia>{r.idreserva.text()}</referencia>;
		
		eliminarProductosRepetidos(r.lineadetalles);
	}
	
	for each (v in xml.Venta){
		
		
		if (v.estadooperacion.idestadooperacion.text() == "4") {
			v.lineadetalles.* = v.lineadetallesanuladas.*;
			v.importe=calcularImporteTotal(v);
		}
		
		primeravisita = getFechaMenorXML(v);
		v.primeravisita = <primeravisita>{primeravisita}</primeravisita>;
		v.referencia = <referencia>{v.idventa.text()}</referencia>;
		
		eliminarProductosRepetidos(v.lineadetalles);
	}
	
	return xml;
}



function calcularImporteTotal(xml) {
	var result = new Number(0);
	for each (l in xml.lineadetalles.Lineadetalle) {
		result = result+new Number(l.importe.text());
	}
	
	//log.info("Resultado a : "+result);
	
	return result;
}



function eliminarProductosRepetidos(xml){
	//Solo queremos mostrar los productos que no estan repetidos
	var productos = new java.util.ArrayList();
	var cont = 0;
	var idToSearch;

	for each (ld in xml.Lineadetalle) {
		idToSearch = Number(ld.producto.idproducto.text());
		if (!productos.contains(idToSearch)) {
			productos.add(cont, idToSearch);
			cont = cont + 1;
		} else {
			delete xml.Lineadetalle[cont];
		}
	}

}

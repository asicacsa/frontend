var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	var param = request.getParameter('xml');
	param = new XML(param.trim());	

	if (log.isDebugEnabled()){
		log.error("obtenerListadoHorasPresentacion, xml entrada: " + param.toXMLString());
	}
	
	if (param=="null"){
		throw new java.lang.Exception("Falta el identificador de la sesion para la carga del combo de horas presentacion");
	}
	
	var methodpost = 'obtenerListaAfluenciaPorSesion';

	var data="";
	var xmlpeticion = "";
	var sesionsLD = param.Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion;
	var sesionsLDGratuitas = 
		param.Lineadetalle.lineadetallegratuitas.Lineadetallegratuita.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion;
		
	if(sesionsLD.toString() == "" && sesionsLDGratuitas.toString() == ""){
		throw new java.lang.Exception("Debe haber al menos una sesion para mostrar las horas de presentacion")
	}
	else{
		xmlpeticion = "<servicio><parametro><list>";
		
		for each (idsesion in sesionsLD.idsesion){
			xmlpeticion = xmlpeticion + "<int>" + idsesion.toString() + "</int>";
		}
		
		for each (idsesion in sesionsLDGratuitas.idsesion){
			xmlpeticion = xmlpeticion + "<int>" + idsesion.toString() + "</int>";
		}
		
		xmlpeticion += "</list></parametro></servicio>";

	}
	
	var resultado = param;
	var aux;
	
	try {
		aux = services.process(methodpost+'SOAWrapper',xmlpeticion);
	} catch (ex) {
		if (log.isErrorEnabled()) {	
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : con este tipo de error : ' + ex);				
			log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
		}
		
		throw ex.javaException;
	}
		
	if (aux != null) {
		aux = new XML(aux.trim());
		resultado = this.postProcessXML(aux, resultado);
	}
	
	if (log.isDebugEnabled()){
		log.error("obtenerListadoHorasPresentacion, final response: " + resultado.toXMLString());
	}

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}


function postProcessXML(xml, resultado) {
	var numasistentes = 0;
	
	// construye etiquetas
	
	for each(var lb in xml.ArrayList.Presentacionsesion) {
		numasistentes = 0;
		for each(var asistentes in lb..numpersonas){
			if (asistentes.toString() == "") {
				asistentes = 0;
			} else {
				asistentes = parseInt(asistentes.text());
			}
			
			numasistentes = numasistentes + parseInt(asistentes);
		}
		lb.label = lb.hora.text() + " convocados " + numasistentes;
	}
	
	
	// actualiza los Presentacionsesion para cada linea de detalle
	
	/*for each (ld in resultado.Lineadetalle) {
		for each (ldzs in ld.lineadetallezonasesions.Lineadetallezonasesion) {
			ldzs.zonasesion.sesion.presentacionsesions.Presentacionsesion = 
				xml.ArrayList.Presentacionsesion.(sesion.idsesion == ldzs.zonasesion.sesion.idsesion); 
		}
	
		for each (ldzs in ld.lineadetallegratuitas.Lineadetallegratuita.lineadetallezonasesions.Lineadetallezonasesion) {
			ldzs.zonasesion.sesion.presentacionsesions.Presentacionsesion = 
				xml.ArrayList.Presentacionsesion.(sesion.idsesion == ldzs.zonasesion.sesion.idsesion); 
		}
	}*/
	
	
	// tenemos una colección de Presentacionsesion. Hay que darle la vuelta al XML,
	// conseguir una colección de Tipoproducto. Cada uno con sesiones. Y cada sesión
	// con presentacionsesiones.
	
	var result = new XML();
	result = <ArrayList/>;
	
	for each(var ps in xml.ArrayList.Presentacionsesion) {
		var tp = result.tipoproducto.(idtipoproducto.text()==ps.sesion.contenido.tipoproducto.idtipoproducto.text());
		if (tp.length() == 0){
			result.* += ps.sesion.contenido.tipoproducto;
		}
		tp = result.tipoproducto.(idtipoproducto.text()==ps.sesion.contenido.tipoproducto.idtipoproducto.text());
		

		var s = tp.sesions.sesion.(idsesion.text()==ps.sesion.idsesion.text());
		if (s.length() == 0){
			tp.sesions.* += ps.sesion;
		}
		s = tp.sesions.sesion.(idsesion.text()==ps.sesion.idsesion.text());
		
		
		var p = s.presentacionsesions.presentacionsesion.(idpresentacionsesion.text()==ps.idpresentacionsesion.text());
		if (p.length() == 0){
			s.presentacionsesions.* += <presentacionsesion>
											{ps.idpresentacionsesion}
											{ps.label}
									   </presentacionsesion>;	
		}
		
		
		// añado el tag donde irá cada presentacionsesion seleccionada.
		if (s.presentacionsesion.length() == 0){
			s.presentacionsesion.idpresentacionsesion = <idpresentacionsesion/>;
		}
		
		
		// marco la presentacionsesion preseleccionada para la sesion.
		// Busco la sesion entre lineas normales y gratuitas.
		var ldzs = resultado.Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.(zonasesion.sesion.idsesion.text()==s.idsesion.text());
		var ldzsGrat = resultado.Lineadetalle.lineadetallegratuitas.Lineadetallegratuita.lineadetallezonasesions.Lineadetallezonasesion.(zonasesion.sesion.idsesion.text()==s.idsesion.text());
		ldzs = ldzs + ldzsGrat;
		
		if (log.isDebugEnabled()){
			log.error("obtenerListadoHorasPresentacion.postProcessXML, ldzs: " + ldzs);
		}
		
		var idps;
		
		if (ldzs.length()>1){
			idps = ldzs[0].presentacionsesion.idpresentacionsesion.text();
		} else if (ldzs.length()==1){
			idps = ldzs.presentacionsesion.idpresentacionsesion.text();
		}
		
		if (log.isDebugEnabled()){
			log.error("obtenerListadoHorasPresentacion.postProcessXML, idps: " + idps.toXMLString());
		}
		if (idps.toString()!="" && idps.toString()!="null") {
			s.presentacionsesion.idpresentacionsesion = <idpresentacionsesion>{idps}</idpresentacionsesion>;
		}
		
	}
	 
	resultado = result;
	
	return resultado;
}
var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {
	var methodpost = 'obtenerVentaPorIdSoloEntradas';

	// si xml no se envia, llama al metodo sin parametros	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		xml = '<servicio />';
	} else {
		var xmlSinHijos;
		var sbf = context.beans.getBean('scriptBeanFactory');
		var comun = sbf.getBean('transformerXML.js');
		
		xmlSinHijos = comun.borraElementosSinHijos(xml);

		xml = <servicio>
				<parametro>
					{xmlSinHijos}
				</parametro>
			  </servicio>
	}
	
	
	var result = null;
	
	try {
		result = services.process(methodpost+'SOAWrapper',xml);
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost);
			log.error("LOG(ERROR) : con esta llamada: " + xml);
			log.error('LOG(ERROR) : con este tipo de error : ' + ex);
			log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
		}
		
		throw ex.javaException;
	}
	
	if (result == null) {
		result = "<ArrayList />";
	} else {
		result = new XML(result);
		
		var numimpresiones = 0;
		
		for each (var ld in result.lineadetalles.Lineadetalle) {
			numimpresiones = ld.bonosForIdlineadetalle.Bono.(impresionesbono.Impresionbono.length() >= 1 && anulado == 0).length();
			
			delete ld.entradas;
			
			ld.numimpresiones = numimpresiones;
			ld.imprimir = Number(ld.cantidad.text()) - numimpresiones;
		}		
	}

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	
	response.writer.println(result);
}

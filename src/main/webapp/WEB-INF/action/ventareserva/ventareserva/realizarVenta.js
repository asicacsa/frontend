var services = context.beans.getBean('httpServiceSOA');
var methodpost = "realizarVenta";


function handle(request, response) {
	var entrada = new Date(); 
	
	var xml = request.getParameter('xml');	
	if (log.isInfoEnabled()) {
		log.info("\nEntrada a realizarVenta.action en el instante " + entrada.getTime() + " ms desde 1970.\n ");
		log.info("\nEntrada a realizarVenta.action XML: " + xml);
	}
		
	var resultado;
	var envio;
	
	if (xml!=null) {
		xml = new XML(xml);
		
		
		xml = this.preProcessXML(xml);
		
		envio = 
			<servicio>
				<parametro>
					{xml}
				</parametro>
			</servicio>
		
		var aux = null;
		
		//Y llamamos al servicio...
		try {
			if (log.isDebugEnabled()) {
				log.debug("realizarVenta.action, envio: " + envio.toXMLString());
			}
		
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			if (log.isErrorEnabled()) {	
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : ');
				log.error('LOG(ERROR) : con esta llamada : ' + envio);
				log.error('LOG(ERROR) : con este tipo de error : ' + ex);				
				log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
			}
			
			throw ex.javaException;
		}
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		if(aux != null){
			aux = new XML(aux);
			resultado = 
					<ok>
						{aux}
					</ok>
		}			
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a realizarVenta es nulo!!!");
		}
		resultado = <error/>
	}
	if (log.isInfoEnabled()) {
		log.info("Resultado de realizarVentaPases : "+resultado.toXMLString());
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
	
	if (log.isInfoEnabled()) {
		log.info("salida de realizarVenta.action tras " + ( new Date().getTime() - entrada.getTime() )  );
	}

}

function preProcessXML(param) {
	var entrada = new Date();
	if (log.isInfoEnabled()) {
		log.info("entrada a preProcessXML dentro de realizarVenta.js en " + entrada.getTime() + " ms desde 1970.");
	}
	
	param = new XML(param);
	
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('transformerXML.js');
	var eliminarBasuraLD = sbf.getBean('eliminarDatosInvalidosLineaDetalle.js');
	
	param = comun.sustituyeLiteralesBooleanosEnVenta(param);
	
	//Eliminamos el tag isBono
	delete param.isBono;
	
	//Eliminamos lo que sobra de la linea de detalle
	param = eliminarBasuraLD.eliminarDatosInvalidosLineaDetalle(param);
	
	//Elimino las etiquetas que contienen la información sobre los descuentos del cliente
	delete param..porcentajesDescuentoLD;
	delete param..porcentajesDescuentoLDA;
	delete param..reciboYcopia;
		
	//Eliminamos todos los elementos que no tengan hijos
	param = comun.borraElementosSinHijos(param.toXMLString());

	if (log.isInfoEnabled()) {
		log.info("\nSalida de preProcessXML dentro de realizarVenta.js tras " + ( new Date().getTime() - entrada.getTime() )  + " ms\n" );
	}

	return param;
	
}
var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {		
	var resultado;
	var methodpost = "actualizarPrerreservaLocalidadesNoNumeradas";
	var envio;
	
	//No pasamos lineas de detalle para anular...
	xml="<lineadetalles/>";
		
	xml = new XML(xml);
	
	envio = 
		<servicio>
			<parametro>
				{xml}
			</parametro>
		</servicio>
			
	
	if (log.isInfoEnabled()) {
		log.info("Este es el envio : "+envio);
	}
	
	var aux = null;
	
	//Y llamamos al servicio...
	try {
		aux = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		if (log.isErrorEnabled()) {	
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : con esta llamada : ' + envio);
			log.error('LOG(ERROR) : result in this Exception : '+ex);
		}
		//No lanzamos excepcion...
	}
	
	resultado = new XML(aux);
	
	if (log.isInfoEnabled()) {
		log.info("## resultado: " + resultado);
	}
			
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

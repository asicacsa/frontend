var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {
	var methodpost = 'obtenerListadoPerfilesAplicables';
	var idtipoventa = 5;
	var idproducto = request.getParameter('idproducto');

	if (log.isInfoEnabled()) {
		log.info( methodpost + ".js: idtipoventa " + idtipoventa);
		log.info( methodpost + ".js: idproducto " + idproducto);
	}
	var envio = <servicio></servicio>;
	if( idproducto != null ) {
		idtipoventa = new XML(idtipoventa);
		idproducto = new XML(idproducto);
		envio = <servicio>
					<parametro>
						<Obtenertarifasparam>
							<idtipoventa>{idtipoventa}</idtipoventa>
							<idproducto>{idproducto}</idproducto>
						</Obtenertarifasparam>
					</parametro>
				</servicio>
	}
	if (log.isInfoEnabled()) {
		log.info("parametro xml (envio) de obtenerListadoPerfilesAplicablesVentaAbonosNoNumerada.js: ");
		log.info( envio );
	}
	var result = null;
	try {
		result = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
		}
		throw ex.javaException;
	}
	if (log.isInfoEnabled()) {
		log.info("Lo que devuelve obtenerListadoPerfilesAplicablesVentaAbonosNoNumerada.js "+result);
	}
	if (result !=  null) {
		result = new XML(result);
	}
	response.setCharacterEncoding( 'UTF-8' );
	response.setContentType( 'text/xml' );
	response.writer.println( result.toXMLString() );
}
var services = context.beans.getBean('httpServiceSOA');
//var servicio_alta ='realizarVentaAbonos';
//var servicio_edicion ='actualizarAbonos';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
	
	if (log.isInfoEnabled()) {
		log.info("Entrada a aplicarDescuentoPromocionalListaActualizandoLineaDetallePases.action");
	}	
		
	var resultado;
	var methodpost = 'aplicarDescuentoPromocionalLista';
	var envio;
		
	var lalinea = request.getParameter('lalinea');
	var idtipoventa = request.getParameter('idtipoventa');
	//GGL Anyado la fecha actual como fecha de venta
	//var fecha = new Date();
	
	//var clienteventa = request.getParameter('clienteventa');
	
	lalinea = new XML(lalinea.trim());
	
	if (log.isDebugEnabled()) {
		log.debug("aplicarDescuentoPromocionalListaActualizandoLineaDetallePases.action, input XML: " + lalinea.toXMLString());
	}
	
	var eliminarBasuraLD = sbf.getBean('eliminarDatosInvalidosLineaDetalle.js');
	
	lalinea = eliminarBasuraLD.eliminarDatosInvalidosLineaDetalleDeUnaLineaPases(lalinea);
	
	var comun = sbf.getBean('transformerXML.js');

	lalinea = comun.borraElementosSinHijos(lalinea);
	
	//var idestadolocalidadDeLaLinea = lalinea.estadolocalidads.Estadolocalidad.idestadolocalidad.text();
	
	var xml = <dto>
	 			<lineadetalles>
	 				{lalinea}
	 			</lineadetalles>
	 			<idtipoventa>{idtipoventa}</idtipoventa>	 			
			  </dto>;
			//<aplicarDescuentoCliente>1</aplicarDescuentoCliente>
	xml = this.preProcessXML(xml);
	envio = 
		<servicio>
			<parametro>
				{xml}
			</parametro>
		</servicio>
		
	if (log.isDebugEnabled()) {
		log.debug("aplicarDescuentoPromocionalListaActualizandoLineaDetallePases, envio: " + envio);
	}
	
	//Y llamamos al servicio...
	var aux = null;
	
	try {
		aux = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : ' + this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper');
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			log.error('LOG(ERROR) : con esta descripcion : '+ ex.javaException.getMessage());
			log.error('LOG(ERROR) : con esta lammada : '+ envio);
		}
		
		throw ex.javaException;
	}
	
	if (log.isDebugEnabled()) {
		log.debug("aplicarDescuentoPromocionalListaActualizandoLineaDetallePases, respuesta servicio: " + aux);
	}
	
	//Este seervicio nunca devuelve null, en todo caso un Array vacio
	aux = new XML(aux.trim());
	
	if(aux.hasOwnProperty("LineadetalleVentaPasesDTO")){
		resultado = aux.LineadetalleVentaPasesDTO;
	} else {
		resultado = new XML();
		if (log.isInfoEnabled()) {
			log.info("aplicarDescuentoPromocionalListaActualizandoLineaDetallePases, resultado null.");
		}
	}
	
	if(resultado.tarifaproducto.importe.toString() == ""){
		resultado.importe= <importe>"--"</importe>;
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
	
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	param = new XML(param.trim());
	//Eliminamos las lineas de detalle gratuitas antiguas
	//delete param..lineadetallegratuitas;
	
	//Eliminamos los datos que son de solo lectura
	//delete param..detalles;
	//delete param..sesion.nombre;
	if(param.lineadetalles.LineadetalleVentaPasesDTO.importe.text() == "--"){
		param.lineadetalles.LineadetalleVentaPasesDTO.importe = "0.00";
	}
	
	if(param.lineadetalles.LineadetalleVentaPasesDTO.tarifaproducto.importe.text() == "--"){
		param.lineadetalles.LineadetalleVentaPasesDTO.tarifaproducto.importe = "0.00";
	}
	
	param = comun.borraElementosSinHijos(param.toXMLString());
	return param;
	
}

var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {
		
	if (log.isInfoEnabled()) {
		log.info("Entrada a actualizarLineadetallesVenta.action");
	}

	var xml = request.getParameter('xml');
	var service = request.getParameter('servicio');	
	var resultado;
	var methodpost = "actualizarLineadetallesVenta";
	var envio;
	
	if (service!=null){
		methodpost = service;
	}
	
	
	if (xml!=null) {
		xml = new XML(xml);
		
		var xmlSinHijos;
		var sbf = context.beans.getBean('scriptBeanFactory');
		var comun = sbf.getBean('transformerXML.js');		
		var eliminarBasuraLD = sbf.getBean('eliminarDatosInvalidosLineaDetalle.js');
		
		xml.modificacionlineadetalles = eliminarBasuraLD.eliminarDatosInvalidosLineaDetalleDeUnaLinea(xml.modificacionlineadetalles);
		
		//Elimino las etiquetas que contienen la información sobre los descuentos del cliente
		delete xml..porcentajesDescuentoLD;
		delete xml..porcentajesDescuentoLDA;
		
		xml = comun.sustituyeLiteralesBooleanosEnVenta( xml );
		xmlSinHijos = comun.borraElementosSinHijos(xml);

		//Aqui vamos a eliminar aquellas modificacionImporteParcial que no tenha hijos
		for each(i in xmlSinHijos.modificacionimporteparcials.Modificacionimporteparcial){
			if(i.importeparcial.toString() == ""){
				delete i.tipomodificacion;
			}
		}
		var selectedNodes = xmlSinHijos..Modificacionimporteparcial.(importeparcial.@isnew == false);
		for(var i = 0; i < selectedNodes.length(); i = 0){
			delete selectedNodes[i];
		}
		xmlSinHijos = comun.borraElementosSinHijos(xmlSinHijos);
		
		envio = 
			<servicio>
				<parametro>
					{xmlSinHijos}
				</parametro>
			</servicio>
		
		var aux = null;
		
		if (log.isInfoEnabled()) {
			log.info("actualizarLineadetallesVenta.action, envio a " + methodpost + ": " + envio.toXMLString());
		}
	
		try {
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			if (log.isErrorEnabled()) {	
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				log.error('LOG(ERROR) : con esta llamada : ' + envio);
				log.error('LOG(ERROR) : result in this Exception : '+ex);
			}
			
			throw ex.javaException;
		}
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		if(aux != null){
		//log.info('aux es distinto de nulo');
			aux = new XML(aux);
			resultado = 
					<ok>
						{aux}
					</ok>
			if (log.isInfoEnabled()) {
				log.info("actualizarLineadetallesVenta.action, resultado: " + resultado);
			}
		} else {
			resultado = new XML();
		}
	} else {
		if (log.isInfoEnabled()) {
			log.info("actualizarLineadetallesVenta, xml de entrada: " + xml);
		}
		throw new java.lang.Exception("Peticion incorrecta.");
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
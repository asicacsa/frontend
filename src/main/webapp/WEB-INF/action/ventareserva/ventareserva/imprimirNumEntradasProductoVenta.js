var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {
	var methodpost = 'imprimirNumEntradasProductoVenta';

	// si xml no se envia, llama al metodo sin parametros	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		xml = '<servicio />';
	} else {
		var xmlSinHijos;
		var sbf = context.beans.getBean('scriptBeanFactory');
		var comun = sbf.getBean('transformerXML.js');
		
		xmlSinHijos = comun.borraElementosSinHijos(xml);
		
		var xmlParaEnvio = <list />;
		
		for each (var ld in xmlSinHijos..Lineadetalle) {
			var xmlAux = <ImprmirNumEntradasProductoVenta><idlineadetalle /><idproducto /><numimpresiones /></ImprmirNumEntradasProductoVenta>; //llamamos integer en lugar de int porque en caso contrario no podemos acceder al nodo
			
			xmlAux.idlineadetalle = ld.idlineadetalle.text()+"";
			xmlAux.idproducto = ld.producto.idproducto.text()+"";
			xmlAux.numimpresiones = ld.imprimir.text()+"";		

			
			xmlParaEnvio.appendChild(xmlAux);

		}

		xml = <servicio>
				<parametro>
					{xmlParaEnvio}
				</parametro>
			  </servicio>
	}

	var result = null;
	
	if (log.isInfoEnabled()) {
		log.info('INFO XML Enviado a '+methodpost+' : '+xml);
	}
	try {
		result = services.process(methodpost+'SOAWrapper',xml);
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost);
			log.error("LOG(ERROR) : con esta llamada: " + xml);
			log.error('LOG(ERROR) : con este tipo de error : ' + ex);
			log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
		}
		
		throw ex.javaException;
	}
	
	if (result == null) {
		result = <ArrayList />;
	} else {
		result = new XML(result);
	}

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	
	response.writer.println(result.toXMLString());
}

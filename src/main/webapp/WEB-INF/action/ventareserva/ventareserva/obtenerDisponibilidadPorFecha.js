var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {	
	
	if (log.isInfoEnabled()) {
		log.info("entrada a obtenerDisponibilidadPorFecha.");
	}
	
	var methodpost = request.getParameter('servicio');		
	
	// si xml no se envia, llama al metodo sin parametros	
	var selecciona = null;
	
	selecciona = request.getParameter('xml');
	
	var envio = <servicio></servicio>
	
	// En este tipo de servicio no van a haber parametros de Entrada.
	if(selecciona != null){
		selecciona = new XML(selecciona);
		envio = <servicio>
					<parametro>
						<date>{selecciona}</date>
					</parametro>
				</servicio>
	}

	if (log.isInfoEnabled()) {
		log.info("obtenerDisponibilidadPorFecha, envío: " + envio.toXMLString());
	}
	
	log.error(" envío: " + envio.toXMLString());
	var result;
	if (log.isErrorEnabled()) {
		try {
			result = services.process(methodpost+'SOAWrapper',envio).trim();
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
	} else {
		result = services.process(methodpost+'SOAWrapper',envio).trim();	
	}
	
	
	if (result !=  null) {
		result = new XML(result);
		result = this.postProcessXML(result);		
	}
	
	if (log.isInfoEnabled()) {
		log.info("obtenerDisponibilidadPorFecha, final result: " + result.toXMLString());
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
	
}

function postProcessXML(xml){
	// procesaremos el resultado para calcular el total de la disponibilidad
	// para cada una de las sesiones
	for each (var sesion in xml.RecintoConUnidadNegocio.recintos.Recinto.tipoproductos.Tipoproducto.Contenido.sesions.Sesion){
		var numlibres = 0;
				
		for each (var zonasesion in sesion.zonasesions.Zonasesion){
			numlibres += (zonasesion.numlibres.text() - 0);
		}
		
		// Añadimos overbooking
		sesion.numlibres += <numlibres>{numlibres + Number(sesion.overbookingventa.text())}</numlibres>;
	}	
	
	//Creo un nuevo objeto donde voy a almacenar los datos que necesito
	var disponibilidad= <ArrayList/>;
	var unidadnegocio;
	var recinto;
	for each (RecintoConUnidadNegocio in xml.RecintoConUnidadNegocio){
		unidadnegocio = <UnidadNegocio>{RecintoConUnidadNegocio.unidadnegocio.nombre}<recintos /></UnidadNegocio>;
		for each (Recinto in RecintoConUnidadNegocio.recintos.Recinto){
			recinto = <Recinto>{Recinto.nombre}<sesiones/></Recinto>;
			for each(Contenido in Recinto.tipoproductos.Tipoproducto.Contenido){
				for each(Sesion in Contenido.sesions.Sesion){
					sesion = 
						<sesion>
							<hora>{Sesion.horainicio.toString()}</hora>
							<contenido>{Contenido.nombre}{Contenido.descripcion}</contenido>
							<numlibres>{Sesion.numlibres.toString()}</numlibres>
						</sesion>;
					recinto.sesiones.appendChild(sesion);
				}	
			}
			unidadnegocio.recintos.appendChild( recinto );
		}
		disponibilidad.appendChild( unidadnegocio );
	}
	
	//Elimino lo recintos que no tienen sesiones
	var i=0;
	var j=0;
	for (i=0; i< disponibilidad.UnidadNegocio.length(); i++){
		for(j=0; j< disponibilidad.UnidadNegocio[i].Recinto.length(); j++){
			if(disponibilidad.UnidadNegocio[i].Recinto[j].sesiones.toString() == ''){
				delete disponibilidad.UnidadNegocio[i].Recinto[j];
				j--;
			}
		}
	}
	
	return disponibilidad;
}

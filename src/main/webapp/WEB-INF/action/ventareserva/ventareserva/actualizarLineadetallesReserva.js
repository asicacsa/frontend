var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {
		
	var xml = request.getParameter('xml');
	var resultado;
	var methodpost = "actualizarLineadetallesReserva";
	var envio;
	
	
	if (xml!=null) {
		xml = new XML(xml);

		if (log.isInfoEnabled()) {
			log.info("\nactualizarLineadetallesReserva. Este es el xml que recibimos:\n" + xml);
		}

		var xmlSinHijos;
		var sbf = context.beans.getBean('scriptBeanFactory');
		var comun = sbf.getBean('transformerXML.js');
		var eliminarBasuraLD = sbf.getBean('eliminarDatosInvalidosLineaDetalle.js');
		
		//Elimino las etiquetas que contienen la información sobre los descuentos del cliente
		delete xml..porcentajesDescuentoLD;
		delete xml..porcentajesDescuentoLDA;
		xml.modificacionlineadetalles = eliminarBasuraLD.eliminarDatosInvalidosLineaDetalleDeUnaLinea(xml.modificacionlineadetalles);

		if (log.isInfoEnabled()) {
			log.info("\nactualizarLineadetallesReserva. Este es xml.modificacionlineadetalles tras ser procesado por eliminarDatosInvalidosLineaDetalleDeUnaLinea:\n" + xml.modificacionlineadetalles );
		}

		// en este caso no hay impresion de recibos o entradas, 
		// quitamos elementos para evitar problemas.
		delete xml.venta.entradasimpresas;
		delete xml.venta.reciboimpreso;
		//API 17 Enero de 2011 Esto hay que eliminarlo también.
		delete xml.venta.imprimirSoloEntradasXDefecto;

		if (log.isInfoEnabled()) {
			log.info("\nactualizarLineadetallesReserva. Este es xml tras eliminar informacion de impresion:\n" + xml );
		}

		xml = comun.sustituyeLiteralesBooleanosEnVenta(xml);
		xmlSinHijos = comun.borraElementosSinHijos(xml);

		if (log.isInfoEnabled()) {
			log.info("\nactualizarLineadetallesReserva. Este es xmlSinHijos tras eliminar hijos vacios y sustituir trues y flases por 1s y 0s:\n" + xmlSinHijos );
		}

		envio = 
			<servicio>
				<parametro>
					{xmlSinHijos}
				</parametro>
			</servicio>
		
		var aux = null;
		
		//Y llamamos al servicio...
		if (log.isInfoEnabled()) {
			log.info("actualizarLineadetallesReserva.action, envio a " + methodpost + ": " + envio.toXMLString());
		}
		try {
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			if (log.isErrorEnabled()) {	
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				log.error('LOG(ERROR) : con esta llamada : ' + envio);
				log.error('LOG(ERROR) : result in this Exception : '+ex);
			}
			
			throw ex.javaException;
		}
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		if(aux != null){
		//log.info('aux es distinto de nulo');
			aux = new XML(aux);
			resultado = 
					<ok>
						{aux}
					</ok>
			if (log.isInfoEnabled()) {
				log.info("actualizarLineadetallesReserva.action, resultado: " + resultado);
			}
		}
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a realizarreserva es: " + xml);
		}
		throw new java.lang.Exception("Peticion incorrecta.");
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
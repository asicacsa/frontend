var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {
	var resultado;
	var methodpost = "actualizarPrerreservaLocalidadesNoNumeradas";
	var envio;

	envio = 
		<servicio>
			<parametro>
				<lineadetalles />
			</parametro>
		</servicio>
	
	var aux = null;
	
	//Y llamamos al servicio...
	try {
		aux = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		if (log.isErrorEnabled()) {	
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : con esta llamada : ' + envio);
			log.error('LOG(ERROR) : result in this Exception : '+ex);
		}
		
		throw ex.javaException;
	}
	
	resultado = new XML(aux);
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
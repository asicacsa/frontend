var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}

	// si xml no se envia, llama al metodo sin parametros	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		xml = '<servicio />';
	} else {
		var xmlSinHijos;
		var sbf = context.beans.getBean('scriptBeanFactory');
		var comun = sbf.getBean('transformerXML.js');
		
		xmlSinHijos = comun.borraElementosSinHijos(xml);

		xml = <servicio>
				<parametro>
					{xmlSinHijos}
				</parametro>
			  </servicio>
	}
	
	
	var result = null;
	try {
		result = services.process(methodpost+'SOAWrapper',xml);
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost);
			log.error("LOG(ERROR) : con esta llamada: " + xml);
			log.error('LOG(ERROR) : con este tipo de error : ' + ex);
			log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
		}
		throw ex.javaException;
	}
	
	if (result == null) {
		result = <ArrayList />;
	} else {
		result = new XML(result.trim());
		result = postProceso(result);
	}
	
	//Si no tiene puntos de recogida agregamos el id
	if (! result.puntosrecogida.hasOwnProperty("idpuntosrecogida")) {
		result.puntosrecogida.idpuntosrecogida = "";
	}
	
	if (log.isDebugEnabled()) {
		log.debug("obtenerReservaPorId.action accesing " + methodpost);
		log.debug("	   result: " + result.toXMLString().substr(0,300) + "[...]");
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	
	log.info('############### reserva = '+result);
	response.writer.println(result.toXMLString());
}


function postProceso(xmlparam) {	
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('obtenerProductoPorIdVentaReserva.js');
	
	//Almacenamos los descuentos de cliente que se han aplicado a la reserva
	var porcentajesDescuentoLD = <porcentajesDescuentoLD />;
	
	//Obtenemos los perfiles para poder modificarlos de cada linea de detalle
	for each (ld in xmlparam.lineadetalles.Lineadetalle) {
		
		//Añado la colección de los porcentajes de cliente aplicados a la reserva, y el porcentaje correspondiente a cada ld
		comun.addPorcentajesClienteAplicados(porcentajesDescuentoLD, ld);
		
		xmlparam.porcentajesDescuentoLD = porcentajesDescuentoLD;
		
		//Si el producto es combinado, habra varias Lineadetallezonasesion y la tarifa ser?? la del producto (no es necesario pasarle el contenido)
		var idtipoventa = 4;		 
		var fechaMin = null;
		var idsesion = null;
		
		//Recorremos todas las sesiones para obtener la que tiene la fecha mas cercana	
		for each(var session in ld.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion) {
			
			var fechaString = session.fecha.text();

			if (fechaString.toString() != "") {
				var array = fechaString.split("-");
				var camposFecha = array[0].split("/");
				
				var fechaActual = new java.util.Date(camposFecha[2], camposFecha[1] - 1, camposFecha[0]);
				
				if (fechaMin == null || fechaActual.before(fechaMin)) {
					fechaMin = fechaActual;
					idsesion = session.idsesion;
				}
				else{
					//Si las sesiones tienen la misma fecha, nos quedamos con la de menor id
					if(fechaActual.equals(fechaMin) && (idsesion == null || idsesion > session.idsesion)){ 
						idsesion = session.idsesion;
					}
				}
			}
			
		}
	
		var perfilLD = new XML(comun.retornaPerfilVisitanteYDescuento(ld.producto.idproducto.text(), xmlparam.canal.idcanal.text(), xmlparam.cliente.idcliente.text(), idsesion, idtipoventa, null));
		
		
		var idcliente = xmlparam.cliente.idcliente.text().toString();
		if(isNaN(idcliente) || idcliente == "")
			idcliente = null;
		else
			idcliente = parseInt(idcliente);
		
		if (fechaMin != null) {
			var dia = "0" + fechaMin.getDate();
			var mes = "0" + (Number(fechaMin.getMonth()) + 1);
	
			fechaMin = dia.substr(dia.length - 2, 2) + "/" + mes.substr(mes.length - 2, 2) + "/" + fechaMin.getYear();
		}
			
		xmlLineadetalle = comun.establecerPerfilesEImportesLD(ld, perfilLD, idcliente, fechaMin, idtipoventa);
	}	
	
	//Almacenamos los descuentos de cliente que se han aplicado a la reserva
	var porcentajesDescuentoLDA = <porcentajesDescuentoLDA />;
	//Obtenemos los perfiles para poder modificarlos de cada linea de detalle anulada
	for each (lda in xmlparam.lineadetallesanuladas.Lineadetalle) {
		//Añado a la colección de los porcentajes de cliente aplicados a la reserva el porcentaje correspondiente a cada ld
		comun.addPorcentajesClienteAplicados(porcentajesDescuentoLDA, lda);
		xmlparam.porcentajesDescuentoLDA = porcentajesDescuentoLDA;
		
		
		//Si el producto es combinado, habrá varias Lineadetallezonasesion y la tarifa ser?? la del producto (no es necesario pasarle el contenido)
		var idsesion = null;
		var idtipoventa = 4;
		var fechaMin = null;
		
		//var importeld = comun.calculaImporteMostrarLD(lda, null);
		
		for each(var session in lda.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion) {
			
			var fechaString = session.fecha.text();	
				
			if (fechaString.toString() != "") {
				var array = fechaString.split("-");
				var camposFecha = array[0].split("/");
				
				var fechaActual = new java.util.Date(camposFecha[2], camposFecha[1] - 1, camposFecha[0]);
							
				if (fechaMin == null || fechaActual.before(fechaMin)) {
					fechaMin = fechaActual;
					idsesion = session.idsesion;
				}
				else{
					//Si las sesiones tienen la misma fecha, nos quedamos con la de menor id
					if(fechaActual.equals(fechaMin)){ 
						if(idsesion == null || idsesion > session.idsesion){
							idsesion = session.idsesion;
						}
					}
				}
			}
		}
		
		var perfilLDA = new XML(comun.retornaPerfilVisitanteYDescuento(lda.producto.idproducto.text(), xmlparam.canal.idcanal.text(), xmlparam.cliente.idcliente.text(), idsesion, idtipoventa, null));
			
		
		if(isNaN(idtipoventa) || idtipoventa == "")
			idtipoventa = null;
		else
			idtipoventa = parseInt(idtipoventa);
			
		var idcliente = xmlparam.cliente.idcliente.text().toString();
		if(isNaN(idcliente) || idcliente == "")
			idcliente = null;
		else
			idcliente = parseInt(idcliente);
		
		if (fechaMin != null) {
			var dia = "0" + fechaMin.getDate();
			var mes = "0" + (Number(fechaMin.getMonth()) + 1);
	
			fechaMin = dia.substr(dia.length - 2, 2) + "/" + mes.substr(mes.length - 2, 2) + "/" + fechaMin.getYear();
		}
			
		xmlLineadetalle = comun.establecerPerfilesEImportesLD(lda, perfilLDA, idcliente, fechaMin, idtipoventa);
	}
	return xmlparam;
	
}

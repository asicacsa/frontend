var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {	
	
	var methodpost = request.getParameter('servicio');		
	
	// si xml no se envia, llama al metodo sin parametros	
	var selecciona = null;
	var selecciona2 = null;
	//API 22 Febrero 2011 vamos a seguir el mismo rollo que hab�a e incluimos un nuevo atributo de tipo entero para detectar el caso en el que estamos. Por defecto asumiremos no numerado.
	var selecciona3 = 0;
	
	selecciona = request.getParameter('idTipoProducto');
	selecciona2 = request.getParameter('date');
	//API 22 Febrero 2011 vamos a seguir el mismo rollo que hab�a e incluimos un nuevo atributo de tipo entero para detectar el caso en el que estamos.
	var numerado = request.getParameter('numerado');
	if ( numerado != null && numerado != "" )
		selecciona3 = request.getParameter('numerado');
	
	if (log.isInfoEnabled()) {
		log.info("obtenerComun: Id para seleccionar " + selecciona);
		log.info("obtenerComun: Id para seleccionar " + selecciona2);
		log.info("obtenerComun: Id para seleccionar " + selecciona3);
	}
	
	var envio = <servicio></servicio>
	
	// En este tipo de servicio no van a haber parametros de Entrada.
	if(selecciona != null && selecciona2 != null){
		
		selecciona= selecciona.trim();
		selecciona2= selecciona2.trim();

		selecciona = new XML(selecciona);
		selecciona2 = new XML(selecciona2);
		//API 22 Febrero 2011 vamos a seguir el mismo rollo que hab�a e incluimos un nuevo atributo de tipo entero para detectar el caso en el que estamos.
		
		envio = <servicio>
					<parametro>
						<java.lang.Integer>{selecciona}</java.lang.Integer>
						<date>{selecciona2}</date>
						//API 22 Febrero 2011
						<java.lang.Integer>{selecciona3}</java.lang.Integer>
					</parametro>
				</servicio>
	}

	if (log.isInfoEnabled()) {
		//log.info("Metodo a llamar : "+methodpost);
		log.info("Envio : "+envio);
	}
	
	var result;

	try {
		result = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	
	
	if (log.isInfoEnabled()) {
		log.info(result);
	}
	
	if (result !=  null) {
		result= result.trim();
		result = new XML(result);
		
		ajustarDisponibilidad(result);
	} else {
		result = new XML();
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
	
}

meta.addMethod(
	'ajustarDisponibilidad',  // param1: nombre de la funcion
	'org.mozilla.javascript.xmlimpl.XML',
	'java.lang.Object'
);
function ajustarDisponibilidad(xml) {
	xml.sesiones = <sesiones />;
	
	var overbooking = 0;
		
	//Para cada sesion si tiene mas de una Zonasesion duplicamos la sesion
	for each (var s in xml.Sesion) {
		//Calculamos el overbooking real que queda en cada sesion para sumarselo a cada zona
		overbookingpendiente = Number(s.overbookingventa);
		
		//Para saber el overbooking real debemos de restar del overbooking las libres que sean negativas
		var overbookingVendido = <zonas />;
		
		overbookingVendido.zonas = s.zonasesions.Zonasesion.(numlibres < 0);

		for each (var zs in overbookingVendido.Zonasesion) {
			overbookingpendiente = overbookingpendiente + Number(zs.numlibres);
		}
		
		var sesion = s;
			
		for each (var zs in s.zonasesions.Zonasesion) {
			//Eliminamos todas las Zonasesion que tuviese
			delete sesion.zonasesions;
			
			//añadimos el overbooking
			if (Number(zs.numlibres) < 0) {
				zs.numlibres = 0;
			}
			
			zs.numlibres = Number(zs.numlibres) + overbookingpendiente;
			
			sesion.zonasesions.Zonasesion = zs;
			
			xml.sesiones.appendChild(sesion);
		}
		

		delete xml.Sesion[s.childIndex()];
	}
	
	return xml;
}

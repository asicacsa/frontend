function handle(request, response) {
	var lineadetalle = request.getParameter('lineadetalle');
	var nuevoProducto = request.getParameter('nuevoProducto');
	var idtipoventa = request.getParameter('idtipoventa');
	var idcliente = request.getParameter('idcliente');
	var idcanal = request.getParameter('idcanal');
	
	var result = new XML();
	
	//Comprobamos que tengamos los datos correctos
	if (lineadetalle == null || lineadetalle == "" || nuevoProducto == null || nuevoProducto == "") {
		throw new java.lang.Exception("Debe de especificar una linea de detalle y el nuevo producto seleccionado");
	}
	
	//Creamos los xml
	lineadetalle = new XML(lineadetalle);
	nuevoProducto = new XML(nuevoProducto);
	
	//Cogemos la primera linea de detalle que nos viene para obtener el esqueleto
	var ldVacia = new XML(lineadetalle.toXMLString());
	var idlineadetalle = ldVacia.idlineadetalle.text();
	
	//Vaciamos la linea de detalle para que nos quede solo con la estructura
	delete ldVacia.perfiles;
	delete ldVacia.lineadetallezonasesions.Lineadetallezonasesion;
	delete ldVacia.producto.*;
	ldVacia.idlineadetalle = "";
	//ldVacia.importeTarifaSeleccionada = "";
	//ldVacia.perfilvisitante.idperfilvisitante = "";
	//ldVacia.descuentopromocional.iddescuentopromocional = "";
	ldVacia.importe = "";
	
	var cantidadMin = 0;
	
	//Obtenemos la menor cantidad
	for each (cantidad in lineadetalle.cantidad) {
		if (cantidadMin == 0 || cantidad < cantidadMin) {
			cantidadMin = cantidad;
		}
	}
	
	ldVacia.cantidad = cantidadMin;
	
	//Ahora que ya tenemos el esqueleto vamos a rellenarlo
	
	//Empezamos poniendo el nuevo producto
	nuevoProducto.setName("producto");
	ldVacia.producto = nuevoProducto;
	
	//Ahora insertamos los perfiles
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('obtenerProductoPorIdVentaReserva.js');

	//Ahora insertamos las lineadetallezonasesions de cada tipo de producto que hay en el nuevo producto con
	// los datos de las lineas de detalle que nos pasan.
	for each (tpp in nuevoProducto.tipoprodproductos.Tipoprodproducto) {
		var ldzs = lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.(zonasesion.sesion.contenido.tipoproducto.idtipoproducto==tpp.tipoproducto.idtipoproducto);
		
		if (ldzs.toXMLString() == "") {
			ldzs = <Lineadetallezonasesion />;
			ldzs.zonasesion.sesion.horainicio = "";
			ldzs.zonasesion.sesion.fecha = "";
			ldzs.zonasesion.numlibres = "";
			ldzs.zonasesion.sesion.contenido.tipoproducto = tpp.tipoproducto;
		}else{
			//se elimina el idlineadetallezonasesion pq se tiene que crear otro nuevo
			delete ldzs.idlineadetallezonasesion;
		}
		
		ldVacia.lineadetallezonasesions.appendChild(ldzs);
	}
	
	
	var fechaMin = null;
	var idsesion = null;
	
	//Recorremos todas las sesiones para obtener la que tiene la fecha mas cercana	
	for each(var session in ldVacia.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion) {
			
		var fechaString = session.fecha.text();
		//@TODO_JS: Copiar esto en todos los js
		if (fechaString.toString() != "") {
			var array = fechaString.split("-");
			var camposFecha = array[0].split("/");
			
			var fechaActual = new java.util.Date(camposFecha[2], camposFecha[1] - 1, camposFecha[0]);
						
			if (fechaMin == null || fechaActual.before(fechaMin)) {
				fechaMin = fechaActual;
				idsesion = session.idsesion;
			}
			else{
				//Si las sesiones tienen la misma fecha, nos quedamos con la de menor id
				if(fechaActual.equals(fechaMin) && (idsesion == null || idsesion > session.idsesion)){ 
					idsesion = session.idsesion;
				}
			}
		}
	}	
	
	var perfilLD = new XML(comun.retornaPerfilVisitanteYDescuento(nuevoProducto.idproducto.text(), idcanal, idcliente, idsesion, idtipoventa, null));
	
	if(isNaN(idtipoventa) || idtipoventa == "")
		idtipoventa = null;
	else
		idtipoventa = parseInt(idtipoventa);
		
	if(isNaN(idcliente) || idcliente == "")
		idcliente = null;
	else
		idcliente = parseInt(idcliente);
		
	if (fechaMin != null) {
		var dia = "0" + fechaMin.getDate();
		var mes = "0" + (Number(fechaMin.getMonth()) + 1);

		fechaMin = dia.substr(dia.length - 2, 2) + "/" + mes.substr(mes.length - 2, 2) + "/" + fechaMin.getYear();
	}	
	ldVacia = comun.establecerPerfilesEImportesLD(ldVacia, perfilLD, idcliente, fechaMin, idtipoventa);
	
	
	result = ldVacia;
		
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
	
}
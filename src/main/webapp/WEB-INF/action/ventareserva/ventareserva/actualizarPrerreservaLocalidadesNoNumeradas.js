var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost = request.getParameter('servicio');
	var envio;
	
	
	if (xml!=null) {
		xml = new XML(xml);

		
		var xmlSinHijos;
		var sbf = context.beans.getBean('scriptBeanFactory');
		var comun = sbf.getBean('transformerXML.js');		
		var eliminarBasuraLD = sbf.getBean('eliminarDatosInvalidosLineaDetalle.js');
		
		xml = eliminarBasuraLD.eliminarDatosInvalidosLineaDetalle(xml);
		//Elimino las etiquetas que contienen la información sobre los descuentos del cliente
		delete xml..porcentajesDescuentoLD;
		delete xml..porcentajesDescuentoLDA;
		
		xmlSinHijos = comun.borraElementosSinHijos(xml);
		
		envio = 
			<servicio>
				<parametro>
					{xmlSinHijos}
				</parametro>
			</servicio>
				
		
		if (log.isInfoEnabled()) {
			log.info("Este es el envio : "+envio);
			//log.info("Este es el metodo post : "+methodpost);
		}
		
		var aux = null;
		
		//Y llamamos al servicio...
		try {
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			if (log.isErrorEnabled()) {	
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				log.error('LOG(ERROR) : con esta llamada : ' + envio);
				log.error('LOG(ERROR) : result in this Exception : '+ex);
			}
			
			throw ex.javaException;
		}
		
		resultado = new XML(aux);
				
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a actualizarPrerreservaLocalidadesNoNumeradas es: " + xml);
		}
		throw new java.lang.Exception("Peticion incorrecta.");
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
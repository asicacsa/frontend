var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {

	var methodpost = 'actualizarReserva';
	var result;

	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}

	// si xml no se envia, llama al metodo sin parametros	
	var xml = request.getParameter('xml');

	if(xml == null ) {
		xml = '<servicio />';
	} else {
		
		var xmlTratatado = preProceso(xml);

		xml = <servicio>
				<parametro>					
					{xmlTratatado}					
				</parametro>
			  </servicio>
	}
	if (log.isInfoEnabled()) {
		log.info('	datos pasados al servicio: ' + xml.toXMLString());
	}
	
	try{
		result = services.process(methodpost+'SOAWrapper',xml);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	if (log.isInfoEnabled()) {
		log.info('	resultado del servicio: ' + result);
	}
	
	if (result == null) {
		result = "<ArrayList />";
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	
	response.writer.println(result);
}

function preProceso(xmlparam) {
	var xmlSinHijos;
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('transformerXML.js');
	
	xmlSinHijos = comun.borraElementosSinHijos(xmlparam);

	delete xmlSinHijos..perfiles;	
	delete xmlSinHijos..importeTarifaSeleccionada;
	delete xmlSinHijos..porcentajedesc;
	delete xmlSinHijos..porcentajesDescuentoLD;
	delete xmlSinHijos..porcentajesDescuentoLDA;
	
	return xmlSinHijos;
}

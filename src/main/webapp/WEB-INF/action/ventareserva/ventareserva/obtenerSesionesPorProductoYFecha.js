var services = context.beans.getBean('httpServiceSOA');
		
function handle(request, response) {
	
	var methodpost = request.getParameter('servicio');
	
	var idProducto = request.getParameter('idproducto');
	var fecha = request.getParameter('fecha');
	var map = new XML(request.getParameter('map'));
	
	var xml = null;
	
	if(idProducto!= null && fecha != null && map != null) {		
		envio = <servicio>
					<parametro>
						<java.lang.Integer>{idProducto}</java.lang.Integer>
						<date>{fecha}</date>
						{map}
					</parametro>
				</servicio>
				
		xml = services.process(methodpost+'SOAWrapper',envio);
	}
	
	if (xml!=null) xml= xml.trim();
	
	xml = new XML(xml);
	
	if (log.isInfoEnabled()){
		log.info('obtenerSesionesPorProductoYFecha, retorno servicio: ' + xml.toXMLString());
	}
	
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comunDisponibilidad = sbf.getBean('obtenerSesionesPorTipoProductoYFecha.js');
	
	//Para cada sesion si tiene mas de una Zonasesion duplicamos la sesion
	for each (var al in xml.ArrayList) {
		comunDisponibilidad.ajustarDisponibilidad(al);
	}	
	
	if (log.isInfoEnabled()){
		log.info('obtenerSesionesPorProductoYFecha, resultado final: ' + xml.toXMLString());
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(xml.toXMLString());
}
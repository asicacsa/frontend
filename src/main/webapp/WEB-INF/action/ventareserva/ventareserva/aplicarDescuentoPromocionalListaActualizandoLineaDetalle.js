var services = context.beans.getBean('httpServiceSOA');
//var servicio_alta ='realizarVentaAbonos';
//var servicio_edicion ='actualizarAbonos';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
	
	if (log.isInfoEnabled()) {
		log.info("Entrada a aplicarDescuentoPromocionalListaActualizandoLineaDetalle.action");
	}

	var resultado;
	var methodpost = 'aplicarDescuentoPromocionalLista';
	var envio;

	var lalinea = request.getParameter('lalinea');
	var idtipoventa = request.getParameter('idtipoventa');

	//MICG
	var idcliente = request.getParameter('idcliente');

	//var clienteventa = request.getParameter('clienteventa');

	lalinea = new XML(lalinea.trim());
	
	if (log.isDebugEnabled()) {
		log.debug("aplicarDescuentoPromocionalListaActualizandoLineaDetalle.action, input XML: " + lalinea.toXMLString());
	}

	var eliminarBasuraLD = sbf.getBean('eliminarDatosInvalidosLineaDetalle.js');

	lalinea = eliminarBasuraLD.eliminarDatosInvalidosLineaDetalleDeUnaLinea(lalinea);

	var comun = sbf.getBean('transformerXML.js');

	lalinea = comun.borraElementosSinHijos(lalinea);

	var idestadolocalidadDeLaLinea = lalinea.estadolocalidads.Estadolocalidad.idestadolocalidad.text();

	//API 23 Abril 2012 Metiendo nulos por la puerta grande !!! Nunca se puede meter un valor a ciegas sin saber si previamente es nulo o no. Pues de lo contrario nos encontraremos con problemas de parseo del xml.
	//if ( idcliente != null && idcliente != "" ) {
	if ( idcliente.isNaN || idcliente == null || idcliente.length == 0 || idcliente == "null" ) {
		var xml = <dto>
			<lineadetalles>
				{lalinea}
			</lineadetalles>
			<idtipoventa>{idtipoventa}</idtipoventa>
		</dto>;
	} else {
		//MICG Incluyo el idcliente
		var xml = <dto>
			<lineadetalles>
				{lalinea}
			</lineadetalles>
			<idtipoventa>{idtipoventa}</idtipoventa>
			<idcliente>{idcliente}</idcliente>
		</dto>;
	}

	xml = this.preProcessXML(xml);

	envio = 
		<servicio>
			<parametro>
				{xml}
			</parametro>
		</servicio>

	if (log.isDebugEnabled()) {
		log.debug("aplicarDescuentoPromocionalListaActualizandoLineaDetalle, envio: " + envio);
	}

	//Y llamamos al servicio...
	var aux = null;

	try {
		aux = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : ' + this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper');
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			log.error('LOG(ERROR) : con esta descripcion : '+ ex.javaException.getMessage());
			log.error('LOG(ERROR) : con esta llamada : '+ envio);
		}
		throw ex.javaException;
	}

	if (log.isDebugEnabled()) {
		log.debug("aplicarDescuentoPromocionalListaActualizandoLineaDetalle, respuesta servicio: " + aux);
	}


	//Este seervicio nunca devuelve null, en todo caso un Array vacio
	aux = new XML(aux.trim());

	if(aux.hasOwnProperty("Lineadetalle")){
		resultado = aux.Lineadetalle;
	} else {
		resultado = new XML();
		if (log.isInfoEnabled()) {
			log.info("aplicarDescuentoPromocionalListaActualizandoLineaDetalle, resultado null.");
		}
	}


	if(resultado.tarifaproducto.importe.toString() == ""){
		resultado.importe= <importe>"--"</importe>;
	}

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	param = new XML(param);
	//Eliminamos las lineas de detalle gratuitas antiguas
	//delete param..lineadetallegratuitas;

	//Eliminamos los datos que son de solo lectura
	delete param..detalles;
	delete param..sesion.nombre;
	if(param.lineadetalles.Lineadetalle.importe.text() == "--"){
		param.lineadetalles.Lineadetalle.importe = "0.00";
	}

	if(param.lineadetalles.Lineadetalle.tarifaproducto.importe.text() == "--"){
		param.lineadetalles.Lineadetalle.tarifaproducto.importe = "0.00";
	}

	param = comun.borraElementosSinHijos(param.toXMLString());
	return param;
}
meta.addMethod(
	'eliminarDatosInvalidosLineaDetalle', // param1: nombre de la funcion
	'Object', // El xml con las lineas de detalle que debo modificar
	'Object'
);

function eliminarDatosInvalidosLineaDetalle(xmlparam) {
	delete xmlparam..Lineadetalle.perfiles;
	delete xmlparam..Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.producto;
	delete xmlparam..Lineadetalle.importeTarifaSeleccionada;
	delete xmlparam..Lineadetalle..tarifaproductos;
	delete xmlparam..Lineadetalle..tarifaproducto;
	delete xmlparam..Lineadetalle..productoscanals;
	delete xmlparam..Lineadetalle..porcentajedesc;
	
	delete xmlparam..Lineadetallegratuita.perfiles;
	delete xmlparam..Lineadetallegratuita.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.producto;
	delete xmlparam..Lineadetallegratuita.importeTarifaSeleccionada;
	delete xmlparam..Lineadetallegratuita..tarifaproductos;
	delete xmlparam..Lineadetallegratuita..tarifaproducto;
	delete xmlparam..Lineadetallegratuita..productoscanals;
	delete xmlparam..Lineadetallegratuita..porcentajedesc;
	//GGL 04.08.2015 AVANQUA
	delete xmlparam..Lineadetalle.lineadetalleivaaplicados;
	delete xmlparam..lineadetalle.lineadetalleivaaplicados;

	return xmlparam;
}

//GGL 19102011
meta.addMethod(
		'eliminarDatosInvalidosLineaDetallePases', // param1: nombre de la funcion
		'Object', // El xml con las lineas de detalle que debo modificar
		'Object'
);

function eliminarDatosInvalidosLineaDetallePases(xmlparam) {
	delete xmlparam..LineadetalleVentaPasesDTO.perfiles;
	delete xmlparam..LineadetalleVentaPasesDTO.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.producto;
	delete xmlparam..LineadetalleVentaPasesDTO.importeTarifaSeleccionada;
	delete xmlparam..LineadetalleVentaPasesDTO..tarifaproductos;
	delete xmlparam..LineadetalleVentaPasesDTO..tarifaproducto;
	delete xmlparam..LineadetalleVentaPasesDTO..productoscanals;
	delete xmlparam..LineadetalleVentaPasesDTO..porcentajedesc;
	delete xmlparam..LineadetalleVentaPasesDTO..producto.tipoprodproductos;
	delete xmlparam..LineadetalleVentaPasesDTO..producto.fechainiciovigencia;
	delete xmlparam..LineadetalleVentaPasesDTO..producto.descripcion;
	delete xmlparam..LineadetalleVentaPasesDTO..producto.fechainicioventa;
	delete xmlparam..LineadetalleVentaPasesDTO..producto.iva;
	delete xmlparam..LineadetalleVentaPasesDTO..producto.disponibleparaabono;
	delete xmlparam..LineadetalleVentaPasesDTO..producto.disponibleparabono;
	delete xmlparam..LineadetalleVentaPasesDTO..producto.dadodebaja;
	delete xmlparam..LineadetalleVentaPasesDTO..producto.imprimirEntradasXDefecto;
	delete xmlparam..LineadetalleVentaPasesDTO..producto.principal;
	delete xmlparam..LineadetalleVentaPasesDTO..producto.nombre;
	delete xmlparam..LineadetalleVentaPasesDTO..importedescuentocliente;
	delete xmlparam..LineadetalleVentaPasesDTO..perfilvisitante.nombre;
	//GGL 06032012 Lo comento para que lleguen estos datos en la venta
	//delete xmlparam.nombre;
	//delete xmlparam.telefono;
	//delete xmlparam.nif;
	//delete xmlparam.cp;
	//delete xmlparam.email;
	//delete xmlparam.telefonomovil;
	
	
	//delete xmlparam..Lineadetallegratuita.perfiles;
	//delete xmlparam..Lineadetallegratuita.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.producto;
	//delete xmlparam..Lineadetallegratuita.importeTarifaSeleccionada;
	//delete xmlparam..Lineadetallegratuita..tarifaproductos;
	//delete xmlparam..Lineadetallegratuita..tarifaproducto;
	//delete xmlparam..Lineadetallegratuita..productoscanals;
	//delete xmlparam..Lineadetallegratuita..porcentajedesc;

	return xmlparam;
}

meta.addMethod(
	'eliminarDatosInvalidosLineaDetalleDeUnaLinea', // param1: nombre de la funcion
	'Object', // El xml con las lineas de detalle que debo modificar
	'Object'
);
function eliminarDatosInvalidosLineaDetalleDeUnaLinea(xmlparam) {
	delete xmlparam..perfiles;
	delete xmlparam..lineadetallezonasesions.Lineadetallezonasesion.zonasesion.producto;
	delete xmlparam..importeTarifaSeleccionada;
	delete xmlparam..tarifaproductos;
	delete xmlparam..tarifaproducto;
	delete xmlparam..productoscanals;
	delete xmlparam..porcentajedesc;
	//GGL 04.08.2015 AVANQUA
	delete xmlparam..lineadetalleivaaplicados;

	for each(i in xmlparam..descuentopromocional){
		if (i.hasOwnProperty('iddescuentopromocional') && i.iddescuentopromocional.toString() == "" ){
			delete i.parent().*[i.childIndex()];
		}
	}
	return xmlparam;
}

//GGL 02092011
meta.addMethod(
		'eliminarDatosInvalidosLineaDetalleDeUnaLineaPases', // param1: nombre de la funcion
		'Object', // El xml con las lineas de detalle que debo modificar
		'Object'
	);
function eliminarDatosInvalidosLineaDetalleDeUnaLineaPases(xmlparam) {
	delete xmlparam..perfiles;
	//delete xmlparam..lineadetallezonasesions.Lineadetallezonasesion.zonasesion.producto;
	//delete xmlparam..importeTarifaSeleccionada;
	delete xmlparam..tarifaproductos;
	//GGL delete xmlparam..tarifaproducto;
	delete xmlparam..productoscanals;
	delete xmlparam..porcentajedesc;

	for each(i in xmlparam..descuentopromocional){
		if (i.hasOwnProperty('iddescuentopromocional') && i.iddescuentopromocional.toString() == "" ){
			delete i.parent().*[i.childIndex()];
		}
	}
	return xmlparam;
}
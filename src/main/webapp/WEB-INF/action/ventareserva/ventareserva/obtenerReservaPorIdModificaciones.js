var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost = 'obtenerReservaPorId';
	var envio;
	
	//log.info("Entra en el action de obtenerVentaPorIdReducido.");
	
	if (xml!=null) {
		xml = new XML(xml);
				
	
		envio = 
			<servicio>
				<parametro>
					{xml}
				</parametro>
			</servicio>			
		
		
		//log.info("Este es el envio : "+envio);
		//log.info("Este es el metodo post : "+methodpost);
		
		//Y llamamos al servicio...
		var aux;
		
		try{
			aux = services.process(methodpost+'SOAWrapper',envio).trim();
		} catch (ex) {
			if (log.isErrorEnabled()) {				
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
				log.error('LOG(ERROR) : con este tipo de error : ' + ex);				
				log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
			}
		
			throw ex.javaException;
		}
				
		if(aux != null){
		//log.info('aux es distinto de nulo');
			aux = new XML(aux);
			//resultado = aux;
			resultado = this.postProcessXML(aux);
			
			//log.info("Este es el resultado despues del POST proceso obtenerVentaPorIdReducido: "+resultado.toXMLString());			
		}			
		
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		//resultado = new XML(aux);
					
	} else {
		//log.info("El xml que llega a obtenerVentaPorIdReducido es nulo!!!");
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function postProcessXML(xmlparam) {
	
	//log.info("Esto es lo que me llega al post Proceso obtenerVentaPorIdReducido: "+xmlparam.toXMLString());
	
	if (xmlparam != null){
		xmlparam = new XML(xmlparam);								
	}
	
	//log.info("Lo que envio a Laszlo......: "+xmlparam);
	
	return xmlparam;
}
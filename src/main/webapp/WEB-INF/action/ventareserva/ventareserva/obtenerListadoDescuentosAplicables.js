var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {	
	
	if (log.isInfoEnabled()) {
		log.info("Entrada a obtenerListadoDescuentosAplicables.");
	}
	
	var methodpost = request.getParameter('servicio');		
	
	var selecciona = request.getParameter('idcanal');
	var selecciona2 = request.getParameter('idproducto');
	var selecciona3 = request.getParameter('idtarifa');
	
	if (log.isInfoEnabled()) {
		log.info("obtenerListadoDescuentosAplicables, idcanal:  " + selecciona);
		log.info("obtenerListadoDescuentosAplicables, idproducto:  " + selecciona2);
		log.info("obtenerListadoDescuentosAplicables, idtarifa:  " + selecciona3);
	}
	
	var envio;
	var result;
	
	if(selecciona != null && selecciona2 != null && selecciona3 != null){
		
		selecciona = new XML(selecciona);
		selecciona2 = new XML(selecciona2);
		selecciona3 = new XML(selecciona3);
		
		envio = <servicio>
					<parametro>
						<tarifa>
							<idtarifa>{selecciona3}</idtarifa>
						</tarifa>						
						<producto>
							<idproducto>{selecciona2}</idproducto>
						</producto>
						<canal>
							<idcanal>{selecciona}</idcanal>
						</canal>
					</parametro>
				</servicio>
				
				
		envio = new XML(envio);
		envio = preProcessXML(envio);
		
		if (log.isInfoEnabled()) {
			log.info("obtenerListadoDescuentosAplicables, envio : " + envio);
		}
		
		
		
		try {
		
			result = services.process(methodpost+'SOAWrapper',envio);
			
		} catch (ex) {
			if (log.isErrorEnabled()) {
				log.error('LOG(ERROR) : Error in this call : ' + this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper');
				log.error('LOG(ERROR) : result in this Exception : '+ex);
				log.error('LOG(ERROR) : con esta descripcion : '+ ex.javaException.getMessage());
				log.error('LOG(ERROR) : con esta lammada : '+ envio);
			}
			
			throw ex.javaException;
		}
		
	} else {
		if (log.isInfoEnabled()) {
			log.error("obtenerListadoDescuentosAplicables, alguno de los parametros es nulo!");
		}
		result = '<error/>';
	}
	
	
	if (log.isInfoEnabled()) {
		log.info("obtenerListadoDescuentosAplicables, result : " + result);
	}
	
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result);
	
}



function preProcessXML(param) {
	var xmlSinHijos= comun.borraElementosSinHijos(param);

	return xmlSinHijos;
}
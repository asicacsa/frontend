importClass(java.util.concurrent.locks.ReentrantLock);
var services = context.beans.getBean('httpServiceSOA');
//var lineaDetalle = '';
var methodpostModel = 'getXMLModel';
var methodPerfilVisitante = 'obtenerPerfilesAplicablesConUnaTarifaProducto';
var methodSelectDefaultSesion = 'obtenerSesionDefectoPorProducto';
var cantidad=1;
this.lock = new ReentrantLock();


function handle(request, response) {
	try {
		this.lock.lock();
		this._handle(request, response);
	} finally {
		this.lock.unlock();
	}
	
}
	

function _handle(request, response) {
	
	if (log.isInfoEnabled()){
		log.info("Entrada a obtenerProductoPorIdVentaReserva.action, lock: " + this.lock.toString());
	}

	var methodpost = 'obtenerProductoPorId';	
	var methodpostSesiones = 'obtenerSesionesPorProductoYFecha';

	var selecciona = null;
	var selecciona3 = null;
	var selecciona4 = null;
	
	//cantidad de entradas
	cantidad = request.getParameter('cantidad');

	if (cantidad != null && cantidad != "null" && cantidad != "" && !isNaN(cantidad) && parseInt(cantidad)>0 ) {
		cantidad = parseInt(cantidad);
	}else{
		cantidad = 1;
	}
	
	
	var idProducto = request.getParameter('idproducto');
	var idCanal = request.getParameter('idcanal');
	var idcliente = request.getParameter('idcliente');
	var idtipoventa = request.getParameter('idtipoventa');
	var idLineadetalle = request.getParameter('idLineadetalle');	
	var isLdGratuita = request.getParameter('isLdGratuita');
	var autosesion = request.getParameter('autosesion');
	
	var lineaDetalle = request.getParameter('tipoLineadetalle');
	
	var idzona = null;
	var idcontenido = null;
	var idsesion = null;
	var sesion = null;
	
	if (autosesion=="true"){
		// seleccionamos sesion por defecto
		sesion = <sesiones>{retornaSesionPorDefecto(idProducto)}</sesiones>;
	} else {
		sesion = request.getParameter('sesion');
		if (sesion!=null) sesion=sesion.trim();
		sesion = new XML(sesion);
	}
	

	if (log.isInfoEnabled()) {
		var msg = "obtenerProductoPorIdVentaReserva: " + " idProducto: " + idProducto + "\n";
		msg = msg + " idCanal: " + idCanal + "\n";
		msg = msg + " idcliente: " + idcliente + "\n";
		msg = msg + " idtipoventa: " + idtipoventa + "\n";
		msg = msg + " idLineadetalle: " + idLineadetalle + "\n";
		msg = msg + " isLdGratuita: " + isLdGratuita + "\n";
		msg = msg + " autosesion: " + autosesion + "\n";
		msg = msg + " sesion: " + sesion + "\n";
		msg = msg + " tipoLineaDetalle: " + lineaDetalle + "\n";
		
		log.info(msg);
	}
	
	var i = 0;		
	var elementoAEliminar = 0;
	
	
	if (idcliente != null && idcliente != "null" && idcliente != "" && !isNaN(idcliente)) {
		idcliente = parseInt(idcliente);
	} else {
		idcliente = null;
	}
	
	if (idtipoventa != null && idtipoventa != "null" && idtipoventa != "" && !isNaN(idtipoventa)) {
		idtipoventa = parseInt(idtipoventa);
	} else {
		idtipoventa = null;
	}
	
	//Obtenemos la lineadetalle pasandole el tipoLineadetalle
	xmlLineadetalle = this.retornaLineadetalle(lineaDetalle);//la Lineadetalle

	var fechaMin = null;

	//Recorremos todas las sesiones para obtener la que tiene la fecha mas cercana	
	for each(var session in sesion.Sesion) {
		var fechaString = session.fecha.text();
		if (fechaString.toString() != "") {
			var array = fechaString.split("-");
			var camposFecha = array[0].split("/");
			
			var fechaActual = new java.util.Date(camposFecha[2], camposFecha[1] - 1, camposFecha[0]);
			
			if (fechaMin == null || fechaActual.before(fechaMin)) {
				fechaMin = fechaActual;
				
				idsesion = session.idsesion;
				idzona = session.zonasesions.Zonasesion.zona.idzona;
				idcontenido = session.contenido.idcontenido;
			}
			else{
				//Si las sesiones tienen la misma fecha, nos quedamos con la de menor idsesion
				if(fechaActual.equals(fechaMin) && (idsesion == null || idsesion > session.idsesion)){ 
					idsesion = session.idsesion;
					idzona = session.zonasesions.Zonasesion.zona.idzona;
					idcontenido = session.contenido.idcontenido;
				}
			}
		}
	}
	
	//Obtenemos el producto que nos pasan
	var productoLD = this.retornaProducto(idProducto);
	
	if (log.isDebugEnabled()) {
		log.debug("obtenerProductoPorIdVentaReserva, resultado obtenerProductoPorId: " + productoLD);
	}
	
	xmlLineadetalle.producto = productoLD;
	
	//Recorremos las sesiones que nos llegan
	for each(var sesionActual in sesion.Sesion) {		
		elementoAEliminar = 0;
		
		//Obtenemos el numero de sesiones que hay
		i = sesionActual.contenido.tipoproducto.tipoprodproductos.Tipoprodproducto.length();
		
		//Nos quedamos con la sesion que le corresponde a este producto
		while (i > 1) {
			if (sesionActual.contenido.tipoproducto.tipoprodproductos.Tipoprodproducto[elementoAEliminar].producto.idproducto.text() != idProducto) {
				delete sesionActual.contenido.tipoproducto.tipoprodproductos.Tipoprodproducto[elementoAEliminar];
			} else {
				//Si el elemento al que accecemos es el que tenemos que eliminar debemos consultar los elementos siguientes
				elementoAEliminar ++;
			}
			
			i = sesionActual.contenido.tipoproducto.tipoprodproductos.Tipoprodproducto.length();
		}
		
		var zonasesions = sesionActual.zonasesions;
		
		var misesion = this.retornaSesionSinZonasesions(sesionActual);
		misesion.setName('sesion');
		
		var zonasesion = new XML();
		var ldzs = new XML();
			
		for each(var zs in zonasesions.Zonasesion) {
			ldzs = <Lineadetallezonasesion/>;
			zs.setName('zonasesion');
			zs.appendChild(misesion);
			zs.appendChild(sesionActual.contenido.tipoproducto.tipoprodproductos.Tipoprodproducto.producto);
			ldzs.appendChild(zs);
			ldzs.appendChild(<presentacionsesion><idpresentacionsesion/></presentacionsesion>);
			xmlLineadetalle.lineadetallezonasesions.appendChild(ldzs);
		}
		
		
		xmlLineadetalle.producto = sesionActual.contenido.tipoproducto.tipoprodproductos.Tipoprodproducto.producto;//agrego el Producto a la Lineadetalle
		delete sesionActual.contenido.tipoproducto.tipoprodproductos.Tipoprodproducto.producto;
		
		xmlLineadetalle.producto.tipoprodproductos.Tipoprodproducto = sesionActual.contenido.tipoproducto.tipoprodproductos.Tipoprodproducto;
		delete sesionActual.contenido.tipoproducto.tipoprodproductos;
		xmlLineadetalle.producto.tipoprodproductos.Tipoprodproducto.tipoproducto = sesionActual.contenido.tipoproducto;	
	}
	
	
	//Obtenemos los perfiles de visitante para el producto, canal, zona, cliente, contenido, sesion, tipo de venta y idlinedetalle(null) que tenemos	
	if(isLdGratuita == "false"){
		var perfilLD = retornaPerfilVisitanteYDescuento(idProducto, idCanal, idcliente, idsesion, idtipoventa, idLineadetalle);
			
		if(isNaN(idtipoventa) || idtipoventa == "")
			idtipoventa = null;
		else
			idtipoventa = parseInt(idtipoventa);
		
		if(isNaN(idcliente) || idcliente == "")
			idcliente = null;
		else
			idcliente = parseInt(idcliente);
		
		if (fechaMin != null) {
			var dia = "0" + fechaMin.getDate();
			var mes = "0" + (Number(fechaMin.getMonth()) + 1);
	
			fechaMin = dia.substr(dia.length - 2, 2) + "/" + mes.substr(mes.length - 2, 2) + "/" + fechaMin.getYear();
		}
				
		xmlLineadetalle = establecerPerfilesEImportesLD(xmlLineadetalle, perfilLD, idcliente, fechaMin, idtipoventa);
		
		if (log.isInfoEnabled()) {
			log.info("obtenerProductoPorIdVentaReserva, resultado final: " + xmlLineadetalle.toXMLString());
		}
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(xmlLineadetalle.toXMLString());
}


function retornaSesionSinZonasesions(arg){
	
	var lasesion = arg;
	delete lasesion.zonasesions
	
	return lasesion;
	
}


function retornaLineadetalle(lineaDetalle){

	var result2;
			
	var envio = <servicio>
					<parametro>
						<{lineaDetalle}/>
					</parametro>
				</servicio>

	var methodpost = methodpostModel;
	
	try {
		result2 = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		if (log.isErrorEnabled()) {	
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
		}
		
		throw ex.javaException;
	}
	
	result2 = new XML(result2.trim());
	
	//Anyadimos la cantidad ya que es requerida y asi podremos calcular el importe segun la tarifa seleccionada
	result2.cantidad = cantidad;
	
	//Ponemos la seleccion por defecto del perfil de visitante
	//result2.perfilvisitante.idperfilvisitante = 1;
				
	return result2;
}

meta.addMethod(
	'retornaPerfilVisitanteYDescuento',  // param1: nombre de la funcion
	'java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String',
	'java.lang.Object'
);
function retornaPerfilVisitanteYDescuento(idProducto, idCanal, idcliente, idsesion, idtipoventa, idlineadetalle){
	var perfilesydescuento;
	var envio = <servicio />;
		
	if (idProducto != null && !isNaN(idProducto)) {
		envio.parametro.listaDto.ObtenerPerfilesy1Tarifaparam.idproducto = <idproducto>{parseInt(idProducto)}</idproducto>;
	}
	
	if (idtipoventa != null && !isNaN(idtipoventa)) {
		envio.parametro.listaDto.ObtenerPerfilesy1Tarifaparam.idtipoventa = <idtipoventa>{parseInt(idtipoventa)}</idtipoventa>;
	} else {
		throw new java.lang.Exception("El tipo de venta es requerido.");
	}
	
	//La sesión es obligatoria para tipos de ventas que no sean de bonos.
	if (idsesion != null && !isNaN(parseInt(idsesion))) {
		envio.parametro.listaDto.ObtenerPerfilesy1Tarifaparam.idsesion = <idsesion>{parseInt(idsesion)}</idsesion>;
	}
	
	if (idcliente != null && !isNaN(parseInt(idcliente))) {
		envio.parametro.listaDto.ObtenerPerfilesy1Tarifaparam.idcliente = <idcliente>{parseInt(idcliente)}</idcliente>;
	}	
	
	if (idCanal != null && !isNaN(parseInt(idCanal))) {
		envio.parametro.listaDto.ObtenerPerfilesy1Tarifaparam.idcanal = <idcanal>{parseInt(idCanal)}</idcanal>;
	}
	
	if (idlineadetalle != null && !isNaN(parseInt(idlineadetalle))) {
		envio.parametro.listaDto.ObtenerPerfilesy1Tarifaparam.idlineadetalle = <idlineadetalle>{parseInt(idlineadetalle)}</idlineadetalle>;
	}		
				
	var methodpost = methodPerfilVisitante;
	
	if (log.isDebugEnabled()) {
		log.debug("obtenerProductoPorIdVentaReserva.retornaPerfilVisitanteYDescuento, envio a " + methodpost + ": " + envio);
	}
	
	if(idsesion == null && idtipoventa != 2){
		perfilesydescuento = 	<ArrayList>
										<PerfilesYDescuento>
											<descuento>
												<porcentajedescuento>0</porcentajedescuento>
											</descuento>
											<perfiles />
										</PerfilesYDescuento>
								</ArrayList>;
	}
	else{
		try {
			perfilesydescuento = new XML(services.process(methodpost+'SOAWrapper',envio).trim());
		} catch (ex) {
			if (log.isErrorEnabled()) {
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : ');
				log.error('LOG(ERROR) : result in this Exception : '+ex);
			}
			
			throw ex.javaException;
		}
	}
	
	//El servicio devuelve un Array de Arrays, pero en este caso solo nos puede devolver una serie de perfiles de visitante
	if (perfilesydescuento.hasOwnProperty("PerfilesYDescuento")) {
		perfilesydescuento = perfilesydescuento.PerfilesYDescuento;
	}
	
	if (log.isDebugEnabled()) {
		log.debug("obtenerProductoPorIdVentaReserva.retornaPerfilVisitanteYDescuento, resultado de " + methodpost + ": " + perfilesydescuento.toXMLString());
	}

	perfilesydescuento.setName("perfilesydescuento");
	
	if (log.isDebugEnabled()) {
		log.debug("obtenerProductoPorIdVentaReserva.retornaPerfilVisitanteYDescuento, resultado final: " + perfilesydescuento.toXMLString());
	}
	return perfilesydescuento;
}



function retornaProducto(idProducto){	
	
	var envio = <servicio>
					<parametro>
						<int>{idProducto}</int>
					</parametro>
				</servicio>

	var methodpost = "obtenerProductoPorId";
	
	var result2;
	
	try {
		result2 = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		if (log.isErrorEnabled()) {	
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
		}
		
		throw ex.javaException;
	}
	
	
	result2 = new XML(result2.trim());
	
	result2.setName("producto");
	
	return result2;
}



function retornaSesionPorDefecto(idProducto){		
	var envio = <servicio>
					<parametro>
						<int>{idProducto}</int>
					</parametro>
				</servicio>
	
	var sesion;
	
	try {
		sesion = services.process(methodSelectDefaultSesion + 'SOAWrapper', envio);
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR): Error in retornaSesionPorDefecto accessing ' + methodSelectDefaultSesion);
			log.error('sending  ' + envio);
			log.error('exception  ' + ex);
		}
		
		throw ex.javaException;
	}
	
	
	sesion = new XML(sesion.trim());	
	return sesion;
}



meta.addMethod(
	'addPorcentajesClienteAplicados',  // param1: nombre de la funcion
	'org.mozilla.javascript.xmlimpl.XML, org.mozilla.javascript.xmlimpl.XML',
	'java.lang.Object'
);
function addPorcentajesClienteAplicados(porcentajesDescuentoLD, ld){
	//Añado a la línea de detalle el porcentaje de cliente aplicado
	//ld.porcentajedesc = list_descuentos.DatosEconomicosLD.(idlineadetalle == ld.idlineadetalle).porcentajedesc;
	
	var porcentajeRepe = porcentajesDescuentoLD.(porcentajedescuento == ld.descuento.porcentajedescuento);
	if(porcentajeRepe.toXMLString() == ""){
		porcentajesDescuentoLD.appendChild(ld.descuento.porcentajedescuento);
	}
	
	return; 
}


meta.addMethod(
	'establecerPerfilesEImportesLD',  // param1: nombre de la funcion
	'org.mozilla.javascript.xmlimpl.XML, org.mozilla.javascript.xmlimpl.XML, java.lang.Integer, java.lang.String, java.lang.Integer',
	'java.lang.Object'
);
function establecerPerfilesEImportesLD(ldAActualizar, nuevosPerfilesYDescuento, idcliente, fecha, idtipoventa){	
	var nuevosPerfiles = nuevosPerfilesYDescuento.perfiles;
	var nuevoDescuento = nuevosPerfilesYDescuento.descuento;

	//comprobamos si nos llegan perfiles para establecer el perfil visitante por defecto y los importes
	if(nuevosPerfiles.hasOwnProperty('Perfilvisitante')){
		//Si no tenemos perfil de visitante seleccionamos el primero de la lista de perfiles aplicables
		if (ldAActualizar.perfilvisitante.idperfilvisitante.text().toString() == "") {
			ldAActualizar.perfilvisitante.idperfilvisitante = nuevosPerfiles.Perfilvisitante[0].idperfilvisitante;

			// necesitamos esto en para que el lazycombobox tenga el texto del perfil
			ldAActualizar.perfilvisitante.nombre = nuevosPerfiles.Perfilvisitante[0].nombre;

			// necesitamos la tarifa con el nombre para las pantallas de rectificación
			ldAActualizar.perfilvisitante.tarifa.nombre = nuevosPerfiles.Perfilvisitante[0].tarifa.nombre;
		}
			
		var perfilVisitanteSel = nuevosPerfiles.Perfilvisitante.(idperfilvisitante == ldAActualizar.perfilvisitante.idperfilvisitante);

		var importe = perfilVisitanteSel.tarifa.Tarifaproducto.importe.text();
		var idtarifaproducto = perfilVisitanteSel.tarifa.Tarifaproducto.idtarifaproducto.text();

		ldAActualizar.tarifaproducto.importe = <importe>{importe}</importe>;
		ldAActualizar.tarifaproducto.idtarifaproducto = <idtarifaproducto>{idtarifaproducto}</idtarifaproducto>;
		
		var tipoPromocion = ldAActualizar.descuentopromocional.iddescuentopromocional.tipopromocion.idtipopromocion.text().toString();

		//Si no tenemos ni promocion ni idcliente tenemos los datos necesarios para calcular el importe de la linea de detalle
		if ((tipoPromocion == "" || tipoPromocion == 5) && isNaN(idcliente)) {		
			var importeld = Number(importe) * Number(ldAActualizar.cantidad.text());
			
			//Descuento del cliente
			ldAActualizar.importedescuentocliente = "0";
			ldAActualizar.descuento.porcentajedescuento = "";
			ldAActualizar.descuento.iddescuento = "";

			//Mostramos las lineas de detalle con el importe total
			ldAActualizar.importe = importeld;
		} else {
			//Quitamos el tag ArrayList y nos quedamos solo con la LD
			var ldAux = calcularImporteLDDesdeServicio(ldAActualizar, idcliente, idtipoventa, fecha, "1");
			//Ahora actualizamos el importe total de la LD para que se muestre como si no tuviese descuento
			// El idcliente se pasa de todas formas, porque es posible que existan tarifas especiales de cliente
			// que sí deben obtenerse (y este dato es independiente del posible descuento de cliente)
			
			// opción 1: llamamos siempre al servicio para conocer el importe sin descuento
			//var ldAuxSinDescCliente = calcularImporteLDDesdeServicio(ldAActualizar, idcliente, idtipoventa, fecha, "0");
			
			//Actualizamos los datos de la LD original con los datos que nos ha devuelto el servicio
		
			
			ldAActualizar.descuento.porcentajedescuento = ldAux.descuento.porcentajedescuento;
			ldAActualizar.descuento.iddescuento = ldAux.descuento.iddescuento;
		//	ldAActualizar.importe = ldAux.importe;
			ldAActualizar.importedescuentocliente = ldAux.importedescuentocliente;
			ldAActualizar.producto.iva.porcentaje = ldAux.producto.iva.porcentaje;
				
		
			// opción 2: este método ya tiene en cuenta el no llamar a servicio si no hay descuento de cliente
			calculaImporteMostrarLD(ldAActualizar, idtipoventa, idcliente);
			
			
		} 
		
	}
	else{
		ldAActualizar.tarifaproducto.importe = "--";
		ldAActualizar.importe = "--";
	}
	
	ldAActualizar.appendChild(nuevosPerfiles);
	
	return ldAActualizar;
}


meta.addMethod(
	'calculaImporteMostrarLD',  // param1: nombre de la funcion
	'org.mozilla.javascript.xmlimpl.XML, java.lang.Integer, java.lang.Integer',
	'java.lang.Object'
);


function calculaImporteMostrarLD(xmlLD, idtipoventa, idcliente){
	
	var descuentoCliSinIVA = Number(xmlLD.importedescuentocliente.text());
	
	if (isNaN(descuentoCliSinIVA)) {
		descuentoCliSinIVA = 0;
	}
	
	if ( log.isInfoEnabled() ) {
		log.info("\nPOSTPROCESO obtenerVentaPorId.js ejecutando calculaImporteMostrarLD descuentoCliSinIVA "+descuentoCliSinIVA +" ld "+xmlLD);
	}
	
	//Si tenemos descuento de cliente tenemos que llamar al servicio para que nos devuelva el importe de la LD sin descontar el descuento del cli
	if (descuentoCliSinIVA != 0) {
		if (log.isInfoEnabled()) {
			log.info("obtenerProductoPorIdVentaReserva.calculaImporteMostrarLD, el cliente "+ idcliente +" tiene descuento, calculamos el importe sin descuento.");
		}
		
		// podemos pasar fecha nula, la comprobará el servicio a partir de fechas de sesiones
		var ldAux = this.calcularImporteLDDesdeServicio(xmlLD, idcliente, idtipoventa, null, "0");
		xmlLD.importe = ldAux.importe;
	} else {
		if (log.isInfoEnabled()) {
			log.info("obtenerProductoPorIdVentaReserva.calculaImporteMostrarLD, el cliente "+ idcliente +" NO tiene descuento.");
		}
	}
	
	return xmlLD;
}


function calcularImporteLDDesdeServicio(xmlParam, idcliente, idtipoventa, fecha, aplicarDescuentoCliente) {
	
	//Eliminamos los nodos vacios
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('transformerXML.js');
	
	//Creamos la LD que enviaremos al servicio y le quitamos lo que no admite el servicio
	var ldAux = comun.borraElementosSinHijos(xmlParam.toXMLString());
	delete ldAux.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.producto;
	delete ldAux.bonosForIdlineadetalle;
	//GGL 16.05.2016 CLUB
	//delete ldAux.abonosForIdlineadetalle;
	delete ldAux..detalles;
	delete ldAux..sesion.nombre;
	
	//Creamos el dto que enviaremos al servicio
	var dto = <dto />;
	
	dto.appendChild(<lineadetalles />);
	dto.lineadetalles.appendChild(ldAux);
	
	//Si tenemos cliente lo añadimos
	if(idcliente != null && !isNaN(idcliente)){
		dto.cliente.idcliente = idcliente;
	}
	
	//Si tenemos tipo de venta lo añadimos
	if (idtipoventa != null && !isNaN(idtipoventa)) {
		dto.idtipoventa = idtipoventa;
	}
	
	//Si tenemos fecha la añadimos al dto
	if (fecha != null) {
		dto.fecha = fecha;
	}
	
	if (aplicarDescuentoCliente != null) {
		dto.aplicarDescuentoCliente = <aplicarDescuentoCliente>{aplicarDescuentoCliente}</aplicarDescuentoCliente>;
	}
	
	var xmlEnvio =  <servicio>
						<parametro>
							{dto}
						</parametro>
					</servicio>
					
					
	if (log.isInfoEnabled()) {
		log.info("obtenerProductoPorIdVentaReserva.calcularImporteLDDesdeServicio, xmlEnvio: " + xmlEnvio);
	}
					
	try {
		ldAux = new XML(services.process('aplicarDescuentoPromocionalListaSOAWrapper',xmlEnvio).trim());
		
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : establecerPerfilesEImportesLD');
			log.error('LOG(ERROR) : to this : aplicarDescuentoPromocionalListaSOAWrapper : ');
			log.error('LOG(ERROR) : result in this Exception : '+ex);
		}
		
		throw ex.javaException;
	}
	
	if (log.isInfoEnabled()) {
		log.info("obtenerProductoPorIdVentaReserva.calcularImporteLDDesdeServicio, resultado: " + ldAux.toXMLString());
	}
	
	return ldAux.Lineadetalle;
}

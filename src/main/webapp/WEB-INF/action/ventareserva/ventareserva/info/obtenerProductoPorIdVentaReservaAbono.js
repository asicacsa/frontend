var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');
//var lineaDetalle = '';
var methodpostModel = 'getXMLModel';
var methodPerfilVisitante = 'obtenerPerfilesAplicablesConUnaTarifaProducto';
var cantidad = 1;

function handle(request, response){

	if (log.isInfoEnabled()){
		log.info("Entrada a obtenerProductoPorIdVentaReservaAbono.action");//, lock: " + this.lock.toString());
	}
	
	var methodpost = 'obtenerProductoPorId';	
	
	//cantidad de entradas
	cantidad = request.getParameter('cantidad');

	if (cantidad != null && cantidad != "null" && cantidad != "" && !isNaN(cantidad) && parseInt(cantidad)>0 ) {
		cantidad = parseInt(cantidad);
	}else{
		cantidad = 1;
	}
		
	var idProducto = request.getParameter('idproducto');
	var idPerfil = request.getParameter('idperfilvisitante');
	var idCanal = request.getParameter('idcanal');
	var idcliente = request.getParameter('idcliente');
	var idtipoventa = request.getParameter('idtipoventa');
	var idLineadetalle = request.getParameter('idLineadetalle');
	
	var idtipoidentificador = request.getParameter('idtipoidentificador');	
	var nif = request.getParameter('nif');
	var nombre = request.getParameter('nombre');
	var apellido1 = request.getParameter('apellido1');
	var apellido2 = request.getParameter('apellido2');
	var codSexo = request.getParameter('codSexo');
	var idperfilvisitante = request.getParameter('idperfilvisitante');	
	var fechanacimiento = request.getParameter('fechanacimiento');
	var codTipoVia = request.getParameter('codTipoVia');
	var via = request.getParameter('via');
	var numvia = request.getParameter('numvia');
	var complementovia = request.getParameter('complementovia');
	var cp = request.getParameter('cp');	
	var idmunicipio = request.getParameter('idmunicipio');
	var idprovincia = request.getParameter('idprovincia');
	var fijo = request.getParameter('fijo');
	var movil = request.getParameter('movil');
	var deseamovil = request.getParameter('deseamovil');
	var email = request.getParameter('email');	
	var deseaemail = request.getParameter('deseaemail');
	var codParentesco = request.getParameter('codParentesco');
	var idModelo = request.getParameter('idModelo');
	var titular = request.getParameter('titular');
	
	//GGL 02022012
	var nombrecompleto = request.getParameter('nombrecompleto');
	
	
	//var isLdGratuita = request.getParameter('isLdGratuita');
	//var autosesion = request.getParameter('autosesion');
	
	var lineaDetalle = request.getParameter('tipoLineadetalle');
	//Por si llega vacio
	if (lineaDetalle == null || lineaDetalle == "null" || lineaDetalle == "") {
		lineaDetalle = "LineadetalleVentaPasesDTO";
	}
	
	
	var idcontenido = null;
		
	if (log.isInfoEnabled()) {
		var msg = "obtenerProductoPorIdVentaReservaAbono: " + " idProducto: " + idProducto + "\n";
		msg = msg + " idPerfil: " + idPerfil + "\n";
		msg = msg + " idCanal: " + idCanal + "\n";
		msg = msg + " idcliente: " + idcliente + "\n";
		msg = msg + " idtipoventa: " + idtipoventa + "\n";
		msg = msg + " idLineadetalle: " + idLineadetalle + "\n";
		msg = msg + " idtipoidentificador: " + idtipoidentificador + "\n";
		msg = msg + " titular: " + titular + "\n";
		//msg = msg + " isLdGratuita: " + isLdGratuita + "\n";
		//msg = msg + " autosesion: " + autosesion + "\n";
		//msg = msg + " sesion: " + sesion + "\n";
		msg = msg + " tipoLineaDetalle: " + lineaDetalle + "\n";
		
		log.info(msg);
	}
	
	var i = 0;		
	var elementoAEliminar = 0;
	
	
	if (idcliente != null && idcliente != "null" && idcliente != "" && !isNaN(idcliente)) {
		idcliente = parseInt(idcliente);
	} else {
		idcliente = null;
	}
	
	if (idtipoventa != null && idtipoventa != "null" && idtipoventa != "" && !isNaN(idtipoventa)) {
		idtipoventa = parseInt(idtipoventa);
	} else {
		idtipoventa = null;
	}
	
	
	
	//Obtenemos el esqueleto de la lineadetalle pasandole el tipoLineadetalle (LineadetalleVentaPasesDTO)
	//GGL Aqui le mete la cantidad que en nuestro caso sera 1. Igual podemos meterle el perfil.
	xmlLineadetalle = this.retornaLineadetalle(lineaDetalle);//la Lineadetalle

	var fechaMin = null;

	//Obtenemos el producto que nos pasan
	var productoLD = this.retornaProducto(idProducto);
	
	if (idPerfil != null && idPerfil != "null" && idPerfil != "" && !isNaN(idPerfil)) {
		idPerfil = parseInt(idPerfil);
	} else {
		//GGL 22092011 Una idea: Lo dejo en blanco y así cogera el primero de la lista de perfiles aplicables que suele ser 'adulto'
		idPerfil = "";
	}
	
	//if (log.isInfoEnabled()) {
	//	log.info("obtenerProductoPorIdVentaReservaAbono, resultado obtenerProductoPorId: " + productoLD);
	//}
	
	//Le paso el perfil que han seleccionado. Mas tarde recogere el nombre de los perfilesAplicables
	xmlLineadetalle.perfilvisitante.idperfilvisitante = <idperfilvisitante>{idPerfil}</idperfilvisitante>;
	//xmlLineadetalle.idtipoventa = <idtipoventa>{idtipoventa}</idtipoventa>;
	
	//Anyadimos el producto a la lineadetalle
	xmlLineadetalle.producto = productoLD;

	
	//Obtenemos los perfiles de visitante para el producto, canal, zona, cliente, contenido, sesion, tipo de venta y idlinedetalle(null) que tenemos	
	//GGL if(isLdGratuita == "false"){
		//var perfilLD = retornaPerfilVisitanteYDescuento(idProducto, idCanal, idcliente, idsesion, idtipoventa, idLineadetalle);
	var perfilLD = retornaPerfilVisitanteYDescuento(idProducto, idCanal, null, null, idtipoventa, null);
	
	//xmlLineadetalle = establecerPerfilesEImportesLD(xmlLineadetalle, perfilLD, idcliente, fechaMin, idtipoventa);
	xmlLineadetalle = establecerPerfilesEImportesLD(xmlLineadetalle, perfilLD, null, fechaMin, idtipoventa);
	
	
	//}
	
	//Rellenamos la LD con todos los params del dataset
	xmlLineadetalle.datosClienteClub.nif = nif;
	xmlLineadetalle.datosClienteClub.idtipoidentificador = <idtipoidentificador>{idtipoidentificador}</idtipoidentificador>;	
	xmlLineadetalle.datosClienteClub.nombre = nombre;
	xmlLineadetalle.datosClienteClub.apellido1 = <apellido1>{apellido1}</apellido1>;
	xmlLineadetalle.datosClienteClub.apellido2 = <apellido2>{apellido2}</apellido2>;
	
	//GGL Para que en la LD del grid aparezca en un solo campo
	if (nombre != null && nombre != "null" && nombre != "") {
		xmlLineadetalle.datosClienteClub.nombrecompleto = <nombrecompleto>{nombre} {apellido1} {apellido2}</nombrecompleto>;
		var nombres = nombre.split(' ');
		var nombresinblancos = "";
		for (var i=0; i < nombres.length; i++) {
			nombresinblancos = nombresinblancos + nombres[i];
		}		
		xmlLineadetalle.datosClienteClub.nombrefoto = <nombrefoto>{nif}{nombresinblancos}{apellido1}{apellido2}.jpg</nombrefoto>;
	} else {
		xmlLineadetalle.datosClienteClub.nombrecompleto = <nombrecompleto>{nombrecompleto}</nombrecompleto>;
		var nombrecompletos = nombrecompleto.split(' ');
		var nombrecompletosinblancos = "";
		for (var i=0; i < nombrecompletos.length; i++) {
			nombrecompletosinblancos = nombrecompletosinblancos + nombrecompletos[i];
		}
		xmlLineadetalle.datosClienteClub.nombrefoto = <nombrefoto>{nif}{nombrecompletosinblancos}.jpg</nombrefoto>;
	}
			
	xmlLineadetalle.datosClienteClub.codSexo = <codSexo>{codSexo}</codSexo>;	
	xmlLineadetalle.datosClienteClub.fechanacimiento = <fechanacimiento>{fechanacimiento}</fechanacimiento>;	
	xmlLineadetalle.datosClienteClub.idperfilvisitante = <idperfilvisitante>{idperfilvisitante}</idperfilvisitante>;
	xmlLineadetalle.datosClienteClub.codTipoVia = <codTipoVia>{codTipoVia}</codTipoVia>;
	xmlLineadetalle.datosClienteClub.via = <via>{via}</via>;
	xmlLineadetalle.datosClienteClub.numvia = <numvia>{numvia}</numvia>;
	xmlLineadetalle.datosClienteClub.complementovia = <complementovia>{complementovia}</complementovia>;
	xmlLineadetalle.datosClienteClub.cp = <cp>{cp}</cp>;
	xmlLineadetalle.datosClienteClub.idmunicipio = <idmunicipio>{idmunicipio}</idmunicipio>;
	xmlLineadetalle.datosClienteClub.idprovincia = <idprovincia>{idprovincia}</idprovincia>;
	xmlLineadetalle.datosClienteClub.fijo = <fijo>{fijo}</fijo>;
	xmlLineadetalle.datosClienteClub.movil = <movil>{movil}</movil>;
	xmlLineadetalle.datosClienteClub.deseamovil = <deseamovil>{deseamovil}</deseamovil>;
	xmlLineadetalle.datosClienteClub.email = <email>{email}</email>;
	xmlLineadetalle.datosClienteClub.deseaemail = <deseaemail>{deseaemail}</deseaemail>;
	xmlLineadetalle.datosClienteClub.codParentesco = <codParentesco>{codParentesco}</codParentesco>;
	xmlLineadetalle.datosClienteClub.idModelo = <idModelo>{idModelo}</idModelo>;
	xmlLineadetalle.datosClienteClub.idproducto = <idproducto>{idProducto}</idproducto>;
	xmlLineadetalle.datosClienteClub.titular = <titular>{titular}</titular>;
	
	if (log.isInfoEnabled()) {
		log.info("obtenerProductoPorIdVentaReservaAbono, resultado final antes de limpiar: " + xmlLineadetalle.toXMLString());
	} 
	
	var param = comun.eliminarElementosNull(xmlLineadetalle.toXMLString());
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(param.toXMLString());
}

function retornaLineadetalle(lineaDetalle){

	var result2;
			
	var envio = <servicio>
					<parametro>
						<{lineaDetalle}/>
					</parametro>
				</servicio>

	var methodpost = methodpostModel;
	
	try {
		result2 = services.process(methodpost+'SOAWrapper',envio).trim();
	} catch (ex) {
		if (log.isErrorEnabled()) {	
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
		}
		
		throw ex.javaException;
	}
	
	result2 = new XML(result2);
	
	//Anyadimos la cantidad ya que es requerida y asi podremos calcular el importe segun la tarifa seleccionada
	result2.cantidad = cantidad;
	
	//Ponemos la seleccion por defecto del perfil de visitante
	//result2.perfilvisitante.idperfilvisitante = 1;
				
	return result2;
}

meta.addMethod(
	'retornaPerfilVisitanteYDescuento',  // param1: nombre de la funcion
	'java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String',
	'java.lang.Object'
);
function retornaPerfilVisitanteYDescuento(idProducto, idCanal, idcliente, idsesion, idtipoventa, idlineadetalle){
	var perfilesydescuento;
	var envio = <servicio />;
	
	//Genero con ECMA el xml de envio, simplemente anyadiendo con puntos los tags
	if (idProducto != null && !isNaN(idProducto)) {
		envio.parametro.listaDto.ObtenerPerfilesy1Tarifaparam.idproducto = <idproducto>{parseInt(idProducto)}</idproducto>;
	}
	
	if (idtipoventa != null && !isNaN(idtipoventa)) {
		envio.parametro.listaDto.ObtenerPerfilesy1Tarifaparam.idtipoventa = <idtipoventa>{parseInt(idtipoventa)}</idtipoventa>;
	} else {
		throw new java.lang.Exception("El tipo de venta es requerido.");
	}

	if (idcliente != null && !isNaN(parseInt(idcliente))) {
		envio.parametro.listaDto.ObtenerPerfilesy1Tarifaparam.idcliente = <idcliente>{parseInt(idcliente)}</idcliente>;
	}	
	
	if (idCanal != null && !isNaN(parseInt(idCanal))) {
		envio.parametro.listaDto.ObtenerPerfilesy1Tarifaparam.idcanal = <idcanal>{parseInt(idCanal)}</idcanal>;
	}
	
	if (idlineadetalle != null && !isNaN(parseInt(idlineadetalle))) {
		envio.parametro.listaDto.ObtenerPerfilesy1Tarifaparam.idlineadetalle = <idlineadetalle>{parseInt(idlineadetalle)}</idlineadetalle>;
	}		
				
	var methodpost = methodPerfilVisitante;
	
	if (log.isInfoEnabled()) {
		log.info("obtenerProductoPorIdVentaReservaAbono.retornaPerfilVisitanteYDescuento, envio a " + methodpost + ": " + envio);
	} 
	
	try {
			perfilesydescuento = new XML(services.process(methodpost+'SOAWrapper',envio).trim());
		} catch (ex) {
			if (log.isErrorEnabled()) {
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : ');
				log.error('LOG(ERROR) : result in this Exception : '+ex);
			}
			
			throw ex.javaException;
		}
	
	
	if (log.isInfoEnabled()) {
		log.info("obtenerProductoPorIdVentaReservaAbono.retornaPerfilVisitanteYDescuento, resultado de " + methodpost + ": " + perfilesydescuento.toXMLString());
	}
		
	//El servicio devuelve un Array de PerfilesYDescuento que es otro array, pero en este caso solo nos puede devolver una serie de perfiles de visitante
	//Asi que ponemos como raiz el nodo PerfilesYDescuento
	if (perfilesydescuento.hasOwnProperty("PerfilesYDescuento")) {
		perfilesydescuento = perfilesydescuento.PerfilesYDescuento;
	}
	
	//Le cambiamos el nombre al nodo PerfilesYDescuento por perfilesydescuento
	perfilesydescuento.setName("perfilesydescuento");
	
	if (log.isInfoEnabled()) {
		log.info("obtenerProductoPorIdVentaReservaAbono.retornaPerfilVisitanteYDescuento, resultado final: " + perfilesydescuento.toXMLString());
	}
	return perfilesydescuento;
}



function retornaProducto(idProducto){	
	
	var envio = <servicio>
					<parametro>
						<int>{idProducto}</int>
					</parametro>
				</servicio>

	var methodpost = "obtenerProductoPorId";
	
	var result2;
	
	try {
		result2 = services.process(methodpost+'SOAWrapper',envio).trim();
	} catch (ex) {
		if (log.isErrorEnabled()) {	
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
		}
		
		throw ex.javaException;
	}
	
	
	result2 = new XML(result2);
	
	result2.setName("producto");
	
	return result2;
}


meta.addMethod(
	'establecerPerfilesEImportesLD',  // param1: nombre de la funcion
	'org.mozilla.javascript.xmlimpl.XML, org.mozilla.javascript.xmlimpl.XML, java.lang.Integer, java.lang.String, java.lang.Integer',
	'java.lang.Object'
);
function establecerPerfilesEImportesLD(ldAActualizar, nuevosPerfilesYDescuento, idcliente, fecha, idtipoventa){	
	var nuevosPerfiles = nuevosPerfilesYDescuento.perfiles;
	var nuevoDescuento = nuevosPerfilesYDescuento.descuento;
	if (log.isInfoEnabled()) {
		log.info("obtenerProductoPorIdVentaReservaAbono.establecerPerfilesEImportesLD, ld, perfiles : " + ldAActualizar.toXMLString() +" @@@@@@@@@@@@@@ "+nuevosPerfilesYDescuento.toXMLString());
	}
	//comprobamos si nos llegan perfiles para establecer el perfil visitante por defecto y los importes
	if(nuevosPerfiles.hasOwnProperty('Perfilvisitante')){
		//Si no tenemos perfil de visitante seleccionamos el primero de la lista de perfiles aplicables 
		if (ldAActualizar.perfilvisitante.idperfilvisitante.text().toString() == "") {
			ldAActualizar.perfilvisitante.idperfilvisitante = nuevosPerfiles.Perfilvisitante[0].idperfilvisitante;

			// necesitamos esto en para que el lazycombobox tenga el texto del perfil
			ldAActualizar.perfilvisitante.nombre = nuevosPerfiles.Perfilvisitante[0].nombre;

			// necesitamos la tarifa con el nombre para las pantallas de rectificación
			ldAActualizar.perfilvisitante.tarifa.nombre = nuevosPerfiles.Perfilvisitante[0].tarifa.nombre;
		}
		//GGL 22092011 Para que coja un perfil por defecto se lo añado a la ldAActualizar.perfilvisitante	
		var perfilVisitanteSel = nuevosPerfiles.Perfilvisitante.(idperfilvisitante == ldAActualizar.perfilvisitante.idperfilvisitante);
		//Seleccionamos la tarifa del perfil seleccionado y sacamos el importe
		var importe = perfilVisitanteSel.tarifa.Tarifaproducto.importe.text();
		var idtarifaproducto = perfilVisitanteSel.tarifa.Tarifaproducto.idtarifaproducto.text();
		//Actualizamos la linea de detalle con importe y tarifa y nombre de perfil para el combo
		ldAActualizar.tarifaproducto.importe = <importe>{importe}</importe>;
		ldAActualizar.tarifaproducto.idtarifaproducto = <idtarifaproducto>{idtarifaproducto}</idtarifaproducto>;
		ldAActualizar.perfilvisitante.nombre = perfilVisitanteSel.nombre;
		
		var tipoPromocion = ldAActualizar.descuentopromocional.iddescuentopromocional.tipopromocion.idtipopromocion.text().toString();
		
		//Si no tenemos ni promocion ni idcliente tenemos los datos necesarios para calcular el importe de la linea de detalle
		if ((tipoPromocion == "" || tipoPromocion == 5) && (isNaN(idcliente) || idcliente == null || idcliente == "")) {		
			var importeld = Number(importe) * Number(ldAActualizar.cantidad.text());
			if (log.isInfoEnabled()) {
				log.info("@@@@@@@@@@@@@@@@@@1obtenerProductoPorIdVentaReservaAbono.establecerPerfilesEImportesLD, idtipoPromocion: " + tipoPromocion+ " importe: "+importeld+"  idcliente: "+idcliente);
			}
			//Descuento del cliente
			ldAActualizar.importedescuentocliente = "0";
			ldAActualizar.descuento.porcentajedescuento = "";
			ldAActualizar.descuento.iddescuento = "";

			//Mostramos las lineas de detalle con el importe total
			ldAActualizar.importe = importeld;
			
		} else {  //TODO GGL TIENE SENTIDO LO DEL CLIENTE??????????????????????????????????????????????????? 
			if (log.isInfoEnabled()) {
				log.info("@@@@@@@@@@@@@@@@@2obtenerProductoPorIdVentaReservaAbono.establecerPerfilesEImportesLD, idtipoPromocion: " + tipoPromocion+"  importe: "+importeld+" idcliente: "+idcliente);
			}
			//Quitamos el tag ArrayList y nos quedamos solo con la LD
			var ldAux = calcularImporteLDDesdeServicio(ldAActualizar, idcliente, idtipoventa, fecha, "1");
			
			//Ahora actualizamos el importe total de la LD para que se muestre como si no tuviese descuento
			// El idcliente se pasa de todas formas, porque es posible que existan tarifas especiales de cliente
			// que sí deben obtenerse (y este dato es independiente del posible descuento de cliente)
			
			// opción 1: llamamos siempre al servicio para conocer el importe sin descuento
			//var ldAuxSinDescCliente = calcularImporteLDDesdeServicio(ldAActualizar, idcliente, idtipoventa, fecha, "0");
			
			//Actualizamos los datos de la LD original con los datos que nos ha devuelto el servicio
			ldAActualizar.descuento.porcentajedescuento = ldAux.descuento.porcentajedescuento;
			ldAActualizar.descuento.iddescuento = ldAux.descuento.iddescuento;
			ldAActualizar.importe = ldAux.importe;
			ldAActualizar.importedescuentocliente = ldAux.importedescuentocliente;
			ldAActualizar.producto.iva.porcentaje = ldAux.producto.iva.porcentaje;
			
			// opción 2: este método ya tiene en cuenta el no llamar a servicio si no hay descuento de cliente
			calculaImporteMostrarLD(ldAActualizar, idtipoventa, idcliente);
			
			
		} 
		
	}
	else{
		ldAActualizar.tarifaproducto.importe = "--";
		ldAActualizar.importe = "--";
	}
	
	ldAActualizar.appendChild(nuevosPerfiles);
	
	return ldAActualizar;
}


meta.addMethod(
	'calculaImporteMostrarLD',  // param1: nombre de la funcion
	'org.mozilla.javascript.xmlimpl.XML, java.lang.Integer, java.lang.Integer',
	'java.lang.Object'
);


function calculaImporteMostrarLD(xmlLD, idtipoventa, idcliente){
	var descuentoCliSinIVA = Number(xmlLD.importedescuentocliente.text());
	
	if (isNaN(descuentoCliSinIVA)) {
		descuentoCliSinIVA = 0;
	}
	
	//Si tenemos descuento de cliente tenemos que llamar al servicio para que nos devuelva el importe de la LD sin descontar el descuento del cli
	if (descuentoCliSinIVA != 0) {
		if (log.isDebugEnabled()) {
			log.debug("obtenerProductoPorIdVentaReservaAbono.calculaImporteMostrarLD, el cliente "+ idcliente +" tiene descuento, calculamos el importe sin descuento.");
		}
		
		// podemos pasar fecha nula, la comprobará el servicio a partir de fechas de sesiones
		var ldAux = this.calcularImporteLDDesdeServicio(xmlLD, idcliente, idtipoventa, null, "0");
		xmlLD.importe = ldAux.importe;
	} else {
		if (log.isDebugEnabled()) {
			log.debug("obtenerProductoPorIdVentaReservaAbono.calculaImporteMostrarLD, el cliente "+ idcliente +" NO tiene descuento.");
		}
	}
	
	return xmlLD;
}


function calcularImporteLDDesdeServicio(xmlParam, idcliente, idtipoventa, fecha, aplicarDescuentoCliente) {
	
	//Eliminamos los nodos vacios
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('transformerXML.js');
	
	//Creamos la LD que enviaremos al servicio y le quitamos lo que no admite el servicio
	var ldAux = comun.borraElementosSinHijos(xmlParam.toXMLString());
	delete ldAux.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.producto;
	delete ldAux.bonosForIdlineadetalle;
	//GGL 16.05.2016
	delete ldAux.abonosForIdlineadetalle;
	delete ldAux..detalles;
	delete ldAux..sesion.nombre;
	
	//Creamos el dto que enviaremos al servicio
	var dto = <dto />;
	
	dto.appendChild(<lineadetalles />);
	dto.lineadetalles.appendChild(ldAux);
	
	//Si tenemos cliente lo añadimos
	if(idcliente != null && !isNaN(idcliente)){
		dto.cliente.idcliente = idcliente;
	}
	
	//Si tenemos tipo de venta lo añadimos
	if (idtipoventa != null && !isNaN(idtipoventa)) {
		dto.idtipoventa = idtipoventa;
	}
	
	//Si tenemos fecha la añadimos al dto
	if (fecha != null) {
		dto.fecha = fecha;
	}
	
	if (aplicarDescuentoCliente != null) {
		dto.aplicarDescuentoCliente = <aplicarDescuentoCliente>{aplicarDescuentoCliente}</aplicarDescuentoCliente>;
	}
	
	var xmlEnvio =  <servicio>
						<parametro>
							{dto}
						</parametro>
					</servicio>
					
	if (log.isInfoEnabled()) {
		log.info("obtenerProductoPorIdVentaReservaAbono.calcularImporteLDDesdeServicio, xmlEnvio: " + xmlEnvio);
	}
					
	try {
		ldAux = new XML(services.process('aplicarDescuentoPromocionalListaSOAWrapper',xmlEnvio).trim());
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : establecerPerfilesEImportesLD');
			log.error('LOG(ERROR) : to this : aplicarDescuentoPromocionalListaSOAWrapper : ');
			log.error('LOG(ERROR) : result in this Exception : '+ex);
		}
		
		throw ex.javaException;
	}
	
	if (log.isInfoEnabled()) {
		log.info("obtenerProductoPorIdVentaReservaAbono.calcularImporteLDDesdeServicio, resultado: " + ldAux.toXMLString());
	}
	//GGL TODO REPASARRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR 
	return ldAux.LineadetalleVentaPasesDTO;
}

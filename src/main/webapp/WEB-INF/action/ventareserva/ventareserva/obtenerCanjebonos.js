var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {

	if (log.isInfoEnabled()) {
		log.info("entrada a default.action");
	}
	
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}

	// si xml no se envia, llama al metodo sin parametros	
	var xml = request.getParameter('xml');
	
	if (log.isInfoEnabled()) {
		log.info("default.action, method: " + methodpost);
		log.info("default.action, xml: " + xml);
	}
	
	if(xml == null ) {
		xml = '<servicio/>';
	} else {
		var xmlSinHijos;
		var sbf = context.beans.getBean('scriptBeanFactory');
		var comun = sbf.getBean('transformerXML.js');
		
		xmlSinHijos = comun.borraElementosSinHijos(xml);
		
		if (xmlSinHijos.name() == "parametro") {
			xml = <servicio>
					{xmlSinHijos}
				  </servicio>
		} else {
			xml = <servicio>
					<parametro>
						{xmlSinHijos}
					</parametro>
				  </servicio>
		}
		
	}
	
	if (log.isInfoEnabled()) {
		log.info("default.action, service parameters: " + xml);
	}
	
	var result = null;
	try {
		result = services.process(methodpost+'SOAWrapper',xml);
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost);
			log.error("LOG(ERROR) : con esta llamada: " + xml);
			log.error('LOG(ERROR) : con este tipo de error : ' + ex);
			log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
		}
		
		throw ex.javaException;
	}
	
	result = this.postProcessXML(new XML(result));
	result = comun.borraElementosSinHijos(result);
	
	if (result == null || result.toString() == "") 
		result = "<ArrayList />";
	
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result);
}


function postProcessXML(xmlparam) {
	
	for each (canjebono in xmlparam.Canjebono){
		canjebono.identrada = <identrada/>;
		
		//Sólo mostramos el número de entrada en caso de que sea un producto de hemisferic
		var perteneceHemisferic = false;
		
		for each (run in canjebono.lineadetallezonasesion.zonasesion.sesion.contenido.tipoproducto.recinto.recintounidadnegocios.Recintounidadnegocio){
			if(run.unidadnegocio.idunidadnegocio == 4)
				perteneceHemisferic = true;
		}
		
		if (perteneceHemisferic){
			
			//Si hay más de una entrada, obtemos el id de la entradatornos que tiene la misma sesión que el bono del canje
			if (canjebono.lineadetallezonasesion.lineadetalle.entradas.Entrada.length() > 1){
				var idsesion = canjebono.lineadetallezonasesion.zonasesion.sesion.idsesion.text();
				
				// buscamos también en entradatornos dados de baja, porque podemos estar consultando
				// canjes anulados.
				for each (entradatorno in canjebono.bono.entradatornosestodos.Entradatornos){
					if(entradatorno.sesion.idsesion.text () == idsesion){
						canjebono.identrada = <identrada>{entradatorno.entrada.identrada.text()}</identrada>;
					}
				}
				
			} else {
				canjebono.identrada = canjebono.lineadetallezonasesion.lineadetalle.entradas.Entrada[0].identrada;
			}
		}
	}
	return xmlparam.toString();
}

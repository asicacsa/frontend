var services = context.beans.getBean('httpServiceSOA');
var methodpost = "realizarVentaBonos";


function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var resultado;
	var envio;
	
	if (xml!=null) {
		xml = new XML(xml);
		
		
		xml = this.preProcessXML(xml);
		
		envio = 
			<servicio>
				<parametro>
					{xml}
				</parametro>
			</servicio>
		
		var aux = null;
		
		//Y llamamos al servicio...
		try {
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			if (log.isErrorEnabled()) {	
				log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
				log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : ');
				log.error('LOG(ERROR) : con esta llamada : ' + envio);
				log.error('LOG(ERROR) : con este tipo de error : ' + ex);				
				log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
			}
			
			throw ex.javaException;
		}
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		if(aux != null){
			aux = new XML(aux);
			resultado = 
					<ok>
						{aux}
					</ok>
		}			
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a realizarVenta es nulo!!!");
		}
		resultado = <error/>
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	param = new XML(param);
	
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('transformerXML.js');
	var eliminarBasuraLD = sbf.getBean('eliminarDatosInvalidosLineaDetalle.js');
	
	//Convertimos todos los campos booleanos a 0 ? 1
//	if (param.financiada.text() == false) {
//		param.financiada=<financiada>0</financiada>
//	} else {
//		param.financiada=<financiada>1</financiada>
//	}
	
//	if (param.entradasimpresas.text() == false) {
//		param.entradasimpresas=<entradasimpresas>0</entradasimpresas>
//	} else {
//		param.entradasimpresas=<entradasimpresas>1</entradasimpresas>
//	}
//	
//	if (param.reciboimpreso.text() == false) {
//		param.reciboimpreso=<reciboimpreso>0</reciboimpreso>
//	} else {
//		param.reciboimpreso=<reciboimpreso>1</reciboimpreso>
//	}

	param = comun.sustituyeLiteralesBooleanosEnVenta( param );

	//Eliminamos el tag isBono
	delete param.isBono;
	
	//Eliminamos lo que sobra de la linea de detalle
	param = eliminarBasuraLD.eliminarDatosInvalidosLineaDetalle(param);
	
	//Elimino las etiquetas que contienen la información sobre los descuentos del cliente
	delete param..porcentajesDescuentoLD;
	delete param..porcentajesDescuentoLDA;
		
	//Eliminamos todos los elementos que no tengan hijos
	param = comun.borraElementosSinHijos(param.toXMLString());

	return param;
	
}
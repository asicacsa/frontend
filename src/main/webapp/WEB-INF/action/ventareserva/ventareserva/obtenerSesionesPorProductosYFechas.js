var services = context.beans.getBean('httpServiceSOA');
		
function handle(request, response) {
	
	var methodpost = 'obtenerSesionesPorProductosYFechas';
	
	var xmlEnvio = request.getParameter('xml');
	var xml = null;
	
	if(xmlEnvio != null) {
		xmlEnvio = new XML(xmlEnvio);
				
		envio = <servicio>
					{xmlEnvio}
				</servicio>
				
		xml = services.process(methodpost+'SOAWrapper',envio);
	}
	
	xml = new XML(xml);
	
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comunDisponibilidad = sbf.getBean('obtenerSesionesPorTipoProductoYFecha.js');

	//Para cada sesion si tiene mas de una Zonasesion duplicamos la sesion
	for each (var nptp in xml.NombreProductoTipoProducto) {
		for each(var al in nptp.listTiposProductoSesiones.ArrayList) {
			comunDisponibilidad.ajustarDisponibilidad(al);
		}
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(xml.toXMLString());
}
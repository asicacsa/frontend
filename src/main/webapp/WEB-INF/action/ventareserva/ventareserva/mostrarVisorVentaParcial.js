var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {

	var xml = request.getParameter('xml');
	var resultado;
	var methodpost = 'mostrarVisorVentaParcial';
	var envio;
	
	if (xml!=null) {
		
		envio = "<servicio><parametro>" + xml + "</parametro></servicio>";
		
		if (log.isDebugEnabled()) {
			log.debug("\nmostrarVisorVentaParcial.action: parametro xml formateado para enviar a la capa de servicios :\n\n" + xml + "\n\n" );
		}

		
		try {
			var aux = services.process(methodpost+'SOAWrapper',envio);
			if (log.isDebugEnabled()) {
				log.debug("\nLlamada a " + methodpost+'SOAWrapper' + " realizada correctamente" );
			}
		} catch ( ex ) {
			aux = '<error>' + ex.javaException.getMessage() + '</error>';
			resultado = new XML(aux);
			if (log.isDebugEnabled()) {
				log.debug("\nSe ha producido una excepcion en la llamada " + methodpost + 'SOAWrapper\n' + aux );
			}
		}
		if(aux != null){
			resultado = new XML(aux);
		}
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
//var service = context.beans.getBean('httpServiceSOA');

// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = "obtenerClientePorId";
	
	if (log.isInfoEnabled()) {
		log.info("Entrada a obtenerClientePorId");
	}
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		var envio = '<servicio><parametro><java.lang.Integer/></parametro></servicio>'
	} else {
		xml = new XML(xml);
		if (log.isInfoEnabled()) {
			log.info("LOG(INFO) : params before the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		}
		
		envio = <servicio>
					<parametro>
						<java.lang.Integer>{xml}</java.lang.Integer>
					</parametro>
				</servicio>	
	}
	var respuesta;
	
	try{
		respuesta = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
		
	//var respuesta = new XML(service.process(methodpost,envio));
	
	if (respuesta!=null) {
		respuesta = new XML(respuesta);
		//log.info("LOG(INFO): valor de la respuesta antes de ser postprocesada: " + respuesta);

		if (log.isInfoEnabled()) {	
			log.info("Resultado de llamar a obtenerClientePorId: "+respuesta.toXMLString());
		}

	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	
	if(respuesta != null)
		response.writer.println(respuesta.toXMLString());
	else
		throw new java.lang.Exception ('No existe ningun cliente con el id introducido');
}

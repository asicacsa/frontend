var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {	
	
	var methodpost = request.getParameter('servicio');		
	

	var idreserva = request.getParameter('idreserva');
	var observaciones = request.getParameter('observ');
	
	if (log.isInfoEnabled()) {
		log.info("idreserva: idreserva " + idreserva);
		log.info("observaciones: observaciones " + observaciones);
	}
	
	var envio = <servicio></servicio>

	if(idreserva != null){
		
		envio = <servicio>
					<parametro>
						<java.lang.Integer>{idreserva}</java.lang.Integer>
						<observaciones>{observaciones}</observaciones>
					</parametro>
				</servicio>
	}

	if (log.isInfoEnabled()) {
		//log.info("Metodo a llamar : "+methodpost);
		log.info("Envio actualiza observaciones de la reserva: "+envio);
	}
	
	var result;
	if (log.isErrorEnabled()) {
		try {
			result = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
	} else {
		result = services.process(methodpost+'SOAWrapper',envio);
	}
	
	
	if (log.isInfoEnabled()) {
		log.info(result);
	}
	
	if (result !=  null) {
		result = new XML(result);

	} else {
		result = new XML();
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
	
}

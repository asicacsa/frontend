var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {	
	
	var methodpost = request.getParameter('servicio');		
	
	// si xml no se envia, llama al metodo sin parametros	
	var selecciona = null;
	
	selecciona = request.getParameter('xml');
	if (log.isInfoEnabled()) {
		log.info("obtenerComun: Id para seleccionar " + selecciona);
	}
	
	var envio = <servicio></servicio>
	
	// En este tipo de servicio no van a haber parametros de Entrada.
	if(selecciona != null){
		selecciona = new XML(selecciona);
		envio = <servicio>
					<parametro>
						<unidadNegocio>
							<idunidadnegocio>{selecciona}</idunidadnegocio>
						</unidadNegocio>
					</parametro>
				</servicio>
	}

	if (log.isInfoEnabled()) {
		//log.info("Metodo a llamar : "+methodpost);
		log.info("Envio : "+envio);
	}
	
	var result;
	if (log.isErrorEnabled()) {
		try {
			result = services.process(methodpost+'SOAWrapper',envio).trim();
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
	} else {
		result = services.process(methodpost+'SOAWrapper',envio).trim();		
	}
	
	
	if (log.isInfoEnabled()) {
		log.info(result);
	}
	
	if (result !=  null) {
		result = new XML(result);
		//result = this.postProcessXML(result, selecciona);		
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
	
}


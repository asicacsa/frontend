var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');
var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {
	var methodpost = "obtenerBonoPorId"; //request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}

	// si xml no se envia, llama al metodo sin parametros	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		xml = '<servicio />';
	} else {
		var xmlSinHijos;
		
		xmlSinHijos = comun.borraElementosSinHijos(xml);
		
		if (xmlSinHijos.name() == "parametro") {
			xml = <servicio>
					{xmlSinHijos}
				  </servicio>
		} else {
			xml = <servicio>
					<parametro>
						{xmlSinHijos}
					</parametro>
				  </servicio>
		}
		
	}
	
	var result = null;
	try {
		result = services.process(methodpost+'SOAWrapper',xml).trim();
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost);
			log.error("LOG(ERROR) : con esta llamada: " + xml);
			log.error('LOG(ERROR) : con este tipo de error : ' + ex);
			log.error('LOG(ERROR) : con esta causa : '+ ex.javaException.getMessage());
		}
		
		throw ex.javaException;
	}
	
	if (result == null) {
		result = "<ArrayList />";
	}
	else{
		result = new XML(result);
		result = this.postProcessXML(result);		
		result = result.toXMLString();
	}
	
	if (log.isDebugEnabled()) {
		log.debug("defult.action accesing " + methodpost);
		log.debug("	   result: " + result.substr(0,300) + "[...]");
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	
	response.writer.println(result);
}

function postProcessXML(xml){
	for each (var entradatorno in xml.entradatornoses.Entradatornos){
		if(entradatorno.toString() != ''){
			
			if(entradatorno.canjebono.lineadetallezonasesion.zonasesion.sesion.toString() != ''){
			
				var fecha = entradatorno.canjebono.lineadetallezonasesion.zonasesion.sesion.fecha.substring(0, entradatorno.canjebono.lineadetallezonasesion.zonasesion.sesion.fecha.indexOf('-'));
				var fechayhora = fecha + "-" + entradatorno.canjebono.lineadetallezonasesion.zonasesion.sesion.horainicio;
				entradatorno.canjebono.lineadetallezonasesion.zonasesion.sesion.fechayhora=<fechayhora>{fechayhora}</fechayhora>;
			}
			else{
				var fechayhora ="---";
				entradatorno.canjebono.lineadetallezonasesion.zonasesion.sesion.fechayhora = <fechayhora>{fechayhora}</fechayhora>;
			}
			
			var idcanjebono= '';
		}
	}
	return xml;
}

var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {
	var methodpost = "obtenerReservaPorId";
	// si xml no se envia, llama al metodo sin parametros	
	var xml = request.getParameter('xml');
	var result;
	
	if(xml == null ) {
		xml = '<servicio />';
	} else {
		var xmlSinHijos;
		var sbf = context.beans.getBean('scriptBeanFactory');
		var comun = sbf.getBean('transformerXML.js');
		
		xmlSinHijos = comun.borraElementosSinHijos(xml);

		xml = <servicio>
				<parametro>
					{xmlSinHijos}
				</parametro>
			  </servicio>
	}
	
	try{
		result = services.process(methodpost+'SOAWrapper',xml);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	if (result == null) {
		result = <ArrayList />;
	} else {
		
		result = new XML (result);
		result = postProceso(result);
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
}

function postProceso(xmlparam) {
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('obtenerProductoPorIdVentaReserva.js');
	var comunLD = sbf.getBean('obtenerVentaPorId.js');
	
	//Obtengo el importe unitario y el descuento cliente que se aplicó a las líneas de detalle en el momento en que se realizó la reserva	
	//var importesDescuentosLdReserva = comun.retornaDescuentosYPorcentajesLDs(xmlparam.idreserva, true);
	//importesDescuentosLdReserva = new XML (importesDescuentosLdReserva);
	
	//Almacenamos los descuentos de cliente que se han aplicado a la reserva
	var porcentajesDescuentoLD = <porcentajesDescuentoLD />;
	
	//Obtenemos los perfiles para poder modificarlos de cada linea de detalle
	for each (ld in xmlparam.lineadetallessinventa.Lineadetalle) {
		//Añado la colección de los porcentajes de cliente aplicados a la reserva, y el porcentaje correspondiente a cada ld
		comun.addPorcentajesClienteAplicados(porcentajesDescuentoLD, ld);
		xmlparam.porcentajesDescuentoLD = porcentajesDescuentoLD;
				
		//if(ld.lineadetallezonasesions.Lineadetallezonasesion.length() == 1)
		//	idcontenido = ld.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.contenido.idcontenido.text();
		
		var fechaMin = null;
		var idsesion = null;
		
		//Recorremos todas las sesiones para obtener la que tiene la fecha mas cercana	
		for each(var session in ld.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion) {
			
			var fechaString = session.fecha.text();
			if (fechaString.toString() != "") {
				var array = fechaString.split("-");
				var camposFecha = array[0].split("/");
				
				var fechaActual = new java.util.Date(camposFecha[2], camposFecha[1] - 1, camposFecha[0]);
							
				if (fechaMin == null || fechaActual.before(fechaMin)) {
					fechaMin = fechaActual;
					idsesion = session.idsesion;
				}
				else{
					//Si las sesiones tienen la misma fecha, nos quedamos con la de menor id
					if(fechaActual.equals(fechaMin) && (idsesion == null || idsesion > session.idsesion)){ 
						idsesion = session.idsesion;
					}
				}
			}
			
		}
		
		var idtipoventa = 4;
		var perfilLD = new XML(comun.retornaPerfilVisitanteYDescuento(ld.producto.idproducto.text(), xmlparam.canal.idcanal.text(), xmlparam.cliente.idcliente.text(), idsesion, idtipoventa, ld.idlineadetalle.text()));
		
		var idcliente = xmlparam.cliente.idcliente.text().toString();
		if(isNaN(idcliente) || idcliente == "")
			idcliente = null;
		else
			idcliente = parseInt(idcliente);
		
		if (fechaMin != null) {
			var dia = "0" + fechaMin.getDate();
			var mes = "0" + (Number(fechaMin.getMonth()) + 1);
	
			fechaMin = dia.substr(dia.length - 2, 2) + "/" + mes.substr(mes.length - 2, 2) + "/" + fechaMin.getYear();
		}		
		xmlLineadetalle = comun.establecerPerfilesEImportesLD(ld, perfilLD, idcliente, fechaMin, idtipoventa);
		
	}
	
	//Comprobamos si existe el elemento idcanal dentro del nodo canalByCanalventa
	if (xmlparam.canalByIdcanal.idcanal.length() == 0) {
		xmlparam.canalByIdcanal.idcanal = "";
	}
	
	//Comprobamos si existe el elemento idcliente dentro del nodo cliente
	if (xmlparam.cliente.idcliente.length() == 0) {
		xmlparam.cliente.idcliente = "";
		xmlparam.cliente.nombre = "";
		xmlparam.cliente.apellido1 = "";
		xmlparam.cliente.apellido2 = "";		
	}
	
	//Comprobamos si existe el elemento idcliente dentro del nodo cliente
	if (xmlparam.cliente.nombre.length() == 0) {
		xmlparam.cliente.idcliente = "";
	}
	
	//Comprobamos si existe el elemento numerobonoagencia dentro del nodo lineadetalle
	for each(var ld in xmlparam.lineadetalles.Lineadetalle) {
		if (ld.numerobonoagencia.length() == 0) {
			ld.numerobonoagencia = "";
		}
	}
	
	return xmlparam;
	
}
var services = context.beans.getBean('httpServiceSOA');

//TODO Investigar por que no parece llamarse desde anularVenta y se acaba llamando al default.js en su lugar.

function handle(request, response) {
	
	if (log.isInfoEnabled()){
		log.info("Entrada a anularVenta.action");
	}
	
	var methodpost = "anularVenta";
	var result;

	
	// en este caso damos prioridad al xml pasado como atributo (vendría de anularVentaPorErrorPago.action)
	// Si no existe, cogemos el pasado como parametro.
	
	var xml = request.getAttribute('xml');
	if (xml == null){
		xml = request.getParameter('xml');
	}
	
	if (log.isInfoEnabled()) {
		log.info('anularVenta.action, entrada:' + xml);
	}
	
	
	var xmlSinHijos;
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('transformerXML.js');
	
	if (xml != null) {
		xml = new XML(xml);
		xmlSinHijos = comun.borraElementosSinHijos(xml);

		// Sustitución de trues y falses en entradasimpresas, reciboimpreso e imprimirSoloEntradasXDefecto
		/*
		if (xmlSinHijos.venta.entradasimpresas.text() == 'true') {
			xmlSinHijos.venta.entradasimpresas = <entradasimpresas>1</entradasimpresas>;
		} else if (xmlSinHijos.venta.entradasimpresas.text() == 'false'){
			xmlSinHijos.venta.entradasimpresas = <entradasimpresas>0</entradasimpresas>;
		}
		if (xmlSinHijos.venta.reciboimpreso.text() == 'true') {
			xmlSinHijos.venta.reciboimpreso = <reciboimpreso>1</reciboimpreso>;
		} else if (xmlSinHijos.venta.reciboimpreso.text() == 'false'){
			xmlSinHijos.venta.reciboimpreso = <reciboimpreso>0</reciboimpreso>;
		}
		if (xmlSinHijos.venta.imprimirSoloEntradasXDefecto.text() == 'true') {
			xmlSinHijos.venta.imprimirSoloEntradasXDefecto = <imprimirSoloEntradasXDefecto>1</imprimirSoloEntradasXDefecto>;
		} else if (xmlSinHijos.venta.imprimirSoloEntradasXDefecto.text() == 'false'){
			xmlSinHijos.venta.imprimirSoloEntradasXDefecto = <imprimirSoloEntradasXDefecto>0</imprimirSoloEntradasXDefecto>;
		}
		*/
		xmlSinHijos = comun.sustituyeLiteralesBooleanosEnVenta( xmlSinHijos );
		
		var i = xmlSinHijos.modificacionimporteparcials.Modificacionimporteparcial.length();
		var elementoActual = 0;

		//Si tenemos decimales y los ponen con "," los sustituimos por "."
		while (elementoActual < i) {
			var importeParcial=xmlSinHijos.modificacionimporteparcials.Modificacionimporteparcial[elementoActual].importeparcial.importe.text();
			var vimporteParcial=importeParcial.split(",");
		
			if (vimporteParcial.length>1){
				var importeParcialMod=vimporteParcial[0]+"."+vimporteParcial[1];
				xmlSinHijos.modificacionimporteparcials.Modificacionimporteparcial[elementoActual].importeparcial.importe=importeParcialMod;
			}
		
			elementoActual ++;
		}
		
		xml = <servicio>
					<parametro>
						{xmlSinHijos}
					</parametro>
			  </servicio>
	}
	
	if (log.isInfoEnabled()) {
		log.info('Parametro pasado al servicio anularVenta:' + xml.toXMLString());
	}
	
	try{
		result = services.process(methodpost+'SOAWrapper',xml);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	if (log.isInfoEnabled()) {
		log.info('Resultado del servicio anularVenta: ' + result);
	}
	
	if (result == null) {
		result = "<ArrayList />";
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	
	response.writer.println(result);
}
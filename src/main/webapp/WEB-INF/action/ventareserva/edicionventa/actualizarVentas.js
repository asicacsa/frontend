var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {

	//log.info('entrada a actualizarVentas.action');
	
	var methodpost = request.getParameter('servicio');
	var result;

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}

	// si xml no se envia, llama al metodo sin parametros	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		xml = '<servicio />';
	} else {
		
		var xmlTratatado = preProceso(xml);

		xml = <servicio>
				<parametro>					
					{xmlTratatado}					
				</parametro>
			  </servicio>
	}

	if (log.isInfoEnabled()) {
		log.info('====> JMARIN (30.08.2018) (actualizarVentas.js): XML enviado al servicio ' + methodpost + ': ' + xml);
	}
	
	try {
			result = services.process(methodpost+'SOAWrapper',xml);
	} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
	}
		
	if (log.isInfoEnabled()) {
		log.info('	resultado del servicio: ' + result);
	}
	
	if (result == null) {
		result = "<ArrayList />";
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	
	response.writer.println(result);
}

function preProceso(xmlparam) {
	var xmlSinHijos;
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('transformerXML.js');
	
	xmlSinHijos = comun.borraElementosSinHijos(xmlparam);
	
	delete xmlSinHijos..perfiles;
	//Si no tenemos el idcanal eliminamos el canal indirecto
	if (xmlSinHijos..canalByIdcanal.idcanal.length() == 0) {
		delete xmlSinHijos..canalByIdcanal;
	}
	
	delete xmlSinHijos..importetotalventa;
	delete xmlSinHijos..importeparcialtotalventa;
	delete xmlSinHijos..importeTarifaSeleccionada;
	delete xmlSinHijos..porcentajedesc;
	delete xmlSinHijos..porcentajesDescuentoLD;
	delete xmlSinHijos..porcentajesDescuentoLDA;
	
	// JMH (05.09.2018): Borrado de los elementos CLIENTECLUB
	delete xmlSinHijos..clienteclub;
	
	//Cambiamos el nombre a la venta, ya que es un atributo y no una propiedad de un set
	for each (venta in xmlSinHijos..Venta){
		venta.setName("venta");
	}
	//xmlSinHijos..Venta.setName("venta");
	
	return xmlSinHijos;
}

var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {
	var methodpost = "obtenerVentaPorId";
	var result;

	// si xml no se envia, llama al metodo sin parametros	
	var xml = request.getParameter('xml');
	
	if (log.isInfoEnabled()){
		log.info("Entrada a obtenerVentaPorIdConPerfiles.action");
	}
	
	if(xml == null ) {
		xml = '<servicio />';
	} else {
		var xmlSinHijos;
		var sbf = context.beans.getBean('scriptBeanFactory');
		var comun = sbf.getBean('transformerXML.js');
		
		xmlSinHijos = comun.borraElementosSinHijos(xml);

		xml = <servicio>
				<parametro>
					{xmlSinHijos}
				</parametro>
			  </servicio>
	}
	
	
	try{
		result = services.process(methodpost+'SOAWrapper',xml).trim();
		
		if (log.isDebugEnabled()){
			log.debug("obtenerVentaPorIdConPerfiles.action, service response: " + result);
		}
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	if (result == null) {
		result = <ArrayList />;
	} else {
		
		result = new XML (result);
		result = postProceso(result);
	}
	if (log.isDebugEnabled()){
		log.debug("obtenerVentaPorIdConPerfiles.action, service response postProcesado: " + result);
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
}

function postProceso(xmlparam) {
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('obtenerProductoPorIdVentaReserva.js');
	var comunLD = sbf.getBean('obtenerVentaPorId.js');
	
	//Obtenemos los porcentajes de descuento que se aplicaron a cada línea de detalle
	//var importesDescuentosLdVenta = comun.retornaDescuentosYPorcentajesLDs(xmlparam.idventa, false);
	//importesDescuentosLdVenta = new XML (importesDescuentosLdVenta);
	
	//Almacenamos los descuentos de cliente que se han aplicado a la reserva
	var porcentajesDescuentoLD = <porcentajesDescuentoLD />;
 	
	//Obtenemos los perfiles para poder modificarlos de cada linea de detalle
	for each (ld in xmlparam.lineadetalles.Lineadetalle) {
		//Añado la colección de los porcentajes de cliente aplicados a la reserva, y el porcentaje correspondiente a cada ld
		comun.addPorcentajesClienteAplicados(porcentajesDescuentoLD, ld);
		xmlparam.porcentajesDescuentoLD = porcentajesDescuentoLD;
		
		//Si el producto es combinado, habrá varias Lineadetallezonasesion y la tarifa será la del producto (no es necesario pasarle el contenido)
		var idlineadetalle = null;
		var fechaMin = null;
		var idsesion = null;
		
		//Recorremos todas las sesiones para obtener la que tiene la fecha mas cercana	
		for each(var session in ld.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion) {
			
			var fechaString = session.fecha.text();
			if (fechaString.toString() != "") {
				var array = fechaString.split("-");
				var camposFecha = array[0].split("/");
				
				var fechaActual = new java.util.Date(camposFecha[2], camposFecha[1] - 1, camposFecha[0]);
							
				if (fechaMin == null || fechaActual.before(fechaMin)) {
					fechaMin = fechaActual;
					idsesion = session.idsesion;
				}
				else{
					//Si las sesiones tienen la misma fecha, nos quedamos con la de menor id
					if(fechaActual.equals(fechaMin) && (idsesion == null || idsesion > session.idsesion)){ 
						idsesion = session.idsesion;
					}
				}
			}
		}
		
		var perfilLD = new XML(comun.retornaPerfilVisitanteYDescuento(ld.producto.idproducto.text(), xmlparam.canalByIdcanal.idcanal.text(), xmlparam.cliente.idcliente.text(), idsesion, xmlparam.tipoventa.idtipoventa.text(), ld.idlineadetalle.text()));	
		
		var idtipoventa = xmlparam.tipoventa.idtipoventa.text().toString();
		
		if(isNaN(idtipoventa) || idtipoventa == "")
			idtipoventa = null;
		else
			idtipoventa = parseInt(idtipoventa);
		
		var idcliente = xmlparam.cliente.idcliente.text().toString();
		if(isNaN(idcliente) || idcliente == "")
			idcliente = null;
		else
			idcliente = parseInt(idcliente);
		
		if (fechaMin != null) {
			var dia = "0" + fechaMin.getDate();
			var mes = "0" + (Number(fechaMin.getMonth()) + 1);
	
			fechaMin = dia.substr(dia.length - 2, 2) + "/" + mes.substr(mes.length - 2, 2) + "/" + fechaMin.getYear();
		}	
		ld = comun.establecerPerfilesEImportesLD(ld, perfilLD, idcliente, fechaMin, idtipoventa);
		
	}
	
	//Comprobamos si existe el elemento idcanal dentro del nodo canalByCanalventa
	if (xmlparam.canalByIdcanal.idcanal.length() == 0) {
		xmlparam.canalByIdcanal.idcanal = "";
	}
	
	//Comprobamos si existe el elemento idcliente dentro del nodo cliente
	if (xmlparam.cliente.idcliente.length() == 0) {
		xmlparam.cliente.idcliente = "";
		xmlparam.cliente.nombre = "";
		xmlparam.cliente.apellido1 = "";
		xmlparam.cliente.apellido2 = "";		
	}
	
	//Comprobamos si existe el elemento idcliente dentro del nodo cliente
	if (xmlparam.cliente.nombre.length() == 0) {
		xmlparam.cliente.idcliente = "";
	}
	
	//Comprobamos si existe el elemento numerobonoagencia dentro del nodo lineadetalle
	for each(var ld in xmlparam.lineadetalles.Lineadetalle) {
		if (ld.numerobonoagencia.length() == 0) {
			ld.numerobonoagencia = "";
		}
	}
	
	return xmlparam;
	
}

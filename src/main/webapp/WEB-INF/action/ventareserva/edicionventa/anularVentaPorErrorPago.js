importPackage(Packages.org.springframework.web.servlet);

var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


/**
 * Anulación especial de ventas, por fallo de confirmación de pago.
 * Debe construir los objetos Modificacionimporteparcial a partir de todos los
 * importes parciales registrados para la venta, marcándolos como anulados.
 */
function handle(request, response) {
	
	if (log.isInfoEnabled()){
		log.info("Entrada a anularVentaPorErrorPago.action");
	}
	
	var methodpost;
	var importesmethod = 'obtenerImportesParcialesPorVenta';
	var importesparciales;

	var modificacion = request.getParameter('xml');
	
	if (log.isDebugEnabled()) {
		log.debug('anularVentaPorErrorPago.action, entrada:' + modificacion);
	}
	
	modificacion = new XML(modificacion);
	var envio = <servicio>
					<parametro>
						<int>
							{modificacion.venta.idventa.text()}
						</int>
					</parametro>
			  </servicio>
	
	
	// primero obtenemos los importes registrados para la venta...
	methodpost = importesmethod;
	try{
		importesparciales = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.error('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	
	
	importesparciales = new XML(importesparciales);
	
	
	// reconstruimos el objeto Modificacion insertando los importesparciales obtenidos.
	for each (var importeparcial in importesparciales.Importeparcial){
		importeparcial.setName("importeparcial");
		importeparcial.anulado = <anulado>1</anulado>;
		modificacion.modificacionimporteparcials.* += 
			<Modificacionimporteparcial>{importeparcial}</Modificacionimporteparcial>;
	}
	
	modificacion =  comun.borraElementosSinHijos(modificacion);
	
	if (log.isDebugEnabled()) {
		log.debug("anularVentaPorErrorPago.action, modificacion reconstruida: " + modificacion.toXMLString());
	}
	
	
	// añadimos el objeto como atributo del request... 
	// (El action al que se redirige dara prioridad al atributo frente al parametro)
	request.setAttribute('xml',modificacion.toXMLString());
	
	// ...y redirigimos al anularVenta normal.
	var forward = 'anularVenta.action';
	var modelAndView = new ModelAndView(forward);
	return modelAndView;
		
}

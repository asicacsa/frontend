var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {
			
	var resultado;
	var methodpost = 'obtenerImportesParcialesPorVenta';
	var envio;

	
	var xml = request.getParameter('xml');

	envio = 
		<servicio>
			<parametro>
				<int>
				{xml}
				</int>
			</parametro>
		</servicio>
			
	//Y llamamos al servicio...
	var aux = null;
	
	try {
		aux = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : ' + this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper');
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			log.error('LOG(ERROR) : con esta descripcion : '+ ex.javaException.getMessage());
			log.error('LOG(ERROR) : con esta llamada : '+ envio);
		}
		
		throw ex.javaException;
	}

	var importe=null;
	var nombre=null;
	var nuevo_resultado= <formaspago />;
	
	
	if(aux != null){
		resultado = new XML(aux);
	
		for each (var ip in resultado.Importeparcial) {
			nuevo_resultado.appendChild(<formapago>{ip.formapago.nombre.text()+' - '+ip.importe.text()+' &#x20AC;'}</formapago>);	
		}

	} else {
		resultado = new XML();
		if (log.isInfoEnabled()) {
			log.info("El resultado es null.");
		}
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(nuevo_resultado.toXMLString());
	
}
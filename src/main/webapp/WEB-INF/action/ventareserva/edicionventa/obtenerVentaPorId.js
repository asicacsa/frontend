var services = context.beans.getBean('httpServiceSOA');
function handle(request, response) {
	var entrada = new Date();
	if ( log.isInfoEnabled() ) {
		log.info("\nEntrando en obtenerVentaPorId.js en el instante " + entrada.getTime() + " ms desde 1970.\n");
	}
	var methodpost = request.getParameter('servicio');
	var result;
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	// si xml no se envia, llama al metodo sin parametros
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		xml = '<servicio />';
	} else {
		var xmlSinHijos;
		var sbf = context.beans.getBean('scriptBeanFactory');
		var comun = sbf.getBean('transformerXML.js');
		xmlSinHijos = comun.borraElementosSinHijos(xml);
		xml = <servicio>
				<parametro>
					{xmlSinHijos}
				</parametro>
			</servicio>
	}
	if ( log.isInfoEnabled() ) {
		log.info("\nEl parametro xml que llega a la llamada al servicio " + methodpost + "\nes\n" + xml + "\n")
	}
	try{
		result = services.process(methodpost+'SOAWrapper',xml);
	} catch (ex) {
		log.info('LOG(ERROR) : Error in this call : '+this.getClass().getName());
		log.info('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		log.info('LOG(ERROR) : result in this Exception : '+ex);
		throw ex.javaException;
	}
	if (result == null) {
		result = <ArrayList />;
	} else {
		result = new XML (result.trim());
		if ( log.isInfoEnabled() ) {
			log.info("\nSaliendo de obtenerVentaPorId.js antes del postproceso " + result);
		}
		result = postProceso(result);
		if ( log.isInfoEnabled() ) {
			log.info("\nSaliendo de obtenerVentaPorId.js una vez postprocesado " + result);
		}
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
	if ( log.isInfoEnabled() ) {
		log.info("\nSaliendo de obtenerVentaPorId.js tras " + ( new Date().getTime() - entrada.getTime() ) + " ms\n");
		log.info("\nSaliendo de obtenerVentaPorId.js enviando " + result);
	}
}

function postProceso(xmlparam) {
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('obtenerProductoPorIdVentaReserva.js');
	if ( log.isInfoEnabled() ) {
		log.info("\nPOSTPROCESO obtenerVentaPorId.js tras " );
	}
	//Obtenemos los porcentajes de descuento que se aplicaron a cada l�nea de detalle
	//var importesDescuentosLdVenta = comun.retornaDescuentosYPorcentajesLDs(xmlparam.idventa, false);
	//importesDescuentosLdVenta = new XML (importesDescuentosLdVenta);
	//Almacenamos los descuentos de cliente que se han aplicado a la venta
	var porcentajesDescuentoLD = <porcentajesDescuentoLD />;
	var porcentajesDescuentoLDA = <porcentajesDescuentoLDA />;
	var idtipoventa = Number(xmlparam.tipoventa.idtipoventa.text().toString());
	var idcliente = Number(xmlparam.cliente.idcliente.text().toString());
	for each (ld in xmlparam.lineadetalles.Lineadetalle) {
		//Añado la colección de los porcentajes de cliente aplicados a la reserva, y el porcentaje correspondiente a cada ld
		comun.addPorcentajesClienteAplicados(porcentajesDescuentoLD, ld);
		xmlparam.porcentajesDescuentoLD = porcentajesDescuentoLD;
		// JAN(30-04-2009): El idcliente es necesario por si hubiera tarifas especiales de cliente
		if ( log.isInfoEnabled() ) {
			log.info("\nPOSTPROCESO obtenerVentaPorId.js ld "+ld );
		}
		comun.calculaImporteMostrarLD(ld, idtipoventa, idcliente);
	}
	for each (lda in xmlparam.lineadetallesanuladas.Lineadetalle) {
		//Añado la colección de los porcentajes de cliente aplicados a la reserva, y el porcentaje correspondiente a cada ld
		comun.addPorcentajesClienteAplicados(porcentajesDescuentoLDA, lda);
		xmlparam.porcentajesDescuentoLDA = porcentajesDescuentoLDA;
	}
	//Comprobamos si existe el elemento idcanal dentro del nodo canalByCanalventa
	if (xmlparam.canalByIdcanal.idcanal.length() == 0) {
		xmlparam.canalByIdcanal.idcanal = "";
	}
	//Comprobamos si existe el elemento idcliente dentro del nodo cliente
	/* GGL 02.2021 NEW  Esto que es???? Para que?
	if (xmlparam.cliente.idcliente.length() == 0) {
		xmlparam.cliente.idcliente = "";
		xmlparam.cliente.nombre = "";
		xmlparam.cliente.apellido1 = "";
		xmlparam.cliente.apellido2 = "";
	}
	//  Comprobamos si existe el elemento idcliente dentro del nodo cliente
	if (xmlparam.cliente.length() == 0) {
		xmlparam.cliente.idcliente = "";
	}*/

	return xmlparam;

}
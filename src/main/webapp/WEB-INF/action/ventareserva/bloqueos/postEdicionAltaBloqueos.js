var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='bloquearLocalidadesZonaNoNumerada';
var servicio_edicion ='actualizarBloqueoZonaNoNumerada';

function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost;
	var envio;
	
	//log.info("Entra en el handler de postEdicionAltaBloqueos");
	
	if (xml!=null) {
	
		if (log.isInfoEnabled()) {
			log.info("Este es el xml que llega a postEdicionAltaBloqueos: " + xml);
		}
	
		xml = new XML(xml);
		xml = xml.Estadolocalidad;
		

		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicion de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.
		
		if(methodpost == null) {
			//Si es un alta no poseera la propiedad idestadolocalidad
			// ALTA
			if (!xml.hasOwnProperty('idestadolocalidad')) {
				methodpost = servicio_alta;
				xml = this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							{xml}
						</parametro>
					</servicio>
			// EDICI?N	
			} else {
				methodpost = servicio_edicion;
				xml= this.preProcessXML(xml);
				envio = 
				//	<servicio>
				//		<parametro>
				//			<list>
				//				{xml}
				//			</list>
				//		</parametro>
				//	</servicio>		
				envio = 
					<servicio>
						<parametro>
								{xml}
						</parametro>
					</servicio>			
			}
		}		
		
		if (log.isInfoEnabled()) {
			log.info("Este es el xml que se manda al servicio: " + envio);
			//log.info("Este es el metodo post: " + methodpost);
		}
		
		//Y llamamos al servicio...
		
		var aux;
		
		try{
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}	
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		resultado = 
				<ok>
					{aux}
				</ok>
					
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a postEdicionAltaBloqueos es nulo!!!");
		}
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	//log.info("Lo que vamos a pasar por el servicio : "+param);
	if (param.orden.text().length() == 0) {
			delete param.orden;
	}
	
	if (param.nombre.text().length() == 0) {
			delete param.nombre;
	}
	
	if (param.descripcion.text().length() == 0) {
			delete param.descripcion;
	}
	
	//Obligatorio : Campo dado de baja, por defecto lo ponemos a 0 ...
	if (param.dadodebaja.text().length() == 0) {
			param.dadodebaja=0;
	}
	
	//Obligatorio : Transformacion de labelValue a Model.
	/** if (param.ubicacion.hasOwnProperty('LabelValue')) {
		log.info('Esta es la ubicacion que poseemos : ');
		if (param.ubicacion.LabelValue.hasOwnProperty('value')) {
			if (param.ubicacion.LabelValue.value.text().length() != 0) {
				param.ubicacion.idubicacion = <idubicacion>{param.ubicacion.LabelValue.value.text()}</idubicacion>
			} else {
				delete param.ubicacion;
			}
		} else {
			delete param.ubicacion;
		}
		if (param.hasOwnProperty('ubicacion')) {
			delete param.ubicacion.LabelValue;
		}
	}

	if (param.recinto.hasOwnProperty('LabelValue')) {
		log.info('Esta es la ubicacion que poseemos : ');
		if (param.recinto.LabelValue.hasOwnProperty('value')) {
			if (param.recinto.LabelValue.value.text().length() != 0) {
				param.recinto.idrecinto = <idrecinto>{param.recinto.LabelValue.value.text()}</idrecinto>
			} else {
				delete param.recinto;
			}
		} else {
			delete param.recinto;
		}
		if (param.hasOwnProperty('recinto')) {
			delete param.recinto.LabelValue;
		}
	} **/
	
	return param;
	
}
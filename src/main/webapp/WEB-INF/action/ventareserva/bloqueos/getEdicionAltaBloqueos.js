var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');			
			
var model = <Estadolocalidad>
				<fecha></fecha>
				<motivobloqueo></motivobloqueo>
				<nrolocalidadesbloqueadas></nrolocalidadesbloqueadas>
				<usuario>
					<idusuario></idusuario>
					<nombre></nombre>
					<apellidos></apellidos>
				</usuario>
				<localidad/>
				<sesion>
					<idsesion></idsesion>
				</sesion>
			</Estadolocalidad>
			
			//<idestadolocalidad></idestadolocalidad>
			//<observaciones/>

			
function handle(request, response) {

	//log.info("Entrada a getEdicionAltaBloqueos.");
		
	var xml = request.getParameter('xml');
	var methodpost = request.getParameter('servicio');

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var resultado;
	
	//print("Este es el xml que llega a getEdicionAltaBloqueos: " + xml);	
	
	xml = new XML(xml);
	
	//print("Serializaci?n de xml que llega a getEdicionAltaBloqueos: " + xml.toXMLString());	
	
	//print("Valor de xml.idestadolocalidad: "+xml.idestadolocalidad);
	if (xml.idestadolocalidad != null && xml.idestadolocalidad != '') {
		
		var envio = <servicio>
						<parametro>
							<int>
								{xml.idestadolocalidad.text()}
							</int>
						</parametro>
					</servicio>
					
		//log.info("Este es el metodo al que vamos a llamar : "+methodpost);
		
		var aux;
		try{
			aux = services.process(methodpost+'SOAWrapper',envio);		
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}	
		
				
		if (log.isInfoEnabled()) {
			log.info('Esto es lo que devuelve el servicio : ' + aux);
		}
		
		if (aux != null) {
			resultado = new XML(aux);							
		}		
		
	} else {
		var now = new Date();
		resultado = <Estadolocalidad>
						<fecha>{now.getDate() + '/' + (now.getMonth()+1) + '/' + now.getFullYear()}</fecha>
						<estado>Bloqueo</estado>
						<motivobloqueo></motivobloqueo>
						<nrolocalidadesbloqueadas></nrolocalidadesbloqueadas>
						<usuario>
							<nombre>{xml.usuario.text()}</nombre>
							<apellidos></apellidos>
						</usuario>
						<sesion>
							<idsesion>{xml.idsesion.text()}</idsesion>
						</sesion>
						<zona>
							<idzona></idzona>
						</zona>
					</Estadolocalidad>;
	}
	
	//resultado = this.postProcessXML(resultado);
	if (log.isInfoEnabled()) {
		log.info("Esto es lo que devuelve getEdicionAltaBloqueos.action: " + resultado.toXMLString());
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}


function postProcessXML(xmlparam) {
	xmlparam = comun.deleteTrash(xmlparam.toXMLString());
	xmlparam = comun.totalMatch(xmlparam,'@idref');
	return xmlparam;
}
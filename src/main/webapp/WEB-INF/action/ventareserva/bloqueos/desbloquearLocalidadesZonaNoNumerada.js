var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = request.getParameter('servicio');
	var envio;
	
	//log.info("Entrada a desbloquearLocalidadesZonaNoNumerada..");
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	//log.info('antes del GET');
	var xml = request.getParameter('xml');
	//log.info('lo que llega.....'+xml.toXMLString());
	if(xml != null ) {
		
		xml = new XML(xml);
		if (log.isInfoEnabled()) {
			log.info("Este es el xml que llega desbloquearLocalidadesZonaNoNumerada: "+xml.toXMLString());
		}
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>	
			
		if (log.isInfoEnabled()) {	
			log.info("Este es el envio : "+envio);
			//log.info("Este es el metodo post : "+methodpost);
		}
		
		var respuesta;
		try{
			respuesta = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
		
		resultado = new XML(respuesta);
		
	}else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a desbloquearLocalidadesZonaNoNumeradanosBloqueados es nulo!!!");
		}
		resultado = <error/>
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
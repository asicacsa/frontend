var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	var resultado;
	var methodpost = 'renovacionMasivaAbonos';
	var envio;
		
	var xml = request.getParameter('xml');
	var idtemporada = request.getParameter('idtemporada');
	
	xml = new XML(xml);
	
	if( xml != null && idtemporada != null){

		envio = 
			<servicio>
				<parametro>
					{xml}		
					<int>{idtemporada}</int>
				</parametro>
			</servicio>
		
	}
	
	if (log.isInfoEnabled()) {
		log.info('renovacionMasivaAbonos, envío: ' + envio.toXMLString());	
	}
		
	//Y llamamos al servicio...
	var aux = services.process(methodpost+'SOAWrapper',envio);
	

	if(aux != null){
		resultado = new XML(aux);
		if (log.isInfoEnabled()) {
			log.info('## es el resultado de renovacionMasivaAbonos '+resultado);	
		}
	} else {
		resultado = new XML();
		if (log.isInfoEnabled()) {
			log.info("El resultado es null.");
		}
		throw new java.lang.Exception("Error.")
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
	
}

var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='renovacionIndividualAbonos';
//var servicio_edicion ='actualizarAbonos';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	
	var resultado;
	var methodpost = 'renovacionIndividualAbonos';
	var envio;
	
		
	if (xml != null) {
		xml = new XML(xml);
		
		var idabonooriginal = xml.Venta.idabonooriginal.text();
		xml = this.preProcessXML(xml);
		
		xml = xml.Venta;
		xml.setName('venta');
		
		
		envio = 
			<servicio>
				<parametro>
					<renovacionIndividualAbonos>
						{xml}
						<mantienelocalidad>0</mantienelocalidad>
						<idabonooriginal>{idabonooriginal}</idabonooriginal>				
					</renovacionIndividualAbonos>
				</parametro>
			</servicio>			
				
			
			var aux = services.process(methodpost+'SOAWrapper',envio);
		}
		if(aux != null){		
			aux = new XML(aux);
			resultado = 
					<ok>
						{aux}
					</ok>			
			}
		else {
			if (log.isInfoEnabled()) {
				log.info("El xml que llega a renovacionIndividualAbonos es nulo!!!");
			 }
			resultado = <error/>
		}			
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
	}


function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	//log.info("Lo que vamos a pasar por preProcessXML : "+param);
	
	param = new XML(param);
	
	
	delete param.Venta.lineadetalles.Lineadetalle.perfiles;
	delete param.Venta.lineadetalles.Lineadetalle.descuentos;
	delete param.Venta.idabonooriginal;
	delete param..nombrecompleto;
	
	if(param.Venta.isBono == true){
	
		delete param.Venta.importeparcials;	
		param.Venta.importeparcialsBono.setName('importeparcials');
		delete param.Venta.isBono;
		//log.info('es un bono agencia y asi queda despues de eliminar y cambiar el nombre: '+param);	
	}
	else{
	//log.info('NO es un Bono agencia y tengo q envia los Importes');
		delete param.Venta.importeparcialsBono;
		delete param.Venta.isBono;
	}
	
	if (param.Venta.entradasimpresas.text() == false) {
			param.Venta.entradasimpresas=<entradasimpresas>0</entradasimpresas>
		} else if (param.Venta.entradasimpresas.text() == true) {
			param.Venta.entradasimpresas=<entradasimpresas>1</entradasimpresas>
		} else {
			//log.warn("LOG(WARN) : postRealizarVent. El dato activada llega con valor distinto a 0 ? 1.")
		}
	if (param.Venta.reciboimpreso.text() == false) {
			param.Venta.reciboimpreso=<reciboimpreso>0</reciboimpreso>
		} else if (param.Venta.reciboimpreso.text() == true) {
			param.Venta.reciboimpreso=<reciboimpreso>1</reciboimpreso>
		} else {
			//log.warn("LOG(WARN) : postRealizarVent. El dato activada llega con valor distinto a 0 ? 1.")
		}
	if (param.Venta.financiada.text() == false) {
			param.Venta.financiada=<financiada>0</financiada>
		} else if (param.Venta.financiada.text() == true) {
			param.Venta.financiada=<financiada>1</financiada>
		} else {
			//log.warn("LOG(WARN) : postRealizarVent. El dato activada llega con valor distinto a 0 ? 1.")
		}
	
	param = comun.borraElementosSinHijos(param.toXMLString());
	//log.info('lo que llega: '+param);
	if(param.Venta.importeparcials.length() == 1){
		//log.info('Solo hay un Importeparcial');
		for each(i in param.Venta.importeparcials.Importeparcial){
			if(i.importe.text() == 0){
				delete param.Venta.importeparcials[i.childIndex()];
					
			}
		}
	}


	return param;
	
}
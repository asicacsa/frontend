//var service = context.beans.getBean('httpServiceSOA');

// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = request.getParameter('servicio');
	
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	
	
	if(xml == null ) {
		
		var envio = '<servicio><parametro><buscarabonosrenovacion/></parametro></servicio>'
	} else {
		xml = new XML(xml);
		
		xml = this.preProcessXML(xml);
		
		
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>	
		log.info("Este es el envio : "+envio);	
	}
	
	var respuesta = services.process(methodpost+'SOAWrapper',envio);
	//var respuesta = new XML(service.process(methodpost,envio));
	
	if (respuesta!=null) {
		respuesta = new XML(respuesta);
		//log.info("LOG(INFO): valor de la respuesta antes de ser postprocesada: " + respuesta);
		respuesta = this.postProcessXML(respuesta);	
		//log.info("Resultado de llamar a buscarCaja: "+respuesta.toXMLString());

	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
	
}


function preProcessXML(param) {

	param = comun.borraElementosSinHijos(param.toXMLString());

	param = comun.deleteSelectedTag(param.toXMLString());
	param = new XML(param);
		
	return param;
	
}
function postProcessXML(xmlparam) {
	if (xmlparam != null){
		xmlparam = new XML(xmlparam);
		
		for each (var abonado in xmlparam.Abono) {
			abonado.nombreabonado = abonado.nombreabonado.text() + ' ' + abonado.apellido1.text() + ' ' + abonado.apellido2.text();			
		}
	}
	
	return xmlparam;
}
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
		
	var idabono = request.getParameter('idabono');		
	var resultado;
	var methodObtenerDatosRenovacion = 'obtenerDatosRenovacionPorAbono';
	var getxmlmodel = 'getXMLModel';
	var methodpost = '';
	var envio;
	
	if (idabono != '') {
	
		methodpost = methodObtenerDatosRenovacion;
			envio = 
				<servicio>
					<parametro>
						<int>{idabono}</int>
					</parametro>
				</servicio>		
		
		var datosrenovacion = services.process(methodpost+'SOAWrapper',envio);		
		var datosrenovacion = new XML(datosrenovacion);
		log.info('Los datos del CLiente...'+datosrenovacion);
		var clienteRenovacion = datosrenovacion.cliente;
		
		
		methodpost = getxmlmodel;
			envio = 
				<servicio>
					<parametro>
						<Venta/>
					</parametro>
				</servicio>		

		var venta = services.process(methodpost+'SOAWrapper',envio);
		var venta = new XML(venta);
		var idabonooriginal = <idabonooriginal>{idabono}</idabonooriginal>;
		
		venta.cliente = clienteRenovacion;
		venta.appendChild(idabonooriginal);
		
		//por defecto el cp del cliente es el de la venta
		venta.cp = clienteRenovacion.direccionorigen.cp.cp;
		
		
		if(venta != null){
			venta = new XML(venta);
			resultado = venta;				
		}else{			
			resultado = <error/>		
		}
		
		if (log.isInfoEnabled()) {
			log.info('La respuesta es: '+venta);
		}
	} else if(idabono == ''){
		
		methodpost = getxmlmodel;
			envio = 
				<servicio>
					<parametro>
						<Venta/>
					</parametro>
				</servicio>		

		var venta = services.process(methodpost+'SOAWrapper',envio);
		
		if(venta != null){
			venta = new XML(venta);
			resultado = venta;				
		}else{			
			resultado = <error/>		
		}	
	
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
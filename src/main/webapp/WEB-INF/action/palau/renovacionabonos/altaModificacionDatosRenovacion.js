var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='altaModificacionDatosRenovacion';

function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	
	var resultado;
	
	var methodpost;
	
	var envio;
	

	if (xml!=null) {
		xml = new XML(xml);
		
		xml = this.preProcessXML(xml);
		
		methodpost = servicio_alta;
		
		
		envio = 
			<servicio>
				<parametro>
					{xml}
				</parametro>
			</servicio>			
		
		
		if (log.isInfoEnabled()) {		
			log.info("Este es el envio : "+envio);
			log.info("Este es el metodo post : "+methodpost);
		}
		
		//Y llamamos al servicio...		
		var aux = services.process(methodpost+'SOAWrapper',envio);
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		
		resultado = 
				<ok>
					{aux}
				</ok>
		//En aux tenemos la respuesta			
	} else {
		resultado = <error/>
	}
	
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	
	var sbf = context.beans.getBean('scriptBeanFactory');
	var comun = sbf.getBean('transformerXML.js');
	delete param..selected; //Eliminamos todos los tags selected
	
	
	
	if(param.hasOwnProperty('abono') == false){
	
		param.setName('Datosrenovacion');
		
		var abono = <abono></abono>;
		//var idabono = <idabono>{txtidabono}</idabono>;
		abono.appendChild(param.idabono);
		abono.appendChild(param.observaciones);
		abono.appendChild(param.renovable);
		abono.appendChild(param.motivorenovacion);

		param.appendChild(abono);
		
		
		
		delete param.nroabonado;
		delete param.tipoabono;
		delete param.localidad;
		delete param.lineadetalle;
		delete param.idabono;
		delete param.domicilio;
		delete param.anulado;
		delete param.bloqueado;
		delete param.observaciones;
		delete param.motivorenovacion;
		delete param.validado;
		delete param.renovable;
		delete param.cliente.nombrecompleto
		delete param.ndImporteNuevoAbono; //nodo que creo en el ondata pero que solo es de lectura
		log.info(param);
	}
		//Obligatorio : Campo dado de baja, por defecto lo ponemos a 0 ...
	if (param.dadodebaja.text().length() == 0) {
			param.dadodebaja=0;
	}
	
	delete param.cliente.nombrecompleto
	delete param.ndImporteNuevoAbono; //nodo que creo en el ondata pero que solo es de lectura
	
	if (param.solicitafactura.text() == true) {
		param.solicitafactura=<solicitafactura>1</solicitafactura>
	} else if (param.solicitafactura.text() == false) {
		param.solicitafactura=<solicitafactura>0</solicitafactura>
	} else {
		//por defecto es 0
		param.solicitafactura=<solicitafactura>0</solicitafactura>
		//log.warn("LOG(WARN) : altaModificacionDatosRenovacion. El dato nonumerada llega con valor distinto a true ? false.")
	}
	if (param.personalizar.text() == true) {
		param.personalizar=<personalizar>1</personalizar>
	} else if (param.personalizar.text() == false) {
		param.personalizar=<personalizar>0</personalizar>
	} else {
		//por defecto es 0
		param.solicitafactura=<solicitafactura>0</solicitafactura>
		//log.warn("LOG(WARN) : altaModificacionDatosRenovacion. El dato nonumerada llega con valor distinto a true ? false.")
	}
	
	//por defecto el punto de recogida es en Taquillas
	if (param.puntorecogida.text().length() == 0) {		
		param.puntorecogida=<puntorecogida>0</puntorecogida>
	} 
	
	
	param = comun.borraElementosSinHijos(param); //Eliminamos todos los elementos que se han quedado sin hijos
	
	return param;
	
}
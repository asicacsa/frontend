var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {
		
	var idplano = request.getParameter('idplano');
	var zonafisica = request.getParameter('zonafisica');
	var resultado;
	var envio;
	
	if (log.isInfoEnabled()) {
		log.info("Entrada a obtenerLocalidadesPorPlanoyZona. idplano:" + idplano + " zonafisica:" + zonafisica);
	}
	
	if (idplano!=null && zonafisica!=null) {
				
		envio = 
			<servicio>
				<parametro>
					<idplano>
						{idplano}
					</idplano>
					<zonafisica>
						{zonafisica}
					</zonafisica>
				</parametro>
			</servicio>		
		
		if (log.isInfoEnabled()) {	
			log.info("Este es el envio : "+envio);
		}
		
		//Y llamamos al servicio...
		resultado = new XML(services.process('obtenerLocalidadesPorPlanoyZonaSOAWrapper',envio));
		
		//Reordenamos rutas de propiedades (idzona, color, ...)
		resultado = reordenarPropiedades(resultado);
		
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a obtenerLocalidadesPorPlanoyZona es nulo!!!");
		}
		resultado = <error/>
	}
	
	if (log.isInfoEnabled()) {
		log.info("Resultado obtenerLocalidadesPorPlanoyZona: " + resultado.toXMLString().substr(0,300) + "[...]");
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}



function reordenarPropiedades(param) {
	
	for each (var localidad in param.localidad){
		localidad.@idzona = localidad.zona.@idzona;
		localidad.@c = localidad.zona.@color;
		//log.info('reordenarPropiedades, localidad: ' + localidad.toXMLString());
		delete localidad.zona;
	}
	
	return param;
	
}
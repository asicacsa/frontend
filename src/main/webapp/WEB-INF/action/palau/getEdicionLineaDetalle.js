var services = context.beans.getBean('httpServiceSOA');
//var servicio_alta ='realizarVentaAbonos';
//var servicio_edicion ='actualizarAbonos';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {	

	var methodpost="obtenerLineaDetallePorId";
	

	// si xml no se envia, llama al metodo sin parametros	
	var selecciona = null;
	
	enviar_id = request.getParameter('xml');

	
	// En este tipo de servicio no van a haber parametros de Entrada.
	var envio = <servicio>
					<parametro>
						<int>{enviar_id}</int>
					</parametro>
				</servicio>;
	
	result = services.process(methodpost+'SOAWrapper',envio);

	
	if (result !=  null) {
		result = new XML(result);
		result = this.postProcessXML(result);		
	}

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result.toXMLString());
	
}


function postProcessXML(param) {

	if (param.descuentopromocional.hasOwnProperty("iddescuentopromocional")==false){
		param.descuentopromocional.iddescuentopromocional=<iddescuentopromocional></iddescuentopromocional>
	}

	return param;
	
}
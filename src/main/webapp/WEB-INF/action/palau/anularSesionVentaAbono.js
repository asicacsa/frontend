// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = request.getParameter('servicio');
	
	//log.info("Entrada a anularAbonos.action-->" + methodpost);
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var idsesion = request.getParameter('idsesion');
	var idabono = request.getParameter('idabono');
	var modificacion = request.getParameter('modificacion');

	//log.info("###idabono: " + idabono);
	//log.info("###idsesion: " + idsesion);
	//log.info("###modificacion: " + modificacion);

	
	if(idsesion == null ) {
		var envio = <servicio/>;
		if (log.isInfoEnabled()) {
			log.info("### Error en la peticon a anulacion de sesiones de Abono ");
		}
	} else {
		xml = new XML(modificacion);

	
		xmlSinHijos = comun.borraElementosSinHijos(xml);

		//Borramos Venta
		delete xmlSinHijos.venta

		var i = xmlSinHijos.modificacionimporteparcials.Modificacionimporteparcial.length();
		var elementoActual = 0;

		//Si tenemos decimales y los ponen con "," los sustituimos por "."
		while (elementoActual < i) {
			var importeParcial=xmlSinHijos.modificacionimporteparcials.Modificacionimporteparcial[elementoActual].importeparcial.importe.text();
			var vimporteParcial=importeParcial.split(",");
		
			if (vimporteParcial.length>1){
				var importeParcialMod=vimporteParcial[0]+"."+vimporteParcial[1];
				xmlSinHijos.modificacionimporteparcials.Modificacionimporteparcial[elementoActual].importeparcial.importe=importeParcialMod;
			}
		
			elementoActual ++;
		}

		
		envio = <servicio>
					<parametro>
						<abono>
							<idabono>
								{idabono}
							</idabono>
						</abono>
						<sesion>
							<idsesion>
								{idsesion}
							</idsesion>
						</sesion>
						{xmlSinHijos}						
					</parametro>
				</servicio>	
	}
	
	//log.info("Par?metro enviado al servicio anularAbonos: " + envio.toXMLString());
	services.process(methodpost+'SOAWrapper',envio.toXMLString());

	//var respuesta = new XML(service.process(methodpost,envio));
	var respuesta = <ok/>;
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
}
var services = context.beans.getBean('httpServiceSOA');
var servicio_edicion ='actualizarLineadetallesVenta';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	var xml = request.getParameter('xml');		



	var resultado;
	var methodpost;
	var envio;
	
	//log.info(" ### Entra en el handle de postEdicionLineaDetalle." + xml) ;
	
	if (xml!=null) {
		xml = new XML(xml);
		//log.info("Este es el xml que llega a postEdicionLineaDetalle : "+xml.toXMLString());
		


		methodpost = servicio_edicion;
		xml= this.preProcessXML(xml);
		envio = 
			<servicio>
				<parametro>
						{xml}
				</parametro>
			</servicio>		
			
		//log.info(" ##### Este es el envio : "+envio);
		//log.info(" ##### Este es el metodo post : "+methodpost);
	
		envio = this.preProcessXML(envio);

		//Y llamamos al servicio...
		var aux = services.process(methodpost+'SOAWrapper',envio);		
			
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		resultado = <ok>{aux}</ok>;
		

					
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a postEdicionLineaDetalle  es nulo!!!");
		}
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	//log.info("Lo que vamos a pasar por preProcessXML : "+param);
	
	delete param.modificacionlineadetalles.Modificacionlineadetalle.lineadetalle.lineadetallezonasesions
	
	
	param = new XML(param);
	param = comun.borraElementosSinHijos(param.toXMLString());

	return param;
	
}
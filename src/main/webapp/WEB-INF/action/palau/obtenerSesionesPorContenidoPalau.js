var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost = 'obtenerSesionesPorContenido';
	var envio;
	
	//log.info("Entra en el action de obtenerSesionesPorContenido.");
	
	if (xml!=null) {
		xml = new XML(xml);
				
		if (log.isInfoEnabled()) {
			log.info("Este es el xml que llega a obtenerSesionesPorContenido: "+xml.toXMLString());	
		}

		//xml= this.preProcessXML(xml);
		

		envio = 
			<servicio>
				<parametro>
					{xml}
				</parametro>
			</servicio>			
		
		if (log.isInfoEnabled()) {
			log.info("Este es el envio : "+envio);
			log.info("Este es el metodo post : "+methodpost);
		}
		
		//Y llamamos al servicio...
		var aux = services.process(methodpost+'SOAWrapper',envio);
				
		if(aux != null){
		
			aux = new XML(aux);
			resultado = aux;
			resultado = this.postProcessXML(aux);
			
			//log.info("Este es el resultado despues del POST proceso: "+resultado.toXMLString());			
		}			
		
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		//resultado = new XML(aux);
					
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a obtenerSesionesPorContenido es nulo!!!");
		}
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
function postProcessXML(param){
	//log.info('en la funcion postProcessXML y lo q llega '+param);
	/*<Sesion>
    <idsesion>1021</idsesion>
    <horainicio>19:30</horainicio>
    <fecha>07/01/2007-00:00:00</fecha>
    <bloqueado/>
    <contenido>
      <idcontenido>1008</idcontenido>
      <nombre>LA BELLE ET LA B?TE</nombre>
      <tipoproducto>
        <recinto>
          <zonas>
            <Zona>
              <idzona>1001</idzona>
              <nombre>Zona 1</nombre>
            </Zona>
            <Zona>
              <idzona>1005</idzona>
              <nombre>Zona 5</nombre>
            </Zona>
            <Zona>
              <idzona>1003</idzona>
              <nombre>Zona 3</nombre>
            </Zona>
            <Zona>
              <idzona>1002</idzona>
              <nombre>Zona 2</nombre>
            </Zona>
            <Zona>
              <idzona>1006</idzona>
              <nombre>Zona 6</nombre>
            </Zona>
            <Zona>
              <idzona>1004</idzona>
              <nombre>Zona 4</nombre>
            </Zona>
          </zonas>
        </recinto>
      </tipoproducto>
    </contenido>
  </Sesion>*/
	
	for each (i in param.Sesion.contenido.tipoproducto.recinto.zonas.Zona){
		//log.info('en el for each');
		//log.info('Esto es i '+i);
		i.setName('zona');
		//log.info('Despues del Change of the Name '+i);
	
	}
	
	return param;
}






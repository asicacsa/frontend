var services = context.beans.getBean('httpServiceSOA');

var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
	
	//log.info('Hemos entrado en actualizarImportesParciales');	

	var importesParciales = request.getParameter('dpGrid');		
	var motivoModificacion = request.getParameter('motivoMod');		
	var idVenta = request.getParameter('idVenta');		
	
	
	var resultado;
	var methodpost = 'actualizarImportesParciales';
	var envio;
		
	importesParciales = new XML(importesParciales);
	
	var xml = <dto>
	 			<importeparcialparams>
	 			</importeparcialparams>
	 			<idventa>{idVenta}</idventa>
	 			<motivomodificacion>{motivoModificacion}</motivomodificacion>
			  </dto>;
	if (log.isInfoEnabled()) {
		log.info("El dto a enviar (antes transformaciones) : "+xml.toXMLString());
	}
	if (importesParciales.descendants('Importeparcial').length() != 0){		
		if (log.isInfoEnabled()) {
			log.info("Dentro del if : ");
		}	
		for each(i in importesParciales.Importeparcial){
			if (log.isInfoEnabled()) {
				log.info("Iterando ImporteParcial : "+i.toXMLString());
			}		
			nodo = <ImporteParcialParam/>;
			//delete i.venta.importetotalventa;
			i.setName('importeparcial');
			
			nodo.appendChild(i.venta.operacioncaja.caja.idcaja);
			delete i.venta;
					
			nodo.appendChild(i);
			
			xml.importeparcialparams.appendChild(nodo);						
					
		}			
	} 
	
	if (log.isInfoEnabled()) {
		log.info("Antes del preproceso : "+xml.toXMLString());
	}

	xml = this.preProcessXML(xml);
	envio = 
		<servicio>
			<parametro>
				{xml}
			</parametro>
		</servicio>
		
	if (log.info) {
		log.info("Este es el envio : "+envio);	
	}
		
	
		//Y llamamos al servicio...
		var aux = services.process(methodpost+'SOAWrapper',envio);
		
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		if(aux != null){
		//log.info('aux es distinto de nulo');
			aux = new XML(aux);
			resultado = aux;
			if (log.isInfoEnabled()) {	
				log.info("Este es el resultado : "+resultado);
			}
		} else {
			resultado = new XML('<ok/>');
			if (log.isInfoEnabled()) {
				log.info("El aux es null");
			}
		}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
	
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	//log.info("Lo que vamos a pasar por preProcessXML : "+param);
	
	param = new XML(param);
	
	
	param = comun.borraElementosSinHijos(param.toXMLString());

	//log.info("Lo que enviamos preparado : "+param);

	return param;	
}
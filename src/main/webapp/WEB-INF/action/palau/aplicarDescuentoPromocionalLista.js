var services = context.beans.getBean('httpServiceSOA');
//var servicio_alta ='realizarVentaAbonos';
//var servicio_edicion ='actualizarAbonos';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
	
	
	if (log.isInfoEnabled()){
		log.info('Entrada a aplicarDescuentoPromocionalLista.action.');	
	}
		
	var resultado;
	var methodpost = 'aplicarDescuentoPromocionalLista';
	var envio;
		
	var lalinea = request.getParameter('lalinea');
	var lineas = request.getParameter('lineas');
	var iddescuento = request.getParameter('iddescuento');
	var idtipoventa = request.getParameter('idtipoventa');

	

	//var clienteventa = request.getParameter('clienteventa');
	
	lalinea = new XML(lalinea);
	lineas = new XML(lineas);
	iddescuento = new String(iddescuento);

	lalinea.setName("Lineadetalle");

	
	
	var idestadolocalidadDeLaLinea = lalinea.estadolocalidads.Estadolocalidad.idestadolocalidad.text();
	var idLocalidadDeLaLinea = lalinea.abonos.Abono.localidad.idlocalidad.text();
	var idTipoabonoDeLaLinea = lalinea.abonos.Abono.tipoabono.idtipoabono.text();
		
	
	if (log.isInfoEnabled()){
		log.info("aplicarDescuentoPromocionalLista :idestadolocalidadDeLaLinea: " + idestadolocalidadDeLaLinea);
		log.info("aplicarDescuentoPromocionalLista: idLocalidadDeLaLinea: " + idLocalidadDeLaLinea);
		log.info("aplicarDescuentoPromocionalLista: idTipoabonoDeLaLinea: " + idTipoabonoDeLaLinea);
		log.info("aplicarDescuentoPromocionalLista: lalinea: " + lalinea.toXMLString());
		log.info("lineas: " + lineas.toXMLString());
		log.info("aplicarDescuentoPromocionalLista: iddescuento: " + iddescuento);
		log.info("aplicarDescuentoPromocionalLista: idtipoventa: " + idtipoventa);
	}
	
	var xml = <dto>
	 			<lineadetalles>
	 			</lineadetalles>
	 			<idtipoventa>{idtipoventa}</idtipoventa>
			  </dto>;
	
	xml.lineadetalles.appendChild(lalinea);	
		  
	xml = this.preProcessXML(xml);
	envio = 
		<servicio>
			<parametro>
				{xml}
			</parametro>
		</servicio>

	
	if (log.isInfoEnabled()){
		log.info("aplicarDescuentoPromocionalLista, envio: " + envio.toXMLString());
	}	
	
	//Y llamamos al servicio...
	var aux = services.process(methodpost+'SOAWrapper',envio);
	
	if (log.isInfoEnabled()){
		log.info("aplicarDescuentoPromocionalLista, retorno servicio: " + aux);
	}	
	
	
//busco la linea con el mismo idestadolocalidad, en las Lineas y con la q me devuelve el servicio
//saco el index q tenia al principio la linea a la q se le va a aplicar el descuento y hago un Replace con la nueva
	if(aux != null){
		resultado = new XML(aux);
		//log.info('Esto ya es el resultado....dentro del AUx != Null.......'+resultado);	
		
		if(idtipoventa == '3'){
			//log.info('El idtipoVenta es 3....');

			if (lineas!=""){

				for each (i in lineas.Lineadetalle) {					
					if (i.estadolocalidads.Estadolocalidad.hasOwnProperty('idestadolocalidad')) {
						if (i.estadolocalidads.Estadolocalidad.idestadolocalidad.text() == idestadolocalidadDeLaLinea) {
							lineas.replace(i.childIndex(), resultado.Lineadetalle);				
						}
					}				
				}

				resultado = this.postProcessXML(lineas);
			}

		}else if(idtipoventa == '1'){
			//log.info('El idtipoVenta es 1....');

			    if (lineas!=""){

					for each (i in lineas.Lineadetalle) {	
								
						if ((i.abonos.Abono.localidad.hasOwnProperty('idlocalidad')) && (i.abonos.Abono.tipoabono.hasOwnProperty('idtipoabono'))) {
							if ((i.abonos.Abono.localidad.idlocalidad.text() == idLocalidadDeLaLinea) && (i.abonos.Abono.tipoabono.idtipoabono.text() == idTipoabonoDeLaLinea)){
								lineas.replace(i.childIndex(), resultado.Lineadetalle);				
							}
						}				
					}
					resultado = this.postProcessXML(lineas);
				}

			}

		
		
		//log.info("Las lineas con Reemplazo del nuevo NODO: " + lineas.toXMLString());
	} else {
		resultado = new XML();
		if (log.isInfoEnabled()) {
			log.info("El resultado es null.");
		}
	}
	
	if (log.isInfoEnabled()) {
		log.info("aplicarDescuentoPromocionalLista, resultado final: " + resultado.toXMLString());
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
	
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	//log.info("Lo que vamos a pasar por preProcessXML : "+param);
	
	param = new XML(param);
	
	param = comun.borraElementosSinHijos(param.toXMLString());

	//log.info("Lo que enviamos preparado : "+param);

	return param;
	
}
function postProcessXML(param) {
	//log.info('en la Function postProcess..'+param);
	param = new XML(param);
	
	var nd = <iddescuentopromocional/>;
		
	for each(i in param.Lineadetalle.descuentopromocional){
		
		if (i.descendants('iddescuentopromocional').length() == 0){
			i.appendChild(nd);	
			}
		}
	//log.info("Lo que enviamos postProcesadoooooo : "+param);
	return param;
}
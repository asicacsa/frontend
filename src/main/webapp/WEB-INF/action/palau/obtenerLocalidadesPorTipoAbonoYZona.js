var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {
		
	var idTipoAbono = request.getParameter('idTipoAbono');
	var zonafisica = request.getParameter('zonafisica');
	var idusuario = request.getParameter('idusuario');
	var resultado;
	var envio;
	
	if (log.isInfoEnabled()) {
		//log.info("Estamos en obtenerLocalidadesPorTipoAbonoYZona");
		log.info("idTipoAbono: " + idTipoAbono);
		log.info("zonafisica: " + zonafisica);
	}
	
	if (idTipoAbono!=null && zonafisica!=null) {
				
		envio = 
			<servicio>
				<parametro>
					<idTipoAbono>
						{idTipoAbono}
					</idTipoAbono>
					<zonafisica>
						{zonafisica}
					</zonafisica>
				</parametro>
			</servicio>		
		
		if (log.isInfoEnabled()) {	
			log.info("Este es el envio : "+envio);
		}
		
		//Y llamamos al servicio...
		resultado = new XML(services.process('obtenerLocalidadesPorTipoAbonoYZonaSOAWrapper',envio));
		
		//Cambiamos los estados a los que realmente son
		resultado = obtenerEstadoReal(resultado, idusuario);
		
		//Establecemos si es de minusvalidos o no
		resultado = establecerMinusvalidos(resultado);
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a obtenerLocalidadesPorTipoAbonoYZona es nulo!!!");
		}
		resultado = <error/>
	}
	
	//log.info("El resultado: ");
	//log.info(resultado.toXMLString());
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function obtenerEstadoReal(param, usuario) {
	var MAX_ESTADOS = 10;
	
	if (log.isInfoEnabled()) {
		log.info("El usuario que me llega: " + usuario);
	}
	
	var xmlEstado3 = param.localidad.(@estado=="3");
	
	for (var i = 0; i < xmlEstado3.length(); i ++) {
		//Si hay mas de una prerreserva o el usuario no coincide con nosotros la tenemos que poner como prerreservada por otro usuario
		if (xmlEstado3[i].@cuenta > 1 || xmlEstado3[i].@user != usuario) {
			xmlEstado3[i].@estado = MAX_ESTADOS;			
		} else {
			//En este caso no hay que hacer nada
		}	
	}

	return param;
	
}

function establecerMinusvalidos(param) {
	var xmlEstado3 = param.localidad.(@estado=="3");
	
	for (var i = 0; i < param.localidad.length(); i ++) {
		//Los valores del campo minsv pares son de minusvalidos, los impares no. Se base en los valores de la base de datos
		if (param.localidad[i].@minusv % 2 == 0) {
			param.localidad[i].@minusv = 1;
		} else {
			param.localidad[i].@minusv = 0;
		}
	}
	
	return param;
	
}
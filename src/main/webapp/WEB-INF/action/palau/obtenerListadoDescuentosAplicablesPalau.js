var services = context.beans.getBean('httpServiceSOA');

var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {

	if (log.isInfoEnabled()){
		log.info('Entrada a obtenerListadoDescuentosAplicablesPalau.');
	}
	
	//var methodpost = request.getParameter('servicio');	
	var methodpost = 'obtenerListadoDescuentosAplicables';
	
	var idcanal = request.getParameter('idcanal');
	var idproducto = request.getParameter('idproducto');
	var idtarifa = request.getParameter('idtarifa');
	var idperfilvisitante = request.getParameter('idperfilvisitante');
	var idsesion = request.getParameter('idsesion');
	
	if (log.isInfoEnabled()){
		log.info('idcanal ' + idcanal);
		log.info('idproducto ' + idproducto);
		log.info('idtarifa ' + idtarifa);
		log.info('idperfilvisitante ' + idperfilvisitante);
		log.info('idsesion ' + idsesion);
	}
	
	var envio = <servicio>
					<parametro>
						<es.di.cac.ticketing.services.administracion.params.ObtenerListadoDescuentosAplicablesParam>
							<tarifa>
								<idtarifa>{idtarifa}</idtarifa>
							</tarifa>
							<perfilvisitante>
								<idperfilvisitante>{idperfilvisitante}</idperfilvisitante>
							</perfilvisitante>						
							<producto>
								<idproducto>{idproducto}</idproducto>
							</producto>
							<canal>
								<idcanal>{idcanal}</idcanal>
							</canal>
							<listasesiones>
								<es.di.cac.ticketing.model.Sesion>
									<idsesion>{idsesion}</idsesion>
								</es.di.cac.ticketing.model.Sesion>
							</listasesiones>
						</es.di.cac.ticketing.services.administracion.params.ObtenerListadoDescuentosAplicablesParam>
					</parametro>
				</servicio>

	
	var result;
	
	try {
		envio = preProcessXML(envio);
		if (log.isInfoEnabled()) {
			log.info("obtenerListadoDescuentosAplicablesPalau, envío : " + envio.toXMLString());
		}
		result = services.process(methodpost+'SOAWrapper',envio.toXMLString());
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call: ' + this.getClass().getName());
			log.error('LOG(ERROR) : to this: '+methodpost+'SOAWrapper');
			log.error('LOG(ERROR) : result in this Exception: '+ex);
			log.error('LOG(ERROR) : with this description: '+ ex.javaException.getMessage());
			log.error('LOG(ERROR) : in call: '+ envio);
		}
		
		throw ex.javaException;
	}
	
	if (log.isInfoEnabled()) {
		log.info("obtenerListadoDescuentosAplicablesPalau, resultado: " + result);
	}
		
	//if (result != null) {
	//	result = new XML(result);
	//	//result = this.postProcessXML(result, selecciona);		
	//}
	

	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(result);
	
}

function preProcessXML(param) {
	// param es string
	param = comun.borraElementosSinHijos(param);

	return param;	
}
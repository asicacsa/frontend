var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {
	//log.info(request);	
	var idContenido = request.getParameter('idContenido');
	
	var fechahora;
	var hora;
	var resultado;
	var envio;
	
	//log.info("Estamos en obtenerSesionesPorContenido.action");
	//log.info("idContenido: " + idContenido);
	
	
	if (idContenido!=null) {
				
		envio = 
			<servicio>
				<parametro>
					<idContenido>
						{idContenido}
					</idContenido>
				</parametro>
			</servicio>		
				
		//log.info("Este es el envio : "+envio);
		
		//Y llamamos al servicio...
		//La respuesta nos llegaria en un String por lo que la pasamos a objeto XML para poder manejarla mejor 
		resultado = new XML(services.process('obtenerSesionesPorContenidoSOAWrapper',envio).trim());
		var sesiones = resultado.Sesion;
		//log.info('Sesiones.........................:'+sesiones.toXMLString());
		
		//Resultado sera un list con todas las sesiones de ese contenido
		for (var i = 0; i<sesiones.length(); i++){		
				fechahora = sesiones[i].fecha;				
				fechahora = fechahora.split('-');				
				fechahora = fechahora[0];
				sesiones[i].fecha = fechahora;				
				hora = sesiones[i].horainicio;
				fechahora = fechahora + ' - ' + hora;
				//Añadimos un nuevo nodo fechahora
				sesiones[i].fechahora = fechahora;
				//log.info('Fechahora.........................:'+sesiones[i].fechahora);
				
		}
		
		
	} else {
		//log.info("El xml que llega a obtenerSesionesPorContenido es nulo!!!");
		resultado = <error/>
	}
		
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

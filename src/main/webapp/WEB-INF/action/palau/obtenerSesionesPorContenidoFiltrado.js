var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	//log.info(request);	
	var idContenido = request.getParameter('idContenido');
	var listaSesiones= request.getParameter('listaSesiones');
	
	var fechahora;
	var hora;
	var resultado;
	var envio;
	
	if (log.isInfoEnabled()) {
		//log.info("Estamos en obtenerSesionesPorContenidoFiltrado.action");
		log.info(" ## idContenido: " + idContenido);
		log.info(" ## listaSesiones: " + listaSesiones);
	}


	listaSesiones = new XML(listaSesiones);

	listaSesiones = comun.borraElementosSinHijos(listaSesiones.toXMLString());
	
	if (idContenido!=null) {
				

		envio = 
			<servicio>
				<parametro>
					{listaSesiones}
					<idContenido>
						{idContenido}
					</idContenido>
				</parametro>
			</servicio>		
				
		//log.info("Este es el envio : "+envio);
		
		//Y llamamos al servicio...
		//La respuesta nos llegaria en un String por lo que la pasamos a objeto XML para poder manejarla mejor 
		resultado = new XML(services.process('obtenerSesionesPorContenidoFiltradoSOAWrapper',envio));
		var sesiones = resultado.Sesion;
		//log.info('Sesiones.........................:'+sesiones.toXMLString());
		
		//Resultado sera un list con todas las sesiones de ese contenido
		for (var i = 0; i<sesiones.length(); i++){		
				fechahora = sesiones[i].fecha;				
				fechahora = fechahora.split('-');				
				fechahora = fechahora[0];
				sesiones[i].fecha = fechahora;				
				hora = sesiones[i].horainicio;
				fechahora = fechahora + ' - ' + hora;
				//A??adimos un nuevo nodo fechahora
				sesiones[i].fechahora = fechahora;
				//log.info('Fechahora.........................:'+sesiones[i].fechahora);
				
		}
		
		
	} else {
		//log.info("El xml que llega a obtenerSesionesPorContenidoFiltrado es nulo!!!");
		resultado = <error/>
	}
	
	//log.info("El resultado: ");
	//log.info(resultado.toXMLString());
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}


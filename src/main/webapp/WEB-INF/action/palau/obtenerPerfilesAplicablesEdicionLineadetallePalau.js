var services = context.beans.getBean('httpServiceSOA');

function handle(request, response) {

	if (log.isInfoEnabled()){
		log.info("Entrada a obtenerPerfilesAplicablesEdicionLineadetallePalau: ");
	}
	
	var methodpost = 'obtenerPerfilesAplicablesEdicionLineadetalle';
	var idcanal = request.getParameter('idcanal');
	var idcliente = request.getParameter('idcliente');
	var idcontenido = request.getParameter('idcontenido');
	var idproducto = request.getParameter('idproducto');
	var idsesion = request.getParameter('idsesion');
	var idtipoabono = request.getParameter('idtipoabono');
	
	var envio = '<servicio><parametro><dto>';
	if (idcanal != null && idcanal != "") envio = envio + '<idcanal>' + idcanal + '</idcanal>';
	if (idcliente != null && idcliente != "") envio = envio + '<idcliente>' + idcliente + '</idcliente>';
	if (idcontenido != null && idcontenido != "") envio = envio + '<idcontenido>' + idcontenido + '</idcontenido>';
	if (idproducto != null && idproducto != "") envio = envio + '<idproducto>' + idproducto + '</idproducto>';
	if (idsesion != null && idsesion != "") envio = envio + '<idsesion>' + idsesion + '</idsesion>';
	if (idtipoabono != null && idtipoabono != "") envio = envio + '<idtipoabono>' + idtipoabono + '</idtipoabono>';
	envio = envio + '</dto></parametro></servicio>';
	log.info("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX "+envio);
	/*envio = <servicio>
				<parametro>
					<idcanal>{idcanal}</idcanal>
					<idcliente>{idcliente}</idcliente>
					<idcontenido>{idcontenido}</idcontenido>
					<idproducto>{idproducto}</idproducto>
					<idsesion>{idsesion}</idsesion>
					<idtipoabono>{idtipoabono}</idtipoabono>
				</parametro>
			</servicio>	*/
	
	//No tratamos el xml de entrada...
	
	if (log.isInfoEnabled()){
		log.info("obtenerPerfilesAplicablesEdicionLineadetallePalau, envio: " + envio);
	}
	
	var respuesta = services.process(methodpost+'SOAWrapper',envio);
	
	if (log.isInfoEnabled()){
		log.info("obtenerPerfilesAplicablesEdicionLineadetallePalau, respuesta: " + respuesta);
	}

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta);

}
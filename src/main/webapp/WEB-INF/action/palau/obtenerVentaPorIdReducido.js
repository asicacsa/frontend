var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost = 'obtenerVentaPorIdReducido';
	var envio;
	
	//log.info("Entra en el action de obtenerVentaPorIdReducido.");
	
	if (xml!=null) {
		xml = new XML(xml);
				
		//log.info("Este es el xml que llega a obtenerVentaPorIdReducido: "+xml.toXMLString());	

		xml= this.preProcessXML(xml);
		
		//494569
		envio = 
			<servicio>
				<parametro>
					{xml}
				</parametro>
			</servicio>			
		
		
		//log.info("Este es el envio : "+envio);
		//log.info("Este es el metodo post : "+methodpost);
		
		//Y llamamos al servicio...
		var aux = services.process(methodpost+'SOAWrapper',envio).trim();
				
		if(aux != null){
		//log.info('aux es distinto de nulo');
			aux = new XML(aux);
			//resultado = aux;
			resultado = this.postProcessXML(aux);
			
			//log.info("Este es el resultado despues del POST proceso obtenerVentaPorIdReducido: "+resultado.toXMLString());			
		}			
		
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		//resultado = new XML(aux);
					
	} else {
		//log.info("El xml que llega a obtenerVentaPorIdReducido es nulo!!!");
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {		
	//Obligatorio : Campo dado de baja, por defecto lo ponemos a 0 ...
	param = comun.deleteSelectedTag(param.toXMLString());
	var xmlSinHijos= comun.borraElementosSinHijos(param);
	return xmlSinHijos;
	
}

function postProcessXML(xmlparam) {
	
	//log.info("Esto es lo que me llega al post Proceso obtenerVentaPorIdReducido: "+xmlparam.toXMLString());
	
	if (xmlparam != null){
		xmlparam = new XML(xmlparam);								
	}
	
	//log.info("Lo que envio a Laszlo......: "+xmlparam);
	
	return xmlparam;
}
var services = context.beans.getBean('httpServiceSOA');
//var servicio_alta ='darDeAltaCliente';
var servicio_edicion ='actualizarVentas';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	var xml = request.getParameter('xml');	
	//Aki recogeremos otro parametro con todos los datos.
	var data = request.getParameter('data_in');
	
	var resultado;
	var methodpost;
	var envio;
	
	//log.info("Estos son los datos a editar : ");
	if (data!=null) {
		data = new XML(data);
	}
	
	
	
	//log.info("Entra en el action de postEdicionVenta");
	
	if (xml!=null) {
		xml = new XML(xml);
		//log.info("Este es el xml que llega a postEdicionVenta: "+xml.toXMLString());
		//log.info("Le insertamoos la informacion del cliente : ");
		xml.Modificacion.venta.financiada=data.financiada;
		xml.Modificacion.venta.nif=data.nif;
		xml.Modificacion.venta.nombre=data.nombre;
		
		
		//xml = xml.Abono;
		

		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicion de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.
		
		if(methodpost == null) {
			//Si es un alta no poseera la propiedad id
			// ALTA
			if (xml.Modificacion.venta.idventa.text().length() == 0) {
				//log.info("No pasamos ninguna venta a actualizar!!!");
				resultado = <error/>
			// EDICION	
			} else {
				methodpost = servicio_edicion;
				xml= this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							{xml}
						</parametro>
					</servicio>		
				
				//log.info("Este es el envio : "+envio);
				//log.info("Este es el metodo post : "+methodpost);
		
				//Y llamamos al servicio...
				var aux = services.process(methodpost+'SOAWrapper',envio);	
				
				//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
				//En aux tenemos la respuesta.
				resultado = <ok>{aux}</ok>;
			}
		}		
					
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a postEdicionVenta es nulo!!!");
		}
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	//log.info("Lo que vamos a pasar por preProcessXML : "+param);
	
	param = new XML(param);
	param = comun.borraElementosSinHijos(param.toXMLString());

	return param;
	
}
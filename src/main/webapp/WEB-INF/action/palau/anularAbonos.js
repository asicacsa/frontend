// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = request.getParameter('servicio');
	
	//log.info("Entrada a anularAbonos.action");
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		var envio = <servicio/>;
	} else {
		xml = new XML(xml);
		
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>	
	}
	
	//log.info("Par?metro enviado al servicio anularAbonos: " + xml.toXMLString());
	services.process(methodpost+'SOAWrapper',envio.toXMLString());
	//var respuesta = new XML(service.process(methodpost,envio));
	var respuesta = <ok/>;
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
}
//var service = context.beans.getBean('httpServiceSOA');

// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = request.getParameter('servicio');
	
	//log.info("Entrada a buscarAbonos");
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		var envio = '<servicio><parametro><buscarabonoparam/></parametro></servicio>'
	} else {
		xml = new XML(xml);
		//log.info("LOG(INFO) : buscarAbonos params before the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		xml = this.preProcessXML(xml);

		if (log.isInfoEnabled()) {
			log.info(" #### Parametros busqueda Abono:" + xml.children().length());
		}

		//Comprobamos que tengamos campos de busuqueda
		if (xml.children().length()<=1){
			throw new java.lang.Exception("Debe introducir algun criterio de busqueda.")
		}

		
		//log.info("LOG(INFO) : buscarAbonos params after the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>	
	}
	
	var respuesta = services.process(methodpost+'SOAWrapper',envio).trim();
	//var respuesta = new XML(service.process(methodpost,envio));
	
	if (respuesta!=null) {
		respuesta = new XML(respuesta);
		//log.info("LOG(INFO): valor de la respuesta antes de ser postprocesada: " + respuesta);
		respuesta = this.postProcessXML(respuesta);	
		//log.info("Resultado de llamar a buscarCaja: "+respuesta.toXMLString());

	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
	
}


function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	//if (param.orden.text().length() == 0) {
	//		delete param.orden;
	//}
	
	//Obligatorio : Campo dado de baja, por defecto lo ponemos a 0 ...
	/**if (param.dadodebaja.text().length() == 0) {
			param.dadodebaja=0;
	}**/
	//log.info("LOG(INFO) : params before the borraElementosSinHijos : "+this.getClass().getName()+" "+param.toXMLString());
	param = comun.borraElementosSinHijos(param.toXMLString());
	//log.info("LOG(INFO) : params after the borraElementosSinHijos : "+this.getClass().getName()+" "+param.toXMLString());	
	param = comun.deleteSelectedTag(param.toXMLString());
	param = new XML(param);
	//log.info("LOG(INFO) : params after the deleteSelectedTag : "+this.getClass().getName()+" "+param.toXMLString());		
	
	return param;
	
}
function postProcessXML(xmlparam) {
	if (xmlparam != null){
		xmlparam = new XML(xmlparam);
		
		for each (var abonado in xmlparam.Abono) {
			abonado.nombreabonado = abonado.nombreabonado.text() + ' ' + abonado.apellido1.text() + ' ' + abonado.apellido2.text();			
		}
	}
	
	return xmlparam;
}

/**
 * Ajustamos el XML a la estructura y campos de la vista.
 */
/**function postProcessXML(xmlparam) {
	var OPERACION_APERTURA = 1;
	var OPERACION_CIERRE = 4;
	var OPERACION_VISTOBUENO = 5;
	
	if (xmlparam != null){
		xmlparam = new XML(xmlparam);
		
		for each (j in xmlparam.Caja){
		
			var caja = j;
			
			// Averiguamos el nombre completo del operador
			var nombrecompleto = caja.usuarioOperador.nombre.text() + " " + 
			                     caja.usuarioOperador.apellidos.text();
			//log.info("LOG(INFO) : Nombrecompleto: " + nombrecompleto);
			caja.usuarioOperador.nombrecompleto = <nombrecompleto>{nombrecompleto}</nombrecompleto>;
	
			
			// Averiguamos las fechas de apertura, cierre, visto bueno y lista de taquillas implicadas
			var fechaapertura = "";
			var fechacierre = "";
			var vistobueno = false;
			var taquillas = new Array();
			
			for each (i in caja.operacioncajas.Operacioncaja){
			
				//log.info("LOG(INFO): idtipooperacioncaja: " + i.tipooperacioncaja.idtipooperacioncaja.text());
				if (i.tipooperacioncaja.idtipooperacioncaja.text() == OPERACION_APERTURA){
					fechaapertura = i.fechayhoraoperacioncaja.text();
				} else if (i.tipooperacioncaja.idtipooperacioncaja.text() == OPERACION_CIERRE){
					fechacierre = i.fechayhoraoperacioncaja.text();
				} else if (i.tipooperacioncaja.idtipooperacioncaja.text() == OPERACION_VISTOBUENO){
					vistobueno = true;
				}
				
				// Creamos array de taquillas implicadas
				var encontrado = false;
				//log.info("taquillas.length: " + taquillas.length);
				//log.info("encontrado: " + encontrado);
				for (var aux = 0; (aux<taquillas.length) && !encontrado; aux++){
					//log.info("taquillas[aux]: " + taquillas[aux]);
					if (taquillas[aux] == i.taquilla.idtaquilla.text()){
						encontrado = true;
					} 
				}
				if (encontrado == false){
					taquillas[taquillas.length] = i.taquilla.idtaquilla.text();
				}
				
			}
			
			caja.fechaapertura = <fechaapertura>{fechaapertura}</fechaapertura>;
			caja.fechacierre = <fechacierre>{fechacierre}</fechacierre>;
			caja.vistobueno = <vistobueno>{vistobueno}</vistobueno>;
			
			caja.taquillas = <taquillas></taquillas>;
			for (var aux = 0; aux < taquillas.length; aux++){
				//xmlparam.taquillas.taquilla[0] += <taquilla>{taquillas[aux]}</taquilla>;
				caja.taquillas.* += <taquilla><idtaquilla>{taquillas[aux]}</idtaquilla></taquilla>;
			}
			
			//log.info("LOG(INFO): Array taquillas: " + taquillas.toString());
			
			// Fuera elementos innecesarios
			delete caja.usuarioOperador.nombre;
			delete caja.usuarioOperador.apellidos;
			delete caja.operacioncajas;
		
		}	
								
	}
	
	//log.warn("LOG(WARN) : getEdicionAltaRecintos. El dato numerada llega con valor distinto a 0 ? 1.")
	return xmlparam;
}**/
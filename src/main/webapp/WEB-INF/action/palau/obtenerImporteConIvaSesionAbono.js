var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	var resultado;
	var methodpost = 'obtenerImporteConIvaSesionAbono';
	var envio;
		
	var lineadetalle = request.getParameter('lineadetalle');
	var sesion = request.getParameter('sesion');
	lineadetalle = new XML(lineadetalle);
	sesion = new XML(sesion);

	var idlineadetalle=lineadetalle.idlineadetalle;
	var idsesion=sesion.idsesion;

	if (log.isInfoEnabled()) {
		log.info("entrada a obtenerImporteConIvaSesionAbono. idlineadetalle: " + idlineadetalle + ", idsesion: " + idsesion);
	}


	if ( isNaN(new Number(idsesion)) ){
		throw new java.lang.Exception("El idsesion no es correcto.")
	}

	if ( isNaN(new Number(idlineadetalle)) ){
		throw new java.lang.Exception("El idlineadetalle no es correcto.")
	}

	envio = 
		<servicio>
			<parametro>
				<Lineadetalle>
					{idlineadetalle}
				</Lineadetalle>
				<Sesion>
					{idsesion}
				</Sesion>
			</parametro>
		</servicio>

	//log.info("####Este es el envio : "+envio);	
		
	
	//Y llamamos al servicio...
	var aux = services.process(methodpost+'SOAWrapper',envio);
	
	//busco la linea con el mismo idestadolocalidad, en las Lineas y con la q me devuelve el servicio
	if(aux != null){
	
		if (log.isInfoEnabled()) {
			log.info('obtenerImporteConIvaSesionAbono, resultado: ' + resultado);	
		}
		resultado = new XML(aux);
		
	} else {
	
		resultado = new XML();
		if (log.isInfoEnabled()) {
			log.info("obtenerImporteConIvaSesionAbono, resultado es null.");
		}
		throw new java.lang.Exception("Error al obtener importe devolucion.");
		
	}
	
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
	
}


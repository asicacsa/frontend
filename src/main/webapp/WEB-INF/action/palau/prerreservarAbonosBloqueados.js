var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

/**
 	 private Integer idabono;
     private Lineadetalle lineadetalle;
     private Producto producto;
     private Localidad localidad;
     private Tipoabono tipoabono;
     private String observaciones;
     private Date fechaemision;
     private Date fechacaducidad;
     private Integer bloqueado;
     private Integer paseclub;
     private Integer anulado;
     private Integer orden;
     private Integer dadodebaja;
     private String nombreabonado;
     private String dniabonado;
     private String codigobarras;
     private String domicilio;
     private String cp;
     private String municipio;
     private String provincia;
     private String pais;
     private String telefono;
     private String email;
     private Set tarjetaentradas = new HashSet(0);
     private Set estadobonos = new HashSet(0);
     private Set renovacionabonos = new HashSet(0);
**/

function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost = 'prerreservarAbonosBloqueados';
	var envio;
	
	//log.info("Entra en el action de prerreservarAbonosBloqueados.");
	
	if (xml!=null) {
		xml = new XML(xml);
		//xml = xml.Abono;
		
		//log.info("Este es el xml que llega a prerreservarAbonosBloqueados: "+xml.toXMLString());	

		//xml= this.preProcessXML(xml);
		envio = 
			<servicio>
				<parametro>
					{xml}
				</parametro>
			</servicio>			
		
		
		//log.info("Este es el envio : "+envio);
		//log.info("Este es el metodo post : "+methodpost);
		
		//Y llamamos al servicio...
		var aux = services.process(methodpost+'SOAWrapper',envio);
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		resultado = new XML(aux);
					
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a prerreservarAbonosBloqueados es nulo!!!");
		}
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {		
	//Obligatorio : Campo dado de baja, por defecto lo ponemos a 0 ...
	if (param.dadodebaja.text().length() == 0) {
			param.dadodebaja=0;
	}
	
	param = comun.deleteSelectedTag(param.toXMLString());
	
	var xmlSinHijos= comun.borraElementosSinHijos(param);
	
	//param = new XML(xmlSinHijos);

	return xmlSinHijos;
	
}
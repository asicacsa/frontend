var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost = 'obtenerVentaPorIdAnulada';
	var envio;
	
	//log.info("Entra en el action de obtenerVentaPorIdAnuladaPalau.");
	
	if (xml!=null) {
		xml = new XML(xml);
				
		//log.info("Este es el xml que llega a obtenerVentaPorIdAnuladaPalau: "+xml.toXMLString());	

		xml= this.preProcessXML(xml);
		
		//494569
		envio = 
			<servicio>
				<parametro>
					{xml}
				</parametro>
			</servicio>			
		
		
		//log.info("Este es el envio : "+envio);
		//log.info("Este es el metodo post : "+methodpost);
		
		//Y llamamos al servicio...
		var aux = services.process(methodpost+'SOAWrapper',envio).trim();
				
		if(aux != null){
		//log.info('aux es distinto de nulo');
			aux = new XML(aux);
			//resultado = aux;
			resultado = this.postProcessXML(aux);
			
			//log.info("Este es el resultado despues del POST proceso: "+resultado.toXMLString());			
		}			
		
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		//resultado = new XML(aux);
					
	} else {
		//log.info("El xml que llega a obtenerVentaPorIdAnuladaPalau es nulo!!!");
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {		
	//Obligatorio : Campo dado de baja, por defecto lo ponemos a 0 ...
	
	
	param = comun.deleteSelectedTag(param.toXMLString());
	
	var xmlSinHijos= comun.borraElementosSinHijos(param);


	return xmlSinHijos;
	
}

function postProcessXML(xmlparam) {
	
	//log.info("##Esto es lo que me llega al post Proceso......: "+xmlparam.toXMLString());
	
	if (xmlparam != null){
		xmlparam = new XML(xmlparam);
				
		if (xmlparam.cliente.descendants('idcliente').length() == 0){
			xmlparam.cliente.appendChild("<nombre></nombre>");				
		}

		if (xmlparam.lineadetallesanuladas!=""){			
			xmlparam.lineadetallesanuladas.setName("lineadetalles");
		}
	
		//Solo nos quedamos con la primera entrada que viene
		for (var i=0; i<xmlparam.lineadetalles.Lineadetalle.length(); i++){		
			
			for (var j=1; j<xmlparam.lineadetalles.Lineadetalle[i].entradas.Entrada.length(); j++){				
				delete xmlparam.lineadetalles.Lineadetalle[i].entradas.Entrada[j];
			}

			//log.info("###Tenemos estadolocalidades......: "+xmlparam.lineadetalles.Lineadetalle[i].estadolocalidads.hasOwnProperty('Estadolocalidad'));

			if (!xmlparam.lineadetalles.Lineadetalle[i].estadolocalidads.hasOwnProperty('Estadolocalidad') ){
				xmlparam.lineadetalles.Lineadetalle[i].estadolocalidads.Estadolocalidad=<Estadolocalidad></Estadolocalidad>;	
			}
		}
		
	}
	
	//log.info("###Lineas de detalle anuladas......: "+xmlparam.lineadetalles.Lineadetalle.length());
	//log.info("###Datos que devolvemos......: "+xmlparam);
	
	return xmlparam;

}
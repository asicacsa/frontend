var services = context.beans.getBean('httpServiceSOA');
//var servicio_alta ='darDeAltaCliente';
var servicio_edicion ='actualizarAbonos';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost;
	var envio;
	
	//log.info("Entra en el handle de postEdicionAbonos");
	
	if (xml!=null) {
		xml = new XML(xml);
		xml = xml.Abono;
		//log.info("Este es el xml que llega a postEdicionAbonos: "+xml.toXMLString());
		

		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicion de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.
		
		if(methodpost == null) {
			//Si es un alta no poseera la propiedad id
			// ALTA
			if (xml.idabono.text().length() == 0) {
				//log.info("No pasamos ning?n abono a actualizar!!!");
				resultado = <error/>
			// EDICION	
			} else {
				methodpost = servicio_edicion;
				xml= this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							<list>
								{xml}
							</list>
						</parametro>
					</servicio>		
				if (log.isInfoEnabled()) {
					log.info("Este es el envio post ABONOS : "+envio);
				}
				//log.info("Este es el metodo post : "+methodpost);
		
				//Y llamamos al servicio...
				var aux = services.process(methodpost+'SOAWrapper',envio);		
				
				//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
				//En aux tenemos la respuesta.
				resultado = <ok>{aux}</ok>;
			}
		}		
					
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a postEdicionAbonos es nulo!!!");
		}
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	//log.info("Lo que vamos a pasar por preProcessXML : "+param);
	
	param = new XML(param);
 
	delete param.lineadetalle.venta.cliente.nombrecompleto;
	delete param.cliente.nombrecompleto;

	if (param.personalizar.text() == false) {
			param.personalizar=<personalizar>0</personalizar>
		} else if (param.personalizar.text() == true) {
			param.personalizar=<personalizar>1</personalizar>
		} else {
			//log.warn("LOG(WARN) : postRealizarVent. El dato activada llega con valor distinto a 0 ? 1.")
		}
	if (param.solicitafactura.text() == false) {
			param.solicitafactura=<solicitafactura>0</solicitafactura>
		} else if (param.solicitafactura.text() == true) {
			param.solicitafactura=<solicitafactura>1</solicitafactura>
		} else {
			//log.warn("LOG(WARN) : postRealizarVent. El dato activada llega con valor distinto a 0 ? 1.")
		}
	param = comun.borraElementosSinHijos(param.toXMLString());

	return param;
	
}
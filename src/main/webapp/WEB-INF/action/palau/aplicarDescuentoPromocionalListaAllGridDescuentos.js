var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
	if ( log.isInfoEnabled() ) log.info('Entrada a aplicarDescuentoPromocionalListaAllGridDescuentos.');

	var resultado;
	var methodpost = 'aplicarDescuentoPromocionalLista';
	var envio;

	var lineas = request.getParameter('lineas');
	var iddescuentopromocional = request.getParameter('iddescuentopromocional');
	var idtipoventa = request.getParameter('idtipoventa');

	lineas = new XML(lineas);
	iddescuentopromocional = new String(iddescuentopromocional);
	iddp = <iddescuentopromocional>{iddescuentopromocional}</iddescuentopromocional>;

	if ( log.isInfoEnabled() ) {
		log.info("iddescuentopromocional: " + iddescuentopromocional);
		log.info("iddescuentopromocional.length: " + iddescuentopromocional.length);
		log.info("lineas: " + lineas.toXMLString());
	}

	var xml = <dto>
	 			<lineadetalles>
	 			</lineadetalles>
	 			<idtipoventa>{idtipoventa}</idtipoventa>
			  </dto>;

	dpvacio = <descuentopromocional/>;
	for each(i in lineas.Lineadetalle) {
		dp = i.descuentopromocional;
		if ( dp == null || dp == '' || dp.length == 0 ) {
			i.appendChild( dpvacio );
		}
	}

	for each(i in lineas.Lineadetalle) {
		iddesc = i.descuentopromocional.iddescuentopromocional;
		if ( iddesc != null && iddesc != '' && iddesc.length > 0  ) {
			i.descuentopromocional.appendChild(iddesc);
		}
		else {
			delete i.descuentopromocional.iddescuentopromocional;
			i.descuentopromocional.appendChild(iddp);
		}
		xml.lineadetalles.appendChild(i);
		if ( log.isInfoEnabled() ) log.info( '\nAņadiendo linea de detalle al xml \n' + i.toXMLString() );
	}

	xml = this.preProcessXML(xml);
	envio = 
		<servicio>
			<parametro>
				{xml}
			</parametro>
		</servicio>

	if ( log.isInfoEnabled() ) log.info("Este es el envio : "+envio);

	//Y llamamos al servicio...
	var aux = services.process(methodpost+'SOAWrapper',envio);	

	//En esta accion no hay que realizar ningun postProceso del XML devuelto.
	//En aux tenemos la respuesta.
	if(aux != null){
		resultado = new XML(aux);
		resultado = this.postProcessXML(resultado);
	} else {
		resultado = new XML();
		if (log.isInfoEnabled()) log.info("El resultado es null.");
	}
	if ( log.isInfoEnabled() ) log.info("Este es el resultado de aplicarDescuentoPromocionalListaAllGridDescuentos: " + resultado.toXMLString());

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}


function preProcessXML(param) {
	param = new XML(param);
	param = comun.borraElementosSinHijos(param.toXMLString());
	return param;
}


function postProcessXML(param) {
	//log.info('en la Function postProcess..'+param);
	param = new XML(param);
	var nd = <iddescuentopromocional/>;
	for each(i in param.Lineadetalle.descuentopromocional){
		//log.info('Hay Nodo descuento??????'+i.descendants('iddescuentopromocional').length());
		if (i.descendants('iddescuentopromocional').length() == 0){
			i.appendChild(nd);
			}
		}
	//log.info("Lo que enviamos postProcesadoooooo : "+param);
	return param;
}
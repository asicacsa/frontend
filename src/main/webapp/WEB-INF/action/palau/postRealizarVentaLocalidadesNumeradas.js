var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='realizarVentaLocalidadesNumeradas';
//var servicio_edicion ='actualizarPrerreservaLocalidadesNumeradas';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost;
	var envio;
	//var idOperacionVenta = request.getParameter('idOperacionVenta');
	
	//log.info("Entra en el handle de postRealizarVentaLocalidadesNumeradas");
	
	
		
	
	if (xml!=null) {
		xml = new XML(xml);
		//log.info("Este es el xml que llega a postRealizarVentaLocalidadesNumeradas antes de asignar la Venta: "+xml.toXMLString());
		xml = xml.Venta;
		//log.info("Este es el xml que llega a postRealizarVentaLocalidadesNumeradas: "+xml.toXMLString());
		

		// si servicio no se envia, no lo podemos obtener del nombre del action
		// esta accion puede ser o alta o edicion de producto.
		// Deberemos saberlo por el parametro a enviar
		// Si es un alta no tendra id, si por el contrario es una edicion si que poseera un id.
		
		if(methodpost == null) {
			//Si es un alta no poseera la propiedad idperfilvisitante
			// ALTA
			if (xml.idventa.text().length() == 0) {
				methodpost = servicio_alta;
				xml = this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							{xml}
						</parametro>
					</servicio>
			// EDICI?N	
			} else {
				methodpost = servicio_edicion;
				xml= this.preProcessXML(xml);
				envio = 
					<servicio>
						<parametro>
							<list>
								{xml}
							</list>
						</parametro>
					</servicio>			
			}
		}		
		
		log.info("Este es el envio : "+envio);
		//log.info("Este es el metodo post : "+methodpost);
		
		//Y llamamos al servicio...
		var aux = services.process(methodpost+'SOAWrapper',envio);
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		if(aux != null){
		//log.info('aux es distinto de nulo');
			aux = new XML(aux);
			resultado = 
					<ok>
						{aux}
					</ok>
			if (log.isInfoEnabled()) {
				log.info("Este es el resultado : "+resultado);
			}
		}			
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a postRealizarVentaLocalidadesNumeradas es nulo!!!");
		}
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	log.info("Lo que vamos a pasar por preProcessXML : "+param);
	
	param = new XML(param);
	
	delete param.lineadetalles.Lineadetalle.perfiles;
	delete param.lineadetalles.Lineadetalle.descuentos;
	delete param.financiada;
	
	if (param.cliente.descendants('nombrecompleto').length() != 0){
		delete param.cliente.nombrecompleto;
	}
	
	//GGL 23022011 copiamos los parametros al tag venta desde el de cliente
	if (param.cliente.idcliente == <idcliente/>){
		//log.info("ENTRA EN LA CONDICION__________________________________");
		param.nombre = param.cliente.nombre.text();
		param.nif = param.cliente.identificador.text();
		param.telefono = param.cliente.direccionorigen.telefonofijo.text();
		param.telefonomovil = param.cliente.direccionorigen.telefonomovil.text();
		delete param.cliente;
	}
	
	if(param.isBono == true){
	//log.info('es un bono agencia');	
		delete param.importeparcials;	
		param.importeparcialsBono.setName('importeparcials');
		delete param.isBono;
		//log.info('es un bono agencia y asi queda despues de eliminar y cambiar el nombre: '+param);	
	}
	else{
	//log.info('NO es un Bono agencia y tengo q envia los Importes');
		delete param.importeparcialsBono;
		delete param.isBono;
	}
	if (param.entradasimpresas.text() == false) {
			param.entradasimpresas=<entradasimpresas>0</entradasimpresas>
		} else if (param.entradasimpresas.text() == true) {
			param.entradasimpresas=<entradasimpresas>1</entradasimpresas>
		} else {
			//log.warn("LOG(WARN) : postRealizarVentaLocalidadesNumeradas. El dato activada llega con valor distinto a 0 ? 1.")
		}
	if (param.reciboimpreso.text() == false) {
			param.reciboimpreso=<reciboimpreso>0</reciboimpreso>
		} else if (param.reciboimpreso.text() == true) {
			param.reciboimpreso=<reciboimpreso>1</reciboimpreso>
		} else {
			//log.warn("LOG(WARN) : postRealizarVentaLocalidadesNumeradas. El dato activada llega con valor distinto a 0 ? 1.")
		}
	
	
	param = comun.borraElementosSinHijos(param.toXMLString());
	
	

	if(param.importeparcials.length() == 1){
		//log.info('Solo hay un Importeparcial');
		for each(i in param.importeparcials.Importeparcial){
			if(i.importe.text() == 0){
				delete param.importeparcials[i.childIndex()];
					
			}
		}
	}
                                                                                                                                                                                                                                                                                                                                                                                                                
	//log.info("Lo que enviamos preparado : "+param);

	return param;
	
}
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

/**
 *	Recibe un idventa y un array de lineas de detalle, y llama a 
 *  actualizarVentas para que esas lineas sean insertadas en la venta.
 */
function handle(request, response) {

	//log.info('entrada a fusionarLineadetallesYVenta.action');
	
	var methodpost = 'actualizarLineadetallesVenta';

	var idventa = request.getParameter('idventa');
	var arrayLineas = request.getParameter('arrayLineas');
	
	var xml = new XML();
	
	if(idventa == null) {
		xml = '<servicio />';
	} else {
		
		var modificacion = createModificacionVentaAddingLineadetalles(idventa,arrayLineas);
		modificacion = preProcessXML(modificacion);

		xml = <servicio>
				<parametro>				
					{modificacion}				
				</parametro>
			  </servicio>
	}
	
	if (log.isInfoEnabled()) {
		log.info('    datos pasados al servicio: ' + xml.toXMLString());
	}
	var result = services.process(methodpost+'SOAWrapper',xml);
	if (log.isInfoEnabled()) {
		log.info('    resultado del servicio: ' + result);
	}
	
	if (result == null) {
		result = "<ArrayList />";
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	
	response.writer.println(result);
}

function createModificacionVentaAddingLineadetalles(idventa, arrayLineas){
	arrayLineas = new XML(arrayLineas);
	
	var modificacion = new XML('<Modificacion/>');
	modificacion.anulacion = <anulacion>0</anulacion>;
	modificacion.venta = <venta><idventa>{idventa}</idventa></venta>;
	modificacion.modificacionlineadetalles = <modificacionlineadetalles/>;
	modificacion.motivomodificacion = <motivomodificacion>Inserci??n de nuevas lineas de detalle</motivomodificacion>;
	
	// procesaremos la lista de lineadetalle
	for each (var lineadetalle in arrayLineas.Lineadetalle){
		var Modificacionlineadetalle = new XML('<Modificacionlineadetalle/>');
		// seg??n documentaci??n, anulado=0 si lineadetalle insertada por el usuario.
		// La propiedad del bean se llama sin embargo tipomodificacion.
		Modificacionlineadetalle.tipomodificacion = <tipomodificacion>0</tipomodificacion>;
		// renombro a min??scula al ser propiedad ??nica, no colecci??n
		lineadetalle.setName('lineadetalle');
		
		Modificacionlineadetalle.lineadetalle = lineadetalle;
	
		modificacion.modificacionlineadetalles.* += Modificacionlineadetalle;
	}
	
	return modificacion;
}


function preProcessXML(param) {
	param = new XML(param);
	param = comun.borraElementosSinHijos(param.toXMLString());

	return param;
}

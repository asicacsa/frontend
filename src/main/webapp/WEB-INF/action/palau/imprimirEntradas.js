var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost = 'imprimirEntradas';
	var envio;
	
	//log.info("Entra en el action de imprimirEntradas.");
	
	//var idFactura = request.getParameter('idFactura');
	
	if (xml!=null) {
		xml = new XML(xml);
				
		//log.info("##Este es el xml que llega a imprimirEntradas: "+xml.toXMLString());	
		
		for each (i in xml.identrada) {
				i.setName('int');
		}
		
		if ((xml=="") || (xml==null)){
			throw new java.lang.Exception("No hay entradas para ser impresas.")
		}
	
		envio = 
			<servicio>
				<parametro>
					{xml}
				</parametro>
			</servicio>			
		
		
		//log.info("Este es el envio : "+envio);
		//log.info("Este es el metodo post : "+methodpost);
		
		//Y llamamos al servicio...
		var aux;
		
		try{
			aux = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
				
		if(aux != null){
		//log.info('aux es distinto de nulo');
			aux = new XML(aux);
			//resultado = aux;
			resultado = aux;
			
			//log.info("Este es el resultado despues del POST proceso: "+resultado.toXMLString());			
		}			
		
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		//resultado = new XML(aux);
					
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a imprimirEntradas es nulo!!!");
		}
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}
//var service = context.beans.getBean('httpServiceSOA');

// el Proxy Invoker... objeto que proporciona el servicio
var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {
	
	var methodpost = request.getParameter('servicio');
	
	//log.info("Entrada a buscarLocalidad");
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var xml = request.getParameter('xml');
	
	if(xml == null ) {
		//log.info('El XML es NULO');
		var envio = '<servicio><parametro><buscarlocalidadparam/></parametro></servicio>'
	} else {
		xml = new XML(xml);
		//log.info("LOG(INFO) : buscarLocalidad params before the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		xml = this.preProcessXML(xml);
		//log.info("LOG(INFO) : buscarLocalidad params after the transformation : "+this.getClass().getName()+" "+xml.toXMLString());
		
		envio = <servicio>
					<parametro>
						{xml}
					</parametro>
				</servicio>	
		//log.info("Este es el envio : "+envio);	
	}
	
	var respuesta = services.process(methodpost+'SOAWrapper',envio);
	//var respuesta = new XML(service.process(methodpost,envio));
	
	if (respuesta!=null) {
		respuesta = new XML(respuesta);
		//log.info("LOG(INFO): valor de la respuesta antes de ser postprocesada: " + respuesta);
		//respuesta = this.postProcessXML(respuesta);	
		//log.info("Resultado de llamar a buscarCaja: "+respuesta.toXMLString());

	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(respuesta.toXMLString());
	
}


function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...
	//if (param.orden.text().length() == 0) {
	//		delete param.orden;
	//}
	
	//Obligatorio : Campo dado de baja, por defecto lo ponemos a 0 ...
	/**if (param.dadodebaja.text().length() == 0) {
			param.dadodebaja=0;
	}**/
	//log.info("LOG(INFO) : params before the borraElementosSinHijos : "+this.getClass().getName()+" "+param.toXMLString());
	param = comun.borraElementosSinHijos(param.toXMLString());
	//log.info("LOG(INFO) : params after the borraElementosSinHijos : "+this.getClass().getName()+" "+param.toXMLString());	
	param = comun.deleteSelectedTag(param.toXMLString());
	param = new XML(param);
	//log.info("LOG(INFO) : params after the deleteSelectedTag : "+this.getClass().getName()+" "+param.toXMLString());		
	
	return param;
	
}
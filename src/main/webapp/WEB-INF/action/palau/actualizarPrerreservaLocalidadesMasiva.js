importPackage(Packages.java.util);
importPackage(Packages.java.text);
var services = context.beans.getBean('httpServiceSOA');
var pattern_date_format = "dd/MM/yyyy-HH:mm";
var estado_ocupada = 4;	

var ID_PRODUCTO_OPERA = 1000;			


var model = <lineadetalles><Lineadetallemasiva></Lineadetallemasiva></lineadetalles>;

function handle(request, response) {
		
	var xml = request.getParameter('xml');
	var methodpost = request.getParameter('servicio');

	if (log.isInfoEnabled()){
		log.info("Call to actualizarPrerreservaLocalidadesMasiva, xml: " + xml);
		log.info("Call to actualizarPrerreservaLocalidadesMasiva, methodpost: " + methodpost);
	}

	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = request.getAttribute('__request_service');
	}
	
	var resultado;
	var envio;


	if (xml!=null) {

		envio  = <servicio>
					<parametro>
						<int>
							{xml}
						</int>
					</parametro>
				</servicio>
		methodpost = 'obtenerSesionPorIdVentaMasiva';
	} else {
		envio  = <servicio>
					<parametro>
						<Altasesionparam/>
					</parametro>
				</servicio>;
		methodpost = 'getXMLModel';
	}
	
	
	//log.info("LOG(INFO) : to this service : "+methodpost+'SOAWrapper'+' : '+this.getClass().getName());
			
	//log.info("LOG(INFO) : with this parameters : "+envio+' : '+this.getClass().getName());					
	var aux;
	if (log.isErrorEnabled()) {
		try {
			aux = services.process(methodpost+'SOAWrapper',envio);				
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
	} else {
		aux = services.process(methodpost+'SOAWrapper',envio);					
	}
	
	//log.info("LOG(INFO) : #### Aux : "+ aux);

	if (aux!=null) {
		
		if (log.isInfoEnabled()){
			log.info('actualizarPrerreservaLocalidadesMasiva, service result: ' + aux);
		}

		resultado = new XML(aux);		
		resultado = this.postProcessXMLEdicion(resultado);

	} else {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result does not Sesion : '+ex);
		}
		var ex = new java.lang.Exception('Exception calling this service : '+methodpost+'SOAWrapper : '+this.getClass().getName());
		throw ex;	
	}
	
	if (log.isInfoEnabled()){
		log.info('actualizarPrerreservaLocalidadesMasiva, final response: ' + resultado);
	}
	
	resultado= new XML(resultado);

	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}


function postProcessXMLEdicion(param) {
	var aux;
	var aux_fech;
	var aux_date = new java.util.Date();
	var date_formater = new java.text.SimpleDateFormat(pattern_date_format);
	if (param.sesionPlantilla.fecha.text().length() != 0) {
		aux = param.sesionPlantilla.fecha.text();
		aux_fech=aux.substr(0,aux.indexOf('-'));
		model.Lineadetallemasiva.fecha = <fecha>{aux_fech}</fecha>		
	}

	var hora = param.sesionPlantilla.horainicio.text();
	var idsesion = param.sesionPlantilla.idsesion.text();
	var nombre = param.sesionPlantilla.contenido.nombre.text();
	var idcontenido = param.sesionPlantilla.contenido.idcontenido.text();
	//var idproducto = param.sesionPlantilla.contenido.tipoproducto.tipoprodproductos[0].Tipoprodproducto.producto.idproducto.text();
	var idproducto = ID_PRODUCTO_OPERA; // cambio porque el producto no va a cambiar para las ventas masivas.


	model.Lineadetallemasiva.horainicio = <horainicio>{hora}</horainicio>		
	model.Lineadetallemasiva.contenido= <contenido>
											<nombre>{nombre}</nombre>
											<idcontenido>{idcontenido}</idcontenido>
									    </contenido>
	model.Lineadetallemasiva.idsesion = <idsesion>{idsesion}</idsesion>
	model.Lineadetallemasiva.filaini = <filaini></filaini>
	model.Lineadetallemasiva.filafin = <filafin></filafin>
	model.Lineadetallemasiva.butacaini = <butacaini></butacaini>
	model.Lineadetallemasiva.butacafin = <butacafin></butacafin>
	model.Lineadetallemasiva.piso = <piso></piso>	
	model.Lineadetallemasiva.descuento = <descuento><iddescuentopromocional></iddescuentopromocional></descuento>
	model.Lineadetallemasiva.impares = <impares></impares>
	model.Lineadetallemasiva.producto = <producto>
											<idproducto>{idproducto}</idproducto>
										</producto>
	model.Lineadetallemasiva.perfilvisitante = <perfilvisitante><idperfilvisitante/></perfilvisitante>
	//model.Lineadetallemasiva.importe = <importe></importe>

	
	return model;

}
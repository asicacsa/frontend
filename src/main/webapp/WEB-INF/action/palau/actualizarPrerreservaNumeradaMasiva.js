var services = context.beans.getBean('httpServiceSOA');
var servicio_alta ='actualizarPrerreservaNumeradaMasiva';
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
		
	var xml = request.getParameter('xml');		
	var resultado;
	var methodpost;
	var envio;
	
	if (log.isInfoEnabled()){
		log.info("Entrada a actualizarPrerreservaNumeradaMasiva.");
		log.info("		xml de entrada: " + xml);
	}
	
	if (xml!=null) {
		xml = new XML(xml);
	
		methodpost = servicio_alta;
		xml = this.preProcessXML(xml);
		envio = 
			<servicio>
				<parametro>
					{xml}
				</parametro>
			</servicio>

		
		if (log.isInfoEnabled()){
			log.info("actualizarPrerreservaNumeradaMasiva, este es el envio : "+envio);
			log.info("actualizarPrerreservaNumeradaMasiva, este es el metodo post : "+methodpost);
		}
		
		//Y llamamos al servicio...
		var aux = services.process(methodpost+'SOAWrapper',envio);
		
		//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
		//En aux tenemos la respuesta.
		if(aux != null){
		//log.info('aux es distinto de nulo');
			aux = new XML(aux);
			resultado = 
					<ok>
						{aux}
					</ok>
			//log.info("Este es el resultado : "+resultado);
		}			
	} else {
		if (log.isInfoEnabled()) {
			log.info("El xml que llega a postRealizarVentaLocalidadesNumeradas es nulo!!!");
		}
		resultado = <error/>
	}
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}

function preProcessXML(param) {
	//Datos obligatorios que no pueden estar vacios ...

	//log.info("Lo que vamos a pasar por preProcessXMLMasiva : "+param);
	
	
	
	param = comun.borraElementosSinHijos(param.toXMLString());

	param = new XML(param);

	delete param.Lineadetallemasiva.fecha;
	delete param.Lineadetallemasiva.horainicio;
	//delete param.Lineadetalle.perfiles;
	delete param.Lineadetalle.descuentos;
	//delete param.financiada;
	
	//log.info("Lo que vamos hemos procesado preProcessXMLMasiva : "+param);

	return param;
	
}
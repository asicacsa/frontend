var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');
function handle(request, response) {	
	
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : Call to getXMLModel."+this.getClass().getName());
	}		
	
	var envio = <servicio>
					<parametro>
						<Buscarcajaparam/>
					</parametro>
				</servicio>
				
	var methodpost = request.getParameter('servicio');
	var	dtoBuscarCaja;
	
	if (log.isInfoEnabled()) {
		log.info("Entrada a getBuscarCaja.action");
	}
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = 'getXMLModel';
	}
												
	if (log.isErrorEnabled()) {
		try {
			dtoBuscarCaja = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
	} else {
		dtoBuscarCaja = services.process(methodpost+'SOAWrapper',envio);
	}
	
	
	var resul;
	
	resul = this.postProcess(dtoBuscarCaja);
	
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : final response : "+resul+' : '+this.getClass().getName());
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resul.toXMLString());
}

function postProcess(buscarCaja) {	
	
	buscarCaja = new XML(buscarCaja);
	

	return buscarCaja;	
}
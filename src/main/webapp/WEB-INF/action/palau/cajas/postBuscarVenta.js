var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {	
				
	if (log.isInfoEnabled()){
		log.info("Entrada a postBuscarVenta.action.");
	}
	
	var methodpost = request.getParameter('servicio');
	var	dtoBuscarVenta = request.getParameter('xml');
		
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = 'buscarVentasPorCaja';
	}
	
	dtoBuscarVenta = this.preProcess(dtoBuscarVenta);

	var envio = <servicio>
					<parametro>
						{dtoBuscarVenta}
					</parametro>
				</servicio>		
					
	
	var resul;			

	try {
		if (log.isInfoEnabled()){
			log.info("postBuscarVenta, envio: " + envio);
		}
		
		resul = services.process(methodpost+'SOAWrapper',envio).trim();
		
		if (log.isInfoEnabled()){
			log.info("postBuscarVenta, respuesta del servicio: " + resul);
		}
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
		}
		throw ex.javaException;
	}
		
	
	if (resul != null) {
		resul = new XML(resul);	
		this.postProcessXML(resul);
	} else {
		resul = <ArrayList/>;
	}
	
	if (log.isInfoEnabled()){
		log.info("postBuscarVenta, resultado final: " + resul.toXMLString());
	}
	
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resul.toXMLString());
}



function preProcess(buscarVenta) {	

	buscarVenta = new XML(buscarVenta);
	//buscarVenta = buscarVenta.buscarVentaparam;
	
	buscarVenta = comun.borraElementosSinHijos(buscarVenta);
	return buscarVenta;	
}



function postProcessXML(xmlparam) {

	// se el agrega otro tag con todas las formas de pago de la venta, 
	// las de los importes originales de la venta
	// mas los de las modificaciones sin repetir
	
	for each (var venta in xmlparam.Venta){
		venta.formasdepagos = <formasdepagos></formasdepagos>;
		
		for each (var importeparcial in venta.importeparcials.Importeparcial){
			// solo agregamos si no existe previamente
			var found = false;
			for each (var formapago in venta.formasdepagos.formapago){
				if (formapago.idformapago.text()==importeparcial.formapago.idformapago.text()) found = true;
			}
			if (!found) venta.formasdepagos.* += importeparcial.formapago;
		}
		
		for each (var importeparcial in venta.modificacions.Modificacion.modificacionimporteparcials.Modificacionimporteparcial.importeparcial){
			// solo agregamos si no existe previamente
			var found = false;
			for each (var formapago in venta.formasdepagos.formapago){
				if (formapago.idformapago.text()==importeparcial.formapago.idformapago.text()) found = true;
			}
			if (!found) venta.formasdepagos.* += importeparcial.formapago;
		}
	}

	return xmlparam;
}



/**
 * Ajustamos el XML a la estructura y campos de la vista.
 */
/*function postProcessXML(xmlparam) { 

	log.error('LOG(ERROR) :#######################################  postProcessXML del PostBuscar');
	var OPERACION_APERTURA = 1;
	var OPERACION_CIERRE = 4;
	var OPERACION_VISTOBUENO = 5;
	
	if (xmlparam != null){
		xmlparam = new XML(xmlparam);
		
		for each (j in xmlparam.Caja){
		
			var caja = j;
			
			// Averiguamos el nombre completo del operador
			//var nombrecompleto = caja.usuarioOperador.nombre.text() + " " + 
			//					 caja.usuarioOperador.apellidos.text();
			//log.info("LOG(INFO) : Nombrecompleto: " + nombrecompleto);
			//caja.usuarioOperador.nombrecompleto = <nombrecompleto>{nombrecompleto}</nombrecompleto>;
	
			
			// Averiguamos las fechas de apertura, cierre, visto bueno y lista de taquillas implicadas
			var fechaapertura = "";
			var fechacierre = "";
			var vistobueno = false;
			var taquillas = <taquillas/>;
			
			for each (i in caja.operacioncajas.Operacioncaja){
			
				//log.debug("LOG(debug): idtipooperacioncaja: " + i.tipooperacioncaja.idtipooperacioncaja.text());
				
				if (i.tipooperacioncaja.idtipooperacioncaja.text() == OPERACION_APERTURA){
					fechaapertura = i.fechayhoraoperacioncaja.text();
				} else if (i.tipooperacioncaja.idtipooperacioncaja.text() == OPERACION_CIERRE){
					fechacierre = i.fechayhoraoperacioncaja.text();
				} else if (i.tipooperacioncaja.idtipooperacioncaja.text() == OPERACION_VISTOBUENO){
					vistobueno = true;
				}
				
				// Creamos array de taquillas implicadas
				var encontrado = false;
				
				//log.debug("Las taquillas de la operacionCaja : "+i.toXMLString())
				
				for each (v in taquillas.taquilla) {
				 	//log.debug("taquillas : " + i.taquilla.toXMLString());
				 	if (i.taquilla.hasOwnProperty('idtaquilla')) {
						if (v.idtaquilla.text() == i.taquilla.idtaquilla.text()) {
							encontrado = true;
							break;
						}
					} else {
						encontrado=true;
						break;
					}
				}	
				if (!encontrado) {
					if (i.taquilla.hasOwnProperty('idtaquilla')) {
						taquillas.appendChild(i.taquilla);
					}
				}
			}
			
			caja.fechaapertura = <fechaapertura>{fechaapertura}</fechaapertura>;
			caja.fechacierre = <fechacierre>{fechacierre}</fechacierre>;
			caja.vistobueno = <vistobueno>{vistobueno}</vistobueno>;
			
			caja.taquillas = taquillas;			
			
			//log.debug("LOG(debug): Array taquillas: " + taquillas.toXMLString());
			
			// Fuera elementos innecesarios
			//delete caja.usuarioOperador.nombre;
			//delete caja.usuarioOperador.apellidos;
			delete caja.operacioncajas;
		
		}	
								
	}
	
	return xmlparam;
}*/
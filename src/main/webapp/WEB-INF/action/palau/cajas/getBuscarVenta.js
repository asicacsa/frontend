var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');
function handle(request, response) {	
	
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : Call to getXMLModel."+this.getClass().getName());
	}		
	
	var envio = <servicio>
					<parametro>
						<Buscarventasporcajaparam/>
					</parametro>
				</servicio>
				
	var methodpost = request.getParameter('servicio');
	var	dtoBuscarVenta;
	
	if (log.isInfoEnabled()) {
		log.info("Entrada a Buscarventasporcajaparam.action");
	}
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = 'getXMLModel';
	}
												
	if (log.isErrorEnabled()) {
		try {
			dtoBuscarVenta = services.process(methodpost+'SOAWrapper',envio);
		} catch (ex) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
			throw ex.javaException;
		}
	} else {
		dtoBuscarVenta = services.process(methodpost+'SOAWrapper',envio);
	}
	
	
	var resul;
	
	resul = this.postProcess(dtoBuscarVenta);
	
	if (log.isInfoEnabled()) {
		log.info("LOG(INFO) : final response : "+resul+' : '+this.getClass().getName());
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resul.toXMLString());
}

function postProcess(buscarVenta) {	
	
	buscarVenta = new XML(buscarVenta);
	

	return buscarVenta;	
}
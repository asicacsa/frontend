var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');

function handle(request, response) {	
				
	var methodpost = request.getParameter('servicio');
	var	dtoBuscarCaja = request.getParameter('xml');
	log.info('El dto que llega: '+dtoBuscarCaja);
	
	
	
	// si servicio no se envia, intenta obtenerlo del nombre del action
	if(methodpost == null) {
		methodpost = 'buscarCaja';
	}
	
	if (log.isDebugEnabled()) {
		log.debug("Par??metros de entrada en postBuscarcaja antes de la transformaci??n : " + dtoBuscarCaja);
	}
	
	dtoBuscarCaja = this.preProcess(dtoBuscarCaja);

	var envio = <servicio>
					<parametro>
						{dtoBuscarCaja}
					</parametro>
				</servicio>					
	if (log.isDebugEnabled()) {
		log.debug("Env??o a buscarCaja, preprocesado: " + envio); 							
	}
	
	var resul;			

	try {
		resul = services.process(methodpost+'SOAWrapper',envio);
	} catch (ex) {
		if (log.isErrorEnabled()) {
			log.error('LOG(ERROR) : Error in this call : '+this.getClass().getName());
			log.error('LOG(ERROR) : to this : '+methodpost+'SOAWrapper : '+this.getClass().getName());
			log.error('LOG(ERROR) : result in this Exception : '+ex);
		}
		throw ex.javaException;
	}
		
	
	if (log.isDebugEnabled()) {
		log.debug("Resultado de buscarCaja antes de postprocesado: " + resul.substr(0,50) + "[...]");
	}
	
	if (resul != null) {
		resul=this.postProcessXML(resul);
	} else {
		resul=<ArrayList/>;
	}
	
	if (log.isInfoEnabled()) {
		log.info("Resultado de buscarCaja tras postprocesado: " + resul.toXMLString().substr(0,50) + "[...]");
	}
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resul.toXMLString());
}


function preProcess(buscarCaja) {	
	buscarCaja = new XML(buscarCaja);
	//buscarCaja = buscarCaja.Buscarcajaparam;
	
	buscarCaja = comun.borraElementosSinHijos(buscarCaja);
	return buscarCaja;	
}


/**
 * Ajustamos el XML a la estructura y campos de la vista.
 */
function postProcessXML(xmlparam) { 

	var OPERACION_APERTURA = 1;
	var OPERACION_CIERRE = 4;
	var OPERACION_VISTOBUENO = 5;
	
	if (xmlparam != null){
		xmlparam = new XML(xmlparam.trim());
		
		for each (j in xmlparam.Caja){
		
			var caja = j;
			
			// Averiguamos el nombre completo del operador
			//var nombrecompleto = caja.usuarioOperador.nombre.text() + " " + 
			//					 caja.usuarioOperador.apellidos.text();
			//log.info("LOG(INFO) : Nombrecompleto: " + nombrecompleto);
			//caja.usuarioOperador.nombrecompleto = <nombrecompleto>{nombrecompleto}</nombrecompleto>;
	
			
			// Averiguamos las fechas de apertura, cierre, visto bueno y lista de taquillas implicadas
			var fechaapertura = "";
			var fechacierre = "";
			var vistobueno = false;
			var taquillas = <taquillas/>;
			
			for each (i in caja.operacioncajas.Operacioncaja){
			
				//log.debug("LOG(debug): idtipooperacioncaja: " + i.tipooperacioncaja.idtipooperacioncaja.text());
				
				if (i.tipooperacioncaja.idtipooperacioncaja.text() == OPERACION_APERTURA){
					fechaapertura = i.fechayhoraoperacioncaja.text();
				} else if (i.tipooperacioncaja.idtipooperacioncaja.text() == OPERACION_CIERRE){
					fechacierre = i.fechayhoraoperacioncaja.text();
				} else if (i.tipooperacioncaja.idtipooperacioncaja.text() == OPERACION_VISTOBUENO){
					vistobueno = true;
				}
				
				// Creamos array de taquillas implicadas
				var encontrado = false;
				
				//log.debug("Las taquillas de la operacionCaja : "+i.toXMLString())
				
				for each (v in taquillas.taquilla) {
				 	//log.debug("taquillas : " + i.taquilla.toXMLString());
				 	if (i.taquilla.hasOwnProperty('idtaquilla')) {
						if (v.idtaquilla.text() == i.taquilla.idtaquilla.text()) {
							encontrado = true;
							break;
						}
					} else {
						encontrado=true;
						break;
					}
				}	
				if (!encontrado) {
					if (i.taquilla.hasOwnProperty('idtaquilla')) {
						taquillas.appendChild(i.taquilla);
					}
				}
			}
			
			caja.fechaapertura = <fechaapertura>{fechaapertura}</fechaapertura>;
			caja.fechacierre = <fechacierre>{fechacierre}</fechacierre>;
			caja.vistobueno = <vistobueno>{vistobueno}</vistobueno>;
			
			caja.taquillas = taquillas;			
			
			//log.debug("LOG(debug): Array taquillas: " + taquillas.toXMLString());
			
			// Fuera elementos innecesarios
			//delete caja.usuarioOperador.nombre;
			//delete caja.usuarioOperador.apellidos;
			delete caja.operacioncajas;
		
		}	
								
	}
	
	return xmlparam;
}
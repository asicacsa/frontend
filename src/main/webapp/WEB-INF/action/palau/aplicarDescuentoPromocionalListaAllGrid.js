var services = context.beans.getBean('httpServiceSOA');
var sbf = context.beans.getBean('scriptBeanFactory');
var comun = sbf.getBean('transformerXML.js');


function handle(request, response) {
	if ( log.isInfoEnabled() ) log.info('Entrada a aplicarDescuentoPromocionalListaAllGrid.');
		
	var resultado;
	var methodpost = 'aplicarDescuentoPromocionalLista';
	var envio;
		
	var lineas = request.getParameter('lineas');
	var idperfilvisitante = request.getParameter('idperfilvisitante');
	var idtipoventa = request.getParameter('idtipoventa');

	lineas = new XML(lineas);
	idperfilvisitante = new String(idperfilvisitante);
	idpv = <idperfilvisitante>{idperfilvisitante}</idperfilvisitante>;
	
	if ( log.isInfoEnabled() ) {
		log.info("idperfilvisitante: " + idperfilvisitante);
		log.info("idperfilvisitante.length: " + idperfilvisitante.length);
		log.info("lineas: " + lineas.toXMLString());
	}

	var xml = <dto>
	 			<lineadetalles>
	 			</lineadetalles>
	 			<idtipoventa>{idtipoventa}</idtipoventa>
			  </dto>;
	
	pvvacio = <perfilvisitante/>;
	for each(i in lineas.Lineadetalle) {
		pv = i.perfilvisitante;
		if ( pv == null || pv == '' || pv.length == 0 ) {
			i.appendChild( pvvacio );
		}
	}

	for each(i in lineas.Lineadetalle) {
		idperf = i.perfilvisitante.idperfilvisitante;
		if ( idperf != null && idperf != '' && idperf.length > 0  ) {
			i.perfilvisitante.appendChild(idperf);
		}
		else {
			delete i.perfilvisitante.idperfilvisitante;
			i.perfilvisitante.appendChild(idpv);
		}
		xml.lineadetalles.appendChild(i);
		if ( log.isInfoEnabled() ) log.info( '\nAņadiendo linea de detalle al xml \n' + i.toXMLString() );
	}
		
	xml = this.preProcessXML(xml);
	envio = 
		<servicio>
			<parametro>
				{xml}
			</parametro>
		</servicio>

		if ( log.isInfoEnabled() ) log.info("Este es el envio : "+envio);
	
	//Y llamamos al servicio...
	var aux = services.process(methodpost+'SOAWrapper',envio);
	
	//En esta accion no hay que realizar ningun postProceso del XML devuelto.	
	//En aux tenemos la respuesta.
	if(aux != null){
		resultado = new XML(aux);
		resultado = this.postProcessXML(resultado);
	} else {
		resultado = new XML();
		if (log.isInfoEnabled()) log.info("El resultado es null.");
	}
	if (log.isInfoEnabled()) log.info("Este es el resultado de aplicarDescuentoPromocionalListaAllGrid: " + resultado.toXMLString());
	
	response.setCharacterEncoding('UTF-8');
	response.setContentType('text/xml');
	response.writer.println(resultado.toXMLString());
}


function preProcessXML(param) {
	param = new XML(param);
	param = comun.borraElementosSinHijos(param.toXMLString());
	return param;
}


function postProcessXML(param) {
	//log.info('en la Function postProcess..'+param);
	param = new XML(param);
	var nd = <iddescuentopromocional/>;
	for each(i in param.Lineadetalle.descuentopromocional){
		//log.info('Hay Nodo descuento??????'+i.descendants('iddescuentopromocional').length());
		if (i.descendants('iddescuentopromocional').length() == 0){
			i.appendChild(nd);	
			}
		}
	//log.info("Lo que enviamos postProcesadoooooo : "+param);
	return param;
}
package es.di.cac.acegisecurity.ui.filters;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import utiles.Constantes;


public class ServicePerformanceMonitorInterceptor implements Filter {
	private static final Log logger = LogFactory.getLog(ServicePerformanceMonitorInterceptor.class);	 
	private static final char SEP = Constantes.CARACTER_TABULADOR;
	private static final char VOID_VALUE = Constantes.CARACTER_MENOS;
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd:HH:mm:ss.SSS");

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		if (logger.isInfoEnabled()) {
			
			long t0=0;
			long t1=0; 
			try {
				writeIN(request);
				t0 = System.currentTimeMillis();
				chain.doFilter(request, response);
				t1 = System.currentTimeMillis();
			} finally {
				writeOUT(request, t0, t1);
			}
		} else {
			chain.doFilter(request, response);
		}
	}
	
	
	private void writeIN(ServletRequest request)throws IOException{
		
		StringBuffer logline = new StringBuffer(Constantes.CADENA_IN);
		try{
			logline.append(SEP);
			logline.append(sdf.format(new Date()));
			logline.append(SEP);
			logline.append(VOID_VALUE);
			HttpSession sesion = ((HttpServletRequest) request).getSession(false);
			logline.append(SEP);
			if (sesion != null) {
				logline.append(sesion.getId());
			} else {
				logline.append(VOID_VALUE);
			}
			logline.append(SEP);
			if (request instanceof HttpServletRequest) {
				HttpServletRequest httprequest = (HttpServletRequest) request;
				logline.append(httprequest.getRequestURL());
			} else {
				logline.append(VOID_VALUE);
			}
		}finally{
			logger.info(logline.toString());
		}
	}
	
	
	
	private void writeOUT(ServletRequest request,long t0,long t1)throws IOException{
		StringBuffer logline = new StringBuffer("OUT");
		try{
			logline.append(SEP);
			logline.append(sdf.format(new Date()));
			logline.append(SEP);
			logline.append(t1 - t0);
			HttpSession sesion = ((HttpServletRequest) request).getSession(false);
			logline.append(SEP);
			if (sesion != null) {
				logline.append(sesion.getId());
			} else {
				logline.append(VOID_VALUE);
			}
			logline.append(SEP);
			if (request instanceof HttpServletRequest) {
				HttpServletRequest httprequest = (HttpServletRequest) request;
				logline.append( httprequest.getRequestURL() );
			} else {
				logline.append(VOID_VALUE);
			}
		}finally{
			logger.info(logline.toString());
		}
	}
	
	
	
	public void init(FilterConfig filterConfig) throws ServletException {}

	public void destroy() {}

}
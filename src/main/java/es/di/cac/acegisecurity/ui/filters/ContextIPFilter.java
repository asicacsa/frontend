package es.di.cac.acegisecurity.ui.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.acegisecurity.context.Context;
import net.sf.acegisecurity.context.ContextHolder;
import net.sf.acegisecurity.context.security.SecureContextUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import utiles.Constantes;
import es.di.cac.acegisecurity.context.security.CacSecureContextImpl;

/**
 * @author JSB
 *
 */
public class ContextIPFilter implements Filter {
	//~ Static fields/initializers =============================================
	private static final Log logger = LogFactory.getLog(ContextIPFilter.class);
	
	public void destroy() {}
	
	public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) throws IOException, ServletException {
		
		if (!(request instanceof HttpServletRequest)) {
			throw new ServletException("Can only process HttpServletRequest");
		}
		if (!(response instanceof HttpServletResponse)) {
			throw new ServletException("Can only process HttpServletResponse");
		}
		if (logger.isDebugEnabled()){
			logger.debug("ContextIPFilter, request.getRemoteAddr(): " + request.getRemoteAddr());
		}
		Context securectxt = SecureContextUtils.getSecureContext();
		if (securectxt != null && securectxt instanceof CacSecureContextImpl) {
			if (logger.isDebugEnabled()){
				logger.debug("ContextIPFilter, securectxt NO nulo: " + securectxt);
				logger.debug("ContextIPFilter, securectxt.realIp: " + ((CacSecureContextImpl)securectxt).getRealIp());
			}
			if (  ( (CacSecureContextImpl)securectxt ).getRealIp() == null ||
				( (CacSecureContextImpl)securectxt ).getRealIp().equals( Constantes.CADENA_VACIA )  ) {
				if (logger.isDebugEnabled()){
					logger.debug( "ContextIPFilter, no hay IP. Estableciendo " + request.getRemoteAddr() );
				}
				HttpServletRequest htreq = (HttpServletRequest) request;
				// Sesión nueva o expirada
				if (htreq.getRequestedSessionId()==null || htreq.getSession(false)==null) {
					if (logger.isDebugEnabled()){
						logger.debug("ContextIPFilter, sesión nueva o expirada, htreq.getRequestedSessionId(): " +
							htreq.getRequestedSessionId()+" htreq.getSession(false).getId(): " +
							(htreq.getSession(false)!=null?htreq.getSession(false).getId():"sin sesion"));
					}
					HttpSession ses =  htreq.getSession(true);
					if (logger.isInfoEnabled()){
						logger.info("ContextIPFilter, Sesión nueva o expirada, asignamos sesión con ID: " + ses.getId() );
					}
				} else {
					if (logger.isInfoEnabled()){
						logger.info("ContextIPFilter, ya había sesión, no reasignamos.");
					}
				}
				((CacSecureContextImpl)securectxt).setRealIp(request.getRemoteAddr());
			}
			if (logger.isDebugEnabled()) {
				logger.debug("El securectxt NO es nulo y la IP establecida es: " +((CacSecureContextImpl)securectxt).getRealIp());
			}
			ContextHolder.setContext((CacSecureContextImpl)securectxt);
		} else {
			logger.warn("El securectxt ES NULO o no es del tipo : " + CacSecureContextImpl.class.getName());
		}
		chain.doFilter(request, response);
	}



	public void init(FilterConfig arg0) throws ServletException {}

}
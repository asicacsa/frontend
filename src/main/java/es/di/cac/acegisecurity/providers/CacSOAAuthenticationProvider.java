package es.di.cac.acegisecurity.providers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.dao.DataAccessException;
import org.springframework.util.Assert;

import com.thoughtworks.xstream.XStream;

import es.di.cac.acegisecurity.ApplicationUserDetails;
import es.di.cac.acegisecurity.context.security.CacSecureContextImpl;
import es.di.framework.commons.KeyValueObject;
import es.di.framework.errores.FrameWorkServerInvokerException;
import es.di.framework.exceptions.BusinessRuleException;
import es.di.framework.service.controller.ISOAServiceController;
import net.sf.acegisecurity.AccountExpiredException;
import net.sf.acegisecurity.Authentication;
import net.sf.acegisecurity.AuthenticationException;
import net.sf.acegisecurity.AuthenticationServiceException;
import net.sf.acegisecurity.BadCredentialsException;
import net.sf.acegisecurity.CredentialsExpiredException;
import net.sf.acegisecurity.DisabledException;
import net.sf.acegisecurity.GrantedAuthority;
import net.sf.acegisecurity.GrantedAuthorityImpl;
import net.sf.acegisecurity.LockedException;
import net.sf.acegisecurity.UserDetails;
import net.sf.acegisecurity.context.Context;
import net.sf.acegisecurity.context.ContextHolder;
import net.sf.acegisecurity.context.security.SecureContext;
import net.sf.acegisecurity.context.security.SecureContextUtils;
import net.sf.acegisecurity.providers.AuthenticationProvider;
import net.sf.acegisecurity.providers.UsernamePasswordAuthenticationToken;
import net.sf.acegisecurity.providers.dao.SaltSource;
import net.sf.acegisecurity.providers.dao.User;
import net.sf.acegisecurity.providers.dao.UserCache;
import net.sf.acegisecurity.providers.dao.UsernameNotFoundException;
import net.sf.acegisecurity.providers.dao.cache.NullUserCache;
import net.sf.acegisecurity.providers.dao.event.AuthenticationFailureAccountExpiredEvent;
import net.sf.acegisecurity.providers.dao.event.AuthenticationFailureAccountLockedEvent;
import net.sf.acegisecurity.providers.dao.event.AuthenticationFailureCredentialsExpiredEvent;
import net.sf.acegisecurity.providers.dao.event.AuthenticationFailureDisabledEvent;
import net.sf.acegisecurity.providers.dao.event.AuthenticationFailureUsernameNotFoundEvent;
import net.sf.acegisecurity.providers.dao.event.AuthenticationSuccessEvent;
import net.sf.acegisecurity.providers.encoding.PasswordEncoder;
import net.sf.acegisecurity.providers.encoding.PlaintextPasswordEncoder;
import net.sf.acegisecurity.ui.WebAuthenticationDetails;
import utiles.Constantes;

/**
 * @author JSB
 *
 */
public class CacSOAAuthenticationProvider implements AuthenticationProvider, InitializingBean, ApplicationContextAware {

	private static final Log log = LogFactory.getLog( CacSOAAuthenticationProvider.class );
	
	private ApplicationContext context;
	private PasswordEncoder passwordEncoder = new PlaintextPasswordEncoder();
	private SaltSource saltSource;
	private UserCache userCache = new NullUserCache();
	private boolean forcePrincipalAsString = false;
	private boolean hideUserNotFoundExceptions = true;
	
	/** Propiedades utilizadas para la invocacion del Servicio **/
	private String serviceName;
	private String parameter;
	private String serviceSOABeanName;
	private ISOAServiceController serviceSOA;
	
	//carga los parametros desde el xml utilizando xstream
	private XStream xstream = new XStream();
	
	private void setServiceSOABeanName() throws Exception {
		Assert.notNull( this.context.getBean(this.serviceSOABeanName), "Does not exist a bean with this name : " + this.serviceSOABeanName );
		try {
			this.serviceSOA = (ISOAServiceController)this.context.getBean( this.serviceSOABeanName );
		} catch (ClassCastException cce) {
			if ( log.isInfoEnabled() ) {
				log.info( "The bean with the name " + this.serviceSOABeanName + " does not exist." );
			}
			throw new Exception( "The bean with the name " + this.serviceSOABeanName + " does not exist.", cce );
		}
	}

	/* (non-Javadoc)
	* @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	*/
	public void afterPropertiesSet() throws Exception {
		Assert.notNull( this.userCache, "A user cache must be set" );
		Assert.notNull( this.serviceSOABeanName, "A ISOAServiceControllerBeanName must be set" );
		Assert.notNull( this.serviceName, "A serviceName must be set" );
		Assert.notNull( this.parameter, "A parameter must be set" );
		
		this.xstream.alias( Constantes.S_ArrayList, ArrayList.class );
		this.xstream.alias( Constantes.S_KeyValueObject, KeyValueObject.class );
		this.xstream.alias( Constantes.S_key, String.class );
		this.xstream.alias( Constantes.S_value, String.class );
	}

	/* (non-Javadoc)
	* @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
	*/
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		this.context = arg0;
	}

	/**
	* @return Returns the forcePrincipalAsString.
	*/
	public boolean isForcePrincipalAsString() {
		return forcePrincipalAsString;
	}

	/**
	* @param forcePrincipalAsString The forcePrincipalAsString to set.
	*/
	public void setForcePrincipalAsString(boolean forcePrincipalAsString) {
		this.forcePrincipalAsString = forcePrincipalAsString;
	}

	/**
	* @return Returns the hideUserNotFoundExceptions.
	*/
	public boolean isHideUserNotFoundExceptions() {
		return hideUserNotFoundExceptions;
	}

	/**
	* @param hideUserNotFoundExceptions The hideUserNotFoundExceptions to set.
	*/
	public void setHideUserNotFoundExceptions(boolean hideUserNotFoundExceptions) {
		this.hideUserNotFoundExceptions = hideUserNotFoundExceptions;
	}

	/**
	* @return Returns the parameter.
	*/
	public String getParameter() {
		return parameter;
	}

	/**
	* @param parameter The parameter to set.
	*/
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	/**
	* @return Returns the passwordEncoder.
	*/
	public PasswordEncoder getPasswordEncoder() {
		return passwordEncoder;
	}

	/**
	* @param passwordEncoder The passwordEncoder to set.
	*/
	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	/**
	* @return Returns the saltSource.
	*/
	public SaltSource getSaltSource() {
		return saltSource;
	}

	/**
	* @param saltSource The saltSource to set.
	*/
	public void setSaltSource(SaltSource saltSource) {
		this.saltSource = saltSource;
	}

	/**
	* @return Returns the serviceName.
	*/
	public String getServiceName() {
		return serviceName;
	}

	/**
	* @param serviceName The serviceName to set.
	*/
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	/**
	* @return Returns the userCache.
	*/
	public UserCache getUserCache() {
		return userCache;
	}

	/**
	* @param userCache The userCache to set.
	*/
	public void setUserCache(UserCache userCache) {
		this.userCache = userCache;
	}
	
	/**
	* 
	* @see net.sf.acegisecurity.providers.AuthenticationProvider#authenticate(net.sf.acegisecurity.Authentication)
	*/
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		// Determine username
		String username = Constantes.NOMBRE_USUARIO_POR_DEFECTO;
		String password = Constantes.CADENA_VACIA;

		if (authentication.getPrincipal() != null) {
			username = authentication.getPrincipal().toString();
			password = authentication.getCredentials().toString();
		}

		if (authentication.getPrincipal() instanceof UserDetails) {
			username = ((UserDetails) authentication.getPrincipal()).getUsername();
			password = ((UserDetails) authentication.getPrincipal()).getPassword();
		}

		boolean cacheWasUsed = true;
		if (log.isDebugEnabled()) {
			log.debug("La cache de usuario : "+this.userCache.getClass().getName());
		}
		
		//Aki tenemos que hacer cosas raras, hemos de identificar si la peticion viene de un filtro de login (BASIC)
		// o de un filtro normal. La diferencia se encuentra en el objeto que vendra como parametro
		// Si viene de Basic este objeto lo crea Acegi con lo que no es de tipo AuthenticationUserDetails
		// si no sera de ese tipo.
		//Si es de ese tipo NO DEBEMOS PEDIRSELO A SERVICES ya que esto acabaria con la Sesionusuario que poseyeramos.
		// asi primero lo pedimos a la cache, comprobamos que sea el mismo password
		// si no lo es, lo pediremos a services, si lo es todo bien.
		
		UserDetails user = null;
		if (authentication.getPrincipal() instanceof ApplicationUserDetails) {
			user=this.userCache.getUserFromCache(username);
		} else {
			if (log.isDebugEnabled()) {
				log.debug("El usuario va a ser logado : (BASIC)");
			}
		}
		if (log.isDebugEnabled()) {
			if (log.isDebugEnabled()) {
				log.debug("El usuario recuperado por la cache : "+user);
			}
			
			/**
			*  Lo de abajo hay que comentarlo que salen planos
			*/
			/**if (user!=null) {
				log.debug("El password de la cache : "+user.getPassword());
				log.debug("El password de la usuario : "+password);
			}**/
		}
		// Check if the provided password is the same as the password in cache
		if (  ( user != null ) && !password.equals( user.getPassword() )  ) {
			user = null;
			this.userCache.removeUserFromCache(username);
		}
		// Trabajo con la cache para mantener la sincronia.
		if (user == null) {
			cacheWasUsed = false;
			try {
				/**
				* Esta linea es necesaria para que en la llamada a services
				* la cache pueda recuperar el header de invocacion.Cokiee del servidor, etc etc...
				*/
				( (SecureContext)ContextHolder.getContext() ).setAuthentication( authentication );
				if ( authentication.getPrincipal() instanceof ApplicationUserDetails ) {
					//log.info("Authentication : "+authentication.toString());
					user=(ApplicationUserDetails)authentication.getPrincipal();
				} else {
					//En este metodo implementaremos la llamada al servicioSOA
					try
					{
					user = getUserFromBackend(username,password);
					}
					catch(Exception ex)
					{
						log.error("Autenticación fallida");
						ex.printStackTrace();
					}
				}
				
			} catch (BadCredentialsException ex) {

				if (this.context != null) {

					context.publishEvent( new AuthenticationFailureUsernameNotFoundEvent(
						authentication,
						new User(  Constantes.CADENA_VACIA.equals(username)
							? "EMPTY_STRING_PROVIDED" : username, "*****",
							false, false, false, false,
							new GrantedAuthority[0] )  )
					);
				}

				throw ex;
			}
		}

		if (!user.isEnabled()) {

			if (this.context != null) {
				context.publishEvent(new AuthenticationFailureDisabledEvent(
						authentication, user));
			}
			if (log.isWarnEnabled()) {
				log.warn("The user : "+username+" is disabled.");
			}
			throw new DisabledException("User is disabled");
		}
		if (!user.isAccountNonExpired()) {
			if (this.context != null) {
				context.publishEvent(new AuthenticationFailureAccountExpiredEvent(
						authentication, user));
			}
			if (log.isWarnEnabled()) {
				log.warn("The user : "+username+" account has expired.");
			}
			throw new AccountExpiredException("User account has expired");
		}
		if ( !user.isAccountNonLocked() ) {
			if (this.context != null) {
				context.publishEvent(  new AuthenticationFailureAccountLockedEvent( authentication, user )  );
			}
			if (log.isWarnEnabled()) {
				log.warn("The user : "+username+" account is locked.");
			}
			throw new LockedException("User account is locked");
		}
		if ( !user.isCredentialsNonExpired() ) {
			if (this.context != null) {
				context.publishEvent(  new AuthenticationFailureCredentialsExpiredEvent( authentication, user )  );
			}
			if (log.isWarnEnabled()) {
				log.warn("The user : "+username+" credentials have expired.");
			}
			throw new CredentialsExpiredException( "User credentials have expired" );
		}
		if ( !cacheWasUsed ) {
			if( log.isInfoEnabled() ) {
				log.info("The user : "+user.toString()+" is push into the cache : "+this.userCache.toString());
			}
			// Put into cache
			this.userCache.putUserInCache(user);
			// As this appears to be an initial login, publish the event
			if (this.context != null) {
				context.publishEvent( new AuthenticationSuccessEvent( authentication, user )  );
			}
		}
		Object principalToReturn = user;
		if ( forcePrincipalAsString ) {
			principalToReturn = user.getUsername();
		}
		
		return createSuccessAuthentication( principalToReturn, authentication, user );
	}



	/** Esta parte de Authenticacion es identica a la del DAOProvider. ***/
	
	/**
	* Este metodo funciona igual que el de DAOAuthenticationProvider
	* @see net.sf.acegisecurity.providers.AuthenticationProvider#supports(java.lang.Class)
	*/
	public boolean supports(Class arg0) {
		if ( UsernamePasswordAuthenticationToken.class.isAssignableFrom(arg0) ) {
			return true;
		} else {
			return false;
		}
	}



	/**
	* Indicates whether the supplied <code>Authentication</code> object
	* provided appropriate credentials. This method can be called several
	* times throughout a single authentication request.
	* 
	* <P>
	* Protected so subclasses can override.
	* </p>
	*
	* @param authentication that was presented to the
	*		<code>DaoAuthenticationProvider</code> for validation
	* @param user that was loaded by the <code>AuthenticationDao</code>
	*
	* @return a boolean indicating whether the credentials were correct
	*/
	protected boolean isPasswordCorrect( Authentication authentication, UserDetails user ) {
		Object salt = null;

		if (this.saltSource != null) {
			salt = this.saltSource.getSalt(user);
		}
		if (log.isDebugEnabled()) {
			log.debug("Estamos en la validacion del password : ");
			if (salt != null) {
				log.debug("El objeto salt que hemos usado : "+salt.toString());
			} else {
				log.debug("No tenemos objeto Salt");
			}
			log.debug("El password encoder : "+this.passwordEncoder.toString());
			log.debug("El password a validar : "+user.getPassword());
			log.debug("Las credentials : "+authentication.getCredentials());
			
		}
		return passwordEncoder.isPasswordValid(user.getPassword(),
			authentication.getCredentials().toString(), salt);
	}
	
	
	
	/**
	* Creates a successful {@link Authentication} object.
	* 
	* <P>
	* Protected so subclasses can override.
	* </p>
	* 
	* <P>
	* Subclasses will usually store the original credentials the user supplied
	* (not salted or encoded passwords) in the returned
	* <code>Authentication</code> object.
	* </p>
	*
	* @param principal that should be the principal in the returned object
	*		(defined by the {@link #isForcePrincipalAsString()} method)
	* @param authentication that was presented to the
	*		<code>DaoAuthenticationProvider</code> for validation
	* @param user that was loaded by the <code>AuthenticationDao</code>
	*
	* @return the successful authentication token
	*/
	protected Authentication createSuccessAuthentication( Object principal, Authentication authentication, UserDetails user ) {
		// Ensure we return the original credentials the user supplied,
		// so subsequent attempts are successful even with encoded passwords.
		// Also ensure we return the original getDetails(), so that future
		// authentication events after cache expiry contain the details
		UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(principal,authentication.getCredentials(), user.getAuthorities());
		result.setDetails((authentication.getDetails() != null)? authentication.getDetails() : null);
		if (log.isDebugEnabled()) {
			log.debug("Username to create success authentication : "+result.toString());
		}
		return result;
	}
	
	
	
	private UserDetails getUserFromBackend(String username,String password) {
		String result;
		ApplicationUserDetails loadedUser;
		try {
			/** Aki cambiaremos el parameter para llamar a al authentication pasandole la Ip`
			* del cliente. Por eso no podemos usar el del proyecto Diwaf.
			*/
			/** Para ello utilizaremos el SecureContext que hemos creado que ya posee la Ip.
			*/
			Context aux = SecureContextUtils.getSecureContext();
			String auxiliar_parameter = null;
			if (  aux != null && aux instanceof CacSecureContextImpl  ) {
				String ip = Constantes.CADENA_VACIA;
				if (  ( (CacSecureContextImpl)aux ).getRealIp() != null &&
						!( (CacSecureContextImpl)aux ).getRealIp().equals( Constantes.CADENA_VACIA )  ) {
					auxiliar_parameter = new StringBuffer( Constantes.MARCA_INICIO_PARAMETRO_XML_AUTENTICACION )
						.append(  ( (CacSecureContextImpl)aux ).getRealIp()  )
						.append( Constantes.MARCA_CIERRE_PARAMETRO_XML_AUTENTICACION )
						.toString();
					} else {
						if (log.isWarnEnabled()) {
							if (log.isWarnEnabled()) {
								log.warn("El contexto no traia Ip, obteniendolo del remoteaddress. " );
							}
						}
						CacSecureContextImpl csi = (CacSecureContextImpl)aux;
						if (csi.getAuthentication()!=null)  {
							if (csi.getAuthentication().getDetails()!=null) {
								WebAuthenticationDetails wad = (WebAuthenticationDetails) csi.getAuthentication().getDetails();
								
								if (log.isInfoEnabled()) {
									log.info("La sesion que llega tiene el id:" + wad.getSessionId());
								}
								ip = wad.getRemoteAddress();
								if (log.isWarnEnabled()) {
									log.warn("La ip obtenida ha sido :" + ip );
								}
							} else {
								log.error("no tenemos detalles en Authentication");
							}
						} else {
							log.error("no tenemos Authentication");
						}
						auxiliar_parameter = new StringBuffer( Constantes.MARCA_INICIO_PARAMETRO_XML_AUTENTICACION )
							.append( ip )
							.append( Constantes.MARCA_CIERRE_PARAMETRO_XML_AUTENTICACION )
							.toString();
					}
			} else {
				if (log.isWarnEnabled()) {
					log.warn("El contexto es nulo o no es del tipo : "+CacSecureContextImpl.class.getName());
				}
			}
			if (log.isDebugEnabled()) {
				log.debug("esto es lo que vamos a enviar al servicio de login : "+auxiliar_parameter);
			}
			if ( auxiliar_parameter == null ) {
				auxiliar_parameter=this.parameter;
			}
			if ( this.serviceSOA == null ) {
				this.setServiceSOABeanName();
			}
			result = this.serviceSOA.process( this.serviceName, auxiliar_parameter );
		} catch (UsernameNotFoundException notFound) {
			if (log.isWarnEnabled()) {
				log.warn("Username not found in the service : "+this.serviceName,notFound);
			}
			if (hideUserNotFoundExceptions) {
				throw new BadCredentialsException("Bad credentials presented");
			} else {
				throw notFound;
			}
		} catch (DataAccessException repositoryProblem) {
			if (log.isWarnEnabled()) {
				log.warn( "DataAccessException in the service : " + this.serviceName,repositoryProblem );
			}
			throw new AuthenticationServiceException( repositoryProblem.getMessage(), repositoryProblem );
		} catch(BusinessRuleException bre) {
			if ( log.isWarnEnabled() ) {
				log.warn( "BussinesRuleException in the service : " + this.serviceName, bre );
			}
			throw new AuthenticationServiceException( bre.getMessage(), bre );
		} catch (FrameWorkServerInvokerException fwsie) {
			if (log.isWarnEnabled()) {
				log.warn("FrameWorkServerException in the service :"+this.serviceName,fwsie);
			}
			//Recibiremos el status_code en esta exception, 401, 403....
			//if ( fwsie.get_statusCode() != null && fwsie.get_statusCode().intValue() == 401 ) {
			if ( fwsie.get_statusCode() != null && fwsie.get_statusCode().intValue() == HttpStatus.SC_UNAUTHORIZED ) {
				throw new BadCredentialsException( "Bad credentials presented", fwsie );
			} else {
				throw new AuthenticationServiceException( fwsie.getMessage(), fwsie );
			}
		} catch ( Exception ex ) {
			if (log.isErrorEnabled()) {
				log.error("Exception in the service : "+this.serviceName , ex);
			}
			throw new AuthenticationServiceException(ex.getMessage(), ex);
		}
		if (result == null) {
			throw new AuthenticationServiceException( "ServiceSOA "+this.serviceName+" returned null, which is an interface contract violation" );
		}
		if (log.isDebugEnabled()) {
			log.debug("Esto es lo que recibimos en la llamada al login de services : "+result);
		}
		List lista = (ArrayList)this.xstream.fromXML(result);
		Map map = new HashMap();
		
		GrantedAuthority[] authorities = new GrantedAuthority[1];
		//log.info("El numero de elementos de la lista : "+lista.size());
		for (Iterator iter = lista.iterator(); iter.hasNext();) {
			KeyValueObject element = (KeyValueObject) iter.next();
			//log.info("Los objetos que recuperamos : "+element.getKey().toString()+" y el otro : "+element.getValue().toString());
			map.put(element.getKey().toString(),element.getValue().toString());
			
		}
		
		String element = (String)map.get(Constantes.S_authority0);
			//log.info("La authority : "+element);
		GrantedAuthority aux_granted = new GrantedAuthorityImpl(element);
		authorities[0]=aux_granted;
		
		element = (String)map.get(Constantes.S_authority1);
		if (element!=null) {
			aux_granted = new GrantedAuthorityImpl(element);
			authorities[0]=aux_granted;
		}
		loadedUser = new ApplicationUserDetails(username, password, true, true, true, true, authorities);
		String data = (String)map.get(Constantes.S_idusuario);
		//log.info("La idusuario : "+data);
		loadedUser.setIdUsuario(data);
		
		data = (String)map.get(Constantes.S_nombre);
		//log.info("La nombre : "+data);
		loadedUser.setNombre(data);
		
		data = (String)map.get(Constantes.S_apellidos);
		//log.info("La apellidos : "+data);
		loadedUser.setApellidos(data);  
		
		data = (String)map.get(Constantes.S_taquilla);
		if (data == null && log.isInfoEnabled()) {
			Context aux = SecureContextUtils.getSecureContext();
			if (aux != null && aux instanceof CacSecureContextImpl) {
				log.info("No hay taquilla para la ip " + ((CacSecureContextImpl)aux).getRealIp());
			} else {
				log.info("No hay taquilla debido a que no hay SecureContext ");
			}
		}
		loadedUser.setTaquilla(data);
		
		data = (String)map.get(Constantes.S_idtaquilla);
		if (data == null && log.isInfoEnabled()) {
			Context aux = SecureContextUtils.getSecureContext();
			if (aux != null && aux instanceof CacSecureContextImpl) {
				log.info("No hay idtaquilla para la ip " + ((CacSecureContextImpl)aux).getRealIp());
			} else {
				log.info("No hay idtaquilla debido a que no hay SecureContext ");
			}
		}
		loadedUser.setIdTaquilla(data);
		
		data = (String)map.get(Constantes.S_idcanal);
		loadedUser.setIdCanal(data);
		
		data = (String)map.get(Constantes.S_nombreCanal);
		loadedUser.setNombreCanal(data);
		
		data = (String)map.get(Constantes.S_nombreTipoCanal);
		loadedUser.setNombreTipocanal(data);
		
		data = (String)map.get(Constantes.S_idTipoCanal);
		loadedUser.setIdTipocanal(data);
		
		/*AÃ±adimos aplicacion por defecto*/
		data = (String)map.get(Constantes.S_appDefecto);
		loadedUser.setAppdefecto(data);
		return loadedUser;
	}

	public void setServiceSOABeanName(String serviceSOABeanName) {
		this.serviceSOABeanName = serviceSOABeanName;
	}

}

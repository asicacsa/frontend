package es.di.cac.acegisecurity;

import es.di.framework.acegisecurity.DiwafUserDetails;
import net.sf.acegisecurity.GrantedAuthority;
import net.sf.acegisecurity.UserDetails;
import utiles.Constantes;

/**
 * @author JSB
 *
 */
public class ApplicationUserDetails extends DiwafUserDetails {
	
	private static String nombrekey = "nombreKey";
	private static String apellidosKey ="apellidosKey";
	private static String idusuarioKey = "idusuarioKey";
	private static String taquillaKey = "taquillaKey";
	private static String idtaquillaKey = "idtaquillaKey";
	private static String idCanalKey = "idCanalKey";
	private static String nombreCanalKey = "nombreCanalKey";
	private static String nombreTipoCanalKey = "nombreTipoCanalKey";
	private static String idTipoCanalKey = "idTipoCanalKey";
	private static String appDefecto = Constantes.S_appDefecto;
	
	public ApplicationUserDetails( String username, String password, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired,
			boolean accountNonLocked, GrantedAuthority[] authorities ) {
		super(username,password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
	}
	
	public ApplicationUserDetails(UserDetails userDetails) {
		super(userDetails);
	}
	
	public Object getNombre() {
		return this.getPropiedades().get(ApplicationUserDetails.nombrekey);
	}
	
	public Object getApellidos() {
		return this.getPropiedades().get(ApplicationUserDetails.apellidosKey);
	}
	
	public Object  getIdUsuario() {
		return this.getPropiedades().get(ApplicationUserDetails.idusuarioKey);
	}
	
	public Object  getTaquilla() {
		return this.getPropiedades().get(ApplicationUserDetails.taquillaKey);
	}
	
	public Object  getIdTaquilla() {
		return this.getPropiedades().get(ApplicationUserDetails.idtaquillaKey);
	}
	
	public void setNombre(Object nombre) {
		this.getPropiedades().put(ApplicationUserDetails.nombrekey,nombre);
	}
	
	public void setApellidos(Object apellidos) {
		this.getPropiedades().put(ApplicationUserDetails.apellidosKey,apellidos);
	}
	
	public void setIdUsuario(Object idUsuario) {
		this.getPropiedades().put(ApplicationUserDetails.idusuarioKey,idUsuario);
	}
	
	public void setTaquilla(Object taquillaNombre) {
		this.getPropiedades().put(ApplicationUserDetails.taquillaKey,taquillaNombre);
	}
	
	public Object  setIdTaquilla(Object idtaquilla) {
		return this.getPropiedades().put(ApplicationUserDetails.idtaquillaKey,idtaquilla);
	}
	
	public void setIdCanal(Object idCanal) {
		this.getPropiedades().put(ApplicationUserDetails.idCanalKey,idCanal);
	}
	
	public Object getIdCanal() {
		return this.getPropiedades().get(ApplicationUserDetails.idCanalKey);
	}
	
	public void setNombreCanal(Object nombreCanal) {
		this.getPropiedades().put(ApplicationUserDetails.nombreCanalKey,nombreCanal);
	}
	
	public Object getNombreCanal() {
		return this.getPropiedades().get(ApplicationUserDetails.nombreCanalKey);
	}
	
	public void setIdTipocanal(Object idTipoCanal) {
		this.getPropiedades().put(ApplicationUserDetails.idTipoCanalKey,idTipoCanal);
	}
	
	public Object getIdTipocanal() {
		return this.getPropiedades().get(ApplicationUserDetails.idTipoCanalKey);
	}
	
	public void setNombreTipocanal(Object nombreTipocanal) {
		this.getPropiedades().put(ApplicationUserDetails.nombreTipoCanalKey,nombreTipocanal);
	}
	
	public Object getNombreTipocanal() {
		return this.getPropiedades().get(ApplicationUserDetails.nombreTipoCanalKey);
	}
	
	public void setAppdefecto(String appDefecto) {
		this.getPropiedades().put(ApplicationUserDetails.appDefecto,appDefecto);
	}
	
	public Object getAppdefecto() {
		return this.getPropiedades().get(ApplicationUserDetails.appDefecto);
	}
}
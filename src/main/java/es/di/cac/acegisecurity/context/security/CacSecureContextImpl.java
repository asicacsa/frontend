package es.di.cac.acegisecurity.context.security;

import net.sf.acegisecurity.context.security.SecureContextImpl;
import es.di.framework.acegisecurity.context.security.ICookieHolder;
import org.apache.commons.httpclient.Cookie;
import java.io.Serializable;

/**
 * @author JSB
 *
 */
public class CacSecureContextImpl extends SecureContextImpl implements ICookieHolder, Serializable {

	String realIp;
	Cookie cookie;

	public String getRealIp() {
		return realIp;
	}

	public void setRealIp(String realIp) {
		this.realIp = realIp;
	}

	public Cookie getCookie(){
		return cookie;
	}

	public void setCookie(Cookie cookie){
		this.cookie = cookie;
	}

	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append(super.toString());
		sb.append("; realIP: ").append((realIp==null)?"null":realIp);
		sb.append("; cookie: ").append((cookie==null)?"null":cookie.toString());
		return sb.toString();
	}

}
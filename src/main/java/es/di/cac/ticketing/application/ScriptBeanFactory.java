package es.di.cac.ticketing.application;

import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.ScriptRuntime;
import org.mozilla.javascript.ScriptableObject;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.script.Script;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionReaderUtils;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.interceptor.TransactionProxyFactoryBean;
import utiles.Constantes;
import es.di.cac.ticketing.application.javascript.JavaScriptFactory;
import es.di.cac.ticketing.application.javascript.JavaScriptScopeThreadLocal;
import es.di.cac.ticketing.application.javascript.support.Transaction;

public class ScriptBeanFactory extends DefaultListableBeanFactory implements ApplicationContextAware {
	
	private static final Log log = LogFactory.getLog(ScriptBeanFactory.class);
	
	protected static String FILE_PATTERN = "*.js";
	protected String path = "WEB-INF/action";
	protected boolean lazyLoading = false;
	protected boolean productionMode = false;
	protected ScriptObjectModel som;
	protected JavaScriptFactory factory;
	protected ApplicationContext appCtx;
	
	public ScriptBeanFactory(){
		super();
	}

	// Public setters

	public void setApplicationContext(ApplicationContext appCtx) {
		this.appCtx = appCtx;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public boolean isLazyLoading() {
		return lazyLoading;
	}

	public void setLazyLoading(boolean lazyLoading) {
		this.lazyLoading = lazyLoading;
	}

	public boolean isProductionMode() {
		return productionMode;
	}

	public void setProductionMode(boolean productionMode) {
		this.productionMode = productionMode;
	}

	// Init method

	public void startup() throws Exception {
		som = new ScriptObjectModel(appCtx);
		setupScriptScope();
		setupJavaScriptFactory();
		logger.info(">>>>>>>>>>>>> initScripts()");
		if(!isLazyLoading()) initScripts();
	}

	// Setup methods
	
	protected void setupScriptScope(){
		Context ctx = Context.enter();
		ScriptableObject scope = ScriptRuntime.getGlobal(ctx);
		//log.debug("insert som in context");
		ScriptableObject.putProperty(scope, "context", Context.javaToJS(som, scope));
		JavaScriptScopeThreadLocal.setScope(scope);
	}

	protected void setupJavaScriptFactory() {
		try{
//			AbstractBeanDefinition bd = BeanDefinitionReaderUtils.createBeanDefinition(JavaScriptFactory.class.getName(), null, null, null, getClassLoader());
			AbstractBeanDefinition bd = BeanDefinitionReaderUtils.createBeanDefinition(null, JavaScriptFactory.class.getName(), getClassLoader());
			bd.setScope(BeanDefinition.SCOPE_SINGLETON);
			bd.setBeanClass(JavaScriptFactory.class);
			registerBeanDefinition("jsFactory",bd);
			factory = (JavaScriptFactory) super.getBean("jsFactory",null,null);
			factory.setResourceLoader(appCtx);
		} catch(Exception ex){
			log.error("Error creating js factory", ex);
		}
	}

	protected void initScripts() throws Exception {
		String ruta = new StringBuffer(path).append(Constantes.MARCA_BUSQUEDA_SUBDIRECTORIOS).append( FILE_PATTERN ).toString();
		Resource[] res = appCtx.getResources( ruta );
		if(  res == null || res.length == 0  ) {
			log.info("No scripts found in location: " + ruta );
		} else {
			for(int i=0; i<res.length; i++){
				Resource resource = res[i];
				String name = resource.getFilename();
				Object bean = null;
				log.info("Loading script for first time: "+name);
				try {
					bean = getBean(name);
				} catch(Exception ex){
					log.error("Error creating script: "+name,ex);
				}
				if(bean==null){
					log.error("Script bean is null: "+name);
				}
			}
		}
	}



	protected ClassLoader getClassLoader(){
		return Thread.currentThread().getContextClassLoader();
	}

	// Bean registration methods

	protected void registerScriptBeanDefinition(String beanName) {
		try {
			// Check the file is there, it will throw an exception if its not
			String rutaBean = new StringBuffer( path ).append( Constantes.MARCA_BUSQUEDA_SUBDIRECTORIOS ).append( beanName ).toString();
			String rutaPatron = new StringBuffer( path ).append( Constantes.MARCA_BUSQUEDA_SUBDIRECTORIOS ).append( FILE_PATTERN ).toString();
			Resource[] res = appCtx.getResources( rutaBean );
			if(res == null || res.length==0){
				log.info("No scripts found in location: " + rutaPatron );
			} else {
				if (res.length > 1) {
					log.warn("More than one scripts found with the same name in this location: " + rutaPatron );
				} else {
					Resource resource = null;
					for (int i=0; i<res.length; i++){
						resource = res[i];

						resource.getFile().lastModified();
						// Continue to load bean definition
						ConstructorArgumentValues cargs = new ConstructorArgumentValues();
						//cargs.addIndexedArgumentValue(0, path + beanName);
						String route = resource.getURL().getFile().substring(resource.getURL().getFile().indexOf(path));
						cargs.addIndexedArgumentValue(0,route);
//						AbstractBeanDefinition bd = BeanDefinitionReaderUtils.createBeanDefinition(null, null, cargs, null, this.getClassLoader());
						AbstractBeanDefinition bd = BeanDefinitionReaderUtils.createBeanDefinition(null,null,this.getClassLoader());
						bd.setConstructorArgumentValues(cargs);
						bd.setFactoryBeanName("jsFactory");
						bd.setFactoryMethodName("create");
						bd.setScope(BeanDefinition.SCOPE_SINGLETON);
						registerBeanDefinition(beanName,bd);
					}
				}
			}
		} catch(Exception ex){
			registerErrorBeanDefinition(beanName, ex);
		}
	}
	
	
	
	/**
	 * TODO API 15 Octubre 2010 Analizar esto en detalle, aqui hay un bean que cumple la interfaz {@link PlatformTransactionManager} que podría ser el motivo por el cual las transacciones no se inician desde el invocador/parte del CLUB de TISSAT, que no esta del todo claro el origen del mal.
	 * TODO API 15 Octubre 2010 ptrotected void registerWrapperBeanDefinition( Object bean ) a este metodo no se le hace referencia desde ningun sitio, algo se me esta escapando. 
	 * @param bean
	 */
	protected void registerWrapperBeanDefinition( Object bean ) {
		try {
			TransactionProxyFactoryBean proxy = new TransactionProxyFactoryBean();
			Transaction wrapped = (Transaction) bean;
			
			String tmName = wrapped.getTransactionManagerName();
			Properties tAttr = wrapped.getTransactionAttributes();
			PlatformTransactionManager tm = (PlatformTransactionManager) appCtx.getBean(tmName);
			if ( false && tm == null ) {
				log.error("Could not find transaction manager called '"+tmName);
				return;
			}
			proxy.setTransactionManager(tm);
			proxy.setTransactionAttributes(tAttr);
			
			Class[] iClazz = bean.getClass().getInterfaces();
			String[] iNames = new String[iClazz.length-1];
			int j = 0;
			for(int k=0; k<iClazz.length; k++){
				if(iClazz[k]!=Transaction.class){
					iNames[j++] = iClazz[k].getName();
				}
			}
			proxy.setProxyInterfaces(iClazz);
			//proxy.setProxyInterfaces(iNames);
			proxy.setTarget(bean);
			proxy.setBeanFactory(appCtx);
			proxy.afterPropertiesSet();
			registerSingleton(bean.getClass().getName()+"_Wrapped",proxy.getObject());
		} catch( Exception ex ) {
			log.error( "Error creating wrapper",ex );
		}
	}
	
	
	
	protected Object wrapIfNeeded(Object bean){
		if(!(bean instanceof Transaction)) return bean;
		if(!containsBean(bean.getClass().getName()+"_Wrapped"))
			registerWrapperBeanDefinition(bean);
		return super.getBean(bean.getClass().getName()+"_Wrapped");
	}
	
	
	
	protected void registerErrorBeanDefinition(String beanName, Exception ex){
		try{
			ConstructorArgumentValues cargs = new ConstructorArgumentValues();
			cargs.addIndexedArgumentValue(0, ex);
//			AbstractBeanDefinition bd = BeanDefinitionReaderUtils.createBeanDefinition(ScriptErrorBean.class.getName(),null,cargs,null,this.getClassLoader());
			AbstractBeanDefinition bd = BeanDefinitionReaderUtils.createBeanDefinition(null,ScriptErrorBean.class.getName(),this.getClassLoader());
			bd.setConstructorArgumentValues(cargs);
			bd.setScope(BeanDefinition.SCOPE_SINGLETON);
			registerBeanDefinition(beanName,bd);
		} catch ( ClassNotFoundException ex2 ) {
			log.error("Class not found exception while creating error bean", ex2);
		}
	}
	
	// Public beanFactory methods
	public Object getBean(String name) throws BeansException {
		return getBean(name, null, null);
	}
		
	public Object getBean(String name, Class requiredType) throws BeansException {
		return getBean(name, requiredType, null);
	}

	/**
	 * Return the bean with the given name,
	 * checking the parent bean factory if not found.
	 * @param name the name of the bean to retrieve
	 * @param args arguments to use if creating a prototype using explicit arguments to a
	 * static factory method. It is invalid to use a non-null args value in any other case.
	 */
	public Object getBean(String name, Object[] args) throws BeansException {
		return getBean(name, null, args);
	}



	public Object getBean(String name, Class requiredType, Object[] args) throws BeansException {
		setupScriptScope();
		Object bean = null;
		if(containsBean(name)){
			bean = super.getBean(name, requiredType, args);
			// if production mode, dont check for reload
			if(productionMode) return wrapIfNeeded(bean);
			// otherwise check the script object
			Script script = null;
			if(factory!=null) script = factory.lookupScript(bean);
			if(script!=null && script.isModified()) 
				registerScriptBeanDefinition(name);
			else { return wrapIfNeeded(bean); }
		} else registerScriptBeanDefinition(name);
		return wrapIfNeeded(super.getBean(name, requiredType, args));
	}

}
package es.di.cac.ticketing.application.javascript;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.script.AbstractScriptFactory;
import org.springframework.beans.factory.script.Script;
import org.springframework.beans.factory.script.ScriptInterfaceException;

import utiles.Constantes;

public class JavaScriptFactory extends AbstractScriptFactory {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3258693191396898612L;
	private long id = 0;
	
	protected final Log log = LogFactory.getLog(getClass());
	

	public JavaScriptFactory(){
		this.setExpirySeconds( Constantes.UNO );
	}
	
	public Script lookupScript(Object o) {
		return super.lookupScript(o);
	}


	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.script.AbstractScriptFactory#createScript(java.lang.String)
	 */
	protected Script createScript(String location) throws BeansException {
		
		if(log.isDebugEnabled())
			log.debug("Creating script from: "+location);
		
		JavaScript js = new JavaScript(Long.toString(id++),location, this);
		log.debug("created javascript");
		return js;
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.script.AbstractScriptFactory#requiresConfigInterface()
	 */
	protected boolean requiresConfigInterface() {
		return false;
	}

	public Script configuredScript(String location, String[] interfaceNames) throws BeansException {
		
		JavaScript script = (JavaScript) createScript(location);
		
		if(interfaceNames!=null && interfaceNames.length > 0){
		
			try {
				Class first = Class.forName(interfaceNames[0]);
				if(!first.isInterface()) script.setExtends(first);
				String[] trueInterfaceNames = new String[interfaceNames.length-1];
				for(int i=0; i<trueInterfaceNames.length; i++){
					trueInterfaceNames[i] = interfaceNames[i+1];
				}
				interfaceNames = trueInterfaceNames;
			}
			catch (ClassNotFoundException ex) {
				throw new ScriptInterfaceException(ex);
			}
		
		}
		
		// Add interfaces. This will not include any config interface.
		try {
			Class[] interfaces = this.toInterfaceArray(interfaceNames);
			for (int i = 0; i < interfaces.length; i++) {
				script.addInterface(interfaces[i]);
			}
			
			return script;
		}
		catch (ClassNotFoundException ex) {
			throw new ScriptInterfaceException(ex);
		}
	}
	
	
    	private Class[] toInterfaceArray(String[] interfaceNames) throws IllegalArgumentException, ClassNotFoundException {
 
         Class interfaces[] = new Class[interfaceNames.length];
         for (int i = 0; i < interfaceNames.length; i++) {
             interfaces[i] = Class.forName(interfaceNames[i].trim(), true, Thread.currentThread().getContextClassLoader());      
             if (!interfaces[i].isInterface()) {
                 throw new IllegalArgumentException("Can proxy only interfaces: [" + interfaces[i].getName() + "] is a class");
             }
         }
         return interfaces;
     }
	
}

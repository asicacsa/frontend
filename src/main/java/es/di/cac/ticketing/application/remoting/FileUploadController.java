/**
 * Handling of file uploads.
 */
package es.di.cac.ticketing.application.remoting;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import es.di.cac.ticketing.application.FileUploadBean;
import utiles.Constantes;

/**
 * @author JAN
 *
 */
public class FileUploadController extends SimpleFormController {

	private String successViewIngresosBancarios;
	private String successViewGestionDocumentos;

	private static final String PREFIJO = "WEB-INF/jsp/";
	private static final String SUFIJO = ".jsp";


	public void setSuccessViewIngresosBancarios(String successViewIngresosBancarios) {
		this.successViewIngresosBancarios = successViewIngresosBancarios;
	}

	public void setSuccessViewGestionDocumentos(String successViewGestionDocumentos) {
		this.successViewGestionDocumentos = successViewGestionDocumentos;
	}



	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
	
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");
		Date hoy = new Date();
		String filerepository = request.getParameter("filerepository");
		String modo = request.getParameter("modo");
		String strMaxSizeFileUpload = request.getParameter("maxSizeFileUpload");
		String filepath=Constantes.CADENA_VACIA;
		String extension=Constantes.CADENA_VACIA;
		String filename=sdf.format(hoy);
		// cast the bean
		FileUploadBean bean = (FileUploadBean)command;
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request; 
		CommonsMultipartFile fileUp = (CommonsMultipartFile) multipartRequest.getFile("file");
		int posicion=fileUp.getOriginalFilename().indexOf(Constantes.PUNTO);
		if (posicion>0){
			extension=fileUp.getOriginalFilename().substring(posicion,fileUp.getOriginalFilename().length());
			filename+=extension;
		}
		// let's see if there's content there
		byte[] file = bean.getFile();
		Integer maxSizeFileUpload = Constantes.TRES;
		if (strMaxSizeFileUpload != null)
			maxSizeFileUpload = Integer.valueOf(strMaxSizeFileUpload);
		if (file == null) {
			// hmm, that's strange, the user did not upload anything
		} else {
			//verificar si el tamaño es permitido
			long maxBytes = maxSizeFileUpload.intValue() *1024*1024;
			if(file.length > maxBytes){
				return devolverVistaError("el tamaño maximo permitido para subir ficheros es " + maxSizeFileUpload + " Mbytes", "ERROR !");
				//throw new IllegalArgumentException("el tamaño maximo permitido del archivo es "+ maxSizeFileUpload+"mb.");
			}
			// looks for the dir, and creates it if it doesn't exists
			File dirpath = new File(filerepository);
			if (!dirpath.exists()) {
				dirpath.mkdir();
			}
			if(modo != null){
				if(modo.equals("gestiondocumentos")){
					filepath = filerepository + filename;
				}else if(modo.equals("ingresosbancarios")){
					// "fib" comes from "fichero de ingresos bancarios"
					// File.separator
					filepath = filerepository + "fib" + String.valueOf(System.currentTimeMillis());
				}
			}
			FileOutputStream fout = new FileOutputStream(filepath);
			fout.write(file);
			fout.close();
		}
		if(modo != null){
			if(modo.equals("gestiondocumentos")){
				request.setAttribute("filepath", filepath);
				request.setAttribute("filename", filename);
				this.setSuccessView(this.successViewGestionDocumentos);
			}else if(modo.equals("ingresosbancarios")){
				// well, let's do nothing with the bean for now and return:
				request.setAttribute("filepath", java.net.URLEncoder.encode(filepath,Constantes.ENCODING_UTF8));
				request.setAttribute("filename", java.net.URLEncoder.encode(fileUp.getOriginalFilename(),Constantes.ENCODING_UTF8));
				this.setSuccessView(this.successViewIngresosBancarios);
			}
		}
		return super.onSubmit(request,response,command,errors);
	}



	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws ServletException {
		// to actually be able to convert Multipart instance to byte[]
		// we have to register a custom editor (in this case the
		// ByteArrayMultipartEditor
		binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
		// now Spring knows how to handle multipart object and convert them
	}



	private ModelAndView devolverVistaError(String mensajeError, String mensajeEncabezado) {
		HashMap map = new HashMap();
		map.put("mensaje", mensajeError);
		map.put("mensajeEncabezado", mensajeEncabezado);
		return new ModelAndView(PREFIJO + "error" + SUFIJO, map);
	}
}
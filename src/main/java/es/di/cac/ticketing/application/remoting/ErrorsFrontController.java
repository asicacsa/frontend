package es.di.cac.ticketing.application.remoting;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.acegisecurity.AccessDeniedException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.logging.Log; 
import org.apache.commons.logging.LogFactory;
import org.mozilla.javascript.JavaScriptException;
import org.mozilla.javascript.NativeJavaObject;
import org.mozilla.javascript.WrappedException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.remoting.RemoteAccessException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.util.HtmlUtils;
import utiles.Constantes;
import com.thoughtworks.xstream.converters.ConversionException;
import es.di.framework.errores.FrameWorkServerInvokerException;
import es.di.framework.exceptions.BusinessRuleException;
import es.di.framework.exceptions.PrintingException;

public class ErrorsFrontController implements Controller {
	
	protected static final Log log = LogFactory.getLog(ErrorsFrontController.class);
	
	//public static final String CAC_ERRORIMPRESION = " ATENCION: Ha habido un error de impresion. Reintente la operacion. Si se han obtenido algunas entradas estas no son validas Error: ";
	public static final String CAC_ERRORIMPRESION = " ATENCION: Se ha detectado una situacion inesperada al intentar imprimr: ";
	public static final String CAC_ERRORCODE = "CAC-ERRORCODE";
	public static final String CAC_ERRORMSG = "CAC-ERRORMSG";
	public static final String CAC_ERRORDETAIL = "CAC-ERRORDETAIL";
	public static final String CAC_ERRORSEVERITY = "CAC-ERRORSEVERITY";

	private Controller controller;
	
	public void setController(Controller controller) {
		this.controller = controller;
	}

	public ModelAndView handleRequest(HttpServletRequest req, HttpServletResponse res) throws Exception {
	
		ModelAndView mv = null;
		String msnerror=null;
		
		try {
			mv = controller.handleRequest(req, res);
		} catch (BusinessRuleException ex) {

			res.addHeader(CAC_ERRORCODE,"Mensaje del sistema");
			res.addHeader(CAC_ERRORMSG,ex.getMessage().trim());
			res.addHeader(CAC_ERRORDETAIL,ex.getMessage().trim());
			res.addHeader(CAC_ERRORSEVERITY,String.valueOf(ex.getSeverity()).trim());
			res.setStatus(HttpStatus.SC_OK);
			
			// el contenido del response debe ser not null
			// puede contener lo que queramos (ej, campos de la validacion que fallaron)
			res.getWriter().write(Constantes.NODO_XML_error_VACIO);
			res.setCharacterEncoding(Constantes.ENCODING_UTF8);
			res.setContentType(Constantes.CONTENT_TYPE_TEXT_XML);
		} catch (IllegalArgumentException ex) {
			res.addHeader(CAC_ERRORCODE,"Mensaje del sistema");
			res.addHeader(CAC_ERRORMSG,ex.getMessage().trim());
			res.addHeader(CAC_ERRORDETAIL,Constantes.CADENA_VACIA);
			res.addHeader(CAC_ERRORSEVERITY, String.valueOf(BusinessRuleException.SEVERITY_WARNING).trim());
			res.setStatus(HttpStatus.SC_OK);
			// el contenido del response debe ser not null
			// puede contener lo que queramos (ej, campos de la validacion que fallaron)
			res.getWriter().write(Constantes.NODO_XML_error_VACIO);
			res.setCharacterEncoding(Constantes.ENCODING_UTF8);
			res.setContentType(Constantes.CONTENT_TYPE_TEXT_XML);
		} catch (PrintingException ex) {
			res.addHeader(CAC_ERRORCODE,"Problema de impresion");
			res.addHeader(CAC_ERRORMSG,ex.getMessage().trim());
			res.addHeader(CAC_ERRORDETAIL,ex.getMessage().trim());
			res.addHeader(CAC_ERRORSEVERITY, "-666"	);
			res.setStatus(HttpStatus.SC_OK);
			// el contenido del response debe ser not null
			// puede contener lo que queramos (ej, campos de la validacion que fallaron)
			res.getWriter().write(Constantes.NODO_XML_error_VACIO);
			res.setCharacterEncoding(Constantes.ENCODING_UTF8);
			res.setContentType(Constantes.CONTENT_TYPE_TEXT_XML);
		} catch (InvocationTargetException ex) {
			InvocationTargetException itex = (InvocationTargetException) ex;
			String auxiliar_error = null;
			if (log.isDebugEnabled()) {
				log.debug("Error en la peticion a services : ",ex);
				log.debug("La causa : ",ex.getTargetException());
			}
			if(itex.getTargetException()!=null) {
				if (itex.getTargetException() instanceof WrappedException) {
					if (log.isDebugEnabled()) {
						log.debug("Error en la peticion : "+((WrappedException)itex.getTargetException()).getCause().getMessage());
					}
					if (itex.getTargetException().getCause() != null && itex.getTargetException().getCause() instanceof RemoteAccessException) {
						if (itex.getTargetException().getCause().getCause() != null && itex.getTargetException().getCause().getCause() instanceof FrameWorkServerInvokerException) {
							if (((FrameWorkServerInvokerException)itex.getTargetException().getCause().getCause()).get_statusCode().toString().equals(Constantes.CADENA_401)) {
								auxiliar_error=Constantes.CADENA_401;
							} else if (((FrameWorkServerInvokerException)itex.getTargetException().getCause().getCause()).get_statusCode().toString().equals(Constantes.CADENA_403)) {
								auxiliar_error=Constantes.CADENA_403;
							} else {
								if (log.isDebugEnabled()) {
									log.debug("Error 105. Detectado con esta linea : ( itex.getTargetException().getCause().getCause() != null && itex.getTargetException().getCause().getCause() instanceof FrameWorkServerInvokerException )",itex.getTargetException().getCause().getCause());
								}
								auxiliar_error=Constantes.CADENA_105;
							}
						} else {
							auxiliar_error=Constantes.CADENA_107;
						}
					} else if (itex.getTargetException().getCause()!= null && itex.getTargetException().getCause() instanceof AccessDeniedException) {
						auxiliar_error=Constantes.CADENA_403;
						//Este status NO ES UN HttpStatus.SC_OK, no escribir en el response...
					} else {
						if (log.isErrorEnabled()) {
							log.error("Error 105. Detectado con esta linea : ( itex.getTargetException().getCause()!= null && !(itex.getTargetException().getCause() instanceof AccessDeniedException) )",itex.getTargetException().getCause().getCause());
						}
						auxiliar_error=Constantes.CADENA_105;
					}
				} else if (itex.getTargetException() instanceof AccessDeniedException) {
					auxiliar_error=Constantes.CADENA_105;
				} else if (itex.getTargetException() instanceof JavaScriptException) {
					if (((JavaScriptException)ex.getTargetException()).getValue() instanceof NativeJavaObject) {
						if (((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap() instanceof RemoteAccessException) {
							if (((RemoteAccessException)((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap()).getCause() != null && ((RemoteAccessException)((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap()).getCause() instanceof FrameWorkServerInvokerException) {
								if (((FrameWorkServerInvokerException)((RemoteAccessException)((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap()).getCause()).get_statusCode().toString().equals(Constantes.CADENA_401)) {
									auxiliar_error=Constantes.CADENA_401;
								} else if (((FrameWorkServerInvokerException)((RemoteAccessException)((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap()).getCause()).get_statusCode().toString().equals(Constantes.CADENA_403)) {
									auxiliar_error=Constantes.CADENA_403;
								} else if (((FrameWorkServerInvokerException)((RemoteAccessException)((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap()).getCause()).get_statusCode().toString().equals(Constantes.CADENA_0)) {
									try{
										msnerror=((FrameWorkServerInvokerException)((RemoteAccessException)((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap()).getCause()).getMessage();
									}catch(Exception Excp){
										msnerror="FrameWorkServerInvokerException, No podemos obtener el mensaje de error"; 
									}
									res.addHeader(CAC_ERRORCODE,"Mensaje del sistema");
									res.addHeader(CAC_ERRORMSG, msnerror );
									res.addHeader(CAC_ERRORDETAIL,Constantes.CADENA_VACIA);
									res.addHeader(CAC_ERRORSEVERITY, Constantes.CADENA_VACIA);
									res.setStatus(HttpStatus.SC_OK);
									res.getWriter().write(Constantes.NODO_XML_error_VACIO);
									res.setCharacterEncoding(Constantes.ENCODING_UTF8);
									res.setContentType(Constantes.CONTENT_TYPE_TEXT_XML);
									return mv;
								} else {
									if (log.isDebugEnabled()) {
										log.debug("Error 105. Detectado con esta linea : ( ((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap() instanceof RemoteAccessException )",((RemoteAccessException)((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap()));
									}
									auxiliar_error=Constantes.CADENA_105;
								}
							}else{
								auxiliar_error=Constantes.CADENA_107;
							}
						//Añadimos nuevos mensajes de error no tratados...
						} else if (((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap() instanceof AccessDeniedException) {
							 auxiliar_error="AccessDeniedException";
						} else if (((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap() instanceof DataIntegrityViolationException) {
							 auxiliar_error="DataIntegrityViolationException";
						} else if (((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap() instanceof UncategorizedSQLException) {
							 auxiliar_error="UncategorizedSQLException"; 
						} else if (((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap() instanceof ConversionException) {
							 auxiliar_error="ConversionException";
						} else if (((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap() instanceof IllegalArgumentException) {
							auxiliar_error=Constantes.CADENA_107;
						} else if ( ((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap() instanceof BusinessRuleException) {
							auxiliar_error="BusinessRuleException";
						} else if ( ((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap() instanceof PrintingException) {
							auxiliar_error="PrintingException";
						} else if (((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap() instanceof IOException) {
							auxiliar_error=Constantes.CADENA_107;
						}else{
							auxiliar_error=Constantes.CADENA_107;
						}
					} else {
						auxiliar_error=Constantes.CADENA_107;
					}
				} else {
					auxiliar_error=Constantes.CADENA_105;
				}
			} else {
				auxiliar_error=Constantes.CADENA_106;
			}
			if (auxiliar_error.equals("PrintingException")) {
				res.addHeader(CAC_ERRORCODE,"ADVERTENCIA IMPORTANTE");
				if (itex.getTargetException() != null && itex.getTargetException().getCause() != null && itex.getTargetException().getCause().getLocalizedMessage() != null) {
					//res.addHeader(CAC_ERRORMSG, CAC_ERRORIMPRESION + itex.getTargetException().getCause().getLocalizedMessage().trim());
					res.addHeader(CAC_ERRORMSG, CAC_ERRORIMPRESION + itex.getTargetException().getCause().getLocalizedMessage().trim() + " .Verifique que la aplicacion que gestiona las impresoras esta ejecutandose y la impresora se encuentra operativa antes de reintenetar la imprimesion." );
					res.addHeader(CAC_ERRORDETAIL,Constantes.CADENA_VACIA);
				} else {
					try {
						//Intentamos obtener el error del objeto que nos viene
						//res.addHeader(CAC_ERRORMSG, CAC_ERRORIMPRESION +  convertError(((Exception)((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap()).getMessage().trim()));
						res.addHeader(CAC_ERRORMSG, CAC_ERRORIMPRESION +  convertError(((Exception)((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap()).getMessage().trim()) + " .Verifique que la aplicacion que gestiona las impresoras esta ejecutandose y la impresora se encuentra operativa antes de reintenetar la imprimesion." );
						res.addHeader(CAC_ERRORDETAIL,Constantes.CADENA_VACIA);
					} catch (Exception e) {
						//res.addHeader(CAC_ERRORMSG,"Error en un javascript se esta realizando un throw de una Exception de javascript.");
						Throwable excause;
						try{
							excause = (Exception)((NativeJavaObject)((JavaScriptException)ex.getCause()).getValue()).unwrap();
						} catch (Exception auxex){
							excause = ex.getCause();
						}
						StackTraceElement[] traza = ex.getStackTrace();
						StackTraceElement[] trazacause = excause.getStackTrace();
						if (log.isErrorEnabled()){
							String errordetail = "Se ha capturado un error no controlado tipo 107: " + ex.toString() + Constantes.SALTO_LINEA;
							for (int i=0; i<traza.length; i++){
								errordetail = Constantes.DOBLE_ESPACIO_BLANCO + errordetail + traza[i] + Constantes.SALTO_LINEA;
							}
							errordetail = errordetail + "Caused by: " + excause.toString() + Constantes.SALTO_LINEA;
							for (int i=0; i<trazacause.length; i++){
								errordetail = Constantes.DOBLE_ESPACIO_BLANCO + errordetail + trazacause[i] + Constantes.SALTO_LINEA;
							}
							log.error(errordetail);
						}
						res.addHeader(CAC_ERRORMSG, CAC_ERRORIMPRESION +  "Se ha capturado un error no controlado tipo 107");
						res.addHeader(CAC_ERRORDETAIL, ex.toString() + " Caused by: " + excause.toString());
					}
				}
				res.addHeader(CAC_ERRORSEVERITY,"-666");
				res.setStatus( HttpStatus.SC_OK );
				res.getWriter().write(Constantes.NODO_XML_error_VACIO);
				res.setCharacterEncoding(Constantes.ENCODING_UTF8);
				res.setContentType(Constantes.CONTENT_TYPE_TEXT_XML);
			}else if (auxiliar_error.equals("AccessDeniedException")) {
				res.addHeader(CAC_ERRORCODE,"Mensaje del sistema");
				res.addHeader(CAC_ERRORMSG, " No tiene permiso para ejecutar esta funcionalidad, consulte con su administrador ");
				res.addHeader(CAC_ERRORDETAIL,Constantes.CADENA_VACIA);
				res.addHeader(CAC_ERRORSEVERITY, Constantes.CADENA_VACIA);
				res.setStatus(HttpStatus.SC_OK);
				res.getWriter().write(Constantes.NODO_XML_error_VACIO);
				res.setCharacterEncoding(Constantes.ENCODING_UTF8);
				res.setContentType(Constantes.CONTENT_TYPE_TEXT_XML);
			}else if (auxiliar_error.equals("BusinessRuleException")) {
				try{
					msnerror=((es.di.framework.exceptions.BusinessRuleException)(((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap())).getMessage();
				}catch(Exception Excp){
					msnerror="BusinessRuleException, No podemos obtener el mensaje de error";
				}
				if (msnerror==null){
					msnerror="BusinessRuleException";
				}
				res.addHeader(CAC_ERRORCODE,"Mensaje del sistema");
				res.addHeader(CAC_ERRORMSG, convertError(msnerror) );
				res.addHeader(CAC_ERRORDETAIL,Constantes.CADENA_VACIA);
				res.addHeader(CAC_ERRORSEVERITY, Constantes.CADENA_VACIA);
				res.setStatus(HttpStatus.SC_OK);
				res.getWriter().write(Constantes.NODO_XML_error_VACIO);
				res.setCharacterEncoding(Constantes.ENCODING_UTF8);
				res.setContentType(Constantes.CONTENT_TYPE_TEXT_XML);
			}else if (auxiliar_error.equals("ConversionException")) {
				res.addHeader(CAC_ERRORCODE,"Mensaje del sistema");
				res.addHeader(CAC_ERRORMSG, "Alguno de los campos no tiene el formato correcto");
				res.addHeader(CAC_ERRORDETAIL,"Por favor, revise el formato ");
				res.addHeader(CAC_ERRORSEVERITY, "Si el problema persiste, contacte con su administrador");
				res.setStatus(HttpStatus.SC_OK);
				res.getWriter().write(Constantes.NODO_XML_error_VACIO);
				res.setCharacterEncoding(Constantes.ENCODING_UTF8);
				res.setContentType(Constantes.CONTENT_TYPE_TEXT_XML);
			}else if (auxiliar_error.equals("DataIntegrityViolationException")) {
				try{
					msnerror=((DataIntegrityViolationException)(((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap())).getMessage();
				}catch(Exception Excp){
					msnerror="BusinessRuleException, No podemos obtener el mensaje de error"; 
				}
				res.addHeader(CAC_ERRORCODE,"Mensaje del sistema");
				res.addHeader(CAC_ERRORMSG, "Alguno de los campos no cumple una restriccion obligatoria");
				res.addHeader(CAC_ERRORDETAIL,"Por favor, revise los valores introducidos ");
				res.addHeader(CAC_ERRORSEVERITY, "Si el problema persiste, contacte con su administrador");
				res.setStatus(HttpStatus.SC_OK);
				res.getWriter().write(Constantes.NODO_XML_error_VACIO);
				res.setCharacterEncoding(Constantes.ENCODING_UTF8);
				res.setContentType(Constantes.CONTENT_TYPE_TEXT_XML);
			}else if (auxiliar_error.equals("UncategorizedSQLException")) {
				// recogemos posibles excepciones SQL no categorizadas
				// e interpretamos los códigos de error.
				int errorcode = 0;
				try{
					UncategorizedSQLException error = (UncategorizedSQLException) (((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap());
					msnerror = error.getMessage();
					errorcode = error.getSQLException().getErrorCode();
				}catch(Exception Excp){
					msnerror="UncategorizedSQLException, No podemos obtener el mensaje de error"; 
				}
				// ORA-12899: el valor es demasiado grande para la columna
				if (errorcode==12899){
					res.addHeader(CAC_ERRORCODE,"Mensaje del sistema");
					res.addHeader(CAC_ERRORMSG, "Alguno de los campos no cumple una restriccion de longitud de campo");
					res.addHeader(CAC_ERRORDETAIL,"Por favor, revise los valores introducidos ");
					res.addHeader(CAC_ERRORSEVERITY, "Si el problema persiste, contacte con su administrador");
				// GENÉRICO
				} else {
					res.addHeader(CAC_ERRORCODE,"Mensaje del sistema");
					res.addHeader(CAC_ERRORMSG, "Se ha producido un problema accediendo a la base de datos (codigo "+ errorcode +Constantes.CIERRE_PARENTESIS);
					res.addHeader(CAC_ERRORDETAIL,"Por favor, revise los valores introducidos ");
					res.addHeader(CAC_ERRORSEVERITY, "Si el problema persiste, contacte con su administrador");
				}
				res.setStatus(HttpStatus.SC_OK);
				res.getWriter().write(Constantes.NODO_XML_error_VACIO);
				res.setCharacterEncoding(Constantes.ENCODING_UTF8);
				res.setContentType(Constantes.CONTENT_TYPE_TEXT_XML);
			}else if (auxiliar_error.equals(Constantes.CADENA_106)) {
				res.addHeader(CAC_ERRORCODE,"Mensaje del sistema");
				res.addHeader(CAC_ERRORMSG,"El sistema tiene problemas para acceder correctamente al servidor de datos o al servicio");
				String detail = null;
				if (itex.getCause()!= null && itex.getCause().getLocalizedMessage()!= null) {
					detail = itex.getCause().getLocalizedMessage();
				} else {
					if (itex.getLocalizedMessage()!= null) {
						detail= "No existe la causa de la excepcion. "+itex.getMessage();
					} else {
						detail= "No existe la causa de la excepcion. ";
					}
				}
				res.addHeader(CAC_ERRORDETAIL,detail.trim());
				res.addHeader(CAC_ERRORSEVERITY, String.valueOf(BusinessRuleException.SEVERITY_ERROR).trim());
				res.setStatus(HttpStatus.SC_OK);
				res.getWriter().write(Constantes.NODO_XML_error_VACIO);
				res.setCharacterEncoding(Constantes.ENCODING_UTF8);
				res.setContentType(Constantes.CONTENT_TYPE_TEXT_XML);
			} else if (auxiliar_error.equals(Constantes.CADENA_105)) {
				res.addHeader(CAC_ERRORCODE,"Mensaje del sistema");
				if (itex.getTargetException() != null && itex.getTargetException().getCause() != null && itex.getTargetException().getCause().getLocalizedMessage() != null) {
					if (!itex.getTargetException().getCause().getLocalizedMessage().equals(Constantes.CADENA_VACIA)) {
						res.addHeader(CAC_ERRORMSG,itex.getTargetException().getCause().getLocalizedMessage().trim());
						msnerror=itex.getTargetException().getCause().getLocalizedMessage().trim();
					} else {}
				} else {
					if (itex.getLocalizedMessage() != null) {
						res.addHeader(CAC_ERRORMSG,"No existe la causa de la excepcion. "+itex.getLocalizedMessage().trim());
					} else {
						res.addHeader(CAC_ERRORMSG,"No existe la causa de la excepcion. "+itex.getTargetException().getMessage().trim());
					}
				}
				res.addHeader(CAC_ERRORDETAIL,Constantes.CADENA_VACIA);
				res.addHeader(CAC_ERRORSEVERITY, String.valueOf(BusinessRuleException.SEVERITY_WARNING).trim());
				res.setStatus(HttpStatus.SC_OK);
				if (msnerror!=null){
					res.getWriter().write(Constantes.NODO_XML_error_APERTURA + msnerror +Constantes.NODO_XML_error_CIERRE);
				}else{
					res.getWriter().write(Constantes.NODO_XML_error_VACIO);
				}
				res.setCharacterEncoding(Constantes.ENCODING_UTF8);
				res.setContentType(Constantes.CONTENT_TYPE_TEXT_XML);
			} else if (auxiliar_error.equals(Constantes.CADENA_403)) {
				res.setStatus(res.SC_FORBIDDEN);
			} else if (auxiliar_error.equals(Constantes.CADENA_401)) {
				res.setStatus(res.SC_UNAUTHORIZED);
			} else if (auxiliar_error.equals(Constantes.CADENA_107)) {
				res.addHeader(CAC_ERRORCODE,"Mensaje del sistema");
				if (itex.getTargetException() != null && itex.getTargetException().getCause() != null && itex.getTargetException().getCause().getLocalizedMessage() != null) {
					res.addHeader(CAC_ERRORMSG,itex.getTargetException().getCause().getLocalizedMessage().trim());
					res.addHeader(CAC_ERRORDETAIL,Constantes.CADENA_VACIA);
				} else {
					try {
						//String error=(((Exception)((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap()).getMessage().trim());
						//String errorDecode = new java.lang.String(error.getBytes(Constantes.ENCODING_ISO_8859_1), Constantes.ENCODING_UTF8);
						//String errorHTML= StringEscapeUtils.escapeHtml(new java.lang.String(error.getBytes(Constantes.ENCODING_ISO_8859_1), Constantes.ENCODING_UTF8) );
						//Intentamos obtener el error del objeto que nos viene
						res.addHeader(CAC_ERRORMSG, convertError(((Exception)((NativeJavaObject)((JavaScriptException)ex.getTargetException()).getValue()).unwrap()).getMessage().trim()));
						res.addHeader(CAC_ERRORDETAIL,Constantes.CADENA_VACIA);
					} catch (Exception e) {
						//res.addHeader(CAC_ERRORMSG,"Error en un javascript se esta realizando un throw de una Exception de javascript.");
						Throwable excause;
						try{
							excause = (Exception)((NativeJavaObject)((JavaScriptException)ex.getCause()).getValue()).unwrap();
						} catch (Exception auxex){
							excause = ex.getCause();
						}
						StackTraceElement[] traza = ex.getStackTrace();
						StackTraceElement[] trazacause = excause.getStackTrace();
						if (log.isErrorEnabled()){
							String errordetail = "Se ha capturado un error no controlado tipo 107: " + ex.toString() + Constantes.SALTO_LINEA;
							for (int i=0; i<traza.length; i++){
								errordetail = Constantes.DOBLE_ESPACIO_BLANCO + errordetail + traza[i] + Constantes.SALTO_LINEA;
							}
							errordetail = errordetail + "Caused by: " + excause.toString() + Constantes.SALTO_LINEA;
							for (int i=0; i<trazacause.length; i++){
								errordetail = Constantes.DOBLE_ESPACIO_BLANCO + errordetail + trazacause[i] + Constantes.SALTO_LINEA;
							}
							log.error(errordetail);
						}
						res.addHeader(CAC_ERRORMSG,"Se ha capturado un error no controlado tipo 107");
						res.addHeader(CAC_ERRORDETAIL, ex.toString() + " Caused by: " + excause.toString());
					}
				}
				res.addHeader(CAC_ERRORSEVERITY, String.valueOf(BusinessRuleException.SEVERITY_ERROR).trim());
				res.setStatus(HttpStatus.SC_OK);
				res.getWriter().write(Constantes.NODO_XML_error_VACIO);
				res.setCharacterEncoding(Constantes.ENCODING_UTF8);
				res.setContentType(Constantes.CONTENT_TYPE_TEXT_XML);
			}
			//res.setStatus(HttpStatus.SC_OK);
			// el contenido del response debe ser not null
			// puede contener lo que queramos (ej, campos de la validacion que fallaron)
		}catch (Exception ex) {
			res.addHeader(CAC_ERRORCODE,"Mensaje del sistema");
			res.addHeader(CAC_ERRORMSG,"Se ha capturado una error no controlado del sistema");
			res.addHeader(CAC_ERRORDETAIL,Constantes.APERTURA_CORCHETE+ex.getClass().getName().trim()+Constantes.CIERRE_CORCHETE);// +ex.getCause().getLocalizedMessage()
			res.addHeader(CAC_ERRORSEVERITY, String.valueOf(BusinessRuleException.SEVERITY_ERROR).trim());
			res.setStatus( HttpStatus.SC_OK );
			res.getWriter().write(Constantes.NODO_XML_error_VACIO);
			res.setCharacterEncoding(Constantes.ENCODING_UTF8);
			res.setContentType(Constantes.CONTENT_TYPE_TEXT_XML);
			if (log.isErrorEnabled()){
				log.error("  ## Exception : " + ex.getClass().getName().trim() );
			}
		}
		return mv;
	}



	public static void main(String[] args) {
		String col= "pepe\n\n";
		System.out.println(col.trim());
		System.out.println(col);
		System.out.println("---");
	}



	private IllegalArgumentException getIllegalArgumentExceptionPropia(Throwable exception){
		if(exception != null ){
			if(exception instanceof IllegalArgumentException && exception.getMessage().startsWith("generadoModel:")){
				return (IllegalArgumentException)exception;
			}else if (exception.getCause() != null){
				return getIllegalArgumentExceptionPropia(exception.getCause());
			}
		}
		return null;
	}



	/**
	 * @author RGP
	 * @since Ni se sabe
	 * @modified 15 Marzo 2012 by API vamos a evitar la conversión para poder generar los mensajes de error con la codificación correctamente 
	 */
	private String convertError(String error) throws Exception {
		//String errorHTML= StringEscapeUtils.escapeHtml(new java.lang.String(error.getBytes(Constantes.ENCODING_ISO_8859_1), Constantes.ENCODING_UTF8) );
		/*API 15 MArzo 2012 Esperamos con esto poder desplegar correctamente bajo linux evitando la conversion desde ISO_8859_1
		return HtmlUtils.htmlEscapeDecimal(new java.lang.String(error.getBytes(Constantes.ENCODING_ISO_8859_1), Constantes.ENCODING_UTF8) );*/
		return HtmlUtils.htmlEscapeDecimal(  new String( error.getBytes( Constantes.ENCODING_UTF8 ), Constantes.ENCODING_UTF8 )  );
	}
}
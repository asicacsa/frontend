package es.di.cac.ticketing.application;

import java.io.IOException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternResolver;

import es.di.cac.ticketing.application.remoting.RequestContext;


public class ScriptObjectModel implements ResourceLoader, ResourcePatternResolver {

	ApplicationContext appCtx;
	
	public ScriptObjectModel(ApplicationContext appCtx){
		this.appCtx = appCtx;
	}
	
	public HttpServletRequest getRequest(){
		return RequestContext.getHttpServletRequest();

	}

	public HttpSession getSession(){
		return RequestContext.getHttpSession();
	}
	
	public HttpSession getSession(boolean create){
		return RequestContext.getHttpSession(create);
	}
	
	public Cookie[] getCookies(){
		return RequestContext.getHttpServletRequest().getCookies();
	}
	
	public Cookie getCookieByName(String name){
		return getCookie(RequestContext.getHttpServletRequest(),name);
	}
	
	public Cookie createCookie(String name, String value){
		return new Cookie(name,value);
	}
	
	public void addCookie(Cookie cookie){
		RequestContext.getHttpServletResponse().addCookie(cookie);
	}
	
	public ListableBeanFactory getBeans(){
		return appCtx;
	}
	
	public ApplicationContext getApplicationContext(){
		return appCtx;
	}
	
	/*
	public ListableBeanFactory getScripts(){
		return getScriptBeanFactory();
	}
	
	public ListableBeanFactory getScriptBeanFactory(){
		return appCtx;
	}*/
	
	public MessageSource getMessageSource(){
		return appCtx;
	}
	
	public Resource getResource(String path) {
		return appCtx.getResource(path);
	}
	
	public Resource[] getResources(String pattern) throws IOException {
		return appCtx.getResources(pattern);
	}
	
	protected Cookie getCookie(HttpServletRequest request, String name) {
		Cookie[] cookies = request.getCookies();
		if(cookies==null) return null;
		for(int i=0; i<cookies.length; i++){
			if(cookies[i].getName().equals(name)) 
				return cookies[i];
		}
		return null;
	}

	public ClassLoader getClassLoader() {
		// TODO Auto-generated method stub
		return null;
	}
	
}

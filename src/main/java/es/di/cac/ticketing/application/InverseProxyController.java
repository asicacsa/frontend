package es.di.cac.ticketing.application;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

public class InverseProxyController implements Controller {

	Properties mappings;

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		if ( mappings == null ) {
			//Status 404
			response.sendError(HttpStatus.SC_NOT_FOUND, "InverseProxyController mal configurado");
		}

		String url = mappings.getProperty( request.getServletPath() );
		if (url == null) {
			response.sendError(HttpStatus.SC_NOT_FOUND, "Mapping no existe en InverseProxyController");
		}

		HttpClient client = new HttpClient();
		
		PostMethod method = new PostMethod( url );
		method.setParameter( "http.useragent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)" );
		// copia los parametros de la peticion a la nueva peticion
		String paramName;
		
		for ( Enumeration enu = request.getParameterNames(); enu.hasMoreElements(); ) {
			paramName = (String) enu.nextElement();
			method.addParameter(  paramName, request.getParameter( paramName )  );
		}
		
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;

		try {
			int statusCode = client.executeMethod( method );

			if ( statusCode != HttpStatus.SC_OK ) {
				System.err.println("Method failed: " + method.getStatusLine());
			}

			// copia los header de la respuesta al response real
			Header[] headers = method.getResponseHeaders();
			for (int i = 0; i < headers.length; i++) {
				response.addHeader(headers[i].getName(), headers[i].getValue());
			}

			bis = new BufferedInputStream(method.getResponseBodyAsStream());
			bos = new BufferedOutputStream(response.getOutputStream());

			byte[] buff = new byte[2048];
			int bytesRead;

			// Simple read/write loop.
			while (   -1 != (  bytesRead = bis.read( buff, 0, buff.length )  )   ) {
				bos.write(buff, 0, bytesRead);
			}

		} catch (HttpException e) {
			System.err.println("Fatal protocol violation: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Fatal transport error: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (bis != null)
				bis.close();
			if (bos != null)
				bos.close();
			method.releaseConnection();
		}
		return null;
	}

	public void setMappings(Properties mappings) {
		this.mappings = mappings;
	}

}
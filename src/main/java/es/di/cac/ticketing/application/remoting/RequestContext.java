package es.di.cac.ticketing.application.remoting;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Luke Hubbard <luke@codegent.com>
 */
public class RequestContext {
	
	private static final Log log = LogFactory.getLog(RequestContext.class);

	static private ThreadLocal httpRequestLocal = new ThreadLocal();

	static private ThreadLocal httpResponseLocal = new ThreadLocal();
	
	static private ThreadLocal requestMsgLocal = new ThreadLocal();

	static private ThreadLocal hibernateSessionLocal = new ThreadLocal();

	private RequestContext() {
		// private constructor
	}

	/**
	 * 
	 * @param req
	 * the servlet request object
	 * 
	 * @see #getHttpServletRequest()
	 *  
	 */
	static public void setHttpServletRequest(HttpServletRequest req) {
		httpRequestLocal.set(req);
	}

	/**
	 * 
	 * @param resp
	 * the servlet response object
	 * 
	 * @see #getHttpServletResponse()
	 *  
	 */
	static public void setHttpServletResponse(HttpServletResponse resp) {
		httpResponseLocal.set(resp);
	}
		
	/**
	 * 
	 * clears the request context data
	 *  
	 */
	static public void clear() {
		httpRequestLocal.set(null);
		httpResponseLocal.set(null);
		requestMsgLocal.set(null);
		hibernateSessionLocal.set(null);
	}

	/**
	 * 
	 * 
	 * @return may return null
	 * 
	 * @see #setHttpServletRequest(HttpServletRequest)
	 *  
	 */
	static public HttpServletRequest getHttpServletRequest() {
		HttpServletRequest req = (HttpServletRequest) httpRequestLocal.get();
		return req;
	}
	

	/**
	 * 
	 * 
	 * @return may return null
	 * 
	 * @see #setHttpServletResponse(HttpServletResponse)
	 *  
	 */
	static public HttpServletResponse getHttpServletResponse() {
		HttpServletResponse resp = (HttpServletResponse) httpResponseLocal.get();
		return resp;
	}

	/**
	 * 
	 * @return a non-null value
	 * 
	 * @see #getHttpServletRequest
	 *  
	 */
	
	static public HttpSession getHttpSession() {
		HttpServletRequest req = getHttpServletRequest();
		if (req == null) {
			throw new IllegalStateException("HttpServletRequest is null");
		}
		return req.getSession();
	}
	
	static public HttpSession getHttpSession(boolean create) {
		HttpServletRequest req = getHttpServletRequest();
		if (req == null) {
			throw new IllegalStateException("HttpServletRequest is null");
		}
		return req.getSession(true);
	}
	
	static void debug(){
		log.debug("Checking Request Context");
		log.debug(httpRequestLocal.get());
		log.debug(httpResponseLocal.get());
		log.debug(requestMsgLocal.get());
		log.debug(hibernateSessionLocal.get());
	}

}
package es.di.cac.ticketing.application.javascript.support;

import java.util.Properties;


public interface Transaction {

	public String getTransactionManagerName();
	public Properties getTransactionAttributes();
	
}

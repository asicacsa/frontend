package es.di.cac.ticketing.application.javascript;

import org.mozilla.javascript.ScriptableObject;

public class JavaScriptScopeThreadLocal {

	private JavaScriptScopeThreadLocal() {
		// private constructor
	}
	
	static private ThreadLocal localScope = new ThreadLocal();
	static private ThreadLocal localMeta = new ThreadLocal();
	
	static public void setScope(ScriptableObject scope) {
		localScope.set(scope);
	}
	
	static public ScriptableObject getScope() {
		return (ScriptableObject) localScope.get();
	}
	
	static public void setMeta(JavaScriptMeta meta) {
		localMeta.set(meta);
	}
	
	static public JavaScriptMeta getMeta() {
		return (JavaScriptMeta) localMeta.get();
	}
	
}

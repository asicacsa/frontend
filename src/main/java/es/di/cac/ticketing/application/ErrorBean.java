package es.di.cac.ticketing.application;

public interface ErrorBean {

	public abstract Exception getException();

}

package es.di.cac.ticketing.application;

public class ScriptErrorBean implements ErrorBean {

	private Exception exception;
	
	public ScriptErrorBean(Exception ex){
		this.exception = ex;
	}

	public Exception getException() {
		return exception;
	}
	
	public void setException(Exception exception) {
		this.exception = exception;
	}

}

package es.di.cac.ticketing.application;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.acegisecurity.context.Context;
import net.sf.acegisecurity.context.security.SecureContextUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import es.cac.colossus.frontend.utils.Tools;
import es.di.cac.acegisecurity.ApplicationUserDetails;
import es.di.cac.acegisecurity.context.security.CacSecureContextImpl;
import utiles.Constantes;

// Comentario: JRR Sadim
// Este controlador no está anotado porque se configura en el web.xml para responder exclusivamente a una petición de login
// La comprobación de las credenciales del login se hace en la clase CacSOAAuthenticationProvider
// En esa clase de comprueba si el usuario está autenticado invocando el método authenticate
// Este método comprueba si el usuario está autenticado, si está en caché, y si no es así realiza una llamada a base de datos con los parámetros de la request con el método getUserFromBackend()
// De tal forma que una vez llegado a este controlador el usuario ya debe de estar autenticado
// Si está autenticado lo que hace este controlador es añadir una serie de parámetros a la sesión

public class LoginController implements Controller {

	private static final Log log = LogFactory.getLog(LoginController.class);
	
	public ModelAndView handleRequest(HttpServletRequest arg0, HttpServletResponse arg1) throws Exception {
				
		Context aux = SecureContextUtils.getSecureContext();
		/* API 15 Octubre 2010 Esta cadena no se utilizaba para nada String auxiliar_parameter = null;*/
		String appDefecto = null;

		if (aux != null && aux instanceof CacSecureContextImpl) {
			log.error("El contexto : "+((ApplicationUserDetails)((CacSecureContextImpl)aux).getAuthentication().getPrincipal()).toString());
			if (log.isDebugEnabled()) {
				log.debug("El contexto : "+aux.toString());
			}
			Object userDetails = ((CacSecureContextImpl)aux).getAuthentication().getPrincipal();
			if (userDetails != null && userDetails instanceof ApplicationUserDetails) {
				if (log.isDebugEnabled()) {
					log.debug("El contexto : "+userDetails.toString());
				}
				arg0.getSession().setAttribute("idUsuario", ((ApplicationUserDetails)userDetails).getIdUsuario().toString());
				if (log.isDebugEnabled()) {
					log.debug("El contexto : "+((ApplicationUserDetails)userDetails).getIdUsuario().toString());
				}
				arg0.getSession().setAttribute(Constantes.S_nombre, ((ApplicationUserDetails)userDetails).getNombre().toString());
				if (log.isDebugEnabled()) {
					log.debug("El contexto : "+((ApplicationUserDetails)userDetails).getNombre().toString());
				}
				arg0.getSession().setAttribute(Constantes.S_apellidos, ((ApplicationUserDetails)userDetails).getApellidos().toString());
				if (log.isDebugEnabled()) {
					log.debug("El contexto : "+((ApplicationUserDetails)userDetails).getApellidos().toString());
				}
				if (((ApplicationUserDetails)userDetails).getTaquilla() != null) {
					arg0.getSession().setAttribute(Constantes.S_taquilla, ((ApplicationUserDetails)userDetails).getTaquilla().toString());
					if (log.isDebugEnabled()) {
						log.debug("El contexto : "+((ApplicationUserDetails)userDetails).getTaquilla().toString());
					}
				} else {
					if (log.isInfoEnabled()) {
						log.debug("No existe taquilla en el contexto.");
					}
				}
				if (((ApplicationUserDetails)userDetails).getIdTaquilla() != null) {
					arg0.getSession().setAttribute(Constantes.S_idtaquilla, ((ApplicationUserDetails)userDetails).getIdTaquilla().toString());
					if (log.isDebugEnabled()) {
						log.debug("El contexto : "+((ApplicationUserDetails)userDetails).getIdTaquilla().toString());
					}
				} else {
					if (log.isInfoEnabled()) {
						log.debug("No existe idtaquilla en el contexto.");
					}
				}
				if (((ApplicationUserDetails)userDetails).getIdCanal() != null) {
					arg0.getSession().setAttribute(Constantes.S_idcanal, ((ApplicationUserDetails)userDetails).getIdCanal().toString());
					if (log.isDebugEnabled()) {
						log.debug("El contexto : "+((ApplicationUserDetails)userDetails).getIdCanal().toString());
					}
				} else {
					if (log.isInfoEnabled()) {
						log.debug("No existe Canal en el contexto.");
					}
				}
				if (((ApplicationUserDetails)userDetails).getNombreCanal() != null) {
					arg0.getSession().setAttribute(Constantes.S_nombreCanal, ((ApplicationUserDetails)userDetails).getNombreCanal().toString());					
					if (log.isDebugEnabled()) {
						log.debug("El contexto : "+((ApplicationUserDetails)userDetails).getNombreCanal().toString());
					}
				} else {
					if (log.isInfoEnabled()) {
						log.debug("No existe Canal en el contexto.");
					}
				}
				if (((ApplicationUserDetails)userDetails).getIdTipocanal() != null) {
					arg0.getSession().setAttribute("idTipocanal", ((ApplicationUserDetails)userDetails).getIdTipocanal().toString());
					if (log.isDebugEnabled()) {
						log.debug("El contexto : "+((ApplicationUserDetails)userDetails).getIdTipocanal().toString());
					}
				} else {
					if (log.isInfoEnabled()) {
						log.debug("No existe idTipocanal en el contexto.");
					}
				}
				
				if (((ApplicationUserDetails)userDetails).getNombreTipocanal() != null) {
					arg0.getSession().setAttribute("nombreTipocanal", ((ApplicationUserDetails)userDetails).getNombreTipocanal().toString());
					if (log.isDebugEnabled()) {
						log.debug("El contexto : "+((ApplicationUserDetails)userDetails).getNombreTipocanal().toString());
					}
				} else {
					if (log.isInfoEnabled()) {
						log.debug("No existe nombreTipocanal en el contexto.");
					}
				}
				
				if (((ApplicationUserDetails)userDetails).getUsername() != null) {
					arg0.getSession().setAttribute("login", ((ApplicationUserDetails)userDetails).getUsername().toString());
					if (log.isDebugEnabled()) {
						log.debug("El contexto : "+((ApplicationUserDetails)userDetails).getUsername().toString());
					}
				} else {
					if (log.isInfoEnabled()) {
						log.debug("No existe Username en el contexto.");
					}
				}
				
				if (((ApplicationUserDetails)userDetails).getAppdefecto() != null) {
					appDefecto=((ApplicationUserDetails)userDetails).getAppdefecto().toString();
				}else {
					if (log.isInfoEnabled()) {
						log.debug("No existe appDfecto en el contexto.");
					}
				}
				
				
			}
		} else {
			if (log.isDebugEnabled()) {
				log.debug("El contexto es nulo o no es del tipo : "+CacSecureContextImpl.class.getName());
			}
		}		
		
		Tools.setCommonObjectsInSession(arg0);
		
		arg1.getWriter().print(appDefecto);
		//Aki hay que trabjar con el SecurityContextHolder mirar
		arg1.getWriter().flush();
		arg1.getWriter().close();
		return null;
	}

}

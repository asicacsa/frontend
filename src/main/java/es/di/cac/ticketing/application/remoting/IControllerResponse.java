package es.di.cac.ticketing.application.remoting;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

public interface IControllerResponse {

	public abstract ModelAndView manageResponse(HttpServletRequest req, HttpServletResponse res, String nombreOrigen, Object respuesta ) throws Exception;

}
package es.di.cac.ticketing.application.util;

/**
 * <p>Title: Acceso a constantes de la aplicación</p>
 * <p>Description: Gestiona las propiedades del proyecto. Permitirá cambiarlas en tiempo de ejecución sin tener que reiniciar el servicio</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Dimensión Informática</p>
 * @author Julio Maiques
 * @version 1.0
 */
import java.util.Properties;
import java.util.ResourceBundle;
import java.io.FileInputStream;;

public class Constantes {
	
	public Constantes () {}
	
	public static String getString( String psVariableABuscar, ResourceBundle rb ) {
		return rb.getString( psVariableABuscar );
	}
	
	public static String getString(String psVariableABuscar, String pathFichero) throws Exception {
		Properties properties = new Properties();
		properties.load(  new FileInputStream( pathFichero )  );
		return properties.getProperty( psVariableABuscar ); 
	}

}
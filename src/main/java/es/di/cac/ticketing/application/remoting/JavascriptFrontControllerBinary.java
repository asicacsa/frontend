package es.di.cac.ticketing.application.remoting;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import utiles.Constantes;
//import es.di.cac.ticketing.application.ScriptBeanFactory;
//import es.di.framework.service.controller.IServiceController;
//GGL TODO NO SE UTILIZA. ELIMINAR.
/*
public class JavascriptFrontControllerBinary implements Controller, ApplicationContextAware {
	
	private static final Log log = LogFactory.getLog(JavascriptFrontControllerBinary.class);

	public static final String REQUEST_SERVICE = "__request_service";
	public static final String DEFAULT_FILE = "default.binary.js";
	public static final String JS_EXTENSION = ".binary.js";

	protected Map services = new HashMap();
	protected ApplicationContext appCtx = null;
	protected ScriptBeanFactory scriptBeanFactory = null;
	private IControllerResponse controllerResponse = null;
	private IServiceController controller = null;

	public void setApplicationContext(ApplicationContext appCtx) {
		this.appCtx = appCtx;
	}

	public void setScriptBeanFactory(ScriptBeanFactory scriptBeanFactory){
		this.scriptBeanFactory = scriptBeanFactory;
	}

	public void setController(IServiceController controller) {
		this.controller = controller;
	}

	public void setControllerResponse(IControllerResponse controllerResponse) {
		this.controllerResponse = controllerResponse;
	}


	public void initializeRequestContext() {}


	public ModelAndView handleRequest(HttpServletRequest req, HttpServletResponse res) throws Exception {
		// TODO: get serviceName from service param (CHANGE THIS!) right now this is hardcoded!
		String className = req.getRequestURI().substring(
			req.getRequestURI().lastIndexOf(Constantes.BARRA) + 1,
			req.getRequestURI().lastIndexOf(Constantes.PUNTO)
		);
		String beanName = className + JS_EXTENSION;
		Object service = null;
		if( scriptBeanFactory.containsBean(beanName) )
			service = scriptBeanFactory.getBean(beanName);
		else
			service = scriptBeanFactory.getBean(DEFAULT_FILE);
		log.debug(service);
		Method[] mts = service.getClass().getMethods();
		if(log.isDebugEnabled())
			for (int i = 0; i < mts.length; i++)
				log.debug(mts[i]);
		ModelAndView result = null;
		Object xmlEntrada = null;
		String serviceName = req.getParameter("service");
		if ( serviceName == null ) {
			// invoke default Controller handler
			Method method = service.getClass().getMethod( "handle", new Class[]{HttpServletRequest.class, HttpServletResponse.class} );
			xmlEntrada = method.invoke(service, new Object[]{req, res});
		} else {
			if( serviceName.length()==0 )
				throw new Exception("Invalid serviceName");
			Method method = service.getClass().getMethod( serviceName, new Class[]{HttpServletRequest.class, HttpServletResponse.class} );
			xmlEntrada = method.invoke( service, new Object[]{req, res} );
		}
		if (!(xmlEntrada instanceof ModelAndView)) {
			//Llamamos al servicio (binary , soa, etc..)
			String servicioALlamar = (String)req.getAttribute( REQUEST_SERVICE );
			Object resultServicio = null;
			if (servicioALlamar!=null)
				resultServicio = controller.process( servicioALlamar, (String)xmlEntrada  );
			else 
				log.warn("NO SE LLAMÓ A NINGUN SERVICIO AL INVOCAR A " + className);
			result = controllerResponse.manageResponse( req, res, className, resultServicio );
		} else
			result = (ModelAndView)xmlEntrada;
		return result;
	}

}*/
package es.di.cac.ticketing.application.gzip;

/**
 *
 *		<filter>
 *			<filter-name>Compress</filter-name>
 *			<filter-class>
 *				es.di.cac.ticketing.application.gzip.GZIPFilter
 *			</filter-class>
 *		</filter>
 *
 * 		<filter-mapping>
 *			<filter-name>Compress</filter-name>
 *			<url-pattern>*.jsp</url-pattern>
 *		</filter-mapping>
 *		<filter-mapping>
 *			<filter-name>Compress</filter-name>
 *			<url-pattern>*.html</url-pattern>
 *		</filter-mapping>
 */

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GZIPFilter implements Filter {
	
	public void doFilter(ServletRequest paramServletRequest, ServletResponse paramServletResponse, FilterChain paramFilterChain) throws IOException, ServletException {
		if ( paramServletRequest instanceof HttpServletRequest ) {
			HttpServletRequest localHttpServletRequest = (HttpServletRequest)paramServletRequest;
			HttpServletResponse localHttpServletResponse = (HttpServletResponse)paramServletResponse;
			String str = localHttpServletRequest.getHeader("accept-encoding");
			
			if (  ( str != null ) && ( str.indexOf("gzip") != -1 )  ) {
				//System.out.println("GZIP supported, compressing.");
				GZIPResponseWrapper localGZIPResponseWrapper = new GZIPResponseWrapper(localHttpServletResponse);
				paramFilterChain.doFilter(paramServletRequest, localGZIPResponseWrapper);
				localGZIPResponseWrapper.finishResponse();
				return;
			}
			paramFilterChain.doFilter(paramServletRequest, paramServletResponse);
		}
	}
	
	public void init(FilterConfig paramFilterConfig) {}
	
	public void destroy() {}
	
}

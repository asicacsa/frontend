package es.di.cac.ticketing.application.remoting; 

import java.io.Reader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import es.di.framework.service.controller.ISOAServiceController;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import utiles.Constantes;

public class pinpadFunctScriptController extends MultiActionController {

	private static final String PREFIJO = "comun/";
	private static final String SUFIJO = ".jsp";

	/**
	 * 
	 * @author RGP
	 * Controler para obtener los datos del Pinpad
	 */
	
	public ModelAndView obtenerInformacionPinPad(HttpServletRequest req, HttpServletResponse res) throws Exception {

		ISOAServiceController services = (ISOAServiceController) this.getApplicationContext().getBean("inetServiceSOA");
		Properties config = new Properties();
		String datos = Constantes.CADENA_VACIA;
		String idtaquilla = req.getParameter(Constantes.S_taquilla);
		String ipRequest= req.getRemoteAddr();
		if ((idtaquilla==null) || (idtaquilla.equals(Constantes.CADENA_VACIA))){
			return devolverVistaError("Falta el parametro identificador de taquilla.<br/>", "ERROR !");
		}
		if ((ipRequest==null) || (ipRequest.equals(Constantes.CADENA_VACIA) )){
			return devolverVistaError("Falta el parametro ip de taquilla.<br/>", "ERROR !");
		}
		try {
			datos = services.process("/obtenerInformacionPinPad.pinpad", "<servicio><parametro><taquilla><idtaquilla>" + idtaquilla + "</idtaquilla><dirip>" + ipRequest +"</dirip></taquilla></parametro></servicio>");
		} catch (Exception e) {
			// Le mostramos el error al usuario
			return devolverVistaError("Se ha producido un error al obtener los datos del PinPad.<br/>" + e.getCause().getMessage(), "ERROR !");
		}
		//Esto no me mola nada de nada...
		datos=datos.replace(Constantes.CARACTER_APERTURA_CORCHETE, Constantes.CARACTER_MENOR);
		datos=datos.replace(Constantes.CARACTER_CIERRE_CORCHETE, Constantes.CARACTER_MAYOR);
		Reader r = (Reader) new StringReader(datos);
		//Creamos una fuente de entrada que utiliza el reader anterior para leer
		InputSource g = new InputSource(r);
		//Indicamos al constructor de documentos que debe generar el xml a partir
		//  de la fuente de datos anterior.
		//Necesitamos un constructor de constructores de documentos
		DocumentBuilderFactory dcf = DocumentBuilderFactory.newInstance();
		//Del constructor de constructores obtenemos un constructor
		DocumentBuilder db = dcf.newDocumentBuilder();
		Document xmlDoc = db.parse(g);
		String connstring = xmlDoc.getElementsByTagName("connstring").item(0).getFirstChild().getNodeValue();
		String commerce = xmlDoc.getElementsByTagName("commerce").item(0).getFirstChild().getNodeValue();
		String terminal = xmlDoc.getElementsByTagName("terminal").item(0).getFirstChild().getNodeValue();
		String key = xmlDoc.getElementsByTagName(Constantes.S_key).item(0).getFirstChild().getNodeValue();
		String libversion = xmlDoc.getElementsByTagName("libversion").item(0).getFirstChild().getNodeValue();
		config.setProperty("connstring", connstring);
		config.setProperty("commerce", commerce);
		config.setProperty("terminal", terminal);
		config.setProperty(Constantes.S_key, key);
		config.setProperty("libversion", libversion);
		return new ModelAndView(PREFIJO + "pinpadfunct" + SUFIJO, "pinpadconfig", config);
	}



	private ModelAndView devolverVistaError(String mensajeError, String mensajeEncabezado) {
		HashMap map = new HashMap();
		map.put("mensaje", mensajeError);
		map.put("mensajeEncabezado", mensajeEncabezado);
		return new ModelAndView(PREFIJO + "error" + SUFIJO, map);
	}



	protected ModelAndView handleRequestInternal(HttpServletRequest request,HttpServletResponse response)throws Exception{
		try {
			return obtenerInformacionPinPad(request,response);
		} catch (Exception e) {
			// Le mostramos el error al usuario
			return devolverVistaError("Se ha producido un error al procesar la peticion del PinPad.<br/>" + e.getCause().getMessage(), "ERROR !");
		}
	}

}
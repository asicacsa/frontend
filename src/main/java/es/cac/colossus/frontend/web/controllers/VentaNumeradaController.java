package es.cac.colossus.frontend.web.controllers;

import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import es.cac.colossus.frontend.utils.AjaxException;
import es.cac.colossus.frontend.utils.Tools;

@Controller
public class VentaNumeradaController {

	@Autowired
	private MessageSource messageSource;

	@RequestMapping("venta/numerada/busquedas.do")
	public ModelAndView showBusquedasNumeradas(HttpServletRequest request) throws Exception {
				
		ModelAndView model = new ModelAndView();

		model.setViewName("app.venta.numerada.busquedas");

		model.addObject("title", messageSource.getMessage("venta.numerada_busquedas.title", null, null));

		model.addObject("menu", Tools.getUserMainMenu(request));
		
		model.addObject("tabventas_selector_temporada", Tools.callServiceXML(request, "obtenerListadoTemporadasProgramacionVentas.action?servicio=obtenerListadoTemporadasProgramacion&xml="));
		model.addObject("tabventas_selector_canales", Tools.callServiceXML(request, "obtenerListadoCanales.action?servicio=obtenerListadoCanales"));
		
		model.addObject("tablocalidades_selector_temporada", Tools.callServiceXML(request, "obtenerListadoTemporadasProgramacion.action?servicio=obtenerListadoTemporadasProgramacion"));
		
		model.addObject("tablocalidadesabonos_selector_areas", Tools.callServiceXML(request, "obtenerNombreAreas.action?servicio=obtenerNombreAreas"));

		model.addObject("tababonos_selector_temporada",Tools.callServiceXML(request, "obtenerListadoTemporadasProgramacionBusquedaAbonos.action?servicio=obtenerListadoTemporadasProgramacion&xml="));

		Tools.callServiceXML(request, "darDeBajaPrerreservaActual.action?servicio=darDeBajaPrerreservaActual");
		
		return model;
	}

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/numerada/list_espectaculos.do")
	public ResponseEntity<?> busquedaNumeradaListEspectaculos(@RequestParam(value = "idtemporada", required = false, defaultValue = "-1") String idtemporada, HttpServletRequest request) throws Exception {

	     String json="";
	       try {
	               json = Tools.callServiceJSON(request, "obtenerListadoContenidosPorTemporadaProgramacion.action?servicio=obtenerListadoContenidosPorTemporadaProgramacion&xml=<int>"+idtemporada+"</int>");
	        } catch (Exception ex) {
	               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}
	
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/numerada/list_tipos_abono_ventas.do")
	public ResponseEntity<?> busquedaNumeradaListTiposAbonoVentas(@RequestParam(value = "idtemporada", required = false, defaultValue = "-1") String idtemporada, HttpServletRequest request) throws Exception {

	     String json="";
	       try {
	               json = Tools.callServiceJSON(request, "obtenerListadoTiposAbonoPorTemporadaVentas.action?servicio=obtenerListadoTiposAbonoPorTemporada&xml=<int>"+idtemporada+"</int>");
	        } catch (Exception ex) {
	               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}	


	// ***************************************************************************************************
	@RequestMapping("ajax/venta/numerada/list_tipos_abono.do")
	public ResponseEntity<?> busquedaNumeradaListTiposAbono(@RequestParam(value = "idtemporada", required = false, defaultValue = "-1") String idtemporada, HttpServletRequest request) throws Exception {

	     String json="";
	       try {
	               json = Tools.callServiceJSON(request, "obtenerListadoTiposAbonoPorTemporada.action?servicio=obtenerListadoTiposAbonoPorTemporada&xml=<int>"+idtemporada+"</int>");
	        } catch (Exception ex) {
	               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}		
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/numerada/list_sesiones.do")
	public ResponseEntity<?> busquedaNumeradaListSesiones(@RequestParam(value = "idespectaculo_localidades", required = false, defaultValue = "") String idContenido, HttpServletRequest request) throws Exception {

	     String json="";
	       try {
	               json = Tools.callServiceJSON(request, "obtenerSesionesPorContenido.action?servicio=obtenerSesionesPorContenido&idContenido="+idContenido);
	        } catch (Exception ex) {
	               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}	
	
		
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/numerada/list_ventas.do")
	public ResponseEntity<?> busquedaNumeradaListVentas(@RequestParam(value = "fechaini", required = false, defaultValue = "") String fechaVentaIni,
									   @RequestParam(value = "fechafin", required = false, defaultValue = "") String fechaVentaFin,
									   @RequestParam(value = "anulada", required = false, defaultValue = "0") String anulada,
									   @RequestParam(value = "idtipoventa", required = false, defaultValue = "") String idTipoVenta,
									   @RequestParam(value = "facturada", required = false, defaultValue = "") String facturada,
									   @RequestParam(value = "refventadesde", required = false, defaultValue = "") String referenciaVentaIni,										
									   @RequestParam(value = "refventahasta", required = false, defaultValue = "") String referenciaVentaFin,
									   @RequestParam(value = "refentrada", required = false, defaultValue = "") String referenciaEntrada,
									   @RequestParam(value = "idcanal", required = false, defaultValue = "") String idCanalVenta, 
									   @RequestParam(value = "idtemporada", required = false, defaultValue = "") String idTemporada, 
									   @RequestParam(value = "idespectaculo", required = false, defaultValue = "") String idContenido, 
									   @RequestParam(value = "idtipoabono", required = false, defaultValue = "") String idTipoAbono, 
									   @RequestParam(value = "idcliente_ventas", required = false, defaultValue = "") String idCliente, 
									   @RequestParam(value = "nombreabonado", required = false, defaultValue = "") String nombreAbonado, 
									   @RequestParam(value = "numabonado", required = false, defaultValue = "") String numeroAbonado, 
									   @RequestParam(value = "numabono", required = false, defaultValue = "") String numeroAbono, 
									   @RequestParam(value = "dni", required = false, defaultValue = "") String dniAbonado, 									   
									   @RequestParam(value = "start", required = false, defaultValue = "0") String startRecord,
									   @RequestParam(value = "length", required = false, defaultValue = "") String maxLength,
									   HttpServletRequest request) throws Exception {
		
		// idtipoventa: vacio=todas, 3=localidades, 1=abonos
		// facturada: vacio=todas, 1=facturada, 0=no facturada

		String json = "";

		try {
			json=Tools.callServiceJSON(request, "buscarVentaNumerada.action?servicio=buscarVentaNumerada&xml="+URLEncoder.encode("<Buscarventanumeradaparam><fechaventaini>"+fechaVentaIni+"</fechaventaini><fechaventafin>"+fechaVentaFin+"</fechaventafin><idcanalventa>"+idCanalVenta+"</idcanalventa><referenciaentrada>"+referenciaEntrada+"</referenciaentrada><referenciaventaini>"+referenciaVentaIni+"</referenciaventaini><referenciaventafin>"+referenciaVentaFin+"</referenciaventafin><idtemporadaprogramacioncontenido>"+idTemporada+"</idtemporadaprogramacioncontenido><idcontenido>"+idContenido+"</idcontenido><dniabonado>"+dniAbonado+"</dniabonado><nombreabonado>"+nombreAbonado+"</nombreabonado><nroabonado>"+numeroAbonado+"</nroabonado><idcliente>"+idCliente+"</idcliente><anulada>"+anulada+"</anulada><idtipoabono>"+idTipoAbono+"</idtipoabono><numeroabono>"+numeroAbono+"</numeroabono><idtipoventa>"+idTipoVenta+"</idtipoventa><facturada>"+facturada+"</facturada><numeroprimerregistro>"+startRecord+"</numeroprimerregistro><maxResultados>"+maxLength+"</maxResultados></Buscarventanumeradaparam>","UTF-8"));
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
	}
	

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/numerada/list_abonos.do")
	public ResponseEntity<?> busquedaNumeradaListAbonos(@RequestParam(value = "nombreabonado", required = false, defaultValue = "") String nombreAbonado, 
			   							@RequestParam(value = "numabonado", required = false, defaultValue = "") String numeroAbonado, 
			   							@RequestParam(value = "numabono", required = false, defaultValue = "") String numeroAbono, 
			   							@RequestParam(value = "dni", required = false, defaultValue = "") String dniAbonado,
									    @RequestParam(value = "idcliente_abonos", required = false, defaultValue = "") String idCliente, 
									    @RequestParam(value = "bloqueado", required = false, defaultValue = "") String bloqueado,
									    @RequestParam(value = "refventa", required = false, defaultValue = "") String idVenta, 
									    @RequestParam(value = "idtemporada", required = false, defaultValue = "") String idTemporada, 
 									    @RequestParam(value = "idtipoabono", required = false, defaultValue = "") String idTipoAbono, 
 									    @RequestParam(value = "nombrearea", required = false, defaultValue = "") String nombreArea, 
 									    @RequestParam(value = "filadesde", required = false, defaultValue = "") String filaDesde, 
 									    @RequestParam(value = "filahasta", required = false, defaultValue = "") String filaHasta, 
									   @RequestParam(value = "start", required = false, defaultValue = "0") String startRecord,
									   @RequestParam(value = "length", required = false, defaultValue = "") String maxLength,
									   HttpServletRequest request) throws Exception {

		String json = "";

		try {
			json=Tools.callServiceJSON(request, "buscarAbonos.action?servicio=buscarAbonos&xml="+URLEncoder.encode("<Buscarabonoparam><numeroabono>"+numeroAbono+"</numeroabono><dniabonado>"+dniAbonado+"</dniabonado><idventa>"+idVenta+"</idventa><idcliente>"+idCliente+"</idcliente><nombreabonado>"+nombreAbonado+"</nombreabonado><idtipoabono>"+idTipoAbono+"</idtipoabono><bloqueado>"+bloqueado+"</bloqueado><nombrearea>"+nombreArea+"</nombrearea><nroabonado>"+numeroAbonado+"</nroabonado><idtemporadaprogramacion>"+idTemporada+"</idtemporadaprogramacion><filaini>"+filaDesde+"</filaini><filafin>"+filaHasta+"</filafin><numeroprimerregistro>"+startRecord+"</numeroprimerregistro><maxResultados>"+maxLength+"</maxResultados></Buscarabonoparam>","UTF-8"));			
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
	}
	
	
	// ***************************************************************************************************	
	@RequestMapping("ajax/venta/numerada/mostrar_dialogo_imprimir_entradas.do")
	public ResponseEntity<?> busquedaNumeradaMostrarDialogoImprimirEntradas(									   
									   @RequestParam(value = "xml", required = true, defaultValue = "") String xml, 
									   HttpServletRequest request) throws Exception {
		

		String json = "";

		try {
			json = Tools.callServiceJSON(request, "comprobarImprimirEntradas.action?servicio=comprobarImprimirEntradas&xml="+URLEncoder.encode(xml,"UTF-8"));
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}
				
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
	}

	
	
	// *****************************************************************************************************
	@RequestMapping("ajax/venta/numerada/show_cliente.do")
	public ModelAndView busquedaNumeradaShowCliente(
			@RequestParam(value = "id", required = false, defaultValue = "") String idCliente,
			HttpServletRequest request) throws Exception {
		
		ModelAndView model = new ModelAndView();
					
		String datos_cliente_xml=Tools.callServiceXML(request, "getEdicionAltaClientesFacturacion.action?servicio=obtenerClientePorId&xml=" + idCliente);
		model.addObject("editar_cliente", datos_cliente_xml);
		
		// Ponemos tambien el objeto en json
		JSONObject json = XML.toJSONObject(datos_cliente_xml);

		if (json.has("error")) throw new Exception(json.getString("error"));
		
		model.addObject("editar_cliente_json", json);
		
		model.addObject("editar_cliente_pais", Tools.callServiceXML(request, "obtenerListadoPaises.action"));
			
		model.addObject("editar_cliente_categorias_actividad", Tools.callServiceXML(request, "obtenerListadoActividades.action?servicio=obtenerListadoActividades"));
		model.addObject("editar_cliente_tipos_documento", Tools.callServiceXML(request, "obtenerListadoTiposDocumento.action?servicio=obtenerListadoTiposDocumento"));

		model.setViewName("app.venta.numerada.busquedas.clientes.edit_cliente");
		
		return model;
	}	
	
	
	// ***************************************************************************************************
	@RequestMapping(value = "ajax/venta/numerada/save_cliente.do", method = RequestMethod.POST)
	public ResponseEntity<?> busquedaNumeradaSaveCliente(
			@RequestParam(value = "cliente_xml", required = false, defaultValue = "") String xmlCliente,
			HttpServletRequest request) throws Exception {
		
		
		String xmlEnvio="<getEdicionAltaClientesPalau>"+xmlCliente+"</getEdicionAltaClientesPalau>";
		String xml_result = Tools.callServiceXML(request, "postEdicionAltaClientesFacturacion.action?_modo=&servicio=postEdicionAltaClientesPalau&xml="  + URLEncoder.encode(xmlEnvio, "UTF-8") );		
						
		if (xml_result.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	
	// ***************************************************************************************************	
	@RequestMapping("ajax/venta/numerada/imprimir_entradas.do")
	public ResponseEntity<?> busquedaNumeradaImprimirEntradas(									   
									   @RequestParam(value = "xml", required = true, defaultValue = "") String xml, 
									   HttpServletRequest request) throws Exception {
		

		String json = "";

		try {
			json = Tools.callServiceJSON(request, "imprimirEntradas.action?servicio=imprimirEntradas&xml="+URLEncoder.encode(xml,"UTF-8"));
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}
				
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
	}

	

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/numerada/regenerar_imprimir_entradas.do")
	public ResponseEntity<?> busquedaNumeradaRegenerarImprimirEntradas(@RequestParam(value = "xml", required = true, defaultValue = "") String xml, HttpServletRequest request) throws Exception {

		String json = "";

		try {
			json = Tools.callServiceJSON(request, "regenerarImprimirEntradas.action?servicio=regenerarImprimirEntradas&xml=" + URLEncoder.encode(xml, "UTF-8"));
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/numerada/imprimir_recibos.do")
	public ResponseEntity<?> busquedaNumeradaImprimirRecibos(@RequestParam(value = "id", required = true, defaultValue = "") String idVenta, HttpServletRequest request) throws Exception {

		String json = "";
		
		try {
			json = Tools.callServiceJSON(request, "imprimirRecibos.action?servicio=imprimirRecibos&xml=<list><idventa>"+idVenta+"</idventa></list>");
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/numerada/desbloquear_localidades.do")
	public ResponseEntity<?> busquedaNumeradaDesbloquearLocalidades(									   
									   @RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
									   HttpServletRequest request) throws Exception {
		

		String json = "";

		try {
			json = Tools.callServiceJSON(request, "desbloquearLocalidadesNumeradas.action?servicio=desbloquearLocalidadesNumeradas&xml="+URLEncoder.encode(xml,"UTF-8"));
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}
				
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
	}

	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/numerada/vender_localidades_bloqueadas.do")
	public ModelAndView busquedaNumeradaVenderLocalidadesBloqueadas(									   
									   @RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
									   HttpServletRequest request) throws Exception {
		
		ModelAndView model = new ModelAndView();
		
		model.addObject("localidades", Tools.callServiceJSON(request, "actualizarPrerreservaLocalidadesNumeradas.action?servicio=actualizarPrerreservaLocalidadesNumeradas&xml="+URLEncoder.encode(xml,"UTF-8")));
		model.setViewName("app.venta.numerada.venta.vender.lineas");
		
		return model;			
			
			
	}	
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/numerada/show_vender_venta.do")
	public ModelAndView showVenderVentaIndividual(
			HttpServletRequest request) throws Exception {
		System.out.println("AKKII");
		ModelAndView model = new ModelAndView();

		model.setViewName("app.venta.numerada.venta");
		model.addObject("ventareserva_formaspago", Tools.callServiceXML(request, "obtenerListadoFormasPagoPorCanal.action?servicio=obtenerListadoFormasPagoPorCanal&xml="+URLEncoder.encode("<canal><idcanal>2</idcanal></canal>","UTF-8")));
		model.addObject("ventareserva_selector_idiomas", Tools.callServiceXML(request, "obtenerListadoIdiomasFiltrado.action?servicio=obtenerListadoIdiomasFiltrado&selected=-1"));
		System.out.println("AKKII2");
		
		return model;
	}
	
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/numerada/list_localidades.do")
	public ResponseEntity<?> busquedaNumeradaListLocalidades(									   
									   @RequestParam(value = "idtemporada", required = false, defaultValue = "") String idTemporada, 
									   @RequestParam(value = "idespectaculo_localidades", required = false, defaultValue = "") String idEspectaculo,									   
									   @RequestParam(value = "idsesion", required = false, defaultValue = "") String idSesion, 									   
									   @RequestParam(value = "bloqueada", required = false, defaultValue = "") String bloqueada, 
									   @RequestParam(value = "nombrearea", required = false, defaultValue = "") String nombreArea,
									   @RequestParam(value = "filadesde", required = false, defaultValue = "") String filaIni,
									   @RequestParam(value = "filahasta", required = false, defaultValue = "") String filaFin,									   
									   @RequestParam(value = "impares", required = false, defaultValue = "") String impares,									   
									   @RequestParam(value = "observaciones", required = false, defaultValue = "") String observaciones,
									   @RequestParam(value = "start", required = false, defaultValue = "0") String startRecord,
									   HttpServletRequest request) throws Exception {
		
		// bloqueada: vacio=todas, 1=bloqueada, 0=no bloqueada

		String json = "";

		try {
			json = Tools.callServiceJSON(request, "buscarLocalidad.action?servicio=buscarLocalidad&xml="+URLEncoder.encode("<Buscarlocalidadparam><idtemporadaprogramacion>"+idTemporada+"</idtemporadaprogramacion><idespectaculo>"+idEspectaculo+"</idespectaculo><idsesion>"+idSesion+"</idsesion><bloqueada>"+bloqueada+"</bloqueada><nombrearea>"+nombreArea+"</nombrearea><filaini>"+filaIni+"</filaini><filafin>"+filaFin+"</filafin><impares>"+impares+"</impares><observaciones>"+observaciones+"</observaciones><numeroprimerregistro>"+startRecord+"</numeroprimerregistro></Buscarlocalidadparam>","UTF-8"));
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}
				
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
	}
	
	
	// *****************************************************************************************************
	@RequestMapping("ajax/venta/numerada/anular_venta.do")
	public ModelAndView busquedaNumeradaAnularVenta(@RequestParam(value = "id", required = false, defaultValue = "") String idVenta, HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();
		
		model.addObject("anularventa_selector_motivos", Tools.callServiceXML(request, "obtenerListadoMotivosModificacion.action?servicio=obtenerListadoMotivosModificacion"));
		model.addObject("anularventa_selector_formas_pago", Tools.callServiceXML(request, "obtenerListadoFormasPago.action?servicio=obtenerListadoFormasPago"));
		model.addObject("anularventa_datos_venta", Tools.callServiceXML(request, "obtenerImportesParcialesPorVenta.action?servicio=obtenerImportesParcialesPorVenta&xml=<int>"+idVenta+"</int>"));		

		model.setViewName("app.venta.numerada.busquedas.ventas.anular");

		return model;
	}
	
	// *****************************************************************************************************
		@RequestMapping("ajax/venta/numerada/modificar_linea_detalle.do")
		public ModelAndView busquedaNumeradaModificarLineaDetalle(
				@RequestParam(value = "id", required = false, defaultValue = "") String idVenta,				
				HttpServletRequest request) throws Exception {

			ModelAndView model = new ModelAndView();
			
			model.addObject("anularventa_selector_motivos", Tools.callServiceXML(request, "obtenerListadoMotivosModificacion.action?servicio=obtenerListadoMotivosModificacion"));
			model.addObject("anularventa_selector_formas_pago", Tools.callServiceXML(request, "obtenerListadoFormasPago.action?servicio=obtenerListadoFormasPago"));
			model.addObject("idVenta", idVenta);

			
			model.setViewName("app.venta.numerada.busquedas.ventas.anular.lineas");

			return model;
		}
	
	
	
	// *****************************************************************************************************
	@RequestMapping("ajax/venta/numerada/anular_linea_detalle.do")
	public ModelAndView busquedaNumeradaAnularLineaDetalle(
			@RequestParam(value = "id", required = false, defaultValue = "") String idVenta,
			@RequestParam(value = "importeDevolucion", required = false, defaultValue = "") String importeDevolucion,
			@RequestParam(value = "importeTotalVenta", required = false, defaultValue = "") String importeTotalVenta,
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();
		
		model.addObject("anularventa_selector_motivos", Tools.callServiceXML(request, "obtenerListadoMotivosModificacion.action?servicio=obtenerListadoMotivosModificacion"));
		model.addObject("anularventa_selector_formas_pago", Tools.callServiceXML(request, "obtenerListadoFormasPago.action?servicio=obtenerListadoFormasPago"));
		model.addObject("idVenta", idVenta);
		model.addObject("importeDevolucion", importeDevolucion);
		model.addObject("importeTotalVenta", importeTotalVenta);
		
		model.setViewName("app.venta.numerada.busquedas.ventas.anular.lineas");

		return model;
	}
	
	
		// *****************************************************************************************************
		@RequestMapping("ajax/venta/numerada/editarVenta/FormasPago.do")
		public ModelAndView busquedaNumeradaEditarVentaFormasPago(
				@RequestParam(value = "id", required = false, defaultValue = "") String idVenta,
				HttpServletRequest request) throws Exception {

			ModelAndView model = new ModelAndView();
			
			model.addObject("selector_motivos", Tools.callServiceXML(request, "obtenerListadoMotivosModificacion.action?servicio=obtenerListadoMotivosModificacion"));
			model.addObject("selector_formas_pago", Tools.callServiceXML(request, "obtenerListadoFormasPago.action?servicio=obtenerListadoFormasPago"));
			model.addObject("datos_venta",Tools.callServiceJSON(request,  "obtenerImportesParcialesPorVenta.action?servicio=obtenerImportesParcialesPorVenta&xml=<int>"+idVenta+"</int>"));

			
			model.setViewName("app.venta.numerada.busquedas.ventas.editar.formaspago");

			return model;
		}
	
	
		// *****************************************************************************************************
		@RequestMapping("ajax/venta/numerada/editarVenta/FormasPagoAnadir.do")
		public ModelAndView busquedaNumeradaEditarVentaFormasPagoAnadir(
				HttpServletRequest request) throws Exception {
			ModelAndView model = new ModelAndView();
			
			model.addObject("selector_formas_pago", Tools.callServiceXML(request, "obtenerListadoFormasPago.action?servicio=obtenerListadoFormasPago"));
					
			model.setViewName("app.venta.numerada.busquedas.ventas.anadir.formaspago");

			return model;	
			
			
		}
	
	
	
	
	// *****************************************************************************************************
	@RequestMapping("/ajax/venta/numerada/editar_venta.do")
	public ModelAndView busquedaNumeradaEditarVenta(@RequestParam(value = "id", required = false, defaultValue = "") String idVenta, HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();
		
		String editar_venta=Tools.callServiceXML(request, "obtenerVentaPorIdPalau.action?servicio=obtenerVentaPorIdPalau&xml="+URLEncoder.encode("<int>"+idVenta+"</int>", "UTF-8"));
		model.addObject("editar_venta", editar_venta);
		JSONObject json = XML.toJSONObject(editar_venta);
		if (json.has("error")) throw new Exception(json.getString("error"));
			model.addObject("editar_venta_json", json);
		
		model.setViewName("app.venta.numerada.busquedas.ventas.editar");

		return model;
	}	
	//********************************************************************************************************
	@RequestMapping("/ajax/venta/busqueda/show_carta_confirmacion_venta.do")
	public ModelAndView showCartaConfirmacionVenta(	
			@RequestParam(value = "id", required = false, defaultValue = "") String idVenta,
			HttpServletRequest request) throws Exception {
		
	
		ModelAndView model = new ModelAndView();
		model.addObject("idioma_carta", Tools.callServiceXML(request, "obtenerListadoIdiomas.action?servicio=obtenerListadoIdiomas"));
		model.addObject("idventa",idVenta);
		
		model.setViewName("app.venta.numerada.busquedas.editar_venta.carta_confirmacion");
		
		return model;
	}
	// *****************************************************************************************************
	@RequestMapping("/ajax/venta/busqueda/show_anuladas.do")
	public ModelAndView showAnuladasVenta(
			@RequestParam(value = "id", required = false, defaultValue = "") String idVenta,
			HttpServletRequest request) throws Exception {
		
		ModelAndView model = new ModelAndView();
		String datos="";
		datos = "<int>"+idVenta+"</int>";
		
		String xml_historico_anuladas =Tools.callServiceXML(request, "obtenerVentaPorIdAnuladaPalau.action?servicio=obtenerVentaPorIdAnuladaPalau&xml=" + datos);
		model.addObject("xml_historico_anuladas", xml_historico_anuladas);
		JSONObject json = XML.toJSONObject(xml_historico_anuladas);
		if (json.has("error")) throw new Exception(json.getString("error"));
			model.addObject("historico_anuladas_json", json);
		
		
		model.addObject("historico_anuladas", xml_historico_anuladas);
		model.setViewName("app.venta.numerada.busquedas.editar_venta.historico_anuladas");
		
		return model;
	}
	
	
	// ***************************************************************************************************
		@RequestMapping("ajax/venta/numerada/actualizarPagos.do")
		public ResponseEntity<?> busquedaNumeradaActualizarPagos(									   
										   @RequestParam(value = "xmlPagos", required = false, defaultValue = "") String xml, 
										   HttpServletRequest request) throws Exception {
			

			String json = "";

			try {
				json = Tools.callServiceJSON(request, "actualizarLineadetallesVenta.action?servicio=actualizarLineadetallesVenta&xml="+URLEncoder.encode(xml,"UTF-8"));
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}
					
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
			
			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
		}	
		
		
		// ***************************************************************************************************
		@RequestMapping("ajax/venta/numerada/eliminarEntradas.do")
		public ResponseEntity<?> busquedaNumeradaEliminarEntradas(									   
										   @RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
										   HttpServletRequest request) throws Exception {
			

			String json = "";

			try {
				json = Tools.callServiceJSON(request, "anularEntradas.action?servicio=anularEntradas&xml="+URLEncoder.encode(xml,"UTF-8"));
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}
					
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
			
			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
		}
		
		//********************************************************************************************************
		@RequestMapping("/ajax/venta/ventareserva/show_edit_sesion_numerada.do")
		public ModelAndView editarSesion(	
				@RequestParam(value = "idproducto", required = false, defaultValue = "") String idproducto,
				@RequestParam(value = "idcanal", required = false, defaultValue = "") String idcanal,
				@RequestParam(value = "iddescuento", required = false, defaultValue = "") String iddescuento,
				@RequestParam(value = "idperfil", required = false, defaultValue = "") String idperfil,
				@RequestParam(value = "idSesion", required = false, defaultValue = "") String idSesion,
				@RequestParam(value = "list", required = false, defaultValue = "") String list,				
				HttpServletRequest request) throws Exception {
			
		
			ModelAndView model = new ModelAndView();
			model.addObject("perfiles", Tools.callServiceXML(request, "obtenerListadoPerfilesAplicablesPalau.action?idsesion="+idSesion+"&idproducto="+idproducto+"&idcanal="+idcanal));
			model.addObject("list",list);
			model.addObject("idperfil",idperfil);
			model.addObject("iddescuento",iddescuento);
			
			model.setViewName("app.venta.numerada.ediatr.sesion");
			
			return model;
		}
		
		
		// ***************************************************************************************************
		@RequestMapping("ajax/venta/numerada/list_contenidos_temporada.do")
		public ResponseEntity<?> busquedaNumeradaListContenidosTemporada(
				@RequestParam(value = "idtemporada", required = false, defaultValue = "") String idtemporada
				, HttpServletRequest request) throws Exception {

		     String json="";
		       try {
		               json = Tools.callServiceJSON(request, "obtenerProgramacionPorTemporada.action?servicio=obtenerProgramacionPorTemporada&xml=<int>"+idtemporada+"</int>");
		        } catch (Exception ex) {
		               throw new AjaxException(ex.getMessage());
		        }		
			
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}
		
		
		// ***************************************************************************************************
				@RequestMapping("ajax/venta/numerada/list_contenidos_by_tipo.do")
				public ResponseEntity<?> busquedaNumeradaListContenidosTipo(
						@RequestParam(value = "idtemporada", required = false, defaultValue = "") String idtemporada,
						@RequestParam(value = "idtipo_producto", required = false, defaultValue = "") String idtipo_producto,
						HttpServletRequest request) throws Exception {

				     String json="";
				       try {
				               json = Tools.callServiceJSON(request, "obtenerProgramacionPorTemporadaYTipoproducto.action?servicio=obtenerProgramacionPorTemporadaYTipoproducto&xml=<param><idTemporada>"+idtemporada+"</idTemporada><idTipoproducto>"+idtipo_producto+"</idTipoproducto></param>");
				        } catch (Exception ex) {
				               throw new AjaxException(ex.getMessage());
				        }		
					
					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}
				
				
				// ***************************************************************************************************
				@RequestMapping("ajax/venta/ventareserva/realizar_venta_numerada.do")
				public ResponseEntity<?> ventareservaRealizarVentaIndividual(
						@RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
						HttpServletRequest request) throws Exception {

				     String json="";
				       try {
			               		json = Tools.callServiceJSON(request, "/postRealizarVentaLocalidadesNumeradas.action?_modo=&servicio=postRealizarVentaLocalidadesNumeradas&idOperacionVenta=&xml="+URLEncoder.encode(xml,"UTF-8"));
				       	   } catch (Exception ex) {
			               throw new AjaxException(ex.getMessage());
				        }		
					
					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}
				
				
				//********************************************************************************************************
				@RequestMapping("/ajax/venta/ventareserva/show_seleccion_localidadeds_venta_numerada.do")
				public ModelAndView seleccionLocalidadesVenta(	
						@RequestParam(value = "idrecinto", required = false, defaultValue = "") String idrecinto,
						@RequestParam(value = "idSesion", required = false, defaultValue = "") String idSesion,
						@RequestParam(value = "vender", required = false, defaultValue = "") String vender,
						HttpServletRequest request) throws Exception {
					
				
					ModelAndView model = new ModelAndView();
					model.addObject("precios", Tools.callServiceJSON(request, "obtenerPreciosPorZona.action?servicio=obtenerPreciosPorZona&xml=<dto><idsesion>"+idSesion+"</idsesion></dto>"));
					model.addObject("zonas", Tools.callServiceJSON(request, "obtenerEstadoZonasGraficaEnSesion.action?servicio=obtenerEstadoZonasGraficaEnSesion&xml=<int>"+idSesion+"</int>"));
					model.addObject("zonasNombre", Tools.callServiceJSON(request, "obtenerZonaFisicaNombreArea.action?servicio=obtenerZonaFisicaNombreArea&xml=<int>"+idrecinto+"</int>"));
					model.addObject("vender", vender);
					model.addObject("idSesion", idSesion);
					
					model.setViewName("app.venta.numerada.seleccion.localidades.venta");
					
					return model;
				}
				
				//********************************************************************************************************
				@RequestMapping("/ajax/venta/ventareserva/realizar_bloqueo_numerada.do")
				public ModelAndView realizarBloqueoNumerada(	
						@RequestParam(value = "xml", required = false, defaultValue = "") String xml,
						HttpServletRequest request) throws Exception {
					
				
					ModelAndView model = new ModelAndView();
					model.addObject("lineasDetalle", Tools.callServiceJSON(request, "actualizarPrerreservaLocalidadesNumeradas.action?servicio=actualizarPrerreservaLocalidadesNumeradas&xml="+xml));
										
					model.setViewName("app.venta.numerada.realizar.bloqueo.localidades.venta");
					
					return model;
				}
				
				// ***************************************************************************************************
				@RequestMapping("ajax/venta/numerada/obtener_localidades_porSesionYZona.do")
				public ResponseEntity<?> obtenerLocalidadesPorSesionYZona(
						@RequestParam(value = "idzonafisica", required = false, defaultValue = "") String idzonafisica,
						@RequestParam(value = "idSesion", required = false, defaultValue = "") String idSesion
						, HttpServletRequest request) throws Exception {

				     String json="";
				       try {
				               json = Tools.callServiceJSON(request, "obtenerLocalidadesPorSesionYZona.action?servicio=obtenerLocalidadesPorSesionYZona&zonafisica="+idzonafisica+"&idSesion="+idSesion);
				        } catch (Exception ex) {
				               throw new AjaxException(ex.getMessage());
				        }		
					
					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}
				
				// ***************************************************************************************************
				@RequestMapping("ajax/venta/numerada/bloquear_localidades.do")
				public ModelAndView busquedaNumeradaBloquearLocalidades(									   
												   @RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
												   HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					
					model.addObject("localidades", Tools.callServiceJSON(request, "actualizarPrerreservaLocalidadesNumeradas.action?servicio=actualizarPrerreservaLocalidadesNumeradas&xml="+URLEncoder.encode(xml,"UTF-8")));
					model.setViewName("app.venta.numerada.venta.bloquear.lineas");
					
					return model;			
						
						
				}
				
				
				// ***************************************************************************************************
				@RequestMapping("ajax/venta/numerada/bloquear_localidades_numerada.do")
				public ResponseEntity<?> bloquearLocalidadesNumerada(
						@RequestParam(value = "xml", required = false, defaultValue = "") String xml,
						HttpServletRequest request) throws Exception {

				     String json="";
				       try {
				               json = Tools.callServiceJSON(request, "bloquearLocalidadesNumeradas.action?servicio=bloquearLocalidadesNumeradas&xml="+xml);
				        } catch (Exception ex) {
				               throw new AjaxException(ex.getMessage());
				        }		
					
					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}
	
				//********************************************************************************************************
				@RequestMapping("/ajax/venta/ventareserva/show_seleccion_localidades_venta_numerada_automatica.do")
				public ModelAndView seleccionLocalidadesVentaAutomatica(	
						@RequestParam(value = "idrecinto", required = false, defaultValue = "") String idrecinto,
						@RequestParam(value = "idSesion", required = false, defaultValue = "") String idSesion,
						HttpServletRequest request) throws Exception {
					
				
					ModelAndView model = new ModelAndView();
					
					model.addObject("zonasPorRecinto", Tools.callServiceXML(request, "obtenerZonasNumeradasPorRecinto.action?servicio=obtenerZonasNumeradasPorRecinto&xml=<int>"+idrecinto+"</int>"));
					model.addObject("maxLoc", Tools.callServiceJSON(request, "obtenerMaximaLocalidadesParaVenta.action?servicio=obtenerMaximaLocalidadesParaVenta"));
				    model.addObject("zonas", Tools.callServiceJSON(request, "obtenerEstadoZonasGraficaEnSesion.action?servicio=obtenerEstadoZonasGraficaEnSesion&xml=<int>"+idSesion+"</int>"));
					model.addObject("zonasNombre", Tools.callServiceJSON(request, "obtenerZonaFisicaNombreArea.action?servicio=obtenerZonaFisicaNombreArea&xml=<int>"+idrecinto+"</int>"));
					model.addObject("idSesion", idSesion);
					
					model.setViewName("app.venta.numerada.seleccion.automatica.localidades.venta");
					
					return model;
				}	
				
				// ***************************************************************************************************
				@RequestMapping("ajax/venta/numerada/imprimir_abono.do")
				public ModelAndView busquedaNumeradaBloquearLocalidades(HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
										
					model.setViewName("app.venta.numerada.abono.imprimir");
					
					return model;			
						
						
				}
				
				
				// ***************************************************************************************************
				@RequestMapping("ajax/venta/numerada/prereservaAutomaticaLocalidades.do")
				public ResponseEntity<?> prereservaAutomaticaLocalidades(
						@RequestParam(value = "zona", required = false, defaultValue = "") String zona,
						@RequestParam(value = "sesion", required = false, defaultValue = "") String sesion,
						@RequestParam(value = "numero", required = false, defaultValue = "") String numero,
						HttpServletRequest request) throws Exception {

				     String json="";
						HttpHeaders responseHeaders = new HttpHeaders();
						responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
				     
				     String xml = "<parametro><int>"+zona+"</int><int>"+sesion+"</int><int>"+numero+"</int></parametro>";
				       try {
				         /*  String xml_result = Tools.callServiceXML(request, "asignacionAutomaticaButacas.action?servicio=asignacionAutomaticaButacas&xml="+xml);
				           JSONObject jsonAsignacion = XML.toJSONObject(xml_result);
				           jsonAsignacion = jsonAsignacion.getJSONObject("ArrayList");
				           
				           if(numero.equalsIgnoreCase("1"))
				           {
				        	   jsonAsignacion = jsonAsignacion.getJSONObject("Temp_resultado_butaca");
				        	   String idlocalidad = ""+ jsonAsignacion.getInt("idlocalidad");
				        	   xml = "<list><Estadolocalidad><sesion><idsesion>"+sesion+"</idsesion></sesion><localidad><idlocalidad>"+idlocalidad+"</idlocalidad></localidad></Estadolocalidad></list>";	  
				           }
				           else
				           {
					           JSONArray jArray ;
					           jArray = new JSONArray();
					           jArray = jsonAsignacion.getJSONArray("Temp_resultado_butaca");
					           xml = "<list>";				           
					           for (int i = 0; i < jArray.length(); i++) {
					        	      	
					        	   String idlocalidad = ""+ jArray.getJSONObject(i).getInt("idlocalidad");
					        	   xml +="<Estadolocalidad><sesion><idsesion>"+sesion+"</idsesion></sesion><localidad><idlocalidad>"+idlocalidad+"</idlocalidad></localidad></Estadolocalidad>";
					        		 
						       }
					           xml +="</list>";
				           }
				           json = Tools.callServiceJSON(request, "actualizarPrerreservaLocalidadesNumeradas.action?servicio=actualizarPrerreservaLocalidadesNumeradas&xml="+xml);
				           */
				    	   json = Tools.callServiceJSON(request, "asignacionAutomaticaButacas.action?servicio=asignacionAutomaticaButacas&xml="+xml);
				        } catch (Exception ex) {
				            throw new AjaxException(ex.getMessage());
				        }		
					


					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}
				
				
}


// -----------------------------------------------------------------------
// TODO: 
// -----------------------------------------------------------------------

package es.cac.colossus.frontend.web.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import es.cac.colossus.frontend.utils.AjaxException;
import es.cac.colossus.frontend.utils.Tools;

@Controller
public class DocumentacionController {
	
	@Autowired
	private MessageSource messageSource;	
	//*****************************************************************************************************
	@RequestMapping("documentacion/informes/gestion.do")
	public String documentacionShowGestionInformes(
			HttpServletRequest request) throws Exception {
				
		String idUsuario=request.getSession().getAttribute("idUsuario").toString();
    	String urlgestor=Tools.callServiceXMLSinReemplazo(request, "abrirInformeUsuario.action?servicio=abrirInformeUsuario&xml="+URLEncoder.encode("<java.lang.Integer>"+idUsuario+"</java.lang.Integer>","UTF-8"));
    	
        return "redirect:"+urlgestor.replaceAll("<[^>]*>", "");
	}
	
	//*****************************************************************************************************
	@RequestMapping("documentacion/documentos/gestion.do")
	public ModelAndView documentacionShowGestion(HttpServletRequest request) throws Exception {
				
		ModelAndView model = new ModelAndView();
    	model.setViewName("app.documentacion.documentos.gestion");
		model.addObject("title", messageSource.getMessage("documentacion.documentos.gestion.title", null, null));
		model.addObject("menu",Tools.getUserMainMenu(request));

		model.addObject("soloConsulta","0");
		model.addObject("gestiondocumentos_selector_categorias",Tools.callServiceXML(request, "obtenerListadoCategoria.action?servicio=obtenerListadoCategoria&xml="));
		model.addObject("gestiondocumentos_rutaweb",Tools.callServiceXML(request, "obtenerRutaWebDocumentos.action?servicio=obtenerRutaWebDocumentos"));
    			
    	return model;
	}

	//*****************************************************************************************************
	@RequestMapping("documentacion/documentos/consulta.do")
	public ModelAndView documentacionShowConsulta(HttpServletRequest request) throws Exception {
				
		ModelAndView model = new ModelAndView();
    	model.setViewName("app.documentacion.documentos.gestion");
		model.addObject("title", messageSource.getMessage("documentacion.documentos.consulta.title", null, null));
		model.addObject("menu",Tools.getUserMainMenu(request));			

		model.addObject("soloConsulta","1");
		model.addObject("gestiondocumentos_selector_categorias",Tools.callServiceXML(request, "obtenerListadoCategoria.action?servicio=obtenerListadoCategoria&xml="));
		model.addObject("gestiondocumentos_rutaweb",Tools.callServiceXML(request, "obtenerRutaWebDocumentos.action?servicio=obtenerRutaWebDocumentos"));
    			
    	return model;
	}
	
	// *****************************************************************************************************
	@RequestMapping("/ajax/documentacion/documentos/list_documentos_gestion.do")
	public ResponseEntity<?> documentacionListDocumentos(
			@RequestParam(value = "idcategoria", required = false, defaultValue = "") String idCategoria,
			HttpServletRequest request) throws Exception {

		String json="";
		
		if (idCategoria.length()>0) {
	        try {
	               json = Tools.callServiceJSON(request, "obtenerListadodocumentosPorCategoria.action?servicio=obtenerListadodocumentosPorCategoria&xml="+URLEncoder.encode("<int>"+idCategoria+"</int>","UTF-8"));
	        } catch (Exception ex) {
	               throw new AjaxException(ex.getMessage());
	        }		
		}
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// ***************************************************************************************************
	@RequestMapping(value = "/ajax/documentacion/documentos/remove_documentos.do", method = RequestMethod.POST)
	public ResponseEntity<String> documnentacionRemoveDocumentos(
			@RequestParam(value = "data", required = false, defaultValue = "") String data,
			HttpServletRequest request) throws Exception {

		String xml = Tools.callServiceXML(request,"darDeBajaDocumento.action?servicio=darDeBajaDocumento&xml="+URLEncoder.encode("<int>"+data+"</int>","UTF-8"));

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}

	//*****************************************************************************************************
	@RequestMapping("/ajax/documentacion/documentos/nuevo_documento.do")
	public ModelAndView documentacionShowNuevoDocumento(HttpServletRequest request) throws Exception {
				
		ModelAndView model = new ModelAndView();
    	model.setViewName("app.documentacion.documentos.gestion.nuevo");
		model.addObject("nuevodocumento_selector_categorias",Tools.callServiceXML(request, "obtenerListadoCategoria.action?servicio=obtenerListadoCategoria&xml="));
		model.addObject("nuevodocumento_rutaweb",Tools.callServiceXML(request, "obtenerRutaWebDocumentos.action?servicio=obtenerRutaWebDocumentos"));
    			
    	return model;
	}

	// ***************************************************************************************************
	@RequestMapping(value = "/ajax/documentacion/documentos/save_documento.do", method = RequestMethod.POST)
	public ResponseEntity<String> documentacionSaveDocumento(
			@RequestParam(value = "ruta", required = false, defaultValue = "") String ruta,
			@RequestParam(value = "idcategoria_nuevo", required = false, defaultValue = "") String idcategoria,
			@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
			@RequestParam(value = "descripcion", required = false, defaultValue = "") String descripcion,
			@RequestParam(value = "fechainiciovigencia", required = false, defaultValue = "") String fechainiciovigencia,
			@RequestParam(value = "fechafinvigencia", required = false, defaultValue = "") String fechafinvigencia,
			HttpServletRequest request) throws Exception {

		String xml = Tools.callServiceXML(request,"postDarDeAltaDocumento.action?_modo=alta&servicio=postDarDeAltaDocumento&xml="+URLEncoder.encode("<getDarDeAltaDocumento><Documento><nombre>"+nombre+"</nombre><descripcion>"+descripcion+"</descripcion><ruta>"+ruta+"</ruta><fechainiciovigencia>"+fechainiciovigencia+"</fechainiciovigencia><fechafinvigencia>"+fechafinvigencia+"</fechafinvigencia><orden/><categoria><idcategoria>"+idcategoria+"</idcategoria></categoria><dadodebaja>0</dadodebaja></Documento></getDarDeAltaDocumento>","UTF-8"));

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}

}

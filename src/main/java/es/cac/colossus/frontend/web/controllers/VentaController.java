package es.cac.colossus.frontend.web.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import es.cac.colossus.frontend.utils.AjaxException;
import es.cac.colossus.frontend.utils.Tools;

@Controller
public class VentaController {
	
	//GGL Meto log
	private static final Log log = LogFactory.getLog(VentaController.class);

	@Autowired
	private MessageSource messageSource;

	// ***************************************************************************************************
	@RequestMapping("ventareserva/ventareserva.do")
	public ModelAndView showVentaReservaInicial(HttpServletRequest request) throws Exception {
		return showVentaReserva(request);
	}
	
	// ***************************************************************************************************
	@RequestMapping("venta/ventareserva/ventareserva.do")
	public ModelAndView showVentaReserva(HttpServletRequest request) throws Exception {
				
		ModelAndView model = new ModelAndView();
	
		model.setViewName("app.venta.ventareserva.ventareserva");
		model.addObject("title", messageSource.getMessage("venta.ventareserva.title", null, null));
		model.addObject("menu", Tools.getUserMainMenu(request));
		
		model.addObject("ventareserva_productos_principales", Tools.callServiceXML(request, "obtenerListadoProductosPrincipales.action?servicio=obtenerListadoProductosPrincipales"));
		model.addObject("ventareserva_puntos_recogida", Tools.callServiceXML(request, "obtenerListadoPuntosrecogida.action?servicio=obtenerListadoPuntosrecogida"));
		model.addObject("ventareserva_canales_indirectos", Tools.callServiceXML(request, "obtenerListadoCanalesIndirectos.action?servicio=obtenerListadoCanalesIndirectos"));
		model.addObject("ventareserva_idiomas", Tools.callServiceXML(request, "obtenerListadoIdiomas.action?servicio=obtenerListadoIdiomas"));
		model.addObject("ventareserva_operaciones_canal", Tools.callServiceXML(request, "obtenerListadoOperacionesPorCanal.action?servicio=obtenerListadoOperacionesPorCanal&xml="+URLEncoder.encode("<canal><idcanal>4</idcanal></canal>","UTF-8")));
		model.addObject("ventareserva_formas_pago", Tools.callServiceXML(request, "obtenerFormasPago.action?servicio=obtenerFormasPago"));
		model.addObject("ventareserva_pago_canal", Tools.callServiceXML(request, "/obtenerListadoFormasPagoPorCanal.action?servicio=obtenerListadoFormasPagoPorCanal&xml="+URLEncoder.encode("<canal><idcanal>4</idcanal></canal>","UTF-8")));
		model.addObject("ventareserva_productos_disponibles", Tools.callServiceXML(request, "obtenerListadoProductosDisponiblesParaAbonoNoNumerados.action?servicio=obtenerListadoProductosDisponiblesParaAbonoNoNumerados"));
		model.addObject("ventareserva_productos_canal", Tools.callServiceXML(request, "obtenerListadoProductosPorCanal.action?servicio=obtenerListadoProductosPorCanal&xml=<c><idcanal>2</idcanal></c>"));	
		model.addObject("ventareserva_tipos_bono", Tools.callServiceXML(request, "obtenerListadoTiposBono.action?servicio=obtenerListadoTiposBono"));
		model.addObject("ventareserva_tarifas_bonos", Tools.callServiceXML(request, "obtenerListadoTarifasBono.action?servicio=obtenerListadoTarifasBono"));

		Tools.callServiceXML(request, "cancelarPrerreservaLocalidadesNoNumeradas.action");
	
		return model;
	}

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/list_subproductos.do")
	public ResponseEntity<?> ventareservaListSubproductos(
			@RequestParam(value = "id", required = false, defaultValue = "") String idUnidad, 
			HttpServletRequest request) throws Exception {

	    String json="";
	     
	    if (idUnidad.length()>0) {
	       try {
	               json = Tools.callServiceJSON(request, "obtenerListadoSubproductosPorUnidadNegocio.action?servicio=obtenerListadoSubproductosPorUnidadNegocio&xml="+idUnidad);
	               
	        } catch (Exception ex) {
	               throw new AjaxException(ex.getMessage());
	        }		
	    }
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}	
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/show_list_combinados.do")
	public ModelAndView showListCombinados(
			@RequestParam(value = "tabla", required = false, defaultValue = "") String tabla, 
			@RequestParam(value = "list", required = false, defaultValue = "") String list, 
			HttpServletRequest request) throws Exception {
	
		ModelAndView model = new ModelAndView();

		model.setViewName("app.venta.ventareserva.list_combinados");
		model.addObject("tabla",tabla);
		model.addObject("list",list);
		
		return model;
	}

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/list_combinados.do")
	public ResponseEntity<?> ventareservaListCombinados(
			@RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
			HttpServletRequest request) throws Exception {

       String json="";
	     
       try {
               json = Tools.callServiceJSON(request, "obtenerListadoProductosCombinados.action?servicio=obtenerListadoProductosCombinados&xml="+URLEncoder.encode(xml,"UTF-8"));
        } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}	
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/show_list_productos.do")
	public ModelAndView showListProductos(
			@RequestParam(value = "tabla", required = false, defaultValue = "") String tabla, 
			@RequestParam(value = "list", required = false, defaultValue = "") String list, 
			HttpServletRequest request) throws Exception {
	
		ModelAndView model = new ModelAndView();

		model.setViewName("app.venta.ventareserva.list_productos");
		model.addObject("tabla",tabla);
		model.addObject("list",list);
		
		return model;
	}

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/list_productos.do")
	public ResponseEntity<?> ventareservaListProductos(
			@RequestParam(value = "idcombinado", required = false, defaultValue = "") String idCombinado, 
			HttpServletRequest request) throws Exception {

       String json="";
	     
       try {
               json = Tools.callServiceJSON(request, "obtenerListadoProductosPorProductoCombinado.action?servicio=obtenerListadoProductosPorProductoCombinado&xml="+URLEncoder.encode("<int>"+idCombinado+"</int>","UTF-8"));
        } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}	

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/combinar_productos.do")
	public ResponseEntity<?> ventareservaCombinarProductos(
			@RequestParam(value = "idcliente", required = false, defaultValue = "") String idCliente,
			@RequestParam(value = "idcanal", required = false, defaultValue = "2") String idCanal,
			@RequestParam(value = "idtipoventa", required = false, defaultValue = "4") String idTipoVenta,
			@RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
			@RequestParam(value = "producto", required = false, defaultValue = "") String producto, 
			HttpServletRequest request) throws Exception {

       String json="";
	     
       try {
               json = Tools.callServiceJSON(request, "combinarLineasDetalle.action?tipoLineadetalle=Lineadetalle&idcliente="+idCliente+"&idtipoventa="+idTipoVenta+"&idcanal="+idCanal+"&lineasdetalle="+URLEncoder.encode(xml,"UTF-8")+"&nuevoProducto="+URLEncoder.encode(producto,"UTF-8"));
        } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
        }		
		
        HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}	

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/get_producto.do")
	public ResponseEntity<?> ventareservaGetProducto(
			@RequestParam(value = "tipolineadetalle", required = false, defaultValue = "Lineadetalle") String tipoLineaDetalle,
			@RequestParam(value = "idcliente", required = false, defaultValue = "") String idCliente,
			@RequestParam(value = "idcanal", required = false, defaultValue = "2") String idCanal,
			@RequestParam(value = "idtipoventa", required = false, defaultValue = "4") String idTipoVenta,
			@RequestParam(value = "idproducto", required = false, defaultValue = "") String idProducto,
			@RequestParam(value = "fecha", required = false, defaultValue = "") String fecha,
			@RequestParam(value = "autosesion", required = false, defaultValue = "false") String autosesion,
			HttpServletRequest request) throws Exception {

	    String json="";
	     
	    if (idTipoVenta.length()>0) {
	       try {
               json = Tools.callServiceJSON(request, "obtenerProductoPorIdVentaReserva.action?servicio=obtenerProductoPorIdVentaReserva&isLdGratuita=false&idLineadetalle=&tipoLineadetalle="+tipoLineaDetalle+"&idtipoventa="+idTipoVenta+"&idcliente="+idCliente+"&idcanal="+idCanal+"&idproducto="+idProducto+"&date="+URLEncoder.encode(fecha,"UTF-8")+"&autosesion="+autosesion);
	        } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }
	    }
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/show_disponibles.do")
	public ModelAndView showDisponibles(
			@RequestParam(value = "idproducto", required = false, defaultValue = "") String idProducto,
			@RequestParam(value = "idcliente", required = false, defaultValue = "") String idCliente,
			@RequestParam(value = "idtipoventa", required = false, defaultValue = "4") String idTipoVenta,
			@RequestParam(value = "producto", required = false, defaultValue = "") String producto,
			@RequestParam(value = "fecha", required = false, defaultValue = "") String fecha,
			@RequestParam(value = "button", required = false, defaultValue = "") String buttonAdd,
			@RequestParam(value = "list", required = false, defaultValue = "") String list,
			@RequestParam(value = "edicion", required = false, defaultValue = "0") String edicion,
			@RequestParam(value = "map", required = false, defaultValue = "<map />") String map,
			HttpServletRequest request) throws Exception {
	
		ModelAndView model = new ModelAndView();
		
	    
	    

	    
	    String xmlCadena = "";
	    
	    String xmlProducto = Tools.callServiceXML(request, "getEdicionAltaProducto.action?servicio=obtenerProductoPorIdNocaducadasConI18n&xml="+idProducto);
	    JSONObject jsonProducto = XML.toJSONObject(xmlProducto);
	    
	    try{
		 Object object = jsonProducto.getJSONObject("Producto").getJSONObject("tarifaproductos").get("Tarifaproducto");
		
		 if(object instanceof JSONArray)
		 {
			 JSONArray tarifas = jsonProducto.getJSONObject("Producto").getJSONObject("tarifaproductos").getJSONArray("Tarifaproducto");
	 	    
	 	    for( int i=0; i<tarifas.length(); i++)
	 			{
	 	        JSONObject jsonTarifa =  tarifas.getJSONObject(i);
	 	       
	 	       String strHoy=fecha.substring(6,10)+fecha.substring(3,5)+fecha.substring(0,2);
	 	       
	 	       
	 	        String fecha_inicio = ""+jsonTarifa.get("fechainiciovigencia");
	 	       fecha_inicio = fecha_inicio.substring(6,10)+fecha_inicio.substring(3,5)+fecha_inicio.substring(0,2);
	 	        String fecha_fin = ""+jsonTarifa.get("fechafinvigencia");
	 	       fecha_fin = fecha_fin.substring(6,10)+fecha_fin.substring(3,5)+fecha_fin.substring(0,2);
	 	        if( (fecha_inicio.compareTo(strHoy)<=0) &&  (strHoy.compareTo(fecha_fin)<=0))
		 	        {
		 	        String idTarifa =""+ jsonTarifa.getJSONObject("tarifa").get("idtarifa");
		 	    	try {
		 	    		String xml = Tools.callServiceXML(request, "getEdicionAltaTarifas.action?servicio=obtenerTarifaPorIdConI18n&xml="+idTarifa);
		 	            JSONObject json = XML.toJSONObject(xml);
		 	            jsonTarifa.put("perfilvisitantes",json.getJSONObject("Tarifa").getJSONObject("perfilvisitantes"));            
		 	    		
		 		        } catch (Exception ex) {
		 	               throw new AjaxException(ex.getMessage());
		 		        }
		 			}    
	 	        		
	 			}
		 }
		 else
		 {
			 JSONObject jsonTarifa = jsonProducto.getJSONObject("Producto").getJSONObject("tarifaproductos").getJSONObject("Tarifaproducto");
	    	 String idTarifa =""+ jsonTarifa.getJSONObject("tarifa").get("idtarifa");
	 	    	try {
	 	    		String xml = Tools.callServiceXML(request, "getEdicionAltaTarifas.action?servicio=obtenerTarifaPorIdConI18n&xml="+idTarifa);
	 	            JSONObject json = XML.toJSONObject(xml);
	 	            jsonTarifa.put("perfilvisitantes",json.getJSONObject("Tarifa").getJSONObject("perfilvisitantes"));            
	 	    		
	 		        } catch (Exception ex) {
	 	               throw new AjaxException(ex.getMessage());
	 		        } 
		 }
	    }
	    catch(Exception ex){
	    	
	    }
	    	
	    model.setViewName("app.venta.ventareserva.disponibles");
		model.addObject("buttonAdd",buttonAdd);
		model.addObject("list",list);
		model.addObject("idProducto",idProducto);
		model.addObject("idCliente",idCliente);
		model.addObject("idtipoventa",idTipoVenta);
		model.addObject("producto",producto);
		model.addObject("fecha",fecha);
		model.addObject("edicion",edicion);
		model.addObject("jsonProducto",jsonProducto);

		String xml_sesiones= Tools.callServiceXML(request, "obtenerSesionesPorProductoYFecha.action?servicio=obtenerSesionesPorProductoYFecha&fecha="+URLEncoder.encode(fecha,"UTF-8")+"&idproducto="+idProducto+"&map="+URLEncoder.encode(map,"UTF-8")); 
		JSONObject json_sesiones = XML.toJSONObject(xml_sesiones);
		if (json_sesiones.has("error")) throw new Exception(json_sesiones.getString("error"));

		model.addObject("disponibles_datos_sesiones",xml_sesiones);
		model.addObject("disponibles_datos_sesiones_json",json_sesiones.toString(4));

		return model;
	}

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/show_tabla_disponibilidad.do")
	public ModelAndView showDisponibles(
			@RequestParam(value = "button", required = false, defaultValue = "") String buttonAdd,
			HttpServletRequest request) throws Exception {
	
		ModelAndView model = new ModelAndView();

		model.setViewName("app.venta.ventareserva.tabla_disponibilidad");
		
		model.addObject("buttonAdd",buttonAdd);

		return model;
	}

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/show_hora_presentacion.do")
	public ModelAndView showHorasPresentacion(
			@RequestParam(value = "button", required = false, defaultValue = "") String buttonAdd,
			@RequestParam(value = "list", required = false, defaultValue = "") String list,
			@RequestParam(value = "xml", required = false, defaultValue = "") String xml,
			HttpServletRequest request) throws Exception {
	
		ModelAndView model = new ModelAndView();

		model.setViewName("app.venta.ventareserva.hora_presentacion");
		
		model.addObject("buttonAdd",buttonAdd);
		model.addObject("list",list);

		String xml_horas= Tools.callServiceXML(request, "obtenerListadoHoraPresentacion.action?servicio=obtenerListadoHoraPresentacion&xml="+URLEncoder.encode(xml,"UTF-8")); 

		model.addObject("hora_presentacion_datos",xml_horas);

		return model;
	}

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/list_tipos_producto.do")
	public ResponseEntity<?> ventareservaListTiposProducto(
			@RequestParam(value = "idtipoproducto", required = false, defaultValue = "-1") String idTipoProducto, 
			@RequestParam(value = "fecha", required = false, defaultValue = "") String fecha, 
			HttpServletRequest request) throws Exception {

	     String json="";
	       try {
               json = Tools.callServiceJSON(request, "obtenerSesionesPorTipoProductoYFecha.action?servicio=obtenerSesionesPorTipoProductoYFecha&idproducto=null&idTipoProducto="+idTipoProducto+"&date="+URLEncoder.encode(fecha,"UTF-8"));
	        } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}	
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/list_datos_sesiones.do")
	public ResponseEntity<?> ventareservaListDatosSesiones(
			@RequestParam(value = "idproducto", required = false, defaultValue = "-1") String idProducto, 
			@RequestParam(value = "fecha", required = false, defaultValue = "") String fecha, 
			@RequestParam(value = "idcliente", required = false, defaultValue = "") String idCliente, 
			@RequestParam(value = "idcanal", required = false, defaultValue = "") String idCanal, 
			@RequestParam(value = "idtipoventa", required = false, defaultValue = "4") String idTipoVenta, 
			@RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
			HttpServletRequest request) throws Exception {

	     String json="";
	       try {
               		json = Tools.callServiceJSON(request, "obtenerProductoPorIdVentaReserva.action?servicio=obtenerProductoPorIdVentaReserva&isLdGratuita=false&idLineadetalle=&tipoLineadetalle=Lineadetalle&idtipoventa="+idTipoVenta+"&idcliente="+idCliente+"&idcanal="+idCanal+"&idproducto="+idProducto+"&date="+URLEncoder.encode(fecha,"UTF-8")+"&sesion="+URLEncoder.encode(xml,"UTF-8")+"&autosesion=false");
	       	   } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/list_datos_disponibilidad.do")
	public ResponseEntity<?> ventareservaListTiposProducto(
			@RequestParam(value = "fecha", required = false, defaultValue = "") String fecha, 
			HttpServletRequest request) throws Exception {

	     String json="";
	       try {
               json = Tools.callServiceJSON(request, "obtenerDisponibilidadPorFecha.action?servicio=obtenerDisponibilidadPorFecha&xml="+URLEncoder.encode(fecha,"UTF-8"));
	        } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}	

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/show_edit_emision_bono.do")
	public ModelAndView showEditEmisionBono(
			HttpServletRequest request) throws Exception {
	
		ModelAndView model = new ModelAndView();

		model.setViewName("app.venta.ventareserva.edit_emision_bono");
		
		model.addObject("ventareserva_listado_tiposbono", Tools.callServiceXML(request, "obtenerListadoTiposBono.action"));
		
		return model;
	}
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/show_vender_emision_bono.do")
	public ModelAndView showVenderEmisionBono(
			
			HttpServletRequest request) throws Exception {
	
		ModelAndView model = new ModelAndView();

		model.setViewName("app.venta.ventareserva.emision_bono");
		model.addObject("ventareserva_formaspago", Tools.callServiceXML(request, "obtenerListadoFormasPagoPorCanal.action?servicio=obtenerListadoFormasPagoPorCanal&xml="+URLEncoder.encode("<canal><idcanal>2</idcanal></canal>","UTF-8")));

		return model;
	}
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/show_vender_venta_individual.do")
	public ModelAndView showVenderVentaIndividual(
			
			@RequestParam(value = "idcanal", required = false, defaultValue = "") String idcanal, 
			HttpServletRequest request) throws Exception {
	
		ModelAndView model = new ModelAndView();

		model.setViewName("app.venta.ventareserva.venta_individual");
		model.addObject("ventareserva_formaspago", Tools.callServiceXML(request, "obtenerListadoFormasPagoPorCanal.action?servicio=obtenerListadoFormasPagoPorCanal&xml="+URLEncoder.encode("<canal><idcanal>"+idcanal+"</idcanal></canal>","UTF-8")));
		model.addObject("ventareserva_selector_idiomas", Tools.callServiceXML(request, "obtenerListadoIdiomasFiltrado.action?servicio=obtenerListadoIdiomasFiltrado&selected=-1"));
		
		return model;
	}
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/show_vender_reserva_individual.do")
	public ModelAndView showVenderReservaIndividual(
			HttpServletRequest request) throws Exception {
	
		ModelAndView model = new ModelAndView();

		model.setViewName("app.venta.ventareserva.reserva_individual");
		model.addObject("ventareserva_selector_idiomas", Tools.callServiceXML(request, "obtenerListadoIdiomasFiltrado.action?servicio=obtenerListadoIdiomasFiltrado&selected=-1"));
		
		return model;
	}
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/show_vender_reserva_grupo.do")
	public ModelAndView showVenderReservaGrupo(
			HttpServletRequest request) throws Exception {
	
		ModelAndView model = new ModelAndView();

		model.setViewName("app.venta.ventareserva.reserva_grupo");
		model.addObject("ventareserva_selector_idiomas", Tools.callServiceXML(request, "obtenerListadoIdiomasFiltrado.action?servicio=obtenerListadoIdiomasFiltrado&selected=-1"));
		model.addObject("ventareserva_selector_puntos_recogida", Tools.callServiceXML(request, "obtenerListadoPuntosrecogida.action?servicio=obtenerListadoPuntosrecogida"));
		
		return model;
	}

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/show_edit_sesion.do")
	public ModelAndView showEditSesion(
			@RequestParam(value = "idproducto", required = false, defaultValue = "-1") String idProducto, 
			@RequestParam(value = "idtarifa", required = false, defaultValue = "-1") String idTarifa, 
			@RequestParam(value = "idtipoventa", required = false, defaultValue = "4") String idTipoVenta, 
			@RequestParam(value = "idcanal", required = false, defaultValue = "") String idCanal, 
			@RequestParam(value = "fecha", required = false, defaultValue = "") String fecha,
			@RequestParam(value = "idcliente", required = false, defaultValue = "") String idCliente, 
			@RequestParam(value = "idxnumero", required = false, defaultValue = "") String idxNumero, 
			@RequestParam(value = "idxunitario", required = false, defaultValue = "") String idxUnitario,
			@RequestParam(value = "idxtotal", required = false, defaultValue = "") String idxTotal,
			@RequestParam(value = "idxbono", required = false, defaultValue = "") String idxBono, 
			@RequestParam(value = "idxtarifa_id", required = false, defaultValue = "") String idxTarifa_id,
			@RequestParam(value = "idxtarifa_txt", required = false, defaultValue = "") String idxTarifa_txt,
			@RequestParam(value = "idxdescuento_id", required = false, defaultValue = "") String idxDescuento_id,
			@RequestParam(value = "idxdescuento_txt", required = false, defaultValue = "") String idxDescuento_txt,
			@RequestParam(value = "idxperfil", required = false, defaultValue = "") String idxPerfil, 
			@RequestParam(value = "button", required = false, defaultValue = "") String buttonEdit,
			@RequestParam(value = "list", required = false, defaultValue = "") String list,
			@RequestParam(value = "map", required = false, defaultValue = "") String map,
			@RequestParam(value = "producto", required = false, defaultValue = "") String producto,
			HttpServletRequest request) throws Exception {
	
		ModelAndView model = new ModelAndView();

		model.setViewName("app.venta.ventareserva.edit_sesion");

		model.addObject("buttonEdit",buttonEdit);
		model.addObject("list",list);
		model.addObject("idProducto",idProducto);
		model.addObject("idCliente",idCliente);
		model.addObject("idTarifa",idTarifa);
		model.addObject("idTarifa",idTipoVenta);
		model.addObject("idxNumero",idxNumero);
		model.addObject("idxUnitario",idxUnitario);
		model.addObject("idxTotal",idxTotal);
		model.addObject("idxBono",idxBono);
		model.addObject("idxTarifa_id",idxTarifa_id);
		model.addObject("idxTarifa_txt",idxTarifa_txt);
		model.addObject("idxDescuento_id",idxDescuento_id);
		model.addObject("idxDescuento_txt",idxDescuento_txt);
		model.addObject("idxPerfil",idxPerfil);
		model.addObject("producto",producto);
		
		String xml_sesiones= Tools.callServiceXML(request, "obtenerSesionesPorProductoYFecha.action?servicio=obtenerSesionesPorProductoYFecha&fecha="+URLEncoder.encode(fecha,"UTF-8")+"&idproducto="+idProducto+"&map="+URLEncoder.encode(map,"UTF-8")); 
		JSONObject json_sesiones = XML.toJSONObject(xml_sesiones);
		if (json_sesiones.has("error")) throw new Exception(json_sesiones.getString("error"));

		model.addObject("disponibles_datos_sesiones",xml_sesiones);
		model.addObject("disponibles_datos_sesiones_json",json_sesiones.toString(4));

		
		model.addObject("ventareserva_selector_descuentos", Tools.callServiceXML(request, "obtenerListadoDescuentosAplicables.action?servicio=obtenerListadoDescuentosAplicables&idtarifa="+idTarifa+"&idproducto="+idProducto+"&idcanal="+idCanal));
		
		return model;
	}
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/list_descuentos.do")
	public ResponseEntity<?> ventareservaListDescuentos(
			@RequestParam(value = "idproducto", required = false, defaultValue = "-1") String idProducto, 
			@RequestParam(value = "idcanal", required = false, defaultValue = "-1") String idCanal, 
			@RequestParam(value = "idtarifa", required = false, defaultValue = "-1") String idTarifa, 
			HttpServletRequest request) throws Exception {

	     String json="";
	       try {
               json = Tools.callServiceJSON(request, "obtenerListadoDescuentosAplicables.action?servicio=obtenerListadoDescuentosAplicables&idtarifa="+idTarifa+"&idproducto="+idProducto+"&idcanal="+idCanal);
	        } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}	

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/aplicar_descuentos.do")
	public ResponseEntity<?> ventareservaAplicarDescuentos(
			@RequestParam(value = "idcliente", required = false, defaultValue = "") String idCliente, 
			@RequestParam(value = "lineadetalle", required = false, defaultValue = "") String lineaDetalle, 
			HttpServletRequest request) throws Exception {

		String json="";
        try {
           json = Tools.callServiceJSON(request, "aplicarDescuentoPromocionalListaActualizandoLineaDetalle.action?servicio=aplicarDescuentoPromocionalListaActualizandoLineaDetalle&idcliente="+idCliente+"&idtipoventa=4&lalinea="+URLEncoder.encode(lineaDetalle,"UTF-8"));
        } catch (Exception ex) {
           throw new AjaxException(ex.getMessage());
        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}
	
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/aplicar_descuentos_pases.do")
	public ResponseEntity<?> AplicarDescuentosPasesClub(
			@RequestParam(value = "idcliente", required = false, defaultValue = "") String idCliente, 
			@RequestParam(value = "lineadetalle", required = false, defaultValue = "") String lineaDetalle, 
			HttpServletRequest request) throws Exception {

		String json="";
        try {
           json = Tools.callServiceJSON(request, "aplicarDescuentoPromocionalListaActualizandoLineaDetallePases.action?servicio=aplicarDescuentoPromocionalListaActualizandoLineaDetallePases&idtipoventa=5&lalinea="+URLEncoder.encode(lineaDetalle,"UTF-8"));
        } catch (Exception ex) {
           throw new AjaxException(ex.getMessage());
        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/actualizar_prerreserva.do")
	public ResponseEntity<?> ventareservaActualizarPrerreserva(
			@RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
			HttpServletRequest request) throws Exception {

	     String json="";
	       try {
               		json = Tools.callServiceJSON(request, "actualizarPrerreservaLocalidadesNoNumeradas.action?servicio=actualizarPrerreservaLocalidadesNoNumeradas&xml="+URLEncoder.encode(xml,"UTF-8"));
	       	   } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/obtener_totales_ventatemporal.do")
	public ResponseEntity<?> ventareservaObtenerTotalesVentaTemporal(
			@RequestParam(value = "idcliente", required = false, defaultValue = "") String idCliente, 
			@RequestParam(value = "idtipoventa", required = false, defaultValue = "4") String idTipoVenta,
			@RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
			HttpServletRequest request) throws Exception {

	     String json="";
	       try {
               		json = Tools.callServiceJSON(request, "obtenerTotalesVentaTemporal.action?servicio=obtenerTotalesVentaTemporal&idcliente="+idCliente+"&idtipoventa="+idTipoVenta+"&lineas="+URLEncoder.encode(xml,"UTF-8"));
	       	   } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/obtener_descuento_cliente.do")
	public ResponseEntity<?> ventareservaObtenerDescuentoCliente(
			@RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
			HttpServletRequest request) throws Exception {

	     String json="";
	       try {
               		json = Tools.callServiceJSON(request, "obtenerPerfilesAplicablesConUnaTarifaProducto.action?servicio=obtenerPerfilesAplicablesConUnaTarifaProducto&xml="+URLEncoder.encode(xml,"UTF-8"));
	       	   } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/cancelar_prerreserva.do")
	public ResponseEntity<?> ventareservaCancelarPrerreserva(
			HttpServletRequest request) throws Exception {

	     String json="";
	       try {
               		json = Tools.callServiceJSON(request, "eliminarPrerreservaLocalidadesNoNumeradas.action?servicio=eliminarPrerreservaLocalidadesNoNumeradas");
	       	   } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/realizar_venta_individual.do")
	public ResponseEntity<?> ventareservaRealizarVentaIndividual(
			@RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
			HttpServletRequest request) throws Exception {

	     String json="";
	       try {
               		json = Tools.callServiceJSON(request, "/realizarVenta.action?servicio=realizarVenta&xml="+URLEncoder.encode(xml,"UTF-8"));
	       	   } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/enviar_carta_confirmacion.do")
	public ResponseEntity<?> enviarCartaConfirmacion(
			@RequestParam(value = "id", required = false, defaultValue = "") String id, 
			@RequestParam(value = "nombre_id", required = false, defaultValue = "") String nombreId,
			@RequestParam(value = "format", required = false, defaultValue = "") String format, 
			@RequestParam(value = "idioma", required = false, defaultValue = "") String idioma, 
			HttpServletRequest request) throws Exception {

		String url="/cartaConfirmacion.post?medioEnvio=&format="+format+"&idioma="+idioma+"&"+nombreId+"="+id;
		
	     String json="";
	       try {
               		json = Tools.callServiceJSON(request,url );
	       	   } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/realizar_reserva_individual.do")
	public ResponseEntity<?> ventareservaRealizarReservaIndividual(
			@RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
			HttpServletRequest request) throws Exception {

	     String json="";
	       try {
               		json = Tools.callServiceJSON(request, "/realizarReserva.action?servicio=realizarReserva&xml="+URLEncoder.encode(xml,"UTF-8"));
	       	   } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/realizar_reserva_grupo.do")
	public ResponseEntity<?> ventareservaRealizarReservaGrupo(
			@RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
			HttpServletRequest request) throws Exception {

	     String json="";
	       try {
               	xml=xml.replace("&","&amp;");
	    	   	json = Tools.callServiceJSON(request, "/realizarReserva.action?servicio=realizarReservaGrupo&xml="+URLEncoder.encode(xml,"UTF-8"));
	       	   } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/realizar_emision_bono.do")
	public ResponseEntity<?> ventareservaEmisionBono(
			@RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
			HttpServletRequest request) throws Exception {

	     String json="";
	       try {
               		json = Tools.callServiceJSON(request, "/realizarVentaBonos.action?servicio=realizarVentaBonos&xml="+URLEncoder.encode(xml,"UTF-8"));
	       	   } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}
		
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/show_emision_bono.do")
	public ModelAndView showEmisionBono(
			HttpServletRequest request) throws Exception {
	
		ModelAndView model = new ModelAndView();

		model.setViewName("app.venta.ventareserva.emision_bono");
		
		return model;
	}
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/show_reserva_grupo.do")
	public ModelAndView showReservaGrupo(
			HttpServletRequest request) throws Exception {
	
		ModelAndView model = new ModelAndView();

		model.setViewName("app.venta.ventareserva.reserva_grupo");
		
		return model;
	}
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/show_resumen_venta.do")
	public ModelAndView showResumenVenta(
			@RequestParam(value = "idventa", required = false, defaultValue = "") String idVenta,
			HttpServletRequest request) throws Exception {
	
		ModelAndView model = new ModelAndView();		
		model.setViewName("app.venta.ventareserva.venta_resumen");
		String ventareserva_resumen= Tools.callServiceXML(request, "obtenerVentaPorId.action?servicio=obtenerVentaPorId&xml="+URLEncoder.encode("<int>"+idVenta+"</int>","UTF-8")); 
		model.addObject("idVenta",idVenta);
		model.addObject("ventareserva_resumen", ventareserva_resumen);

		//Ponemos tambien el objeto en json
		JSONObject json = XML.toJSONObject(ventareserva_resumen);
		if (json.has("error")) throw new Exception(json.getString("error"));
			model.addObject("ventareserva_resumen_json", json);
		
		return model;
	}
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/show_resumen_reserva_individual.do")
	public ModelAndView showResumenReservaIndividual(
			@RequestParam(value = "idreserva", required = false, defaultValue = "") String idReserva,
			HttpServletRequest request) throws Exception {
	
		ModelAndView model = new ModelAndView();

		model.setViewName("app.venta.ventareserva.reserva_individual_resumen");

		String ventareserva_resumen= Tools.callServiceXML(request, "obtenerReservaPorId.action?servicio=obtenerReservaPorId&xml="+URLEncoder.encode("<int>"+idReserva+"</int>","UTF-8")); 
		model.addObject("idReserva",idReserva);
		model.addObject("ventareserva_resumen", ventareserva_resumen);

		//Ponemos tambien el objeto en json
		JSONObject json = XML.toJSONObject(ventareserva_resumen);
		if (json.has("error")) throw new Exception(json.getString("error"));
			model.addObject("ventareserva_resumen_json", json);
		
		return model;
	}
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/show_resumen_reserva_grupo.do")
	public ModelAndView showResumenReservaGrupo(
			@RequestParam(value = "idreserva", required = false, defaultValue = "") String idReserva,
			HttpServletRequest request) throws Exception {
	
		ModelAndView model = new ModelAndView();

		model.setViewName("app.venta.ventareserva.reserva_grupo_resumen");
		
		String ventareserva_resumen= Tools.callServiceXML(request, "obtenerReservaPorId.action?servicio=obtenerReservaPorId&xml="+URLEncoder.encode("<int>"+idReserva+"</int>","UTF-8")); 
		model.addObject("idReserva",idReserva);
		model.addObject("ventareserva_resumen", ventareserva_resumen);
		model.addObject("ventareserva_selector_puntos_recogida", Tools.callServiceXML(request, "obtenerListadoPuntosrecogida.action?servicio=obtenerListadoPuntosrecogida"));

		//Ponemos tambien el objeto en json
		JSONObject json = XML.toJSONObject(ventareserva_resumen);
		if (json.has("error")) throw new Exception(json.getString("error"));
			model.addObject("ventareserva_resumen_json", json);
		
		return model;
	}
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/show_resumen_emision_bono.do")
	public ModelAndView showResumenEmisionBono(
			@RequestParam(value = "idventa", required = false, defaultValue = "") String idVenta,
			HttpServletRequest request) throws Exception {
	
		ModelAndView model = new ModelAndView();

		model.setViewName("app.venta.ventareserva.emision_bono_resumen");
		String ventareserva_resumen= Tools.callServiceXML(request, "obtenerVentaPorId.action?servicio=obtenerVentaPorId&xml="+URLEncoder.encode("<int>"+idVenta+"</int>","UTF-8")); 
		model.addObject("idVenta",idVenta);
		model.addObject("ventareserva_resumen", ventareserva_resumen);

		//Ponemos tambien el objeto en json
		JSONObject json = XML.toJSONObject(ventareserva_resumen);
		if (json.has("error")) throw new Exception(json.getString("error"));
			model.addObject("ventareserva_resumen_json", json);
		
		return model;
	}
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/imprimir_recibo.do")
	public ResponseEntity<?> ventareservaImprimirRecibo(
			@RequestParam(value = "idventa", required = false, defaultValue = "") String idVenta, 
			HttpServletRequest request) throws Exception {

	     String json="";
	       try {
               		json = Tools.callServiceJSON(request, "imprimirRecibosVentaReserva.action?servicio=imprimirRecibosVentaReserva&xml="+URLEncoder.encode("<int>"+idVenta+"</int>","UTF-8"));
	       	   } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}	
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/imprimir_bono_venta.do")
	public ResponseEntity<?> ventareservaImprimirReciboBono(
			@RequestParam(value = "idventa", required = false, defaultValue = "") String idVenta, 
			HttpServletRequest request) throws Exception {

	     String json="";
	       try {
               		json = Tools.callServiceJSON(request, "imprimirBonosVenta.action?servicio=imprimirBonosVenta&xml="+URLEncoder.encode("<int>"+idVenta+"</int>","UTF-8"));
	       	   } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}	
	
	// ***************************************************************************************************
	@RequestMapping("/ajax/venta/ventareserva/show_generar_factura.do")
	public ModelAndView ventareservaShowGenerarFactura(
			@RequestParam(value = "idventa", required = false, defaultValue = "") String idVenta, 
			@RequestParam(value = "idcliente", required = false, defaultValue = "") String idCliente, 
			HttpServletRequest request) throws Exception {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("app.venta.ventareserva.generar_factura");

		model.addObject("idVenta",idVenta);
		model.addObject("idCliente",idCliente);
		model.addObject("tipos_serie_generacion_factura", Tools.callServiceXML(request,"obtenerListadoTiposFacturaEntidadGestora.action?servicio=obtenerListadoTiposFacturaEntidadGestora"));
		
		return model;
	}	
	
	// ***************************************************************************************************
		@RequestMapping("//ajax/venta/modificarClienteVenta.do")
		public ResponseEntity<?> modificarClienteVenta(
				@RequestParam(value = "idVenta", required = false, defaultValue = "") String idVenta, 
				@RequestParam(value = "idCliente", required = false, defaultValue = "") String idCliente,
				HttpServletRequest request) throws Exception {

		     String json="";
		       try {
	               		json = Tools.callServiceJSON(request, "modificarClienteVenta.action?servicio=modificarClienteVenta&xml="+URLEncoder.encode("<parametro><int>"+idVenta+"</int><cliente><idcliente>"+idCliente+"</idcliente></cliente></parametro>","UTF-8"));
		       	   } catch (Exception ex) {
	               throw new AjaxException(ex.getMessage());
		        }		
			
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}	
	
	
	// ***************************************************************************************************
	@RequestMapping("/ajax/venta/ventareserva/generar_factura.do")
	public ResponseEntity<?> ventareservaGenerarFactura(
			@RequestParam(value = "idventa", required = false, defaultValue = "") String idVenta, 
			@RequestParam(value = "idtipofactura", required = false, defaultValue = "") String idTipoFactura, 
			HttpServletRequest request) throws Exception {

	     String json="";
	       try {
               		json = Tools.callServiceJSON(request, "generarFactura.action?servicio=generarFactura&xml="+URLEncoder.encode("<gfp><listaIdsVenta><int>"+idVenta+"</int></listaIdsVenta><idTipoFactura>"+idTipoFactura+"</idTipoFactura></gfp>","UTF-8"));
	       	   } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}	
	
	

	// *****************************************************************************************************
	@RequestMapping("/ajax/venta/ventareserva/show_historico_canje_bono.do")
	public ModelAndView facturacionShowEmpresasGrupo(
			@RequestParam(value = "id", required = false, defaultValue = "") String idbono,
			HttpServletRequest request) throws Exception {
		
		ModelAndView model = new ModelAndView();
		String datos="";
		datos = "<int>"+idbono+"</int>";
		model.addObject("idbono", idbono);
		model.addObject("historico_bono", Tools.callServiceXML(request, "obtenerHistoricoEstadoBono.action?servicio=obtenerHistoricoEstadoBono&xml=" + datos));
		
		model.setViewName("app.venta.ventareserva.historico_bono");
		
		return model;
	}
	
	//*********************************LISTAR CANJE_BONO********************************************
	@RequestMapping(value = "/ajax/venta/ventareserva/canje_bono/list_canjebono.do", method = RequestMethod.POST)
	public ResponseEntity<?> ventareservaLisCanjeBono(
			@RequestParam(value = "fechainiciovigenciadecanje", required = false, defaultValue = "") String fechaEmisionIni,						 
			@RequestParam(value = "fechainiciovigenciahastacanje", required = false, defaultValue = "") String fechaEmisionFin, 
			@RequestParam(value = "numBonoIni", required = false, defaultValue = "") String numBonoIni,
			@RequestParam(value = "numBonoFin", required = false, defaultValue = "") String numBonoFin, 
			@RequestParam(value = "idBonoIni", required = false, defaultValue = "") String idBonoIni,			 
			@RequestParam(value = "idBonoFin", required = false, defaultValue = "") String idBonoFin,
			@RequestParam(value = "idventa", required = false, defaultValue = "") String idventa, 
			@RequestParam(value = "dniCliente", required = false, defaultValue = "") String dniCliente,
			@RequestParam(value = "idcliente2_canje_bono", required = false, defaultValue = "") String idcliente,
			@RequestParam(value = "idProducto", required = false, defaultValue = "") String idProducto,						
			@RequestParam(value = "idtipobono", required = false, defaultValue = "") String idtipobono,
			@RequestParam(value = "fechafinvigenciadecanje", required = false, defaultValue = "") String fechaCaducidadIni,
			@RequestParam(value = "fechafinvigenciahastacanje", required = false, defaultValue = "") String fechaCaducidadFin,
			@RequestParam(value = "idPerfil", required = false, defaultValue = "") String idPerfil,
			@RequestParam(value = "anulado", required = false, defaultValue = "") String anulado,
			@RequestParam(value = "canjeado", required = false, defaultValue = "false") String canjeado,
			@RequestParam(value = "facturado", required = false, defaultValue = "") String facturado,
			@RequestParam(value = "start", required = false, defaultValue = "0") String startRecord,
      		@RequestParam(value = "length", required = false, defaultValue = "") String maxLength,
      		@RequestParam(value = "idlineadetalleVentaBono", required = false, defaultValue = "") String idlineadetalleVentaBono,
      		HttpServletRequest request) throws Exception {
		
		String json = "";
		String xml="";
		String metodo="";
		
		if(idlineadetalleVentaBono.equalsIgnoreCase(""))
		{
		
		metodo = "buscarBonos.action?servicio=buscarBonos&xml=";	
			
		
		xml = xml + "<Buscarbonoparam>";
	
		xml = xml +"<fechaEmisionIni>"+fechaEmisionIni+"</fechaEmisionIni>";
		xml = xml +"<fechaEmisionFin>"+fechaEmisionFin+"</fechaEmisionFin>";
		xml = xml +"<numBonoIni/>";
		xml = xml +"<numBonoFin/>";
		xml = xml +"<idBonoIni>"+idBonoIni+"</idBonoIni>";
		xml = xml +"<idBonoFin>"+idBonoFin+"</idBonoFin>";
		xml = xml +"<idventa>"+idventa+"</idventa>";
		xml = xml +"<dniCliente>"+dniCliente+"</dniCliente>";
		xml = xml +"<idcliente>"+idcliente+"</idcliente>";
		xml = xml +"<idProducto>"+idProducto+"</idProducto>";
		xml = xml +"<idtipobono>"+idtipobono+"</idtipobono>";
		xml = xml +"<fechaCaducidadIni>"+fechaCaducidadIni+"</fechaCaducidadIni>";
		xml = xml +"<fechaCaducidadFin>"+fechaCaducidadFin+"</fechaCaducidadFin>";
		xml = xml +"<idPerfil>"+idPerfil+"</idPerfil>";
		xml = xml +"<anulado/>";
		xml = xml +"<canjeado/>";
		xml = xml +"<facturado/>";
		xml = xml + "<numeroprimerregistro>"+startRecord+"</numeroprimerregistro>";
		xml = xml + "<maxResultados>"+maxLength+"</maxResultados>";			

		xml = xml + "</Buscarbonoparam>";
		
		}
		else
		{
			metodo = "obtenerBonoPorLineaDetalle.action?servicio=obtenerBonoPorLineaDetalle&xml=";
			
			xml = xml + "<ObtenerBonoPorLineaDetalleparam>";
			xml = xml + "<idlineadetalle>"+idlineadetalleVentaBono+"</idlineadetalle>";
			xml = xml + "<numeroprimerregistro>"+startRecord+"</numeroprimerregistro>";
			xml = xml + "<maxResultados>"+maxLength+"</maxResultados>";	
			xml+="</ObtenerBonoPorLineaDetalleparam>";			
		}
		
		

		try {
			json = Tools.callServiceJSON(request, metodo+xml);
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	//*********************************LISTAR GESTION PASES CLUB********************************************
	@RequestMapping(value = "/ajax/venta/ventareserva/pases_club/list_pasesclub.do", method = RequestMethod.POST)
	public ResponseEntity<?> ventareservaLisPasesClub(
			@RequestParam(value = "dni_gestion_pases_club", required = false, defaultValue = "") String dniabonado,						 
			@RequestParam(value = "nombre_gestion_pases_club", required = false, defaultValue = "") String nombreabonado, 
			@RequestParam(value = "apellido1_gestion_pases_club", required = false, defaultValue = "") String apellido1,
			@RequestParam(value = "apellido2_gestion_pases_club", required = false, defaultValue = "") String apellido2, 
			@RequestParam(value = "idproducto_gestion_pases_club", required = false, defaultValue = "") String idProducto,			 
			@RequestParam(value = "fechafinvigenciadepasesclub", required = false, defaultValue = "") String fechaCaducidadIni,
			@RequestParam(value = "fechafinvigenciahastapasesclub", required = false, defaultValue = "") String fechaCaducidadFin, 
			@RequestParam(value = "fechainiciovigenciadepasesclub", required = false, defaultValue = "") String fechaEmisionIni,
			@RequestParam(value = "fechainiciovigenciahastapasesclub", required = false, defaultValue = "") String fechaEmisionFin,		
			@RequestParam(value = "start", required = false, defaultValue = "0") String startRecord,
      		@RequestParam(value = "length", required = false, defaultValue = "") String maxLength,
      		HttpServletRequest request) throws Exception {
		
		String xml="";
		xml = xml + "<Buscarpaseparam>";
	
		xml = xml+"<dniabonado>"+dniabonado+"</dniabonado>";
		xml = xml+"<nombreabonado>"+nombreabonado+"</nombreabonado>";
		xml = xml+"<apellido1>"+apellido1+"</apellido1>";
		xml = xml+"<apellido2>"+apellido2+"</apellido2>";
		xml = xml+"<idProducto>"+idProducto+"</idProducto>";
		xml = xml+"<fechaCaducidadIni>"+fechaCaducidadIni+"</fechaCaducidadIni>";
		xml = xml+"<fechaCaducidadFin>"+fechaCaducidadFin+"</fechaCaducidadFin>";
		xml = xml+"<idPerfil/>";
		xml = xml+"<fechaEmisionIni>"+fechaEmisionIni+"</fechaEmisionIni>";
		xml = xml+"<fechaEmisionFin>"+fechaEmisionFin+"</fechaEmisionFin>";
		//xml = xml+"<numeroprimerregistro>0</numeroprimerregistro>";
		//GGL NEW 
		if (dniabonado.equalsIgnoreCase("") && nombreabonado.equalsIgnoreCase("") && apellido1.equalsIgnoreCase("") && apellido2.equalsIgnoreCase("") && idProducto.equalsIgnoreCase("") &&
				fechaCaducidadIni.equalsIgnoreCase("") && fechaCaducidadFin.equalsIgnoreCase("") && fechaEmisionIni.equalsIgnoreCase("") && fechaEmisionFin.equalsIgnoreCase("") &&
				startRecord.equalsIgnoreCase("0") && maxLength.equalsIgnoreCase(""))
			xml = xml+"<todo>true</todo>";
		else xml = xml+"<todo>false</todo>";
		xml = xml+"<idabono/>";
		xml = xml+"<numeroprimerregistro>"+startRecord+"</numeroprimerregistro>";
		xml = xml+"<maxResultados>"+maxLength+"</maxResultados>";			

		xml = xml + "</Buscarpaseparam>";		
		
	
		String json = "";

		try {
			json = Tools.callServiceJSON(request, "buscarPases.action?servicio=buscarPases&xml="+xml);
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}

		HttpHeaders responseHeaders = new HttpHeaders(); 
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}
	
	// ****************************************NUEVO ABONO GESTION PASES CLUB******************************************************
	@RequestMapping("/ajax/venta/ventareserva/pases_club/new_abono.do")
	public ModelAndView ventareservaNewAbono(HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.addObject("newabono_selector_productosdisponibles", Tools.callServiceXML(request, "obtenerListadoProductosDisponiblesParaAbonoNoNumerados.action?servicio=obtenerListadoProductosDisponiblesParaAbonoNoNumerados"));
		model.addObject("newabono_selector_modelospase", Tools.callServiceXML(request, "obtenerListadoModelosPase.action?servicio=obtenerListadoModelosPaseNew"));
		model.addObject("newabono_selector_tiposidentificador", Tools.callServiceXML(request, "obtenerListadoTiposIdentificadorClubCiutat.action?servicio=obtenerListadoTiposIdentificadorClubCiutat"));
		//model.addObject("newabono_selector_sexos", Tools.callServiceXML(request, "obtenerListadoSexosClubCiutat.action?servicio=obtenerListadoSexosClubCiutat"));
		model.addObject("newabono_selector_sexos", "<ArrayList><CacSexo><codSexo>M</codSexo><descripcion>MUJER</descripcion></CacSexo><CacSexo><codSexo>V</codSexo><descripcion>VARON</descripcion></CacSexo><CacSexo><codSexo>9</codSexo><descripcion>SIN DETERMINAR</descripcion></CacSexo><CacSexo><codSexo>E</codSexo><descripcion>EMPRESA</descripcion></CacSexo></ArrayList>");
		//TODO GGL MARCIANADA, si lo quito peta la renderización de la jsp!. Parece que comentar un parse en una jsp no funciona, sigue intentando parsear el objeto del modelo. Lo he borrado y solucionado.
		//model.addObject("newabono_selector_tiposvia", Tools.callServiceXML(request, "obtenerListadoTiposViaClubCiutat.action?servicio=obtenerListadoTiposViaClubCiutat"));
		model.addObject("newabono_selector_provincias", Tools.callServiceXML(request, "obtenerListadoProvinciasClubCiutat.action?servicio=obtenerListadoProvinciasClubCiutat"));
		model.addObject("newabono_selector_parentesco", Tools.callServiceXML(request, "obtenerListadoParentescosClubCiutat.action?servicio=obtenerListadoParentescosClubCiutatNew"));
		
		model.setViewName("app.venta.ventareserva.new_abono");
		return model;
	}
	
	// *****************************VALIDAR IDENTIFICADOR CONTRA SIM***************************
	@RequestMapping("/ajax/venta/ventareserva/pases_club/validar_contra_sim.do")
	public ResponseEntity<String> seguridadSavePerfil(
													@RequestParam(value = "selector_producto_pases_bono", required = false, defaultValue = "") String producto,
												   @RequestParam(value = "selector_identificador_pases_bono", required = false, defaultValue = "") String tipoidentificador, 
												   @RequestParam(value = "identificador_pases_club", required = false, defaultValue = "") String identificador,
												   HttpServletRequest request) throws Exception {

		
		String xml = Tools.callServiceXML(request, "obtenerDatosClienteClubPorIdentificadorYProducto.action?servicio=obtenerDatosClienteClubPorIdentificadorYProducto&xml="+ URLEncoder.encode("<ParamsValidacionClienteClubDTO><idproducto>"+ producto +"</idproducto><idtipoidentificador>"+tipoidentificador+"</idtipoidentificador><identificador>"+identificador+"</identificador></ParamsValidacionClienteClubDTO>", "UTF-8"));												       

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}		
		
		JSONObject json = XML.toJSONObject(xml);
		if (json.has("error")) throw new Exception(json.getString("error"));
		
		json.append("iTotalRecords", "99999");
		json.append("iTotalDisplayRecords", "99999");
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		return new ResponseEntity<String>(json.toString(4), responseHeaders,HttpStatus.OK);
	}
	
	// *****************************VALIDAR IDENTIFICADOR CONTRA SIM***************************
		@RequestMapping("/ajax/venta/ventareserva/pases_club/validar_cliente.do")
		public ResponseEntity<String> validarCliente(
														@RequestParam(value = "selector_producto_pases_bono", required = false, defaultValue = "") String producto,
													   @RequestParam(value = "selector_identificador_pases_bono", required = false, defaultValue = "") String tipoidentificador, 
													   @RequestParam(value = "identificador_pases_club", required = false, defaultValue = "") String identificador,
													   HttpServletRequest request) throws Exception {

			
			String xml = Tools.callServiceXML(request, "validarClienteNew.action?servicio=validarClienteNew&xml="+ URLEncoder.encode("<ParamsValidacionClienteClubDTO><idproducto>"+ producto +"</idproducto><idtipoidentificador>"+tipoidentificador+"</idtipoidentificador><identificador>"+identificador+"</identificador></ParamsValidacionClienteClubDTO>", "UTF-8"));												       
			/*
			if (xml.startsWith("<error>")) {
				//xml=Tools.extractXMLError(xml);
				//return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
			} else xml="El cliente no existe, siga rellenando sus datos para darlo de alta.";
			*/
			//log.error("----------------------xml ------------------------"+xml);
			//GGL Ahora devolvera un booleano. Si es true no existe el cliente y hay que desbloquear los botones para seguir con el alta, si es false no.
			
			JSONObject json = XML.toJSONObject(xml);
			//log.error("----------------------json ------------------------"+json);
			if (json.has("error")) throw new Exception(json.getString("error"));
			
			//json.append("iTotalRecords", "99999");
			//json.append("iTotalDisplayRecords", "99999");
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
			return new ResponseEntity<String>(json.toString(4), responseHeaders,HttpStatus.OK);
		}

	//***********************LISTAR PERFILES A PARTIR DE TIPO DE PRODUCTO************************
	@RequestMapping(value = "/ajax/venta/ventareserva/pases_club/select_list_perfiles.do", method = RequestMethod.POST)
	public ResponseEntity<?> searchClientesListActividades(@RequestParam(value = "id", required = false, defaultValue = "") String id, HttpServletRequest request) throws Exception {
		
		String json = Tools.callServiceJSON(request, "obtenerListadoPerfilesAplicablesVentaAbonosNoNumerados.action?servicio=obtenerListadoPerfilesAplicablesVentaAbonosNoNumerados&idproducto="+id);
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}			
	
	//***********************LISTAR MUNICIPIOS PARTIR DE PROVINCIA************************
	@RequestMapping(value = "/ajax/venta/ventareserva/pases_club/select_list_municipios.do", method = RequestMethod.POST)
	public ResponseEntity<?> searchClientesListMunicipios(@RequestParam(value = "id", required = false, defaultValue = "") String id, HttpServletRequest request) throws Exception {
		String json = Tools.callServiceJSON(request, "obtenerListadoMunicipiosPorIdProvincia.action?servicio=obtenerListadoMunicipiosPorIdProvincia&xml=<int>"+id+"</int>");
													  	
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}			

	// *****************************************************************************************************
	@RequestMapping("/ajax/venta/ventareserva/pases_club/editar_grupo_abono.do")
	public ModelAndView ventareservaEditarGrupo(
			@RequestParam(value = "id", required = false, defaultValue = "") String idgrupo,
			HttpServletRequest request) throws Exception {
		
		ModelAndView model = new ModelAndView();
		String xml = Tools.callServiceXML(request, "obtenerPasesDtoSalidaDelGrupo.action?servicio=obtenerPasesDtoSalidaDelGrupoNew&xml="+URLEncoder.encode("<int>"+idgrupo+"</int>","UTF-8"));
		model.addObject("listadogrupo_pasesclub", xml);
		
        
		JSONObject json = XML.toJSONObject(xml);
		if (json.has("error")) throw new Exception(json.getString("error"));


		model.addObject("listadogrupo_pasesclub_json",json.toString(4));

		
		
		model.setViewName("app.venta.ventareserva.edit_grupo_abono");
		return model;
	
	}
		
		// *****************************************************************************************************
		@RequestMapping("/ajax/venta/ventareserva/pases_club/pantalla_impresion.do")
		public ModelAndView ventareservaEditarGrupoImprimir(
				@RequestParam(value = "xml", required = false, defaultValue = "") String xml,
				HttpServletRequest request) throws Exception {
			
			ModelAndView model = new ModelAndView();
			model.addObject("ventareserva_ruta_web", Tools.callServiceXML(request, "obtenerRutaImagenesClub.action?servicio=obtenerRutaImagenesClub"));
			
			model.setViewName("app.venta.ventareserva.edit_grupo_abono_imprimir");
			return model;
		
		}
		
		// *****************************************************************************************************
		@RequestMapping("/ajax/venta/ventareserva/pases_club/pantalla_imprimir_pase.do")
		public ModelAndView ventareservaEditarGrupoImprimirPase(				
				HttpServletRequest request) throws Exception {
			
			ModelAndView model = new ModelAndView();
			
			
			model.setViewName("app.venta.ventareserva.edit_grupo_abono_imprimir_pase");
			return model;
		
		}
		
		
		// *****************************************************************************************************
		@RequestMapping("/ajax/venta/ventareserva/pases_club/pantalla_renovacion.do")
		public ModelAndView ventareservaEditarGrupoImprimir(
				HttpServletRequest request) throws Exception {
			
			ModelAndView model = new ModelAndView();
			model.addObject("ventareserva_productos_disponibles", Tools.callServiceXML(request, "obtenerListadoProductosDisponiblesParaAbonoNoNumerados.action?servicio=obtenerListadoProductosDisponiblesParaAbonoNoNumerados"));
			
			model.setViewName("app.venta.ventareserva.edit_grupo_abono_renovar");
			return model;
		
		}
		
		
		// ****************************************NUEVO ABONO GESTION PASES CLUB**************SOLO VA PARA PASES****************************************
		@RequestMapping("/ajax/venta/ventareserva/pases_club/editar_pase.do")
		public ModelAndView ventareservaEditarPase(
				@RequestParam(value = "xml", required = false, defaultValue = "") String xml,				
				HttpServletRequest request) throws Exception {
			
			log.error("----------------------ventareservaEditarPase ------------------------");
			ModelAndView model = new ModelAndView();

			model.addObject("newabono_selector_productosdisponibles", Tools.callServiceXML(request, "obtenerListadoProductosDisponiblesParaAbonoNoNumerados.action?servicio=obtenerListadoProductosDisponiblesParaAbonoNoNumerados"));
			model.addObject("newabono_selector_modelospase", Tools.callServiceXML(request, "obtenerListadoModelosPase.action?servicio=obtenerListadoModelosPaseNew"));
			model.addObject("newabono_selector_tiposidentificador", Tools.callServiceXML(request, "obtenerListadoTiposIdentificadorClubCiutat.action?servicio=obtenerListadoTiposIdentificadorClubCiutat"));
			//model.addObject("newabono_selector_sexos", Tools.callServiceXML(request, "obtenerListadoSexosClubCiutat.action?servicio=obtenerListadoSexosClubCiutat"));
			model.addObject("newabono_selector_sexos", "<ArrayList><CacSexo><codSexo>M</codSexo><descripcion>MUJER</descripcion></CacSexo><CacSexo><codSexo>V</codSexo><descripcion>VARON</descripcion></CacSexo><CacSexo><codSexo>9</codSexo><descripcion>SIN DETERMINAR</descripcion></CacSexo><CacSexo><codSexo>E</codSexo><descripcion>EMPRESA</descripcion></CacSexo></ArrayList>");
			//model.addObject("newabono_selector_tiposvia", Tools.callServiceXML(request, "obtenerListadoTiposViaClubCiutat.action?servicio=obtenerListadoTiposViaClubCiutat"));
			model.addObject("newabono_selector_provincias", Tools.callServiceXML(request, "obtenerListadoProvinciasClubCiutat.action?servicio=obtenerListadoProvinciasClubCiutat"));
			model.addObject("newabono_selector_parentesco", Tools.callServiceXML(request, "obtenerListadoParentescosClubCiutat.action?servicio=obtenerListadoParentescosClubCiutatNew"));
			model.addObject("newabono_datos_usuario", Tools.callServiceJSON(request, "obtenerDatosClienteClub.action?servicio=obtenerDatosClienteClubNew&xml="+xml));
			
			model.setViewName("app.venta.ventareserva.edit_pase");
			return model;
		}
		
		
		
		// *****************************Devuelve listado de impresión***************************
		@RequestMapping("/ajax/venta/ventareserva/pases_club/listado_impresion.do")
		public ResponseEntity<String> listadoUsuariosImpresion(
				@RequestParam(value = "xml", required = false, defaultValue = "") String xml_inicial,
				HttpServletRequest request) throws Exception {

			
			String xml = Tools.callServiceXML(request, "obtenerDatosParaImpresionPases.action?servicio=obtenerDatosParaImpresionPasesNew&xml="+URLEncoder.encode(xml_inicial,"UTF-8"));												       

			if (xml.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
			}		
			
			JSONObject json = XML.toJSONObject(xml);
			if (json.has("error")) 
				throw new Exception(json.getString("error"));
			
			json.append("iTotalRecords", "99999");
			json.append("iTotalDisplayRecords", "99999");
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
			return new ResponseEntity<String>(json.toString(4), responseHeaders,HttpStatus.OK);		
			
			
		}	
		
		// *****************************Guarda un pase club***************************
		@RequestMapping("/ajax/venta/ventareserva/pases_club/guardar_pase.do")
		public ResponseEntity<String> ventareservaGuardarPase(
				@RequestParam(value = "xml", required = false, defaultValue = "") String xml_inicial,
				HttpServletRequest request) throws Exception {

			//log.error("ANTES DE LLAMADA A actualizarTarjetaPaseNew con "+URLEncoder.encode(xml_inicial,"UTF-8"));
			String xml = Tools.callServiceXML(request, "actualizarTarjetaPase.action?servicio=actualizarTarjetaPaseNew&xml="+URLEncoder.encode(xml_inicial,"UTF-8"));												       

			if (xml.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
			}		
			
			JSONObject json = XML.toJSONObject(xml);
			if (json.has("error")) 
				throw new Exception(json.getString("error"));
			
			json.append("iTotalRecords", "99999");
			json.append("iTotalDisplayRecords", "99999");
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
			return new ResponseEntity<String>(json.toString(4), responseHeaders,HttpStatus.OK);		
			
			
		}	
		
		
	
		
		
		
	// *****************************************************************************************************
		
		
		
		@RequestMapping("ajax/venta/ventareserva/show_disponibles_bono.do")
		public ModelAndView showDisponiblesBono(
				@RequestParam(value = "idproducto", required = false, defaultValue = "") String idProducto,
				@RequestParam(value = "idcliente", required = false, defaultValue = "") String idCliente,
				@RequestParam(value = "idtipoventa", required = false, defaultValue = "4") String idTipoVenta,
				@RequestParam(value = "producto", required = false, defaultValue = "") String producto,
				@RequestParam(value = "fecha", required = false, defaultValue = "") String fecha,
				@RequestParam(value = "button", required = false, defaultValue = "") String buttonAdd,
				@RequestParam(value = "list", required = false, defaultValue = "") String list,
				@RequestParam(value = "edicion", required = false, defaultValue = "0") String edicion,
				HttpServletRequest request) throws Exception {
		
			ModelAndView model = new ModelAndView();
			
			model.setViewName("app.venta.ventareserva.disponibles.bono");
			
			model.addObject("buttonAdd",buttonAdd);
			model.addObject("list",list);
			model.addObject("idProducto",idProducto);
			model.addObject("idCliente",idCliente);
			model.addObject("idtipoventa",idTipoVenta);
			model.addObject("producto",producto);
			model.addObject("fecha",fecha);
			model.addObject("edicion",edicion);

			String xml_sesiones= Tools.callServiceXML(request, "obtenerSesionesPorProductoYFecha.action?servicio=obtenerSesionesPorProductoYFecha&fecha="+URLEncoder.encode(fecha,"UTF-8")+"&idproducto="+idProducto+"&map="+URLEncoder.encode("<map />","UTF-8")); 
			JSONObject json_sesiones = XML.toJSONObject(xml_sesiones);
			if (json_sesiones.has("error")) throw new Exception(json_sesiones.getString("error"));

			model.addObject("disponibles_datos_sesiones",xml_sesiones);
			model.addObject("disponibles_datos_sesiones_json",json_sesiones.toString(4));

			return model;
		}

		//*****************************************************************************
		
		@RequestMapping("ajax/venta/ventareserva/show_vender_reserva.do")
		public ModelAndView showVenderReserva(
				@RequestParam(value = "idreserva", required = false, defaultValue = "") String idReserva,
				@RequestParam(value = "idcanal", required = false, defaultValue = "") String idcanal,
				HttpServletRequest request) throws Exception {
		
			ModelAndView model = new ModelAndView();

			
			String xml_reserva= Tools.callServiceXML(request, "obtenerVentaConReservaParaVender.action?servicio=obtenerVentaConReservaParaVender&xml=<Reserva><idreserva>"+URLEncoder.encode(idReserva,"UTF-8")+"</idreserva></Reserva>"); 
			JSONObject json = XML.toJSONObject(xml_reserva);
			if (json.has("error")) throw new Exception(json.getString("error"));
				model.addObject("json_reserva", json);
			
			
			
			model.setViewName("app.venta.ventareserva.venta_de_reserva");
			model.addObject("ventareserva_formaspago", Tools.callServiceXML(request, "obtenerListadoFormasPagoPorCanal.action?servicio=obtenerListadoFormasPagoPorCanal&xml="+URLEncoder.encode("<canal><idcanal>"+idcanal+"</idcanal></canal>","UTF-8")));
			model.addObject("ventareserva_selector_idiomas", Tools.callServiceXML(request, "obtenerListadoIdiomasFiltrado.action?servicio=obtenerListadoIdiomasFiltrado&selected=-1"));
			
			return model;
		}
		//************************************************************************************
		@RequestMapping("ajax/venta/ventareserva/realizar_venta_de_reserva.do")
		public ResponseEntity<?> ventareservaRealizarVentaDeReserva(
				@RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
				HttpServletRequest request) throws Exception {

		     String json="";
		       try {
	               		json = Tools.callServiceJSON(request, "/realizarVentaReserva.action?servicio=realizarVentaReserva&xml="+URLEncoder.encode(xml,"UTF-8"));
		       	   } catch (Exception ex) {
	               throw new AjaxException(ex.getMessage());
		        }		
			
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}
		// ***************************************************************************************************
		@RequestMapping("ajax/venta/ventareserva/show_resumen_venta_de_reserva.do")
		public ModelAndView showResumenVenta_de_Reserva(
				@RequestParam(value = "idventa", required = false, defaultValue = "") String idVenta,
				HttpServletRequest request) throws Exception {
			
		
			ModelAndView model = new ModelAndView();		
			model.setViewName("app.venta.ventareserva.venta_de_reserva_resumen");
			String ventareserva_resumen= Tools.callServiceXML(request, "obtenerVentaPorId.action?servicio=obtenerVentaPorId&xml="+URLEncoder.encode("<int>"+idVenta+"</int>","UTF-8")); 
			model.addObject("idVenta",idVenta);
			model.addObject("ventareserva_resumen", ventareserva_resumen);

			//Ponemos tambien el objeto en json
			JSONObject json = XML.toJSONObject(ventareserva_resumen);
			if (json.has("error")) throw new Exception(json.getString("error"));
				model.addObject("ventareserva_resumen_json", json);
			
			return model;
		}
		// ***************************************************************************************************
		@RequestMapping("/ajax/venta/ventareserva/imprimir_entradas.do")
		public ModelAndView ventareservaImprimirEntradas(
				@RequestParam(value = "idventa", required = false, defaultValue = "") String idVenta, 
				HttpServletRequest request) throws Exception {

			ModelAndView model = new ModelAndView();		
			model.setViewName("app.venta.ventareserva.venta.imprimir_entradas");
			String ventareserva_entradas= Tools.callServiceXML(request, "obtenerVentaPorIdSoloEntradas.action?servicio=obtenerVentaPorIdSoloEntradas&xml="+URLEncoder.encode("<int>"+idVenta+"</int>","UTF-8")); 
			model.addObject("idVenta",idVenta);
			model.addObject("ventareserva_entradas", ventareserva_entradas);

			JSONObject json = XML.toJSONObject(ventareserva_entradas);
			if (json.has("error")) throw new Exception(json.getString("error"));
				model.addObject("ventareserva_entradas_json", json);
			
			return model;			
		 }	
		
		
		
		// ***********************************************************************************
		@RequestMapping(value = "ajax/ventareserva/abono/establecerTitular.do", method = RequestMethod.POST)
		public ResponseEntity<?> abonoEstablecerTitular(
				@RequestParam(value = "idpase", required = false, defaultValue = "") String idpase,
				@RequestParam(value = "abono", required = false, defaultValue = "") String abono,
				HttpServletRequest request) throws Exception {
			HttpHeaders responseHeaders = new HttpHeaders();
			String respuesta="";
			try {
				respuesta = Tools.callServiceXML(request, "cambiarEstadoAbono.action?servicio=cambiarEstadoAbonoNew&idestado=T&seleccion=<list><IdentificadorPasesDto><idpase>"+idpase+"</idpase><esAbono>"+abono+"</esAbono></IdentificadorPasesDto></list>");
									
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}		
			
			if (respuesta.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
			}
			
			return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
		}	
		
		
		// ***********************************************************************************
			@RequestMapping(value = "ajax/ventareserva/abono/bloquearPases.do", method = RequestMethod.POST)
			public ResponseEntity<?> abonoBloquearPase(
					@RequestParam(value = "seleccion", required = false, defaultValue = "") String seleccion,
					HttpServletRequest request) throws Exception {
				HttpHeaders responseHeaders = new HttpHeaders();
				String respuesta="";
				try {
					respuesta = Tools.callServiceXML(request, "cambiarEstadoAbono.action?servicio=cambiarEstadoAbonoNew&idestado=B&seleccion="+seleccion);
										
				} catch (Exception ex) {
					throw new AjaxException(ex.getMessage());
				}		
				
				if (respuesta.startsWith("<error>")) {
					return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
				}
				
				return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
			}	
			
			// ***********************************************************************************
			@RequestMapping(value = "ajax/ventareserva/abono/cancelarPases.do", method = RequestMethod.POST)
			public ResponseEntity<?> abonoCancelarPase(
					@RequestParam(value = "seleccion", required = false, defaultValue = "") String seleccion,
					HttpServletRequest request) throws Exception {
				HttpHeaders responseHeaders = new HttpHeaders();
				String respuesta="";
				try {
					respuesta = Tools.callServiceXML(request, "cambiarEstadoAbono.action?servicio=cambiarEstadoAbonoNew&idestado=C&seleccion="+seleccion);
										
				} catch (Exception ex) {
					throw new AjaxException(ex.getMessage());
				}		
				
				if (respuesta.startsWith("<error>")) {
					return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
				}
				
				return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
			}
	
		
		//***************************************************************************************************
		@RequestMapping("/ajax/venta/ventareserva/lanzar_imprimir_entradas.do")
				public ResponseEntity<?> ventareservaLanzarImprimirEntradas(
						@RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
						HttpServletRequest request) throws Exception {

				     String json="";
				       try {
				    	   		
			               		json = Tools.callServiceJSON(request, "imprimirNumEntradasProductoVenta.action?servicio=imprimirNumEntradasProductoVenta&xml="+URLEncoder.encode(xml,"UTF-8"));
				    	   		//json= Tools.callServiceJSON(request, "imprimirNumEntradasProductoVenta.action?servicio=imprimirNumEntradasProductoVenta&xml="+URLEncoder.encode("<Venta><idventa>7211835</idventa><lineadetalles><Lineadetalle><idlineadetalle>24189954</idlineadetalle><cantidad>1</cantidad><anulada>0</anulada><importe>6.40</importe><importedescuentocliente>1.45</importedescuentocliente><lineadetallezonasesions><Lineadetallezonasesion><nombre/><zonasesion><numlibres>14832</numlibres><sesion><horainicio>10:00</horainicio><fecha>24/01/2019-00:00:00</fecha><contenido><nombre>Exposiciones Museu</nombre><tipoproducto><nombre>Museu de les Ciències</nombre></tipoproducto></contenido></sesion></zonasesion></Lineadetallezonasesion></lineadetallezonasesions><producto><idproducto>441</idproducto><nombre>MUSEO</nombre><iva><porcentaje>8.00</porcentaje></iva><imprimirEntradasXDefecto>1</imprimirEntradasXDefecto></producto><perfilvisitante><idperfilvisitante>20000</idperfilvisitante><nombre>Adulto</nombre></perfilvisitante><descuentopromocional/><bonosForIdlineadetalle/><tarifaproducto><importe>8.00</importe><idtarifaproducto>3694</idtarifaproducto></tarifaproducto><descuento><iddescuento>875</iddescuento><porcentajedescuento>20.00</porcentajedescuento></descuento><numimpresiones>0</numimpresiones><imprimir>1</imprimir></Lineadetalle></lineadetalles></Venta>","UTF-8"));
				       	   } catch (Exception ex) {
			               throw new AjaxException(ex.getMessage());
				        }		
					
					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}
		
		//***************************************************************************************************
		@RequestMapping("/ajax/venta/ventareserva/lanzar_reimprimir_entradas.do")
				public ResponseEntity<?> ventareservaLanzarReimprimirEntradas(
						@RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
						HttpServletRequest request) throws Exception {

				     String json="";
				       try {
			               		json = Tools.callServiceJSON(request, "reimprimirNumEntradasProductoVenta.action?servicio=reimprimirNumEntradasProductoVenta&xml="+URLEncoder.encode(xml,"UTF-8"));
				       	   } catch (Exception ex) {
			               throw new AjaxException(ex.getMessage());
				        }		
					
					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}
		
		
		
	//************************************************************************************
		
		@RequestMapping("/ajax/venta/ventareserva/imprimir_entradas_reserva.do")
		public ModelAndView ventareservaImprimirEntradasReserva(
				@RequestParam(value = "idreserva", required = false, defaultValue = "") String idReserva, 
				HttpServletRequest request) throws Exception {
			
			ModelAndView model = new ModelAndView();		
			model.setViewName("app.venta.ventareserva.venta.imprimir_entradas_reserva");
			String ventareserva_entradas= Tools.callServiceXML(request, "obtenerReservaPorIdSoloEntradas.action?servicio=obtenerReservaPorIdSoloEntradas&xml="+URLEncoder.encode("<int>"+idReserva+"</int>","UTF-8")); 
			model.addObject("idReserva",idReserva);
			model.addObject("ventareserva_entradas", ventareserva_entradas);

			JSONObject json = XML.toJSONObject(ventareserva_entradas);
			if (json.has("error")) throw new Exception(json.getString("error"));
				model.addObject("ventareserva_entradas_json", json);
			
			return model;			
		 }	
		
		
		
		// ***************************************************************************************************
		@RequestMapping("/ajax/venta/ventareserva/show_productos.do")
		public ModelAndView showProductos(HttpServletRequest request) throws Exception {
		ModelAndView model = new ModelAndView();		
		model.setViewName("app.venta.ventareserva.abonos.ventas");				
		return model;
		}
		
		
		//******************************************************
		
		@RequestMapping("/ajax/venta/ventareserva/list_productos_abono.do")
		public ResponseEntity<?> ventareservaListProductosAbono(
				@RequestParam(value = "titular", required = false, defaultValue = "") String titular,
				@RequestParam(value = "codParentesco", required = false, defaultValue = "") String codParentesco,
				@RequestParam(value = "deseaemail", required = false, defaultValue = "") String deseaemail,
				@RequestParam(value = "email", required = false, defaultValue = "") String email,
				@RequestParam(value = "deseamovil", required = false, defaultValue = "") String deseamovil,
				@RequestParam(value = "movil", required = false, defaultValue = "") String movil,
				@RequestParam(value = "fijo", required = false, defaultValue = "") String fijo,
				@RequestParam(value = "idprovincia", required = false, defaultValue = "0") String idprovincia,
				@RequestParam(value = "idmunicipio", required = false, defaultValue = "0") String idmunicipio,
				@RequestParam(value = "cp", required = false, defaultValue = "") String cp,
				@RequestParam(value = "complementovia", required = false, defaultValue = "") String complementovia,
				@RequestParam(value = "numvia", required = false, defaultValue = "") String numvia,
				@RequestParam(value = "via", required = false, defaultValue = "") String via,
				@RequestParam(value = "codTipoVia", required = false, defaultValue = "") String codTipoVia,
				@RequestParam(value = "fechanacimiento", required = false, defaultValue = "") String fechanacimiento,
				@RequestParam(value = "codSexo", required = false, defaultValue = "") String codSexo,
				@RequestParam(value = "apellido2", required = false, defaultValue = "") String apellido2,
				@RequestParam(value = "apellido1", required = false, defaultValue = "") String apellido1,
				@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
				@RequestParam(value = "idModelo", required = false, defaultValue = "1") String idModelo,
				@RequestParam(value = "nif", required = false, defaultValue = "") String nif,
				@RequestParam(value = "isLdGratuita", required = false, defaultValue = "") String isLdGratuita,
				@RequestParam(value = "idLineadetalle", required = false, defaultValue = "") String idLineadetalle,
				@RequestParam(value = "tipoLineadetalle", required = false, defaultValue = "") String tipoLineadetalle,
				@RequestParam(value = "idtipoventa", required = false, defaultValue = "") String idtipoventa,
				@RequestParam(value = "idtipoidentificador", required = false, defaultValue = "") String idtipoidentificador,
				@RequestParam(value = "idcanal", required = false, defaultValue = "") String idcanal,
				@RequestParam(value = "idperfilvisitante", required = false, defaultValue = "") String idperfilvisitante,
				@RequestParam(value = "idproducto", required = false, defaultValue = "") String idproducto,
				HttpServletRequest request) throws Exception {

		    //String json="";
		    
		    if(idprovincia.equalsIgnoreCase(""))
		    	idprovincia = "0";
		    
		    if(idmunicipio.equalsIgnoreCase(""))
		    	idmunicipio = "0";
		    
		    String xml="";
	        xml = "titular="+titular+"&"; 
	        xml+="codParentesco="+codParentesco+"&";
	        xml+="deseaemail="+deseaemail+"&";
	        xml+="email="+email+"&";
	        xml+="deseamovil="+deseamovil+"&";
	        xml+="movil="+movil+"&";
	        xml+="fijo="+fijo+"&";
	        xml+="idmunicipio="+idmunicipio+"&";
	        xml+="idprovincia="+idprovincia+"&";
	        xml+="cp="+cp+"&";
	        xml+="complementovia="+complementovia+"&";
	        xml+="numvia="+numvia+"&";
	        xml+="via="+via+"&";
	        xml+="codTipoVia="+codTipoVia+"&";
	        xml+="fechanacimiento="+fechanacimiento+"&";
	        xml+="codSexo="+codSexo+"&";
	        xml+="apellido2="+apellido2+"&";
	        xml+="apellido1="+apellido1+"&";
	        xml+="nombre="+nombre+"&";
	        xml+="idModelo="+idModelo+"&";
	        xml+="nif="+nif+"&";
	        xml+="isLdGratuita=false&";
	        xml+="idLineadetalle=&";
	        xml+="tipoLineadetalle=LineadetalleVentaPasesDTO&";
	        xml+="idtipoventa=5&";
	        xml+="idtipoidentificador="+idtipoidentificador+"&";
	        xml+="idcanal=2&";
	        xml+="idperfilvisitante="+idperfilvisitante+"&";
	        xml+="idproducto="+idproducto;
	        String xml_productos= Tools.callServiceXML(request, "obtenerProductoPorIdVentaReservaAbono.action?"+xml);
	        JSONObject json_productos = XML.toJSONObject(xml_productos);
	    	if (json_productos.has("error")) throw new Exception(json_productos.getString("error"));
	    	
	           
		   HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json_productos.toString(4), responseHeaders, HttpStatus.OK);
		}
		
		
		// ***************************************************************************************************
		@RequestMapping("ajax/venta/ventareserva/show_vender_grupo_abonos.do")
		public ModelAndView showVenderVentaGrupoAbonos(
				HttpServletRequest request) throws Exception {
		
			
			ModelAndView model = new ModelAndView();

			model.setViewName("app.venta.ventareserva.venta_grupo_abonos");
			model.addObject("ventareserva_formaspago", Tools.callServiceXML(request, "obtenerListadoFormasPagoPorCanal.action?servicio=obtenerListadoFormasPagoPorCanal&xml="+URLEncoder.encode("<canal><idcanal>2</idcanal></canal>","UTF-8")));
			model.addObject("ventareserva_selector_idiomas", Tools.callServiceXML(request, "obtenerListadoIdiomasFiltrado.action?servicio=obtenerListadoIdiomasFiltrado&selected=-1"));
			
			return model;
		}
		
		
		// ***************************************************************************************************
		@RequestMapping("ajax/venta/ventareserva/realizar_venta_abono.do")
		public ResponseEntity<?> ventareservaRealizarVentaAbono(
				@RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
				HttpServletRequest request) throws Exception {
			log.error("----------------------ventareservaRealizarVentaAbono ------------------------");
			log.error("----------------------ventareservaRealizarVentaAbono ------------------------" +xml);
		     //String xml_venta="";
		    String json ="";
	        try {
	    	   	json = Tools.callServiceJSON(request, "/realizarVentaPasesClub.action?servicio=realizarVentaPasesClubNew&xml="+URLEncoder.encode(xml,"UTF-8"));
	    	   	log.error("ventareservaRealizarVentaAbono RESULTADO  "+json);
	    	   	//JSONObject json = XML.toJSONObject(xml_venta);
	       	   } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }		
			
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}
		
		// ****************************************NUEVO MIEMBRO ABONO GESTION PASES CLUB******************************************************
		@RequestMapping("/ajax/venta/ventareserva/pases_club/new_abono_miembro.do")
		public ModelAndView ventareservaNewAbonoMiembro(HttpServletRequest request) throws Exception {

			ModelAndView model = new ModelAndView();

			model.addObject("newabono_selector_productosdisponibles", Tools.callServiceXML(request, "obtenerListadoProductosDisponiblesParaAbonoNoNumerados.action?servicio=obtenerListadoProductosDisponiblesParaAbonoNoNumerados"));
			model.addObject("newabono_selector_modelospase", Tools.callServiceXML(request, "obtenerListadoModelosPase.action?servicio=obtenerListadoModelosPaseNew"));
			model.addObject("newabono_selector_tiposidentificador", Tools.callServiceXML(request, "obtenerListadoTiposIdentificadorClubCiutat.action?servicio=obtenerListadoTiposIdentificadorClubCiutat"));
			//model.addObject("newabono_selector_sexos", Tools.callServiceXML(request, "obtenerListadoSexosClubCiutat.action?servicio=obtenerListadoSexosClubCiutat"));
			model.addObject("newabono_selector_sexos", "<ArrayList><CacSexo><codSexo>M</codSexo><descripcion>MUJER</descripcion></CacSexo><CacSexo><codSexo>V</codSexo><descripcion>VARON</descripcion></CacSexo><CacSexo><codSexo>9</codSexo><descripcion>SIN DETERMINAR</descripcion></CacSexo><CacSexo><codSexo>E</codSexo><descripcion>EMPRESA</descripcion></CacSexo></ArrayList>");
			model.addObject("newabono_selector_parentesco", Tools.callServiceXML(request, "obtenerListadoParentescosClubCiutat.action?servicio=obtenerListadoParentescosClubCiutatNew"));
			
			model.setViewName("app.venta.ventareserva.new_abono_miembro");
			return model;
		}
		
		
		@RequestMapping("/ajax/venta/ventareserva/list_lineas_detalles_abono.do")
		public ResponseEntity<?> ventareservaListLineasDetalleAbonos(
				@RequestParam(value = "idtipoventa", required = false, defaultValue = "") String idtipoventa,
				@RequestParam(value = "idproducto", required = false, defaultValue = "") String idproducto,
				@RequestParam(value = "idcanal", required = false, defaultValue = "") String idcanal,
				@RequestParam(value = "xml", required = false, defaultValue = "") String xml,
				HttpServletRequest request) throws Exception {
			/** Ejemplo de lo que llega en el param xml
			<list><PaseDtoSalida>
					<tipoPase>La Ciutat</tipoPase>
					<estado>CADUCADO</estado>
					<fechaAlta>23/11/2018-00:00:00</fechaAlta>
					<grupo>1203</grupo>
					<pasesAsociados>P628</pasesAsociados>
					<nif>26627744T</nif>
					<nombreTitular>MARIA CAROLINA MARTINEZ ANDION</nombreTitular> OJO LLEGA NOMBRETITULAR!!!!!
					<finVigencia>31/12/2018-00:00:00</finVigencia>
					<parentesco>SIN DETERMINAR</parentesco>
					<codigobarras>020181123142916439</codigobarras>
					<idtipoidentificador>0</idtipoidentificador>
					<idproducto>484</idproducto>
					<titular>1</titular>
					<identificador><esAbono>1</esAbono><idpase>628</idpase></identificador>
			</PaseDtoSalida></list>
			*/
			log.error("----------------------ventareservaListLineasDetalleAbonos ------------------------" +xml);

		    String parametros="idtipoventa="+idtipoventa+"&idproducto="+idproducto+"&idcanal="+idcanal+"&xml="+URLEncoder.encode(xml,"UTF-8");
		    
	        //TODO Como aqui no vienen los datos del titular, luego no los puedo meter en la LineadetalleVentaPasesDTO, por lo que no salen en el slide
		    //JSONArray jsonPasesDtoSalida = XML.toJSONObject(xml);
		    //JSONObject jsonGrupo = XML.toJSONObject(xml);
		    
		    String[] grupo = org.apache.commons.lang.StringUtils.substringsBetween(xml,"<grupo>","</grupo>");
		    
			String xmlTitular ="";
	        try {
	        	xmlTitular = Tools.callServiceXML(request, "/obtenerTitularGrupoPorIdgrupo.action?servicio=obtenerTitularGrupoPorIdgrupo&xml="+URLEncoder.encode("<int>"+grupo[0]+"</int>","UTF-8"));
	    	   	//log.error("obtenerTitularGrupoPorIdgrupo RESULTADO  "+xmlTitular.toString());
	       	   } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }
	        JSONObject jsonTitular = XML.toJSONObject(xmlTitular);
	    	if (jsonTitular.has("error")) throw new Exception(jsonTitular.getString("error"));
	        
	    	//log.error("----------------------ventareservaListLineasDetalleAbonos ------------------------" +jsonTitular.toString());   
		    
	        String xml_productos= Tools.callServiceXML(request, "obtenerLineasDetalleAbonos.action?servicio=obtenerLineasDetalleAbonos&"+parametros);
	        //JSONObject json = XML.parse(xml_productos,);
	        JSONObject json = XML.toJSONObject(xml_productos);
	    	if (json.has("error")) throw new Exception(json.getString("error"));
	    	//String jsonPrettyPrintString = json.toString(4);
	    	//log.error("----------------------ventareservaListLineasDetalleAbonos ------------------------" +json.toString(4)); 
	    	
	    	JSONArray jArray = json.getJSONObject("lineadetalles").getJSONArray("LineadetalleVentaPasesDTO");
	    	JSONObject lineaTitular = jArray.getJSONObject(0);
	    	lineaTitular.getJSONObject("datosClienteClub").putOnce("cp", jsonTitular.getJSONObject("DatosClienteClubDTO").get("cp").toString());
	    	lineaTitular.getJSONObject("datosClienteClub").putOnce("email", jsonTitular.getJSONObject("DatosClienteClubDTO").get("email").toString());
	    	lineaTitular.getJSONObject("datosClienteClub").putOnce("movil", jsonTitular.getJSONObject("DatosClienteClubDTO").get("movil").toString());
	    	/*
	    	json.getJSONObject("lineadetalles").getJSONObject("LineadetalleVentaPasesDTO").getJSONObject("datosClienteClub").putOnce("cp", jsonTitular.getJSONObject("DatosClienteClubDTO").get("cp").toString());
	    	json.getJSONObject("lineadetalles").getJSONObject("LineadetalleVentaPasesDTO").getJSONObject("datosClienteClub").putOnce("email", jsonTitular.getJSONObject("DatosClienteClubDTO").get("email").toString());
	    	json.getJSONObject("lineadetalles").getJSONObject("LineadetalleVentaPasesDTO").getJSONObject("datosClienteClub").putOnce("movil", jsonTitular.getJSONObject("DatosClienteClubDTO").get("movil").toString());
	    	*/
	    	log.error("----------------------ventareservaListLineasDetalleAbonos ------------------------" +json.toString()); 
	    	//log.error("----------------------ventareservaListLineasDetalleAbonos ------------------------" +json.getJSONObject("lineadetalles").getJSONObject("LineadetalleVentaPasesDTO").getJSONObject("datosClienteClub").get("nombrecompleto").toString());  
	    	
	    	HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json.toString(4), responseHeaders, HttpStatus.OK);
		}
		
		// ****************************************TOMAR FOTO WEBCAM******************************************************
		@RequestMapping("/ajax/venta/ventareserva/tomar_foto.do")
		public ModelAndView ventareservaTomarFoto(HttpServletRequest request) throws Exception {

			ModelAndView model = new ModelAndView();

				
			model.setViewName("app.venta.ventareserva.tomar_foto");
			return model;
		}
		
		// ***************************************************************************************************
		@RequestMapping("ajax/venta/ventareserva/savePhoto.do")
		public ResponseEntity<?> ventareservaSavePhoto(
				@RequestParam(value = "nombreFoto", required = false, defaultValue = "") String nombreFoto, 
				@RequestParam(value = "contenido", required = false, defaultValue = "") String contenido, 
				HttpServletRequest request) throws Exception {


			String base64Image = contenido.split(",")[1];
			byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);
			String xml_ruta = Tools.callServiceXML(request, "obtenerRutaImagenesClubFS.action?servicio=obtenerRutaImagenesClubFS");
			JSONObject json = XML.toJSONObject(xml_ruta);
			String path = json.getString("string") + nombreFoto;
			log.error("La ruta para guardar las imagenes que nos llega es: "+path);
	        File file = new File(path);
	        
	        OutputStream outputStream=null;
	        try {
	        	//TODO Cerrar el output????????????????????
	        	outputStream = new BufferedOutputStream(new FileOutputStream(file));
	            outputStream.write(imageBytes);
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	        	outputStream.close();
	        }
			
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json.toString(4), responseHeaders, HttpStatus.OK);
		}
		
		// ***********************************************************************************
		@RequestMapping(value = "ajax/ventareserva/abono/actualizarImpresion.do", method = RequestMethod.POST)
		public ResponseEntity<?> paseActualizarImpresion(
				@RequestParam(value = "xml", required = false, defaultValue = "") String xml,
				HttpServletRequest request) throws Exception {
			HttpHeaders responseHeaders = new HttpHeaders();
			String respuesta="";
			try {
				respuesta = Tools.callServiceXML(request, "actualizarImpresionPasesClubNew.action?servicio=actualizarImpresionPasesClubNew&xml="+xml);
									
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}		
			
			if (respuesta.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
			}
			
			return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
		}	
		
		// ***************************************************************************************************
		@RequestMapping("ajax/venta/ventareserva/list_paises.do")
		public ResponseEntity<?> ventareservaListPaises(
				@RequestParam(value = "strpais", required = false, defaultValue = "") String strpais, 
				HttpServletRequest request) throws Exception {

		    String json="";
		     

		       try {
		               json = Tools.callServiceJSON(request, "obtenerListadoPaisesPorNombre.action?nombre="+strpais);
		        } catch (Exception ex) {
		               throw new AjaxException(ex.getMessage());
		        }		

			
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}	
		
		
		// ***************************************************************************************************
		@RequestMapping("ajax/venta/ventareserva/show_multisesion.do")
		public ModelAndView showMultisesion(
				@RequestParam(value = "idproducto", required = false, defaultValue = "") String idProducto,
				@RequestParam(value = "idcliente", required = false, defaultValue = "") String idCliente,
				@RequestParam(value = "idtipoventa", required = false, defaultValue = "4") String idTipoVenta,
				@RequestParam(value = "producto", required = false, defaultValue = "") String producto,
				@RequestParam(value = "fecha", required = false, defaultValue = "") String fecha,
				@RequestParam(value = "button", required = false, defaultValue = "") String buttonAdd,
				@RequestParam(value = "list", required = false, defaultValue = "") String list,
				@RequestParam(value = "edicion", required = false, defaultValue = "0") String edicion,
				@RequestParam(value = "xml", required = false, defaultValue = "") String xml,
				HttpServletRequest request) throws Exception {
		
			ModelAndView model = new ModelAndView();
			
			

			model.setViewName("app.venta.ventareserva.multisesion");
			
			model.addObject("buttonAdd",buttonAdd);
			model.addObject("list",list);
			model.addObject("idProducto",idProducto);
			model.addObject("idCliente",idCliente);
			model.addObject("idtipoventa",idTipoVenta);
			model.addObject("producto",producto);
			model.addObject("fecha",fecha);
			model.addObject("edicion",edicion);

			String xml_sesiones= Tools.callServiceXML(request, "obtenerSesionesPorProductosYFechas.action?servicio=obtenerSesionesPorProductosYFechas&xml="+URLEncoder.encode(xml,"UTF-8")); 
			JSONObject json_sesiones = XML.toJSONObject(xml_sesiones);
			if (json_sesiones.has("error")) throw new Exception(json_sesiones.getString("error"));

			model.addObject("disponibles_datos_sesiones",xml_sesiones);
			model.addObject("disponibles_datos_sesiones_json",json_sesiones.toString(4));

			return model;
		}
		
		// ***************************************************************************************************
		@RequestMapping("ajax/venta/ventareserva/getDatosProducto.do")
		public ResponseEntity<?> ventareservaGetDatosProducto(
				@RequestParam(value = "idProducto", required = false, defaultValue = "") String idProducto, 
				HttpServletRequest request) throws Exception {

		     String json="";
		       try {
	               		json = Tools.callServiceJSON(request, "getEdicionAltaProducto.action?servicio=obtenerProductoPorIdNocaducadasConI18n&xml="+idProducto);
		       	   } catch (Exception ex) {
	               throw new AjaxException(ex.getMessage());
		        }		
			
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}
		
		// ***************************************************************************************************
		@RequestMapping("ajax/venta/ventareserva/imprimirEntradasVenta.do")
		public ResponseEntity<?> ventareservaImprimirEntradasVenta(
				@RequestParam(value = "xmlImpresion", required = false, defaultValue = "") String xml, 
				HttpServletRequest request) throws Exception {

		     String json="";
		       try {
	               		json = Tools.callServiceJSON(request, "imprimirEntradasVentaSinRespuesta.action?servicio=imprimirEntradasVentaSinRespuesta&xml="+xml);
		       	   } catch (Exception ex) {
	               throw new AjaxException(ex.getMessage());
		        }		
			
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}		
		
		// ***************************************************************************************************
				@RequestMapping("ajax/venta/ventareserva/imprimirBonosVenta.do")
				public ResponseEntity<?> ventareservaImprimirBonosVenta(
						@RequestParam(value = "xmlImpresion", required = false, defaultValue = "") String xml, 
						HttpServletRequest request) throws Exception {

				     String json="";
				       try {
			               		json = Tools.callServiceJSON(request, "imprimirBonosVenta.action?servicio=imprimirBonosVenta&xml="+xml);
				       	   } catch (Exception ex) {
			               throw new AjaxException(ex.getMessage());
				        }		
					
					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}

}

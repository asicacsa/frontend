package es.cac.colossus.frontend.web.controllers;

import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import es.cac.colossus.frontend.utils.AjaxException;
import es.cac.colossus.frontend.utils.Tools;

@Controller
public class EncuestasController {
	
	@Autowired
	private MessageSource messageSource;
	
	@RequestMapping("gestionencuestas/encuestas/mantenimientoencuestas.do")
	public ModelAndView showMantenimientoEncuestas(HttpServletRequest request) throws Exception {
		ModelAndView model = new ModelAndView();
		model.addObject("title", messageSource.getMessage("encuestas.mantenimientoencuestas.title", null, null));
		model.addObject("menu", Tools.getUserMainMenu(request));
		model.setViewName("app.gestionencuestas.encuestas.mantenimientoencuestas");
		return model;
	}
	//**********************************************************************************************************
	@RequestMapping("gestionencuestas/encuestas/responderencuestas.do")
	public ModelAndView showResponderEncuestas(HttpServletRequest request) throws Exception {
		ModelAndView model = new ModelAndView();
		model.addObject("title", messageSource.getMessage("encuestas.responderencuestas.title", null, null));
		model.addObject("menu", Tools.getUserMainMenu(request));
		model.setViewName("app.gestionencuestas.encuestas.responderencuestas");
		return model;
	}
	
	
	
	
	//****************************LISTAR ENCUESTAS***************************************************************
			@RequestMapping(value = "/ajax/gestionencuestas/encuestas/mantenimientoencuestas/list_encuestas.do", method = RequestMethod.POST)
			public ResponseEntity<?> gestionEncuestasListEncuestas(HttpServletRequest request) throws Exception {
			
				String json = "";

				try {
					json = Tools.callServiceJSON(request, "obtenerEncuestas.action");
				} catch (Exception ex) {
					throw new AjaxException(ex.getMessage());
				}

				HttpHeaders responseHeaders = new HttpHeaders();
				responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

				return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
			}
			// ***********************************NUEVO/EDITAR ENCUESTA***********************************
			@RequestMapping("/ajax/gestionencuestas/encuestas/mantenimientoencuestas/show_encuesta.do")
			public ModelAndView gestionEncuestasShowEncuesta(
					@RequestParam(value = "id", required = false, defaultValue = "") String idEncuesta,
					HttpServletRequest request) throws Exception {
				
				
				ModelAndView model = new ModelAndView();
				
				if (idEncuesta.length() > 0){
					String editar_encuesta =Tools.callServiceXML(request, "getEdicionAltaEncuesta.action?servicio=obtenerEncuesta&xml=" + idEncuesta);
					model.addObject("editar_encuesta", editar_encuesta);
					
					String traduccion_encuesta =Tools.callServiceXML(request, "obtenerTraduccionesEncuesta.action?servicio=obtenerTraduccionesEncuesta&xml=<long>"+idEncuesta+"</long>");
					model.addObject("traduccion_encuesta", traduccion_encuesta);					
				}
				else{
					model.addObject("editar_encuesta", "<empty/>");
					model.addObject("traduccion_encuesta", "<empty/>");				
				}				
				
				model.setViewName("app.gestionencuestas.encuestas.editar_encuesta");
				
				return model;
			}	
						
	//**********************************************************************************************************
			@RequestMapping("/ajax/gestionencuestas/encuestas/mantenimientoencuestas/show_pregunta.do")
			public ModelAndView encuestasShowPregunta(
					@RequestParam(value = "idpregunta", required = true, defaultValue = "") String idpregunta,
					@RequestParam(value = "idencuesta", required = true, defaultValue = "") String idencuesta, 
					HttpServletRequest request) throws Exception {

				ModelAndView model = new ModelAndView();
				
				System.out.println("IdPregunta:"+idpregunta);
				System.out.println("IdEncuesta:"+idencuesta);
				
				
				if (idpregunta.length() > 0)
					{
					model.addObject("crear", "0");
					model.addObject("editar_pregunta", Tools.callServiceXML(request, "getEdicionAltaPregunta.action?servicio=obtenerPregunta&xml="+URLEncoder.encode("<long>"+ idpregunta +"</long><long>1</long>", "UTF-8")));
					model.addObject("traduccion_pregunta", Tools.callServiceXML(request, "obtenerTraduccionesPregunta.action?servicio=obtenerTraduccionesPregunta&xml="+URLEncoder.encode("<long>"+ idpregunta +"</long>", "UTF-8")));
					}
				else
					{
					model.addObject("crear", "1");
					model.addObject("editar_pregunta", Tools.callServiceXML(request, "newXMLPreguntaEncuesta.action?servicio=newXMLPreguntaEncuesta&xml="+idencuesta));
					model.addObject("traduccion_pregunta", "<empty/>");		
					}
				
				model.setViewName("app.gestionencuestas.encuestas.dialog.edit_pregunta");
				model.addObject("idpregunta", idpregunta);
				return model;
			}
			
	
			//**********************************************************************************************************
			@RequestMapping("/ajax/gestionencuestas/encuestas/mantenimientoencuestas/search_pregunta.do")
			public ModelAndView encuestasSearchPregunta(
					HttpServletRequest request) throws Exception {
				ModelAndView model = new ModelAndView();
				model.addObject("search_pregunta", "<empty/>");		
				model.setViewName("app.gestionencuestas.encuestas.dialog.search_pregunta");
				return model;
			}
			
		//****************************LISTAR PREGUNTAS***************************************************************
		@RequestMapping(value = "/ajax/gestionencuestas/encuestas/mantenimientoencuestas/list_preguntas.do", method = RequestMethod.POST)
		public ResponseEntity<?> gestionEncuestasListPreguntas(
				@RequestParam(value = "texto_pregunta", required = false, defaultValue = "") String texto,
				@RequestParam(value = "cod_encuesta", required = false, defaultValue = "") String id,
				HttpServletRequest request) throws Exception {
			String json = "";

			try {
				json = Tools.callServiceJSON(request, "obtenerNuevasPreguntasEncuesta.action?servicio=obtenerNuevasPreguntasEncuesta&xml=<long>"+id+"</long>"+URLEncoder.encode("<filtro>"+texto, "UTF-8")+"</filtro>");
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}

			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}
		//******************************GUARDAR PREGUNTA***********************************************		
		@RequestMapping(value = "/ajax/gestionencuestas/encuestas/mantenimientoencuestas/save_pregunta.do", method = RequestMethod.POST)
		public ResponseEntity<?> gestionEncuestasSavePregunta(
				@RequestParam(value = "xml_datos_pregunta", required = true, defaultValue = "") String xml,
				@RequestParam(value = "id_pregunta", required = true, defaultValue = "0") String id_pregunta,
				HttpServletRequest request) throws Exception {
		
			String json = "";
			System.out.println("id_pregunta: "+ id_pregunta);
			try {
		
				if (id_pregunta.equalsIgnoreCase("0"))
					json = Tools.callServiceJSON(request, "postEdicionAltaEncuesta.action?_modo=postEdicionAltaEncuesta&servicio=postEdicionAltaEncuesta&xml="+URLEncoder.encode(xml, "UTF-8"));										
				else
					json = Tools.callServiceJSON(request, "postEdicionAltaPregunta.action?_modo=postEdicionAltaPregunta&servicio=postEdicionAltaPregunta&xml="+URLEncoder.encode(xml, "UTF-8"));
				System.out.println(json);	
				} catch (Exception ex) {
					throw new AjaxException(ex.getMessage());
				}

			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}
		//******************************GUARDAR ENCUESTA***********************************************		
		@RequestMapping(value = "/ajax/gestionencuestas/encuestas/mantenimientoencuestas/save_encuesta.do", method = RequestMethod.POST)
		public ResponseEntity<?> gestionEncuestasSaveEncuesta(
				@RequestParam(value = "codigo", required = true, defaultValue = "") String cod, 
				@RequestParam(value = "codigo_anterior", required = true, defaultValue = "") String codanterior,
				@RequestParam(value = "nombre", required = true, defaultValue = "") String des,
				@RequestParam(value = "lanzamiento", required = true, defaultValue = "") String fini,
				@RequestParam(value = "vencimiento", required = true, defaultValue = "") String ffin,					
				@RequestParam(value = "cbestado", required = true, defaultValue = "1") String baja,
				HttpServletRequest request) throws Exception {
			
			String json = "";
			String xml="";
			xml = "<getEdicionAltaEncuesta>";
			xml = xml + "<Encuesta>";
			xml = xml +"<cod>"+cod+"</cod>";
			xml = xml +"<codOld>"+codanterior+"</codOld>";
			xml = xml +"<dadoDeBaja>"+baja+"</dadoDeBaja>";
			xml = xml +"<des>"+des+"</des>";
			xml = xml +"<fecFin>"+ffin+"</fecFin>";
			xml = xml +"<fecIni>"+fini+"</fecIni>";
			xml = xml +"<preguntaEncuestas/>";
			xml = xml +"<sortedPreguntaEncuestas/>";
			xml = xml +"<traduccionEncuestas/>";
			xml = xml +"</Encuesta></getEdicionAltaEncuesta>";
			
		
			try {
				json = Tools.callServiceJSON(request, "postEdicionAltaEncuesta.action?_modo=postEdicionAltaEncuesta&servicio=postEdicionAltaEncuesta&xml="+URLEncoder.encode(xml, "UTF-8"));
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}

			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}
			//******************************CAMBIAR ESTADO ENCUESTA/DAR DE BAJA***************************************		
				@RequestMapping(value = "/ajax/gestionencuestas/encuestas/mantenimientoencuestas/remove_encuesta.do", method = RequestMethod.POST)
				public ResponseEntity<String> gestionEncuestasRemoveEncuesta(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
					String xmlList = "";
					Iterator<String> dataIterator = data.iterator();
					xmlList = "<list>";
					while (dataIterator.hasNext()) {
						xmlList += "<java.lang.Long>" + dataIterator.next() + "</java.lang.Long>";
					}
					xmlList = xmlList +"</list>";
					String xml = Tools.callServiceXML(request, "darDeBajaEncuestas.action?servicio=darDeBajaEncuestas&xml="+URLEncoder.encode(xmlList, "UTF-8"));

					if (xml.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
					}

					return new ResponseEntity<String>(HttpStatus.OK);
				}
				//*****************************CAMBIAR ESTADO PREGUNTA/ELIMINAR***************************************		
				@RequestMapping(value = "/ajax/gestionencuestas/encuestas/mantenimientoencuestas/remove_pregunta.do", method = RequestMethod.POST)
				public ResponseEntity<String> gestionEncuestasRemovePregunta(
						@RequestParam(value = "data", required = true) String xmlList,						
						HttpServletRequest request) throws Exception {
					String xml = Tools.callServiceXML(request, "darDeBajaPreguntas.action?servicio=darDeBajaPreguntas&xml="+URLEncoder.encode(xmlList, "UTF-8"));

					if (xml.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
					}

					return new ResponseEntity<String>(HttpStatus.OK);
				}
							
				//********************************************NUEVA/EDITAR RESPUESTA*************************
				@RequestMapping("/ajax/gestionencuestas/encuestas/mantenimientoencuestas/show_respuesta.do")
				public ModelAndView encuestasShowRespuesta(
						@RequestParam(value = "codtiporespuesta", required = true, defaultValue = "") String codtiporespuesta,
						@RequestParam(value = "destiporespuesta", required = true, defaultValue = "") String destiporespuesta,
						@RequestParam(value = "codvalorrespuesta", required = true, defaultValue = "") String codvalorrespuesta, 
						@RequestParam(value = "valorrespuesta", required = true, defaultValue = "") String valorrespuesta,
						HttpServletRequest request) throws Exception {

					ModelAndView model = new ModelAndView();
					
				
					
					if (destiporespuesta.length() == 0){
						model.addObject("crear_respuesta", Tools.callServiceXML(request, "getEdicionAltaValorRespuesta.action?servicio=nuevoValorRespuesta&xml="+URLEncoder.encode("<long>"+ codtiporespuesta +"</long><long>1</long>", "UTF-8")));
						model.addObject("traduccion_respuesta", "<empty/>");
					}else{
						model.addObject("crear_respuesta", Tools.callServiceXML(request, "getEdicionAltaValorRespuesta.action?servicio=obtenerValorRespuesta&xml="+URLEncoder.encode("<long>"+ codtiporespuesta +"</long><long>1</long><long>"+codvalorrespuesta+"</long>", "UTF-8")));
						String traduccion_respuesta =Tools.callServiceXML(request, "obtenerTraduccionesRespuesta.action?servicio=obtenerTraduccionesRespuesta&xml=<parametro><long>"+codtiporespuesta+"</long><long>"+codvalorrespuesta+"</long></parametro>");
						model.addObject("traduccion_respuesta", traduccion_respuesta);	
					}
					model.addObject("codtiporespuesta", codtiporespuesta);
					model.addObject("codvalorrespuesta", codvalorrespuesta);
					
					
					model.setViewName("app.gestionencuestas.encuestas.dialog.edit_respuesta");
					
					return model;
				}
				//*****************************ELIMINAR RESPUESTA***************************************		
				@RequestMapping(value = "/ajax/gestionencuestas/encuestas/mantenimientoencuestas/remove_respuesta.do", method = RequestMethod.POST)
				public ResponseEntity<String> gestionEncuestasRemoveRespuesta(
						@RequestParam(value = "xmllist_respuestas", required = true, defaultValue = "") String xmlList,
						HttpServletRequest request) throws Exception {
					String xml = Tools.callServiceXML(request, "darDeBajaRespuestas.action?servicio=darDeBajaRespuestas&xml="+URLEncoder.encode(xmlList, "UTF-8"));

					if (xml.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
					}

					return new ResponseEntity<String>(HttpStatus.OK);
				}
				//******************************GUARDAR RESPUESTA***********************************************		
				@RequestMapping(value = "/ajax/gestionencuestas/encuestas/mantenimientoencuestas/save_respuesta.do", method = RequestMethod.POST)
				public ResponseEntity<?> gestionEncuestasSaveRespuesta(
						@RequestParam(value = "xml_respuesta", required = true, defaultValue = "") String xml, 
						HttpServletRequest request) throws Exception {
												
					String json = "";
					try {
						json = Tools.callServiceJSON(request, "postEdicionAltaValorRespuesta.action?_modo=postEdicionAltaValorRespuesta&servicio=postEdicionAltaValorRespuesta&xml="+URLEncoder.encode(xml, "UTF-8"));
					} catch (Exception ex) {
						throw new AjaxException(ex.getMessage());
					}

					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}
				
				//******************************GUARDAR INTERNACIONALIZACION ENCUESTA****************************************		
				@RequestMapping(value = "/ajax/gestionencuestas/encuestas/mantenimientoencuestas/save_internacionalizacion_encuesta.do", method = RequestMethod.POST)
				public ResponseEntity<?> gestionEncuestasSaveInternacionalizacionEncuesta(
						@RequestParam(value = "data", required = true, defaultValue = "") String xml,
						@RequestParam(value = "codEncuesta", required = true, defaultValue = "") String codEncuesta, 
						HttpServletRequest request) throws Exception {
										
					
					
					
					String json = "";
					String codigo="";
					String texto="";
					String []traduccion;
					String []traducciones;
					String xml_traduccion="";
					
					traducciones = xml.split("\\^");
					
					
					
					for (int i=0; i< traducciones.length; i++)
					{
						traduccion = traducciones[i].split("\\~");
						codigo= traduccion[1];
					
						if (traduccion.length == 2)
							texto="";
						else
							texto= traduccion[2];
						
						
						xml_traduccion="<TraduccionEncuesta><codEncuesta>"+codEncuesta+"</codEncuesta><codIdioma>"+codigo+"</codIdioma><texto>"+texto+"</texto></TraduccionEncuesta>";
						
						if (texto!="")
						{
						try {
							json = Tools.callServiceJSON(request, "postEdicionAltaTraduccionEncuesta.action?_modo=guardarTraduccionEncuesta&servicio=guardarTraduccionEncuesta&xml="+URLEncoder.encode(xml_traduccion, "UTF-8"));
						} catch (Exception ex) {
							throw new AjaxException(ex.getMessage());
						}
						}
						
					}
								
					

					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}	
				
				
				//******************************GUARDAR INTERNACIONALIZACION ENCUESTA****************************************		
				@RequestMapping(value = "/ajax/gestionencuestas/encuestas/mantenimientoencuestas/save_internacionalizacion_respuesta.do", method = RequestMethod.POST)
				public ResponseEntity<?> gestionEncuestasSaveInternacionalizacionRespuesta(
						@RequestParam(value = "data", required = true, defaultValue = "") String xml,
						@RequestParam(value = "codValorResp", required = true, defaultValue = "") String codValorResp, 
						@RequestParam(value = "cod_tiporespuesta", required = true, defaultValue = "") String cod_tiporespuesta,
						HttpServletRequest request) throws Exception {
										
					
					
					
					String json = "";
					String codigo="";
					String texto="";
					String []traduccion;
					String []traducciones;
					String xml_traduccion="";
					
					traducciones = xml.split("\\^");
					
					
					
					for (int i=0; i< traducciones.length; i++)
					{
						traduccion = traducciones[i].split("\\~");
						codigo= traduccion[1];
					
						if (traduccion.length == 2)
							texto="";
						else
							texto= traduccion[2];
						
						xml_traduccion="<ValorRespuesta>";
						xml_traduccion+="<id>";
						xml_traduccion+="<codTipoResp>"+cod_tiporespuesta+"</codTipoResp>";
						xml_traduccion+="<codIdioma>"+codigo+"</codIdioma>";
						xml_traduccion+="<codValorResp>"+codValorResp+"</codValorResp>";
						xml_traduccion+="</id>";
						xml_traduccion+="<valorResp>"+texto+"</valorResp>";
						xml_traduccion+="</ValorRespuesta>";
						
						
						if (texto!="")
						{
						try {
							json = Tools.callServiceJSON(request, "postEdicionAltaTraduccionRespuesta.action?_modo=guardarTraduccionRespuesta&servicio=guardarTraduccionRespuesta&xml="+URLEncoder.encode(xml_traduccion, "UTF-8"));
						} catch (Exception ex) {
							throw new AjaxException(ex.getMessage());
						}
						}
						
					}
								
					

					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}					
				
				
				//******************************GUARDAR INTERNACIONALIZACION ENCUESTA****************************************		
				@RequestMapping(value = "/ajax/gestionencuestas/encuestas/mantenimientoencuestas/save_internacionalizacion_pregunta.do", method = RequestMethod.POST)
				public ResponseEntity<?> gestionEncuestasSaveInternacionalizacionPregunta(
						@RequestParam(value = "data", required = true, defaultValue = "") String xml,
						@RequestParam(value = "codPregunta", required = true, defaultValue = "") String codPregunta,
						@RequestParam(value = "idTipoPregunta", required = true, defaultValue = "") String idTipoPregunta,
						@RequestParam(value = "TipoPreguntaTexto", required = true, defaultValue = "") String TipoPreguntaTexto, 
						HttpServletRequest request) throws Exception {
										
					
					
					
					String json = "";
					String codigo="";
					String texto="";
					String []traduccion;
					String []traducciones;
					String xml_traduccion="";
					
					traducciones = xml.split("\\^");
					
					
					
					for (int i=0; i< traducciones.length; i++)
					{
						traduccion = traducciones[i].split("\\~");
						codigo= traduccion[1];
					
						if (traduccion.length == 2)
							texto="";
						else
							texto= traduccion[2];
											
						xml_traduccion="<Pregunta>";
						xml_traduccion+="<id>";
						xml_traduccion+="<codPregunta>"+codPregunta+"</codPregunta>";
						xml_traduccion+="<codIdioma>"+codigo+"</codIdioma>";
						xml_traduccion+="<codIdiomaOld/>";
						xml_traduccion+="</id>";
						xml_traduccion+="<des>"+texto+"</des>";
						xml_traduccion+="<tipoRespuesta>";
						xml_traduccion+="<cod>"+idTipoPregunta+"</cod>";
						xml_traduccion+="<des>"+TipoPreguntaTexto+"</des>";
						xml_traduccion+="</tipoRespuesta>";
						xml_traduccion+="</Pregunta>";
						
						
						if (texto!="")
						try {
							json = Tools.callServiceJSON(request, "postEdicionAltaTraduccionPregunta.action?_modo=guardarTraduccionPregunta&servicio=guardarTraduccionPregunta&xml="+URLEncoder.encode(xml_traduccion, "UTF-8"));
						} catch (Exception ex) {
							throw new AjaxException(ex.getMessage());
						}
						
						
					}
								
					

					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}	
				
				//***********************LISTAR ENCUESTAS POR IDIOMA*******************************************************
				@RequestMapping("/ajax/gestionencuestas/encuestas/encuestasIdioma.do")
				public ModelAndView showEncuestasPorIdioma(
							@RequestParam(value = "id", required = false, defaultValue = "") String idIdioma,
							HttpServletRequest request) throws Exception {
					ModelAndView model = new ModelAndView();
					
					
					model.addObject("encuestas", Tools.callServiceJSON(request, "obtenerEncuestasIdioma.action?servicio=obtenerEncuestasIdioma&xml="+idIdioma));
					model.addObject("idioma", idIdioma);
					
					model.setViewName("app.gestionencuestas.encuestas.encuestasidiomas");
					return model;
				}
				//************************************MOSTRAR PREGUNTAS ENCUESTA**********************************
				@RequestMapping("/ajax/gestionencuestas/encuestas/encuestasIdioma_show_preguntas.do")
				public ModelAndView encuestasShowPreguntas(
						@RequestParam(value = "idEncuesta", required = true, defaultValue = "") String codEncuesta,
						@RequestParam(value = "idIdioma", required = true, defaultValue = "") String codIdioma,
						HttpServletRequest request) throws Exception {

					ModelAndView model = new ModelAndView();
					
					String encuesta_xml =Tools.callServiceXML(request, "obtenerEncuestaSatisfaccion.action?servicio=obtenerEncuestaSatisfaccion&xml="+URLEncoder.encode("<long>"+ codEncuesta +"</long><long>"+codIdioma+"</long>", "UTF-8"));
					
					model.addObject("encuesta", encuesta_xml);
					
					JSONObject encuesta_json = XML.toJSONObject(encuesta_xml);
					if (encuesta_json.has("error")) throw new Exception(encuesta_json.getString("error"));
						model.addObject("encuesta_json", encuesta_json);
					
					model.setViewName("app.gestionencuestas.encuestas.dialog.show_encuesta");
					
					return model;
				}
				//***********************************GUARDAR RESPUESTAS ENCUESTA*******************************
				@RequestMapping(value = "/ajax/gestionencuestas/encuestas/save_contestacion_respuestas_encuesta.do", method = RequestMethod.POST)
				public ResponseEntity<?> gestionEncuestasGrabarContestacionEncuesta(
						@RequestParam(value = "xml_respuestas_encuesta", required = true, defaultValue = "") String xml_encuesta,						
						HttpServletRequest request) throws Exception {
					
					String xml = Tools.callServiceXML(request, "responderEncuestaSatisfaccion.action?xml=" + URLEncoder.encode(xml_encuesta, "UTF-8"));

					if (xml.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
					}

					return new ResponseEntity<String>(HttpStatus.OK);
				}	
				


				//******************************ASIGNAR PREGUNTA***********************************************		
				@RequestMapping(value = "/ajax/gestionencuestas/encuestas/mantenimientoencuestas/asignar_pregunta.do", method = RequestMethod.POST)
				public ResponseEntity<?> gestionEncuestasAsignarPregunta(
						@RequestParam(value = "xml_datos_pregunta", required = true, defaultValue = "") String xml,
						HttpServletRequest request) throws Exception {
				
					String json = "";
					try {						
							json = Tools.callServiceJSON(request, "asignarNuevasPreguntasEncuesta.action?servicio=asignarNuevasPreguntasEncuesta&xml="+URLEncoder.encode(xml, "UTF-8"));
						} catch (Exception ex) {
							throw new AjaxException(ex.getMessage());
						}

					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}
}




package es.cac.colossus.frontend.web.controllers;

import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import es.cac.colossus.frontend.utils.AjaxException;
import es.cac.colossus.frontend.utils.Tools;

@Controller
public class ApplicationController {
	
	private static final Log log = LogFactory.getLog(ApplicationController.class);
		
	//*****************************************************************************************************
	@RequestMapping("error404.do")
	public ModelAndView handleError404(HttpServletRequest request, Exception ex)
	{
    	ModelAndView model = new ModelAndView();
    	model.setViewName("app.error.404");
    	model.addObject("exception", ExceptionUtils.getStackTrace(ex).substring(0,500)+" ...");
		
    	log.error("Error 404 accediendo a: "+request.getAttribute("javax.servlet.error.request_uri"));
        return model;		
	}
		
	
	//*****************************************************************************************************
	@RequestMapping("login.do")
	public ModelAndView showLogin() throws Exception {
				
		ModelAndView model = new ModelAndView();
    	model.setViewName("app.login");
    					
    	return model;
	}
		

	//*****************************************************************************************************
	@RequestMapping("logout.do") 
	public ModelAndView logout(HttpServletRequest request) throws Exception {
	
		String idUsuario=request.getSession().getAttribute("idUsuario").toString();
		Tools.callServiceXML(request, "cerrarSesion.action?servicio=cerrarSesionesAbiertasUsuario&xml=<int>"+idUsuario+"</int>");

		ModelAndView model = new ModelAndView();
    	model.setViewName("app.login");
    			
    	return model;
	}	
	
	
	// *****************************************************************************************************
	@RequestMapping("ajax/change_password.do")
	public ResponseEntity<String> changePassword(@RequestParam(value = "old_password", required = false, defaultValue = "") String oldPassword, 
			@RequestParam(value = "new_password", required = false, defaultValue = "") String newPassword,
			@RequestParam(value = "repeat_password", required = false, defaultValue = "") String repeatPassword,
			HttpServletRequest request)
			throws Exception {

		String xml = Tools.callServiceXML(request, "cambiarContrasenyaUsuario.action?servicio=cambiarContrasenyaUsuario&confirmacioncontrasenyanueva="+repeatPassword+"&contrasenyanueva="+newPassword+"&contrasenyaactual="+oldPassword);	

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}		
		
		return new ResponseEntity<String>(HttpStatus.OK);
	}

	
	
	// *****************************************************************************************************
	@RequestMapping("ajax/show_buscadorcliente.do")
	public ModelAndView searchClientes(
			@RequestParam(value = "editable", required = false, defaultValue = "0") String editable,
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();
						
		model.addObject("buscarcliente_selector_categoriasactividad", Tools.callServiceXML(request, "obtenerListadoCategoriaActividad.action"));						
		
		model.addObject("buscarcliente_buscador", "<empty/>");
		model.addObject("editable", editable);

		model.setViewName("app.dialog.buscar_cliente");

		return model;
	}

	
	// *****************************************************************************************************
	@RequestMapping(value = "ajax/select_list_actividades.do", method = RequestMethod.POST)
	public ResponseEntity<?> searchClientesListActividades(@RequestParam(value = "id", required = false, defaultValue = "") String id, HttpServletRequest request) throws Exception {

		String json = Tools.callServiceJSON(request, "/obtenerListadoActividadesPorCategoria_BC.action?servicio=obtenerListadoActividadesPorCategoria&xml=<int>"+id+"</int>");

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	
	// *****************************************************************************************************
	@RequestMapping(value = "ajax/list_clientes.do", method = RequestMethod.POST)
	public ResponseEntity<?> searchClientesList(
			@RequestParam(value = "apellido2", required = false, defaultValue = "") String apellido2, 
			@RequestParam(value = "apellido1", required = false, defaultValue = "") String apellido1,
			@RequestParam(value = "nombre_razon", required = false, defaultValue = "") String nombre,
			@RequestParam(value = "codigo", required = false, defaultValue = "") String idcliente,
			@RequestParam(value = "cp", required = false, defaultValue = "") String codigoPostal,
			@RequestParam(value = "nombreContacto", required = false, defaultValue = "") String nombreContacto,
			@RequestParam(value = "poblacion", required = false, defaultValue = "") String localidad,
			@RequestParam(value = "direccion", required = false, defaultValue = "") String direccion,
			@RequestParam(value = "telefono", required = false, defaultValue = "") String telefono,
			@RequestParam(value = "fax", required = false, defaultValue = "") String fax,
			@RequestParam(value = "dni", required = false, defaultValue = "") String nifCif,
			@RequestParam(value = "principal", required = false, defaultValue = "") String principal,
			@RequestParam(value = "numeroprimerregistro", required = false, defaultValue = "") String numeroprimerregistro,
			@RequestParam(value = "localizadorAgencia", required = false, defaultValue = "") String localizadorAgencia,
			@RequestParam(value = "provincia", required = false, defaultValue = "") String provincia,
			@RequestParam(value = "pais", required = false, defaultValue = "") String pais,
			@RequestParam(value = "idactividad", required = false, defaultValue = "") String idactividad,
			@RequestParam(value = "idcategoria", required = false, defaultValue = "") String idcategoriacategoriaactividad,
			@RequestParam(value = "start", required = false, defaultValue = "0") String start,
			@RequestParam(value = "length", required = false, defaultValue = "") String length,
			HttpServletRequest request) throws Exception {
		
		
		String xml="";
		
		xml = "<BuscarClienteParam>";
		xml= xml + "<apellido2>"+apellido2+"</apellido2>";
		xml= xml + "<apellido1>"+apellido1+"</apellido1>";
		xml= xml + "<nombre>"+nombre+"</nombre>";				
		xml= xml + "<idcliente>"+idcliente+"</idcliente>";
		xml= xml + "<codigoPostal>"+codigoPostal+"</codigoPostal>";
		xml= xml + "<nombreContacto/>";
		xml= xml + "<localidad>"+localidad+"</localidad>";
		xml= xml + "<direccion>"+direccion+"</direccion>";
		xml= xml + "<telefono>"+telefono+"</telefono>";
		xml= xml + "<fax>"+fax+"</fax>";
		xml= xml + "<nifCif>"+nifCif+"</nifCif>";
		xml= xml + "<principal>"+principal+"</principal>";
		xml= xml + "<numeroprimerregistro>"+start+"</numeroprimerregistro>";
		xml= xml + "<maxResultados>"+length+"</maxResultados>";
		xml= xml + "<provincia>"+provincia+"</provincia>";
		xml= xml + "<pais>"+pais+"</pais>";
		xml= xml + "<idactividad>"+idactividad+"</idactividad>";
		xml= xml + "<idcategoriacategoriaactividad>"+idcategoriacategoriaactividad+"</idcategoriacategoriaactividad>";
		xml= xml + "</BuscarClienteParam>";
		String json = "";

		try {
			json = Tools.callServiceJSON(request, "buscarCliente.action?servicio=buscarCliente&xml=" + URLEncoder.encode(xml, "UTF-8"));					
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

}

// -----------------------------------------------------------------------
// TODO: 
// -----------------------------------------------------------------------

package es.cac.colossus.frontend.web.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class FileUploadController {
	
	//*****************************************************************************************************
	@RequestMapping(value="/ajax/documentacion/singleUpload.do", method=RequestMethod.POST )
	public @ResponseBody String singleSave(
			HttpServletRequest request,
			@RequestParam(value = "rutaweb") String rutaweb,
			@RequestParam("file") MultipartFile file)  {
    	String fileName = null;
    	JSONObject json = new JSONObject();
    	if (!file.isEmpty()) {
            try {
                fileName = file.getOriginalFilename();
                Date date = Calendar.getInstance().getTime();
                SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss") ;
                String[] splits = fileName.split("\\.");
                String extension = "";
                if(splits.length >= 2)
                {
                    extension = splits[splits.length-1];
                }                
                String fileSaved = dateFormat.format(date)+"."+extension;
                                byte[] bytes = file.getBytes();
                System.out.println("Ruta Web "+ rutaweb);
                BufferedOutputStream buffStream = 
                        new BufferedOutputStream(new FileOutputStream(new File(/*"D:"+*/ rutaweb + fileSaved)));
                buffStream.write(bytes);
                buffStream.close();
                json.put("name", fileName);
                json.put("saved", fileSaved);                
        		return json.toString(4);
            } catch (Exception e) {
            	System.out.println("Excepcion "+ e.toString());
            	return "Error al subir " + fileName + ": " + e.getMessage();
            }
        } else {
            return "No se puede subir. El fichero está vacío.";
        }

    }
    
	//*****************************************************************************************************
	@RequestMapping(value="/ajax/documentacion/multipleUpload.do", method=RequestMethod.POST )
    public @ResponseBody String multipleSave(
    		HttpServletRequest request,
			@RequestParam(value = "rutaweb") String rutaweb,
    		@RequestParam("file") MultipartFile[] files){
    	String fileName = null;
    	String msg = "";
    	if (files != null && files.length >0) {
    		for(int i =0 ;i< files.length; i++){
	            try {
	                fileName = files[i].getOriginalFilename();
	                byte[] bytes = files[i].getBytes();
	                BufferedOutputStream buffStream = 
	                        new BufferedOutputStream(new FileOutputStream(new File(/*"D:"+*/ rutaweb + fileName)));
	                buffStream.write(bytes);
	                buffStream.close();
	                msg += "Se ha subido correctamente " + fileName +"<br/>";
	            } catch (Exception e) {
	                return "Error al subir " + fileName + ": " + e.getMessage() +"<br/>";
	            }
    		}
    		return msg;
        } else {
            return "No se puede subir. El fichero está vacío.";
        }
    }
	

} 

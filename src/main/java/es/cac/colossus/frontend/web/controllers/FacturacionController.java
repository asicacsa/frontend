package es.cac.colossus.frontend.web.controllers;

import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import es.cac.colossus.frontend.utils.AjaxException;
import es.cac.colossus.frontend.utils.Tools;

@SuppressWarnings("unused")
@Controller
public class FacturacionController {

	@Autowired
	private MessageSource messageSource;
	private static final Log log = LogFactory.getLog(FacturacionController.class);

// **********************MOSTRAS PESTAÑAS FACTURACION******************************************************************

		@RequestMapping("facturacionclientes/facturacionclientes.do")
		public ModelAndView showFacturasInicial(HttpServletRequest request) throws Exception {
			return showFacturas(request);
		}
	
	
		@RequestMapping("facturacion/facturas/facturas.do")
		public ModelAndView showFacturas(HttpServletRequest request) throws Exception {
			ModelAndView model = new ModelAndView();
			model.addObject("tabgeneracionfacturas_selector_series", Tools.callServiceXML(request, "obtenerListadoTiposFacturaNoRectificativasActivas.action?servicio=obtenerListadoTiposFacturaNoRectificativasActivas"));
			String idUsuario=request.getSession().getAttribute("idUsuario").toString();
			
			model.addObject("title", messageSource.getMessage("facturacion.facturas.title", null, null));
			model.addObject("menu", Tools.getUserMainMenu(request));


			String url_impresion= Tools.callServiceXMLSinReemplazo(request, "abrirInformeUsuario.action?servicio=abrirInformeUsuario&xml=<java.lang.Integer>"+idUsuario+"</java.lang.Integer>"); 
			
			
			url_impresion = url_impresion.replace("<String>", "").replace("</String>", "");
						
			URL aURL = new URL(url_impresion);
			
			
			model.addObject("url_validacion_impresion",url_impresion);
			model.addObject("url_impresion",aURL.getAuthority());
			
			model.setViewName("app.facturacion.facturas.facturas");
			return model;
		}
		//****************************LISTAR CLIENTES***************************************************************
		@RequestMapping(value = "/ajax/facturacion/facturas/clientes/list_clientes.do", method = RequestMethod.POST)
		public ResponseEntity<?> facturacionListClientes(
				@RequestParam(value = "codigocliente", required = false, defaultValue = "") String codigocliente, 
				@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre, 
				@RequestParam(value = "apellido1", required = false, defaultValue = "") String apellido1, 
				@RequestParam(value = "apellido2", required = false, defaultValue = "") String apellido2,
				@RequestParam(value = "dni", required = false, defaultValue = "") String dni,
				@RequestParam(value = "telefono", required = false, defaultValue = "") String telefono, 
				@RequestParam(value = "fax", required = false, defaultValue = "") String fax, 
				@RequestParam(value = "direccion", required = false, defaultValue = "") String direccion, 
				@RequestParam(value = "localidad", required = false, defaultValue = "") String localidad,
				@RequestParam(value = "cp", required = false, defaultValue = "") String cp,
				@RequestParam(value = "prin", required = false, defaultValue = "0") String principal,
 			   @RequestParam(value = "start", required = false, defaultValue = "0") String startRecord,
      		   @RequestParam(value = "length", required = false, defaultValue = "") String maxLength,

				HttpServletRequest request) throws Exception {
			
			if(principal.startsWith("on"))
				principal = "1";		
			
			String xml="";
			xml = xml + "<BuscarClienteParam><apellido2>"+apellido2+"</apellido2>";
			xml = xml + "<apellido1>"+apellido1+"</apellido1>";
			xml = xml + "<nombre>"+nombre+"</nombre>";
			xml = xml + "<idcliente>"+codigocliente+"</idcliente>";
			xml = xml + "<codigoPostal>"+cp+"</codigoPostal>";
			xml = xml + "<nombreContacto></nombreContacto>";
			xml = xml + "<localidad>"+localidad+"</localidad>";
			xml = xml + "<direccion>"+localidad+"</direccion>";
			xml = xml + "<telefono>"+telefono+"</telefono>";
			xml = xml + "<fax>"+fax+"</fax>";
			xml = xml + "<nifCif>"+dni+"</nifCif>";
			xml = xml + "<principal>"+principal+"</principal>";
			xml = xml + "<numeroprimerregistro>"+startRecord+"</numeroprimerregistro>";
			xml = xml + "<maxResultados>"+maxLength+"</maxResultados>";
			xml = xml + "<localizadorAgencia/>";
			xml = xml + "<provincia/>";
			xml = xml + "<pais/>";
			xml = xml + "<idactividad/>";
			xml = xml + "<idcategoriacategoriaactividad/></BuscarClienteParam>";
			
			
			String json = "";

			try {
				json = Tools.callServiceJSON(request, "buscarCliente.action?servicio=buscarClienteFacturacion&xml="+URLEncoder.encode(xml,"UTF-8"));
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}

			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}
		// ***********************LISTAR TIPO CLIENTES******************************************************************
		@RequestMapping(value = "/ajax/facturacion/facturas/tiposcliente/list_tiposcliente.do", method = RequestMethod.POST)
		public ResponseEntity<?> facturacionListTiposCliente(HttpServletRequest request) throws Exception {
			
			String json = "";

			try {
				json = Tools.callServiceJSON(request, "obtenerTiposCliente.action?servicio=obtenerTiposCliente");
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}

			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}
		// ***********************LISTAR GRUPOS******************************************************************
		@RequestMapping(value = "/ajax/facturacion/facturas/grupos/list_grupos.do", method = RequestMethod.POST)
		public ResponseEntity<?> facturacionListGrupos(HttpServletRequest request) throws Exception {
		
			String json = "";

			try {
				json = Tools.callServiceJSON(request, "obtenerGruposEmpresas.action?servicio=obtenerGruposEmpresas");
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}
				HttpHeaders responseHeaders = new HttpHeaders();
				responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
				return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
			}
		// ***********************LISTAR GESTION FACTURAS************************************************************
				@RequestMapping(value = "/ajax/facturacion/facturas/gestionfacturas/list_gestionfacturas.do", method = RequestMethod.POST)
				public ResponseEntity<?> facturacionListGestionFacturas(
						@RequestParam(value = "codcliente1", required = false, defaultValue = "") String codigoClienteDesde, 
						@RequestParam(value = "codcliente2", required = false, defaultValue = "") String codigoClienteHasta, 
						@RequestParam(value = "fechainiciofacturacion", required = false, defaultValue = "") String fechaFacturacionDesde, 
						@RequestParam(value = "fechafinfacturacion", required = false, defaultValue = "") String fechaFacturacionHasta,
						@RequestParam(value = "fechainicioreserva", required = false, defaultValue = "") String fechaReservaDesde,
						@RequestParam(value = "fechafinreserva", required = false, defaultValue = "") String fechaReservaHasta, 
						@RequestParam(value = "fechainicioventa", required = false, defaultValue = "") String fechaVentaDesde, 
						@RequestParam(value = "fechafinventa", required = false, defaultValue = "") String fechaVentaHasta, 
						@RequestParam(value = "fechainicioenvio", required = false, defaultValue = "") String fechaEnvioDesde,
						@RequestParam(value = "fechafinenvio", required = false, defaultValue = "") String fechaEnvioHasta,
						@RequestParam(value = "numfactura1", required = false, defaultValue = "") String numFacturaDesde,
						@RequestParam(value = "numfactura2", required = false, defaultValue = "") String numFacturaHasta,
						@RequestParam(value = "rectificativas", required = false, defaultValue = "") String rectificativasDe,
						@RequestParam(value = "refventainicial", required = false, defaultValue = "") String refVentaDesde,
						@RequestParam(value = "refventafinal", required = false, defaultValue = "") String refVentaHasta,
						@RequestParam(value = "refreserva", required = false, defaultValue = "") String refReserva,
						@RequestParam(value = "tipo-emitidas", required = false, defaultValue = "") String sinEmitir,						
						@RequestParam(value = "tipo-validadas", required = false, defaultValue = "") String validada,
						@RequestParam(value = "tipo-enviadas", required = false, defaultValue = "") String enviada,
						@RequestParam(value = "tipo-facturas", required = false, defaultValue = "") String idTipoFactura,
						@RequestParam(value = "numeroprimerregistro", required = false, defaultValue = "") String numeroprimerregistro,
						@RequestParam(value = "seriefactura", required = false, defaultValue = "") String serie,
						@RequestParam(value = "localizador", required = false, defaultValue = "") String localizadoragencia,
						@RequestParam(value = "start", required = false, defaultValue = "0") String startRecord,
			      		@RequestParam(value = "length", required = false, defaultValue = "") String maxLength,
						
						HttpServletRequest request) throws Exception {
					
					switch (sinEmitir)
					{
					case "1":
						sinEmitir="";
						break;
					case "2":
						sinEmitir="false";
						break;
					case "3":
						sinEmitir="true";
						break;
					}
					
					switch (validada)
					{
					case "1":
						validada="";
						break;
					case "2":
						validada="false";
						break;
					case "3":
						validada="true";
						break;
					}
					
					switch (enviada)
					{
					case "1":
						enviada="";
						break;
					case "2":
						enviada="false";
						break;
					case "3":
						enviada="true";
						break;
					}
										
					String xml="";
					
					xml = xml + "<Buscarfacturaparam><codigoClienteDesde>"+codigoClienteDesde+"</codigoClienteDesde>";
					xml = xml + "<codigoClienteHasta>"+codigoClienteHasta+"</codigoClienteHasta>";
					xml = xml + "<fechaFacturacionDesde>"+fechaFacturacionDesde+"</fechaFacturacionDesde>";
					xml = xml + "<fechaFacturacionHasta>"+fechaFacturacionHasta+"</fechaFacturacionHasta>";
					xml = xml + "<fechaReservaDesde>"+fechaReservaDesde+"</fechaReservaDesde>";
					xml = xml + "<fechaReservaHasta>"+fechaReservaHasta+"</fechaReservaHasta>";
					xml = xml + "<fechaVentaDesde>"+fechaVentaDesde+"</fechaVentaDesde>";
					xml = xml + "<fechaVentaHasta>"+fechaVentaHasta+"</fechaVentaHasta>";
					xml = xml + "<fechaEnvioDesde>"+fechaEnvioDesde+"</fechaEnvioDesde>";
					xml = xml + "<fechaEnvioHasta>"+fechaEnvioHasta+"</fechaEnvioHasta>";
					xml = xml + "<numFacturaDesde>"+numFacturaDesde+"</numFacturaDesde>";
					xml = xml + "<numFacturaHasta>"+numFacturaHasta+"</numFacturaHasta>";
					xml = xml + "<rectificativasDe>"+rectificativasDe+"</rectificativasDe>";
					xml = xml + "<refVentaDesde>"+refVentaDesde+"</refVentaDesde>";
					xml = xml + "<refVentaHasta>"+refVentaHasta+"</refVentaHasta>";
					xml = xml + "<refReserva>"+refReserva+"</refReserva>";
					xml = xml + "<sinEmitir>"+sinEmitir+"</sinEmitir>";
					xml = xml + "<validada>"+validada+"</validada>";
					xml = xml + "<enviada>"+enviada+"</enviada>";
					xml = xml + "<idTipoFactura/>";
					xml = xml + "<numeroprimerregistro>"+startRecord+"</numeroprimerregistro>";
					xml = xml + "<maxResultados>"+maxLength+"</maxResultados>";
					xml = xml + "<serie>"+serie+"</serie>";
					xml = xml + "<localizadoragencia>"+localizadoragencia+"</localizadoragencia>";
					xml = xml + "</Buscarfacturaparam>";
					
					String json = "";

					try {
						json = Tools.callServiceJSON(request, "buscarFacturas.action?servicio=buscarFacturas&xml="+URLEncoder.encode(xml,"UTF-8"));
					} catch (Exception ex) {
						throw new AjaxException(ex.getMessage());
					}

					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}

				// ***********************LISTAR GENERACION FACTURAS************************************************************
				@RequestMapping(value = "/ajax/facturacion/facturas/generacionfacturas/list_generacionfacturas.do", method = RequestMethod.POST)
				public ResponseEntity<?> facturacionListGeneracionFacturas(
						@RequestParam(value = "idcliente", required = false, defaultValue = "") String idCliente,						 
						@RequestParam(value = "fechafinreservageneracion", required = false, defaultValue = "") String fechaReservaFin, 
						@RequestParam(value = "fechainicioreservageneracion", required = false, defaultValue = "") String fechaReservaIni,
						@RequestParam(value = "fechafinespectaculo", required = false, defaultValue = "") String fechaSesionFin, 
						@RequestParam(value = "fechainicioespectaculos", required = false, defaultValue = "") String fechaSesionIni,
						@RequestParam(value = "fechafinventageneracion", required = false, defaultValue = "") String fechaVentaFin, 
						@RequestParam(value = "fechainicioventageneracion", required = false, defaultValue = "") String fechaVentaIni,
						@RequestParam(value = "fechafincangegeneracion", required = false, defaultValue = "") String fechaCanjeFin, 
						@RequestParam(value = "fechacangeventageneracion", required = false, defaultValue = "") String fechaCanjeIni,
						@RequestParam(value = "tipo-ventas", required = false, defaultValue = "") String tipoVentas,						
						@RequestParam(value = "refreservageneracioninicial", required = false, defaultValue = "") String refReservaIni,
						@RequestParam(value = "refreservageneracionfinal", required = false, defaultValue = "") String refReservaFin,
						@RequestParam(value = "refventageneracioninicial", required = false, defaultValue = "") String refVentaIni,
						@RequestParam(value = "refventageneracionfinal", required = false, defaultValue = "") String refVentaFin,
						@RequestParam(value = "refreserva", required = false, defaultValue = "") String refReserva,
						@RequestParam(value = "soloprin", required = false, defaultValue = "false") String soloPrincipal,
						@RequestParam(value = "localizador", required = false, defaultValue = "") String localizadorAgencia,
						@RequestParam(value = "start", required = false, defaultValue = "0") String startRecord,
			      		@RequestParam(value = "length", required = false, defaultValue = "") String maxLength,
						HttpServletRequest request) throws Exception {
					
					
					
					if(soloPrincipal.startsWith("on"))
						soloPrincipal = "true";
					int tipoBusqueda=0;
					String buscarProductosYBonosAgencia="";
					String buscarBonoPrepago="";
					String bonos="";
					
					switch (tipoVentas)
					{
					case "1":
						
						buscarProductosYBonosAgencia="true";
						buscarBonoPrepago="false";
						bonos="false";
						tipoBusqueda=1;
						break;
					case "2":
						
						buscarProductosYBonosAgencia="false";
						buscarBonoPrepago="true";
						bonos="false";
						tipoBusqueda=1;
						break;
					case "3":
						
						buscarProductosYBonosAgencia="false";
						buscarBonoPrepago="false";
						bonos="true";
						tipoBusqueda=2;
						break;
					}
										
					String xml="";
					
					
			 if (tipoBusqueda==1){
				 	xml = "<Buscarventanofacturadaparam>";
					xml = xml + "<fechaReservaFin>"+fechaReservaFin+"</fechaReservaFin>";
					xml = xml + "<fechaReservaIni>"+fechaReservaIni+"</fechaReservaIni>";
					xml = xml + "<fechaSesionFin>"+fechaSesionFin+"</fechaSesionFin>";					
					xml = xml + "<fechaSesionIni>"+fechaSesionIni+"</fechaSesionIni>";					
					xml = xml + "<fechaVentaFin>"+fechaVentaFin+"</fechaVentaFin>";					
					xml = xml + "<fechaVentaIni>"+fechaVentaIni+"</fechaVentaIni>";					
					xml = xml + "<idcliente>"+idCliente+"</idcliente>";					
					xml = xml + "<idSucursal/>";
					xml = xml + "<refReservaFin>"+refReservaFin+"</refReservaFin>";					
					xml = xml + "<refReservaIni>"+refReservaIni+"</refReservaIni>";					
					xml = xml + "<refVentaFin>"+refVentaFin+"</refVentaFin>";					
					xml = xml + "<refVentaIni>"+refVentaIni+"</refVentaIni>";					
					xml = xml + "<soloPrincipal>"+soloPrincipal+"</soloPrincipal>"; //false
					xml = xml + "<idTipoFactura/>";					
					xml = xml + "<buscarProductosYBonosAgencia>"+buscarProductosYBonosAgencia+"</buscarProductosYBonosAgencia>";//true
					xml = xml + "<buscarBonoPrepago>"+buscarBonoPrepago+"</buscarBonoPrepago>";//false					
					xml = xml + "<numeroprimerregistro>"+startRecord+"</numeroprimerregistro>";
					xml = xml + "<maxResultados>"+maxLength+"</maxResultados>";
					xml = xml + "<localizadoragencia>"+localizadorAgencia+"</localizadoragencia>";				
					xml = xml + "</Buscarventanofacturadaparam>";	
			 } else
			 {
				 	xml = "<BuscarBonosCreditoNoFacturadosParam>";
				 	xml = xml + "<fechaSesionFin>"+fechaSesionFin+"</fechaSesionFin>";					
					xml = xml + "<fechaSesionIni>"+fechaSesionIni+"</fechaSesionIni>";					
					xml = xml + "<fechaCanjeFin>"+fechaCanjeFin+"</fechaCanjeFin>";					
					xml = xml + "<fechaCanjeIni>"+fechaCanjeIni+"</fechaCanjeIni>";					
					xml = xml + "<idcliente>"+idCliente+"</idcliente>";					
					xml = xml + "<idSucursal/>";
					xml = xml + "<refVentaFin>"+refVentaFin+"</refVentaFin>";					
					xml = xml + "<refVentaIni>"+refVentaIni+"</refVentaIni>";					
					xml = xml + "<soloPrincipal>"+soloPrincipal+"</soloPrincipal>"; //false
					xml = xml + "<idTipoFactura/>";					
					xml = xml + "<numeroprimerregistro>0</numeroprimerregistro>";
					xml = xml + "</BuscarBonosCreditoNoFacturadosParam>";
			}
			 					
					
					
					String json = "";

					try {
						 if (tipoBusqueda==1){
							 json = Tools.callServiceJSON(request, "buscarVentasNoFacturadas.action?servicio=buscarVentasNoFacturadas&xml="+xml);
						 }else
							 json = Tools.callServiceJSON(request, "buscarBonosCreditoNoFacturados.action?servicio=buscarBonosCreditoNoFacturados&xml="+xml);
					} catch (Exception ex) {
						throw new AjaxException(ex.getMessage());
					}

					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}

		//***********************ASOCIAR CLIENTE A VENTA**********************************************************
				@RequestMapping("/ajax/facturacion/facturas/generacionfacturas/asociarcliente.do")
				public ResponseEntity<String> facturacionAsociarClienteAVenta(
						@RequestParam(value = "data", required = true) List<String> data,
						@RequestParam(value = "idclienteasociado", required = false, defaultValue = "") String idclienteasociado, 
						HttpServletRequest request) throws Exception {
					String xmlList = "";
					Iterator<String> dataIterator = data.iterator();
					while (dataIterator.hasNext()) {
						xmlList += "<int>" + dataIterator.next() + "</int>";
					}

					String xml = Tools.callServiceXML(request, "modificarClienteVentas.action?servicio=modificarClienteVentas&xml=<parametro><list>" + xmlList + "</list><cliente><idcliente>"+idclienteasociado+"</idcliente></cliente></parametro>");
					
					if (xml.startsWith("<error>")) {					
						return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
					}					
					return new ResponseEntity<String>(HttpStatus.OK);
				}		
				
				// ***********************************ELIMINAR CLIENTE DE VENTA*********************************
				@RequestMapping("/ajax/facturacion/facturas/generacionfacturas/remove_clienteventa.do")
				public ResponseEntity<String> facturacionRemoveClienteVenta(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
					String xmlList = "";
					Iterator<String> dataIterator = data.iterator();
					while (dataIterator.hasNext()) {
						xmlList += "<int>" + dataIterator.next() + "</int>";
					}

					String xml = Tools.callServiceXML(request, "eliminarClienteVentas.action?servicio=eliminarClienteVentas&xml=<list>" + xmlList + "</list>");
					

					if (xml.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
					}

					return new ResponseEntity<String>(HttpStatus.OK);
				}
				// ***********************************GENERAR FACTURA*********************************
				@RequestMapping("/ajax/facturacion/facturas/generacionfacturas/generar_factura.do")
				public ResponseEntity<String> facturacionGenerarFactura(
						@RequestParam(value = "data", required = true) List<String> data, 
						@RequestParam(value = "idtipofactura", required = false, defaultValue = "") String idtipofactura, 
						HttpServletRequest request) throws Exception {
					String xmlList = "";
					Iterator<String> dataIterator = data.iterator();
					while (dataIterator.hasNext()) {
						xmlList += "<int>" + dataIterator.next() + "</int>";
					}
					
					String json;
					try {
						 json = Tools.callServiceJSON(request, "generarFactura.action?servicio=generarFactura&xml=<Generarfacturaventaparam><listaIdsVenta>" + xmlList + "</listaIdsVenta><idTipoFactura>"+idtipofactura+"</idTipoFactura></Generarfacturaventaparam>");
					} catch (Exception ex) {
						throw new AjaxException(ex.getMessage());
					}

					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}							
		// ***********************LISTAR SERIES******************************************************************
		@RequestMapping(value = "/ajax/facturacion/facturas/series/list_series.do", method = RequestMethod.POST)
		public ResponseEntity<?> facturacionListSeries(HttpServletRequest request) throws Exception {
				
			String json = "";
			try {
				json = Tools.callServiceJSON(request, "obtenerListadoTiposFactura.action?servicio=obtenerListadoTiposFactura");
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}

			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}
		// ***********************************NUEVO/EDITAR SERIE***********************************
		@RequestMapping("/ajax/facturacion/facturas/series/show_serie.do")
		public ModelAndView facturacionShowSerie(
				@RequestParam(value = "id", required = false, defaultValue = "") String idtipofactura,
				HttpServletRequest request) throws Exception {
			
			ModelAndView model = new ModelAndView();
			
			if (idtipofactura.length() > 0)
				model.addObject("editar_serie", Tools.callServiceXML(request, "getDarDeAltaTipoFactura.action?servicio=obtenerTipoFacturaPorId&xml=" + idtipofactura));																			
			else
				model.addObject("editar_serie", "<empty/>");								
			
			model.addObject("editar_serie_entidadgestora", Tools.callServiceXML(request, "obtenerListadoEntidadgestora.action?servicio=obtenerListadoEntidadgestora"));
			model.addObject("editar_serie_serierectificativa", Tools.callServiceXML(request, "obtenerListadoTiposFacturaRectificativa.action?servicio=obtenerListadoTiposFacturaRectificativa"));
			model.setViewName("app.facturacion.facturas.edit_series");
			
			return model;
		}
		//********************************************GUARDAR SERIE**************************************				
		@RequestMapping("/ajax/facturacion/facturas/series/save_serie.do")
		public ResponseEntity<String> facturacionSaveSerie(
				@RequestParam(value = "idtipofactura", required = false, defaultValue = "") String idtipofactura, 
				@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
				@RequestParam(value = "descripcion", required = false, defaultValue = "") String descripcion,
				@RequestParam(value = "identidadgestora", required = false, defaultValue = "") String identidadgestora,
				@RequestParam(value = "idserierectificativa", required = false, defaultValue = "") String idserierectificativa,
				@RequestParam(value = "serie", required = false, defaultValue = "") String serie,
				@RequestParam(value = "rectificativa", required = false, defaultValue = "0") String rectificativa,
				@RequestParam(value = "pordefecto", required = false, defaultValue = "0") String pordefecto,
				@RequestParam(value = "activa", required = false, defaultValue = "0") String activa,
				@RequestParam(value = "valor-actual", required = false, defaultValue = "") String numcontador,
				@RequestParam(value = "idtipofacturarectificativa", required = false, defaultValue = "") String idtipofacturarectificativa,
				
				HttpServletRequest request) throws Exception {
			
			String xmlCabecera="";
			String xml="";
			if ( (idtipofactura == null) || (idtipofactura.isEmpty()) ){
				xmlCabecera = "postDarDeAltaTipoFactura.action?_modo=alta&servicio=postDarDeAltaTipoFactura&xml=";
				xml = "<getDarDeAltaTipoFactura><Tipofactura>";
			}else		{
				xmlCabecera = "postDarDeAltaTipoFactura.action?_modo=edicion&servicio=postDarDeAltaTipoFactura&xml=";
				xml = "<getDarDeAltaTipoFactura><Tipofactura><idtipofactura>"+idtipofactura+"</idtipofactura>";
			}
			
			
			if(rectificativa.startsWith("on"))
				rectificativa = "1";
			if(pordefecto.startsWith("on"))
				pordefecto = "1";
			if(activa.startsWith("on"))
				activa = "1";			
			
			
			xml = xml +	"<entidadgestora><identidadgestora>"+identidadgestora+"</identidadgestora></entidadgestora>";
			xml = xml +	"<serie>"+serie+"</serie>";
			xml = xml +	"<nombre>"+nombre+"</nombre>";
			xml = xml +	"<descripcion>"+descripcion+"</descripcion>";
			xml = xml +	"<rectificativa>"+rectificativa+"</rectificativa>";
			xml = xml +	"<pordefecto>"+pordefecto+"</pordefecto>";
			xml = xml +	"<activa>"+activa+"</activa>";
			xml = xml +	"<numcontador>"+numcontador+"</numcontador>";
			xml = xml +	"<idtipofacturarectificativa>"+idserierectificativa+"</idtipofacturarectificativa>";
			
			xml = xml+"</Tipofactura></getDarDeAltaTipoFactura>";
			
			String xml_result = Tools.callServiceXML(request, xmlCabecera + URLEncoder.encode(xml, "UTF-8"));				
		
			if (xml_result.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
			}

			return new ResponseEntity<String>(HttpStatus.OK);
		}
		// ***********************************ELIMINAR SERIES*********************************
		@RequestMapping("/ajax/facturacion/facturas/series/remove_series.do")
		public ResponseEntity<String> facturacionRemoveSeries(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
			String xmlList = "";
			Iterator<String> dataIterator = data.iterator();
			while (dataIterator.hasNext()) {
				xmlList += "<int>" + dataIterator.next() + "</int>";
			}

			String xml = Tools.callServiceXML(request, "darDeBajaTipoFactura.action?servicio=darDeBajaTipoFactura&xml=<list>" + xmlList + "</list>");
			

			if (xml.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
			}

			return new ResponseEntity<String>(HttpStatus.OK);
		}		
		// ***********************LISTAR PERIODOS******************************************************************
		@RequestMapping(value = "/ajax/facturacion/facturas/periodos/list_periodos.do", method = RequestMethod.POST)
		public ResponseEntity<?> facturacionListPeriodos(HttpServletRequest request) throws Exception {
					
			String json = "";
			try {
				json = Tools.callServiceJSON(request, "obtenerPeriodosFacturacion.action?servicio=obtenerPeriodosFacturacion");
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}

			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}
		// ***********************************NUEVO/EDITAR PERIODO***********************************
				@RequestMapping("/ajax/facturacion/facturas/periodos/show_periodo.do")
				public ModelAndView facturacionShowPeriodo(
						@RequestParam(value = "id", required = false, defaultValue = "") String idPeriodoFacturacion,
						HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					
					if (idPeriodoFacturacion.length() > 0)
						model.addObject("editar_periodo", Tools.callServiceXML(request, "getAltaEdicionPeriodos.action?servicio=obtenerPeriodoFacturacionPorId&xml=" + idPeriodoFacturacion));																			
					else
						model.addObject("editar_periodo", "<empty/>");								
					
					model.addObject("editar_periodo_tipoperiodo", Tools.callServiceXML(request, "obtenerTiposPeriodoFacturacion.action?servicio=obtenerTiposPeriodoFacturacion"));
					model.setViewName("app.facturacion.facturas.edit_periodos");
					
					return model;
				}
		//********************************************GUARDAR PERIODO**************************************				
				@RequestMapping("/ajax/facturacion/facturas/periodos/save_periodo.do")
				public ResponseEntity<String> facturacionSavePeriodo(
						@RequestParam(value = "idperiodofacturacion", required = false, defaultValue = "") String idperiodofacturacion, 
						@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
						@RequestParam(value = "descripcion", required = false, defaultValue = "") String descripcion,
						@RequestParam(value = "numdias", required = false, defaultValue = "") String numdias,
						@RequestParam(value = "diasemanal", required = false, defaultValue = "") String diasemanal,
						@RequestParam(value = "numsemanas", required = false, defaultValue = "") String numsemanas,
						@RequestParam(value = "diamensual", required = false, defaultValue = "") String diamensual,
						@RequestParam(value = "mesmensual", required = false, defaultValue = "") String mesmensual,
						@RequestParam(value = "ordinaldiasemana", required = false, defaultValue = "") String ordinaldiasemana,
						@RequestParam(value = "diasemana", required = false, defaultValue = "") String diasemana,
						@RequestParam(value = "cadameses", required = false, defaultValue = "") String cadameses,
						@RequestParam(value = "dia", required = false, defaultValue = "") String dia,
						@RequestParam(value = "nummeses", required = false, defaultValue = "") String nummeses,
						@RequestParam(value = "periodicidad", required = false, defaultValue = "") String idtipoperiodofacturacion,
						
						HttpServletRequest request) throws Exception {
					
					String xmlCabecera="";
					if ( (idperiodofacturacion == null) || (idperiodofacturacion.isEmpty()) ) {
						xmlCabecera = "postAltaEdicionPeriodos.action?_modo=alta&servicio=postAltaEdicionPeriodos&xml=<getAltaEdicionPeriodos>";
						xmlCabecera = xmlCabecera +"<Periodofacturacion><idperiodofacturacion/>";
					}else
					{
						xmlCabecera = "postAltaEdicionPeriodos.action?_modo=edicion&servicio=postAltaEdicionPeriodos&xml=<getAltaEdicionPeriodos>";
						xmlCabecera = xmlCabecera +"<Periodofacturacion>";
						xmlCabecera = xmlCabecera + "<idperiodofacturacion>"+idperiodofacturacion+"</idperiodofacturacion>";
					}
					
					
					String xml="";	
					xml = xml +"<nombre>"+nombre+"</nombre>";
					xml = xml +"<descripcion>"+descripcion+"</descripcion>";
					xml = xml +"<numdias>"+numdias+"</numdias>";
					xml = xml +"<numsemanas>"+numsemanas+"</numsemanas>";
					if ((idtipoperiodofacturacion != null) && idtipoperiodofacturacion.equals("2"))
						xml = xml +"<diasemana>"+diasemanal+"</diasemana>";
					else if ((idtipoperiodofacturacion != null) && idtipoperiodofacturacion.equals("4"))						
						xml = xml +"<diasemana>"+diasemana+"</diasemana>";
					else						
						xml = xml +"<diasemana></diasemana>";
					
					xml = xml +"<dia>"+dia+"</dia>";
					xml = xml +"<nummeses>"+nummeses+"</nummeses>";
					xml = xml +"<diamensual>"+diamensual+"</diamensual>";
					xml = xml +"<mesmensual>"+mesmensual+"</mesmensual>";
					xml = xml +"<cadameses>"+cadameses+"</cadameses>";
					xml = xml +"<ordinaldiasemana>"+ordinaldiasemana+"</ordinaldiasemana>";
					xml = xml +"<tipoperiodofacturacion><idtipoperiodofacturacion>"+idtipoperiodofacturacion+"</idtipoperiodofacturacion></tipoperiodofacturacion>";
					
					
					xml = xml+"</Periodofacturacion></getAltaEdicionPeriodos>";
					
					
					String xml_result = Tools.callServiceXML(request, xmlCabecera + URLEncoder.encode(xml, "UTF-8"));				
				
					if (xml_result.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
					}

					return new ResponseEntity<String>(HttpStatus.OK);
				}
				// ***********************************ELIMINAR PERIODOS*********************************
				@RequestMapping("/ajax/facturacion/facturas/periodos/remove_periodos.do")
				public ResponseEntity<String> facturacionRemovePeriodos(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
					String xmlList = "";
					Iterator<String> dataIterator = data.iterator();
					while (dataIterator.hasNext()) {
						xmlList += "<int>" + dataIterator.next() + "</int>";
					}

					String xml = Tools.callServiceXML(request, "darDeBajaPeriodosFacturacion.action?servicio=darDeBajaPeriodosFacturacion&xml=<list>" + xmlList + "</list>");
					

					if (xml.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
					}

					return new ResponseEntity<String>(HttpStatus.OK);
				}				
		// ***********************LISTAR IVA******************************************************************
		@RequestMapping(value = "/ajax/facturacion/facturas/iva/list_iva.do", method = RequestMethod.POST)
		public ResponseEntity<?> facturacionListIva(HttpServletRequest request) throws Exception {
					
			String json = "";
			try {
				json = Tools.callServiceJSON(request, "obtenerListadoTiposIva.action?servicio=obtenerListadoTiposIva");
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}	
		// ***********************************NUEVO TIPO DE IVA***********************************
		@RequestMapping("/ajax/facturacion/facturas/iva/show_iva.do")
		public ModelAndView facturacionShowIVA(
				@RequestParam(value = "id", required = false, defaultValue = "") String idIVA,
				HttpServletRequest request) throws Exception {
			
			ModelAndView model = new ModelAndView();
			
			
			if (idIVA.length() > 0)
				model.addObject("editar_iva", Tools.callServiceXML(request, "getDarDeAltaTipoIva.action?servicio=obtenerTipoIvaPorId&xml=" + idIVA));																			
			else
				model.addObject("editar_iva", "<empty/>");
			
			
			
			model.setViewName("app.facturacion.facturas.edit_iva");
			
			return model;
		}
		//********************************************GUARDAR TIPO DE IVA**************************************				
		@RequestMapping("/ajax/facturacion/facturas/iva/save_iva.do")
		public ResponseEntity<String> facturacionSaveIVA(
				@RequestParam(value = "idiva", required = false, defaultValue = "") String idIVA, 
				@RequestParam(value = "nombreiva", required = false, defaultValue = "") String nombre,
				@RequestParam(value = "codigosap", required = false, defaultValue = "") String codigoSAP,
				@RequestParam(value = "porcentaje", required = false, defaultValue = "") String porcentaje,
		 		HttpServletRequest request) throws Exception {

			
			String xml = "<getDarDeAltaTipoIva><Iva>";
			xml = xml + "<idiva>"+idIVA+"</idiva>";
			xml = xml + "<nombre>"+nombre+"</nombre>";
			xml = xml + "<codigosap>"+codigoSAP+"</codigosap>";
			xml = xml + "<descripcion/>";
			xml = xml + "<porcentaje>"+porcentaje+"</porcentaje>";
			xml = xml + "<orden/>";
			xml = xml + "<dadodebaja>0</dadodebaja>";
			xml = xml + "</Iva></getDarDeAltaTipoIva>";
			
			String xml_result="";
			xml_result = Tools.callServiceXML(request, "postDarDeAltaTipoIva.action?_modo=edicion&servicio=postDarDeAltaTipoIva&xml=" + URLEncoder.encode(xml, "UTF-8"));				
		
			if (xml_result.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
			}

			return new ResponseEntity<String>(HttpStatus.OK);
		}
		// ***********************************ELIMINAR TIPOS DE IVA***********************************
				@RequestMapping("/ajax/facturacion/facturas/iva/remove_iva.do")
				public ResponseEntity<String> facturacionRemoveIVA(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
					String xmlList = "";
					Iterator<String> dataIterator = data.iterator();
					while (dataIterator.hasNext()) {
						xmlList += "<int>" + dataIterator.next() + "</int>";
					}

					String xml = Tools.callServiceXML(request, "darDeBajaTiposIva.action?servicio=darDeBajaTiposIva&xml=<list>" + xmlList + "</list>");

					if (xml.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
					}

					return new ResponseEntity<String>(HttpStatus.OK);
				}
		// ***********************LISTAR ROYALTIES***************************************************************
		@RequestMapping(value = "/ajax/facturacion/facturas/royalties/list_royalties.do", method = RequestMethod.POST)
		public ResponseEntity<?> facturacionListRoyalties(HttpServletRequest request) throws Exception {
					
			String json = "";

			try {
				json = Tools.callServiceJSON(request, "obtenerListadoMarcas.action?servicio=obtenerListadoMarcas");
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}

			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}
		// ***********************************NUEVO ROYALTY***********************************
		@RequestMapping("/ajax/facturacion/facturas/royalties/show_royalty.do")
		public ModelAndView facturacionShowRoyalty(
				@RequestParam(value = "id", required = false, defaultValue = "") String idMarca,
				HttpServletRequest request) throws Exception {
			
			ModelAndView model = new ModelAndView();	
			
			
			if (idMarca.length() > 0)
				model.addObject("editar_royalty", Tools.callServiceXML(request, "getDarDeAltaMarca.action?servicio=obtenerMarcaPorId&xml=" + idMarca));																			
			else
				model.addObject("editar_royalty", "<empty/>");			
			
			model.setViewName("app.facturacion.facturas.edit_royalty");
			
			return model;
		}	
		//********************************************GUARDAR ROYALTY**************************************	
		@RequestMapping("/ajax/facturacion/facturas/royalties/save_royalty.do")
		public ResponseEntity<String> facturacionSaveRoyalty(
				@RequestParam(value = "idroyalty", required = false, defaultValue = "0") String idmarca,
				@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre, 
				@RequestParam(value = "descripcion", required = false, defaultValue = "") String descripcion,
				@RequestParam(value = "fechacomienzocalculo", required = false, defaultValue = "") String fechacomienzocalculo,
				@RequestParam(value = "umbral", required = false, defaultValue = "") String umbral,
				@RequestParam(value = "porcentaje", required = false, defaultValue = "") String porcentaje,
				@RequestParam(value = "importe", required = false, defaultValue = "") String importe,
				@RequestParam(value = "tipocambio", required = false, defaultValue = "") String tipocambio,
				HttpServletRequest request) throws Exception {
				
				
			String xml="<getDarDeAltaMarca><Marca>";
			
			xml = xml + "<nombre>"+nombre+"</nombre>";
			if (idmarca.equals("0"))			
				xml = xml + "<idmarca/>";
			else
				xml = xml + "<idmarca>"+idmarca+"</idmarca>";
			xml = xml + "<descripcion>"+descripcion+"</descripcion>";
			xml = xml + "<umbralanualbase>"+umbral+"</umbralanualbase>";
			xml = xml + "<porcentajeroyalty>"+porcentaje+"</porcentajeroyalty>";
			xml = xml + "<royaltyminimo>"+importe+"</royaltyminimo>";
			xml = xml + "<tipocambiodolar>"+tipocambio+"</tipocambiodolar>";
			xml = xml + "<fechacomienzocalculoanual>"+fechacomienzocalculo+"</fechacomienzocalculoanual>";
			xml = xml + "<dadodebaja>0</dadodebaja>";
			xml = xml + "<contenidos/>";
			xml = xml + "</Marca></getDarDeAltaMarca>";
				
			
			String xml_result="";
			if (idmarca.equals("0"))
			{
				xml_result = Tools.callServiceXML(request, "postDarDeAltaMarca.action?_modo=alta&servicio=postDarDeAltaMarca&xml="+URLEncoder.encode(xml,"UTF-8"));	
				
			}else
			{
				xml_result = Tools.callServiceXML(request, "postDarDeAltaMarca.action?_modo=edicion&servicio=postDarDeAltaMarca&xml="+URLEncoder.encode(xml,"UTF-8"));
				
			}			
							
		
			if (xml_result.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
			}

			return new ResponseEntity<String>(HttpStatus.OK);
		
			
				}
				// ***********************************SHOW DISTRIBUIDORA ROYALTY***********************************
				@RequestMapping("/ajax/facturacion/facturas/royalties/show_distribuidoras_royalty.do")
				public ModelAndView facturacionShowDistribuidoras(HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					model.setViewName("app.facturacion.facturas.show_distribuidoras_royalty");
					return model;
				}	
				// ***********************LISTAR DISTRIBUIDORAS******************************************************************
				@RequestMapping(value = "/ajax/facturacion/facturas/royalties/list_distribuidoras_royalty.do", method = RequestMethod.POST)
				public ResponseEntity<?> facturacionListDistribuidorasRoyalty(HttpServletRequest request) throws Exception {
						
					String json = "";
					try {
						json = Tools.callServiceJSON(request, "obtenerDistribuidoras.action?servicio=obtenerDistribuidoras");
					} catch (Exception ex) {
						throw new AjaxException(ex.getMessage());
					}
					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}	
				// ***********************************NUEVA DISTRIBUIDORA ROYALTY**********************************
				@RequestMapping("/ajax/facturacion/facturas/royalties/new_distribuidora_royalty.do")
				public ModelAndView facturacionNewDistribuidoraRoyalty(HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					model.addObject("editar_royalty", "<empty/>");			
					model.setViewName("app.facturacion.facturas.new_distribuidora_royalty");
					
					return model;
		}	
				//********************************************GUARDAR DISTRIBUIDORA**************************************				
				@RequestMapping("/ajax/facturacion/facturas/royalties/save_distribuidora_royalty.do")
				public ResponseEntity<String> facturacionSaveDistribuidora(
						@RequestParam(value = "nombredistribuidora", required = false, defaultValue = "") String nombre, 
						@RequestParam(value = "descripciondistribuidora", required = false, defaultValue = "") String descripcion,
						
				 		HttpServletRequest request) throws Exception {

					
					String xml = "postDarDeAltaDistribuidora.action?_modo=<Distribuidora>";
					xml = xml + "<nombre>" +nombre +"</nombre>";
					xml = xml + "<descripcion>"+ descripcion +"</descripcion></Distribuidora>";
					xml=  xml + "&servicio=postDarDeAltaDistribuidora&xml=<getDarDeAltaDistribuidora><Distribuidora>";
					xml = xml + "<nombre>" + nombre +"</nombre>";
					xml = xml + "<descripcion>" + descripcion +"</descripcion></Distribuidora></getDarDeAltaDistribuidora>";
					
					String xml_result="";
					xml_result = Tools.callServiceXML(request, xml);				
				
					if (xml_result.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
					}

					return new ResponseEntity<String>(HttpStatus.OK);
				}
				// ***********************************ELIMINAR DISTRIBUIDORA ROYALTY*********************************
				@RequestMapping("/ajax/facturacion/facturas/rappels/remove_distribuidoras_royalty.do")
				public ResponseEntity<String> facturacionRemoveDistribuidorasRoyalty(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
					String xmlList = "";
					Iterator<String> dataIterator = data.iterator();
					while (dataIterator.hasNext()) {
						xmlList += "<int>" + dataIterator.next() + "</int>";
					}

					String xml = Tools.callServiceXML(request, "darDeBajaDistribuidora.action?servicio=darDeBajaDistribuidora&xml=<list>" + xmlList + "</list>");
					

					if (xml.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
					}

					return new ResponseEntity<String>(HttpStatus.OK);
				}		
		// ***********************************ELIMINAR ROYALTIES***********************************
				@RequestMapping("/ajax/facturacion/facturas/rappels/remove_royalties.do")
				public ResponseEntity<String> facturacionRemoveRoyalties(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
					String xmlList = "";
					Iterator<String> dataIterator = data.iterator();
					while (dataIterator.hasNext()) {
						xmlList += "<int>" + dataIterator.next() + "</int>";
					}

					String xml = Tools.callServiceXML(request, "darDeBajaMarcas.action?servicio=darDeBajaMarcas&xml=<list>" + xmlList + "</list>");

					if (xml.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
					}

					return new ResponseEntity<String>(HttpStatus.OK);
				}

		//***********************************LISTAR RAPPELS****************************************************
		@RequestMapping(value = "/ajax/facturacion/facturas/rappels/list_rappels.do", method = RequestMethod.POST)
		public ResponseEntity<?> facturacionListRappels(HttpServletRequest request) throws Exception {
					
			String json = "";

			try {
				json = Tools.callServiceJSON(request, "obtenerTiposRappels.action?servicio=obtenerTiposRappels");
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}

			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}
		// ***********************************ELIMINAR RAPPELS***********************************
		@RequestMapping("/ajax/facturacion/facturas/rappels/remove_rappels.do")
		public ResponseEntity<String> facturacionRemoveRappels(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
			String xmlList = "";
			Iterator<String> dataIterator = data.iterator();
			while (dataIterator.hasNext()) {
				xmlList += "<int>" + dataIterator.next() + "</int>";
			}

			String xml = Tools.callServiceXML(request, "darDeBajaTiposRappel.action?servicio=darDeBajaTiposRappel&xml=<list>" + xmlList + "</list>");

			if (xml.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
			}

			return new ResponseEntity<String>(HttpStatus.OK);
		}
		// ***********************************NUEVO RAPPEL***********************************
		@RequestMapping("/ajax/facturacion/facturas/rappels/show_rappel.do")
		public ModelAndView facturacionShowRappel(
				@RequestParam(value = "id", required = false, defaultValue = "") String idRappel,
				HttpServletRequest request) throws Exception {
			
			ModelAndView model = new ModelAndView();
			
			if (idRappel.length() > 0)
				model.addObject("editar_rappel", Tools.callServiceXML(request, "getDarDeAltaTipoRappel.action?servicio=obtenerTipoRappelPorId&xml=" + idRappel));																			
			else
				model.addObject("editar_rappel", "<empty/>");
			
			
			model.addObject("editar_rappel_unidades_negocio", Tools.callServiceXML(request, "obtenerUnidadesNegocioPorFuncionalidad.action?servicio=obtenerUnidadesNegocioPorFuncionalidad&xml=<int>53</int>"));
			
			
			model.setViewName("app.facturacion.facturas.edit_rappel");
			
			return model;
		}	
		
		// ***********************************NUEVO INTERVALO RAPPEL***********************************
				@RequestMapping("/ajax/facturacion/facturas/rappels/new_intervalo_rappel.do")
				public ModelAndView facturacionNewIntervaloRappel(HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					model.setViewName("app.facturacion.facturas.new_intervalo_rappel");
					
					return model;
				}	
		//********************************************GUARDAR RAPPEL**************************************				
				@RequestMapping("/ajax/facturacion/facturas/rappels/save_rappel.do")
				public ResponseEntity<String> seguridadSaveRappel(
						@RequestParam(value = "idtiporappel", required = false, defaultValue = "") String idtiporappel, 
						@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
						@RequestParam(value = "descripcion", required = false, defaultValue = "") String descripcion,
				 		@RequestParam(value = "criterio", required = false, defaultValue = "") String criterio,
				 		@RequestParam(value = "acumulativo", required = false, defaultValue = "false") String acumulativo,
				 		@RequestParam(value = "idunidadnegocio", required = false) String idunidadnegocio,
				 		@RequestParam(value = "intervalos_rappel", required = false, defaultValue = "")String intervalos,
			 			HttpServletRequest request) throws Exception {

					if(acumulativo.startsWith("on"))
						acumulativo = "true";
					
					String xml = "<getDarDeAltaTipoRappel><Tiporappel>";
					xml = xml + "<idtiporappel>"+idtiporappel+"</idtiporappel>";
					xml = xml + "<nombre>"+nombre+"</nombre>";
					xml = xml + "<descripcion>"+descripcion+"</descripcion>";
					xml = xml + "<condicionpagoultima/>";
					xml = xml + "<criteriorappel>"+criterio+"</criteriorappel>";
					xml = xml + "<acumulativo>"+acumulativo+"</acumulativo>";
					xml = xml + intervalos;
					xml = xml +  "<unidadnegocio><idunidadnegocio>"+idunidadnegocio+"</idunidadnegocio></unidadnegocio>";
					xml = xml +  "</Tiporappel></getDarDeAltaTipoRappel>";
					
					String xml_result="";
					if ( (idtiporappel == null) || (idtiporappel.isEmpty()) )
						xml_result = Tools.callServiceXML(request, "postDarDeAltaTipoRappel.action?_modo=alta&servicio=postDarDeAltaTipoRappel&xml=" + URLEncoder.encode(xml, "UTF-8"));
						else
							xml_result = Tools.callServiceXML(request, "postDarDeAltaTipoRappel.action?_modo=edicion&servicio=postDarDeAltaTipoRappel&xml=" + URLEncoder.encode(xml, "UTF-8"));

				
										
					if (xml_result.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
					}

					return new ResponseEntity<String>(HttpStatus.OK);
				}			
				// ***********************************VENTANA DEFINIR PERIODO DE CALCULO*******************************
				@RequestMapping("/ajax/facturacion/facturas/rappels/show_definir_periodo.do")
				public ModelAndView facturacionShowDefinirPeriodo(HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					model.addObject("editar_periodo_rappel", Tools.callServiceXML(request,"obtenerPeriodoCalculoRappels.action?servicio=obtenerPeriodoCalculoRappels"));
					model.setViewName("app.facturacion.facturas.show_definir_periodos_rappels");
					
					return model;
				}			
				//********************************************GUARDAR PERIODO RAPPEL**************************************				
				@RequestMapping("/ajax/facturacion/facturas/rappels/save_periodo_rappel.do")
				public ResponseEntity<String> facturacionSavePeriodoRappel(
						@RequestParam(value = "fechafinperiodo", required = false, defaultValue = "") String fechahasta, 
						@RequestParam(value = "fechainicioperiodo", required = false, defaultValue = "") String fechadesde,
						@RequestParam(value = "fechacalculo", required = false, defaultValue = "") String fechacalculo,
				 		HttpServletRequest request) throws Exception {

					
					String xml = "fechaHasta="+ fechahasta +"&fechaDesde="+ fechadesde +"&fechaCalculo="+ fechacalculo;
					
					String xml_result="";
					xml_result = Tools.callServiceXML(request, "actualizarPeriodoCalculoRappels.action?servicio=actualizarPeriodoCalculoRappels&" + xml);				
				
					if (xml_result.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
					}

					return new ResponseEntity<String>(HttpStatus.OK);
				}	
				// ***********************************VENTANA DEFINIR PERIODO DE CALCULO*******************************
				@RequestMapping("/ajax/facturacion/facturas/rappels/show_generar_factura.do")
				public ModelAndView facturacionShowGenerarFactura(HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					model.addObject("tipos_serie_rappel", Tools.callServiceXML(request,"obtenerListadoTiposFacturaActivas.action?servicio=obtenerListadoTiposFacturaRectificativa"));
					model.setViewName("app.facturacion.facturas.show_generar_facturas_rappels");
					
					return model;
				}
				//********************************************GUARDAR GENRACION FACTURA RAPPEL**************************************				
				@RequestMapping("/ajax/facturacion/facturas/rappels/save_generar_factura_rappel.do")
				public ResponseEntity<String> facturacionSaveGenerarFacturaRappel(
						@RequestParam(value = "idcliente_rappel", required = false, defaultValue = "") String idcliente,
						@RequestParam(value = "serie", required = false, defaultValue = "") String serie,
						@RequestParam(value = "fechainiciogeneracion", required = false, defaultValue = "") String fechainiciogeneracion,
						@RequestParam(value = "fechafingeneracion", required = false, defaultValue = "") String fechafingeneracion,
				 		HttpServletRequest request) throws Exception {

					
					String xml = "<Generarfacturarappelsparam><idcliente>"+ idcliente +"</idcliente>";
					xml = xml + "<fechainicio>" + fechainiciogeneracion+"</fechainicio>";
					xml = xml + "<fechafin>"+fechafingeneracion+"</fechafin>";
					xml= xml+"<idunidadesnegocio/>";
					xml=xml+"<identidadgestora/>";
					xml=xml+"<idtipofactura>" + serie + "</idtipofactura></Generarfacturarappelsparam>";
					
					String xml_result="";
					xml_result = Tools.callServiceXML(request, "generarRappels.action?servicio=generarRappels&xml="+xml);			
				
					if (xml_result.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
					}

					return new ResponseEntity<String>(HttpStatus.OK);
				}			
		//***********************************LISTAR MODELOS DE CORREO***************************************************
				@RequestMapping(value = "/ajax/facturacion/facturas/correo/list_modelosdecorreo.do", method = RequestMethod.POST)
				public ResponseEntity<?> facturacionListModelosCorreo(HttpServletRequest request) throws Exception {
						
					String json = "";

					try {
						json = Tools.callServiceJSON(request, "obtenerModelosCorreo.action?servicio=obtenerModelosCorreo");
					} catch (Exception ex) {
						throw new AjaxException(ex.getMessage());
					}

					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}
		//***********************************EDITAR MODELO DE CORREO***************************************************
		@RequestMapping(value = "/ajax/facturacion/facturas/correo/edit_modelodecorreo.do", method = RequestMethod.POST)
		public ResponseEntity<?> facturacionEditModeloCorreo(
				@RequestParam(value = "id", required = false, defaultValue = "") String idModelo,
				HttpServletRequest request) throws Exception {
				
			String json = "";

			try {
				json = Tools.callServiceJSON(request, "getDardeAltaModeloCorreo.action?servicio=obtenerModeloCorreoPorId&xml="+idModelo);
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}

			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}					

//*********************************ELIMINAR MODELO DE CORREO********************************************
		@RequestMapping("/ajax/facturacion/facturas/correo/remove_modelosdecorreo.do")
		public ResponseEntity<String> facturacionRemoveModeloCorreo(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
			String xmlList = "";
			Iterator<String> dataIterator = data.iterator();
			while (dataIterator.hasNext()) {
				xmlList += "<int>" + dataIterator.next() + "</int>";
			}

			String xml = Tools.callServiceXML(request, "FALTA SABER A QUE MÉTODO SE LLAMA *********** &xml=<list>" + xmlList + "</list>");

			if (xml.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
			}

			return new ResponseEntity<String>(HttpStatus.OK);
		}

		// *****************************************************************************************************
		@RequestMapping("ajax/facturacion/facturas/clientes/show_cliente.do")
		public ModelAndView facturacionShowCliente(
				@RequestParam(value = "id", required = false, defaultValue = "") String idCliente,
				@RequestParam(value = "tipo", required = false, defaultValue = "0") String tipo,
				HttpServletRequest request) throws Exception {
			
			ModelAndView model = new ModelAndView();
						
			if (idCliente.length() > 0)
				model.addObject("editar_cliente", Tools.callServiceXML(request, "getEdicionAltaClientesFacturacion.action?servicio=obtenerClientePorId&xml=" + idCliente));
			else
				model.addObject("editar_cliente", "<empty/>");
			
			
			model.addObject("editar_cliente_pais", Tools.callServiceXML(request, "obtenerListadoPaises.action"));
				
			model.addObject("editar_cliente_forma_pago", Tools.callServiceXML(request, "obtenerListadoFormasPagoVigentes.action?servicio=obtenerListadoFormasPagoVigentes"));
			model.addObject("tipo",tipo);
			model.addObject("editar_cliente_perodo_facturacion", Tools.callServiceXML(request, "obtenerPeriodosFacturacion.action?servicio=obtenerPeriodosFacturacion"));
			model.addObject("editar_cliente_periodo_facturacion_por_defecto", Tools.callServiceXML(request, "obtenerPeriodoFacturacionPorDefecto.action?servicio=obtenerPeriodoFacturacionPorDefecto"));
			model.addObject("editar_cliente_periodos_envio", Tools.callServiceXML(request, "obtenerPeriodosEnvio.action?servicio=obtenerPeriodosEnvio"));
			model.addObject("editar_cliente_periodos_envio_por_defecto", Tools.callServiceXML(request, "obtenerPeriodoEnvioPorDefecto.action?servicio=obtenerPeriodoEnvioPorDefecto"));
			model.addObject("editar_cliente_actividades", "<empty/>");
			model.addObject("editar_cliente_categorias_actividad", Tools.callServiceXML(request, "obtenerListadoCategoriaActividad.action?servicio=obtenerListadoCategoriaActividad"));
			model.addObject("editar_cliente_tipos_documento", Tools.callServiceXML(request, "obtenerListadoTiposDocumento.action?servicio=obtenerListadoTiposDocumento"));
			model.addObject("editar_cliente_tipos_cliente", Tools.callServiceXML(request, "obtenerListadoTiposCliente.action?servicio=obtenerListadoTiposCliente"));
			model.addObject("editar_cliente_tipos_rappel", Tools.callServiceXML(request, "obtenerListadoTiposRappel.action?servicio=obtenerListadoTiposRappel"));
			model.addObject("editar_cliente_canales", Tools.callServiceXML(request, "obtenerListadoCanales.action?servicio=obtenerListadoCanales"));
			model.addObject("editar_cliente_grupos_empresas", Tools.callServiceXML(request, "obtenerListadoGruposEmpresas.action?servicio=obtenerListadoGruposEmpresas"));
			model.addObject("editar_cliente_direcciones_envio", Tools.callServiceXML(request, "obtenerListadoDireccionesEnvio.action?servicio=obtenerListadoDireccionesEnvio"));
			model.setViewName("app.facturacion.facturas.edit_cliente");
			
			return model;
		}	
		
		// ***************************************************************************************************
		@RequestMapping(value = "ajax/facturacion/facturas/clientes/select_list_provincias.do", method = RequestMethod.POST)
		public ResponseEntity<?> clienteSelectListProvincias(
				@RequestParam(value = "idPais", required = false, defaultValue = "") String idPais,
				@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
				HttpServletRequest request) throws Exception {

			String json = Tools.callServiceJSON(request, "obtenerListadoProvinciasPorNombreYPais.action?idPais="+idPais+"&nombre="+nombre);
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}
		
		// ***************************************************************************************************
		@RequestMapping(value = "ajax/facturacion/facturas/clientes/select_list_poblaciones.do", method = RequestMethod.POST)
		public ResponseEntity<?> clienteSelectListPoblaciones(
				@RequestParam(value = "idPais", required = false, defaultValue = "") String idPais,
				@RequestParam(value = "idProvincia", required = false, defaultValue = "") String idProvincia,
				@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
				HttpServletRequest request) throws Exception {

			String json = Tools.callServiceJSON(request, "obtenerListadoMunicipiosPorNombreProvinciaYPais.action?idProvincia="+idProvincia+"&idPais="+idPais+"&nombre="+nombre);
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}	
		
		// ***************************************************************************************************
		@RequestMapping(value = "ajax/facturacion/facturas/clientes/select_list_CP.do", method = RequestMethod.POST)
		public ResponseEntity<?> clienteSelectListCP(
				@RequestParam(value = "idPais", required = false, defaultValue = "472") String idPais,
				@RequestParam(value = "idProvincia", required = false, defaultValue = "") String idProvincia,
				@RequestParam(value = "idMunicipio", required = false, defaultValue = "") String idMunicipio,
				@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
				HttpServletRequest request) throws Exception {

			String json = Tools.callServiceJSON(request, "buscarCPPorMunicipioProvinciaPais.action?idMunicipio="+idMunicipio+"&idProvincia="+idProvincia+"&idPais="+idPais+"&nombrecp="+nombre);
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}	
		
		// ***************************************************************************************************
		@RequestMapping(value = "ajax/facturacion/facturas/clientes/alta_CP.do", method = RequestMethod.POST)
		public ResponseEntity<?> darDeAltaCpMunicipio(
				@RequestParam(value = "new_poblacion", required = false, defaultValue = "") String idMunicipio,
				@RequestParam(value = "new_cp", required = false, defaultValue = "") String cp,
				HttpServletRequest request) throws Exception {

			String xml="<parametro><int>"+idMunicipio+"</int><cp>"+cp+"</cp></parametro>";
			String xmlResult = Tools.callServiceXML(request, "darDeAltaCpMunicipio.action?servicio=darDeAltaCpMunicipio&xml="+URLEncoder.encode(xml));
			
			if (xmlResult.indexOf("error")>0) {
				return new ResponseEntity<String>(Tools.extractXMLError(xmlResult), HttpStatus.BAD_REQUEST);
			}
			
			String json = XML.toJSONObject(xmlResult).toString(4);
			
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}	
		
		
		// *****************************************************************************************************
		@RequestMapping("ajax/facturacion/facturas/clientes/show_tipo_cliente.do")
		public ModelAndView facturacionShowTipoCliente(
				@RequestParam(value = "id", required = false, defaultValue = "") String idTipoCliente,
				HttpServletRequest request) throws Exception {
			
			ModelAndView model = new ModelAndView();

			
			if (idTipoCliente.length() > 0)
				model.addObject("editar_tipo_cliente", Tools.callServiceXML(request, "getDarDeAltaTipoCliente.action?servicio=obtenerTipoClientePorId&xml=" + idTipoCliente));
			else
				model.addObject("editar_tipo_cliente", "<empty/>");

			model.addObject("editar_tipo_cliente_selector_unidades", Tools.callServiceXML(request, "obtenerListadoUnidadesNegocioFiltrado.action?servicio=obtenerListadoUnidadesNegocioFiltrado"));
			
			model.setViewName("app.facturacion.facturas.edit_tipo_cliente");
			
			return model;
		}
	
		
		// *****************************************************************************************************
		@RequestMapping("ajax/facturacion/facturas/clientes/save_tipo_cliente.do")
		public ResponseEntity<String> facturacionSaveTipoCliente(
				@RequestParam(value = "idtipocliente", required = false, defaultValue = "") String idtipocliente,
				@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
				@RequestParam(value = "idunidadnegocio", required = false, defaultValue = "") String idunidadnegocio,
				@RequestParam(value = "tipo_contrato", required = false, defaultValue = "") String tipo_contrato,
				@RequestParam(value = "comisiones_asignados", required = false, defaultValue = "") String comisiones_asignados,
				@RequestParam(value = "descuentos_asignados", required = false, defaultValue = "") String descuentos_asignados,
				HttpServletRequest request) throws Exception 
		{
		String xml = "<getDarDeAltaTipoCliente><Tipocliente>";
		
		xml = xml + "<formapagotipoclientes/>";
		xml = xml + comisiones_asignados;
		xml = xml + descuentos_asignados;
		xml = xml + "<unidadnegocio><idunidadnegocio>"+idunidadnegocio+"</idunidadnegocio></unidadnegocio>";
		xml = xml +"<condicionpagoultima><credito>"+tipo_contrato+"</credito></condicionpagoultima>";
		xml = xml +  "<idtipocliente>"+idtipocliente+"</idtipocliente><nombre>"+nombre+"</nombre>";
		xml = xml +  "</Tipocliente></getDarDeAltaTipoCliente>";
		
		String xml_result;
		if(idtipocliente.equalsIgnoreCase(""))
			xml_result = Tools.callServiceXML(request, "postDarDeAltaTipoCliente.action?_modo=alta&servicio=postDarDeAltaTipoCliente&xml=" + URLEncoder.encode(xml, "UTF-8"));
		else
			xml_result = Tools.callServiceXML(request, "postDarDeAltaTipoCliente.action?_modo=edicion&servicio=postDarDeAltaTipoCliente&xml=" + URLEncoder.encode(xml, "UTF-8"));
		
		if (xml_result.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
		}	
	
		return new ResponseEntity<String>(HttpStatus.OK);

		}

		// ***********************************ELIMINAR RAPPELS***********************************
		@RequestMapping("/ajax/facturacion/facturas/tipoCliente/remove_tipoCliente.do")
		public ResponseEntity<String> facturacionRemoveTipoCliente(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
			String xmlList = "";
			Iterator<String> dataIterator = data.iterator();
			while (dataIterator.hasNext()) {
				xmlList += "<int>" + dataIterator.next() + "</int>";
			}

			String xml = Tools.callServiceXML(request, "darDeBajaTiposCliente.action?servicio=darDeBajaTiposCliente&xml=<list>" + xmlList + "</list>");

			if (xml.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
			}

			return new ResponseEntity<String>(HttpStatus.OK);
		}

		
		// *****************************************************************************************************
		@RequestMapping("ajax/facturacion/facturas/grupos/show_grupo.do")
		public ModelAndView facturacionShowGrupo(
				@RequestParam(value = "id", required = false, defaultValue = "") String idgrupoempresas,
				HttpServletRequest request) throws Exception {
			
			ModelAndView model = new ModelAndView();

			
			if (idgrupoempresas.length() > 0)
				model.addObject("editar_grupo", Tools.callServiceXML(request, "getDarDeAltaGrupoEmpresas.action?servicio=obtenerGrupoEmpresasPorId&xml=" + idgrupoempresas));
			else
				model.addObject("editar_grupo", "<empty/>");

			model.setViewName("app.facturacion.facturas.edit_grupo");
			
			return model;
		}
		
		// *****************************************************************************************************
		@RequestMapping("ajax/facturacion/facturas/grupos/show_rappel_grupo.do")
		public ModelAndView facturacionShowRappelGrupo(
				@RequestParam(value = "creando", required = false, defaultValue = "") String creando,
				@RequestParam(value = "id", required = false, defaultValue = "") String id,
				@RequestParam(value = "idRappel", required = false, defaultValue = "") String idRappel,
				@RequestParam(value = "fechainicio", required = false, defaultValue = "") String fechainicio,
				@RequestParam(value = "fechafin", required = false, defaultValue = "") String fechafin,
				HttpServletRequest request) throws Exception {
			
			ModelAndView model = new ModelAndView();

			model.addObject("editar_rappel_grupo", Tools.callServiceXML(request, "obtenerTiposRappels.action?servicio=obtenerTiposRappels"));
			model.addObject("creando", creando);
			model.addObject("id", id);
			model.addObject("idRappel", idRappel);
			model.addObject("fechainicio", fechainicio);
			model.addObject("fechafin", fechafin);			
			
			model.setViewName("app.facturacion.facturas.edit_rappel_grupo");
			
			return model;
		}		
		
		// *****************************************************************************************************
		@RequestMapping("ajax/facturacion/facturas/grupos/show_empresas_grupo.do")
		public ModelAndView facturacionShowEmpresasGrupo(
				@RequestParam(value = "id", required = false, defaultValue = "") String idgrupoempresas,
				HttpServletRequest request) throws Exception {
			
			ModelAndView model = new ModelAndView();

			model.addObject("empresas_grupo", Tools.callServiceXML(request, "obtenerHistoricoEmpresasGrupo.action?servicio=obtenerHistoricoEmpresasGrupo&xml=" + idgrupoempresas));
			
			model.setViewName("app.facturacion.facturas.list_empresa_grupo");
			
			return model;
		}
		

		// ***********************************ELIMINAR RAPPELS***********************************
		@RequestMapping("/ajax/facturacion/facturas/grupos/save_grupo.do")
		public ResponseEntity<String> facturacionGrupoGuardar(
				@RequestParam(value = "idgrupo", required = false, defaultValue = "") String idgrupoempresas,
				@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
				@RequestParam(value = "descripcion", required = false, defaultValue = "") String descripcion,
				@RequestParam(value = "xmlRappels", required = false, defaultValue = "") String xmlRappels,
				@RequestParam(value = "xmlEmpresas", required = false, defaultValue = "") String xmlEmpresas,
				HttpServletRequest request) throws Exception {

			String servicio = "";
			String xml_result;
			if (idgrupoempresas.length() > 0)
				servicio = "postDarDeAltaGrupoEmpresas.action?_modo=edicion&servicio=postDarDeAltaGrupoEmpresas&xml=";
			else
				{
				servicio = "postDarDeAltaGrupoEmpresas.action?_modo=alta&servicio=postDarDeAltaGrupoEmpresas&xml=";
				}
			
			servicio += "<getDarDeAltaGrupoEmpresas><Grupoempresas>";
			servicio += "<idgrupoempresas>"+idgrupoempresas+"</idgrupoempresas>";
			servicio+="<nombre>"+nombre+"</nombre>";			
			servicio+="<descripcion>"+descripcion+"</descripcion>";
			servicio+=xmlRappels;
			servicio+=xmlEmpresas;
			servicio +="</Grupoempresas></getDarDeAltaGrupoEmpresas>";
			
			
			xml_result = Tools.callServiceXML(request, servicio );
			
			if (xml_result.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
			}

			return new ResponseEntity<String>(HttpStatus.OK);
		}
		
		
		// *****************************************************************************************************
		@RequestMapping("ajax/facturacion/facturas/clientes/pantalla_bloqueo.do")
		public ModelAndView facturacionClientesPantallaBloqueo(
				@RequestParam(value = "id", required = false, defaultValue = "") String idCliente,
				@RequestParam(value = "bloqueado", required = false, defaultValue = "") String bloqueado,
				HttpServletRequest request) throws Exception {
			
			ModelAndView model = new ModelAndView();

			model.addObject("bloqueado",bloqueado);
			
			model.addObject("idCliente",idCliente);			
			
			model.addObject("historico_bloqueos", Tools.callServiceXML(request, "obtenerHistoricoBloqueosCliente.action?servicio=obtenerHistoricoBloqueosCliente&xml=" + idCliente));
			
			model.addObject("motivos_bloqueo", Tools.callServiceXML(request, "obtenerListadoMotivosBloqueoCliente.action?servicio=obtenerListadoMotivosBloqueoCliente&xml="));
			
			
			model.setViewName("app.facturacion.facturas.clientes.bloqueo");
			
			return model;
		}	
		
		// ***********************************ELIMINAR RAPPELS***********************************
		@RequestMapping("/ajax/facturacion/facturas/cliente/desbloquear.do")
		public ResponseEntity<String> facturacionDesbloquearCliente(
				@RequestParam(value = "id", required = false, defaultValue = "") String idCliente,
				@RequestParam(value = "bloqueado", required = false, defaultValue = "") String bloqueado,
				@RequestParam(value = "motivo", required = false, defaultValue = "") String motivo,
				HttpServletRequest request) throws Exception {

			String xml_result;
			if(bloqueado.equalsIgnoreCase("1"))
				xml_result = Tools.callServiceXML(request, "desbloquearClientes.action?servicio=desbloquearClientes&xml=" + idCliente );
			else
				{
				String xml="<getBloqueoCliente><BloqueoBajaClienteParam>";
				xml+="<listaIdsCliente><int>"+idCliente+"</int></listaIdsCliente>";
				xml+="<motivo>"+motivo+"</motivo>";
				xml+="</BloqueoBajaClienteParam></getBloqueoCliente>";
				xml_result = Tools.callServiceXML(request, "bloquearClientes.action?servicio=bloquearClientes&xml="  + URLEncoder.encode(xml, "UTF-8") );
				}

			if (xml_result.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
			}

			return new ResponseEntity<String>(HttpStatus.OK);
		}		
		
		// *****************************************************************************************************
		@RequestMapping("ajax/facturacion/facturas/clientes/pantalla_eliminacion.do")
		public ModelAndView facturacionClientesPantallaBloqueo(
				@RequestParam(value = "ids", required = false, defaultValue = "") String ids,						
				HttpServletRequest request) throws Exception {
			
			ModelAndView model = new ModelAndView();

			model.addObject("ids",ids);
			
			model.addObject("motivos_bloqueo", Tools.callServiceXML(request, "obtenerListadoMotivosBloqueoCliente.action?servicio=obtenerListadoMotivosBloqueoCliente&xml="));
								
			model.setViewName("app.facturacion.facturas.clientes.eliminar");
			
			return model;
		}		
		// ***********************************ELIMINAR Cliente***********************************
		@RequestMapping("/ajax/facturacion/facturas/cliente/eliminar.do")
		public ResponseEntity<String> facturacionEliminarCliente(
				@RequestParam(value = "ids", required = false, defaultValue = "") String ids,
				@RequestParam(value = "motivo", required = false, defaultValue = "") String motivo,
				HttpServletRequest request) throws Exception {

			String idsArray[] = ids.split(",");	
			
			String xmlIds="<listaIdsCliente>";
			for( int i=0; i<idsArray.length; i++)
			{
				xmlIds+="<int>"+idsArray[i]+"</int>";
			}
			xmlIds+="</listaIdsCliente>";
			
			String xml_result = Tools.callServiceXML(request, "darDeBajaClientes.action?servicio=darDeBajaClientes&motivo="+URLEncoder.encode("<motivo>"+motivo+"</motivo>","UTF-8")+"&xml="  + URLEncoder.encode(xmlIds, "UTF-8") );
		
			if (xml_result.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
			}

			return new ResponseEntity<String>(HttpStatus.OK);
		}
		
		// ***************************************************************************************************
		@RequestMapping(value = "ajax/facturacion/clientes/select_list_actividad_por_tipo.do", method = RequestMethod.POST)
		public ResponseEntity<?> facturacionClientesCargarActividad(
				@RequestParam(value = "idCategoria", required = false, defaultValue = "") String idCategoria,
				HttpServletRequest request) throws Exception {

			String xml="<int>"+idCategoria+"</int>";
			String json = Tools.callServiceJSON(request, "obtenerListadoActividadesPorCategoria.action?servicio=obtenerListadoActividadesPorCategoria&xml="+ URLEncoder.encode(xml, "UTF-8"));

			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}
		
		// *****************************************************************************************************
		@RequestMapping("ajax/facturacion/facturas/clientes/asignar_tarifas.do")
		public ModelAndView facturacionClientesAsignarTarifas(
				@RequestParam(value = "id", required = false, defaultValue = "") String idcliente,
				HttpServletRequest request) throws Exception {
			
			ModelAndView model = new ModelAndView();
			
			model.addObject("idcliente",idcliente);
			
			model.addObject("asignarTarifas_productos", Tools.callServiceXML(request, "obtenerListadoProductos.action?servicio=obtenerListadoProductos"));

			model.setViewName("app.facturacion.facturas.cliente.asignar.tarifas");
			
			return model;
		}
		
		
		// ***************************************************************************************************
		@RequestMapping(value = "ajax/facturacion/facturas/clientes/listTarifasProductos.do", method = RequestMethod.POST)
		public ResponseEntity<?> clienteListTarifasPorProducto(
				@RequestParam(value = "idProducto", required = false, defaultValue = "") String idProducto,
				@RequestParam(value = "idCliente", required = false, defaultValue = "") String idCliente,
				HttpServletRequest request) throws Exception {

			String json = Tools.callServiceJSON(request, "obtenerTarifasProductoPorProductoYCliente.action?servicio=obtenerTarifasProductoPorProductoYCliente&idcliente="+idCliente+"&idproducto="+idProducto);
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}

		
		
		// ***************************************************************************************************
		@RequestMapping(value = "ajax/facturacion/facturas/clientes/save_cliente.do", method = RequestMethod.POST)
		public ResponseEntity<?> facturacionclienteGuardar(
				@RequestParam(value = "idcliente", required = false, defaultValue = "") String idcliente,
				@RequestParam(value = "idprincipal", required = false, defaultValue = "") String idprincipal,
				@RequestParam(value = "personafisica", required = false, defaultValue = "") String personafisica,
				@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
				@RequestParam(value = "apellido1", required = false, defaultValue = "") String apellido1,
				@RequestParam(value = "apellido2", required = false, defaultValue = "") String apellido2,
				@RequestParam(value = "idactividad", required = false, defaultValue = "") String idactividad,
				@RequestParam(value = "idcategoriacategoriaactividad", required = false, defaultValue = "") String idcategoriacategoriaactividad,
				@RequestParam(value = "idtipoidentificador", required = false, defaultValue = "0") String idtipoidentificador,
				@RequestParam(value = "identificador_identificador", required = false, defaultValue = "") String identificador_identificador,
				@RequestParam(value = "identificador", required = false, defaultValue = "") String identificador,
				@RequestParam(value = "ciffiscal", required = false, defaultValue = "") String ciffiscal,
				@RequestParam(value = "cifdeclienteprincipal", required = false, defaultValue = "") String cifdeclienteprincipal,				
				@RequestParam(value = "principal", required = false, defaultValue = "0") String principal,
				@RequestParam(value = "xmlDireccionOrigen", required = false, defaultValue = "") String xmlDireccionOrigen,
				@RequestParam(value = "xmlDireccionFiscal", required = false, defaultValue = "") String xmlDireccionFiscal,
				@RequestParam(value = "xmlDireccionAuxiliar", required = false, defaultValue = "") String xmlDireccionAuxiliar,
				@RequestParam(value = "idperiodofacturacion", required = false, defaultValue = "") String idperiodofacturacion,
				@RequestParam(value = "idtipocliente", required = false, defaultValue = "") String idtipocliente,
				@RequestParam(value = "idtiporappel", required = false, defaultValue = "") String idtiporappel,
				@RequestParam(value = "iddireccionenvio", required = false, defaultValue = "") String iddireccionenvio,
				@RequestParam(value = "idgrupoempresas", required = false, defaultValue = "") String idgrupoempresas,
				@RequestParam(value = "aparecepersonaenfactura", required = false, defaultValue = "0") String aparecepersonaenfactura,
				@RequestParam(value = "observaciones", required = false, defaultValue = "") String observaciones,
				@RequestParam(value = "idformapago", required = false, defaultValue = "") String idformapago,
				@RequestParam(value = "idcanal", required = false, defaultValue = "") String idcanal,
				@RequestParam(value = "facturable", required = false, defaultValue = "0") String facturable,
				@RequestParam(value = "facturarsucursal", required = false, defaultValue = "0") String facturarsucursal,
				@RequestParam(value = "generarrappelporsucursal", required = false, defaultValue = "0") String generarrappelporsucursal,
				@RequestParam(value = "rappelautomatico", required = false, defaultValue = "0") String rappelautomatico,
				@RequestParam(value = "idperiodoenvio", required = false, defaultValue = "") String idperiodoenvio,
				@RequestParam(value = "facturable_electronico", required = false, defaultValue = "0") String facturable_electronico,
				@RequestParam(value = "razonSocial", required = false, defaultValue = "0") String razonSocial,
				HttpServletRequest request) throws Exception {
			
			if(generarrappelporsucursal.startsWith("on"))
				generarrappelporsucursal = "1";
			
			
			if(rappelautomatico.startsWith("on"))
				rappelautomatico = "1";
			
		
			
			if(aparecepersonaenfactura.startsWith("on"))
				aparecepersonaenfactura = "1";
			
			if(facturable.startsWith("on"))
				facturable = "1";	
			else
				facturable = "false";
			

			if(facturable_electronico.startsWith("on"))
				facturable_electronico = "1";
			else
				facturable_electronico = "false";
			
			
			String xmlEnvio="<getEdicionAltaClientesFacturacion><Cliente>";
			xmlEnvio+="<idcliente>"+idcliente+"</idcliente>";
			xmlEnvio+="<personafisica>"+personafisica+"</personafisica>";
			xmlEnvio+="<nombre>"+nombre+"</nombre>";
			xmlEnvio+="<nombrecompleto>"+nombre+" "+ apellido1+ " "+ apellido2+"</nombrecompleto>";
			xmlEnvio+="<apellido1>"+apellido1+"</apellido1><apellido2>"+apellido2+"</apellido2>";
			xmlEnvio+="<localizadorAgencia/>";
			xmlEnvio+="<actividad><idactividad>"+idactividad+"</idactividad><categoriaactividad><idcategoriacategoriaactividad>"+idcategoriacategoriaactividad+"</idcategoriacategoriaactividad>";
			xmlEnvio+="</categoriaactividad></actividad>";
			xmlEnvio+="<tipoidentificador><idtipoidentificador>"+idtipoidentificador+"</idtipoidentificador></tipoidentificador>";
			xmlEnvio+="<identificador>"+identificador+"</identificador>";
			if(idprincipal.equalsIgnoreCase(""))
				xmlEnvio+="<ciffiscal>"+ciffiscal+"</ciffiscal><cifdeclienteprincipal></cifdeclienteprincipal><principal>1</principal>";
			else
				xmlEnvio+="<ciffiscal></ciffiscal><cifdeclienteprincipal>"+ciffiscal+"</cifdeclienteprincipal><principal>0</principal>";
			xmlEnvio+="<nrocuenta/>";
			xmlEnvio+="<observacionescb/>";
			xmlEnvio+=xmlDireccionOrigen;
			xmlEnvio+=xmlDireccionFiscal;
			xmlEnvio+=xmlDireccionAuxiliar;
			xmlEnvio+="<cliente><idcliente>"+idprincipal+"</idcliente></cliente>";
			xmlEnvio+="<periodofacturacion><idperiodofacturacion>"+idperiodofacturacion+"</idperiodofacturacion><nombre/></periodofacturacion>";
			xmlEnvio+="<tipoclienteactivo><idtipocliente>"+idtipocliente+"</idtipocliente><nombre/></tipoclienteactivo>";
			xmlEnvio+="<tiporappelactivo><idtiporappel>"+idtiporappel+"</idtiporappel><nombre/></tiporappelactivo>";
			xmlEnvio+="<direccionenvio><iddireccionenvio>"+iddireccionenvio+"</iddireccionenvio><nombre/></direccionenvio>";
			xmlEnvio+="<canal><idcanal>"+idcanal+"</idcanal><nombre></nombre></canal>";
			xmlEnvio+="<grupoempresasactivo><idgrupoempresas>"+idgrupoempresas+"</idgrupoempresas><nombre/></grupoempresasactivo>";
			xmlEnvio+="<aparecepersonaenfactura>"+aparecepersonaenfactura+"</aparecepersonaenfactura>";
			xmlEnvio+="<razonsocial>"+razonSocial+"</razonsocial><observaciones>"+observaciones+"</observaciones>";
			xmlEnvio+="<facturable>"+facturable+"</facturable><bloqueado>0</bloqueado>";
			xmlEnvio+="<facturable_electronico>"+facturable_electronico+"</facturable_electronico>";
			xmlEnvio+="<formapago><idformapago>"+idformapago+"</idformapago><nombre/></formapago>";
			xmlEnvio+="<facturarsucursal>"+principal+"</facturarsucursal><generarrappelporsucursal>"+generarrappelporsucursal+"</generarrappelporsucursal>";
			xmlEnvio+="<rappelautomatico>"+rappelautomatico+"</rappelautomatico>";
			xmlEnvio+="<periodoenvio><idperiodoenvio>"+idperiodoenvio+"</idperiodoenvio><nombre/></periodoenvio>";
			xmlEnvio+="<clientes/></Cliente>";
			xmlEnvio+="</getEdicionAltaClientesFacturacion>";
			
			
			
			/*xmlEnvio="<getEdicionAltaClientesFacturacion><Cliente><idcliente>333607</idcliente><personafisica>0</personafisica><nombre>HOTEL NH LAS ARTES</nombre><nombrecompleto>HOTEL NH LAS ARTES</nombrecompleto><apellido1></apellido1><apellido2></apellido2><localizadorAgencia/><actividad><idactividad>803</idactividad><categoriaactividad><idcategoriacategoriaactividad>2</idcategoriacategoriaactividad></categoriaactividad></actividad><fechaalta>25/11/2015-00:00:00</fechaalta><tipoidentificador><idtipoidentificador>0</idtipoidentificador></tipoidentificador><identificador></identificador><ciffiscal/><principal>0</principal><fechaultimamodificacion>22/04/2019-00:00:00</fechaultimamodificacion><nrocuenta/><observacionescb/><direccionorigen><iddireccion>333608</iddireccion><personacontacto>Nº Pedido: 4501954042</personacontacto><tipovia>Avenida</tipovia><direccion>INSTITUTO OBRERO DE VALENCIA, 28</direccion><pais><idpais>472</idpais><nombre>España</nombre></pais><provincia><idprovincia>46</idprovincia><nombreprovincia>Valencia</nombreprovincia></provincia><cp><idcp>9930</idcp><cp>46013</cp></cp><municipio><idmunicipio>53076</idmunicipio><nombre>VALENCIA</nombre></municipio><telefonofijo>963351310</telefonofijo><telefonomovil>963351314</telefonomovil><fax></fax><correoelectronico>f.romaguera@nh-hotels.com; g.trivino@nh-hotels.com</correoelectronico></direccionorigen><cliente><idcliente>333601</idcliente></cliente><cifdeclienteprincipal>A58511882</cifdeclienteprincipal><direccionauxiliar><iddireccion>333603</iddireccion><personacontacto></personacontacto><tipovia></tipovia><direccion></direccion><pais><idpais>472</idpais><nombre>España</nombre></pais><provincia><idprovincia></idprovincia><nombreprovincia></nombreprovincia></provincia><cp><idcp></idcp><cp></cp></cp><municipio><idmunicipio></idmunicipio><nombre></nombre></municipio><telefonofijo></telefonofijo><telefonomovil></telefonomovil><fax></fax><correoelectronico></correoelectronico></direccionauxiliar><direccionfiscal><iddireccion>333604</iddireccion><personacontacto></personacontacto><tipovia>Avenida</tipovia><direccion>TRAVESSERA DE LES CORTS , 114</direccion><pais><idpais>472</idpais><nombre>España</nombre></pais><provincia><idprovincia>8</idprovincia><nombreprovincia>Barcelona</nombreprovincia></provincia><cp><idcp>1571</idcp><cp>08028</cp></cp><municipio><idmunicipio>4253</idmunicipio><nombre>BARCELONA</nombre></municipio><telefonofijo></telefonofijo><telefonomovil></telefonomovil><fax></fax><correoelectronico>c.acien@nh-hotels.com</correoelectronico></direccionfiscal><periodofacturacion><idperiodofacturacion>0</idperiodofacturacion></periodofacturacion><tipoclienteactivo><idtipocliente>877</idtipocliente><nombre>Crédito 15%</nombre><porcentajedescuento>15.00</porcentajedescuento><porcentajecomision/></tipoclienteactivo><tiporappelactivo><idtiporappel></idtiporappel><nombre/></tiporappelactivo><direccionenvio><iddireccionenvio>1</iddireccionenvio></direccionenvio><canal><idcanal/><nombre></nombre></canal><grupoempresasactivo><idgrupoempresas></idgrupoempresas><nombre/></grupoempresasactivo><aparecepersonaenfactura>0</aparecepersonaenfactura><razonsocial>NH HOTELES ESPAÑA S.A.</razonsocial><observaciones>Atención a proveedores para consultas sobre facturas, será se.ptp@nh-hotels.com";
			xmlEnvio+="Carolina. ";
			xmlEnvio+="Contactos:";
			xmlEnvio+="NH Artes I: Finabel";
			xmlEnvio+="NH Artes II: Isabel";
			xmlEnvio+="NH Center: Elena y Gemma (envio a ambos correos)";
			xmlEnvio+="NH Ciudad de Valencia: Salvador";
			xmlEnvio+="NH</observaciones><bloqueado>0</bloqueado><formapago><idformapago></idformapago><nombre/></formapago><facturarsucursal>1</facturarsucursal><generarrappelporsucursal>0</generarrappelporsucursal><rappelautomatico>0</rappelautomatico><periodoenvio><idperiodoenvio>290753</idperiodoenvio></periodoenvio><clientes/><facturable>1</facturable><facturable_electronico>1</facturable_electronico></Cliente></getEdicionAltaClientesFacturacion>";			
			*/String xml_result = Tools.callServiceXML(request, "postEdicionAltaClientesFacturacion.action?_modo=&servicio=postEdicionAltaClientesFacturacion&xml="  + URLEncoder.encode(xmlEnvio, "UTF-8") );
			
						
			
			if (xml_result.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
			}
			
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
			
			JSONObject json = XML.toJSONObject(xml_result);
			json.append("iTotalRecords", "99999").append("iTotalDisplayRecords", "99999");
			
			return new ResponseEntity<String>(json.toString(4), responseHeaders, HttpStatus.OK);

			//return new ResponseEntity<String>(HttpStatus.OK);
		}
		
		
		@RequestMapping(value = "/ajax/facturacion/facturas/clientes/list_tarifas_productos.do", method = RequestMethod.POST)
		public ResponseEntity<?> facturacionListClientes(
				@RequestParam(value = "idcliente", required = false, defaultValue = "0") String idcliente, 
				@RequestParam(value = "idproducto", required = false, defaultValue = "0") String idproducto, 
				HttpServletRequest request) throws Exception {
		
		String json = "";

		try {
			json = Tools.callServiceJSON(request, "obtenerTarifasProductoPorProductoYCliente.action?servicio=obtenerTarifasProductoPorProductoYCliente&idcliente="+idcliente+"&idproducto="+idproducto);
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				
		}
		
		@RequestMapping("ajax/facturacion/facturas/clientes/pantalla_creacion_tarifas.do")
		public ModelAndView facturacionClientesCreacionTarifas(
				@RequestParam(value = "data", required = true) List<String> data,
				HttpServletRequest request) throws Exception {
			ModelAndView model = new ModelAndView();
			
			
			String filter = "<envio>";
			Iterator<String> dataIterator = data.iterator();
			int i = 0;
			while (dataIterator.hasNext()) {
				
				filter  = filter +"<parametro id=\""+i+"\">"+dataIterator.next()+"</parametro>";
				i++;
			}
			
			filter+="</envio>";
			
			model.addObject("asignarTarifas_productos_tarifas", Tools.callServiceXML(request, "obtenerListadoTarifas.action?servicio=obtenerListadoTarifas&selected=-1&filter="+ URLEncoder.encode(filter, "UTF-8")));
			
			model.setViewName("app.facturacion.facturas.cliente.asignar.tarifas.creacionTarifa");
			
			return model;
		
		}
		
		
			@RequestMapping(value = "ajax/facturacion/facturas/clientes/asignar_tarifas.do", method = RequestMethod.POST)
			public ResponseEntity<?> facturacionclienteAsignarTarifas(
					@RequestParam(value = "xmlEnvio", required = false, defaultValue = "") String xmlEnvio, 
					HttpServletRequest request) throws Exception {
					
				String json = "";

				try {
					json = Tools.callServiceJSON(request, "actualizarTarifaproductoPorClienteYProducto.action?servicio=actualizarTarifaproductoPorClienteYProducto&xml="+ URLEncoder.encode(xmlEnvio, "UTF-8") );
				} catch (Exception ex) {
					throw new AjaxException(ex.getMessage());
				}

				HttpHeaders responseHeaders = new HttpHeaders();
				responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

				return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
			}
			
			
			@RequestMapping("ajax/facturacion/facturas/facturas/rectificar.do")
			public ModelAndView facturacionFacturasRectificar(
					@RequestParam(value = "idFactura", required = true) String idFactura,
					HttpServletRequest request) throws Exception {
				ModelAndView model = new ModelAndView();
				
				String datos_factura_xml = Tools.callServiceXML(request,"obtenerFacturaPorIdParaRectificar.action?servicio=obtenerFacturaPorIdParaRectificar&idFactura="+idFactura);
				
				model.addObject("datos_factura", datos_factura_xml);
				
				//Ponemos tambien el objeto en json
				JSONObject json = XML.toJSONObject(datos_factura_xml);

				if (json.has("error")) throw new Exception(json.getString("error"));
				
				model.addObject("datos_factura_json", json);
				model.setViewName("app.facturacion.facturas.factura.rectificar");
				return model;
			}
			
			
			@RequestMapping("/ajax/facturacion/facturas/facturas/rectificarLineas.do")
			public ModelAndView facturacionFacturasRectificarLineas(
					HttpServletRequest request) throws Exception {
				ModelAndView model = new ModelAndView();
				
				model.addObject("ventareserva_productos_principales", Tools.callServiceXML(request, "obtenerListadoProductosPrincipales.action?servicio=obtenerListadoProductosPrincipales"));
												
				model.setViewName("app.facturacion.facturas.factura.rectificarLineas");
				return model;
			}
			
			
			///***************************************************************************************
			@RequestMapping("/ajax/facturacion/facturas/facturas/rectificarLineasPago.do")					
			public ModelAndView rectificarLineasPago(
					@RequestParam(value = "id", required = false, defaultValue = "") String id,
					@RequestParam(value = "importetotalventa", required = false, defaultValue = "") String importetotalventa,
					@RequestParam(value = "importetotalpagado", required = false, defaultValue = "") String importetotalpagado,
					@RequestParam(value = "idcanal", required = false, defaultValue = "") String idcanal,
					HttpServletRequest request) throws Exception {
				
				ModelAndView model = new ModelAndView();
				
				model.addObject("motivosModificacion", Tools.callServiceXML(request, "obtenerListadoMotivosModificacion.action?servicio=obtenerListadoMotivosModificacion"));
				model.addObject("selectorFormasdePagoPorCanal", Tools.callServiceXML(request, "obtenerListadoFormasPagoPorCanal.action?servicio=obtenerListadoFormasPagoPorCanal&xml=<canal><idcanal>"+idcanal+"</idcanal></canal>"));
				model.addObject("importesParciales", Tools.callServiceXML(request, "obtenerImportesParcialesPorVenta.action?servicio=obtenerImportesParcialesPorVenta&xml=<int>"+id+"</int>"));
								
				model.addObject("id", id);
						
				float total= Float.parseFloat(importetotalventa);
				String importeTotal =String.format("%.2f", total);
				float pagado= Float.parseFloat(importetotalpagado);
				String importePagado= String.format("%.2f", pagado);
				float importeDiferencia = total-pagado;
				String  diferencia= String.format("%.2f", importeDiferencia);
				
				model.addObject("importeTotal",importeTotal);
				model.addObject("importePagado",importePagado);
				model.addObject("diferencia",diferencia);
				
				model.setViewName("app.facturacion.facturas.factura.rectificarLineas.pagos");
				
				return model;
			}
			
			
			
			@RequestMapping("ajax/facturacion/facturas/facturas/rectificar_motivos.do")
			public ModelAndView facturacionFacturasRectificarPantallaMotivos(
					HttpServletRequest request) throws Exception {
				ModelAndView model = new ModelAndView();
				
				
				model.addObject("motivos_rectificacion", Tools.callServiceXML(request,"obtenerListadoMotivosRectificacion.action?servicio=obtenerListadoMotivosRectificacion"));
								
				model.setViewName("app.facturacion.facturas.factura.rectificar.motivos");
				
				return model;
			
			}			
			
			
			
	
			@RequestMapping(value = "ajax/facturacion/factura/validar_factura.do", method = RequestMethod.POST)
			public ResponseEntity<?> facturacionfacturaValidarAsignarTarifas(
					@RequestParam(value = "xmlFacturas", required = false, defaultValue = "") String xmlFacturas, 
					HttpServletRequest request) throws Exception {
					
				String json = "";

				try {
					json = Tools.callServiceJSON(request, "validarFacturas.action?servicio=validarFacturas&xml="+ URLEncoder.encode("<parametro>"+xmlFacturas+"</parametro>", "UTF-8") );
				} catch (Exception ex) {
					throw new AjaxException(ex.getMessage());
				}

				HttpHeaders responseHeaders = new HttpHeaders();
				responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

				return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
			}			

			@RequestMapping(value = "ajax/factura/factura/factura_envio.do", method = RequestMethod.POST)
			public ResponseEntity<?> facturacionfacturaEnviar(
					@RequestParam(value = "xml", required = false, defaultValue = "") String xml, 
					HttpServletRequest request) throws Exception {
					
				String json = "";

				try {
					//xml="<parametro><list><Factura idfactura=\"72095\"><idfactura>72095</idfactura><numerofactura>100</numerofactura><cliente><idcliente>4980</idcliente><nombrecompleto>VIAJES AZAMAR, S.A.</nombrecompleto><direccionauxiliar/><direccionfiscal><fax>964480943</fax><correoelectronico>vicky@tourazamar.com</correoelectronico></direccionfiscal><direccionorigen><fax>964480943</fax><correoelectronico>vicky@tourazamar.com</correoelectronico></direccionorigen></cliente><fechageneracion>31/12/2008-00:00:05</fechageneracion><validada>1</validada><usuariovalidacion><idusuario>5058722</idusuario><login>mprado-sadim</login></usuariovalidacion><fechavalidacion>29/11/2017-00:00:00</fechavalidacion><enviada/><usuarioenvio/><fechaenvio/><importetotal>1425.60</importetotal><numimpresiones>1</numimpresiones><direccionenvio><iddireccionenvio>1</iddireccionenvio><nombre>Origen</nombre></direccionenvio><tipofactura><serie>908</serie><rectificativa>0</rectificativa></tipofactura><rectificativas/><serieynumero>908/100</serieynumero><direccionenvio><iddireccionenvio>1</iddireccionenvio><nombre>Origen</nombre></direccionenvio><ventafacturas><Ventafactura><venta><tipoventa><idtipoventa>4</idtipoventa></tipoventa></venta></Ventafactura></ventafacturas><bonofacturas/><anulada>0</anulada><esrectificativa>0</esrectificativa></Factura></list><medioenvio>email</medioenvio></parametro>";
					json = Tools.callServiceJSON(request, "enviarFacturas.action?servicio=enviarFacturas&xml="+URLEncoder.encode(xml, "UTF-8") );
				} catch (Exception ex) {
					throw new AjaxException(ex.getMessage());
				}

				HttpHeaders responseHeaders = new HttpHeaders();
				responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

				return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
			}
			
			@RequestMapping("ajax/facturacion/facturas/factura_modificar.do")
			public ModelAndView facturacionFacturasModificar(
					@RequestParam(value = "idFactura", required = true) String idFactura,
					HttpServletRequest request) throws Exception {
				ModelAndView model = new ModelAndView();
				
				model.addObject("editar_cliente_forma_pago", Tools.callServiceXML(request, "obtenerListadoFormasPagoVigentes.action?servicio=obtenerListadoFormasPagoVigentes"));
				model.addObject("datos_factura", Tools.callServiceXML(request,"obtenerFacturaPorId.action?servicio=obtenerFacturaPorId&idFactura="+idFactura));
				model.addObject("editar_cliente_direcciones_envio", Tools.callServiceXML(request, "obtenerListadoDireccionesEnvio.action?servicio=obtenerListadoDireccionesEnvio"));
				
				model.setViewName("app.facturacion.facturas.factura.modificar");
				
				return model;
			
			}
			
			@RequestMapping(value = "ajax/factura/factura/factura_modificar.do", method = RequestMethod.POST)
			public ResponseEntity<?> facturacionfacturaModificar(
					@RequestParam(value = "observaciones", required = false, defaultValue = "") String observaciones, 
					@RequestParam(value = "numImpresiones", required = false, defaultValue = "") String numImpresiones,
					@RequestParam(value = "fechageneracion", required = false, defaultValue = "") String fechaGeneracion,
					@RequestParam(value = "idFormaenvio", required = false, defaultValue = "") String idFormaenvio,
					@RequestParam(value = "idFormapago", required = false, defaultValue = "") String idFormapago,
					@RequestParam(value = "idFactura", required = false, defaultValue = "") String idFactura,
					HttpServletRequest request) throws Exception {
					
				String json = "";
				String cadena="";
				try {
					cadena +="observaciones="+observaciones;
					cadena +="&numImpresiones="+numImpresiones;
					cadena +="&fechaGeneracion="+fechaGeneracion;
					cadena +="&idFormaenvio="+idFormaenvio;
					cadena +="&idFormapago="+idFormapago;
					cadena +="&idFactura="+idFactura;
					
					
					json = Tools.callServiceJSON(request, "actualizarFactura.action?servicio=actualizarFactura&"+cadena);
				} catch (Exception ex) {
					throw new AjaxException(ex.getMessage());
				}

				HttpHeaders responseHeaders = new HttpHeaders();
				responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

				return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
			}			
			@RequestMapping("ajax/facturacion/facturas/factura_pantalla_anular.do")
			public ModelAndView facturacionFacturasPantallaAnular(
					HttpServletRequest request) throws Exception {
				ModelAndView model = new ModelAndView();
				
				model.addObject("motivo_anulacion", Tools.callServiceXML(request,"obtenerListadoMotivosRectificacion.action?servicio=obtenerListadoMotivosRectificacion"));
                				
				model.setViewName("app.facturacion.facturas.factura.anular");
				
				return model;
			
			}
			// ***********************************LISTADO FACTURAS GENERACION***********************************
			@RequestMapping("/ajax/facturacion/facturas/generacionfacturas/show_listadofacturas.do")
			public ModelAndView facturacionShowListadoFacturas(
					@RequestParam(value = "facturas", required = false) String facturas,
					HttpServletRequest request) throws Exception {
				
				ModelAndView model = new ModelAndView();
				
				facturas="<ArrayList>"+facturas+"</ArrayList>";
							
				model.addObject("listar_facturas", facturas);
				model.setViewName("app.facturacion.facturas.list_facturas");
				
				return model;
			}
			

			// ***********************************************************************************
			@RequestMapping(value = "ajax/factura/factura/factura_anular.do", method = RequestMethod.POST)
			public ResponseEntity<?> facturacionfacturaAnular(
					@RequestParam(value = "motivo", required = false, defaultValue = "") String motivo, 
					@RequestParam(value = "data", required = true) List<String> data,
					@RequestParam(value = "tipo_anulacion", required = true, defaultValue = "1") int tipo_anulacion,
					HttpServletRequest request) throws Exception {
					
				String json = "";
				try {
					String xmlList = "<list>";
					Iterator<String> dataIterator = data.iterator();
					int i = 0;
					while (dataIterator.hasNext()) {
						i++;
						String valor = dataIterator.next();
						if(i==1)
							xmlList += "<long>" + valor + "</long>";
						else
							i=0;
					}
					xmlList += "</list>";
					String llamada ="";
					if(tipo_anulacion==1)
						llamada = "anularFacturas.action?servicio=anularFacturas&motivo="+URLEncoder.encode("<motivo>"+motivo+"</motivo>", "UTF-8")+"&xml="+URLEncoder.encode(xmlList, "UTF-8");
					else
						llamada = "anularFacturasYVentas.action?servicio=anularFacturasYVentas&xml=<parametro>"+xmlList+"<motivo>"+motivo+"</motivo></parametro>";
					json = Tools.callServiceJSON(request, llamada);
				} catch (Exception ex) {
					throw new AjaxException(ex.getMessage());
				}

				HttpHeaders responseHeaders = new HttpHeaders();
				responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

				return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
			}	

			
			// *****************************************************************************************************
			@RequestMapping("ajax/facturacion/facturas/clientes/pantalla_enviar_correo.do")
			public ModelAndView facturacionClientesPantallaCorreo(
					@RequestParam(value = "ids", required = false, defaultValue = "") String ids,						
					HttpServletRequest request) throws Exception {
				
				ModelAndView model = new ModelAndView();

				model.addObject("ids",ids);
				
				model.addObject("modelos_correo", Tools.callServiceXML(request, "obtenerListadoModelosCorreo.action?servicio=obtenerListadoModelosCorreo"));
									
				model.setViewName("app.facturacion.facturas.clientes.enviar_correo");
				
				return model;
			}	
			
			
			// ***********************************************************************************
			@RequestMapping(value = "ajax/facturacion/facturas/cliente/enviar_correo.do", method = RequestMethod.POST)
			public ResponseEntity<?> facturacionClienteEnviarCorreo(
					@RequestParam(value = "ids", required = false, defaultValue = "") String ids, 
					@RequestParam(value = "id_modelo", required = true, defaultValue = "") int id_modelo,
					HttpServletRequest request) throws Exception {
					
				String json = "";
				
				try {
					String idsArray[] = ids.split(",");	
					
					String xmlIds="<list>";
					for( int i=0; i<idsArray.length; i++)
					{
						xmlIds+="<int>"+idsArray[i]+"</int>";
					}
					xmlIds+="</list>";
					String llamada = "enviarCorreos.action?servicio=enviarCorreos&idmodelo="+id_modelo+"&xml="+xmlIds;
					json = Tools.callServiceJSON(request, llamada);
					json = json.replaceAll("es.di.cac.ticketing.model.dto.ResultadoEnviarCorreos", "ResultadoEnviarCorreos");
					
				} catch (Exception ex) {
					throw new AjaxException(ex.getMessage());
				}

				HttpHeaders responseHeaders = new HttpHeaders();
				responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

				return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
			}	
			
		
			@RequestMapping(value = "ajax/facturacion/facturas/facturas/buscarVentasDeCliente.do")
			public ModelAndView facturacionbuscarVentasCliente( 
					@RequestParam(value = "idcliente", required = false, defaultValue = "") String idcliente,						
					HttpServletRequest request) throws Exception {
				ModelAndView model = new ModelAndView();

				model.addObject("idcliente", idcliente);
				
				model.setViewName("app.facturacion.facturas.ventasporcliente");
				
				return model;
			}
			
			
			// ***************************************************************************************************
			@RequestMapping("ajax/facturacion/show_list_combinados.do")
			public ModelAndView showListCombinados(
					@RequestParam(value = "tabla", required = false, defaultValue = "") String tabla, 
					@RequestParam(value = "list", required = false, defaultValue = "") String list, 
					HttpServletRequest request) throws Exception {
			
				ModelAndView model = new ModelAndView();

				model.setViewName("app.facturacion.facturas.list_combinados");
				model.addObject("tabla",tabla);
				model.addObject("list",list);
				
				return model;
			}
			
			// ***************************************************************************************************
			@RequestMapping("ajax/facturacion/show_list_productos.do")
			public ModelAndView showListProductos(
					@RequestParam(value = "tabla", required = false, defaultValue = "") String tabla, 
					@RequestParam(value = "list", required = false, defaultValue = "") String list, 
					HttpServletRequest request) throws Exception {
			
				ModelAndView model = new ModelAndView();

				model.setViewName("app.facturacion.facturas.list_productos");
				model.addObject("tabla",tabla);
				model.addObject("list",list);
				
				return model;
			}
			
			// ***************************************************************************************************
			@RequestMapping("ajax/facturacion/show_edit_sesion.do")
			public ModelAndView showEditSesion(
					@RequestParam(value = "idproducto", required = false, defaultValue = "-1") String idProducto, 
					@RequestParam(value = "idtarifa", required = false, defaultValue = "-1") String idTarifa, 
					@RequestParam(value = "idtipoventa", required = false, defaultValue = "4") String idTipoVenta,
					@RequestParam(value = "idcanal", required = false, defaultValue = "2") String idCanal,
					@RequestParam(value = "fecha", required = false, defaultValue = "") String fecha,
					@RequestParam(value = "idcliente", required = false, defaultValue = "") String idCliente, 
					@RequestParam(value = "idxnumero", required = false, defaultValue = "") String idxNumero, 
					@RequestParam(value = "idxunitario", required = false, defaultValue = "") String idxUnitario,
					@RequestParam(value = "idxtotal", required = false, defaultValue = "") String idxTotal,
					@RequestParam(value = "idxbono", required = false, defaultValue = "") String idxBono, 
					@RequestParam(value = "idxtarifa_id", required = false, defaultValue = "") String idxTarifa_id,
					@RequestParam(value = "idxtarifa_txt", required = false, defaultValue = "") String idxTarifa_txt,
					@RequestParam(value = "idxdescuento_id", required = false, defaultValue = "") String idxDescuento_id,
					@RequestParam(value = "idxdescuento_txt", required = false, defaultValue = "") String idxDescuento_txt,
					@RequestParam(value = "idxperfil", required = false, defaultValue = "") String idxPerfil, 
					@RequestParam(value = "button", required = false, defaultValue = "") String buttonEdit,
					@RequestParam(value = "list", required = false, defaultValue = "") String list,
					HttpServletRequest request) throws Exception {
			
				ModelAndView model = new ModelAndView();

				model.setViewName("app.facturacion.facturas.edit_sesion");

				model.addObject("buttonEdit",buttonEdit);
				model.addObject("list",list);
				model.addObject("idProducto",idProducto);
				model.addObject("idCliente",idCliente);
				model.addObject("idTarifa",idTarifa);
				model.addObject("idTarifa",idTipoVenta);
				model.addObject("idxNumero",idxNumero);
				model.addObject("idxUnitario",idxUnitario);
				model.addObject("idxTotal",idxTotal);
				model.addObject("idxBono",idxBono);
				model.addObject("idxTarifa_id",idxTarifa_id);
				model.addObject("idxTarifa_txt",idxTarifa_txt);
				model.addObject("idxDescuento_id",idxDescuento_id);
				model.addObject("idxDescuento_txt",idxDescuento_txt);
				model.addObject("idxPerfil",idxPerfil);
				
				model.addObject("ventareserva_selector_descuentos", Tools.callServiceXML(request, "obtenerListadoDescuentosAplicables.action?servicio=obtenerListadoDescuentosAplicables&idtarifa="+idTarifa+"&idproducto="+idProducto+"&idcanal="+idCanal));
				
				
				return model;
			}
			
			
			
				// ***********************************************************************************
				@RequestMapping(value = "ajax/facturacion/factura_rectificar.do", method = RequestMethod.POST)
				public ResponseEntity<?> facturacionRectificar(
						@RequestParam(value = "motivo", required = false, defaultValue = "") String idMotivo, 
						@RequestParam(value = "idcliente_rectificacion", required = false, defaultValue = "") String idCliente,
						@RequestParam(value = "idFactura", required = false, defaultValue = "") String idFactura,
						@RequestParam(value = "otros", required = true, defaultValue = "") String otros,
						@RequestParam(value = "tipo_rectificacion", required = true, defaultValue = "") String tipo_rectificacion,
						@RequestParam(value = "listIds", required = true, defaultValue = "") String listIds,
						@RequestParam(value = "xml_venta", required = true, defaultValue = "") String xml_venta,
						
						
						HttpServletRequest request) throws Exception {
					
					String json="";
					
						
					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
					if(tipo_rectificacion.equalsIgnoreCase("1"))
						{
						String llamada="rectificarFacturaPorCliente.action?servicio=rectificarFacturaPorCliente&motivo="+idMotivo+"&idcliente="+idCliente+"&idfactura="+idFactura;
						try {
							json = Tools.callServiceJSON(request, llamada);
						} catch (Exception ex) {
							throw new AjaxException(ex.getMessage());
						}
					}
					else if(tipo_rectificacion.equalsIgnoreCase("2"))
					{
						String llamada="crearRectificativaPorIncluirExcluirVentas.action?servicio=crearRectificativaPorIncluirExcluirVentas&motivo="+idMotivo+"&idfactura="+idFactura+"&ventas="+URLEncoder.encode(listIds, "UTF-8");
						try {
							json = Tools.callServiceJSON(request, llamada);
							
						} catch (Exception ex) {
							throw new AjaxException(ex.getMessage());
						}
					}
					else
					{
						String llamada="rectificarFactura.action?motivo="+URLEncoder.encode("<motivo>"+idMotivo+"</motivo>", "UTF-8")+"&listaventas="+URLEncoder.encode(xml_venta, "UTF-8");
	//					llamada ="rectificarFactura.action?motivo=%3Cmotivo%3EError%20en%20datos%20cliente%3C%2Fmotivo%3E&listaventas=%3Clist%3E%3CVenta%20idventa%3D%225712766%22%3E%3Cidventa%3E5712766%3C%2Fidventa%3E%3Cfechayhoraventa%3E20%2F02%2F2018%2D10%3A21%3A59%3C%2Ffechayhoraventa%3E%3Cimportetotalventa%3E7%2E20%3C%2Fimportetotalventa%3E%3Cimporteparcialtotalventa%3E7%2E20%3C%2Fimporteparcialtotalventa%3E%3Ccliente%3E%3Cidcliente%3E20%3C%2Fidcliente%3E%3Cnombre%3EVIAJES%20MR%2E%20CHARLY%3C%2Fnombre%3E%3Cnombrecompleto%3EVIAJES%20MR%2E%20CHARLY%3C%2Fnombrecompleto%3E%3Capellido1%2F%3E%3Capellido2%2F%3E%3Crazonsocial%3EVIAJES%20MR%2E%20CHARLY%3C%2Frazonsocial%3E%3Cpersonacontactofactura%2F%3E%3Cdireccionenvio%3E%3Ciddireccionenvio%3E2%3C%2Fiddireccionenvio%3E%3Cnombre%3EFiscal%3C%2Fnombre%3E%3C%2Fdireccionenvio%3E%3Ctipoclienteactivo%3E%3Cidtipocliente%3E1287%3C%2Fidtipocliente%3E%3Cnombre%3EPrepago%2010%25%3C%2Fnombre%3E%3Cporcentajedescuento%3E10%2E00%3C%2Fporcentajedescuento%3E%3Cporcentajecomision%2F%3E%3C%2Ftipoclienteactivo%3E%3C%2Fcliente%3E%3Cfacturada%3E1%3C%2Ffacturada%3E%3Ctipoventa%3E%3Cidtipoventa%3E4%3C%2Fidtipoventa%3E%3C%2Ftipoventa%3E%3Clineadetalles%3E%3CLineadetalle%3E%3Cidlineadetalle%3E19083030%3C%2Fidlineadetalle%3E%3Ccantidad%3E2%3C%2Fcantidad%3E%3Cimporte%3E16%3C%2Fimporte%3E%3Cimporteiva%3E0%2E65%3C%2Fimporteiva%3E%3Cimportedescuentocliente%3E0%2E73%3C%2Fimportedescuentocliente%3E%3Cimportedescuentopromo%3E0%2E00%3C%2Fimportedescuentopromo%3E%3Cproducto%3E%3Cidproducto%3E441%3C%2Fidproducto%3E%3Cnombre%3EMUSEO%3C%2Fnombre%3E%3Civa%3E%3Cporcentaje%3E8%2E00%3C%2Fporcentaje%3E%3C%2Fiva%3E%3Ctipoprodproductos%3E%3CTipoprodproducto%3E%3Ctipoproducto%3E%3Cidtipoproducto%3E344%3C%2Fidtipoproducto%3E%3Cnombre%3EMuseo%20de%20las%20Ciencias%20Pr%C3%ADncipe%20Felipe%3C%2Fnombre%3E%3C%2Ftipoproducto%3E%3C%2FTipoprodproducto%3E%3C%2Ftipoprodproductos%3E%3C%2Fproducto%3E%3Cperfilvisitante%3E%3Cidperfilvisitante%3E20000%3C%2Fidperfilvisitante%3E%3Cnombre%3EAdulto%3C%2Fnombre%3E%3Ctarifa%3E%3Cidtarifa%3E297%3C%2Fidtarifa%3E%3Cnombre%3EPARTICULAR%3C%2Fnombre%3E%3C%2Ftarifa%3E%3C%2Fperfilvisitante%3E%3Cdescuentopromocional%3E%3Ciddescuentopromocional%3E%3C%2Fiddescuentopromocional%3E%3Cnombre%3E%3C%2Fnombre%3E%3C%2Fdescuentopromocional%3E%3Clineadetallezonasesions%3E%3CLineadetallezonasesion%3E%3Cidlineadetallezonasesion%3E19083031%3C%2Fidlineadetallezonasesion%3E%3Czonasesion%3E%3Cidzonasesion%3E1384473%3C%2Fidzonasesion%3E%3Cnumlibres%3E14997%3C%2Fnumlibres%3E%3Csesion%3E%3Cidsesion%3E1384145%3C%2Fidsesion%3E%3Cfecha%3E20%2F02%2F2018%2D00%3A00%3A00%3C%2Ffecha%3E%3Chorainicio%3E10%3A00%3C%2Fhorainicio%3E%3Ccontenido%3E%3Cidcontenido%3E384%3C%2Fidcontenido%3E%3Cnombre%3EExposiciones%20Museu%3C%2Fnombre%3E%3Cdescripcion%3EExposiciones%20Museu%3C%2Fdescripcion%3E%3Ctipoproducto%3E%3Cidtipoproducto%3E344%3C%2Fidtipoproducto%3E%3Cnombre%3EMuseo%20de%20las%20Ciencias%20Pr%C3%ADncipe%20Felipe%3C%2Fnombre%3E%3C%2Ftipoproducto%3E%3C%2Fcontenido%3E%3Cnombre%3EExposiciones%20Museu%2020%2DFeb%2D2018%2010%3A00%3C%2Fnombre%3E%3C%2Fsesion%3E%3Czona%3E%3Cidzona%3E219%3C%2Fidzona%3E%3C%2Fzona%3E%3C%2Fzonasesion%3E%3C%2FLineadetallezonasesion%3E%3C%2Flineadetallezonasesions%3E%3CbonosForLineacanje%2F%3E%3Ctarifaproducto%3E%3Cimporte%3E8%2E00%3C%2Fimporte%3E%3Cidtarifaproducto%3E4443%3C%2Fidtarifaproducto%3E%3C%2Ftarifaproducto%3E%3Cdetalles%3E%3CDetalle%3EExposiciones%20Museu%2020%2DFeb%2D2018%2010%3A00%3C%2FDetalle%3E%3C%2Fdetalles%3E%3Cdescuento%3E%3Cporcentajedescuento%3E10%2E00%3C%2Fporcentajedescuento%3E%3Ciddescuento%3E1288%3C%2Fiddescuento%3E%3C%2Fdescuento%3E%3Cperfiles%3E%3CPerfilvisitante%3E%3Cidperfilvisitante%3E20000%3C%2Fidperfilvisitante%3E%3Cnombre%3EAdulto%3C%2Fnombre%3E%3Ctarifa%3E%3Cidtarifa%3E297%3C%2Fidtarifa%3E%3Cnombre%3EPARTICULAR%3C%2Fnombre%3E%3CTarifaproducto%3E%3Cidtarifaproducto%3E4443%3C%2Fidtarifaproducto%3E%3Cimporte%3E8%2E00%3C%2Fimporte%3E%3C%2FTarifaproducto%3E%3C%2Ftarifa%3E%3C%2FPerfilvisitante%3E%3CPerfilvisitante%3E%3Cidperfilvisitante%3E20001%3C%2Fidperfilvisitante%3E%3Cnombre%3ENi%C3%B1o%3C%2Fnombre%3E%3Ctarifa%3E%3Cidtarifa%3E300%3C%2Fidtarifa%3E%3Cnombre%3EREDUCIDA%3C%2Fnombre%3E%3CTarifaproducto%3E%3Cidtarifaproducto%3E4450%3C%2Fidtarifaproducto%3E%3Cimporte%3E6%2E00%3C%2Fimporte%3E%3C%2FTarifaproducto%3E%3C%2Ftarifa%3E%3C%2FPerfilvisitante%3E%3CPerfilvisitante%3E%3Cidperfilvisitante%3E20002%3C%2Fidperfilvisitante%3E%3Cnombre%3EJubilado%20y%2Fo%20Pensionista%3C%2Fnombre%3E%3Ctarifa%3E%3Cidtarifa%3E300%3C%2Fidtarifa%3E%3Cnombre%3EREDUCIDA%3C%2Fnombre%3E%3CTarifaproducto%3E%3Cidtarifaproducto%3E4450%3C%2Fidtarifaproducto%3E%3Cimporte%3E6%2E00%3C%2Fimporte%3E%3C%2FTarifaproducto%3E%3C%2Ftarifa%3E%3C%2FPerfilvisitante%3E%3CPerfilvisitante%3E%3Cidperfilvisitante%3E20003%3C%2Fidperfilvisitante%3E%3Cnombre%3EDiscapacitado%3C%2Fnombre%3E%3Ctarifa%3E%3Cidtarifa%3E300%3C%2Fidtarifa%3E%3Cnombre%3EREDUCIDA%3C%2Fnombre%3E%3CTarifaproducto%3E%3Cidtarifaproducto%3E4450%3C%2Fidtarifaproducto%3E%3Cimporte%3E6%2E00%3C%2Fimporte%3E%3C%2FTarifaproducto%3E%3C%2Ftarifa%3E%3C%2FPerfilvisitante%3E%3C%2Fperfiles%3E%3Cnumerobonoagencia%3E%3C%2Fnumerobonoagencia%3E%3C%2FLineadetalle%3E%3C%2Flineadetalles%3E%3CcanalByIdcanal%3E%3Cidcanal%3E2%3C%2Fidcanal%3E%3Cnombre%3ETaquillas%20Museu%3C%2Fnombre%3E%3Ctipocanal%3E%3Cidtipocanal%3E1%3C%2Fidtipocanal%3E%3Cdirecto%3E1%3C%2Fdirecto%3E%3C%2Ftipocanal%3E%3C%2FcanalByIdcanal%3E%3CcanalByCanalventa%2F%3E%3CporcentajesDescuentoLD%2F%3E%3Cmodificacions%3E%3CModificacion%3E%3Cidmodificacion%2F%3E%3Cmotivomodificacion%3EModificaci%C3%B3n%20de%20fecha%2Fhoras%20de%20visita%3C%2Fmotivomodificacion%3E%3Cobservaciones%3E%3C%2Fobservaciones%3E%3Cventa%3E%3Cidventa%3E5712766%3C%2Fidventa%3E%3Centradasimpresas%3Efalse%3C%2Fentradasimpresas%3E%3Creciboimpreso%3E0%3C%2Freciboimpreso%3E%3CimprimirSoloEntradasXDefecto%3Efalse%3C%2FimprimirSoloEntradasXDefecto%3E%3C%2Fventa%3E%3Creserva%3E%3Cidreserva%2F%3E%3C%2Freserva%3E%3Cmodificacionimporteparcials%3E%3CModificacionimporteparcial%3E%3Cimporteparcial%3E%3Cformapago%3E%3Cidformapago%3E%3C%2Fidformapago%3E%3C%2Fformapago%3E%3Cbono%3E%3Cnumeracion%2F%3E%3C%2Fbono%3E%3Cimporte%3E%3C%2Fimporte%3E%3Crts%2F%3E%3Cnumpedido%2F%3E%3Canulado%2F%3E%3C%2Fimporteparcial%3E%3C%2FModificacionimporteparcial%3E%3CModificacionimporteparcial%3E%3Cimporteparcial%3E%3Cformapago%3E%3Cidformapago%3E%3C%2Fidformapago%3E%3C%2Fformapago%3E%3Cbono%3E%3Cnumeracion%2F%3E%3C%2Fbono%3E%3Cimporte%3E%3C%2Fimporte%3E%3Crts%2F%3E%3Cnumpedido%2F%3E%3Canulado%2F%3E%3C%2Fimporteparcial%3E%3C%2FModificacionimporteparcial%3E%3CModificacionimporteparcial%3E%3Cimporteparcial%3E%3Cformapago%3E%3Cidformapago%3E%3C%2Fidformapago%3E%3C%2Fformapago%3E%3Cbono%3E%3Cnumeracion%2F%3E%3C%2Fbono%3E%3Cimporte%3E%3C%2Fimporte%3E%3Crts%2F%3E%3Cnumpedido%2F%3E%3Canulado%2F%3E%3C%2Fimporteparcial%3E%3C%2FModificacionimporteparcial%3E%3C%2Fmodificacionimporteparcials%3E%3Cmodificacionlineadetalles%3E%3CModificacionlineadetalle%3E%3Clineadetalle%3E%3Cidlineadetalle%2F%3E%3C%2Flineadetalle%3E%3C%2FModificacionlineadetalle%3E%3CModificacionlineadetalle%3E%3Ctipomodificacion%3E0%3C%2Ftipomodificacion%3E%3Clineadetalle%3E%3Cidlineadetalle%3E19083030%3C%2Fidlineadetalle%3E%3Ccantidad%3E2%3C%2Fcantidad%3E%3Cimporte%3E16%3C%2Fimporte%3E%3Cimporteiva%3E0%2E65%3C%2Fimporteiva%3E%3Cimportedescuentocliente%3E0%2E73%3C%2Fimportedescuentocliente%3E%3Cimportedescuentopromo%3E0%2E00%3C%2Fimportedescuentopromo%3E%3Cproducto%3E%3Cidproducto%3E441%3C%2Fidproducto%3E%3Cnombre%3EMUSEO%3C%2Fnombre%3E%3Civa%3E%3Cporcentaje%3E8%2E00%3C%2Fporcentaje%3E%3C%2Fiva%3E%3Ctipoprodproductos%3E%3CTipoprodproducto%3E%3Ctipoproducto%3E%3Cidtipoproducto%3E344%3C%2Fidtipoproducto%3E%3Cnombre%3EMuseo%20de%20las%20Ciencias%20Pr%C3%ADncipe%20Felipe%3C%2Fnombre%3E%3C%2Ftipoproducto%3E%3C%2FTipoprodproducto%3E%3C%2Ftipoprodproductos%3E%3C%2Fproducto%3E%3Cperfilvisitante%3E%3Cidperfilvisitante%3E20000%3C%2Fidperfilvisitante%3E%3Cnombre%3EAdulto%3C%2Fnombre%3E%3Ctarifa%3E%3Cidtarifa%3E297%3C%2Fidtarifa%3E%3Cnombre%3EPARTICULAR%3C%2Fnombre%3E%3C%2Ftarifa%3E%3C%2Fperfilvisitante%3E%3Cdescuentopromocional%3E%3Ciddescuentopromocional%3E%3C%2Fiddescuentopromocional%3E%3Cnombre%3E%3C%2Fnombre%3E%3C%2Fdescuentopromocional%3E%3Clineadetallezonasesions%3E%3CLineadetallezonasesion%3E%3Cidlineadetallezonasesion%3E19083031%3C%2Fidlineadetallezonasesion%3E%3Czonasesion%3E%3Cidzonasesion%3E1384473%3C%2Fidzonasesion%3E%3Cnumlibres%3E14997%3C%2Fnumlibres%3E%3Csesion%3E%3Cidsesion%3E1384145%3C%2Fidsesion%3E%3Cfecha%3E20%2F02%2F2018%2D00%3A00%3A00%3C%2Ffecha%3E%3Chorainicio%3E10%3A00%3C%2Fhorainicio%3E%3Ccontenido%3E%3Cidcontenido%3E384%3C%2Fidcontenido%3E%3Cnombre%3EExposiciones%20Museu%3C%2Fnombre%3E%3Cdescripcion%3EExposiciones%20Museu%3C%2Fdescripcion%3E%3Ctipoproducto%3E%3Cidtipoproducto%3E344%3C%2Fidtipoproducto%3E%3Cnombre%3EMuseo%20de%20las%20Ciencias%20Pr%C3%ADncipe%20Felipe%3C%2Fnombre%3E%3C%2Ftipoproducto%3E%3C%2Fcontenido%3E%3Cnombre%3EExposiciones%20Museu%2020%2DFeb%2D2018%2010%3A00%3C%2Fnombre%3E%3C%2Fsesion%3E%3Czona%3E%3Cidzona%3E219%3C%2Fidzona%3E%3C%2Fzona%3E%3C%2Fzonasesion%3E%3C%2FLineadetallezonasesion%3E%3C%2Flineadetallezonasesions%3E%3CbonosForLineacanje%2F%3E%3Ctarifaproducto%3E%3Cimporte%3E8%2E00%3C%2Fimporte%3E%3Cidtarifaproducto%3E4443%3C%2Fidtarifaproducto%3E%3C%2Ftarifaproducto%3E%3Cdetalles%3E%3CDetalle%3EExposiciones%20Museu%2020%2DFeb%2D2018%2010%3A00%3C%2FDetalle%3E%3C%2Fdetalles%3E%3Cdescuento%3E%3Cporcentajedescuento%3E10%2E00%3C%2Fporcentajedescuento%3E%3Ciddescuento%3E1288%3C%2Fiddescuento%3E%3C%2Fdescuento%3E%3Cperfiles%3E%3CPerfilvisitante%3E%3Cidperfilvisitante%3E20000%3C%2Fidperfilvisitante%3E%3Cnombre%3EAdulto%3C%2Fnombre%3E%3Ctarifa%3E%3Cidtarifa%3E297%3C%2Fidtarifa%3E%3Cnombre%3EPARTICULAR%3C%2Fnombre%3E%3CTarifaproducto%3E%3Cidtarifaproducto%3E4443%3C%2Fidtarifaproducto%3E%3Cimporte%3E8%2E00%3C%2Fimporte%3E%3C%2FTarifaproducto%3E%3C%2Ftarifa%3E%3C%2FPerfilvisitante%3E%3CPerfilvisitante%3E%3Cidperfilvisitante%3E20001%3C%2Fidperfilvisitante%3E%3Cnombre%3ENi%C3%B1o%3C%2Fnombre%3E%3Ctarifa%3E%3Cidtarifa%3E300%3C%2Fidtarifa%3E%3Cnombre%3EREDUCIDA%3C%2Fnombre%3E%3CTarifaproducto%3E%3Cidtarifaproducto%3E4450%3C%2Fidtarifaproducto%3E%3Cimporte%3E6%2E00%3C%2Fimporte%3E%3C%2FTarifaproducto%3E%3C%2Ftarifa%3E%3C%2FPerfilvisitante%3E%3CPerfilvisitante%3E%3Cidperfilvisitante%3E20002%3C%2Fidperfilvisitante%3E%3Cnombre%3EJubilado%20y%2Fo%20Pensionista%3C%2Fnombre%3E%3Ctarifa%3E%3Cidtarifa%3E300%3C%2Fidtarifa%3E%3Cnombre%3EREDUCIDA%3C%2Fnombre%3E%3CTarifaproducto%3E%3Cidtarifaproducto%3E4450%3C%2Fidtarifaproducto%3E%3Cimporte%3E6%2E00%3C%2Fimporte%3E%3C%2FTarifaproducto%3E%3C%2Ftarifa%3E%3C%2FPerfilvisitante%3E%3CPerfilvisitante%3E%3Cidperfilvisitante%3E20003%3C%2Fidperfilvisitante%3E%3Cnombre%3EDiscapacitado%3C%2Fnombre%3E%3Ctarifa%3E%3Cidtarifa%3E300%3C%2Fidtarifa%3E%3Cnombre%3EREDUCIDA%3C%2Fnombre%3E%3CTarifaproducto%3E%3Cidtarifaproducto%3E4450%3C%2Fidtarifaproducto%3E%3Cimporte%3E6%2E00%3C%2Fimporte%3E%3C%2FTarifaproducto%3E%3C%2Ftarifa%3E%3C%2FPerfilvisitante%3E%3C%2Fperfiles%3E%3Cnumerobonoagencia%3E%3C%2Fnumerobonoagencia%3E%3C%2Flineadetalle%3E%3C%2FModificacionlineadetalle%3E%3C%2Fmodificacionlineadetalles%3E%3Canulaciondesdecaja%2F%3E%3C%2FModificacion%3E%3C%2Fmodificacions%3E%3C%2FVenta%3E%3C%2Flist%3E";
						try {
							json = Tools.callServiceJSON(request, llamada);
							
						} catch (Exception ex) {
							throw new AjaxException(ex.getMessage());
						}
					}
					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
					
					
				}	
				
				// ***************************************************************************************************
				@RequestMapping("ajax/facturacion/ventareserva/show_disponibles.do")
				public ModelAndView showDisponibles(
						@RequestParam(value = "idproducto", required = false, defaultValue = "") String idProducto,
						@RequestParam(value = "idcliente", required = false, defaultValue = "") String idCliente,
						@RequestParam(value = "idtipoventa", required = false, defaultValue = "4") String idTipoVenta,
						@RequestParam(value = "producto", required = false, defaultValue = "") String producto,
						@RequestParam(value = "fecha", required = false, defaultValue = "") String fecha,
						@RequestParam(value = "button", required = false, defaultValue = "") String buttonAdd,
						@RequestParam(value = "list", required = false, defaultValue = "") String list,
						@RequestParam(value = "edicion", required = false, defaultValue = "0") String edicion,
						@RequestParam(value = "map", required = false, defaultValue = "<map />") String map,
						HttpServletRequest request) throws Exception {
				
					ModelAndView model = new ModelAndView();

					model.setViewName("app.facturacion.facturas.factura.rectificarLineas.disponibles");
					
					model.addObject("buttonAdd",buttonAdd);
					model.addObject("list",list);
					model.addObject("idProducto",idProducto);
					model.addObject("idCliente",idCliente);
					model.addObject("idtipoventa",idTipoVenta);
					model.addObject("producto",producto);
					model.addObject("fecha",fecha);
					model.addObject("edicion",edicion);

					String xml_sesiones= Tools.callServiceXML(request, "obtenerSesionesPorProductoYFecha.action?servicio=obtenerSesionesPorProductoYFecha&fecha="+URLEncoder.encode(fecha,"UTF-8")+"&idproducto="+idProducto+"&map="+URLEncoder.encode(map,"UTF-8"));
					//String xml_sesiones= Tools.callServiceXML(request, "obtenerSesionesPorProductoYFecha.action?servicio=obtenerSesionesPorProductoYFecha&fecha="+URLEncoder.encode(fecha,"UTF-8")+"&idproducto="+idProducto+"&map="+URLEncoder.encode("<map />","UTF-8")); 
					JSONObject json_sesiones = XML.toJSONObject(xml_sesiones);
					if (json_sesiones.has("error")) throw new Exception(json_sesiones.getString("error"));

					model.addObject("disponibles_datos_sesiones",xml_sesiones);
					model.addObject("disponibles_datos_sesiones_json",json_sesiones.toString(4));

					return model;
				}
				

				// ***********************************************************************************
				@RequestMapping(value = "ajax/facturacion/agruparFacturasParaImprimir.do", method = RequestMethod.POST)
				public ResponseEntity<?> agruparFacturasParaImprimir(
						HttpServletRequest request) throws Exception {
					HttpHeaders responseHeaders = new HttpHeaders();
					String json="";
					try {
						json = Tools.callServiceXML(request, "agruparFacturasNoImpresas.action?servicio=agruparFacturasNoImpresas");
						json = json.replace("<long>", "").replace("</long>", "");						
					} catch (Exception ex) {
						throw new AjaxException(ex.getMessage());
					}
					
					
					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}
				
				//*************************************************************************************
				@RequestMapping("/ajax/facturacion/bono/modificarLineas.do")
				public ModelAndView bonoModificarLineas(
						HttpServletRequest request) throws Exception {
					ModelAndView model = new ModelAndView();
					
					model.addObject("ventareserva_productos_principales", Tools.callServiceXML(request, "obtenerListadoProductosPrincipales.action?servicio=obtenerListadoProductosPrincipales"));
					model.addObject("ventareserva_canales_indirectos", Tools.callServiceXML(request, "obtenerListadoCanalesIndirectos.action?servicio=obtenerListadoCanalesIndirectos"));
									
					model.setViewName("app.facturacion.facturas.bono.editarLineas");
					return model;
				}
				//*************************************************************************************
				
				@RequestMapping("ajax/facturacion/bono/show_edit_emision_bono.do")
				public ModelAndView showEditEmisionBono(
						HttpServletRequest request) throws Exception {
				
					ModelAndView model = new ModelAndView();

					
					model.setViewName("app.facturacion.facturas.bono.edit_emision_bono");
					
					model.addObject("ventareserva_listado_tiposbono", Tools.callServiceXML(request, "obtenerListadoTiposBono.action"));
					
					return model;
				}
				
				// ***************************************************************************************************
				
				// ***********************************************************************************
				@RequestMapping(value = "ajax/facturacion/buscarClientePorId.do", method = RequestMethod.POST)
				public ResponseEntity<?> buscarClientePorId(
						@RequestParam(value = "idcliente", required = false, defaultValue = "") String idCliente,
						HttpServletRequest request) throws Exception {
					
					String json="";
					try {
						json = Tools.callServiceJSON(request, "obtenerClientePorId.action?servicio=obtenerClientePorId&xml="+idCliente);
						
					} catch (Exception ex) {
						
						throw new AjaxException(ex.getMessage());
					}				
					
					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");					
					
					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
					
				}
				

				// ***********************************************************************************
				@RequestMapping(value = "ajax/facturacion/facturas/grupos/remove_grupo.do", method = RequestMethod.POST)
				public ResponseEntity<?> eliminarGrupo(
						@RequestParam(value = "data", required = false, defaultValue = "") String idGrupo,
						HttpServletRequest request) throws Exception {
					
					String json="";
					try {
						json = Tools.callServiceJSON(request, "darDeBajaGruposEmpresas.action?servicio=darDeBajaGruposEmpresas&xml=<list><int>"+idGrupo+"</int></list>");
						
					} catch (Exception ex) {
						
						throw new AjaxException(ex.getMessage());
					}				
					
					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");					
					
					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
					
				}
				

				//*************************************************************************************
				
				@RequestMapping("ajax/facturacion/show_mensaje_impresion_masiva.do")
				public ModelAndView showMensajeImpresionMasiva(
						HttpServletRequest request) throws Exception {
				
					ModelAndView model = new ModelAndView();
					
					model.setViewName("app.facturacion.facturas.impresion.masiva.show");				
										
					return model;
				}
				
				// ***********************************************************************************
				@RequestMapping(value = "ajax/facturacion/actualizar_impresion_masiva.do", method = RequestMethod.POST)
				public ResponseEntity<?> actualizarImpresionMasiva(
						@RequestParam(value = "id_grupo", required = false, defaultValue = "") String idGrupo,
						HttpServletRequest request) throws Exception {
					
					String json="";
					try {
						json = Tools.callServiceJSON(request, "marcarGrupoFacturasComoImpresas.action?servicio=marcarGrupoFacturasComoImpresas&xml=<long>"+idGrupo+"</long>");
						
					} catch (Exception ex) {
						
						throw new AjaxException(ex.getMessage());
					}				
					
					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");					
					
					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
					
				}				
				
				
}

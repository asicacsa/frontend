package es.cac.colossus.frontend.web.controllers;

import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import es.cac.colossus.frontend.utils.AjaxException;
import es.cac.colossus.frontend.utils.Tools;

@Controller
public class OperacionesNumerada {

	@Autowired
	private MessageSource messageSource;

	@RequestMapping("venta/numerada/operaciones.do")
	public ModelAndView showOperacionesNumeradas(HttpServletRequest request) throws Exception {
				
		ModelAndView model = new ModelAndView();

		model.setViewName("app.venta.numerada.operaciones");
		model.addObject("title", messageSource.getMessage("venta.numerada.title", null, null));
		model.addObject("menu", Tools.getUserMainMenu(request));
		
		
		Tools.callServiceXML(request, "darDeBajaPrerreservaActual.action?servicio=darDeBajaPrerreservaActual");
		model.addObject("numerada_temporadas_programacion", Tools.callServiceJSON(request, "obtenerListadoTemporadasProgramacion.action?servicio=obtenerListadoTemporadasProgramacion"));
		model.addObject("numerada_tipos_producto", Tools.callServiceXML(request, "obtenerListadoTiposProductoVentaNumerada.action?servicio=obtenerListadoTiposProductoVentaNumerada"));	
		
		return model;
	}

	// ***************************************************************************************************
	
	
	
	
	
}



package es.cac.colossus.frontend.web.controllers;


import java.io.IOException;
import java.net.ConnectException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import es.cac.colossus.frontend.utils.AjaxException;

/*
*   ATENCION: Esta clase implementa el control de errores en Spring
*   
*   Para que esta clase funcione es necesario añadir en el WebAppInitializer 
*   registration.setInitParameter("throwExceptionIfNoHandlerFound","true");
*
*   Más info: http://stackoverflow.com/questions/13356549/handle-error-404-with-spring-controller
*/

@ControllerAdvice
public class AppExceptionController
{

	private static final Log log = LogFactory.getLog(AppExceptionController.class);

	
	//*****************************************************************************************************
	@ExceptionHandler(Exception.class)
	public ModelAndView handleError500(Exception ex)
	{
    	ModelAndView model = new ModelAndView();
    	model.setViewName("app.error.500");
    	model.addObject("exception", ExceptionUtils.getStackTrace(ex).substring(0,500)+" ...");
		
    	log.error("Error Java 500 debido a: "+ExceptionUtils.getStackTrace(ex));
    	    	
        return model;		
	}
	
	

	//*****************************************************************************************************
	@ExceptionHandler(ConnectException.class)
	public Object handleConnectionError(Exception ex)
	{	
		String errorCode=ex.getMessage();
		
    	log.error("Error "+errorCode+": "+ExceptionUtils.getStackTrace(ex));
    	    	
    	if (errorCode.equalsIgnoreCase("401")) {
        	ModelAndView model = new ModelAndView();
        	model.setViewName("app.error.403");
        	model.addObject("exception", ExceptionUtils.getStackTrace(ex).substring(0,500)+" ...");
        	return model;
    	}
    	
    	if (errorCode.equalsIgnoreCase("401-TIMEOUT")) {
    		return "redirect:/login.do?timeout=1";
    	}
    	
    	ModelAndView model = new ModelAndView();
    	model.setViewName("app.error.500");
    	model.addObject("exception", ExceptionUtils.getStackTrace(ex).substring(0,500)+" ...");
		
    	log.error("Error Java 500 debido a: "+ExceptionUtils.getStackTrace(ex));
    	    	
        return model;	
	}
	
	
	//*****************************************************************************************************
	@ExceptionHandler(AjaxException.class)
	public Object handleAjaxError(AjaxException ex)
	{	
		String errorCode=ex.getMessage();
		
    	log.error("Error AJAX "+errorCode+": "+ExceptionUtils.getStackTrace(ex));    	    
    	
    	if(errorCode.equalsIgnoreCase("401"))
    		errorCode = "Se ha perdido la sesión, por favor vuelva a loguearse";
    	
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/plain; charset=ISO-8859-1");

		return new ResponseEntity<String>(errorCode, responseHeaders, HttpStatus.BAD_REQUEST);	
	}
}

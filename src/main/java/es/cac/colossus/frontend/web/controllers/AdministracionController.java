package es.cac.colossus.frontend.web.controllers;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import es.cac.colossus.frontend.utils.AjaxException;
import es.cac.colossus.frontend.utils.Tools;

@Controller
public class AdministracionController {

	@Autowired
	private MessageSource messageSource;



	// *****************************************************************************************************
	@RequestMapping("admon/seguridad/seguridad.do")
	public ModelAndView showSeguridad(HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		
		model.setViewName("app.admon.seguridad.seguridad");

		model.addObject("title", messageSource.getMessage("administracion.seguridad.title", null, null));

		model.addObject("menu", Tools.getUserMainMenu(request));

		model.addObject("tabusuarios_selector_grupos", Tools.callServiceXML(request, "obtenerComunForCombo.action?servicio=obtenerGruposSinInternet"));
		model.addObject("tabusuarios_selector_unidades", Tools.callServiceXML(request, "obtenerComunForCombo.action?servicio=obtenerListadoUnidadesNegocio"));
		model.addObject("tabusuarios_selector_perfiles", Tools.callServiceXML(request, "obtenerComunForCombo.action?servicio=obtenerListadoPerfilesUsuario"));

		model.addObject("tabauditoria_datos_auditoria", Tools.callServiceXML(request, "getObtenerAuditoria.action?servicio=modelService"));

		String xmlFuncionalidades=Tools.callServiceXML(request, "obtenerClasificacionesFuncionalidades.action?servicio=obtenerClasificacionesFuncionalidades");
		
		model.addObject("json_selector_funcionalidades", Tools.generateSelect2TreeJSON(xmlFuncionalidades, "ArrayList", "Clasiffuncionalidades", "Funcionalidad", "idclasiffuncionalidades", "idfuncionalidad"));


		return model;
	}



	// *****************************************************************************************************
	@RequestMapping("ajax/admon/seguridad/seguridad/show_user.do")
	public ModelAndView seguridadShowUser(@RequestParam(value = "id", required = false, defaultValue = "") String idUser, HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		if (idUser.length() > 0)
			model.addObject("editarusuario_datos_usuario", Tools.callServiceXML(request, "getEdicionAltaUsuario.action?servicio=obtenerUsuarioPorId&xml=" + idUser));
		else
			model.addObject("editarusuario_datos_usuario", "<empty/>");

		model.addObject("editarusuario_selector_unidades", Tools.callServiceXML(request, "obtenerComunForCombo.action?servicio=obtenerListadoUnidadesNegocio"));
		model.addObject("editarusuario_selector_grupos", Tools.callServiceXML(request, "obtenerComunForCombo.action?servicio=obtenerListadoGrupos"));
		model.addObject("editarusuario_selector_perfiles", Tools.callServiceXML(request, "obtenerComunForCombo.action?servicio=obtenerListadoPerfilesUsuario"));

		model.addObject("editarusuario_selector_canal", Tools.callServiceXML(request, "obtenerListadoCanales.action?servicio=obtenerListadoCanales"));

		model.setViewName("app.admon.seguridad.seguridad.dialog.edit_usuario");

		return model;
	}



	// *****************************************************************************************************
	@RequestMapping("ajax/change_user_password_by_admin.do")
	public ResponseEntity<String> changeUserPasswordByAdmin(@RequestParam(value = "id_usuario", required = false, defaultValue = "") String idUsuario, @RequestParam(value = "new_password", required = false, defaultValue = "") String newPassword, @RequestParam(value = "repeat_password", required = false, defaultValue = "") String repeatPassword, HttpServletRequest request) throws Exception {

		String xml = Tools.callServiceXML(request, "cambiarContrasenyaAdministrador.action?servicio=cambiarContrasenyaAdministrador&confPass=" + repeatPassword + "&newPass=" + newPassword + "&idusuario=" + idUsuario);

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}



	// *****************************************************************************************************
	@RequestMapping("ajax/admon/seguridad/seguridad/save_user.do")
	public ResponseEntity<String> seguridadSaveUser(@RequestParam(value = "idusuario", required = false, defaultValue = "") String idUser, @RequestParam(value = "nombre", required = false, defaultValue = "") String nombre, @RequestParam(value = "apellidos", required = false, defaultValue = "") String apellidos, @RequestParam(value = "dni", required = false, defaultValue = "") String dni, @RequestParam(value = "login", required = false, defaultValue = "") String login, @RequestParam(value = "contrasena", required = false, defaultValue = "") String contrasena, @RequestParam(value = "bloqueado", required = false, defaultValue = "") String bloqueado, @RequestParam(value = "auditado", required = false, defaultValue = "") String auditado, @RequestParam(value = "idcanal", required = false, defaultValue = "") String idCanal, @RequestParam(value = "unidades[]", required = false) List<String> unidades, @RequestParam(value = "grupos[]", required = false) List<String> grupos, @RequestParam(value = "perfiles[]", required = false) List<String> perfiles, HttpServletRequest request) throws Exception {

		String xmlUnidades = "";
		if (unidades != null) {
			Iterator<String> dataIterator = unidades.iterator();
			while (dataIterator.hasNext()) {
				xmlUnidades += "<Usuariounidadnegocio><unidadnegocio><idunidadnegocio>" + dataIterator.next() + "</idunidadnegocio></unidadnegocio></Usuariounidadnegocio>";
			}
		}

		String xmlPerfiles = "";
		if (perfiles != null) {
			Iterator<String> dataIterator = perfiles.iterator();
			while (dataIterator.hasNext()) {
				xmlPerfiles += "<Perfilusuariousuario><perfilusuario><idperfilusuario>" + dataIterator.next() + "</idperfilusuario></perfilusuario></Perfilusuariousuario>";
			}
		}

		String xmlGrupos = "";
		if (grupos != null) {
			Iterator<String> dataIterator = grupos.iterator();
			while (dataIterator.hasNext()) {
				xmlGrupos += "<Grupousuario><grupo><idgrupo>" + dataIterator.next() + "</idgrupo></grupo></Grupousuario>";
			}
		}

		String xml = Tools.callServiceXML(request, "postEdicionAltaUsuario.action?confirmcontra=" + contrasena + "&_modo=postEdicionAltaUsuario&servicio=postEdicionAltaUsuario&xml=" + URLEncoder.encode("<getEdicionAltaUsuario><Usuario><idusuario>" + idUser + "</idusuario><nombre>" + nombre + "</nombre><apellidos>" + apellidos + "</apellidos><dni>" + dni + "</dni><login>" + login + "</login><cuentabloqueada>" + (bloqueado.equalsIgnoreCase("on")) + "</cuentabloqueada><auditado>" + (auditado.equalsIgnoreCase("on")) + "</auditado><usuariounidadnegocios>" + xmlUnidades + "</usuariounidadnegocios><reds/><perfilusuariousuarios>" + xmlPerfiles + "</perfilusuariousuarios> <grupousuarios>" + xmlGrupos + "</grupousuarios><contrasenya>" + contrasena + "</contrasenya><canal><idcanal>" + idCanal + "</idcanal><descripcion></descripcion></canal></Usuario></getEdicionAltaUsuario>", "UTF-8"));

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}



	// ***************************************************************************************************
	@RequestMapping(value = "ajax/admon/seguridad/seguridad/list_users.do", method = RequestMethod.POST)
	public ResponseEntity<?> seguridadListUsers(@RequestParam(value = "activo", required = false, defaultValue = "") String activo, @RequestParam(value = "nombre", required = false, defaultValue = "") String nombre, @RequestParam(value = "apellidos", required = false, defaultValue = "") String apellidos, @RequestParam(value = "dni", required = false, defaultValue = "") String dni, @RequestParam(value = "idperfil", required = false, defaultValue = "") String idperfil, @RequestParam(value = "idgrupo", required = false, defaultValue = "") String idgrupo, @RequestParam(value = "idunidadnegocio", required = false, defaultValue = "") String idunidadnegocio
		, @RequestParam(value = "login", required = false, defaultValue = "") String login, HttpServletRequest request) throws Exception {

		String json = "";

		try {
			json = Tools.callServiceJSON(request, "postBuscarUsuarios.action?_modo=postBuscarUsuarios&servicio=buscarUsuarios&xml=<getBuscarUsuarios><Buscarusuarioparam><nombre>" + nombre + "</nombre><apellidos>" + apellidos + "</apellidos><login>"+login+"</login><dni>" + dni + "</dni><activo>" + activo + "</activo><idgrupo>" + idgrupo + "</idgrupo><idperfil>" + idperfil + "</idperfil><idunidadnegocio>" + idunidadnegocio + "</idunidadnegocio><numeroprimerregistro>0</numeroprimerregistro></Buscarusuarioparam></getBuscarUsuarios>");
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}



	// ***************************************************************************************************
	@RequestMapping(value = "ajax/admon/seguridad/seguridad/remove_users.do", method = RequestMethod.POST)
	public ResponseEntity<String> seguridadRemoveUsers(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {

		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request, "darDeBajaUsuarios.action?servicio=darDeBajaUsuarios&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}



	// ***************************************************************************************************

	@RequestMapping(value = "ajax/admon/seguridad/seguridad/lock_users.do", method = RequestMethod.POST)
	public ResponseEntity<String> seguridadLockUsers(@RequestParam(value = "data", required = true) List<String> data, @RequestParam(value = "lock") Boolean lock, HttpServletRequest request) throws Exception {

		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<Usuario><idusuario>" + dataIterator.next() + "</idusuario></Usuario>";
		}

		String xml = Tools.callServiceXML(request, "bloquearUsuarios.action?servicio=bloquearUsuarios&xml=<parametro><list>" + xmlList + "</list><java.lang.Boolean>" + lock + "</java.lang.Boolean></parametro>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}



	// ***************************************************************************************************
	// TAB: PERFILES
	// ***************************************************************************************************
	@RequestMapping(value = "ajax/admon/seguridad/seguridad/list_perfiles.do", method = RequestMethod.POST)
	public ResponseEntity<?> seguridadListPerfiles(HttpServletRequest request) throws Exception {

		String json = "";

		try {
			json = Tools.callServiceJSON(request, "obtenerPerfilesUsuario.action?servicio=obtenerPerfilesUsuario");
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// ***************************************************************************************************
	@RequestMapping(value = "ajax/admon/seguridad/seguridad/remove_perfiles.do", method = RequestMethod.POST)
	public ResponseEntity<String> seguridadRemovePerfiles(@RequestParam(value = "data", required = true) List<String> data,
			HttpServletRequest request) throws Exception {

		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request,
				"darDeBajaPerfilesUsuario.action?servicio=darDeBajaPerfilesUsuario&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}

	// *****************************************************************************************************
	@RequestMapping("ajax/admon/seguridad/seguridad/show_perfil.do")
	public ModelAndView seguridadShowPerfil(@RequestParam(value = "id", required = false, defaultValue = "") String idPerfil, HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		if (idPerfil.length() > 0)
			model.addObject("editarperfil_datos_perfil", Tools.callServiceXML(request, "getEdicionAltaPerfilUsuario.action?servicio=obtenerPerfilUsuarioPorId&xml=" + idPerfil));
		else
			model.addObject("editarperfil_datos_perfil", "<empty/>");

		model.addObject("editarperfil_selector_unidades", Tools.callServiceXML(request, "obtenerComunForCombo.action?servicio=obtenerListadoUnidadesNegocio"));
		model.addObject("editarperfil_selector_roles", Tools.callServiceXML(request, "obtenerListadoRolesJasper.action?servicio=obtenerListadoRolesJasper"));

		String xmlFuncionalidades=Tools.callServiceXML(request, "obtenerClasificacionesFuncionalidades.action?servicio=obtenerClasificacionesFuncionalidades");
		
		model.addObject("json_selector_funcionalidades", Tools.generateSelect2TreeJSON(xmlFuncionalidades, "ArrayList", "Clasiffuncionalidades", "Funcionalidad", "idclasiffuncionalidades", "idfuncionalidad"));		
		
		model.setViewName("app.admon.seguridad.seguridad.dialog.edit_perfil");

		return model;
	}

	// *****************************************************************************************************
	@RequestMapping("ajax/admon/seguridad/seguridad/save_perfil.do")
	public ResponseEntity<String> seguridadSavePerfil(@RequestParam(value = "idperfil", required = false, defaultValue = "") String idPerfil, 
												   @RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
												   @RequestParam(value = "descripcion", required = false, defaultValue = "") String descripcion,
												   @RequestParam(value = "idappdefecto", required = false, defaultValue = "") String idAppDefecto,
	 								 			   @RequestParam(value = "usuarios[]", required = false, defaultValue = "") List<String> usuarios,
												   @RequestParam(value = "idunidad", required = false, defaultValue = "") String idUnidad,
												   @RequestParam(value = "idrol", required = false, defaultValue = "") String idRol,
	 								 			   @RequestParam(value = "funcionalidades[]", required = false, defaultValue = "") List<String> funcionalidades,											   
 								 				   HttpServletRequest request) throws Exception {

		String xmlUsuarios = "";
		if (usuarios != null) {
			Iterator<String> dataIterator = usuarios.iterator();
			while (dataIterator.hasNext()) {
				xmlUsuarios += "<Perfilusuariousuario><usuario><idusuario>" + dataIterator.next() + "</idusuario></usuario></Perfilusuariousuario>";
			}
		}

		String xmlFuncionalidades = "";
		if (funcionalidades != null) {
			Iterator<String> dataIterator = funcionalidades.iterator();
			while (dataIterator.hasNext()) {
				xmlFuncionalidades += "<Perfilusufunc><funcionalidad><idfuncionalidad>" + dataIterator.next() + "</idfuncionalidad></funcionalidad></Perfilusufunc>";
			}
		}
	
		String xml = Tools.callServiceXML(request, "postEdicionAltaPerfilUsuario.action?_modo=postEdicionAltaPerfilUsuario&servicio=postEdicionAltaPerfilUsuario&xml=" + URLEncoder.encode("<getEdicionAltaPerfilUsuario><Perfilusuario><idperfilusuario>"+ idPerfil +"</idperfilusuario><nombre>" + nombre + "</nombre><descripcion>" + descripcion + "</descripcion><appdefecto>" + idAppDefecto + "</appdefecto><roljasper><idroljasper>" + idRol + "</idroljasper></roljasper><unidadnegocio><idunidadnegocio>" + idUnidad + "</idunidadnegocio></unidadnegocio><perfilusuariousuarios>" + xmlUsuarios + "</perfilusuariousuarios><perfilusufuncs>" + xmlFuncionalidades + "</perfilusufuncs></Perfilusuario></getEdicionAltaPerfilUsuario>", "UTF-8"));

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}		

	// ***************************************************************************************************
	@RequestMapping(value = "ajax/admon/seguridad/seguridad/select_list_usuario.do", method = RequestMethod.POST)
	public ResponseEntity<?> seguridadSelectListUsuarios(HttpServletRequest request) throws Exception {

		String json = Tools.callServiceJSON(request, "/obtenerListadoUsuariosFiltrado.action?servicio=obtenerListadoUsuariosFiltrado&selected=-1");

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// ***************************************************************************************************
	// TAB: GRUPOS
	// ***************************************************************************************************
	@RequestMapping(value = "ajax/admon/seguridad/seguridad/list_grupos.do", method = RequestMethod.POST)
	public ResponseEntity<?> seguridadListGrupos(HttpServletRequest request) throws Exception {

		String json = "";

		try {
			json = Tools.callServiceJSON(request, "/obtenerGrupos.action?servicio=obtenerGrupos");
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// *****************************************************************************************************
	@RequestMapping("ajax/admon/seguridad/seguridad/show_grupo.do")
	public ModelAndView seguridadShowGrupo(
			@RequestParam(value = "id", required = false, defaultValue = "") String idGrupo, HttpServletRequest request)
			throws Exception {

		ModelAndView model = new ModelAndView();

		if (idGrupo.length() > 0)
			model.addObject("editargrupo_datos_grupo", Tools.callServiceXML(request,
					"getEdicionAltaGrupo.action?servicio=obtenerGrupoUsuariosPorId&xml=" + idGrupo));
		else
			model.addObject("editargrupo_datos_grupo", "<empty/>");

		model.addObject("editargrupo_selector_unidades",
				Tools.callServiceXML(request, "/obtenerUnidadesNegocioPorFuncionalidad.action?servicio=obtenerUnidadesNegocioPorFuncionalidad&xml=%3Cint%3E57%3C%2Fint%3E"));
		
		model.setViewName("app.admon.seguridad.seguridad.dialog.edit_grupo");

		return model;
	}
	
	// *****************************************************************************************************
	@RequestMapping("ajax/admon/seguridad/seguridad/save_grupo.do")
	public ResponseEntity<String> seguridadSaveGrupo(@RequestParam(value = "idgrupo", required = false, defaultValue = "") String idGrupo, 
	 								 				 @RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
	 								 				 @RequestParam(value = "idunidad", required = false, defaultValue = "") String idUnidad,
	 								 				 @RequestParam(value = "usuarios[]", required = false) List<String> usuarios, 
 								 				 	 HttpServletRequest request) throws Exception {

		String xmlUsuarios = "";
		if (usuarios != null) {
			Iterator<String> dataIterator = usuarios.iterator();
			while (dataIterator.hasNext()) {
				xmlUsuarios += "<Grupousuario><usuario><idusuario>" + dataIterator.next() + "</idusuario></usuario></Grupousuario>";
			}
		}
	
		String xml = Tools.callServiceXML(request, "postEdicionAltaGrupo.action?_modo=postEdicionAltaGrupo&servicio=postEdicionAltaGrupo&xml=" + URLEncoder.encode("<getEdicionAltaGrupo><Grupo><idgrupo>" + idGrupo + "</idgrupo><nombre>" + nombre + "</nombre><grupousuarios>" + xmlUsuarios + "</grupousuarios><unidadnegocio><idunidadnegocio>" + idUnidad + "</idunidadnegocio></unidadnegocio></Grupo></getEdicionAltaGrupo>", "UTF-8"));

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}	

	// ***************************************************************************************************
	@RequestMapping(value = "ajax/admon/seguridad/seguridad/remove_grupos.do", method = RequestMethod.POST)
	public ResponseEntity<String> seguridadRemoveGrupos(@RequestParam(value = "data", required = true) List<String> data,
			HttpServletRequest request) throws Exception {

		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request,
				"darDeBajaGrupos.action?servicio=darDeBajaGrupos&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}

	// ***************************************************************************************************
	// TAB: REDES
	// ***************************************************************************************************
	@RequestMapping(value = "ajax/admon/seguridad/seguridad/list_redes.do", method = RequestMethod.POST)
	public ResponseEntity<?> seguridadListRedes(HttpServletRequest request) throws Exception {

		String json = "";

		try {
			json = Tools.callServiceJSON(request, "/obtenerRedes.action?servicio=obtenerRedes");
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}


	// *****************************************************************************************************
	@RequestMapping("ajax/admon/seguridad/seguridad/show_red.do")
	public ModelAndView seguridadShowRed(
			@RequestParam(value = "id", required = false, defaultValue = "") String idRed, HttpServletRequest request)
			throws Exception {

		ModelAndView model = new ModelAndView();

		if (idRed.length() > 0)
			model.addObject("editarred_datos_red", Tools.callServiceXML(request,
					"getEdicionAltaRed.action?servicio=obtenerRedPorId&xml=" + idRed));
		else
			model.addObject("editarred_datos_red", "<empty/>");
		
		model.addObject("editarred_selector_unidades",
				Tools.callServiceXML(request, "/obtenerUnidadesNegocioPorFuncionalidad.action?servicio=obtenerUnidadesNegocioPorFuncionalidad&xml=%3Cint%3E57%3C%2Fint%3E"));

		model.setViewName("app.admon.seguridad.seguridad.dialog.edit_red");

		return model;
	}
	
	// *****************************************************************************************************
	@RequestMapping("ajax/admon/seguridad/seguridad/save_red.do")
	public ResponseEntity<String> seguridadSaveRed(@RequestParam(value = "idred", required = false, defaultValue = "") String idRed, 
												   @RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
												   @RequestParam(value = "descripcion", required = false, defaultValue = "") String descripcion,
	 								 			   @RequestParam(value = "unidades[]", required = false, defaultValue = "") List<String> unidades,
	 								 			   @RequestParam(value = "idusuario", required = false) String idUsuario, 
 								 				   HttpServletRequest request) throws Exception {

		String xmlUnidades = "";
		String xmlRed="";
		if (unidades != null) {
			Iterator<String> dataIterator = unidades.iterator();
			while (dataIterator.hasNext()) {
				xmlUnidades += "<Redunidadnegocio><unidadnegocio><idunidadnegocio>" + dataIterator.next() + "</idunidadnegocio></unidadnegocio></Redunidadnegocio>";
			}
		}
		
		if(!idRed.equalsIgnoreCase(""))
			xmlRed = "<idred>"+ idRed +"</idred>";
		
			
	
		String xml = Tools.callServiceXML(request, "postEdicionAltaRed.action?_modo=postEdicionAltaRed&servicio=postEdicionAltaRed&xml=" + URLEncoder.encode("<getEdicionAltaRed><Red>"+xmlRed+"<nombre>" + nombre + "</nombre><descripcion>" + descripcion + "</descripcion><usuario><idusuario>" + idUsuario + "</idusuario></usuario><redunidadnegocios>" + xmlUnidades + "</redunidadnegocios></Red></getEdicionAltaRed>", "UTF-8"));

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}	

	// ***************************************************************************************************
	@RequestMapping(value = "ajax/admon/seguridad/seguridad/remove_redes.do", method = RequestMethod.POST)
	public ResponseEntity<String> seguridadRemoveRedes(@RequestParam(value = "data", required = true) List<String> data,
			HttpServletRequest request) throws Exception {

		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request,
				"darDeBajaRedes.action?servicio=darDeBajaRedes&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}

	// ***************************************************************************************************
	// TAB: RESTRICCIONES ENTRE REDES
	// ***************************************************************************************************
	@RequestMapping(value = "ajax/admon/seguridad/seguridad/list_restricciones.do", method = RequestMethod.POST)
	public ResponseEntity<?> seguridadListRestricciones(HttpServletRequest request) throws Exception {

		String json = "";

		try {
			json = Tools.callServiceJSON(request, "/obtenerRestriccionesRedes.action?servicio=obtenerRestriccionesRedes");
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}


	// *****************************************************************************************************
	@RequestMapping("ajax/admon/seguridad/seguridad/show_restriccion.do")
	public ModelAndView seguridadShowRestriccion(
			@RequestParam(value = "id", required = false, defaultValue = "") String idRestriccion, HttpServletRequest request)
			throws Exception {

		ModelAndView model = new ModelAndView();

		if (idRestriccion.length() > 0)
			model.addObject("editarrestriccion_datos_restriccion", Tools.callServiceXML(request,
					"getEdicionAltaRestriccionRedes.action?servicio=obtenerRestriccionRedPorId&xml=" + idRestriccion));
		else
			model.addObject("editarrestriccion_datos_restriccion", "<empty/>");
		
		model.addObject("editarrestriccion_selector_redorigen",
				Tools.callServiceXML(request, "/obtenerListadoRedOrigen.action?xml="+ idRestriccion +"&servicio=obtenerListadoRedes"));

		model.addObject("editarrestriccion_selector_reddestino",
				Tools.callServiceXML(request, "/obtenerListadoRedDestino.action?xml="+ idRestriccion +"&servicio=obtenerListadoRedes"));

		model.addObject("editarrestriccion_selector_funcionalidades", 
				Tools.callServiceXML(request, "obtenerClasificacionesFuncionalidades.action?servicio=obtenerClasificacionesFuncionalidades"));

		model.setViewName("app.admon.seguridad.seguridad.dialog.edit_restriccion");

		return model;
	}

	// ***************************************************************************************************
	@RequestMapping(value = "ajax/admon/seguridad/seguridad/remove_restricciones.do", method = RequestMethod.POST)
	public ResponseEntity<String> seguridadRemoveRestricciones(@RequestParam(value = "data", required = true) List<String> data,
			HttpServletRequest request) throws Exception {

		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request,
				"darDeBajaRestriccionesRedes.action?servicio=darDeBajaRestriccionesRedes&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	
	// *****************************************************************************************************
	@RequestMapping("ajax/admon/seguridad/seguridad/save_restriccion.do")
	public ResponseEntity<String> seguridadSaveRestriccion(@RequestParam(value = "idrestriccion", required = false, defaultValue = "") String idRestriccion, 
												   @RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
												   @RequestParam(value = "idredorigen", required = false, defaultValue = "") String idRedOrigen,
												   @RequestParam(value = "idreddestino", required = false, defaultValue = "") String idRedDestino,
	 								 			   @RequestParam(value = "funcionalidades[]", required = false, defaultValue = "") List<String> funcionalidades,
 								 				   HttpServletRequest request) throws Exception {

		String xmlFuncionalidades = "";
		if (funcionalidades != null) {
			Iterator<String> dataIterator = funcionalidades.iterator();
			while (dataIterator.hasNext()) {
				xmlFuncionalidades += "<Restrredesfunc><funcionalidad><idfuncionalidad>" + dataIterator.next() + "</idfuncionalidad></funcionalidad></Restrredesfunc>";
			}
		}
	
		String xml = Tools.callServiceXML(request, "postEdicionAltaRestriccionRedes.action?_modo=postEdicionAltaRestriccionRedes&servicio=postEdicionAltaRestriccionRedes&xml=" + URLEncoder.encode("<getEdicionAltaRestriccionRedes><Restriccionredes><idrestriccionredes>"+ idRestriccion +"</idrestriccionredes><nombre>" + nombre + "</nombre><redorigen><idred>" + idRedOrigen + "</idred></redorigen><reddestino><idred>" + idRedDestino + "</idred></reddestino><restrredesfuncs>" + xmlFuncionalidades + "</restrredesfuncs></Restriccionredes></getEdicionAltaRestriccionRedes>", "UTF-8"));

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}	

	// ***************************************************************************************************
	// TAB: AUDITORÍA
	// ***************************************************************************************************

	@RequestMapping("ajax/admon/seguridad/seguridad/save_auditoria.do")
	public ResponseEntity<String> seguridadSaveAuditoria(
			   									   @RequestParam(value = "activada", required = false, defaultValue = "") String activada,
	 								 			   @RequestParam(value = "usuarios[]", required = false, defaultValue = "") List<String> usuarios,
	 								 			   @RequestParam(value = "funcionalidades[]", required = false, defaultValue = "") List<String> funcionalidades,
 								 				   HttpServletRequest request) throws Exception {

		activada= (activada.length()>0) ? "true":"false"; 
		
		String xmlUsuarios = "";
		if (usuarios != null) {
			Iterator<String> dataIterator = usuarios.iterator();
			while (dataIterator.hasNext()) {
				xmlUsuarios += "<Usuario><idusuario>" + dataIterator.next() + "</idusuario></Usuario>";
			}
		}

		String xmlFuncionalidades = "";
		if (funcionalidades != null) {
			Iterator<String> dataIterator = funcionalidades.iterator();
			while (dataIterator.hasNext()) {
				xmlFuncionalidades += "<Funcionalidad><idfuncionalidad>" + dataIterator.next() + "</idfuncionalidad></Funcionalidad>";
			}
		}
	
		String xml = Tools.callServiceXML(request, "postActualizarAuditoria.action?_modo=postActualizarAuditoria&servicio=postActualizarAuditoria&xml=" + URLEncoder.encode("<getObtenerAuditoria><Auditoria><usuarios>" + xmlUsuarios + "</usuarios><funcionalidades>" + xmlFuncionalidades + "</funcionalidades><auditoriaActivada>"+activada+"</auditoriaActivada></Auditoria></getObtenerAuditoria>", "UTF-8"));

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}	

	// ******************************************INICIO CONFIGURACION INTERNET***********************************
	/**
	 * Pantalla Configuración de Internet
	 */
	@RequestMapping("admon/configuracioninternet/configuracion.do")
	public ModelAndView showConfiguracionInternet(HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.configuracioninternet.configuracioninternet");

		model.addObject("title", messageSource.getMessage("administracion.configuracioninternet.title", null, null));

		model.addObject("menu", Tools.getUserMainMenu(request));

		model.addObject("tabparametros_data", Tools.callServiceXML(request, "obtenerConfiguracionInternet.action?servicio=obtenerConfiguracionInternet"));		
		model.addObject("tabzonas_data", Tools.callServiceXML(request, "obtenerZonasInternet.action?servicio=obtenerZonasInternet"));
		
		model.addObject("editarzonasinternet_selector_zonas", Tools.callServiceXML(request, "obtenerListadoRecintosPrimerNivel.action?listaIdsZonas"));

		String xmlSelectorZonas=Tools.callServiceXML(request, "obtenerListadoRecintosPrimerNivel.action?listaIdsZonas");
		model.addObject("json_editarzonasinternet_selector_zonas", Tools.generateSelect2TreeJSON(xmlSelectorZonas, "ArrayList", "Recinto", "Zona", "idrecinto", "idzona"));

		return model;
	}



	// ********************************************************************************************************
	/**
	 * Graba los parámetros de Internet
	 *
	 */

	@RequestMapping(value = "admon/configuracioninternet/save_parametros_configuracion_internet.do", method = RequestMethod.POST)
	public ResponseEntity<String> saveParametrosConfiguracionInternet(@RequestParam(value = "servicioActivo", required = true, defaultValue = "true") String servicioActivo, @RequestParam(value = "mensajeInactividad", required = false, defaultValue = "") String mensajeInactividad, @RequestParam(value = "dsMerchantCurrency", required = false, defaultValue = "") String dsMerchantCurrency, @RequestParam(value = "dsMerchantProductDescription", required = false, defaultValue = "") String dsMerchantProductDescription, @RequestParam(value = "dsMerchantMerchantCode", required = false, defaultValue = "") String dsMerchantMerchantCode, @RequestParam(value = "dsMerchantMerchantURL", required = false, defaultValue = "") String dsMerchantMerchantURL, @RequestParam(value = "dsMerchantUrlKO", required = false, defaultValue = "") String dsMerchantUrlKO, @RequestParam(value = "dsMerchantUrlOK", required = false, defaultValue = "") String dsMerchantUrlOK, @RequestParam(value = "merchantName", required = false, defaultValue = "") String merchantName, @RequestParam(value = "merchantPassword", required = false, defaultValue = "") String merchantPassword,
			@RequestParam(value = "merchantTerminal", required = false, defaultValue = "") String merchantTerminal, @RequestParam(value = "merchantTransactionType", required = false, defaultValue = "") String merchantTransactionType, @RequestParam(value = "nummaxReservasInternet", required = false, defaultValue = "") String nummaxReservasInternet, @RequestParam(value = "contactoCAC", required = false, defaultValue = "") String contactoCAC, HttpServletRequest request) throws Exception {

		/*
		 * Si alguno de los parámetros que le pasamos contiene blancos nos da un
		 * error 505. La solución es hacer un encode
		 */
		
		String xml=Tools.callServiceXML(request, "setConfiguracionInternet.action?servicio=setConfiguracionInternet&xml=" + URLEncoder.encode("<ConfiguracionInternet><servicioActivo>" + servicioActivo + "</servicioActivo><mensajeInactividad>" + mensajeInactividad + "</mensajeInactividad><dsMerchantCurrency>" + dsMerchantCurrency + "</dsMerchantCurrency><dsMerchantProductDescription>" + dsMerchantProductDescription + "</dsMerchantProductDescription><dsMerchantMerchantCode>" + dsMerchantMerchantCode + "</dsMerchantMerchantCode><dsMerchantMerchantURL>" + dsMerchantMerchantURL + "</dsMerchantMerchantURL><dsMerchantUrlKO>" + dsMerchantUrlKO + "</dsMerchantUrlKO><dsMerchantUrlOK>" + dsMerchantUrlOK + "</dsMerchantUrlOK><merchantName>" + merchantName + "</merchantName><merchantPassword>" + merchantPassword + "</merchantPassword><merchantTerminal>" + merchantPassword + "</merchantTerminal><merchantTransactionType>" + merchantTransactionType + "</merchantTransactionType><nummaxReservasInternet>" + nummaxReservasInternet + "</nummaxReservasInternet><contactoCAC>" + contactoCAC + "</contactoCAC></ConfiguracionInternet>", "UTF-8"));

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);		
	}
	
	//******************************************************************************************************
	@RequestMapping(value = "admon/configuracioninternet/save_zonas_configuracion_internet.do", method = RequestMethod.POST)
	public ResponseEntity<String> saveZonasConfiguracionInternet
	       (@RequestParam(value = "selector_zonas[]", required = false) List<String> zonas, HttpServletRequest request) throws Exception {
				
		String xmlZonas = "";

		if (zonas != null) {
			Iterator<String> dataIterator = zonas.iterator();
			while (dataIterator.hasNext()) {
				xmlZonas = xmlZonas + "<Zona><idzona>" + dataIterator.next() + "</idzona><nombre></nombre><disponibleinternet></disponibleinternet><recinto><idrecinto></idrecinto><nombre></nombre></recinto><recinto_zona></recinto_zona></Zona>";
			}
		}
		
		String xml = Tools.callServiceXML(request, "actualizarZonasInternet.action?xml=" + URLEncoder.encode("<list>" +  xmlZonas+ "</list>", "UTF-8"));

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}


	// **********************FIN CONFIGURACION INTERNET********************************************************
	// **********************INICIO PRODUCTOS******************************************************************
	@RequestMapping("admon/productostarifas/productostarifas.do")
	public ModelAndView showProductosInicial(HttpServletRequest request) throws Exception {
		return showProductos(request);
	}
	
	@RequestMapping("admon/productos/productos.do")
	public ModelAndView showProductos(HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.productos.productos");

		model.addObject("title", messageSource.getMessage("administracion.productos.title", null, null));

		model.addObject("menu", Tools.getUserMainMenu(request));

		model.addObject("tabcontenidos_data", Tools.callServiceXML(request, "obtenerListadoTiposProducto.action?servicio=obtenerListadoTiposProducto"));
		model.addObject("tabproductos_data", Tools.callServiceXML(request, "obtenerListadoUnidadesNegocio.action?servicio=obtenerListadoUnidadesNegocio"));
		model.addObject("tabtiposproductos_data", Tools.callServiceXML(request, "obtenerListadoUnidadesNegocio.action?servicio=obtenerListadoUnidadesNegocio"));
		model.addObject("tabcanales_data", Tools.callServiceXML(request, "obtenerListadoTiposCanal.action?servicio=obtenerListadoTiposCanal"));
		return model;
	}
	
	
	
	// ----------------------------------LISTADO CONTENIDOS POR TIPO DE PRODUCTO------------------------------------
		@RequestMapping(value = "/ajax/admon/productos/tipos/listado.do", method = RequestMethod.POST)
		public ResponseEntity<?> contenidosListTiposProductos( HttpServletRequest request) throws Exception {
			String json = "";

			try {
				json = Tools.callServiceJSON(request, "obtenerListadoTiposProducto.action?servicio=obtenerListadoTiposProducto");
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}		
			
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}
	
	
	
	// ----------------------------------LISTADO CONTENIDOS POR TIPO DE PRODUCTO------------------------------------
	@RequestMapping(value = "/ajax/admon/productos/contenidos/list_contenidos.do", method = RequestMethod.POST)
	public ResponseEntity<?> productosListContenidos(@RequestParam(value = "idtipoproducto", required = false, defaultValue = "-1") String idtipoproducto, HttpServletRequest request) throws Exception {
		String json = "";

		try {
			json = Tools.callServiceJSON(request, "obtenerContenidosPorTipoProducto.action?servicio=obtenerContenidosPorTipoProducto&xml=" + idtipoproducto);
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}
	// ----------------------------------FIN LISTADO PRODUCTOS POR UNIDAD DE NEGOCIO-------------------------------
	// ----------------------------------INICIO VENTANA NUEVO/EDITA CONTENIDO--------------------------------------
	@RequestMapping("/ajax/admon/productos/contenidos/show_contenido.do")
	public ModelAndView productosShowContenido(@RequestParam(value = "id", required = false, defaultValue = "") String idContenido, HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.addObject("editarcontenido_selector_idiomas", Tools.callServiceXML(request, "obtenerListadoIdiomasFiltrado.action?servicio=obtenerListadoIdiomasFiltrado&selected=-1"));
		model.addObject("editarcontenido_selector_tiposproductos", Tools.callServiceXML(request, "obtenerListadoTiposProducto.action?servicio=obtenerListadoTiposProducto"));
		model.addObject("editarcontenido_selector_distribuidoras", Tools.callServiceXML(request, "obtenerListadoDistribuidoras.action?servicio=obtenerListadoDistribuidoras"));
		model.addObject("editarcontenido_selector_marcas", Tools.callServiceXML(request, "obtenerListadoMarcas.action?servicio=obtenerListadoMarcas"));
		
		model.addObject("editarcontenido_selector_tarifa", Tools.callServiceXML(request, "obtenerListadoTarifas.action?servicio=obtenerListadoTarifas"));
		
		if (idContenido.length() > 0)
			{
			model.addObject("editar_contenido", Tools.callServiceXML(request, "getEdicionAltaContenidos.action?servicio=obtenerContenidoPorIdNocaducadasConI18n&xml=" + idContenido));
			model.addObject("editarcontenido_selector_tarifasnovigentes", Tools.callServiceXML(request, "obtenerTarifasNoBonoNoVigentesPorProducto.action?servicio=obtenerTarifaProductoNoVigentesPorContenido&xml=<int>" + idContenido + "</int>"));
			}
		else
			{
			model.addObject("editar_contenido", "<empty/>");
			model.addObject("editarcontenido_selector_tarifasnovigentes", "<empty/>");
			}

		model.setViewName("app.admon.productos.contenido.dialog.edit_contenido");

		return model;
	}



	// ----------------------------------FIN VENTANA NUEVO/EDITAR CONTENIDO/ -------------------------------------------------------
	// ----------------------------------INICIO VENTANA MOSTRAR TARIFAS NO VIGENTES------------------------------------------------
	@RequestMapping("/ajax/admon/productos/contenidos/show_tarifas_vigentes.do")
	public ModelAndView productosShowContenidosTarifasVigentes(
			@RequestParam(value = "listaIdsTarifasVigentes", required = false, defaultValue = "") 
			List<String> listaIdsTarifasVigentes, HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		String xmlList = "";

		String param = "obtenerListadoTarifas.action?servicio=obtenerListadoTarifas&selected=-1&filter=<envio>" + URLEncoder.encode(xmlList, "UTF-8") + "</envio>";
		model.addObject("show_tarifas_vigentes", Tools.callServiceXML(request, param));
		model.setViewName("app.admon.productos.contenido.dialog.show_tarifas_vigentes");

		return model;
	}



	// ----------------------------------FIN VENTANA MOSTRAR TARIFAS NO VIGENTES------------------------------------------------
	// ----------------------------------INICIO ELIMINAR CONTENIDOS-------------------------------------------------------------
	@RequestMapping("/ajax/admon/productos/contenidos/remove_contenidos.do")
	public ResponseEntity<String> productosRemoveContenidos(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request, "darDeBajaContenidos.action?servicio=darDeBajaContenidos&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}

	// ----------------------------------FIN ELIMINAR CONTENIDOS--------------------------------------------------
	// ----------------------------------LISTADO PRODUCTOS POR UNIDAD DE NEGOCIO----------------------------------
	@RequestMapping(value = "/ajax/admon/productos/productos/list_productos.do", method = RequestMethod.POST)
	public ResponseEntity<?> productosListProductos(@RequestParam(value = "idunidadnegocio", required = false, defaultValue = "0") String idunidadnegocio, @RequestParam(value = "caducados", required = false, defaultValue = "0") String caducados, HttpServletRequest request) throws Exception {

		String json = "";
		try {
			if (idunidadnegocio.equals("0")) {
				json = Tools.callServiceJSON(request, "obtenerProductosPorUnidadDenegocio.action?servicio=obtenerProductosPorUnidadDenegocio&xml=<ObtenerProductosPorUnidadDeNegocioParam><idunidadnegocio></idunidadnegocio><caducados>" + caducados + "</caducados></ObtenerProductosPorUnidadDeNegocioParam>");
			} else {
				json = Tools.callServiceJSON(request, "obtenerProductosPorUnidadDenegocio.action?servicio=obtenerProductosPorUnidadDenegocio&xml=<ObtenerProductosPorUnidadDeNegocioParam><idunidadnegocio>" + idunidadnegocio + "</idunidadnegocio><caducados>" + caducados + "</caducados></ObtenerProductosPorUnidadDeNegocioParam>");
			}		
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}
		

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// ----------------------------------FIN VENTANA NUEVO/EDITAR PRODUCTO----------------------------------------------------
	// ----------------------------------INICIO ELIMINAR PRODUCTOS------------------------------------------------------------
	@RequestMapping("/ajax/admon/productos/contenidos/remove_productos.do")
	public ResponseEntity<String> productosRemoveProductos(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request, "darDeBajaProductos.action?servicio=darDeBajaProductos&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}

	// ----------------------------------FIN ELIMINAR PRODUCTOS--------------------------------------------------
	// ----------------------------------LISTADO TIPOS PRODUCTOS POR UNIDAD DE NEGOCIO --------------------------
	@RequestMapping(value = "/ajax/admon/productos/tiposproductos/list_tiposproductos.do", method = RequestMethod.POST)
	public ResponseEntity<?> productosListTiposProductos(@RequestParam(value = "idunidadnegocio", required = false, defaultValue = "0") String idunidadnegocio, HttpServletRequest request) throws Exception {
		
		String json = "";

		try {
			json = Tools.callServiceJSON(request, "obtenerTiposProductoPorUnidadNegocio.action?servicio=obtenerTiposProductoPorUnidadNegocio&xml=<int>" + idunidadnegocio + "</int>");
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}		

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}
	// ----------------------------------FIN LISTADO TIPOS PRODUCTOS POR UNIDAD DE NEGOCIO ---------------------------
	// ----------------------------------INICIO ELIMINAR TIPO PRODUCTOS------------------------------------------------
	@RequestMapping("/ajax/admon/productos/contenidos/remove_tipoproductos.do")
	public ResponseEntity<String> productosRemoveTipoProductos(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request, "darDeBajaTiposProducto.action?servicio=darDeBajaTiposProducto&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}
	// ----------------------------------FIN ELIMINAR TIPO PRODUCTOS--------------------------------------------------
	// ----------------------------------LISTADO CLASIFICACIONES------------------------------------------------------
	@RequestMapping(value = "/ajax/admon/productos/clasificaciones/list_clasificacionesproductos.do", method = RequestMethod.POST)
	public ResponseEntity<?> productosListClasificaciones(HttpServletRequest request) throws Exception {
		
		String json = "";

		try {
			json = Tools.callServiceJSON(request, "obtenerClasificacionesProductos.action?servicio=obtenerClasificacionesProductos");
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}	

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// ----------------------------------FIN LISTADO CLASIFICACIONES -------------------------------------------------
	//-----------------------------------INICIO VENTANA NUEVO/EDITAR CLASIFICACION-----------------------------------
	@RequestMapping("/ajax/admon/productos/clasificaciones/show_clasificacion.do")
	public ModelAndView productosShowClasificacion(@RequestParam(value = "id", required = false, defaultValue = "") String idClasificacion, HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.addObject("editarclasificacion_selector_productos", Tools.callServiceXML(request, "obtenerListadoProductosFiltrado.action?servicio=obtenerListadoProductosFiltrado"));
		

		if (idClasificacion.length() > 0)
			model.addObject("editarclasificacion_datos_clasificacion", Tools.callServiceXML(request, "getEdicionAltaClasificaciones.action?servicio=obtenerClasificacionProductoPorId&xml=" + idClasificacion));
		else
			model.addObject("editarclasificacion_datos_clasificacion", "<empty/>");

		model.setViewName("app.admon.productos.clasificacion.dialog.edit_clasificacion");

		return model;
	}
	//-----------------------------------FIN VENTANA NUEVA/EDIAR CLASIFICACION----------------------------------------
	//----------------------------------INICIO GRABAR CLASIFICACION---------------------------------------------------
		@RequestMapping("/ajax/admon/productos/clasificaciones/save_clasificacion.do")
		public ResponseEntity<String>productosSaveClasificacion(@RequestParam(value = "idclasificacion", required = false, defaultValue = "") String idClasificacion, 
													   @RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
													   @RequestParam(value = "descripcion", required = false, defaultValue = "") String descripcion,
		 								 			   @RequestParam(value = "productos[]", required = false, defaultValue = "") List<String> productos,		 								 			    
	 								 				   HttpServletRequest request) throws Exception {
			String xmlProductos = "";
			String[] parts;
			String valor="";
			
			String xml="";
			//Si es un alta de clasificación:
			if ( (idClasificacion == null) || (idClasificacion.isEmpty()) ) {
				if (productos != null) {
					Iterator<String> dataIterator = productos.iterator();
					while (dataIterator.hasNext()) {
						xmlProductos += "<Clasifprodprod><producto><idproducto>" + dataIterator.next() + "</idproducto></producto></Clasifprodprod>";
					}					
				xml = Tools.callServiceXML(request, "postEdicionAltaClasificaciones.action?_modo=alta&servicio=postEdicionAltaClasificaciones&xml="
						+ URLEncoder.encode("<getEdicionAltaClasificaciones><Clasificacionproductos><clasifprodprods>" + xmlProductos + "</clasifprodprods><dadodebaja>0</dadodebaja><descripcion>" + descripcion + "</descripcion><idclasificacionproductos>"+ idClasificacion +"</idclasificacionproductos><nombre>" + nombre + "</nombre><orden/></Clasificacionproductos></getEdicionAltaClasificaciones>", "UTF-8"));
				}
			}else//Si es una edición de clasificación:
			{
				if (productos != null) {
					Iterator<String> dataIterator = productos.iterator();
					while (dataIterator.hasNext()) {
						valor=dataIterator.next();
						
						parts = valor.split("\\|");
						if (parts.length == 2){
						//Al editar una clasificación y añadir un producto hay que indicar siempre el nodo <idclasifprodprod>
						//En el caso de que la Clasificación ya exista y queramos añadir un producto ese dato no lo tenemos por eso el caso en el else	
						xmlProductos += "<Clasifprodprod><idclasifprodprod>"+ parts[1]+"</idclasifprodprod><producto><idproducto>" + parts[0] + "</idproducto></producto></Clasifprodprod>";
						}else{
							xmlProductos += "<Clasifprodprod><idclasifprodprod></idclasifprodprod><producto><idproducto>" + parts[0] + "</idproducto></producto></Clasifprodprod>";	
						}
					}					
				xml = Tools.callServiceXML(request, "postEdicionAltaClasificaciones.action?_modo=edicion&servicio=postEdicionAltaClasificaciones&xml="
				+ URLEncoder.encode("<getEdicionAltaClasificaciones><Clasificacionproductos><clasifprodprods>" + xmlProductos + "</clasifprodprods><dadodebaja>0</dadodebaja><descripcion>" + descripcion + "</descripcion><idclasificacionproductos>"+ idClasificacion +"</idclasificacionproductos><nombre>" + nombre + "</nombre><orden/></Clasificacionproductos></getEdicionAltaClasificaciones>", "UTF-8"));
			}
			}		

			if (xml.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
			}

			return new ResponseEntity<String>(HttpStatus.OK);
		}	
	//----------------------------------FIN GRABAR CLASIFICACION------------------------------------------------------
	// ----------------------------------INICIO ELIMINAR CLASIFICACIONES----------------------------------------------
	@RequestMapping("/ajax/admon/productos/clasificaciones/remove_clasificacionesproductos.do")
	public ResponseEntity<String> productosRemoveClasificaciones(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request, "darDeBajaClasificacionesProductos.action?servicio=darDeBajaClasificacionesProductos&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}

	// ----------------------------------FIN ELIMINAR CLASIFICACIONES--------------------------------------------
	// ----------------------------------LISTADO DESCUENTOS PROMOCIONALES----------------------------------------
	@RequestMapping(value = "/ajax/admon/productos/descuentos/list_descuentospromocionales.do", method = RequestMethod.POST)
	public ResponseEntity<?> productosListDescuentosPromocionales(@RequestParam(value = "caducados", required = false, defaultValue = "0") String caducados, HttpServletRequest request) throws Exception {

		String json = "";

		try {
			json = Tools.callServiceJSON(request, "obtenerDescuentosFiltrandoCaducados.action?servicio=obtenerDescuentosFiltrandoCaducados&xml=<ObtenerDescuentosCaducadosParam><caducados>" + caducados + "</caducados></ObtenerDescuentosCaducadosParam>");
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}	
				
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// ----------------------------------FIN LISTADO DESCUENTOS PROMOCIONALES -------------------------------
	//-----------------------------------INICIO VENTANA NUEVO/EDITAR TARIFA-----------------------------------
			@RequestMapping("/ajax/admon/productos/descuentos/show_descuento.do")
			public ModelAndView productosShowDescuento(@RequestParam(value = "id", required = false, defaultValue = "") String idDescuentoPromocional, HttpServletRequest request) throws Exception {

				ModelAndView model = new ModelAndView();
								
				model.addObject("editardescuento_selector_tipospromocion", Tools.callServiceXML(request, "obtenerListadoTiposPromocion.action?servicio=obtenerListadoTiposPromocion"));
				model.addObject("editardescuento_selector_unidadesnegocio", Tools.callServiceXML(request, "obtenerUnidadesNegocioPorFuncionalidad.action?servicio=obtenerUnidadesNegocioPorFuncionalidad&xml=<int>99</int>"));
				model.addObject("editardescuento_selector_productos", Tools.callServiceXML(request, "obtenerListadoProductos.action?servicio=obtenerListadoProductos"));
				model.addObject("editardescuento_selector_tarifas", Tools.callServiceXML(request, "obtenerTarifas.action?servicio=obtenerTarifas"));
								
				if (idDescuentoPromocional.length() > 0)
					model.addObject("editardescuento_datos_descuento", Tools.callServiceXML(request, "getEdicionAltaDescuentosPromocionales.action?servicio=obtenerDescuentoPromocionalPorId&xml=" + idDescuentoPromocional));
				else
					model.addObject("editardescuento_datos_descuento", "<empty/>");

				model.setViewName("app.admon.productos.descuentospromo.dialog.edit_descuentospromo");

				return model;
			}
	//-----------------------------------FIN VENTANA NUEVA/EDIAR TARIFA-------------------------------------------------
	//-----------------------------------INICIO GUARDAR DESCUENTO PROMOCIONAL----------------------------------------------------------
			@RequestMapping("/ajax/admon/productos/descuentos/save_descuentospromocionales.do")
			public ResponseEntity<String> productosSaveDescuentosPromocionales(
					@RequestParam(value = "iddescuentopromocional", required = false, defaultValue = "") String iddescuentopromocional, 
					@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre, 
					@RequestParam(value = "descripcion", required = false, defaultValue = "") String descripcion, 
					@RequestParam(value = "descuento", required = false, defaultValue = "0") String descuento, 
					@RequestParam(value = "descuento2", required = false, defaultValue = "0") String descuento2,
					@RequestParam(value = "fechainiciovigencia", required = false, defaultValue = "") String fechainiciovigencia, 
					@RequestParam(value = "fechafinvigencia", required = false, defaultValue = "") String fechafinvigencia, 
					@RequestParam(value = "orden", required = false, defaultValue = "") String orden, 
					@RequestParam(value = "idtipopromocion", required = false, defaultValue = "0") String idtipopromocion, 
					@RequestParam(value = "idunidadnegocio", required = false) String idunidadnegocio,
					@RequestParam(value = "idproductopromo", required = false) String idproductopromo,
					@RequestParam(value = "idtarifaorigenpromo", required = false) String idtarifaorigenpromo,
					@RequestParam(value = "idtarifadestinopromo", required = false) String idtarifadestinopromo,
					@RequestParam(value = "idcliente", required = false, defaultValue = "") String idcliente,
					@RequestParam(value = "cliente", required = false, defaultValue = "") String nombrecliente,
					@RequestParam(value = "tarifasdescuentos[]", required = false) List<String> tarifas,
					@RequestParam(value = "productosdescuentos[]", required = false) List<String> productos,
					HttpServletRequest request) throws Exception {
				
				
				
				String xml = "";	
			
				xml = xml + "<getEdicionAltaDescuentosPromocionales><Descuentopromocional>";
				if ( (iddescuentopromocional != null) && (!iddescuentopromocional.isEmpty()) ) 
					xml = xml + "<iddescuentopromocional>" + iddescuentopromocional + "</iddescuentopromocional>";
				
				xml = xml + "<nombre>" + nombre + "</nombre>";
				xml = xml + "<descripcion>" + descripcion + "</descripcion>";
				xml = xml + "<descuento>" + descuento + "</descuento>";
				if(descuento2.equalsIgnoreCase("0_"))
					descuento2 = "0";
				xml = xml + "<descuento2>" + descuento2 + "</descuento2>";
				xml = xml + "<fechainiciovigencia>" + fechainiciovigencia + "</fechainiciovigencia>";
				xml = xml + "<fechafinvigencia>" + fechafinvigencia + "</fechafinvigencia>";
				xml = xml + "<orden>" + orden + "</orden>";
				xml = xml + "<tipopromocion><idtipopromocion>" + idtipopromocion + "</idtipopromocion></tipopromocion>";
				xml = xml + "<unidadnegocio><idunidadnegocio>" + idunidadnegocio + "</idunidadnegocio></unidadnegocio>";
				xml = xml + "<descprodpromocionados><Descprodpromocionado><producto><idproducto>" + idproductopromo + "</idproducto></producto></Descprodpromocionado></descprodpromocionados>";
				xml = xml + "<tarifaorigen><idtarifa>" + idtarifaorigenpromo + "</idtarifa></tarifaorigen>";
				xml = xml + "<tarifadestino><idtarifa>" + idtarifadestinopromo + "</idtarifa></tarifadestino>";
				xml = xml + "<cliente><idcliente>" + idcliente + "</idcliente><nombre>"+nombrecliente +"</nombre></cliente>";
				
				
				
				String xmlTarifas = "";
				if (tarifas != null) {
					Iterator<String> dataIterator = tarifas.iterator();
					while (dataIterator.hasNext()) {
						xmlTarifas += "<Descuentotarifa selected='false'><tarifa><idtarifa>" + dataIterator.next() + "</idtarifa></tarifa></Descuentotarifa>";
					}
				}			
				xml = xml + "<descuentotarifas>" + xmlTarifas + "</descuentotarifas>";
				
				String xmlProductos = "";
				if (productos != null) {
					Iterator<String> dataIterator = productos.iterator();
					while (dataIterator.hasNext()) {
						xmlProductos += "<Descuentoproducto><producto><idproducto>" + dataIterator.next() + "</idproducto></producto></Descuentoproducto>";
					}
				}			
				xml = xml + "<descuentoproductos>" + xmlProductos + "</descuentoproductos>";
				
				
				xml = xml + "</Descuentopromocional></getEdicionAltaDescuentosPromocionales>";
				
				
				//xml= "<getEdicionAltaDescuentosPromocionales><Descuentopromocional><iddescuentopromocional>6176288</iddescuentopromocional><nombre>ext2</nombre><descripcion></descripcion><descuento>15.00</descuento><descuento2>0.00</descuento2><fechainiciovigencia>23/04/2019</fechainiciovigencia><fechafinvigencia>23/04/2019</fechafinvigencia><orden></orden><tipopromocion><idtipopromocion>2</idtipopromocion><nombre>% de descuento</nombre></tipopromocion><unidadnegocio><idunidadnegocio>1</idunidadnegocio><nombre>Museo de las Ciencias</nombre></unidadnegocio><tarifaorigen/><tarifadestino/><cliente><idcliente></idcliente></cliente><descuentoproductos><Descuentoproducto><producto><idproducto>3161066</idproducto><nombre>CIENCIA A ESCENA</nombre></producto></Descuentoproducto></descuentoproductos><descuentotarifas><Descuentotarifa selected='false'><tarifa><idtarifa>49915</idtarifa><nombre>FAMILIAS</nombre></tarifa></Descuentotarifa></descuentotarifas><descuentosesiones/><descprodpromocionados><Descprodpromocionado><producto><idproducto></idproducto></producto></Descprodpromocionado></descprodpromocionados></Descuentopromocional></getEdicionAltaDescuentosPromocionales>";
				
		
				String xml_result = Tools.callServiceXML(request, "postEdicionAltaDescuentosPromocionales.action?_modo=&servicio=postEdicionAltaDescuentosPromocionales&xml=" + URLEncoder.encode(xml, "UTF-8"));
				
				

				if (xml_result.startsWith("<error")) {
					return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
				}

				return new ResponseEntity<String>(HttpStatus.OK);
			}
			//-----------------------------------FIN GUARDAR DESCUENTO PROMOCIONAL---------------------------------


			// ----------------------------------INICIO ELIMINAR DESCUENTOS PROMOCIONALES----------------------------
			@RequestMapping("/ajax/admon/productos/descuentos/remove_descuentospromocionales.do")
			public ResponseEntity<String> productosRemoveDescuentosPromocionales(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
				String xmlList = "";
				Iterator<String> dataIterator = data.iterator();
				while (dataIterator.hasNext()) {
					xmlList += "<int>" + dataIterator.next() + "</int>";
				}

				String xml = Tools.callServiceXML(request, "darDeBajaDescuentosPromocionales.action?servicio=darDeBajaDescuentosPromocionales&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}

	// ----------------------------------FIN ELIMINAR DESCUENTOS PROMOCIONALES--------------------------------------------
	// ----------------------------------LISTADO TARIFAS -----------------------------------------------------------------
	@RequestMapping(value = "/ajax/admon/productos/tarifas/list_tarifas.do", method = RequestMethod.POST)
	public ResponseEntity<?> productosListTarifas(HttpServletRequest request) throws Exception {

		String json = "";

		try {
			json = Tools.callServiceJSON(request, "obtenerTarifas.action?servicio=obtenerTarifas");
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}	
				
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// ----------------------------------FIN LISTADO TARIFAS-------------------------------------------------------------
	//-----------------------------------INICIO VENTANA NUEVO/EDITAR TARIFA-----------------------------------
		@RequestMapping("/ajax/admon/productos/tarifas/show_tarifa.do")
		public ModelAndView productosShowTarifa(@RequestParam(value = "id", required = false, defaultValue = "") String idTarifa, HttpServletRequest request) throws Exception {

			ModelAndView model = new ModelAndView();
			model.addObject("editartarifa_selector_unidadnegocio", Tools.callServiceXML(request, "obtenerUnidadesNegocioPorFuncionalidad.action?servicio=obtenerUnidadesNegocioPorFuncionalidad&xml=<int>101</int>"));			
			model.addObject("editartarifa_selector_perfilesvisitante",Tools.callServiceXML(request, "obtenerListadoPerfilesVisitanteFiltrado.action?servicio=obtenerListadoPerfilesVisitanteFiltrado"));
			
			if (idTarifa.length() > 0)
				model.addObject("editartarifa_datos_tarifa", Tools.callServiceXML(request, "getEdicionAltaTarifas.action?servicio=obtenerTarifaPorIdConI18n&xml=" + idTarifa));
			else
				model.addObject("editartarifa_datos_tarifa", "<empty/>");

			model.setViewName("app.admon.productos.tarifas.dialog.edit_tarifa");

			return model;
		}
	//-----------------------------------FIN VENTANA NUEVA/EDIAR TARIFA-------------------------------------------------
	//-----------------------------------INICIO GUARDAR TARIFA----------------------------------------------------------
		@RequestMapping("/ajax/admon/productos/tarifas/save_tarifa.do")
		public ResponseEntity<String> productosSaveTarifa(@RequestParam(value = "idtarifa", required = false, defaultValue = "") String idtarifa, @RequestParam(value = "nombre", required = false, defaultValue = "") String nombre, @RequestParam(value = "descripcion", required = false, defaultValue = "") String descripcion, @RequestParam(value = "dadodebaja", required = false, defaultValue = "") String dadodebaja, @RequestParam(value = "bono", required = false, defaultValue = "0") String bono, @RequestParam(value = "abono", required = false, defaultValue = "0") String abono, @RequestParam(value = "orden", required = false, defaultValue = "") String orden, @RequestParam(value = "basesapinvitacion", required = false, defaultValue = "0") String basesapinvitacion, @RequestParam(value = "idunidadnegocio", required = false) String idunidadnegocio,@RequestParam(value = "i18nFieldNombre_tarifa", required = false, defaultValue = "") String i18nNombre, @RequestParam(value = "perfilesvisitante[]", required = false) List<String> perfilesvisitante,HttpServletRequest request) throws Exception {
			String xml = "";
			
			
			if(bono.startsWith("on"))
				bono = "true";
			else 
				bono = "false";


			if(abono.startsWith("on"))
				abono = "true";
			else
				abono = "false";

			if(basesapinvitacion.startsWith("on"))
				basesapinvitacion = "true";
			else
				basesapinvitacion = "false";
		

			xml = xml + "<Tarifa><idtarifa>" + idtarifa + "</idtarifa>";
			xml = xml + "<nombre>" + nombre + "</nombre>";
			xml = xml + "<descripcion>" + descripcion + "</descripcion>";
			xml = xml + "<bono>" + bono + "</bono>";
			//xml = xml + "<abono>" + abono + "</abono>";
			xml = xml + "<orden>" + orden + "</orden>";
			xml = xml + "<basesapinvitacion>" + basesapinvitacion + "</basesapinvitacion>";
			xml = xml + "<unidadnegocio><idunidadnegocio>" + idunidadnegocio + "</idunidadnegocio></unidadnegocio>";
			
			
			String xmlPerfilesVisitante = "";
			if (perfilesvisitante != null) {
				Iterator<String> dataIterator = perfilesvisitante.iterator();
				while (dataIterator.hasNext()) {
					xmlPerfilesVisitante += "<Perfilvisitante selected='false'><idperfilvisitante>" + dataIterator.next() + "</idperfilvisitante></Perfilvisitante>";
				}
			}			
			xml = xml + "<perfilvisitantes>" + xmlPerfilesVisitante + "</perfilvisitantes>";
			HashMap<String,String> i18n=new HashMap<String, String>();
			i18n.put("nombre", i18nNombre);
			xml = xml + "<i18ns>"+Tools.generateI18nXMLFromForm(i18n, nombre)+"</i18ns>";
			xml = xml + "</Tarifa>";
	
			String xml_result = Tools.callServiceXML(request, "postEdicionAltaTarifas.action?xml=" + URLEncoder.encode(xml, "UTF-8")+"&cPerfiles=true");

			if (xml_result.startsWith("<error")) {
				return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
			}

			return new ResponseEntity<String>(HttpStatus.OK);
		}
	//-----------------------------------FIN GUARDAR TARIFA-------------------------------------------------------------	
	// ----------------------------------INICIO ELIMINAR TARIFAS -------------------------------------------------------
	@RequestMapping("/ajax/admon/productos/tarifas/remove_tarifas.do")
	public ResponseEntity<String> productosRemoveTarifas(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request, "darDeBajaTarifas.action?servicio=darDeBajaTarifas&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}

	// ----------------------------------FIN ELIMINAR TARIFAS------------------------------------------------------------
	// ----------------------------------LISTADO PERFILES VISITANTE------------------------------------------------------
	@RequestMapping(value = "/ajax/admon/productos/perfilesvisitante/list_perfilesvisitante.do", method = RequestMethod.POST)
	public ResponseEntity<?> productosListPerfilesVisitante(HttpServletRequest request) throws Exception {

		String json = "";

		try {
			json = Tools.callServiceJSON(request, "obtenerPerfilesVisitante.action?servicio=obtenerPerfilesVisitante");
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}	
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// ----------------------------------FIN LISTADO PERFILES VISITANTE--------------------------------------------------
	//-----------------------------------INICIO VENTANA NUEVO/EDITAR PERFIL VISITANTE-----------------------------------
			@RequestMapping("/ajax/admon/productos/perfilesvisitante/show_perfilesvisitante.do")
			public ModelAndView productosShowPerfilVisitante(@RequestParam(value = "id", required = false, defaultValue = "") String idPerfilVisitante, HttpServletRequest request) throws Exception {

				ModelAndView model = new ModelAndView();
				
				if (idPerfilVisitante.length() > 0)
					model.addObject("editarperfilvisitante_datos_perfil", Tools.callServiceXML(request, "getEdicionAltaPerfilesVisitante.action?servicio=obtenerPerfilVisitantePorIdConI18n&xml=" + idPerfilVisitante));
				else
					model.addObject("editarperfilvisitante_datos_perfil", "<empty/>");

				model.setViewName("app.admon.productos.perfilesvisitante.dialog.edit_perfil");

				return model;
			}
	//-----------------------------------FIN VENTANA NUEVA/EDIAR PERFIL VISITANTE-------------------------------------------------
	//-----------------------------------INICIO GUARDAR PERFIL VISITANTE----------------------------------------------------------
			@RequestMapping("/ajax/admon/productos/perfilesvisitante/save_perfilvisitante.do")
			public ResponseEntity<String> productosSavePerfilVisitante(@RequestParam(value = "idperfilvisitante", required = false, defaultValue = "") String idperfilvisitante, @RequestParam(value = "nombre", required = false, defaultValue = "") String nombre, @RequestParam(value = "descripcion", required = false, defaultValue = "") String descripcion, @RequestParam(value = "basesapinvitacion", required = false, defaultValue = "false") String basesapinvitacion, @RequestParam(value = "acreditacion", required = false, defaultValue = "0") String acreditacion, @RequestParam(value = "orden", required = false, defaultValue = "") String orden, @RequestParam(value = "activo", required = false, defaultValue = "0") String activo, @RequestParam(value = "i18nFieldNombre_perfilvisitante", required = false, defaultValue = "") String i18nNombre,HttpServletRequest request) throws Exception {
				String xml = "";
				
				
				if(basesapinvitacion.startsWith("on"))
					basesapinvitacion = "true";
				
				if(acreditacion.startsWith("on"))
					acreditacion = "true";
				
				if(activo.startsWith("on"))
					activo = "1";			

				xml = xml + "<getEdicionAltaPerfilesVisitante><perfilvisitante>";
				
				if(!idperfilvisitante.isEmpty())				
					xml = xml +"<idperfilvisitante>" + idperfilvisitante  + "</idperfilvisitante>";				
				xml = xml + "<nombre>" + nombre + "</nombre>";	
				xml = xml + "<descripcion>" + descripcion + "</descripcion>";
				xml = xml + "<orden>" + orden + "</orden>";
				xml = xml + "<sapinvitacion>" + basesapinvitacion + "</sapinvitacion>";
				xml = xml + "<activo>" + activo + "</activo>";
				xml = xml + "<requiereAcreditacion>" + acreditacion + "</requiereAcreditacion>";
				xml = xml + "<dadodebaja/>";				
				
				
				HashMap<String,String> i18n=new HashMap<String, String>();
				i18n.put("nombre", i18nNombre);
				xml = xml + "<i18ns>"+Tools.generateI18nXMLFromForm(i18n, nombre)+"</i18ns>";
				xml = xml + "</perfilvisitante></getEdicionAltaPerfilesVisitante>";
																   
				String xml_result = Tools.callServiceXML(request, "postEdicionAltaPerfilesVisitante.action?_modo=&servicio=postEdicionAltaPerfilesVisitante&xml=" + URLEncoder.encode(xml, "UTF-8"));

				if (xml_result.startsWith("<error")) {
					return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
				}

				return new ResponseEntity<String>(HttpStatus.OK);
			}
    //-----------------------------------FIN GUARDAR PERFIL VISITANTE----------------------------------------------------		
	// ----------------------------------INICIO ELIMINAR PERFIL VISITANTE------------------------------------------------
	@RequestMapping("/ajax/admon/productos/perfilesvisitante/remove_perfilesvisitante.do")	
	public ResponseEntity<String> productosRemovePerfilesVisitante(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
		String xmlList = "";		
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request, "darDeBajaPerfilesVisitante.action?servicio=darDeBajaPerfilesVisitante&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}

	// ----------------------------------FIN ELIMINAR PERFIL VISITANTE-----------------------------------------
	// ----------------------------------LISTADO CANALES POR TIPO CANAL----------------------------------------
	@RequestMapping(value = "/ajax/admon/productos/canales/list_canalesportipocanal.do", method = RequestMethod.POST)
	public ResponseEntity<?> productosListCanalesPorTipoCanal(@RequestParam(value = "idtipocanal", required = false, defaultValue = "-1") String idtipocanal, HttpServletRequest request) throws Exception {
		
		String json = "";

		try {
			json = Tools.callServiceJSON(request, "obtenerCanalesPorTipoCanal.action?servicio=obtenerCanalesPorTipoCanal&xml=<int>" + idtipocanal + "</int>");
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}	
		 
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// ----------------------------------FIN LISTADO TIPOS PRODUCTOS POR UNIDAD DE NEGOCIO -----------------------------
	// ----------------------------------INICIO ELIMINAR CANALES--------------------------------------------------------
	@RequestMapping("/ajax/admon/productos/canales/remove_canalesportipocanal.do")
	public ResponseEntity<String> productosRemoveCanalesPorTipoCanal(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request, "darDeBajaCanales.action?servicio=darDeBajaCanales&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}

	// ----------------------------------FIN ELIMINAR CANALES---------------------------------------------------------
	// ----------------------------------LISTADO FORMAS DE PAGO-------------------------------------------------------
	@RequestMapping(value = "/ajax/admon/productos/formaspago/list_formaspago.do", method = RequestMethod.POST)
	public ResponseEntity<?> productosListFormasDePago(HttpServletRequest request) throws Exception {
	
		String json = "";

		try {
			json = Tools.callServiceJSON(request, "obtenerFormasPago.action?servicio=obtenerFormasPago");
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// ----------------------------------FIN LISTADO FORMAS DE PAGO ---------------------------------------
	//-----------------------------------INICIO VENTANA NUEVO/EDITAR FORMAS DE PAGO------------------------
	@RequestMapping("/ajax/admon/productos/formaspago/show_formaspago.do")
	public ModelAndView productosShowFormasDePago(@RequestParam(value = "id", required = false, defaultValue = "") String idFormaPago, HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();
		model.addObject("editarformapago_selector_tipoformapago", Tools.callServiceXML(request, "obtenerListadoTiposformapago.action?servicio=obtenerListadoTiposformapago"));
		if (idFormaPago.length() > 0)
			model.addObject("editarformapago_datos_formapago", Tools.callServiceXML(request, "getDarDeAltaFormaPago.action?servicio=obtenerFormaPagoPorId&xml=" + idFormaPago));
		else
			model.addObject("editarformapago_datos_formapago", "<empty/>");

		model.setViewName("app.admon.productos.formaspago.dialog.edit_formapago");

		return model;
	}
	//-----------------------------------FIN VENTANA NUEVA/EDIAR FORMAS DE PAGO-----------------------------------------
	//-----------------------------------INICIO GUARDAR FORMAS DE PAGO----------------------------------------------------------
	@RequestMapping("/ajax/admon/productos/formaspago/save_formaspago.do")
	public ResponseEntity<String> productosSaveFormasDePago(
			@RequestParam(value = "idformapago", required = false, defaultValue = "") String idformapago, 
			@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre, 
			@RequestParam(value = "idtipoformapago", required = false, defaultValue = "") String idtipoformapago,
			@RequestParam(value = "descripcion", required = false, defaultValue = "") String descripcion,
			@RequestParam(value = "fechacaducidad", required = false, defaultValue = "") String fechacaducidad,
			@RequestParam(value = "orden", required = false, defaultValue = "") String orden,
			@RequestParam(value = "credito", required = false, defaultValue = "false") String credito,
			@RequestParam(value = "codigo", required = false, defaultValue = "0") String codigosap,
			@RequestParam(value = "distintaformapago", required = false, defaultValue = "false") String distintaformapagodevolucion,HttpServletRequest request) throws Exception {
		
		String xml = "";		
		
		if(distintaformapagodevolucion.startsWith("on"))
			distintaformapagodevolucion = "true";
		
		if(credito.startsWith("on"))
			credito= "true";
	
		xml = xml + "<getDarDeAltaFormaPago><Formapago>";
		
					
		xml = xml +"<idformapago>" + idformapago  + "</idformapago>";
		xml = xml + "<nombre>" + nombre + "</nombre>";
		xml = xml + "<tipoformapago><idtipoformapago>" + idtipoformapago + "</idtipoformapago></tipoformapago>";	
		xml = xml + "<descripcion>" + descripcion + "</descripcion>";
		xml = xml + "<fechacaducidad>" + fechacaducidad + "</fechacaducidad>";		
		xml = xml + "<orden>" + orden + "</orden>";
		xml = xml + "<dadodebaja/>";
		xml = xml + "<credito>" + credito + "</credito>";
		xml = xml + "<codigosap>" + codigosap + "</codigosap>";
		xml = xml + "<distintaformapagodevolucion>" + distintaformapagodevolucion + "</distintaformapagodevolucion>";		
		xml = xml + "<importeparcials/>";
		xml = xml + "<formapagotipoclientes/>";
		xml = xml + "<facturas/>";	
		xml = xml + "<formapagocanals/>";
		
		
		xml = xml + "</Formapago></getDarDeAltaFormaPago>";
		String xml_result;
		if ( (idformapago == null) || (idformapago.isEmpty()) )
		xml_result = Tools.callServiceXML(request, "postDarDeAltaFormaPago.action?_modo=alta&servicio=postDarDeAltaFormaPago&xml=" + URLEncoder.encode(xml, "UTF-8"));
		else
			xml_result = Tools.callServiceXML(request, "postDarDeAltaFormaPago.action?_modo=edicion&servicio=postDarDeAltaFormaPago&xml=" + URLEncoder.encode(xml, "UTF-8"));
		

		if (xml_result.startsWith("<error")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}
	//-----------------------------------FIN GUARDAR FORMAS DE PAGO-----------------------------------------------------
	// ----------------------------------INICIO ELIMINAR FORMAS DE PAGO-------------------------------------------------
	@RequestMapping("/ajax/admon/productos/formaspago/remove_formasdepago.do")
	public ResponseEntity<String> productosRemoveFormasDePago(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request, "darDeBajaFormasPago.action?servicio=darDeBajaFormasPago&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}

	// ----------------------------------FIN ELIMINAR FORMAS DE PAGO-----------------------------------------
	// *******************************FIN PRODUCTOS*********************************************************



	// ***************************************************************************************************
	// Recintos
	
	@RequestMapping("admon/espacios/espacios.do")
	public ModelAndView showRecintosInicial(HttpServletRequest request) throws Exception {
		return showRecintos(request);
	}

	@RequestMapping("admon/recintos/recintos.do")
	public ModelAndView showRecintos(HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.recintos.recintos");

		model.addObject("title", messageSource.getMessage("administracion.recintos.title", null, null));

		model.addObject("menu", Tools.getUserMainMenu(request));

		model.addObject("tabrecintos_selector_unidades", Tools.callServiceXML(request, "obtenerListadoUnidadesNegocio.action?servicio=obtenerListadoUnidadesNegocio"));

		return model;

	}



	// ***************************************************************************************************
	@RequestMapping(value = "ajax/admon/recintos/taquillas/list_taquillas.do", method = RequestMethod.POST)
	public ResponseEntity<?> recintosListTaquillas(HttpServletRequest request) throws Exception {

       String json="";
       try {
               json = Tools.callServiceJSON(request, "obtenerTaquillas.action?servicio=obtenerTaquillas");
        } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
        }

		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}



	// *****************************************************************************************************
	@RequestMapping("ajax/admon/recintos/taquillas/show_taquilla.do")
	public ModelAndView recintosShowTaquilla(@RequestParam(value = "id", required = false, defaultValue = "") String idTaquilla, HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.addObject("editartaquilla_selector_recintos", Tools.callServiceXML(request, "obtenerListadoRecintos.action?servicio=obtenerListadoRecintos"));
		model.addObject("editartaquilla_selector_ubicaciones", Tools.callServiceXML(request, "obtenerListadoUbicaciones.action?servicio=obtenerListadoUbicaciones"));

		if (idTaquilla.length() > 0)
			model.addObject("editar_taquilla", Tools.callServiceXML(request, "getEdicionAltaTaquillas.action?servicio=obtenerTaquillaPorId&xml=" + idTaquilla));
		else
			model.addObject("editar_taquilla", "<empty/>");

		model.setViewName("app.admon.recintos.taquilla.dialog.edit_taquilla");

		return model;
	}



	// ***************************************************************************************************
	@RequestMapping(value = "/ajax/admon/recintos/recintos/list_recintos.do", method = RequestMethod.POST)
	public ResponseEntity<?> recintosListRecintos(@RequestParam(value = "idunidadnegocio", required = false, defaultValue = "-1") String idunidadnegocio, HttpServletRequest request) throws Exception {

	     String json="";
	       try {
	               json = Tools.callServiceJSON(request, "obtenerRecintosPorUnidadNegocio.action?servicio=obtenerRecintosPorUnidadNegocio&xml=" + idunidadnegocio);
	        } catch (Exception ex) {
	               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}



	// ***************************************************************************************************
	@RequestMapping(value = "ajax/admon/recintos/unidades/list_unidades.do", method = RequestMethod.POST)
	public ResponseEntity<?> recintosListUnidades(HttpServletRequest request) throws Exception {

		String json="";
	       try {
	               json = Tools.callServiceJSON(request, "obtenerUnidadesNegocio.action?servicio=obtenerUnidadesNegocio");
	        } catch (Exception ex) {
	               throw new AjaxException(ex.getMessage());
	        }			
		
	     HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}



	// ***************************************************************************************************
	@RequestMapping(value = "ajax/admon/recintos/ubicaciones/list_ubicaciones.do", method = RequestMethod.POST)
	public ResponseEntity<?> recintosListUbicaciones(HttpServletRequest request) throws Exception {

		String json="";
	       try {
	               json =  Tools.callServiceJSON(request, "obtenerUbicaciones.action?servicio=obtenerUbicaciones");
	        } catch (Exception ex) {
	               throw new AjaxException(ex.getMessage());
	        }			

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}



	// ***************************************************************************************************
	@RequestMapping(value = "ajax/admon/recintos/puntos_recogida/list_puntos.do", method = RequestMethod.POST)
	public ResponseEntity<?> recintosListPuntosRecogida(HttpServletRequest request) throws Exception {

		String json="";
	       try {
	               json = Tools.callServiceJSON(request, "obtenerPuntosrecogida.action?servicio=obtenerPuntosrecogida");
	        } catch (Exception ex) {
	               throw new AjaxException(ex.getMessage());
	        }		
		
	     HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}



	// *****************************************************************************************************
	@RequestMapping("ajax/admon/recintos/puntos_recogida/show_puntos_recogida.do")
	public ModelAndView recintosShowPuntosRecogida(@RequestParam(value = "id", required = false, defaultValue = "") String idPuntoRecogida, HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		if (idPuntoRecogida.length() > 0)
			model.addObject("editarpunto_datos_punto", Tools.callServiceXML(request, "getEdicionAltaPuntosrecogida.action?servicio=obtenerPuntosrecogidaPorId&xml=" + idPuntoRecogida));
		else
			model.addObject("editarpunto_datos_punto", "<empty/>");

		model.setViewName("app.admon.recintos.puntos_recogida.dialog.edit_puntos_recogida");

		return model;
	}



	// *****************************************************************************************************
	@RequestMapping("ajax/admon/recintos/puntos_recogida/save_puntoRecogida.do")
	public ResponseEntity<String> recintosSavePuntoRecogida(@RequestParam(value = "idPuntoRecogida", required = false, defaultValue = "") String idPuntoRecogida, @RequestParam(value = "nombre", required = false, defaultValue = "") String nombre, @RequestParam(value = "i18nFieldNombre_puntorecogida", required = false, defaultValue = "") String i18nNombre, HttpServletRequest request) throws Exception {
		String xml = "";

		HashMap<String,String> i18n=new HashMap<String, String>();
		i18n.put("nombre", i18nNombre);
		
		xml = xml + "<getEdicionAltaPuntosrecogida><Puntosrecogida><idpuntosrecogida>" + idPuntoRecogida + "</idpuntosrecogida>";
		xml = xml + "<nombre>" + nombre + "</nombre>";
		xml = xml + "<i18ns>"+Tools.generateI18nXMLFromForm(i18n, nombre)+"</i18ns>";
		xml = xml + "</Puntosrecogida></getEdicionAltaPuntosrecogida>";

		String xml_result = Tools.callServiceXML(request, "postEdicionAltaPuntosrecogida.action?_modo=&servicio=postEdicionAltaPuntosrecogida&xml=" + URLEncoder.encode(xml, "UTF-8"));

		if (xml_result.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>(HttpStatus.OK);
	}



	// *****************************************************************************************************
	@RequestMapping("ajax/admon/recintos/puntos_recogida/remove_puntos_recogida.do")
	public ResponseEntity<String> recintosRemovePuntosRecogida(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request, "darDeBajaPuntosrecogida.action?servicio=darDeBajaPuntosrecogida&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}



	// *****************************************************************************************************
	@RequestMapping("ajax/admon/recintos/taquilla/save_taquilla.do")
	public ResponseEntity<String> recintosSaveTaquilla(@RequestParam(value = "idtaquilla", required = false, defaultValue = "") String idtaquilla, @RequestParam(value = "nombre", required = false, defaultValue = "") String nombre, @RequestParam(value = "dirip", required = false, defaultValue = "") String dirip, @RequestParam(value = "idubicacion", required = false, defaultValue = "") String idubicacion, @RequestParam(value = "idrecinto", required = false, defaultValue = "") String idrecinto, @RequestParam(value = "configdatafono", required = false, defaultValue = "") String configdatafono, @RequestParam(value = "terminaldatafono", required = false, defaultValue = "") String terminaldatafono, HttpServletRequest request) throws Exception {
		String xml = "";

		xml = xml + "<getEdicionAltaTaquillas><Taquilla><idtaquilla>" + idtaquilla + "</idtaquilla>";
		xml = xml + "<nombre>" + nombre + "</nombre><dadodebaja>0</dadodebaja>";
		xml = xml + "<dirip>" + dirip + "</dirip>";
		xml = xml + "<ubicacion><idubicacion>" + idubicacion + "</idubicacion></ubicacion>";
		xml = xml + "<recinto><idrecinto>" + idrecinto + "</idrecinto></recinto>";
		xml = xml + "<configdatafono>" + configdatafono + "</configdatafono><terminaldatafono>" + terminaldatafono + "</terminaldatafono>";
		xml = xml + "</Taquilla></getEdicionAltaTaquillas>";

		String xml_result = Tools.callServiceXML(request, "postEdicionAltaTaquilla.action?_modo=&servicio=postEdicionAltaTaquilla&xml=" + URLEncoder.encode(xml, "UTF-8"));

		if (xml_result.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}



	// *****************************************************************************************************
	@RequestMapping("ajax/admon/recintos/taquillas/remove_taquillas.do")
	public ResponseEntity<String> recintosRemoveTaquillas(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request, "darDeBajaTaquillas.action?servicio=darDeBajaTaquillas&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}



	// *****************************************************************************************************
	@RequestMapping("ajax/admon/recintos/ubicaciones/show_ubicacion.do")
	public ModelAndView recintosShowUbicacion(@RequestParam(value = "id", required = false, defaultValue = "") String idUbicacion, HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();
		model.addObject("editarubicacion_selector_taquillas", Tools.callServiceXML(request, "obtenerListadoTaquillasFiltrado.action"));

		if (idUbicacion.length() > 0)
			model.addObject("editar_ubicacion", Tools.callServiceXML(request, "getEdicionAltaUbicaciones.action?servicio=obtenerUbicacionPorId&xml=" + idUbicacion));
		else
			model.addObject("editar_ubicacion", "<empty/>");

		model.setViewName("app.admon.recintos.taquilla.dialog.edit_ubicacion");

		return model;
	}



	// *****************************************************************************************************
	@RequestMapping("ajax/admon/recintos/ubicacion/save_ubicacion.do")
	public ResponseEntity<String> recintosSaveUbicacion(@RequestParam(value = "idubicacion", required = false, defaultValue = "") String idubicacion, @RequestParam(value = "nombre", required = false, defaultValue = "") String nombre, @RequestParam(value = "descripcion", required = false, defaultValue = "") String descripcion, @RequestParam(value = "taquillas[]", required = false) List<String> taquillas, HttpServletRequest request) throws Exception {

		String xmlUnidades = "";
		if (taquillas != null) {
			Iterator<String> dataIterator = taquillas.iterator();
			while (dataIterator.hasNext()) {
				xmlUnidades += "<Taquilla><idtaquilla>" + dataIterator.next() + "</idtaquilla></Taquilla>";
			}
		}

		String xml = "";
		xml = xml + "<getEdicionAltaUbicaciones><Ubicacion><idubicacion>" + idubicacion + "</idubicacion>";
		xml = xml + "<nombre>" + nombre + "</nombre><descripcion>" + descripcion + "</descripcion>";
		xml = xml + "<dadodebaja>0</dadodebaja><orden/>";
		xml = xml + "<taquillas>" + xmlUnidades + "</taquillas>";
		xml = xml + "</Ubicacion></getEdicionAltaUbicaciones>";

		String xml_result = Tools.callServiceXML(request, "postEdicionAltaUbicaciones.action?_modo=&servicio=postEdicionAltaUbicaciones&xml=" + URLEncoder.encode(xml, "UTF-8"));

		if (xml_result.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);

	}



	// *****************************************************************************************************
	@RequestMapping("ajax/admon/recintos/ubicaciones/remove_ubicaciones.do")
	public ResponseEntity<String> recintosRemoveUbicaciones(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request, "darDeBajaUbicaciones.action?servicio=darDeBajaUbicaciones&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}



	// *****************************************************************************************************
	@RequestMapping("ajax/admon/recintos/unidades/remove_unidades.do")
	public ResponseEntity<String> recintosRemoveUnidades(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request, "darDeBajaUnidadesNegocio.action?servicio=darDeBajaUnidadesNegocio&xml=<list>" + xmlList + "</list>");
		
		
		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}



	// *****************************************************************************************************
	@RequestMapping("ajax/admon/recintos/unidades/show_unidades.do")
	public ModelAndView recintosShowUnidad(@RequestParam(value = "id", required = false, defaultValue = "") String idUnidadNegocio, HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();
		model.addObject("editarubicacion_selector_taquillas", Tools.callServiceXML(request, "obtenerTaquillas.action?servicio=obtenerTaquillas"));

		if (idUnidadNegocio.length() > 0)
			model.addObject("editar_unidad_negocio", Tools.callServiceXML(request, "getEdicionAltaUnidadNegocio.action?servicio=obtenerUnidadNegocioPorId&xml=" + idUnidadNegocio));
		else
			model.addObject("editar_unidad_negocio", "<empty/>");

		model.addObject("editarunidad_selector_entidadGestora", Tools.callServiceXML(request, "obtenerListadoEntidadgestora.action"));

		model.addObject("editarunidad_selector_redes", Tools.callServiceXML(request, "obtenerListadoRedesFiltrado.action?servicio=obtenerListadoRedesFiltrado"));

		model.setViewName("app.admon.recintos.unidades_negocio.dialog.edit_unidad_negocio");

		return model;
	}



	// ***************************************************************************************************
	@RequestMapping(value = "ajax/admon/recintos/select_list_recintos.do", method = RequestMethod.POST)
	public ResponseEntity<?> recintosSelectListRecintos(HttpServletRequest request) throws Exception {

		String json = Tools.callServiceJSON(request, "obtenerListadoRecintosFiltrado.action?servicio=obtenerListadoRecintosFiltrado");

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}



	// ***************************************************************************************************
	@RequestMapping(value = "ajax/admon/recintos/select_list_usuario.do", method = RequestMethod.POST)
	public ResponseEntity<?> recintosSelectListUsuarios(HttpServletRequest request) throws Exception {

		String json = Tools.callServiceJSON(request, "obtenerListadoUsuariosFiltrado.action?servicio=obtenerListadoUsuariosFiltrado");

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}



	// *****************************************************************************************************
	@RequestMapping("ajax/admon/recintos/unidad/save_unidad.do")
	public ResponseEntity<String> recintosSaveUnidad(@RequestParam(value = "idunidadnegocio", required = false, defaultValue = "") String idunidadnegocio, @RequestParam(value = "nombre", required = false, defaultValue = "") String nombre, @RequestParam(value = "orden", required = false, defaultValue = "") String orden, @RequestParam(value = "idEntidadGestora", required = false, defaultValue = "") String idEntidadGestora, @RequestParam(value = "mostrar", required = false, defaultValue = "") String mostrar, @RequestParam(value = "recintos[]", required = false) List<String> recintos, @RequestParam(value = "redes[]", required = false) List<String> redes, @RequestParam(value = "usuarios[]", required = false) List<String> usuarios, HttpServletRequest request) throws Exception {

		String xmlRecintos = "";
		if (recintos != null) {
			Iterator<String> dataIterator = recintos.iterator();
			while (dataIterator.hasNext()) {
				xmlRecintos = xmlRecintos + "<Recintounidadnegocio><recinto><idrecinto>" + dataIterator.next() + "</idrecinto></recinto></Recintounidadnegocio>";
			}
		}

		String xmlRedes = "";
		if (recintos != null) {
			Iterator<String> dataIterator = redes.iterator();
			while (dataIterator.hasNext()) {
				xmlRedes = xmlRedes + "<Redunidadnegocio><red><idred>" + dataIterator.next() + "</idred></red></Redunidadnegocio>";
			}
		}

		String xmlUsuarios = "";
		if (usuarios != null) {
			Iterator<String> dataIterator = usuarios.iterator();
			while (dataIterator.hasNext()) {
				xmlUsuarios = xmlUsuarios + "<Usuariounidadnegocio><usuario><idusuario>" + dataIterator.next() + "</idusuario></usuario></Usuariounidadnegocio>";
			}
		}

		if (mostrar.equalsIgnoreCase("on"))
			mostrar = "1";
		else
			mostrar = "0";

		String xml = "";
		xml = xml + "<getEdicionAltaUnidadNegocio><Unidadnegocio><nombre>" + nombre + "</nombre>";
		xml = xml + "<idunidadnegocio>" + idunidadnegocio + "</idunidadnegocio>";
		xml = xml + "<orden>" + orden + "</orden><mostrar>" + mostrar + "</mostrar><dadodebaja>0</dadodebaja>";
		xml = xml + "<entidadgestora><identidadgestora>" + idEntidadGestora + "</identidadgestora></entidadgestora>";
		xml = xml + "<usuariounidadnegocios>" + xmlUsuarios + "</usuariounidadnegocios>";
		xml = xml + "<redunidadnegocios>" + xmlRedes + "</redunidadnegocios>";
		xml = xml + "<recintounidadnegocios>" + xmlRecintos + "</recintounidadnegocios>";
		xml = xml + "</Unidadnegocio></getEdicionAltaUnidadNegocio>";

		String xml_result = Tools.callServiceXML(request, "postEdicionAltaUnidadNegocio.action?_modo=&servicio=postEdicionAltaUnidadNegocio&xml=" + URLEncoder.encode(xml, "UTF-8"));
        
	
		if (xml_result.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);

	}



	// *****************************************************************************************************
	@RequestMapping("ajax/admon/recintos/recintos/remove_recintos.do")
	public ResponseEntity<String> recintosRemoveRecintos(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {
		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request, "darDeBajaRecintos.action?servicio=darDeBajaRecintos&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}



	// *****************************************************************************************************
	@RequestMapping("ajax/admon/recintos/recintos/bloquear_recintos.do")
	public ResponseEntity<String> recintosBloquearRecintos(@RequestParam(value = "data", required = true) String id_recinto, @RequestParam(value = "estado", required = true) String estado, HttpServletRequest request) throws Exception {
		String estado_final;

		if (estado.equals("0"))
			estado_final = "1";
		else
			estado_final = "0";

		String xml = Tools.callServiceXML(request, "bloquearRecintos.action?servicio=bloquearRecintos&bloqueado=" + estado_final + "&idrecinto=" + id_recinto);

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}



	// *****************************************************************************************************
	@RequestMapping("ajax/admon/recintos/recintos/show_recinto.do")
	public ModelAndView recintosShowRecinto(@RequestParam(value = "id", required = false, defaultValue = "") String idRecinto, HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.recintos.recintos.dialog.edit_recinto");

		if (idRecinto.length() > 0)
			model.addObject("editar_datos_recinto", Tools.callServiceXML(request, "getEdicionAltaRecintos.action?servicio=obtenerRecintoPorIdConI18n&xml=" + idRecinto));
		else
			model.addObject("editar_datos_recinto", "<empty/>");

		model.addObject("editar_datos_recinto_selector_unidades", Tools.callServiceXML(request, "obtenerListadoUnidadesNegocioFiltrado.action?servicio=obtenerListadoUnidadesNegocioFiltrado"));

		model.addObject("editarrecinto_datos_recinto", "<empty/>");

		return model;
	}



	// *****************************************************************************************************
	@RequestMapping("ajax/admon/recintos/recintos/show_zona.do")
	public ModelAndView recintosShowRecintoZona(
			@RequestParam(value = "id", required = false, defaultValue = "") String idZona, 
			@RequestParam(value = "creando", required = true, defaultValue = "true") String creando, 
			@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre, 
			@RequestParam(value = "aforo", required = false, defaultValue = "") String aforo, 
			@RequestParam(value = "descripcion", required = false, defaultValue = "") String descripcion,
			@RequestParam(value = "orden", required = false, defaultValue = "") String orden,
			@RequestParam(value = "numerada", required = false, defaultValue = "") String numerada,
			@RequestParam(value = "i18n_nombre", required = false, defaultValue = "") String i18nNombre,
			@RequestParam(value = "i18n_descripcion", required = false, defaultValue = "") String i18nDescripcion,
			@RequestParam(value = "subrecinto", required = false, defaultValue = "0") String subrecinto,
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();
				
		model.setViewName("app.admon.recintos.recintos.dialog.edit_zona");
		model.addObject("idzona", idZona);
		model.addObject("creando", creando);
		model.addObject("nombre", nombre);
		model.addObject("aforo", aforo);
		model.addObject("descripcion", descripcion);
		model.addObject("orden", orden);
		model.addObject("numerada", numerada);
		model.addObject("i18n_nombre", i18nNombre);
		model.addObject("i18n_descripcion", i18nDescripcion);
		model.addObject("subrecinto", subrecinto);

		return model;
	}
	
	// *****************************************************************************************************
	@RequestMapping("ajax/admon/recintos/recinto/save_recinto.do")
	public ResponseEntity<String> recintosSaveRecinto(
			@RequestParam(value = "idrecinto", required = false, defaultValue = "") String idrecinto, 
			@RequestParam(value = "zonasSeleccionadas", required = false, defaultValue = "") String zonasSeleccionadas, 
			@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
			@RequestParam(value = "horaapertura", required = false, defaultValue = "") String horaapertura,
			@RequestParam(value = "horacierre", required = false, defaultValue = "") String horacierre,
			@RequestParam(value = "maxreimpresiones", required = false, defaultValue = "") String maxreimpresiones,
			@RequestParam(value = "orden", required = false, defaultValue = "") String orden,	
			@RequestParam(value = "overbooking", required = false, defaultValue = "") String overbooking,
			@RequestParam(value = "bloqueado", required = false, defaultValue = "") String bloqueado,
			@RequestParam(value = "admitesolapamiento", required = false, defaultValue = "") String admitesolapamiento,
			@RequestParam(value = "unidades[]", required = false, defaultValue = "") List<String> unidades,
			@RequestParam(value = "taquillas[]", required = false, defaultValue = "") List<String> taquillas,		
			HttpServletRequest request) throws Exception {
		
		String xml = "";
		String xmlUnidades="",xmlTaquillas="";
		
		xmlUnidades = "<recintounidadnegocios>";
		if (unidades != null) {
			Iterator<String> dataIterator = unidades.iterator();
			while (dataIterator.hasNext()) {
				xmlUnidades = xmlUnidades + "<Recintounidadnegocio><unidadnegocio><idunidadnegocio>" + dataIterator.next() + "</idunidadnegocio></unidadnegocio></Recintounidadnegocio>";
			}
		}
		xmlUnidades = xmlUnidades + "</recintounidadnegocios>";

		
		xmlTaquillas = "<taquillas>";
		if (taquillas != null) {
			Iterator<String> dataIterator = taquillas.iterator();
			while (dataIterator.hasNext()) {
				xmlTaquillas = xmlTaquillas + "<Taquilla><idtaquilla>" + dataIterator.next() + "</idtaquilla></Taquilla>";
			}
		}
		xmlTaquillas = xmlTaquillas + "</taquillas>";		
	
		if(overbooking.equals("on"))
			overbooking = "true";
		else
			overbooking = "false";
		
		if(bloqueado.equals("on"))
			bloqueado = "true";
		else
			bloqueado = "false";
		
		if(admitesolapamiento.equals("on"))
			admitesolapamiento = "true";
		else
			admitesolapamiento = "false";
		
		String zonas[]= zonasSeleccionadas.split("\\$");
				
		String xmlZonas = "<zonas>";
		for( int i=0; i<zonas.length; i++)
			{
			String zona = zonas[i];
			String idzona = zona.substring(0,zona.indexOf("|"));
			zona = zona.substring(zona.indexOf("|")+1);
			String nombreZona = zona.substring(0,zona.indexOf("|"));
			zona = zona.substring(zona.indexOf("|")+1);
			String aforo = zona.substring(0,zona.indexOf("|"));
			zona = zona.substring(zona.indexOf("|")+1);
			String descripcion = zona.substring(0,zona.indexOf("|"));
			zona = zona.substring(zona.indexOf("|")+1);
			String ordenZona = zona.substring(0,zona.indexOf("|"));	
			zona = zona.substring(zona.indexOf("|")+1);
			String numerada = zona.substring(0,zona.indexOf("|"));	
			zona = zona.substring(zona.indexOf("|")+1);
			String i18n_nombre = zona.substring(0,zona.indexOf("|"));	
			String i18n_descripcion =  zona.substring(zona.indexOf("|")+1);			
			
			
			HashMap<String,String> i18n=new HashMap<String, String>();
			i18n.put("nombre", i18n_nombre);
			i18n.put("descripcion", i18n_descripcion);
						
			xmlZonas +="<Zona><idzona>"+idzona+"</idzona><nombre>"+nombreZona+"</nombre>";
			xmlZonas +="<descripcion>"+descripcion+"</descripcion><aforo>"+aforo+"</aforo>";
			xmlZonas +="<orden>"+ordenZona+"</orden><numerada>"+numerada+"</numerada><color/><dadodebaja>false</dadodebaja>";
			xmlZonas +="<i18ns>"+Tools.generateI18nXMLFromForm(i18n,nombreZona)+"</i18ns>";
			xmlZonas +="</Zona>";
			}
		
		xmlZonas += "</zonas>";
							
		xml +="<getEdicionAltaRecintos><Recinto><idrecinto>"+idrecinto+"</idrecinto><nombre>"+nombre+"</nombre><dadodebaja>false</dadodebaja>";
		xml +="<maxreimpresiones>"+maxreimpresiones+"</maxreimpresiones><bloqueado>"+bloqueado+"</bloqueado>";
		xml += "<admitesolapamiento>"+admitesolapamiento+"</admitesolapamiento><horaapertura>"+horaapertura+"</horaapertura><horacierre>"+horacierre+"</horacierre>";
		xml	+= "<overbooking>"+overbooking+"</overbooking><recintos/>";
		xml += xmlUnidades;
		xml += xmlTaquillas;
		xml += xmlZonas;
		xml += "<orden>"+orden+"</orden></Recinto></getEdicionAltaRecintos>";
				
		String xml_result = Tools.callServiceXML(request, "postEdicionAltaRecinto.action?_modo=&servicio=postEdicionAltaRecinto&xml=" + URLEncoder.encode(xml, "UTF-8"));

		if (xml_result.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
		}
		
		if(xml_result.length()>6)
			idrecinto = xml_result.substring(13, xml_result.indexOf("</Integer></ok>"));
			
		return new ResponseEntity<String>(idrecinto, HttpStatus.OK);

	}

	
	
	// *****************************************************************************************************
	@RequestMapping("ajax/admon/recintos/recintos/show_subrecintos.do")
	public ModelAndView recintosShowSubRecinto(
			@RequestParam(value = "id", required = false, defaultValue = "") String idSubRecinto, 
			@RequestParam(value = "id_padre", required = false, defaultValue = "") String id_padre,
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.recintos.recintos.dialog.edit_subrecinto");

		model.addObject("id_padre", id_padre);
		
		if (idSubRecinto.length() > 0)
			model.addObject("editar_datos_subrecinto", Tools.callServiceXML(request, "getEdicionAltaSubRecintos.action?servicio=getEdicionAltaSubRecintos&idsubrecinto="+idSubRecinto+"&xml="+id_padre));
		else
			model.addObject("editar_datos_subrecinto", "<empty/>");

		model.addObject("editar_datos_recinto_selector_unidades", Tools.callServiceXML(request, "obtenerListadoUnidadesNegocioFiltrado.action?servicio=obtenerListadoUnidadesNegocioFiltrado"));

		return model;
	}

	
	// *****************************************************************************************************
	@RequestMapping("ajax/admon/recintos/recinto/save_subRecinto.do")
	public ResponseEntity<String> recintosSaveSubRecinto(
			@RequestParam(value = "idPadre", required = false, defaultValue = "") String idPadre,
			@RequestParam(value = "idrecinto", required = false, defaultValue = "") String idrecinto, 
			@RequestParam(value = "zonasSeleccionadas", required = false, defaultValue = "") String zonasSeleccionadas, 
			@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
			@RequestParam(value = "horaapertura", required = false, defaultValue = "") String horaapertura,
			@RequestParam(value = "horacierre", required = false, defaultValue = "") String horacierre,
			@RequestParam(value = "maxreimpresiones", required = false, defaultValue = "") String maxreimpresiones,
			@RequestParam(value = "orden", required = false, defaultValue = "") String orden,	
			@RequestParam(value = "overbooking", required = false, defaultValue = "") String overbooking,
			@RequestParam(value = "bloqueado", required = false, defaultValue = "") String bloqueado,
			@RequestParam(value = "admitesolapamiento", required = false, defaultValue = "") String admitesolapamiento,
			@RequestParam(value = "unidades[]", required = false, defaultValue = "") List<String> unidades,
			@RequestParam(value = "taquillas[]", required = false, defaultValue = "") List<String> taquillas,		
			HttpServletRequest request) throws Exception {
		
		String xml = "";
		String xmlUnidades="",xmlTaquillas="";
		
		xmlUnidades = "<recintounidadnegocios>";
		if (unidades != null) {
			Iterator<String> dataIterator = unidades.iterator();
			while (dataIterator.hasNext()) {
				xmlUnidades = xmlUnidades + "<Recintounidadnegocio><unidadnegocio><idunidadnegocio>" + dataIterator.next() + "</idunidadnegocio></unidadnegocio></Recintounidadnegocio>";
			}
		}
		xmlUnidades = xmlUnidades + "</recintounidadnegocios>";

		
		xmlTaquillas = "<taquillas>";
		if (taquillas != null) {
			Iterator<String> dataIterator = taquillas.iterator();
			while (dataIterator.hasNext()) {
				xmlTaquillas = xmlTaquillas + "<Taquilla><idtaquilla>" + dataIterator.next() + "</idtaquilla></Taquilla>";
			}
		}
		xmlTaquillas = xmlTaquillas + "</taquillas>";		
	
		if(overbooking.equals("on"))
			overbooking = "true";
		else
			overbooking = "false";
		
		if(bloqueado.equals("on"))
			bloqueado = "true";
		else
			bloqueado = "false";
		
		if(admitesolapamiento.equals("on"))
			admitesolapamiento = "true";
		else
			admitesolapamiento = "false";
		
		String zonas[]=  zonasSeleccionadas.split("\\$");

		String xmlZonas = "<zonas>";
		for( int i=0; i<zonas.length; i++)
			{
			String zona = zonas[i];
			String idzona = zona.substring(0,zona.indexOf("|"));
			zona = zona.substring(zona.indexOf("|")+1);
			String nombreZona = zona.substring(0,zona.indexOf("|"));
			zona = zona.substring(zona.indexOf("|")+1);
			String aforo = zona.substring(0,zona.indexOf("|"));
			zona = zona.substring(zona.indexOf("|")+1);
			String descripcion = zona.substring(0,zona.indexOf("|"));
			zona = zona.substring(zona.indexOf("|")+1);
			String ordenZona = zona.substring(0,zona.indexOf("|"));	
			String numerada =  zona.substring(zona.indexOf("|")+1);

			
			xmlZonas += "<Zona><idzona>"+idzona+"</idzona><nombre>"+nombreZona+"</nombre>";
			xmlZonas += "<descripcion>"+descripcion+"</descripcion><aforo>"+aforo+"</aforo>";
			xmlZonas +="<orden>"+ordenZona+"</orden><numerada>"+numerada+"</numerada><color/><dadodebaja>false</dadodebaja><i18ns/></Zona>";
			}
		xmlZonas += "</zonas>";						
		
		xml +="<getEdicionAltaSubRecintos><Recinto><idrecinto>"+idrecinto+"</idrecinto><nombre>"+nombre+"</nombre><dadodebaja>false</dadodebaja>";
		xml +="<maxreimpresiones>"+maxreimpresiones+"</maxreimpresiones><bloqueado>"+bloqueado+"</bloqueado>";
		xml += "<admitesolapamiento>"+admitesolapamiento+"</admitesolapamiento><horaapertura>"+horaapertura+"</horaapertura><horacierre>"+horacierre+"</horacierre>";
		xml	+= "<overbooking>"+overbooking+"</overbooking><recintos/>";
		xml += xmlUnidades;
		xml += xmlTaquillas;
		xml += xmlZonas;
		xml += "<recinto><idrecinto>"+idPadre+"</idrecinto></recinto>";
		xml += "<orden>"+orden+"</orden></Recinto></getEdicionAltaSubRecintos>";

		String xml_result = Tools.callServiceXML(request, "postEdicionAltaSubRecinto.action?servicio=postEdicionAltaSubRecinto&xml=" + URLEncoder.encode(xml, "UTF-8"));

		if (xml_result.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<String>(HttpStatus.OK);

	}

	
	// *****************************************************************************************************
	@RequestMapping("ajax/admon/productos/productos/show_producto.do")
	public ModelAndView productosShowProducto(
			@RequestParam(value = "id", required = false, defaultValue = "") String idProducto,
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.recintos.productos.dialog.edit_producto");

		
		model.addObject("editarproducto_selector_tarifa", Tools.callServiceXML(request, "obtenerListadoTarifas.action?servicio=obtenerListadoTarifas"));
		model.addObject("editarproducto_tarifas_no_vigente", Tools.callServiceXML(request, "obtenerTarifasNoBonoNoVigentesPorProducto.action?servicio=obtenerTarifasNoBonoNoVigentesPorProducto&xml=<int>"+idProducto+"</int>"));

		model.addObject("editarproducto_selector_iva", Tools.callServiceXML(request, "btenerListadoTiposIva.action?servicio=obtenerListadoTiposIva"));
		model.addObject("editarproducto_selector_tipo_producto", Tools.callServiceXML(request, "obtenerListadoTiposProductoFiltrado.action?servicio=obtenerListadoTiposProductoFiltrado"));
				
		if (idProducto.length() > 0)
			model.addObject("editar_producto", Tools.callServiceXML(request, "getEdicionAltaProducto.action?servicio=obtenerProductoPorIdNocaducadasConI18n&xml="+idProducto));
		else
			model.addObject("editar_producto", "<empty/>");

		return model;
	}
	
	
	// *****************************************************************************************************
	@RequestMapping("ajax/admon/productos/productos/save_producto.do")
	public ResponseEntity<String> productosSaveProducto(
			@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
			@RequestParam(value = "i18nFieldNombre_producto", required = false, defaultValue = "") String i18nNombre,
			@RequestParam(value = "fechafinvigencia", required = false, defaultValue = "") String fechafinvigencia,
			@RequestParam(value = "fechainiciovigencia", required = false, defaultValue = "") String fechainiciovigencia,
			@RequestParam(value = "descripcion", required = false, defaultValue = "") String descripcion,
			@RequestParam(value = "idproducto", required = false, defaultValue = "") String idproducto,
			@RequestParam(value = "fechafinventa", required = false, defaultValue = "") String fechafinventa,
			@RequestParam(value = "fechainicioventa", required = false, defaultValue = "") String fechainicioventa,
			@RequestParam(value = "idiva", required = false, defaultValue = "") String idiva,
			@RequestParam(value = "orden", required = false, defaultValue = "") String orden,
			@RequestParam(value = "tipoproducto[]", required = false, defaultValue = "") List<String> tipoproducto,
			@RequestParam(value = "imprimirEntradasXDefecto", required = false, defaultValue = "0") String imprimirEntradasXDefecto,
			@RequestParam(value = "disponibleparabono", required = false, defaultValue = "0") String disponibleparabono,
			@RequestParam(value = "principal", required = false, defaultValue = "0") String principal,
			@RequestParam(value = "tarifasSeleccionadas", required = false, defaultValue = "") String tarifasSeleccionadas,
			@RequestParam(value = "tarifasBonoSeleccionadas", required = false, defaultValue = "") String tarifasBonoSeleccionadas,
			HttpServletRequest request)
			throws Exception 
	{
		String xml = "", xml_tarifas = "",xml_tarifas_bono = "",xml_tipo_producto = "";
		String tarifas[];
		HashMap<String,String> i18n=new HashMap<String, String>();
		
		if(imprimirEntradasXDefecto.equalsIgnoreCase("0"))
			imprimirEntradasXDefecto="false";

		
		i18n.put("nombre", i18nNombre);
			
		xml_tarifas = "<tarifaproductos>";
		if(!tarifasSeleccionadas.equalsIgnoreCase(""))
		{
			tarifas= tarifasSeleccionadas.split("\\$");	
			for( int i=0; i<tarifas.length; i++)
				{
				String tarifa = tarifas[i];
				String idtarifaproducto = tarifa.substring(0,tarifa.indexOf("|"));
				tarifa = tarifa.substring(tarifa.indexOf("|")+1);
				String idtarifa = tarifa.substring(0,tarifa.indexOf("|"));
				tarifa = tarifa.substring(tarifa.indexOf("|")+1);
				String fechainicionovigencia_tarifa = tarifa.substring(0,tarifa.indexOf("|"));
				tarifa = tarifa.substring(tarifa.indexOf("|")+1);
				String fechafinvigencia_tarifa = tarifa.substring(0,tarifa.indexOf("|"));
				String importe = tarifa.substring(tarifa.indexOf("|")+1);
							
				xml_tarifas = xml_tarifas + "<Tarifaproducto>";
				xml_tarifas = xml_tarifas + "<idtarifaproducto>"+idtarifaproducto+"</idtarifaproducto>";
				xml_tarifas = xml_tarifas + "<fechainiciovigencia>"+fechainicionovigencia_tarifa+"</fechainiciovigencia>";
				xml_tarifas = xml_tarifas + "<fechafinvigencia>"+fechafinvigencia_tarifa+"</fechafinvigencia>";
				xml_tarifas = xml_tarifas + "<importe>"+importe+"</importe>";
				xml_tarifas = xml_tarifas + "<tarifa><idtarifa>"+idtarifa+"</idtarifa></tarifa><bono>0</bono>";
				xml_tarifas = xml_tarifas + "</Tarifaproducto>";			
				}
			}
		xml_tarifas = xml_tarifas + "</tarifaproductos>";
		
		
			
		xml_tarifas_bono = "<tarifaproductosbono>";
		
		if(!tarifasBonoSeleccionadas.equalsIgnoreCase(""))
		{	
		tarifas= tarifasBonoSeleccionadas.split("\\$");
		for( int i=0; i<tarifas.length; i++)
			{
			String tarifa = tarifas[i];
			String idtarifaproducto = tarifa.substring(0,tarifa.indexOf("|"));
			tarifa = tarifa.substring(tarifa.indexOf("|")+1);
			String idtarifa = tarifa.substring(0,tarifa.indexOf("|"));
			tarifa = tarifa.substring(tarifa.indexOf("|")+1);
			String fechainicionovigencia_tarifa = tarifa.substring(0,tarifa.indexOf("|"));
			tarifa = tarifa.substring(tarifa.indexOf("|")+1);
			String fechafinvigencia_tarifa = tarifa.substring(0,tarifa.indexOf("|"));
			String importe = tarifa.substring(tarifa.indexOf("|")+1);
			
			
			
			xml_tarifas_bono = xml_tarifas_bono + "<Tarifaproducto>";
			xml_tarifas_bono = xml_tarifas_bono + "<idtarifaproducto>"+idtarifaproducto+"</idtarifaproducto>";
			xml_tarifas_bono = xml_tarifas_bono + "<fechainiciovigencia>"+fechainicionovigencia_tarifa+"</fechainiciovigencia>";
			xml_tarifas_bono = xml_tarifas_bono + "<fechafinvigencia>"+fechafinvigencia_tarifa+"</fechafinvigencia>";
			xml_tarifas_bono = xml_tarifas_bono + "<importe>"+importe+"</importe>";
			xml_tarifas_bono = xml_tarifas_bono + "<tarifa><idtarifa>"+idtarifa+"</idtarifa></tarifa><bono>0</bono>";
			xml_tarifas_bono = xml_tarifas_bono + "</Tarifaproducto>";			
			}
		}
		
		xml_tarifas_bono = xml_tarifas_bono + "</tarifaproductosbono>";		
			
		if (tipoproducto != null) {
			Iterator<String> dataIterator = tipoproducto.iterator();
			while (dataIterator.hasNext()) {				
				xml_tipo_producto = xml_tipo_producto + "<Tipoprodproducto>";
				xml_tipo_producto = xml_tipo_producto + "<tipoproducto><idtipoproducto>"+dataIterator.next()+"</idtipoproducto></tipoproducto>";
				xml_tipo_producto = xml_tipo_producto + "</Tipoprodproducto>";				
			}
		}
				
		
		if(imprimirEntradasXDefecto.startsWith("on"))
			imprimirEntradasXDefecto = "1";
		
		if(disponibleparabono.startsWith("on"))
			disponibleparabono = "1";
		
		if(principal.startsWith("on"))
			principal = "1";
		
				
		
		xml= xml + "<getEdicionAltaProducto><Producto>";
		
		xml= xml + "<nombre>"+nombre+"</nombre><fechafinvigencia>"+fechafinvigencia+"</fechafinvigencia>";
		xml= xml + "<fechainiciovigencia>"+fechainiciovigencia+"</fechainiciovigencia><descripcion>"+descripcion+"</descripcion>";
		xml= xml + "<idproducto>"+idproducto+"</idproducto><dadodebaja>0</dadodebaja><fechafinventa>"+fechafinventa+"</fechafinventa>";
		xml= xml + "<fechainicioventa>"+fechainicioventa+"</fechainicioventa><iva><idiva>"+idiva+"</idiva></iva>";
		xml= xml + "<disponibleparaabono>0</disponibleparaabono><disponibleparabono>"+disponibleparabono+"</disponibleparabono>";
		xml= xml + "<principal>"+principal+"</principal><orden>"+orden+"</orden><imprimirEntradasXDefecto>"+imprimirEntradasXDefecto+"</imprimirEntradasXDefecto>";
		xml= xml + xml_tarifas;
		xml= xml + xml_tarifas_bono;
		xml= xml + "<tipoprodproductos>"+xml_tipo_producto+"</tipoprodproductos>";
		xml= xml + "<i18ns>"+Tools.generateI18nXMLFromForm(i18n, nombre)+"</i18ns>";
		xml= xml + "<productoscanals/>";
		
		xml= xml+ "</Producto></getEdicionAltaProducto>";
		
		String xml_result = "";
		if(idproducto.equalsIgnoreCase(""))
			xml_result = Tools.callServiceXML(request, "postEdicionAltaProducto.action?_modo=alta&servicio=postEdicionAltaProducto&xml=" + URLEncoder.encode(xml, "UTF-8"));
		else
			xml_result = Tools.callServiceXML(request, "postEdicionAltaProducto.action?_modo=edicion&servicio=postEdicionAltaProducto&xml=" + URLEncoder.encode(xml, "UTF-8"));
		
		if (xml_result.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
		}	
		
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	
	// *****************************************************************************************************
	@RequestMapping("ajax/admon/productos/productos/show_tipo_producto.do")
	public ModelAndView productosShowTipoProducto(
			@RequestParam(value = "id", required = false, defaultValue = "") String idTipoProducto,
			@RequestParam(value = "idUnidadNegocio", required = false, defaultValue = "") String idUnidadNegocio,
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.recintos.productos.dialog.edit_tipo_producto");
		
		model.addObject("editar_tipo_producto_selector_recintos", Tools.callServiceXML(request, "obtenerListadoRecintosPorUnidadNegocio.action?servicio=obtenerListadoRecintosPorUnidadNegocio&xml=<int>"+idUnidadNegocio+"</int>"));
		
		model.addObject("editar_tipo_producto_selector_atributo", Tools.callServiceXML(request, "obtenerListadoTiposAtributo.action?servicio=obtenerListadoTiposAtributo"));
				
		if (idTipoProducto.length() > 0)
			model.addObject("editar_tipo_producto", Tools.callServiceXML(request, "getEdicionAltaTipoProducto.action?servicio=obtenerTipoProductoPorIdConI18n&xml="+idTipoProducto));
		else
			model.addObject("editar_tipo_producto", "<empty/>");

		return model;
	}
	
	
	// *****************************************************************************************************
	@RequestMapping("ajax/admon/productos/productos/save_tipoproducto.do")
	public ResponseEntity<String> productosSaveTipoProducto(
			@RequestParam(value = "idtipoproducto", required = false, defaultValue = "") String idtipoproducto,
			@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
			@RequestParam(value = "descripcion", required = false, defaultValue = "") String descripcion,
			@RequestParam(value = "nummaxusosentrada", required = false, defaultValue = "") String nummaxusosentrada,
			@RequestParam(value = "orden", required = false, defaultValue = "") String orden,
			@RequestParam(value = "idrecinto", required = false, defaultValue = "") String idrecinto,
			@RequestParam(value = "i18nFieldNombre_tipoproducto", required = false, defaultValue = "") String i18nNombre,
			@RequestParam(value = "atributosSeleccionados", required = false, defaultValue = "") String atributosSeleccionados,
			HttpServletRequest request) throws Exception {
		
		String xml = "",xml_atributos;
		String atributos[];
		
		
		HashMap<String,String> i18n=new HashMap<String, String>();
		i18n.put("nombre", i18nNombre);
		
		
		xml_atributos = "<atributos>";		
		if(!atributosSeleccionados.equalsIgnoreCase(""))
		{	
        atributos= atributosSeleccionados.split("\\$");
		for( int i=0; i<atributos.length; i++)
			{
			String atributo = atributos[i];
			String idatributo = atributo.substring(0,atributo.indexOf("|"));
			atributo = atributo.substring(atributo.indexOf("|")+1);
			String idtipoatributo = atributo.substring(0,atributo.indexOf("|"));
			String nombreAtributo  = atributo.substring(atributo.indexOf("|")+1);
			xml_atributos = xml_atributos + "<Atributo><idatributo>"+idatributo+"</idatributo><dadodebaja>0</dadodebaja>";
			xml_atributos = xml_atributos + "<nombre>"+nombreAtributo+"</nombre><descripcion/><orden/><tipoatributo>";
			xml_atributos = xml_atributos + "<idtipoatributo>"+idtipoatributo+"</idtipoatributo></tipoatributo></Atributo>";
			}		
		}
		xml_atributos = xml_atributos + "</atributos>";
		
		
		xml = xml + "<getEdicionAltaTipoProducto><Tipoproducto>";
		xml = xml + "<idtipoproducto>"+idtipoproducto+"</idtipoproducto><nombre>"+nombre+"</nombre>";
		xml = xml + "<descripcion>"+descripcion+"</descripcion><nummaxusosentrada>"+nummaxusosentrada+"</nummaxusosentrada>";
		xml = xml + "<orden>"+orden+"</orden>";
		xml = xml + "<recinto><idrecinto>"+idrecinto+"</idrecinto></recinto>";
		xml = xml + xml_atributos;		
		xml= xml + "<i18ns>"+Tools.generateI18nXMLFromForm(i18n, nombre)+"</i18ns>";
		xml = xml +"</Tipoproducto></getEdicionAltaTipoProducto>";
		
		String xml_result = "";
		if(idtipoproducto.equalsIgnoreCase(""))
			xml_result = Tools.callServiceXML(request, "postEdicionAltaTipoProducto.action?_modo=alta&servicio=postEdicionAltaTipoProducto&xml=" + URLEncoder.encode(xml, "UTF-8"));
		else 
			xml_result = Tools.callServiceXML(request, "postEdicionAltaTipoProducto.action?_modo=edicion&servicio=postEdicionAltaTipoProducto&xml=" + URLEncoder.encode(xml, "UTF-8"));
		
		if (xml_result.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
		}	
	
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	// *****************************************************************************************************
	@RequestMapping("ajax/admon/productos/contenidos/show_atributo.do")
	public ModelAndView productosShowAtributosContenidos(
			@RequestParam(value = "idtipoproducto", required = false, defaultValue = "") String idtipoproducto,
			@RequestParam(value = "idsSeleccionados", required = false, defaultValue = "") String idsSeleccionados,
			@RequestParam(value = "idatributoscontenido", required = false, defaultValue = "") String idatributoscontenido,
			@RequestParam(value = "idatributo", required = false, defaultValue = "") String idatributo,
			@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
			@RequestParam(value = "valor", required = false, defaultValue = "") String valor,
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();
		
		model.setViewName("app.admon.productos.contenidos.dialog.edit_atributo");
				
		if(idatributo.equalsIgnoreCase(""))
		{
			String filter = "<envio>";
			if(!idsSeleccionados.equalsIgnoreCase(""))
			{
			String ids[] = idsSeleccionados.split("\\|");
			for(int i=0; i<ids.length;i++)
				{
				filter  = filter +"<parametro id=\""+i+"\">"+ids[i]+"</parametro>";
				}
			}	
			filter  = filter +"</envio>";	 
			
			System.out.println("Aqui se va a la mierda");
			
			model.addObject("editar_tipo_producto_selector_atributo", Tools.callServiceXML(request, "obtenerAtributosPorTipoProductoFiltrado.action?servicio=obtenerAtributosPorTipoProductoFiltrado&combo="+idtipoproducto+"&filter="+ URLEncoder.encode(filter, "UTF-8")));
		}
		else
		{
			model.addObject("idatributoscontenido",idatributoscontenido);
			model.addObject("idatributo",idatributo);
			model.addObject("nombre",nombre);
			model.addObject("valor",valor);
			model.addObject("editar_tipo_producto_selector_atributo", "<empty/>");
		}
		return model;
	}
	
	
	// *****************************************************************************************************
		@RequestMapping("ajax/admon/productos/productos/save_contenido.do")
		public ResponseEntity<String> productosSaveContenido(
				@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
				@RequestParam(value = "i18nFieldNombre_contenido", required = false, defaultValue = "") String i18nNombre,
				@RequestParam(value = "i18nFieldDescripcion_contenido", required = false, defaultValue = "") String i18nDescripcion,
				@RequestParam(value = "descripcion", required = false, defaultValue = "") String descripcion,
				@RequestParam(value = "idcontenido", required = false, defaultValue = "") String idcontenido,
				@RequestParam(value = "aforo", required = false, defaultValue = "") String aforo,
				@RequestParam(value = "duracion", required = false, defaultValue = "") String duracion,
				@RequestParam(value = "orden", required = false, defaultValue = "") String orden,
				@RequestParam(value = "referenciacoste", required = false, defaultValue = "") String referenciacoste,
				@RequestParam(value = "escentrocoste", required = false, defaultValue = "") String escentrocoste,
				@RequestParam(value = "idtipo", required = false, defaultValue = "") String idtipoproducto,
				@RequestParam(value = "iddistribuidora", required = false, defaultValue = "") String iddistribuidora,
				@RequestParam(value = "idmarca", required = false, defaultValue = "") String idmarca,
				@RequestParam(value = "tarifasSeleccionadas", required = false, defaultValue = "") String tarifasSeleccionadas,
				@RequestParam(value = "atributosSeleccionados", required = false, defaultValue = "") String atributosSeleccionados,
				@RequestParam(value = "idiomas[]", required = false, defaultValue = "") List<String> idiomas,
				HttpServletRequest request) throws Exception {
			
			
			
			
			HashMap<String,String> i18n=new HashMap<String, String>();		
			i18n.put("nombre", i18nNombre);
			
			HashMap<String,String> i18d=new HashMap<String, String>();		
			i18d.put("descripcion", i18nDescripcion);
			
			String tarifas[],atributos[];
			String xml_tarifas,xml_atributos,xml_idiomas,xml_result;
			
			xml_tarifas = "<tarifaproductos>";
			if(!tarifasSeleccionadas.equalsIgnoreCase(""))
			{
				tarifas= tarifasSeleccionadas.split("\\$");	
				for( int i=0; i<tarifas.length; i++)
					{
					String tarifa = tarifas[i];
					String idtarifaproducto = tarifa.substring(0,tarifa.indexOf("|"));
					tarifa = tarifa.substring(tarifa.indexOf("|")+1);
					String idtarifa = tarifa.substring(0,tarifa.indexOf("|"));
					tarifa = tarifa.substring(tarifa.indexOf("|")+1);
					String fechainicionovigencia_tarifa = tarifa.substring(0,tarifa.indexOf("|"));
					tarifa = tarifa.substring(tarifa.indexOf("|")+1);
					String fechafinvigencia_tarifa = tarifa.substring(0,tarifa.indexOf("|"));
					String importe = tarifa.substring(tarifa.indexOf("|")+1);
								
					xml_tarifas = xml_tarifas + "<Tarifaproducto>";
					xml_tarifas = xml_tarifas + "<idtarifaproducto>"+idtarifaproducto+"</idtarifaproducto>";
					xml_tarifas = xml_tarifas + "<fechainiciovigencia>"+fechainicionovigencia_tarifa+"</fechainiciovigencia>";
					xml_tarifas = xml_tarifas + "<fechafinvigencia>"+fechafinvigencia_tarifa+"</fechafinvigencia>";
					xml_tarifas = xml_tarifas + "<importe>"+importe+"</importe>";
					xml_tarifas = xml_tarifas + "<tarifa><idtarifa>"+idtarifa+"</idtarifa></tarifa><bono>0</bono>";
					xml_tarifas = xml_tarifas + "</Tarifaproducto>";			
					}
				}
			xml_tarifas = xml_tarifas + "</tarifaproductos>";
			
			
			xml_atributos = "<atributoscontenidos>";		
			if(!atributosSeleccionados.equalsIgnoreCase(""))
			{	
	        atributos= atributosSeleccionados.split("\\$");
			for( int i=0; i<atributos.length; i++)
				{
				String atributo = atributos[i];
				String idatributoscontenido = atributo.substring(0,atributo.indexOf("|"));
				atributo = atributo.substring(atributo.indexOf("|")+1);
				String idtipoatributo = atributo.substring(0,atributo.indexOf("|"));
				String valor  = atributo.substring(atributo.indexOf("|")+1);
				xml_atributos = xml_atributos + "<Atributoscontenido><idatributoscontenido>"+idatributoscontenido+"</idatributoscontenido><dadodebaja>0</dadodebaja>";
				xml_atributos = xml_atributos + "<valor>"+valor+"</valor><descripcion/><orden/>";
				xml_atributos = xml_atributos + "<atributo><idatributo>"+idtipoatributo+"</idatributo></atributo></Atributoscontenido>";
				}		
			}
			xml_atributos = xml_atributos + "</atributoscontenidos>";
			
			xml_idiomas = "<idiomacontenidocontenidos><Idiomaprod>";
			if (idiomas != null) {
				Iterator<String> dataIterator = idiomas.iterator();
				while (dataIterator.hasNext()) {				
					xml_idiomas = xml_idiomas + "<idiomacontenido>";
					xml_idiomas = xml_idiomas + "<ididiomacontenido>"+dataIterator.next()+"</ididiomacontenido>";
					xml_idiomas = xml_idiomas + "</idiomacontenido>";				
				}
			}

			xml_idiomas = xml_idiomas + "</Idiomaprod></idiomacontenidocontenidos>";
					
			String xml="<getEdicionAltaContenidos><Contenido><idcontenido>"+idcontenido+"</idcontenido>";
			xml = xml + "<nombre>"+nombre+"</nombre><descripcion>"+descripcion+"</descripcion>";
			xml = xml + "<aforo>"+aforo+"</aforo><duracion>"+duracion+"</duracion><orden>"+orden+"</orden>";
			xml = xml + "<referenciacoste>"+referenciacoste+"</referenciacoste><escentrocoste>"+escentrocoste+"</escentrocoste>";
			xml = xml + "<tipoproducto><idtipoproducto>"+idtipoproducto+"</idtipoproducto></tipoproducto>";
			xml = xml + "<distribuidora><iddistribuidora>"+iddistribuidora+"</iddistribuidora></distribuidora>";
			xml = xml + "<marca><idmarca>"+idmarca+"</idmarca></marca>";
			xml = xml + xml_idiomas;
			xml = xml + xml_tarifas;
			xml = xml + xml_atributos;
			xml= xml + "<i18ns>"+Tools.generateI18nXMLFromFormNombreDescripcion(i18n, i18d, nombre)+"</i18ns>";
			xml = xml +"</Contenido></getEdicionAltaContenidos>";
			
			if(idcontenido.equalsIgnoreCase(""))
				xml_result = Tools.callServiceXML(request, "postEdicionAltaContenidos.action?_modo=alta&servicio=postEdicionAltaContenidos&xml=" + URLEncoder.encode(xml, "UTF-8"));
			else
				xml_result = Tools.callServiceXML(request, "postEdicionAltaContenidos.action?_modo=edicion&servicio=postEdicionAltaContenidos&xml=" + URLEncoder.encode(xml, "UTF-8"));
			
			if (xml_result.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
			}	
			
			return new ResponseEntity<String>(HttpStatus.OK);
		}
		
		
		// *****************************************************************************************************
		@RequestMapping("ajax/admon/productos/canales/show_canal.do")
		public ModelAndView productosShowCanal(
				@RequestParam(value = "id", required = false, defaultValue = "") String idCanal,
				HttpServletRequest request) throws Exception {

			ModelAndView model = new ModelAndView();

			model.setViewName("app.admon.productos.canales.dialog.edit_canal");
					
			model.addObject("editar_canal_selector_unidad", Tools.callServiceXML(request, "obtenerListadoUnidadesNegocio.action?servicio=obtenerListadoUnidadesNegocio"));
			
			model.addObject("editar_canal_selector_tipo_canal", Tools.callServiceXML(request, "obtenerListadoTiposCanal.action?servicio=obtenerListadoTiposCanal"));
			
			model.addObject("editar_canal_selector_operacion", Tools.callServiceXML(request, "obtenerListadoOperacionesFiltrado.action?servicio=obtenerListadoOperacionesFiltrado"));			
			
			model.addObject("editar_canal_selector_tarifas", Tools.callServiceXML(request, "obtenerListadoTarifasFiltrado.action?servicio=obtenerListadoTarifasFiltrado"));
			
			model.addObject("editar_canal_selector_formasPago", Tools.callServiceXML(request, "obtenerListadoFormasPagoFiltrado.action?servicio=obtenerListadoFormasPagoFiltrado"));
			
			model.addObject("editar_canal_selector_productos", Tools.callServiceXML(request, "obtenerListadoProductosFiltrado.action?servicio=obtenerListadoProductosFiltrado"));
			
			model.addObject("editar_canal_selector_descuentosPromo", Tools.callServiceXML(request, "obtenerListadoDescuentosPromocionalesFiltrado.action?servicio=obtenerListadoDescuentosPromocionalesFiltrado"));
			
			
			
			if (idCanal.length() > 0)
				model.addObject("editar_canal", Tools.callServiceXML(request, "getDarDeAltaCanal.action?servicio=obtenerCanalPorId&xml="+idCanal));
			else
				model.addObject("editar_canal", "<empty/>");

			return model;
		}

		// *****************************************************************************************************
		@RequestMapping("ajax/admon/productos/productos/save_canal.do")
		public ResponseEntity<String> productosSaveCanal(
				@RequestParam(value = "idcanal", required = false, defaultValue = "") String idcanal,
				@RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
				@RequestParam(value = "descripcion", required = false, defaultValue = "") String descripcion,
				@RequestParam(value = "idcliente", required = false, defaultValue = "") String idcliente,
				@RequestParam(value = "orden", required = false, defaultValue = "") String orden,
				@RequestParam(value = "idtipocanal", required = false, defaultValue = "") String idtipocanal,
				@RequestParam(value = "idunidadnegocio", required = false, defaultValue = "") String idunidadnegocio,
				@RequestParam(value = "productos[]", required = false, defaultValue = "")  List<String> productos,
				@RequestParam(value = "tarifas[]", required = false, defaultValue = "")  List<String> tarifas,
				@RequestParam(value = "formaspago[]", required = false, defaultValue = "")  List<String> formaspago,
				@RequestParam(value = "descuentosPromocional[]", required = false, defaultValue = "")  List<String> descuentosPromocional,
				@RequestParam(value = "operaciones[]", required = false, defaultValue = "")  List<String> operaciones,			
				HttpServletRequest request)  throws Exception {
			
			String xml="",xml_result,xml_tarifas,xml_productos,xml_formaspago,xml_descuentos,xml_operaciones;
			
			xml_tarifas = "<tarifacanals>";
			if (tarifas != null) {
				Iterator<String> dataIterator = tarifas.iterator();
				while (dataIterator.hasNext()) {
					xml_tarifas = xml_tarifas + "<Tarifacanal><tarifa><idtarifa>"+dataIterator.next()+"</idtarifa></tarifa></Tarifacanal>";
				}
			}
			xml_tarifas  = xml_tarifas + "</tarifacanals>";
			
			xml_productos = "<productoscanals>";
			if (productos != null) {
				Iterator<String> dataIterator = productos.iterator();
				while (dataIterator.hasNext()) {
					xml_productos = xml_productos + "<Productoscanal><producto><idproducto>"+dataIterator.next()+"</idproducto></producto></Productoscanal>";
				}
			}
			xml_productos = xml_productos + "</productoscanals>";                        
           
			
			
			xml_formaspago = "<formapagocanals>";
			if (formaspago != null) {
				Iterator<String> dataIterator = formaspago.iterator();
				while (dataIterator.hasNext()) {
					xml_formaspago = xml_formaspago + "<Formapagocanal><formapago><idformapago>"+dataIterator.next()+"</idformapago></formapago></Formapagocanal>";
				}
			}
			xml_formaspago = xml_formaspago + "</formapagocanals>";
			
			xml_descuentos = "<descuentopromocionalcanals>";
			if (descuentosPromocional != null) {
				Iterator<String> dataIterator = descuentosPromocional.iterator();
				while (dataIterator.hasNext()) {
					xml_descuentos = xml_descuentos + "<Descuentopromocionalcanal><descuentopromocional><iddescuentopromocional>"+dataIterator.next()+"</iddescuentopromocional></descuentopromocional></Descuentopromocionalcanal>";
				}
			}
			xml_descuentos = xml_descuentos + "</descuentopromocionalcanals>";   
			
			
			xml_operaciones = "<operacioncanals>";
			if (operaciones != null) {
				Iterator<String> dataIterator = operaciones.iterator();
				while (dataIterator.hasNext()) {
					xml_operaciones = xml_operaciones + "<Operacioncanal><operacion><idoperacion>"+dataIterator.next()+"</idoperacion></operacion></Operacioncanal>";
				}
			}
			xml_operaciones = xml_operaciones + "</operacioncanals>";	
			

			
			xml = "<getDarDeAltaCanal><Canal>";
			xml = xml + "<idcanal>"+idcanal+"</idcanal><nombre>"+nombre+"</nombre>";
			xml = xml + "<descripcion>"+descripcion+"</descripcion><cupo/>";
			xml = xml + "<clientes><Cliente><idcliente>"+idcliente+"</idcliente></Cliente></clientes>";
			xml = xml + "<orden>"+orden+"</orden>";
			xml = xml + "<tipocanal><idtipocanal>"+idtipocanal+"</idtipocanal></tipocanal>";
			xml = xml + "<unidadnegocio><idunidadnegocio>"+idunidadnegocio+"</idunidadnegocio></unidadnegocio>";
			xml = xml + xml_tarifas;
			xml = xml + xml_productos;
			xml = xml + xml_descuentos;
			xml = xml + xml_formaspago;
			xml = xml + xml_operaciones;
			xml = xml +"</Canal></getDarDeAltaCanal>";
			
			
			
			if(idcanal.equalsIgnoreCase(""))
				xml_result = Tools.callServiceXML(request, "postDarDeAltaCanal.action?_modo=alta&servicio=postDarDeAltaCanal&xml=" + URLEncoder.encode(xml, "UTF-8"));
			else
				xml_result = Tools.callServiceXML(request, "postDarDeAltaCanal.action?_modo=edicion&servicio=postDarDeAltaCanal&xml=" + URLEncoder.encode(xml, "UTF-8"));
			
			if (xml_result.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
			}	

		
			return new ResponseEntity<String>(HttpStatus.OK);
		}
		

	
	

	// *****************************************************************************************************
	// PROGRAMACIÓN
	
	@RequestMapping("admon/programacion/programacion.do")
	public ModelAndView showProgramacion(HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.programacion.programacion");

		model.addObject("title", messageSource.getMessage("administracion.programacion.title", null, null));

		model.addObject("menu", Tools.getUserMainMenu(request));

		model.addObject("programacion_selector_unidades", Tools.callServiceXML(request, "obtenerComunForCombo.action?servicio=obtenerListadoUnidadesNegocio"));

		return model;
	}
	
	// ***************************************************************************************************
	@RequestMapping(value = "ajax/admon/programacion/list_sesiones.do", method = RequestMethod.POST)
	public ResponseEntity<?> programacionListSesiones(
									   @RequestParam(value = "idrecinto", required = false, defaultValue = "-1") String idRecinto, 
									   @RequestParam(value = "idcontenido", required = false, defaultValue = "") String idContenido, 
									   @RequestParam(value = "fechaIni", required = false, defaultValue = "") String fechaIni,
									   @RequestParam(value = "fechaFin", required = false, defaultValue = "") String fechaFin,
									   @RequestParam(value = "horaDesde", required = false, defaultValue = "") String horaDesde,
									   @RequestParam(value = "horaHasta", required = false, defaultValue = "") String horaHasta,
									   @RequestParam(value = "start", required = false, defaultValue = "0") String startRecord,
						      		   @RequestParam(value = "length", required = false, defaultValue = "") String maxLength,
									   HttpServletRequest request) throws Exception {

       String json="";
       try {
			json=Tools.callServiceJSON(request, "buscarSesiones.action?servicio=buscarSesiones&xml="+URLEncoder.encode("<BuscarSesionesParam><maxResultados>"+maxLength+"</maxResultados><numeroprimerregistro>"+startRecord+"</numeroprimerregistro><idrecinto>"+idRecinto+"</idrecinto><idcontenido>"+idContenido+"</idcontenido><fechainicio>"+fechaIni+"</fechainicio><fechafin>"+fechaFin+"</fechafin><horainicio>"+horaDesde+"</horainicio><horafin>"+horaHasta+"</horafin></BuscarSesionesParam>","UTF-8"));
       } catch (Exception ex) {
           throw new AjaxException(ex.getMessage());
       }		
				
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
	}
	

	// ***************************************************************************************************
	@RequestMapping(value = "/ajax/admon/programacion/list_contenido.do", method = RequestMethod.POST)
	public ResponseEntity<?> recintosListContenido(@RequestParam(value = "idrecinto", required = false, defaultValue = "") String idrecinto, HttpServletRequest request) throws Exception {

	     String json="";
	       try {
	               json = Tools.callServiceJSON(request, "obtenerListadoContenidosPorRecinto.action?servicio=obtenerListadoContenidosPorRecinto&xml=" + URLEncoder.encode("<int>"+idrecinto+"</int>", "UTF-8"));
	        } catch (Exception ex) {
	               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}
	
	// ***************************************************************************************************
	@RequestMapping(value = "/ajax/admon/programacion/list_sesiones_recinto.do", method = RequestMethod.POST)
	public ResponseEntity<?> recintosListContenido(
			   @RequestParam(value = "idrecinto", required = false, defaultValue = "-1") String idRecinto, 
			   @RequestParam(value = "mes", required = false, defaultValue = "") String mes, 
			   @RequestParam(value = "anyo", required = false, defaultValue = "") String anyo,
			   HttpServletRequest request) throws Exception {
	     String json="";
	       try {
	               json = Tools.callServiceJSON(request, "obtenerSesionesPorRecintoYMesYAnyoYContenido.action?servicio=obtenerSesionesPorRecintoYMesYAnyoYContenido&xml=<BuscarSesionesRecintoParam><idrecinto>"+idRecinto+"</idrecinto><idcontenido></idcontenido><mes>"+mes+"</mes><anyo>"+anyo+"</anyo></BuscarSesionesRecintoParam>");
	        } catch (Exception ex) {
	               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	
	// *****************************************************************************************************
	@RequestMapping("ajax/admon/programacion/show_sesion.do")
	public ModelAndView programacionShowSesion(
			@RequestParam(value = "id", required = false, defaultValue = "") String idSesion,
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		if (idSesion.length() > 0) {
			model.addObject("editarsesion_datos_sesion", Tools.callServiceXML(request, "getEdicionAltaSesion.action?servicio=obtenerSesionPorId&xml="+idSesion));
			model.addObject("nueva_sesion","0");
		}
		else {
			model.addObject("editarsesion_datos_sesion", "<empty/>");
			model.addObject("nueva_sesion","1");
		}

		model.setViewName("app.admon.programacion.programacion.dialog.edit_sesion");

		return model;
	}

	
	// *****************************************************************************************************
	@RequestMapping("ajax/admon/programacion/show_sesion_multiple.do")
	public ModelAndView programacionShowSesionMultiple(
			@RequestParam(value = "data", required = true) List<String> data,
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		model.addObject("editarsesionmultiples_datos_sesiones", xmlList);

		model.setViewName("app.admon.programacion.programacion.dialog.edit_sesion_multiple");

		return model;
	}
	
	
	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/select_contenido.do")
	public ModelAndView programacionSelectContenido(
			@RequestParam(value = "id", required = false, defaultValue = "") String idRecinto,
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.programacion.programacion.dialog.select_contenido");
		
		model.addObject("programacion_selector_tiposproducto", Tools.callServiceXML(request, "obtenerListado.action?xml="+idRecinto+"&servicio=obtenerListadoTiposProductoPorRecinto"));

		return model;
	}
	
	// *****************************************************************************************************
	@RequestMapping("ajax/admon/programacion/select_franja.do")
	public ModelAndView programacionSelectFranja(
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.programacion.programacion.dialog.select_franja");

		return model;
	}

	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/list_franjas.do")
	public ResponseEntity<?> programacionListFranjas(
			HttpServletRequest request) throws Exception {

		String json="";
        try {
               json = Tools.callServiceJSON(request, "obtenerListado.action?servicio=obtenerFranjasHorarias");
        } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
        }		
	
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/select_plantillas_presentacion.do")
	public ModelAndView programacionSelectPlantillasPresentacion(
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.programacion.programacion.dialog.select_plantillas_presentacion");

		return model;
	}

	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/list_plantillas_presentacion.do")
	public ResponseEntity<?> programacionListPlantillasPresentacion(
			HttpServletRequest request) throws Exception {

		String json="";
        try {
               json = Tools.callServiceJSON(request, "obtenerListadoPlantillasPresentacion.action?servicio=obtenerListadoPlantillasPresentacion");
        } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
        }		
	
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/select_ver_afluencia.do")
	public ModelAndView programacionSelectVerAfluencia(
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.programacion.programacion.dialog.select_ver_afluencia");

		return model;
	}

	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/list_ver_afluencia.do")
	public ResponseEntity<?> programacionListVerAfluencia(
			@RequestParam(value = "id", required = false, defaultValue = "") String idSesion,
			HttpServletRequest request) throws Exception {

		String json="";
        try {
               json = Tools.callServiceJSON(request, "obtenerAfluenciaPorSesion.action?servicio=obtenerAfluenciaPorSesion&xml="+URLEncoder.encode("<list><int>"+idSesion+"</int></list>","UTF-8"));
               
        } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
        }		
	
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}

	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/save_sesion.do")
	public ResponseEntity<String> programacionSaveSesion(
												@RequestParam(value = "idsesion", required = false, defaultValue = "") String idsesion, 
												@RequestParam(value = "idcontenido_dialogo", required = false, defaultValue = "") String idcontenido, 
												@RequestParam(value = "contenido", required = false, defaultValue = "") String contenido, 
												@RequestParam(value = "idrecinto_dialogo", required = false, defaultValue = "") String idrecinto, 
												@RequestParam(value = "horainicio", required = false, defaultValue = "") String horainicio, 
												@RequestParam(value = "horafin", required = false, defaultValue = "") String horafin, 
												@RequestParam(value = "fecha_sesion", required = false, defaultValue = "") String fecha_sesion, 
												@RequestParam(value = "fecha_creacion_simple", required = false, defaultValue = "") String fecha_creacion_simple, 
												@RequestParam(value = "horas_id", required = false, defaultValue = "") String horas_id, 
												@RequestParam(value = "horas", required = false, defaultValue = "") String horas, 
												@RequestParam(value = "fecha_creacion_ini", required = false, defaultValue = "") String fecha_creacion_ini, 
												@RequestParam(value = "fecha_creacion_fin", required = false, defaultValue = "") String fecha_creacion_fin,
												@RequestParam(value = "dias-semana-select[]", required = false) List<String> dias_semana_select,											
												@RequestParam(value = "overbookingaforo", required = false, defaultValue = "") String overbookingaforo, 
												@RequestParam(value = "overbookingventa", required = false, defaultValue = "") String overbookingventa, 
												@RequestParam(value = "fechainicioventa", required = false, defaultValue = "") String fechainicioventa, 
												@RequestParam(value = "horainicioventa", required = false, defaultValue = "") String horainicioventa, 
												@RequestParam(value = "cantidad-horas-antes", required = false, defaultValue = "") String cantidad_horas_antes, 
												@RequestParam(value = "fechafinventa", required = false, defaultValue = "") String fechafinventa, 
												@RequestParam(value = "horafinventa", required = false, defaultValue = "") String horafinventa, 
												@RequestParam(value = "cantidad-horas-despues", required = false, defaultValue = "") String cantidad_horas_despues, 
												@RequestParam(value = "aperturatornos", required = false, defaultValue = "") String aperturatornos, 
												@RequestParam(value = "cierretornos", required = false, defaultValue = "") String cierretornos, 
												@RequestParam(value = "idinicidencia", required = false, defaultValue = "") String idincidencia, 
 								 				HttpServletRequest request) throws Exception {

	
		String xml = "";
		String periodo= "false";
		String fechainicioventa_calculada="";
		
		xml= (idsesion.length()>0)?"<getEdicionAltaSesionPopUp><Altasesionparam>":"<getEdicionAltaSesion><Altasesionparam>";
		if (idsesion.length()>0) {
			xml+="<fecha>"+fecha_sesion+"</fecha>";
			fechainicioventa_calculada= fecha_sesion;
		}
		else {
			if (fecha_creacion_simple.length()>0) {
				xml+="<listaFechas>";
				StringTokenizer tokens= new StringTokenizer(fecha_creacion_simple,",");
				String fecha_actual="";
				while(tokens.hasMoreTokens()) {
					fecha_actual= tokens.nextToken();
					xml+="<date>"+fecha_actual+"</date>";
					if (fechainicioventa_calculada=="") fechainicioventa_calculada= fecha_actual;
				}
				xml+="</listaFechas>";
			}
			else {
				periodo= "true";
				xml+="<panelfechaInicio>"+fecha_creacion_ini+"</panelfechaInicio>";
				xml+="<panelfechaFin>"+fecha_creacion_fin+"</panelfechaFin>";
				fechainicioventa_calculada= fecha_creacion_ini;
				if (dias_semana_select != null) {
					String dias_semana_xml[]= {"lunes","martes","miercoles","jueves","viernes","sabado","domingo"};
					String dias_semana[]= {"Lunes","Martes","Miércoles","Jueves","Viernes","Sábado","Domingo"};
					for (int i=0; i<7; i++) {
						xml+= "<"+dias_semana_xml[i]+">"+(dias_semana_select.contains(dias_semana[i])?"true":"false")+"</"+dias_semana_xml[i]+">";
					}
				}
				else xml+="<diario>true</diario>";
			}
		}
		
		if (cantidad_horas_antes.length()>0) {
			fechainicioventa= fechainicioventa_calculada;
			horainicioventa= Tools.cambiarHoras(horainicio, cantidad_horas_antes, false);
		}
		if (cantidad_horas_despues.length()>0) {
			fechafinventa= fechainicioventa_calculada;
			horafinventa= Tools.cambiarHoras(horainicio, cantidad_horas_despues, true);
		}
		
		xml+="<sesionPlantilla>";
		if (idsesion.length()>0) xml+="<idsesion>"+idsesion+"</idsesion>";
		xml+="<plantillafranjas><idplantillafranjas>"+horas_id+"</idplantillafranjas></plantillafranjas>";
		xml+="<horainicio>"+horainicio+"</horainicio>";
		xml+="<horafin>"+horafin+"</horafin>";
		xml+="<contenido>";
		xml+="<idcontenido>"+idcontenido+"</idcontenido>";
		xml+="<tipoproducto><recinto><idrecinto>"+idrecinto+"</idrecinto></recinto></tipoproducto>";
		xml+="</contenido>";
		xml+="<IncidenciaICAA><codigo>"+idincidencia+"</codigo></IncidenciaICAA>";
		xml+="<overbookingaforo>"+(overbookingaforo.length()>0?overbookingaforo:"0")+"</overbookingaforo>";
		xml+="<overbookingventa>"+(overbookingventa.length()>0?overbookingventa:"0")+"</overbookingventa>";
		xml+="<aperturatornos>"+aperturatornos+"</aperturatornos>";
		xml+="<cierretornos>"+cierretornos+"</cierretornos>";
		xml+="<fechafinventa>"+fechafinventa+"</fechafinventa>";
		xml+="<horafinventa>"+horafinventa+"</horafinventa>";
		xml+="<fechainicioventa>"+fechainicioventa+"</fechainicioventa>";
		xml+="<horainicioventa>"+horainicioventa+"</horainicioventa>";
		xml+="</sesionPlantilla>";
		xml+=(idsesion.length()>0)?"</Altasesionparam></getEdicionAltaSesionPopUp>":"</Altasesionparam></getEdicionAltaSesion>";
		
		String xml_result= Tools.callServiceXML(request, "postEdicionAltaSesion.action?servicio=postEdicionAltaSesion&periodo="+periodo+"&xml=" + URLEncoder.encode(xml, "UTF-8"));

		if (xml_result.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}		


	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/save_sesion_multiple.do")
	public ResponseEntity<String> programacionSaveSesionMultiple(
												@RequestParam(value = "idsesiones", required = false, defaultValue = "") String idsesiones, 
												@RequestParam(value = "idcontenido_dialogo", required = false, defaultValue = "") String idcontenido, 
												@RequestParam(value = "horainicio", required = false, defaultValue = "") String horainicio, 
												@RequestParam(value = "horafin", required = false, defaultValue = "") String horafin, 
												@RequestParam(value = "fecha_sesion", required = false, defaultValue = "") String fechasesion, 
 								 				HttpServletRequest request) throws Exception {

	
		String xml = "";
		xml+="<Actualizarserieparam>";
		xml+="<fecha>"+fechasesion+"</fecha>";
		xml+="<horainicio>"+horainicio+"</horainicio>";
		xml+="<horafin>"+horafin+"</horafin>";
		xml+="<codigo/>";
		xml+="<idContenido>"+idcontenido+"</idContenido>";
		xml+="<listaIdSesiones>"+idsesiones+"</listaIdSesiones>";
		xml+="</Actualizarserieparam>";
		
		String xml_result= Tools.callServiceXML(request, "actualizarSerieSesiones.action?servicio=actualizarSerieSesiones&xml=" + URLEncoder.encode(xml, "UTF-8"));

		if (xml_result.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}		
	
	
	
	// ***************************************************************************************************
	@RequestMapping(value = "/ajax/admon/programacion/remove_sesions.do", method = RequestMethod.POST)
	public ResponseEntity<String> programacionRemoveSesions(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {

		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request, "darDeBajaComun.action?servicio=darDeBajaSesiones&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	// ***************************************************************************************************
	@RequestMapping(value = "/ajax/admon/programacion/lock_sesions.do", method = RequestMethod.POST)
	public ResponseEntity<String> programacionLockSesions(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {

		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<idsesion>" + dataIterator.next() + "</idsesion>";
		}
		
		String xml = Tools.callServiceXML(request, "bloquearSesiones.action?servicio=bloquearSesiones&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}

	// ***************************************************************************************************
	@RequestMapping(value = "/ajax/admon/programacion/unlock_sesions.do", method = RequestMethod.POST)
	public ResponseEntity<String> programacionUnlockSesions(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {

		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}
		
		String xml = Tools.callServiceXML(request, "desbloquearSesiones.action?servicio=desbloquearSesiones&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	
	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/show_establecer_cupos.do")
	public ModelAndView programacionEstablecerCupos(
			@RequestParam(value = "id", required = false, defaultValue = "") String idSesion,
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.programacion.programacion.dialog.establecer_cupos");
		
		model.addObject("idSesion",idSesion);
		model.addObject("establecercupos_datos_cupo", Tools.callServiceXML(request, "obtenerCupoCanalPorSesion.action?servicio=obtenerCupoCanalPorSesion&idSesion="+idSesion));

		return model;
	}
	

	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/show_edit_cupo.do")
	public ModelAndView programacionEditCupo(
			@RequestParam(value = "id", required = false, defaultValue = "") String idCanal,
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.programacion.programacion.dialog.edit_cupo");

		model.addObject("idCanal",idCanal);
		model.addObject("editarcupo_datos_cupo", Tools.callServiceXML(request, "obtenerListadoCanalesFiltrado.action?servicio=obtenerListadoCanalesFiltrado&selected=-1&filter=<envio><parametro>4</parametro></envio>"));

		return model;
	}
	
	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/save_cupos.do")
	public ResponseEntity<String> programacionSaveCupos( 
			 @RequestParam(value = "idsesion", required = false, defaultValue = "") String idSesion,
			 @RequestParam(value = "cupos_asignados", required = false, defaultValue = "") String cupos_asignados,
			 HttpServletRequest request) throws Exception {

		String xml="";
		
		if (cupos_asignados.length()>0)
			xml= Tools.callServiceXML(request, "actualizarCupoCanalSesion.action?servicio=actualizarCupoCanalSesion&idSesion="+idSesion+"&xml=" + URLEncoder.encode(cupos_asignados,"UTF-8"));

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}	
	
	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/show_franjas.do")
	public ModelAndView programacionShowFranjas(
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.programacion.programacion.dialog.listar_franjas");
		
		return model;
	}

	// ***************************************************************************************************
	@RequestMapping(value = "/ajax/admon/programacion/remove_franjas.do", method = RequestMethod.POST)
	public ResponseEntity<String> programacionRemoveFranjas(@RequestParam(value = "data", required = true) List<String> data, HttpServletRequest request) throws Exception {

		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request, "darDeBajaComun.action?servicio=darDeBajaFranjasHoraria&xml=<list>" + xmlList + "</list>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/show_new_franja.do")
	public ModelAndView programacionShowNewFranja(
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.programacion.programacion.dialog.crear_franja");
		
		return model;
	}
	
	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/save_franja.do")
	public ResponseEntity<String> programacionSaveFranja( 
			 @RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
			 @RequestParam(value = "horainicio", required = false, defaultValue = "") String horainicio,
			 @RequestParam(value = "horafin", required = false, defaultValue = "") String horafin,
			 HttpServletRequest request) throws Exception {

		String xml= Tools.callServiceXML(request, "postEdicionAltaFranjaHoraria.action?servicio=darAltaFranjaHoraria&xml=" + URLEncoder.encode("<getEdicionAltaFranjaHoraria><Franjahoraria><nombre>"+nombre+"</nombre><horainicio>"+horainicio+"</horainicio><horafin>"+horafin+"</horafin></Franjahoraria></getEdicionAltaFranjaHoraria>","UTF-8"));

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}	
	
	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/show_bloqueos.do")
	public ModelAndView programacionShowBloqueos(
			@RequestParam(value = "id", required = false, defaultValue = "") String idSesion,
			@RequestParam(value = "button", required = false, defaultValue = "") String buttonBloqueo,
			@RequestParam(value = "lista", required = false, defaultValue = "lista") String listaBloqueos,
			@RequestParam(value = "es_programacion", required = false, defaultValue = "true") String esProgramacion,
			@RequestParam(value = "idx_libres", required = false, defaultValue = "0") String idxLibres,
			@RequestParam(value = "idx_bloqueadas", required = false, defaultValue = "0") String idxBloqueadas,
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.programacion.programacion.dialog.listar_bloqueos");

		model.addObject("idSesion",idSesion);
		model.addObject("buttonBloqueo",buttonBloqueo);
		model.addObject("listaBloqueos",listaBloqueos);
		model.addObject("esProgramacion",esProgramacion);
		model.addObject("idxLibres",idxLibres);
		model.addObject("idxBloqueadas",idxBloqueadas);
		
		return model;
	}

	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/show_bloqueos_multiple.do")
	public ModelAndView programacionShowBloqueos(
			@RequestParam(value = "data", required = true) List<String> data,
			@RequestParam(value = "lista", required = false, defaultValue = "lista") String listaBloqueos,
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.programacion.programacion.dialog.listar_bloqueos_multiple");

		String xmlList = "";
		String idSesion = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			String s=dataIterator.next();
			xmlList += "<int>" + s + "</int>";
			if (idSesion.length()<=0) idSesion=s;
		}
		
		model.addObject("listaBloqueos",listaBloqueos);
		model.addObject("idSesionMultiple",xmlList);
		model.addObject("idSesion",idSesion);
		model.addObject("esProgramacion","true");
		
		return model;
	}
	
	
	
	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/list_bloqueos.do")
	public ResponseEntity<?> programacionListBloqueos(
			@RequestParam(value = "id", required = false, defaultValue = "") String idSesion,
			HttpServletRequest request) throws Exception {

		String json="";
        try {
           json = Tools.callServiceJSON(request, "getEdicionAltaSesion.action?servicio=obtenerSesionPorId&xml="+idSesion);
        } catch (Exception ex) {
           throw new AjaxException(ex.getMessage());
        }		
	
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}
	
	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/show_new_bloqueo.do")
	public ModelAndView programacionShowNewBloqueo(
			@RequestParam(value = "id", required = false, defaultValue = "") String idSesion,
			@RequestParam(value = "idmultiple", required = false, defaultValue = "") String idSesionMultiple,
			@RequestParam(value = "button", required = false, defaultValue = "") String buttonBloqueo,
			@RequestParam(value = "lista", required = false, defaultValue = "") String listaBloqueos,
			@RequestParam(value = "zona", required = false, defaultValue = "") String idZona,
			@RequestParam(value = "es_programacion", required = false, defaultValue = "true") String esProgramacion,
			@RequestParam(value = "idx_libres", required = false, defaultValue = "0") String idxLibres,
			@RequestParam(value = "idx_bloqueadas", required = false, defaultValue = "0") String idxBloqueadas,
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();		

		if (idSesionMultiple.length()>0) 
			model.addObject("esMultiple","true");
		else
			model.addObject("esMultiple","false");
		
		model.setViewName("app.admon.programacion.programacion.dialog.crear_bloqueo");
		model.addObject("idSesion",idSesion);
		model.addObject("idSesionMultiple",idSesionMultiple);
		model.addObject("buttonBloqueo",buttonBloqueo);
		model.addObject("listaBloqueos",listaBloqueos);
		model.addObject("idZona",idZona);
		model.addObject("esProgramacion",esProgramacion);
		model.addObject("idxLibres",idxLibres);
		model.addObject("idxBloqueadas",idxBloqueadas);
		model.addObject("crearbloqueo_selector_motivos", Tools.callServiceXML(request, "obtenerListadoMotivosBloqueo.action?servicio=obtenerListadoMotivosBloqueo"));
		model.addObject("crearbloqueo_selector_zonas", Tools.callServiceXML(request, "obtenerListadoZonasDeRecintoPorSesion.action?servicio=obtenerListadoZonasDeRecintoPorSesion&xml=<int>"+idSesion+"</int>"));
		
		return model;
	}

	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/show_remove_bloqueos.do")
	public ModelAndView programacionShowRemoveBloqueos(
			@RequestParam(value = "id", required = false, defaultValue = "") String idEstadoLocalidad,
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.programacion.programacion.dialog.remove_bloqueos");
		model.addObject("idEstadoLocalidad",idEstadoLocalidad);
		
		return model;
	}

	// ***************************************************************************************************
	@RequestMapping(value = "/ajax/admon/programacion/remove_bloqueos.do", method = RequestMethod.POST)
	public ResponseEntity<String> programacionRemoveBloqueos(
			@RequestParam(value = "idestadolocalidad", required = false, defaultValue = "") String idEstadoLocalidad,
			@RequestParam(value = "cantidad-borrado", required = false, defaultValue = "") String cantidad,
			HttpServletRequest request) throws Exception {
		
		String xmlCantidad="";
		if (cantidad.length()>0) xmlCantidad= "<cantidadlocalidades>"+cantidad+"</cantidadlocalidades><prerresrvar>0</prerresrvar>";

		String xml = Tools.callServiceXML(request, "desbloquearLocalidadesZonaNoNumerada.action?servicio=desbloquearLocalidadesZonaNoNumerada&xml="+URLEncoder.encode("<list><DesbloquearLocalidadesZonaNoNumeradaParam><idestadolocalidad>"+idEstadoLocalidad+"</idestadolocalidad>"+xmlCantidad+"</DesbloquearLocalidadesZonaNoNumeradaParam></list>","UTF-8"));

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}

	
	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/save_bloqueo.do")
	public ResponseEntity<String> programacionSaveBloqueo( 
			 @RequestParam(value = "idsesion", required = false, defaultValue = "") String idsesion,
			 @RequestParam(value = "idsesionmultiple", required = false, defaultValue = "") String idsesionmultiple,
			 @RequestParam(value = "numbloqueadas", required = false, defaultValue = "") String numbloqueadas,
			 @RequestParam(value = "motivo", required = false, defaultValue = "") String motivo,
			 @RequestParam(value = "motivobloqueo", required = false, defaultValue = "") String motivobloqueo,
			 @RequestParam(value = "idusuario", required = false, defaultValue = "") String idusuario,
			 @RequestParam(value = "usuario", required = false, defaultValue = "") String usuario,
			 @RequestParam(value = "zona", required = false, defaultValue = "") String zona,
			 @RequestParam(value = "observaciones", required = false, defaultValue = "") String observaciones,
			 @RequestParam(value = "activada", required = false, defaultValue = "") String activada,
			 HttpServletRequest request) throws Exception {

		activada= (activada.length()>0) ? "true":"false"; 

		String xml=""; 

		if (idsesionmultiple.length()>0)
			xml= Tools.callServiceXML(request, "bloquearLocalidadesZonaNoNumerada.action?servicio=bloquearLocalidadesZonaNoNumerada&estado="+URLEncoder.encode("<Estadolocalidad><motivobloqueo>"+motivobloqueo+"</motivobloqueo><desbloqueasoloresponsable>"+activada+"</desbloqueasoloresponsable><observaciones>"+observaciones+"</observaciones><nrolocalidadesbloqueadas>"+numbloqueadas+"</nrolocalidadesbloqueadas><zona><idzona>"+zona+"</idzona></zona><zonas/><estado><idestado>4</idestado></estado><usuario><idusuario>"+idusuario+"</idusuario><login>"+usuario+"</login></usuario></Estadolocalidad>","UTF-8")+"&xml=" + URLEncoder.encode("<list>"+idsesionmultiple+"</list>","UTF-8"));
		else
			xml= Tools.callServiceXML(request, "bloquearLocalidadesZonaNoNumerada.action?servicio=bloquearLocalidadesZonaNoNumerada&estado="+URLEncoder.encode("<Estadolocalidad><motivobloqueo>"+motivobloqueo+"</motivobloqueo><desbloqueasoloresponsable>"+activada+"</desbloqueasoloresponsable><observaciones>"+observaciones+"</observaciones><nrolocalidadesbloqueadas>"+numbloqueadas+"</nrolocalidadesbloqueadas><zona><idzona>"+zona+"</idzona></zona><zonas/><estado><idestado>4</idestado></estado><usuario><idusuario>"+idusuario+"</idusuario><login>"+usuario+"</login></usuario></Estadolocalidad>","UTF-8")+"&xml=" + URLEncoder.encode("<list><int>"+idsesion+"</int></list>","UTF-8"));
		
		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}	
	
	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/show_info_bloqueo.do")
	public ModelAndView programacionShowInfoBloqueo(
			@RequestParam(value = "id", required = false, defaultValue = "") String idSesion,
			HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();

		model.setViewName("app.admon.programacion.programacion.dialog.listar_aforo");
		model.addObject("idSesion",idSesion);
		
		return model;
	}

	// *****************************************************************************************************
	@RequestMapping("/ajax/admon/programacion/list_aforo.do")
	public ResponseEntity<?> programacionListAforo(
			@RequestParam(value = "id", required = false, defaultValue = "") String idSesion,
			HttpServletRequest request) throws Exception {

		String json="";
        try {
               json = Tools.callServiceJSON(request, "getEdicionAltaSesion.action?servicio=obtenerSesionPorId&xml="+idSesion);
        } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
        }		
	
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}
	//************************************************************************************************************
	
	@RequestMapping("/ajax/admon/productos/productos/asignar_tarifas.do")	
	public ModelAndView asignarTarifas(
			@RequestParam(value = "idbono", required = false, defaultValue = "") String idBono,
			@RequestParam(value = "idproducto", required = false, defaultValue = "") String idProducto,
			@RequestParam(value = "cabecera", required = false, defaultValue = "") String cabecera,
			HttpServletRequest request) throws Exception {
		
		ModelAndView model = new ModelAndView();
		String datos="";
		datos = "idbono="+idBono+"&idproducto="+idProducto;
		String arrayCabecera[] = cabecera.split("-");
		String xmlCabecera;
		
		xmlCabecera="<xmlCabecera>";
		for (int i=0; i<arrayCabecera.length; i++)
			xmlCabecera+="<xmlNodoCabecera>"+arrayCabecera[i]+"</xmlNodoCabecera>";
		xmlCabecera+="</xmlCabecera>";

		model.addObject("xmlcabecera", xmlCabecera);		
		model.addObject("tarifas_disponibles", Tools.callServiceXML(request,"obtenerImportesProporcionalTarifaProductoCombinado.action?servicio=obtenerImportesProporcionalTarifaProductoCombinado&" + datos));
		model.addObject("tarifas_disponibles_json",Tools.callServiceJSON(request,  "obtenerImportesProporcionalTarifaProductoCombinado.action?servicio=obtenerImportesProporcionalTarifaProductoCombinado&" + datos));
		model.setViewName("app.admon.productos.productos.dialog.asignar_tarifas");
		
		return model;
	}
	
	
	// ***************************************************************************************************
		@RequestMapping(value = "/ajax/admon/programacion/programacion/list_recintos.do", method = RequestMethod.POST)
		public ResponseEntity<?> programacionListRecintos(@RequestParam(value = "idunidadnegocio", required = false, defaultValue = "-1") String idunidadnegocio, HttpServletRequest request) throws Exception {

		     String json="";
		       try {
		               json = Tools.callServiceJSON(request, "obtenerListadoRecintosPorUnidadNegocio.action?servicio=obtenerListadoRecintosPorUnidadNegocio&xml=<int>" + idunidadnegocio+"</int>");
		        } catch (Exception ex) {
		               throw new AjaxException(ex.getMessage());
		        }		
			
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}

	
	
	
	
	//********************************************GUARDAR IMPORTES PRODUCTOS**************************************				
			@RequestMapping("/ajax/admon/productos/productos/save_importes.do")
			public ResponseEntity<String> productosSaveImportes(
					@RequestParam(value = "xml_tarifas_actualizadas", required = false, defaultValue = "") String xml, 
					HttpServletRequest request) throws Exception {

				
				System.out.println("data:");
				System.out.println(xml);
				
				String xml_result="";
				xml_result = Tools.callServiceXML(request, "actualizarImportesProporcionalTarifaProductoCombinado.action?servicio=actualizarImportesProporcionalTarifaProductoCombinado&xml=" + URLEncoder.encode(xml, "UTF-8"));				
			
				if (xml_result.startsWith("<error>")) {
					return new ResponseEntity<String>(Tools.extractXMLError(xml_result), HttpStatus.BAD_REQUEST);
				}

				return new ResponseEntity<String>(HttpStatus.OK);
			}
			// ***********************************ELIMINAR TIPOS DE IVA***********************************
}

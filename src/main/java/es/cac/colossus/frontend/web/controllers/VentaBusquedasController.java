package es.cac.colossus.frontend.web.controllers;

import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import es.cac.colossus.frontend.utils.AjaxException;
import es.cac.colossus.frontend.utils.Tools;

@Controller
public class VentaBusquedasController {

	@Autowired
	private MessageSource messageSource;
	
	private static final Log log = LogFactory.getLog(Tools.class);
	
	@RequestMapping("venta/ventareserva/busquedas.do")
	public ModelAndView ventaReservaBusquedas(HttpServletRequest request) throws Exception {
		ModelAndView model = new ModelAndView();
		String idUsuario=request.getSession().getAttribute("idUsuario").toString();
		model.addObject("title", messageSource.getMessage("ventareserva.busquedas.title", null, null));
		model.addObject("menu", Tools.getUserMainMenu(request));
		model.addObject("tiposAbono", Tools.callServiceXML(request, "obtenerListadoTiposAbono.action?servicio=obtenerListadoTiposAbono"));
		model.addObject("tiposBono", Tools.callServiceXML(request, "obtenerListadoTiposBono.action?servicio=obtenerListadoTiposBono"));
		model.addObject("tarifas", Tools.callServiceXML(request, "obtenerListadoTarifas.action?servicio=obtenerListadoTarifas"));
		model.addObject("productos", Tools.callServiceXML(request, "obtenerListadoProductos.action?servicio=obtenerListadoProductos"));
		//GGL NEW Para que en la busqueda de abonos aparezcan solo los prod disponibles para abono
		model.addObject("ventareserva_productos_disponibles", Tools.callServiceXML(request, "obtenerListadoProductosDisponiblesParaAbonoNoNumerados.action?servicio=obtenerListadoProductosDisponiblesParaAbonoNoNumerados"));
		model.addObject("ventareserva_canales_indirectos", Tools.callServiceXML(request, "obtenerListadoCanalesIndirectos.action?servicio=obtenerListadoCanalesIndirectos"));
		model.addObject("ventareserva_productos_canal", Tools.callServiceXML(request, "obtenerListadoProductosPorCanal.action?servicio=obtenerListadoProductosPorCanal&xml=<c><idcanal>2</idcanal></c>"));	
		model.addObject("ventareserva_tipos_bono", Tools.callServiceXML(request, "obtenerListadoTiposBono.action?servicio=obtenerListadoTiposBono"));
		model.addObject("ventareserva_tarifas_bonos", Tools.callServiceXML(request, "obtenerListadoTarifasBono.action?servicio=obtenerListadoTarifasBono"));
		model.addObject("ventareserva_motivos_bonos", Tools.callServiceXML(request, "obtenerListadoMotivosBono.action?servicio=obtenerListadoMotivosBono"));
		String url_impresion= Tools.callServiceXMLSinReemplazo(request, "abrirInformeUsuario.action?servicio=abrirInformeUsuario&xml=<java.lang.Integer>"+idUsuario+"</java.lang.Integer>"); 
		url_impresion = url_impresion.replace("<String>", "").replace("</String>", "");
		
		URL aURL = new URL(url_impresion);		
		model.addObject("url_impresion",aURL.getAuthority());
		model.addObject("url_validacion",url_impresion);
		
		model.setViewName("app.ventareserva.ventareserva.busquedas");
		return model;
	}
	
	//****************************LISTAR Venta abonos***************************************************************
	@RequestMapping(value = "/ajax/venta/ventareserva/venta_abono/list_venta_abono.do", method = RequestMethod.POST)
	public ResponseEntity<?> ventaReservaBusquedaVentaAbono(
			@RequestParam(value = "refventa", required = false, defaultValue = "") String refventa,
			@RequestParam(value = "refabono", required = false, defaultValue = "") String refabono, 
			@RequestParam(value = "fechaoperacioninicial", required = false, defaultValue = "") String fechaoperacioninicial,
			@RequestParam(value = "fechaoperacionfinal", required = false, defaultValue = "") String fechaoperacionfinal,
			@RequestParam(value = "idproducto", required = false, defaultValue = "") String idproducto,
			@RequestParam(value = "idtipoabono", required = false, defaultValue = "") String idtipoabono, 
		    @RequestParam(value = "start", required = false, defaultValue = "0") String startRecord,
      		@RequestParam(value = "length", required = false, defaultValue = "") String maxLength,
      		HttpServletRequest request) throws Exception	
	{		
		
		String xml="<Buscarventaabonosparam>";
		xml+="<refventa>"+refventa+"</refventa>";
		xml+="<refabono>"+refabono+"</refabono>";
		xml+="<fechaoperacioninicial>"+fechaoperacioninicial+"</fechaoperacioninicial>";
		xml+="<fechaoperacionfinal>"+fechaoperacionfinal+"</fechaoperacionfinal>";
		xml+="<idproducto>"+idproducto+"</idproducto>";
		xml+="<idtipoabono>"+idtipoabono+"</idtipoabono>";
		xml+="<numeroprimerregistro>"+startRecord+"</numeroprimerregistro>";
		xml+="</Buscarventaabonosparam>";
		
		String json = "";

		try {
			json = Tools.callServiceJSON(request, "buscarVentaAbonos.action?servicio=buscarVentaAbonos&xml="+ URLEncoder.encode(xml, "UTF-8") );
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);

	}
	
	
	// ***********************************************************************************
	@RequestMapping(value = "ajax/ventareserva/busqueda/bono/imprimir.do", method = RequestMethod.POST)
	public ResponseEntity<?> bonoimprimir(
			@RequestParam(value = "xmlBonos", required = false, defaultValue = "") String xml,
			HttpServletRequest request) throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();
		
		String respuesta="";
		try {
			respuesta = Tools.callServiceXML(request, "imprimirBonos.action?servicio=imprimirBonos&xml="+xml);
									
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}		
		
		if (respuesta.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
	}
	
	
	// ***********************************************************************************
	@RequestMapping(value = "ajax/ventareserva/busqueda/bono/reimprimir.do", method = RequestMethod.POST)
	public ResponseEntity<?> bonoReimprimir(
			@RequestParam(value = "xmlBonos", required = false, defaultValue = "") String xml,
			HttpServletRequest request) throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();
		
		String respuesta="";
		try {
			respuesta = Tools.callServiceXML(request, "reimprimirBonos.action?servicio=reimprimirBonos&xml="+xml);
								
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}		
		
		if (respuesta.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
	}	
	
	
	// ***********************************************************************************
	@RequestMapping(value = "ajax/ventareserva/busqueda/bono/activar.do", method = RequestMethod.POST)
	public ResponseEntity<?> bonoActivar(
			@RequestParam(value = "xmlBonos", required = false, defaultValue = "") String xml,
			HttpServletRequest request) throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();
		String respuesta="";
		try {
			respuesta = Tools.callServiceXML(request, "activarBonos.action?servicio=activarBonos&xml="+xml);
								
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}		
		
		if (respuesta.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
	}	
	
	// ***********************************************************************************
	@RequestMapping(value = "ajax/ventareserva/busqueda/bono/anular.do", method = RequestMethod.POST)
	public ResponseEntity<?> bonoAnular(
			@RequestParam(value = "xmlBonos", required = false, defaultValue = "") String xml,
			HttpServletRequest request) throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();
		
		String respuesta="";
		try {
			respuesta = Tools.callServiceXML(request, "anularBonos.action?servicio=anularBonos&xml="+xml);
									
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}		
		
		if (respuesta.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
	}	
	
	//****************************LISTAR Venta abonos***************************************************************
	@RequestMapping(value = "ajax/ventareserva/busqueda/bono/editar.do")
	public ModelAndView bonoEditar(
			@RequestParam(value = "id", required = false, defaultValue = "") String idBono,
			HttpServletRequest request) throws Exception	
	{		
		ModelAndView model = new ModelAndView();
		
		String xml ="<int>"+idBono+"</int>";
		String xmlResultado = Tools.callServiceXML(request, "obtenerBonoPorId.action?servicio=obtenerBonoPorId&xml="+xml);
		JSONObject json = XML.toJSONObject(xmlResultado);
		model.addObject("editarBono",xmlResultado );
		model.addObject("editarBonoJson",json );
		model.setViewName("app.ventareserva.ventareserva.busquedas.bono.editar");
		return model;
	}
	//***********************************************************************
	@RequestMapping("/ajax/ventareserva/busqueda/bono/modificarLineas.do")
	public ModelAndView bonoModificarLineas(
			@RequestParam(value = "data", required = true) String data,
			@RequestParam(value = "idventabono", required = false, defaultValue = "") String idventabono,
			@RequestParam(value = "idcanal", required = false, defaultValue = "") String idcanal,
			@RequestParam(value = "idtipoventa", required = false, defaultValue = "") String idtipoventa,
			@RequestParam(value = "idcliente", required = false, defaultValue = "") String idcliente,
			HttpServletRequest request) throws Exception {
		ModelAndView model = new ModelAndView();
		
		model.addObject("ventareserva_productos_principales", Tools.callServiceXML(request, "obtenerListadoProductosPrincipales.action?servicio=obtenerListadoProductosPrincipales"));
		model.addObject("ventareserva_canales_indirectos", Tools.callServiceXML(request, "obtenerListadoCanalesIndirectos.action?servicio=obtenerListadoCanalesIndirectos"));
		
	
	String xmlList = "";
	String[] perfiles;
	String[] partes;
	String idLineaDetalle="";
	String idProducto="";	
	perfiles=data.split("\\|");
	
	xmlList="<listaDto>";
	for (int i=0; i<perfiles.length;i++) {
		partes= perfiles[i].split("-");
		idLineaDetalle=partes[0];
		idProducto=partes[1];
		xmlList += "<ObtenerPerfilesy1Tarifaparam>";
		xmlList += "<idcliente>"+idcliente+"</idcliente>";
		xmlList += "<idtipoventa>"+idtipoventa+"</idtipoventa>";
		xmlList += "<idcanal>"+idcanal+"</idcanal>";
		xmlList += "<idproducto>"+idProducto+"</idproducto>";
		xmlList += "<idlineadetalle>"+idLineaDetalle+"</idlineadetalle>";				
		xmlList+="</ObtenerPerfilesy1Tarifaparam>";
	}
	xmlList+="</listaDto>";
	

	model.addObject("ventareserva_perfiles_tarifas", Tools.callServiceXML(request, "obtenerPerfilesAplicablesConUnaTarifaProducto.action?servicio=obtenerPerfilesAplicablesConUnaTarifaProducto&xml="+ URLEncoder.encode(xmlList, "UTF-8")));
	
	String xml_ventareserva_venta_bono= Tools.callServiceXML(request, "obtenerVentaPorIdPopupBonoConPerfiles.action?servicio=obtenerVentaPorId&xml=<int>"+idventabono+"</int>");
	
	JSONObject json_venta_lineas_bono = XML.toJSONObject(xml_ventareserva_venta_bono);
	if (json_venta_lineas_bono.has("error")) 
		throw new Exception(json_venta_lineas_bono.getString("error"));
	model.addObject("json_venta_lineas_bono",json_venta_lineas_bono.toString(4));
	
	model.addObject("ventareserva_venta_bono",xml_ventareserva_venta_bono);
		
	model.setViewName("app.ventareserva.ventareserva.busquedas.bono.editarLineas");
	return model;
	}
	//***********************************************************************
	@RequestMapping("/ajax/ventareserva/busqueda/bono/editarUso.do")
	public ModelAndView bonoEditarUso(
			@RequestParam(value = "uso", required = false, defaultValue = "") String uso,
			HttpServletRequest request) throws Exception {
		ModelAndView model = new ModelAndView();
		
		model.addObject("uso", uso);
		
						
		model.setViewName("app.ventareserva.ventareserva.busquedas.bono.editarUso");
		return model;
	}
	
	// ***************************************************************************************************
		@RequestMapping("/ajax/ventareserva/busqueda/bono/asignarSesion.do")
		public ResponseEntity<?> ventareservaBonoasignarSesion(
				@RequestParam(value = "xmlCanje", required = false, defaultValue = "") String xmlCanje, 
				HttpServletRequest request) throws Exception {

		     String json="";
		       try {
	               		json = Tools.callServiceJSON(request, "asignarSesiones.action?servicio=asignarSesiones&xml="+URLEncoder.encode(xmlCanje,"UTF-8"));
	               		
		       	   } catch (Exception ex) {
	               throw new AjaxException(ex.getMessage());
		        }		
			
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}
		
		// ***************************************************************************************************
		@RequestMapping("/ajax/ventareserva/busqueda/bono/cancelarCanjeBono.do")
		public ResponseEntity<?> ventareservaBonocancelarCanjeBono(
				@RequestParam(value = "idCanje", required = false, defaultValue = "") String idCanje, 
				HttpServletRequest request) throws Exception {
	
			String json="";
		       try {
	               		json = Tools.callServiceJSON(request, "cancelarCanjeBono.action?servicio=cancelarCanjeBono&xml="+URLEncoder.encode("<int>"+idCanje+"</int>","UTF-8"));
	               		
		       	   } catch (Exception ex) {
	               throw new AjaxException(ex.getMessage());
		        }		
			
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
	
			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}
	
		
		@RequestMapping("/ajax/ventareserva/busqueda/bono/actualizarBono.do")
		public ResponseEntity<?> actualizarBono(
				@RequestParam(value = "xmlGuardarBono", required = false, defaultValue = "") String xml, 
				HttpServletRequest request) throws Exception {
	
			   String json="";
		       try {
	               	json = Tools.callServiceJSON(request, "actualizarBonos.action?servicio=actualizarBonos&xml="+URLEncoder.encode(xml,"UTF-8"));
	               		
		       	   } catch (Exception ex) {
	               throw new AjaxException(ex.getMessage());
		        }		
			
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
	
			return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
		}
		
		
	// ***************************************************************************************************
	@RequestMapping("/ajax/ventareserva/busqueda/bono/generar_factura.do")
	public ResponseEntity<?> ventareservaBonoGenerarFactura(
			@RequestParam(value = "idventa", required = false, defaultValue = "") String idVenta, 
			HttpServletRequest request) throws Exception {

	     String json="";
	       try {
               		json = Tools.callServiceJSON(request, "generarFactura.action?servicio=generarFactura&xml="+URLEncoder.encode("<Generarfacturaventaparam><listaIdsVenta><int>"+idVenta+"</int></listaIdsVenta></Generarfacturaventaparam>","UTF-8"));
               		
	       	   } catch (Exception ex) {
               throw new AjaxException(ex.getMessage());
	        }		
		
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
	}
	
	// ***************************************************************************************************
	@RequestMapping("ajax/venta/ventareserva/busqueda/show_edit_emision_bono.do")
	public ModelAndView showEditEmisionBono(
			HttpServletRequest request) throws Exception {
	
		ModelAndView model = new ModelAndView();

		model.setViewName("app.venta.ventareserva.busqueda.edit_emision_bono");
		
		model.addObject("ventareserva_listado_tiposbono", Tools.callServiceXML(request, "obtenerListadoTiposBono.action"));
		
		return model;
	}
	
	
	
	
	
	
	@RequestMapping("ajax/ventareserva/busqueda/bono/show_disponibles.do")
	public ModelAndView showDisponibles(
			@RequestParam(value = "idtipoproducto", required = false, defaultValue = "4") String idtipoproducto,
			@RequestParam(value = "producto", required = false, defaultValue = "") String producto,
			@RequestParam(value = "fecha", required = false, defaultValue = "") String fecha,
			@RequestParam(value = "button", required = false, defaultValue = "") String buttonAdd,
			HttpServletRequest request) throws Exception {
	
		ModelAndView model = new ModelAndView();

		if(fecha.equalsIgnoreCase(""))
		{
			Date myDate = new Date();
			fecha =""+ new SimpleDateFormat("dd/MM/yyyy").format(myDate);	
		}
		
		model.setViewName("app.ventareserva.ventareserva.busquedas.bono.disponibles");
		
		model.addObject("buttonAdd",buttonAdd);
		model.addObject("idtipoproducto",idtipoproducto);
		model.addObject("producto",producto);
		model.addObject("fecha",fecha);

		String xml_sesiones= Tools.callServiceXML(request, "obtenerSesionesPorTipoProductoYFecha.action?servicio=obtenerSesionesPorTipoProductoYFecha&numerado=0&date="+fecha+"&idTipoProducto="+idtipoproducto); 
		JSONObject json_sesiones = XML.toJSONObject(xml_sesiones);
		if (json_sesiones.has("error")) throw new Exception(json_sesiones.getString("error"));

		model.addObject("disponibles_datos_sesiones",xml_sesiones);
		model.addObject("disponibles_datos_sesiones_json",json_sesiones.toString(4));

		return model;
	}
	
	// ***********************************************************************************
	@RequestMapping(value = "ajax/ventareserva/busqueda/bono/listadoTiposProducto.do", method = RequestMethod.POST)
	public ResponseEntity<?> obtenerListadoTiposProductoPorRecintoYBono(
			@RequestParam(value = "idRecinto", required = false, defaultValue = "") String idRecinto,
			@RequestParam(value = "idBono", required = false, defaultValue = "") String idBono,
			HttpServletRequest request) throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();
		String respuesta="";
		try {
			respuesta = Tools.callServiceJSON(request, "obtenerListadoTiposProductoPorRecintoYBono.action?servicio=obtenerListadoTiposProductoPorRecintoYBono&xml=<parametro><int>"+idBono+"</int><int>"+idRecinto+"</int></parametro>");
									
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}		
		
		if (respuesta.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
	}	
	
	
	// ***********************************************************************************
	@RequestMapping(value = "ajax/ventareserva/busqueda/bono/actualizar.do", method = RequestMethod.POST)
	public ResponseEntity<?> actualizarBono(
			@RequestParam(value = "idRecinto", required = false, defaultValue = "") String idRecinto,
			@RequestParam(value = "idBono", required = false, defaultValue = "") String idBono,
			@RequestParam(value = "idproducto", required = false, defaultValue = "") String idproducto,
			@RequestParam(value = "idtarifa", required = false, defaultValue = "") String idtarifa,
			@RequestParam(value = "idtipobono", required = false, defaultValue = "") String idtipobono,
			@RequestParam(value = "fechaemision", required = false, defaultValue = "") String fechaemision,
			@RequestParam(value = "fechacaducidad", required = false, defaultValue = "") String fechacaducidad,
			@RequestParam(value = "observaciones_bono", required = false, defaultValue = "") String observaciones_bono,
			@RequestParam(value = "lineacanje", required = false, defaultValue = "") String lineacanje,
			@RequestParam(value = "entradatornoses", required = false, defaultValue = "") String entradatornoses,
			HttpServletRequest request) throws Exception {
		
		HttpHeaders responseHeaders = new HttpHeaders();
		String respuesta="";
		try {
			
			String xml="<obtenerBonoPorId><Bono>";
			xml +="<idbono>"+idBono+"</idbono>";
			xml +="<fechaemision>"+fechaemision+"</fechaemision><fechacaducidad>"+fechacaducidad+"</fechacaducidad>";
			xml += "<producto><idproducto>"+idproducto+"</idproducto></producto>";
			xml +="<tarifa><idtarifa>"+idtarifa+"</idtarifa></tarifa>";
			xml +="<tipobono><idtipobono>"+idtipobono+"</idtipobono></tipobono>";
			xml +="<anulado>0</anulado><observaciones>"+observaciones_bono+"</observaciones>";
			xml +="</Bono></obtenerBonoPorId>";

			
			
			respuesta = Tools.callServiceJSON(request, "actualizarBonos.action?servicio=actualizarBonos&xml="+ URLEncoder.encode(xml, "UTF-8") );
							
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}		
		
		if (respuesta.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
	}	
	
	
	
	
	//****************************LISTAR Venta abonos***************************************************************
	@RequestMapping(value = "/ajax/venta/ventareserva/venta_abono/list_venta_bono.do", method = RequestMethod.POST)
	public ResponseEntity<?> ventaReservaBusquedaVentaBono(
			@RequestParam(value = "refventa", required = false, defaultValue = "") String refventa,
			@RequestParam(value = "refbono", required = false, defaultValue = "") String refbono, 
			@RequestParam(value = "fechabuscadorbonodesde", required = false, defaultValue = "") String fechaDesde,
			@RequestParam(value = "fechabuscadorbonohasta", required = false, defaultValue = "") String fechaHasta,			
			@RequestParam(value = "idcliente_venta_bono", required = false, defaultValue = "") String idcliente,
			@RequestParam(value = "selector_tarifa", required = false, defaultValue = "") String idtarifa,
			@RequestParam(value = "selector_producto", required = false, defaultValue = "") String idproducto,
			@RequestParam(value = "selector_tipo_bono", required = false, defaultValue = "") String idtipobono,
			@RequestParam(value = "anulada", required = false, defaultValue = "") String anulada,
			@RequestParam(value = "facturados", required = false, defaultValue = "") String facturado, 
		    @RequestParam(value = "start", required = false, defaultValue = "0") String startRecord,
      		@RequestParam(value = "length", required = false, defaultValue = "") String maxLength,
      		HttpServletRequest request) throws Exception	
	{		
		
		String xml="<Buscarventabonosparam>";		
		xml+="<refventa>"+refventa+"</refventa>";
		xml+="<refbono>"+refbono+"</refbono>";
		xml+="<idproducto>"+idproducto+"</idproducto>";		
		xml+="<fechaIni>"+fechaDesde+"</fechaIni>";
		xml+="<fechaFin>"+fechaHasta+"</fechaFin>";		
		xml+="<idcliente>"+idcliente+"</idcliente>";
		xml+="<idtipobono>"+idtipobono+"</idtipobono>";
		xml+="<idtarifa>"+idtarifa+"</idtarifa>";
		xml+="<anulada>"+anulada+"</anulada>";
		xml+="<facturado>"+facturado+"</facturado>";
		xml+="<numeroprimerregistro>"+startRecord+"</numeroprimerregistro>";
		xml+="</Buscarventabonosparam>";
		
		String json = "";

		try {
			json = Tools.callServiceJSON(request, "buscarVentaBonos.action?servicio=buscarVentaBonos&xml="+ URLEncoder.encode(xml, "UTF-8") );
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);

	}
	
	//************************************************************************************************************
	@RequestMapping(value = "/ajax/ventareserva/busqueda/ventareservas/listadoCanales.do", method = RequestMethod.POST)
	public ResponseEntity<?> obtenerListadoCanales(HttpServletRequest request) throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();
		String respuesta="";
		try {
			respuesta = Tools.callServiceJSON(request, "obtenerListadoCanales.action?servicio=obtenerListadoCanales");

		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}		
		
		if (respuesta.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
		}
		
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
	}
	//************************************************************************************************************
	@RequestMapping(value = "/ajax/ventareserva/busqueda/ventareservas/listadoUsuarios.do", method = RequestMethod.POST)
	public ResponseEntity<?> obtenerListadoUsuarios(HttpServletRequest request) throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();
		String respuesta="";
		try {
			respuesta = Tools.callServiceJSON(request, "obtenerListadoUsuarios.action?servicio=obtenerListadoUsuarios");			
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}		
		
		if (respuesta.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
		}
		
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
	}
	//************************************************************************************************************	
	@RequestMapping(value = "/ajax/ventareserva/busqueda/ventareservas/listadoFormasPago.do", method = RequestMethod.POST)
	public ResponseEntity<?> obtenerListadoFormasPago(HttpServletRequest request) throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();
		String respuesta="";
		try {
			respuesta = Tools.callServiceJSON(request, "obtenerListadoFormasPago.action?servicio=obtenerListadoFormasPago");			
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}		
		
		if (respuesta.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
		}
		
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
	}	
	//************************************************************************************************************
	
	@RequestMapping(value = "/ajax/venta/ventareserva/busqueda/list_entradas.do", method = RequestMethod.POST)
	public ResponseEntity<?> ventaReservaBusquedaEntradas(
			@RequestParam(value = "ventas-entradas", required = false, defaultValue = "") String ventas,
			@RequestParam(value = "foperacion-entradas", required = false, defaultValue = "") String foperacion, 
			@RequestParam(value = "fvisita-entradas", required = false, defaultValue = "") String fvisita,
			@RequestParam(value = "refentrada", required = false, defaultValue = "") String refEntrada,
			@RequestParam(value = "refventa", required = false, defaultValue = "") String refventa,
			@RequestParam(value = "refreserva", required = false, defaultValue = "") String refreserva, 
			@RequestParam(value = "idCanalVenta", required = false, defaultValue = "") String idCanalVenta,
			@RequestParam(value = "anulada", required = false, defaultValue = "0") String anulada,
			@RequestParam(value = "reservagrupo", required = false, defaultValue = "1") String reservagrupo,
			@RequestParam(value = "idProducto", required = false, defaultValue = "") String idProducto,
			@RequestParam(value = "idrecinto", required = false, defaultValue = "") String idrecinto,
			@RequestParam(value = "dniCliente", required = false, defaultValue = "") String dniCliente,
			@RequestParam(value = "idcliente_entradas", required = false, defaultValue = "") String idcliente_entradas,
			@RequestParam(value = "idUsuario", required = false, defaultValue = "") String idUsuario,			
			@RequestParam(value = "idformapago", required = false, defaultValue = "") String idformapago,
			@RequestParam(value = "idContenido", required = false, defaultValue = "") String idContenido,
			@RequestParam(value = "telefono", required = false, defaultValue = "") String telefono,
			@RequestParam(value = "movil", required = false, defaultValue = "") String movil,			
			@RequestParam(value = "localizadoragencia", required = false, defaultValue = "") String localizadoragencia,			
			@RequestParam(value = "nombreVenta", required = false, defaultValue = "") String nombreVenta,
			@RequestParam(value = "start", required = false, defaultValue = "0") String startRecord,
      		@RequestParam(value = "length", required = false, defaultValue = "") String maxLength,
      		HttpServletRequest request) throws Exception	
	{		
		String xml="<param>";		
		xml+="<ventas>"+ventas+"</ventas>";
		xml+="<fechaIni>"+foperacion+"</fechaIni>";
		xml+="<fechaFin>"+foperacion+"</fechaFin>";
		xml+="<idCanalVenta>"+idCanalVenta+"</idCanalVenta>";
		xml+="<referenciaEntradaIni>"+refEntrada+"</referenciaEntradaIni>";
		xml+="<referenciaEntradaFin>"+refEntrada+"</referenciaEntradaFin>";
		xml+="<referenciaVentaIni>"+refventa+"</referenciaVentaIni>";
		xml+="<referenciaVentaFin>"+refventa+"</referenciaVentaFin>";
		xml+="<anulada>"+anulada+"</anulada>";
		xml+="<idProducto>"+idProducto+"</idProducto>";
		xml+="<idrecinto>"+idrecinto+"</idrecinto>";
		xml+="<dniCliente>"+dniCliente+"</dniCliente>";
		xml+="<idcliente>"+idcliente_entradas+"</idcliente>";
		xml+="<idUsuario>"+idUsuario+"</idUsuario>";
		xml+="<referenciaReservaIni>"+refreserva+"</referenciaReservaIni>";
		xml+="<referenciaReservaFin>"+refreserva+"</referenciaReservaFin>";
		xml+="<idformapago>"+idformapago+"</idformapago>";
		xml+="<fechaVisitaIni>"+fvisita+"</fechaVisitaIni>";
		xml+="<fechaVisitaFin>"+fvisita+"</fechaVisitaFin>";
		xml+="<telefono>"+telefono+"</telefono>";
		xml+="<movil>"+movil+"</movil>";
		xml+="<localizadoragencia>"+localizadoragencia+"</localizadoragencia>";
		xml+="<idContenido>"+idContenido+"</idContenido>";
		xml+="<nombreVenta>"+nombreVenta+"</nombreVenta>";
		xml+="<reservagrupo>"+reservagrupo+"</reservagrupo>";
		xml+="<numeroprimerregistro>"+startRecord+"</numeroprimerregistro>";
		xml+="</param>";
	
		
		
		
		String json = "";

		try {
			json = Tools.callServiceJSON(request, "obtenerEntradas.action?servicio=obtenerEntradas&xml="+ URLEncoder.encode(xml, "UTF-8") );
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);

	}
	
	// ***********************************************************************************
	@RequestMapping(value = "ajax/ventareserva/busqueda/entrada/actualizarUsos.do", method = RequestMethod.POST)
	public ResponseEntity<?> actualizarUsosEntrada(
			@RequestParam(value = "xml", required = false, defaultValue = "") String xml,
			@RequestParam(value = "usos", required = false, defaultValue = "") String usos,
			@RequestParam(value = "idEntrada", required = false, defaultValue = "") String idEntrada,
			HttpServletRequest request) throws Exception {
		
		HttpHeaders responseHeaders = new HttpHeaders();
		String respuesta="";
		try {
			if(xml.equalsIgnoreCase(""))
			{
					String peticion="<param>";
					peticion+="<ventas></ventas>";
					peticion+="<fechaIni></fechaIni><fechaFin></fechaFin>";
					peticion+="<fechaVisitaIni></fechaVisitaIni><fechaVisitaFin></fechaVisitaFin>";
					peticion+="<referenciaEntradaIni>"+idEntrada+"</referenciaEntradaIni><referenciaEntradaFin>"+idEntrada+"</referenciaEntradaFin>";
					peticion+="<referenciaVentaIni></referenciaVentaIni><referenciaVentaFin></referenciaVentaFin>";
					peticion+="<referenciaReservaIni></referenciaReservaIni><referenciaReservaFin></referenciaReservaFin>";
					peticion+="<idCanalVenta></idCanalVenta>";
					peticion+="<anulada></anulada>";
					peticion+="<reservagrupo></reservagrupo>";
					peticion+="<idProducto></idProducto>";
					peticion+="<idrecinto></idrecinto>";
					peticion+="<dniCliente></dniCliente>";
					peticion+="<idcliente></idcliente>";
					peticion+="<idUsuario></idUsuario>";
					peticion+="<idformapago></idformapago>";
					peticion+="<idContenido></idContenido>";
					peticion+="<telefono></telefono>";       
					peticion+="<movil></movil>";
					peticion+="<localizadoragencia></localizadoragencia>";
					peticion+="<numeroprimerregistro></numeroprimerregistro>";
					peticion+="<nombreVenta></nombreVenta>";
					peticion+="</param>";
					
					xml = Tools.callServiceXML(request, "obtenerEntradas.action?servicio=obtenerEntradas&xml="+ URLEncoder.encode(peticion, "UTF-8") );
					xml = xml.replaceAll("<ArrayList>", "");
					xml = xml.replaceAll("</ArrayList>", "");
					
			}
			
			
			respuesta = Tools.callServiceJSON(request, "actualizarEntradasBsqEntradas.action?numeroUsos="+usos+"&xml="+ URLEncoder.encode(xml, "UTF-8") );
							
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}		
		
		if (respuesta.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
	}		
	
	
	// ***************************************************************************************************
	@RequestMapping(value = "ajax/ventareserva/busqueda/entrada/imprimir.do", method = RequestMethod.POST)
	public ResponseEntity<String> imprimirEntrada(
			@RequestParam(value = "data", required = true) List<String> data
			, HttpServletRequest request) throws Exception {

		String xmlList = "";
		Iterator<String> dataIterator = data.iterator();
		while (dataIterator.hasNext()) {
			xmlList += "<int>" + dataIterator.next() + "</int>";
		}

		String xml = Tools.callServiceXML(request, "imprimirEntradas.action?servicio=imprimirEntradas&xml=<listaIdEntradas>" + xmlList + "</listaIdEntradas>");

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	///***************************************************************************************
	@RequestMapping("venta/ventareserva/busqueda/ventaBonos/pantalla_anular.do")
	public ModelAndView ventaBonosPantallaAnular(
			@RequestParam(value = "id", required = false, defaultValue = "") String idVenta,
			HttpServletRequest request) throws Exception {
		
		ModelAndView model = new ModelAndView();
		model.addObject("motivosModificacion", Tools.callServiceXML(request, "obtenerListadoMotivosModificacion.action?servicio=obtenerListadoMotivosModificacion"));
		model.addObject("idVenta", idVenta);
    	
	
    	model.setViewName("app.ventareserva.ventareserva.busquedas.venta_bono.anular");
		return model;
	}
	
	///***************************************************************************************
	@RequestMapping("venta/ventareserva/busqueda/Bonos/pantalla_entradas.do")
	public ModelAndView bonosPantallaEntradas(
			@RequestParam(value = "id", required = false, defaultValue = "") String idBono,
			HttpServletRequest request) throws Exception {
		
		ModelAndView model = new ModelAndView();
		model.addObject("entradas", Tools.callServiceXML(request, "obtenerEntradasPorLD.action?servicio=obtenerEntradasPorLD&xml=<dto><listaIds><int>"+idBono+"</int></listaIds><numeroprimerregistro>0</numeroprimerregistro></dto>"));
    	
	
    	model.setViewName("app.ventareserva.ventareserva.busquedas.bono.entradas");
		return model;
	}	
	
	///***************************************************************************************
		@RequestMapping("venta/ventareserva/busqueda/Bonos/entradas/impresiones.do")
		public ModelAndView impresionesPantallaEntradasBonos(
				@RequestParam(value = "id", required = false, defaultValue = "") String idEntrada,
				HttpServletRequest request) throws Exception {
			
			ModelAndView model = new ModelAndView();
			model.addObject("impresion_entradas", Tools.callServiceXML(request, "obtenerHistoricoImpresionesPorEntrada.action?servicio=obtenerHistoricoImpresionesPorEntrada&xml=<int>"+idEntrada+"</int>"));
	    	
		
	    	model.setViewName("app.ventareserva.ventareserva.busquedas.bono.entradas.impresiones");
			return model;
		}
	
	
	
	
	// ***********************************************************************************
	@RequestMapping(value = "ajax/ventareserva/ventabono/anular.do", method = RequestMethod.POST)
	public ResponseEntity<?> bonoVentaAnular(
			@RequestParam(value = "xmlVentaBonos", required = false, defaultValue = "") String xml,
			HttpServletRequest request) throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();
		
		String respuesta="";
		try {
			respuesta = Tools.callServiceXML(request, "anularVenta.action?servicio=anularVenta&xml="+xml);
								
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}		
		
		if (respuesta.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
		}
		
		return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
	}	

	
	
	//**********************************************Modificar Usos
	@RequestMapping(value = "ajax/ventareserva/busqueda/entrada/modificar.do")
	public ModelAndView entradaModificar(
			@RequestParam(value = "tipoBonos", required = false, defaultValue = "0") String tipoBonos,
			@RequestParam(value = "numUsos", required = false, defaultValue = "0") String numUsos,
			HttpServletRequest request) throws Exception	
	{		
		ModelAndView model = new ModelAndView();
		model.addObject("tipoBonos", tipoBonos);
		model.addObject("numUsos", numUsos);
		model.setViewName("app.ventareserva.ventareserva.busquedas.entradas.modificar");
		return model;
	}
	
	//**********************************************Modificar Usos Masivo
		@RequestMapping(value = "ajax/ventareserva/busqueda/entrada/modificarUsosMasivo.do")
		public ModelAndView entradaModificarMasivo(
			HttpServletRequest request) throws Exception	
		{		
			ModelAndView model = new ModelAndView();
			model.setViewName("app.ventareserva.ventareserva.busquedas.entradas.modificar.masivo");
			return model;
		}
	
	
	
	///**********************************GGL ABONOS*****************************************************
	@RequestMapping("/venta/ventareserva/busqueda/ventareserva/anular.do")					
	public ModelAndView ventaReservaAnular(
			@RequestParam(value = "id", required = false, defaultValue = "") String id,
			@RequestParam(value = "tipo", required = false, defaultValue = "") String tipo,
			@RequestParam(value = "importetotalventa", required = false, defaultValue = "") String importetotalventa,
			@RequestParam(value = "importetotalpagado", required = false, defaultValue = "") String importetotalpagado,
			@RequestParam(value = "idcanal", required = false, defaultValue = "") String idcanal,
			HttpServletRequest request) throws Exception {
		
		ModelAndView model = new ModelAndView();
		
		model.addObject("motivosModificacion", Tools.callServiceXML(request, "obtenerListadoMotivosModificacion.action?servicio=obtenerListadoMotivosModificacion"));
		model.addObject("selectorFormasdePagoPorCanal", Tools.callServiceXML(request, "obtenerListadoFormasPagoPorCanal.action?servicio=obtenerListadoFormasPagoPorCanal&xml=<canal><idcanal>"+idcanal+"</idcanal></canal>"));
		model.addObject("importesParciales", Tools.callServiceXML(request, "obtenerImportesParcialesPorVenta.action?servicio=obtenerImportesParcialesPorVenta&xml=<int>"+id+"</int>"));
		
		
		model.addObject("tipo", tipo);
		model.addObject("id", id);
		
	
		float total= Float.parseFloat(importetotalventa);
		String importeTotal =String.format("%.2f", total);
		float pagado= Float.parseFloat(importetotalpagado);
		String importePagado= String.format("%.2f", pagado);
		float importeDiferencia = total-pagado;
		String  diferencia= String.format("%.2f", importeDiferencia);
		
		model.addObject("importeTotal",importeTotal);
		model.addObject("importePagado",importePagado);
		model.addObject("diferencia",diferencia);
		
		model.setViewName("app.ventareserva.ventareserva.busquedas.venta_reserva.anular");
		
		return model;
	}
	
	
	///***************************************************************************************
	@RequestMapping("/venta/ventareserva/busqueda/entradas/cancelar.do")					
	public ModelAndView entradasCancelar(
			@RequestParam(value = "id", required = false, defaultValue = "") String id,
			@RequestParam(value = "tipo", required = false, defaultValue = "") String tipo,
			@RequestParam(value = "importetotalventa", required = false, defaultValue = "") String importetotalventa,
			@RequestParam(value = "importetotalpagado", required = false, defaultValue = "") String importetotalpagado,
			@RequestParam(value = "idcanal", required = false, defaultValue = "") String idcanal,
			HttpServletRequest request) throws Exception {
		
		ModelAndView model = new ModelAndView();
		
		model.addObject("motivosModificacion", Tools.callServiceXML(request, "obtenerListadoMotivosModificacion.action?servicio=obtenerListadoMotivosModificacion"));
		model.addObject("selectorFormasdePagoPorCanal", Tools.callServiceXML(request, "obtenerListadoFormasPagoPorCanal.action?servicio=obtenerListadoFormasPagoPorCanal&xml=<canal><idcanal>"+idcanal+"</idcanal></canal>"));
		if(tipo.equalsIgnoreCase("V"))
			model.addObject("importesParciales", Tools.callServiceXML(request, "obtenerImportesParcialesPorVenta.action?servicio=obtenerImportesParcialesPorVenta&xml=<int>"+id+"</int>"));
		else
			model.addObject("importesParciales",  "<empty/>");
		
		model.addObject("tipo", tipo);
		model.addObject("id", id);
		
	
		float total= Float.parseFloat(importetotalventa);
		String importeTotal =String.format("%.2f", total);
		float pagado= Float.parseFloat(importetotalpagado);
		String importePagado= String.format("%.2f", pagado);
		float importeDiferencia = total-pagado;
		String  diferencia= String.format("%.2f", importeDiferencia);
		
		model.addObject("importeTotal",importeTotal);
		model.addObject("importePagado",importePagado);
		model.addObject("diferencia",diferencia);
		
		model.setViewName("app.ventareserva.ventareserva.busquedas.entradas.cancelar");
		
		return model;
	}
	
	/*******************************************************************/
	@RequestMapping("/ajax/ventareserva/busqueda/entradas/cancelar.do")
	public ResponseEntity<String> entradasCancelar(
				@RequestParam(value = "xml_anulacion", required = false, defaultValue = "") String xml_anulacion,																		   
				@RequestParam(value = "xml_entradas", required = false, defaultValue = "") String xml_entradas,
 				HttpServletRequest request) throws Exception {

		
		String xml = Tools.callServiceXML(request, "cancelarEntradas.action?modificacion=" + URLEncoder.encode(xml_anulacion, "UTF-8")+"&entradas="+URLEncoder.encode(xml_entradas, "UTF-8"));

		if (xml.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	
	// *****************************************************************************************************
	@RequestMapping("/venta/ventareserva/busqueda/ventareserva/editar_venta_bono.do")
	public ModelAndView bono_editar_venta(
			@RequestParam(value = "id", required = false, defaultValue = "") String id, HttpServletRequest request)
			throws Exception {

		ModelAndView model = new ModelAndView();
		
		String editar_venta = Tools.callServiceXML(request,"obtenerVentaPorId.action?servicio=obtenerVentaPorId&xml=<int>" + id +"</int>");
		model.addObject("editar_venta", editar_venta);
		
		//Ponemos tambien el objeto en json
		JSONObject json = XML.toJSONObject(editar_venta);
		if (json.has("error")) throw new Exception(json.getString("error"));
			model.addObject("editar_venta_json", json);
		
		model.addObject("listado_estados_venta", Tools.callServiceXML(request,"obtenerListadoEstadosVenta.action?servicio=obtenerListadoEstadosVenta"));
		model.addObject("entradas_anuladas", Tools.callServiceXML(request,"obtenerEntradasAnuladasPorIdVenta.action?servicio=obtenerEntradasAnuladasPorIdVenta&xml=<int>" + id +"</int>"));
		model.addObject("id", id);
		model.setViewName("app.ventareserva.ventareserva.busquedas.bonos.editar_bono");
		return model;
	}

	
	
	
	
	
	//**********************************************************************************
	@RequestMapping(value = "/ajax/ventareserva/busqueda/ventareservas/listadoRecintos.do", method = RequestMethod.POST)
	public ResponseEntity<?> obtenerListadoRecintos(HttpServletRequest request) throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();
		String respuesta="";
		try {
			respuesta = Tools.callServiceJSON(request, "obtenerListadoRecintosNonumerados.action?servicio=obtenerListadoRecintosNonumerados");			
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}		
		
		if (respuesta.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
		}
		
		
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		
		return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
	}
	//**********************************************************************************
	@RequestMapping(value = "/ajax/ventareserva/busqueda/ventareservas/listadoProductosPorRecinto.do", method = RequestMethod.POST)
		public ResponseEntity<?> obtenerListadoProductosPorRecinto(
				@RequestParam(value = "idRecinto", required = false, defaultValue = "") String idRecinto,
				HttpServletRequest request) throws Exception {
		
			HttpHeaders responseHeaders = new HttpHeaders();
			String respuesta="";
			try {
				respuesta = Tools.callServiceJSON(request, "obtenerListadoProductosPorRecinto.action?servicio=obtenerListadoProductosPorRecinto&xml="+idRecinto);
				
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}		
			
			if (respuesta.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
			}
			
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
			
			return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
		}
	//**********************************************************************************
	@RequestMapping(value = "/ajax/ventareserva/busqueda/ventareservas/listadoProductos.do", method = RequestMethod.POST)
	public ResponseEntity<?> obtenerListadoProductos(HttpServletRequest request) throws Exception {
		HttpHeaders responseHeaders = new HttpHeaders();
		String respuesta="";
		try {
			respuesta = Tools.callServiceJSON(request, "obtenerListadoProductosNoNumeradosVigentes.action?servicio=obtenerListadoProductosNoNumeradosVigentes");			
		} catch (Exception ex) {
			throw new AjaxException(ex.getMessage());
		}		
		
		if (respuesta.startsWith("<error>")) {
			return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
		}
		
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
	}	
	//**********************************************************************************
		@RequestMapping(value = "/ajax/ventareserva/busqueda/ventareservas/listadoContenidos.do", method = RequestMethod.POST)
		public ResponseEntity<?> obtenerListadoContenidos(HttpServletRequest request) throws Exception {
			HttpHeaders responseHeaders = new HttpHeaders();
			String respuesta="";
			try {
				respuesta = Tools.callServiceJSON(request, "obtenerContenidosNoNumerados.action?servicio=obtenerContenidosNoNumerados");			
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}		
			
			if (respuesta.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
			}
			
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
			return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
		}			
	//**********************************************************************************
		@RequestMapping(value = "/ajax/ventareserva/busqueda/ventareservas/listadoContenidosPorRecinto.do", method = RequestMethod.POST)
			public ResponseEntity<?> obtenerListadoContenidosPorRecinto(
					@RequestParam(value = "idRecinto", required = false, defaultValue = "") String idRecinto,
					HttpServletRequest request) throws Exception {
			
				HttpHeaders responseHeaders = new HttpHeaders();
				String respuesta="";
				try {
					respuesta = Tools.callServiceJSON(request, "obtenerListadoContenidosPorRecinto.action?servicio=obtenerListadoContenidosPorRecinto&xml=<java.lang.Integer>"+idRecinto+"</java.lang.Integer>");
					
				} catch (Exception ex) {
					throw new AjaxException(ex.getMessage());
				}		
				
				if (respuesta.startsWith("<error>")) {
					return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
				}
				
				responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
				
				return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
			}
	//**********************************************************************************
	@RequestMapping(value = "/ajax/ventareserva/busqueda/ventareservas/listadoContenidosPorProducto.do", method = RequestMethod.POST)
		public ResponseEntity<?> obtenerListadoContenidosPorProducto(
				@RequestParam(value = "idProducto", required = false, defaultValue = "") String idProducto,
				HttpServletRequest request) throws Exception {
		
			HttpHeaders responseHeaders = new HttpHeaders();
			String respuesta="";
			try {
				respuesta = Tools.callServiceJSON(request, "obtenerListadoContenidosPorProducto.action?servicio=obtenerListadoContenidosPorProducto&xml="+idProducto);
				
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}		
			
			if (respuesta.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
			}
			
			return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
		}		
	//**********************************************************************************
		@RequestMapping(value = "/ajax/ventareserva/busqueda/ventareservas/listadoUsuariosCanal.do", method = RequestMethod.POST)
			public ResponseEntity<?> obtenerListadoUsuariosPorCanal(
					@RequestParam(value = "idCanal", required = false, defaultValue = "") String idCanal,
					HttpServletRequest request) throws Exception {
			
				HttpHeaders responseHeaders = new HttpHeaders();
				String respuesta="";
				try {
					respuesta = Tools.callServiceJSON(request, "obtenerListadoUsuariosCanal.action?servicio=obtenerListadoUsuariosCanal&xml=<int>"+idCanal+"</int>");
					
				} catch (Exception ex) {
					throw new AjaxException(ex.getMessage());
				}		
				
				if (respuesta.startsWith("<error>")) {
					return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
				}
				
				return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
			}
		//**********************************************************************************
		@RequestMapping(value = "/ajax/ventareserva/busqueda/ventareservas/listadopuntosrecogida.do", method = RequestMethod.POST)
		public ResponseEntity<?> obtenerListadoPuntosDeRecogida(HttpServletRequest request) throws Exception {
			HttpHeaders responseHeaders = new HttpHeaders();
			String respuesta="";
			try {
				respuesta = Tools.callServiceJSON(request, "obtenerListadoPuntosrecogida.action?servicio=obtenerListadoPuntosrecogida");			
			} catch (Exception ex) {
				throw new AjaxException(ex.getMessage());
			}		
			
			if (respuesta.startsWith("<error>")) {
				return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
			}
			
			
			responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

			
			return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
		}
		//**********************************************************************************
				@RequestMapping(value = "/ajax/ventareserva/busqueda/ventareservas/formasdepago.do", method = RequestMethod.POST)
				public ResponseEntity<?> obtenerListadoFormasDePago(HttpServletRequest request) throws Exception {
					HttpHeaders responseHeaders = new HttpHeaders();
					String respuesta="";
					try {
						respuesta = Tools.callServiceJSON(request, "obtenerFormasPago.action?servicio=obtenerFormasPago");			
					} catch (Exception ex) {
						throw new AjaxException(ex.getMessage());
					}		
					
					if (respuesta.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(respuesta), HttpStatus.BAD_REQUEST);
					}
					
					
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					
					return new ResponseEntity<String>(respuesta, responseHeaders, HttpStatus.OK);
				}	
		//************************************************************************************************************
				@RequestMapping(value = "/ajax/ventareserva/busqueda/ventareservas/list_venta_reservas_busqueda.do", method = RequestMethod.POST)
				public ResponseEntity<?> listBusquedasVentaReserva(
						@RequestParam(value = "dni_busqueda", required = false, defaultValue = "") String dniCliente,
						@RequestParam(value = "ventas", required = false, defaultValue = "") String anulada,
						@RequestParam(value = "fechafinbusquedaoperacion", required = false, defaultValue = "") String fechafinventa,
						@RequestParam(value = "fechainiciobusquedaoperacion", required = false, defaultValue = "") String fechaventa,
						@RequestParam(value = "fechafinbusquedavisita", required = false, defaultValue = "") String fechafinvisita,
						@RequestParam(value = "fechainiciobusquedavisita", required = false, defaultValue = "") String fechavisita,
						@RequestParam(value = "financiada", required = false, defaultValue = "") String financiada,
						@RequestParam(value = "selector_canal", required = false, defaultValue = "") String idCanal ,
						@RequestParam(value = "idcliente_busqueda", required = false, defaultValue = "") String idCliente,
						@RequestParam(value = "selector_producto", required = false, defaultValue = "") String idProducto,
						@RequestParam(value = "selector_contenido", required = false, defaultValue = "") String idContenido,
						@RequestParam(value = "selector_usuario", required = false, defaultValue = "") String idUsuario,
						@RequestParam(value = "selector_formaspago", required = false, defaultValue = "") String idFormaPago,
						@RequestParam(value = "cliente_busqueda", required = false, defaultValue = "") String nombreCliente,
						@RequestParam(value = "nombreventa_busqueda", required = false, defaultValue = "") String nombreVenta,
						@RequestParam(value = "refentrada_busqueda", required = false, defaultValue = "") String referenciaEntradaFin,
						@RequestParam(value = "refentrada_busqueda", required = false, defaultValue = "") String referenciaEntradaIni,
						@RequestParam(value = "refreserva_busqueda", required = false, defaultValue = "") String referenciaReservaFin,
						@RequestParam(value = "refreserva_busqueda", required = false, defaultValue = "") String referenciaReservaIni,
						@RequestParam(value = "refventa_busqueda", required = false, defaultValue = "") String referenciaVentaIni,
						@RequestParam(value = "refventa_busqueda", required = false, defaultValue = "") String referenciaVentaFin,
						@RequestParam(value = "ventas-reservas", required = false, defaultValue = "") String ventas,
						@RequestParam(value = "reservas", required = false, defaultValue = "") String reservagrupo,
						@RequestParam(value = "telefono_busqueda", required = false, defaultValue = "") String telefono,
						@RequestParam(value = "movil_busqueda", required = false, defaultValue = "") String movil,
						@RequestParam(value = "selector_recinto", required = false, defaultValue = "") String idRecinto,
						@RequestParam(value = "localizador_busqueda", required = false, defaultValue = "") String localizadorAgencia,
						@RequestParam(value = "selector_puntorecogida", required = false, defaultValue = "") String idPuntosRecogida,
						@RequestParam(value = "start", required = false, defaultValue = "0") String startRecord,
			      		@RequestParam(value = "length", required = false, defaultValue = "") String maxLength,
						HttpServletRequest request) throws Exception {

					String jsonstr = "";
					String xmlresultado="";
					String xml="";
					xml = "<param>";
					xml = xml + "<dniCliente jr_param=\"p_identificadoronif\">"+dniCliente+"</dniCliente>";
					xml = xml + "<anulada jr_param=\"p_anuladas\">"+anulada+"</anulada>";
					xml = xml + "<fechaFin jr_param=\"p_fechafinventa\">"+fechafinventa+"</fechaFin>";
					xml = xml + "<fechaIni jr_param=\"p_fechaventa\">"+fechaventa+"</fechaIni>";
					xml = xml + "<fechaVisitaFin jr_param=\"p_fechafinvisita\">"+fechafinvisita+"</fechaVisitaFin>";
					xml = xml + "<fechaVisitaIni jr_param=\"p_fechavisita\">"+fechavisita+"</fechaVisitaIni>";
					xml = xml + "<financiada>"+financiada+"</financiada>";
					xml = xml + "<idCanalVenta jr_param=\"p_idcanal\">"+idCanal+"</idCanalVenta>";
					xml = xml + "<idcliente jr_param=\"p_idcliente\">"+idCliente+"</idcliente>";
					xml = xml + "<idProducto jr_param=\"p_idproducto\">"+idProducto+"</idProducto>";
					xml = xml + "<idContenido jr_param=\"p_idcontenido\">"+idContenido+"</idContenido>";
					xml = xml + "<idUsuario jr_param=\"p_idusuario\">"+idUsuario+"</idUsuario>";
					xml = xml + "<idformapago jr_param=\"p_idformapago\">"+idFormaPago+"</idformapago>";
					xml = xml + "<nombreCliente>"+nombreCliente+"</nombreCliente>";
					xml = xml + "<nombreVenta jr_param=\"p_nombreventa\">"+nombreVenta+"</nombreVenta>";
					xml = xml + "<numeroprimerregistro>"+startRecord+"</numeroprimerregistro>";
					xml = xml + "<referenciaEntradaFin>"+referenciaEntradaFin+"</referenciaEntradaFin>";
					xml = xml + "<referenciaEntradaIni jr_param=\"p_identrada\">"+referenciaEntradaIni+"</referenciaEntradaIni>";
					xml = xml + "<referenciaReservaFin>"+referenciaReservaFin+"</referenciaReservaFin>";
					xml = xml + "<referenciaReservaIni jr_param=\"p_idreserva\">"+referenciaReservaIni+"</referenciaReservaIni>";
					xml = xml + "<referenciaVentaFin>"+referenciaVentaFin+"</referenciaVentaFin>";
					xml = xml + "<referenciaVentaIni jr_param=\"p_idventa\">"+referenciaVentaIni+"</referenciaVentaIni>";
					xml = xml + "<ventas jr_param=\"p_ventas\">"+ventas+"</ventas>";
					xml = xml + "<reservagrupo jr_param=\"p_grupo\">"+reservagrupo+"</reservagrupo>";
					xml = xml + "<login/>";
					xml = xml + "<telefono jr_param=\"p_telefono\">"+telefono+"</telefono>";
					xml = xml + "<movil jr_param=\"p_telefonomovil\">"+movil+"</movil>";
					xml = xml + "<idrecinto jr_param=\"p_idrecinto\">"+idRecinto+"</idrecinto>";
					xml = xml + "<localizadoragencia jr_param=\"p_localizador\">"+localizadorAgencia+"</localizadoragencia>";
					xml = xml + "<idpuntosrecogida jr_param=\"p_idpuntosrecogida\">"+idPuntosRecogida+"</idpuntosrecogida>";
					xml = xml + "</param>";
					try {
						
						xmlresultado = Tools.callServiceXML(request, "obtenerVentasYReservas.action?servicio=obtenerVentasYReservas&xml="+ URLEncoder.encode(xml, "UTF-8"));
						//Este json hay que tratarlo para convertir los nodos ventas y reservas a <item> e idventa e idreserva a <iditem>
						xmlresultado=xmlresultado.replaceAll("Reserva", "Item");
						xmlresultado=xmlresultado.replaceAll("Venta", "Item");
						xmlresultado=xmlresultado.replaceAll("idreserva", "iditem");
						xmlresultado=xmlresultado.replaceAll("idventa", "iditem");
						
						
						
						JSONObject json = XML.toJSONObject(xmlresultado);
						
						
						if (json.has("error")) throw new Exception(json.getString("error"));
						json.append("iTotalRecords", "99999"); 
						json.append("iTotalDisplayRecords", "99999");
						jsonstr=json.toString(4);
						
												
					} catch (Exception ex) {
						throw new AjaxException(ex.getMessage());
					}

					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(jsonstr, responseHeaders, HttpStatus.OK);
				}
				//**********************************************************************************
				@RequestMapping(value = "/ajax/ventareserva/busqueda/ventareservas/listado_ventasreservas.do", method = RequestMethod.POST)
				public ResponseEntity<?> showListadoVentasReservas(
						@RequestParam(value = "idusuario", required = false, defaultValue = "") String idUsuario,
						HttpServletRequest request) throws Exception {
					
					HttpHeaders responseHeaders = new HttpHeaders();
					String json="";
					try {
						json = Tools.callServiceXMLSinReemplazo(request, "abrirInformeUsuario.action?servicio=abrirInformeUsuario&xml=<java.lang.Integer>"+idUsuario+"</java.lang.Integer>");
					} catch (Exception ex) {
						throw new AjaxException(ex.getMessage());
					}
					
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}						
		//***************************************************************************************************
				@RequestMapping(value = "/venta/ventareserva/busqueda/entrada/ver_formas_pago.do")
				public ModelAndView verFormasDePago(
						@RequestParam(value = "id", required = false, defaultValue = "") String id,
						HttpServletRequest request) throws Exception{		
					
					
					ModelAndView model = new ModelAndView();
					String xml ="<int>"+id+"</int>";
					model.addObject("formas_pago", Tools.callServiceXML(request, "obtenerImportesParcialesPorVenta.action?servicio=obtenerImportesParcialesPorVenta&xml="+xml));
					model.setViewName("app.ventareserva.ventareserva.busquedas.venta_reserva.ver_formas_pago");
					return model;
				}
		//***************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/ventareservas/anular.do")
				public ResponseEntity<String> ventaReserva_anular(
							@RequestParam(value = "xml_anulacion", required = false, defaultValue = "") String xml_anulacion,																		   
			 				HttpServletRequest request) throws Exception {

					
					String xml = Tools.callServiceXML(request, "anularVenta.action?servicio=anularVenta&xml=" + URLEncoder.encode(xml_anulacion, "UTF-8"));

					if (xml.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
					}
					return new ResponseEntity<String>(HttpStatus.OK);
				}
				
				//***************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/ventareservas/anular_reserva.do")
				public ResponseEntity<String> ventaReserva_anularReserva(
							@RequestParam(value = "xml_anulacion", required = false, defaultValue = "") String xml_anulacion,																		   
			 				HttpServletRequest request) throws Exception {

					
					String xml = Tools.callServiceXML(request, "nularReserva.action?servicio=anularReserva&xml=" + URLEncoder.encode(xml_anulacion, "UTF-8"));

					if (xml.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
					}
					return new ResponseEntity<String>(HttpStatus.OK);
				}
				
				
				

				// *****************************************************************************************************
				@RequestMapping("/venta/ventareserva/busqueda/ventareserva/editar_venta.do")
				public ModelAndView ventaReserva_editar(
						@RequestParam(value = "id", required = false, defaultValue = "") String id, HttpServletRequest request)
						throws Exception {

					ModelAndView model = new ModelAndView();
					
					String editar_venta = Tools.callServiceXML(request,"obtenerVentaPorId.action?servicio=obtenerVentaPorId&xml=<int>" + id +"</int>");
					model.addObject("editar_venta", editar_venta);
					
					//Ponemos tambien el objeto en json
					JSONObject json = XML.toJSONObject(editar_venta);
					if (json.has("error")) throw new Exception(json.getString("error"));
						model.addObject("editar_venta_json", json);
					
					model.addObject("listado_estados_venta", Tools.callServiceXML(request,"obtenerListadoEstadosVenta.action?servicio=obtenerListadoEstadosVenta"));
					model.addObject("entradas_anuladas", Tools.callServiceXML(request,"obtenerEntradasAnuladasPorIdVenta.action?servicio=obtenerEntradasAnuladasPorIdVenta&xml=<int>" + id +"</int>"));
					model.addObject("id", id);
					model.setViewName("app.ventareserva.ventareserva.busquedas.venta_reserva.editar_venta");
					

					return model;
				}
				
				//**********************************************************************************
				@RequestMapping(value = "/ajax/ventareserva/busqueda/ventareservas/listado_detallesanuladas.do", method = RequestMethod.POST)
				public ResponseEntity<?> showListadoDetallesAnuladas(
						@RequestParam(value = "idventa", required = false, defaultValue = "") String idVenta,						
			      		HttpServletRequest request) throws Exception	
				{		
					String json = "";

					try {
						json = Tools.callServiceJSON(request, "obtenerVentaPorId.action?servicio=obtenerVentaPorId&xml=<int>" + idVenta +"</int>");
					} catch (Exception ex) {
						throw new AjaxException(ex.getMessage());
					}

					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);

				}
				//**********************************************************************************
				@RequestMapping(value = "/ajax/ventareserva/busqueda/ventareservas/listado_entradasanuladas.do", method = RequestMethod.POST)
				public ResponseEntity<?> showListadoEntradasAnuladas(
						@RequestParam(value = "idventa", required = false, defaultValue = "") String idVenta,						
			      		HttpServletRequest request) throws Exception	
				{		
					
					
					String json = "";

					try {
						json = Tools.callServiceJSON(request, "obtenerEntradasAnuladasPorIdVenta.action?servicio=obtenerEntradasAnuladasPorIdVenta&xml=<int>"+idVenta+"</int>");
					} catch (Exception ex) {
						throw new AjaxException(ex.getMessage());
					}

					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);

				}
				// *****************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/show_historico_cambios_venta.do")
				public ModelAndView showHistoricoCambiosVenta(
						@RequestParam(value = "id", required = false, defaultValue = "") String idVenta,
						HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					String datos="";
					datos = "<int>"+idVenta+"</int>";
					

					model.addObject("historico_cambios_venta", Tools.callServiceXML(request, "obtenerVentaPorIdReducidoModificaciones.action?servicio=obtenerVentaPorIdReducidoModificaciones&xml=" + datos));
					model.setViewName("app.ventareserva.ventareserva.busquedas.venta_reserva.historico_cambios_venta");
					
					return model;
				}
				
				//********************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/show_observaciones_venta.do")
				public ModelAndView showObservacionesVenta(
						@RequestParam(value = "id", required = false, defaultValue = "") String idVenta,
						@RequestParam(value = "observaciones", required = false, defaultValue = "") String observaciones,
						HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					model.addObject("id", idVenta);
					model.addObject("observaciones", observaciones);
					model.setViewName("app.ventareserva.ventareserva.busquedas.venta_reserva.observaciones_venta");
					
					return model;
				}

				// *****************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/save_observaciones.do")
				public ResponseEntity<String> saveObservacionesVenta(
							@RequestParam(value = "idVenta", required = false, defaultValue = "") String idVenta,																		   
							@RequestParam(value = "observaciones_venta_dialog", required = false, defaultValue = "") String observaciones,
			 				HttpServletRequest request) throws Exception {

					
					String xml = Tools.callServiceXML(request, "actualizarObservacionesVenta.action?observ="+observaciones+"&idventa="+idVenta+"&servicio=actualizarObservacionesVenta");

					if (xml.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
					}
					return new ResponseEntity<String>(HttpStatus.OK);
				}
				//********************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/show_carta_confirmacion_venta.do")
				public ModelAndView showCartaConfirmacionVenta(						
						@RequestParam(value = "idVenta", required = false, defaultValue = "") String idVenta,
						HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					model.addObject("idVenta", idVenta);
					model.addObject("idioma_carta", Tools.callServiceXML(request, "obtenerListadoIdiomas.action?servicio=obtenerListadoIdiomas"));
					model.setViewName("app.ventareserva.ventareserva.busquedas.venta_reserva.carta_confirmacion_venta");
					
					return model;
				}
				// *****************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/show_usos_impresiones_venta.do")
				public ModelAndView showUsosImpresionesVenta(		
						@RequestParam(value = "idVenta", required = false, defaultValue = "") String idVenta,
						@RequestParam(value = "idReserva", required = false, defaultValue = "") String idReserva,
						@RequestParam(value = "idLineaDetalle", required = false, defaultValue = "") String idLineaDetalle,
						HttpServletRequest request) throws Exception {
					
								
					
					ModelAndView model = new ModelAndView();
					model.addObject("id",idVenta);
					model.addObject("idReserva",idReserva);
					model.addObject("idLineaDetalleUsuos",idLineaDetalle);

					
					model.setViewName("app.ventareserva.ventareserva.busquedas.venta_reserva.usos_impresiones_venta");
					
					return model;
				}
				// *****************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/show_historico_impresiones_por_entrada.do")
				public ModelAndView showHistoricoImpresionesPorentrada(		
						@RequestParam(value = "identrada", required = false, defaultValue = "") String idEntrada,
						HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					model.addObject("id",idEntrada);
					model.addObject("historico_impresiones_entrada", Tools.callServiceXML(request, "obtenerHistoricoImpresionesPorEntrada.action?servicio=obtenerHistoricoImpresionesPorEntrada&xml=<int>"+idEntrada+"</int>"));
					model.setViewName("app.ventareserva.ventareserva.busquedas.venta_reserva.historico_impresiones_por_entrada");
					
					return model;
				}
				// *****************************************************************************************************
					
				@RequestMapping(value = "/ajax/ventareserva/busqueda/entrada/reimprimir.do", method = RequestMethod.POST)
				public ResponseEntity<String> reimprimirEntrada(
						@RequestParam(value = "data", required = true) List<String> data
						, HttpServletRequest request) throws Exception {

					String xmlList = "";
					Iterator<String> dataIterator = data.iterator();
					while (dataIterator.hasNext()) {
						xmlList += "<int>" + dataIterator.next() + "</int>";
					}

					String xml = Tools.callServiceXML(request, "regenerarImprimirEntradas.action?servicio=regenerarImprimirEntradas&xml=<listaIdEntrada>" + xmlList + "</listaIdEntrada>");

					if (xml.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
					}

					return new ResponseEntity<String>(HttpStatus.OK);
				}
				

				//*****************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/show_formas_de_pago_venta.do")
				public ModelAndView showFormasDePagoVenta(		
						@RequestParam(value = "idVenta", required = false, defaultValue = "") String idVenta,
						HttpServletRequest request) throws Exception {
					
					
					ModelAndView model = new ModelAndView();
					
					model.addObject("selector_motivos", Tools.callServiceXML(request, "obtenerListadoMotivosModificacion.action?servicio=obtenerListadoMotivosModificacion"));
					model.addObject("selector_formas_pago", Tools.callServiceXML(request, "obtenerListadoFormasPago.action?servicio=obtenerListadoFormasPago"));
					model.addObject("datos_venta",Tools.callServiceJSON(request,  "obtenerImportesParcialesPorVenta.action?servicio=obtenerImportesParcialesPorVenta&xml=<int>"+idVenta+"</int>"));
					model.setViewName("app.ventareserva.ventareserva.busquedas.editar_formas_de_pago_venta");
					
					return model;
				}
				//********************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/show_nom_bonos_agencia_venta.do")
				public ModelAndView showNumBonosAgenciaVenta(
						@RequestParam(value = "idlineadetalle", required = false, defaultValue = "") String idLineaDetalle,
						@RequestParam(value = "numbonoagencia", required = false, defaultValue = "") String numeroBonoAgencia,
						HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					model.addObject("id", idLineaDetalle);
					model.addObject("numero", numeroBonoAgencia);
					model.setViewName("app.ventareserva.ventareserva.busquedas.venta_reserva.num_bono_agencia_venta");
					
					return model;
				}
				// *****************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/save_venta")
				public ResponseEntity<String> saveVenta(
							@RequestParam(value = "venta_xml", required = false, defaultValue = "") String xml_venta,																   
							
			 				HttpServletRequest request) throws Exception {
					String venta= "<listaModificacion><Modificacion>" + xml_venta + "</Modificacion></listaModificacion>";
										
					String xml = Tools.callServiceXML(request, "actualizarVentas.action?servicio=actualizarVentas&xml="+ URLEncoder.encode(venta, "UTF-8"));

					if (xml.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
					}
					return new ResponseEntity<String>(HttpStatus.OK);
				}
				// ***************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/show_generar_factura.do")
				public ModelAndView ventareservaShowGenerarFactura(
						@RequestParam(value = "idventa", required = false, defaultValue = "") String idVenta, 
						@RequestParam(value = "idcliente", required = false, defaultValue = "") String idCliente, 
						HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					
					model.setViewName("app.venta.ventareserva.generar_factura");

					model.addObject("idVenta",idVenta);
					model.addObject("idCliente",idCliente);
					model.addObject("tipos_serie_generacion_factura", Tools.callServiceXML(request,"obtenerListadoTiposFacturaEntidadGestora.action?servicio=obtenerListadoTiposFacturaEntidadGestora"));
					
					return model;
				}	
				
				
				// ***************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/generar_factura.do")
				public ResponseEntity<?> ventareservaGenerarFactura(
						@RequestParam(value = "idventa", required = false, defaultValue = "") String idVenta, 
						@RequestParam(value = "idtipofactura", required = false, defaultValue = "") String idTipoFactura, 
						HttpServletRequest request) throws Exception {

				     String json="";
				       try {
			               		json = Tools.callServiceJSON(request, "generarFactura.action?servicio=generarFactura&xml="+URLEncoder.encode("<gfp><listaIdsVenta><int>"+idVenta+"</int></listaIdsVenta><idTipoFactura>"+idTipoFactura+"</idTipoFactura></gfp>","UTF-8"));
				       	   } catch (Exception ex) {
			               throw new AjaxException(ex.getMessage());
				        }		
					
					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}	
				// *****************************************************************************************************
				@RequestMapping("/venta/ventareserva/busqueda/ventareserva/editar_reserva.do")
				public ModelAndView ventaReserva_reserva(
						@RequestParam(value = "id", required = false, defaultValue = "") String id, HttpServletRequest request)
						throws Exception {

					ModelAndView model = new ModelAndView();
					
					String editar_reserva = Tools.callServiceXML(request,"obtenerReservaPorId.action?servicio=obtenerReservaPorId&xml=<int>" + id +"</int>");
					model.addObject("editar_reserva", editar_reserva);
					
					//Ponemos tambien el objeto en json
					JSONObject json = XML.toJSONObject(editar_reserva);
					
					
					/*if (json.has("error")) 
						throw new Exception(json.getString("error"));*/
					
					model.addObject("editar_reserva_json", json);
					model.addObject("listado_estados_reserva", Tools.callServiceXML(request,"obtenerListadoEstadosReserva.action?servicio=obtenerListadoEstadosReserva"));
					model.addObject("listado_puntos_recogida", Tools.callServiceXML(request,"obtenerListadoPuntosrecogida.action?servicio=obtenerListadoPuntosrecogida"));
					model.addObject("id", id);
					model.setViewName("app.ventareserva.ventareserva.busquedas.venta_reserva.editar_reserva");
					
					
					return model;
				}	
				// *****************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/show_historico_cambios_reserva.do")
				public ModelAndView showHistoricoCambiosReserva(
						@RequestParam(value = "id", required = false, defaultValue = "") String idReserva,
						HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					String datos="";
					datos = "<int>"+idReserva+"</int>";
					

					model.addObject("historico_cambios_reserva", Tools.callServiceXML(request,"obtenerReservaPorIdModificaciones.action?servicio=obtenerReservaPorIdModificaciones&xml=" + datos));
					model.setViewName("app.ventareserva.ventareserva.busquedas.venta_reserva.historico_cambios_reserva");
					
					return model;
				}
				//********************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/show_observaciones_reserva.do")
				public ModelAndView showObservacionesReserva(
						@RequestParam(value = "id", required = false, defaultValue = "") String idReserva,
						@RequestParam(value = "observaciones", required = false, defaultValue = "") String observaciones,
						HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					model.addObject("id", idReserva);
					model.addObject("observaciones", observaciones);
					model.setViewName("app.ventareserva.ventareserva.busquedas.venta_reserva.observaciones_reserva");
					
					return model;
				}
				// *****************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/save_observaciones_reserva.do")
				public ResponseEntity<String> saveObservacionesReserva(
							@RequestParam(value = "idReserva", required = false, defaultValue = "") String idReserva,																		   
							@RequestParam(value = "observaciones_reserva_dialog", required = false, defaultValue = "") String observaciones,
			 				HttpServletRequest request) throws Exception {

					
					String datos= observaciones+"&idreserva="+idReserva+"&servicio=actualizarObservacionesReserva";
					
					String xml = Tools.callServiceXML(request, "actualizarObservacionesReserva.action?observ="+URLEncoder.encode(observaciones,"UTF-8")+"&idreserva="+idReserva+"&servicio=actualizarObservacionesReserva");
					//String xml = Tools.callServiceXML(request, "actualizarObservacionesReserva.action?observ="+URLEncoder.encode(datos,"UTF-8"));

					if (xml.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
					}
					return new ResponseEntity<String>(HttpStatus.OK);
				}
				//*****************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/show_formas_de_pago_reserva.do")
				public ModelAndView showFormasDePagoReserva(		
						@RequestParam(value = "idReserva", required = false, defaultValue = "") String idReserva,
						@RequestParam(value = "idVenta", required = false, defaultValue = "") String idVenta,
						@RequestParam(value = "idCanal", required = false, defaultValue = "") String idCanal,
						HttpServletRequest request) throws Exception {
					
				
				
					ModelAndView model = new ModelAndView();
					model.addObject("id",idReserva);
					model.addObject("idVenta",idVenta);
					model.addObject("listado_formas_pago_por_canal", Tools.callServiceXML(request, "obtenerListadoFormasPagoPorCanal.action?servicio=obtenerListadoFormasPagoPorCanal&xml=<canal><idcanal>"+idCanal+"</idcanal></canal>"));
					model.addObject("motivos_modificacion", Tools.callServiceXML(request, "obtenerListadoMotivosModificacion.action?servicio=obtenerListadoMotivosModificacion"));
					
					
					model.addObject("datos_reserva", Tools.callServiceJSON(request, "obtenerFormasPagoPorReserva.action?servicio=obtenerFormasPagoPorReserva&xml=<int>"+idReserva+"</int>"));
					model.addObject("datos_reserva_xml", Tools.callServiceXML(request, "obtenerFormasPagoPorReserva.action?servicio=obtenerFormasPagoPorReserva&xml=<int>"+idReserva+"</int>"));
					
					model.setViewName("app.ventareserva.ventareserva.busquedas.editar_formas_de_pago_reserva");
					
					return model;
				}
				
				
				
				
				
				// *****************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/save_formas_de_pago_reserva.do")
				public ResponseEntity<String> saveFormasDePagoReserva(
							@RequestParam(value = "xmlPagos", required = false, defaultValue = "") String xmlPagos,																		   
							@RequestParam(value = "strMotivo", required = false, defaultValue = "") String motivo,
			 				HttpServletRequest request) throws Exception {

					
					String xml = Tools.callServiceXML(request, "actualizarFormasPagoReserva.action?servicio=actualizarFormasPagoReserva&motivomodificacion="+motivo+"&ventas="+URLEncoder.encode(xmlPagos, "UTF-8"));
					
					if (xml.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
					}
					return new ResponseEntity<String>(HttpStatus.OK);
				}
				//********************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/show_carta_confirmacion_reserva.do")
				public ModelAndView showCartaConfirmacionReserva(
						@RequestParam(value = "idReserva", required = false, defaultValue = "") String idReserva,
						HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					model.addObject("idReserva", idReserva);
					model.addObject("idioma_carta", Tools.callServiceXML(request, "obtenerListadoIdiomas.action?servicio=obtenerListadoIdiomas"));
					model.setViewName("app.ventareserva.ventareserva.busquedas.venta_reserva.carta_confirmacion_reserva");
					
					return model;
				}
				//********************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/show_usos_impresiones_reserva.do")
				public ModelAndView showUsosImpresionesReserva(		
						@RequestParam(value = "idReserva", required = false, defaultValue = "") String idReserva,
						HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					model.addObject("id",idReserva);
				
					model.addObject("usos_impresiones_reserva", Tools.callServiceXML(request, "obtenerEntradasPorReserva.action?servicio=obtenerEntradasPorReserva&xml=<dto><idreserva>"+idReserva+"</idreserva><numeroprimerregistro>0</numeroprimerregistro></dto>"));
					model.setViewName("app.ventareserva.ventareserva.busquedas.venta_reserva.usos_impresiones_reserva");
					
					return model;
				}
				// *****************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/save_reserva.do")
				public ResponseEntity<String> saveReserva(
							@RequestParam(value = "reserva_xml", required = false, defaultValue = "") String xml_reserva,																   
							
			 				HttpServletRequest request) throws Exception {
					
					String reserva=  xml_reserva ;
					String xml = Tools.callServiceXML(request, "actualizarReserva.action?servicio=actualizarReserva&xml="+ URLEncoder.encode(reserva, "UTF-8"));
					
					if (xml.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
					}
					return new ResponseEntity<String>(HttpStatus.OK);
				}
				//********************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/modificar_venta.do")
				public ModelAndView editarReservarModificarVenta(
						HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					
					model.addObject("ventareserva_productos_principales", Tools.callServiceXML(request, "obtenerListadoProductosPrincipales.action?servicio=obtenerListadoProductosPrincipales"));
					
					model.setViewName("app.ventareserva.ventareserva.busquedas.venta_reserva.editar_reserva.modificar_venta");
					return model;
				}
				//********************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/show_hora_presentacion.do")
				public ModelAndView showHorasPresentacionBusqueda(
						@RequestParam(value = "button", required = false, defaultValue = "") String buttonAdd,
						@RequestParam(value = "list", required = false, defaultValue = "") String list,
						@RequestParam(value = "xml", required = false, defaultValue = "") String xml,
						HttpServletRequest request) throws Exception {
				
					ModelAndView model = new ModelAndView();

					model.setViewName("app.ventareserva.ventareserva.busquedas.hora_presentacion");
					
					model.addObject("buttonAdd",buttonAdd);
					model.addObject("list",list);

					String xml_horas= Tools.callServiceXML(request, "obtenerListadoHoraPresentacion.action?servicio=obtenerListadoHoraPresentacion&xml="+URLEncoder.encode(xml,"UTF-8")); 

					model.addObject("hora_presentacion_datos",xml_horas);

					return model;
				}
				// *****************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/actualizar_horas_presentacio.do")
				public ResponseEntity<String> actualizarHorasPresentacion(
							@RequestParam(value = "xml_horas", required = false, defaultValue = "") String xml_horas,																   
							
			 				HttpServletRequest request) throws Exception {
					
					String reserva=  "<listaLDZS>"+xml_horas+"</listaLDZS>";
					String xml = Tools.callServiceXML(request, "actualizarHorasPresentacionLDZS.action?servicio=actualizarHorasPresentacionLDZS&xml="+ URLEncoder.encode(reserva, "UTF-8"));
					
					if (xml.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xml), HttpStatus.BAD_REQUEST);
					}
					return new ResponseEntity<String>(HttpStatus.OK);
				}
				
				// *****************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/bono/historico_canjes.do")
				public ModelAndView showHistoricoCanjeBonos(
						@RequestParam(value = "idBono", required = false, defaultValue = "") String idBono,
						HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					String datos="";
					datos = "<int>"+idBono+"</int>";
					
					model.addObject("idBono", idBono);
					model.addObject("historico_canje_bono", Tools.callServiceXML(request,"obtenerCanjebonos.action?servicio=obtenerCanjebonos&xml=" + datos));
					model.setViewName("app.ventareserva.ventareserva.busquedas.bonos.historico_canje_bono");
					
					return model;
				}
				// *****************************************************************************************************

				
				@RequestMapping("/ajax/ventareserva/busqueda/save_formas_de_pago_venta.do")
				public ResponseEntity<String> saveFormasDePagoVenta(
							@RequestParam(value = "xmlPagos", required = false, defaultValue = "") String xml,																		   
							HttpServletRequest request) throws Exception {

					
					String xmlResult = Tools.callServiceXML(request, "actualizarLineadetallesVenta.action?servicio=actualizarLineadetallesVenta&xml="+URLEncoder.encode(xml, "UTF-8"));
					
					if (xmlResult.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xmlResult), HttpStatus.BAD_REQUEST);
					}
					return new ResponseEntity<String>(HttpStatus.OK);
				}
				// *****************************************************************************************************
				
				@RequestMapping("ajax/ventareserva/busqueda/show_disponibles_bono_canje.do")
				public ModelAndView showDisponiblesBonoCanje(
						@RequestParam(value = "idproducto", required = false, defaultValue = "") String idProducto,
						@RequestParam(value = "idcliente", required = false, defaultValue = "") String idCliente,
						@RequestParam(value = "idtipoventa", required = false, defaultValue = "4") String idTipoVenta,
						@RequestParam(value = "producto", required = false, defaultValue = "") String producto,
						@RequestParam(value = "fecha", required = false, defaultValue = "") String fecha,
						@RequestParam(value = "button", required = false, defaultValue = "") String buttonAdd,
						@RequestParam(value = "list", required = false, defaultValue = "") String list,
						@RequestParam(value = "edicion", required = false, defaultValue = "0") String edicion,
						HttpServletRequest request) throws Exception {
				
					ModelAndView model = new ModelAndView();
					
					model.setViewName("app.ventareserva.ventareserva.busquedas.disponibles.bono.canje");
					
					model.addObject("buttonAdd",buttonAdd);
					model.addObject("list",list);
					model.addObject("idProducto",idProducto);
					model.addObject("idCliente",idCliente);
					model.addObject("idtipoventa",idTipoVenta);
					model.addObject("producto",producto);
					model.addObject("fecha",fecha);
					model.addObject("edicion",edicion);

					String xml_sesiones= Tools.callServiceXML(request, "obtenerSesionesPorProductoYFecha.action?servicio=obtenerSesionesPorProductoYFecha&fecha="+URLEncoder.encode(fecha,"UTF-8")+"&idproducto="+idProducto+"&map="+URLEncoder.encode("<map />","UTF-8")); 
					JSONObject json_sesiones = XML.toJSONObject(xml_sesiones);
					if (json_sesiones.has("error")) throw new Exception(json_sesiones.getString("error"));

					model.addObject("disponibles_datos_sesiones",xml_sesiones);
					model.addObject("disponibles_datos_sesiones_json",json_sesiones.toString(4));

					return model;
				}
				
				// ***************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/bono/canjear_bono.do")
				public ResponseEntity<?> ventareservaBonoCanjear(
						@RequestParam(value = "xmlCanje", required = false, defaultValue = "") String xmlCanje, 
						HttpServletRequest request) throws Exception {

				     String json="";
				       try {
			               		json = Tools.callServiceJSON(request, "canjearBonos.action?servicio=canjearBonos&xml="+URLEncoder.encode(xmlCanje,"UTF-8"));
			               		
				       	   } catch (Exception ex) {
			               throw new AjaxException(ex.getMessage());
				        }		
					
					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}
                //**************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/venta/showLineas.do")
				public ModelAndView showLineas(
						@RequestParam(value = "idVenta", required = false, defaultValue = "") String idVenta,
						HttpServletRequest request) throws Exception {
					ModelAndView model = new ModelAndView();
					
					model.addObject("ventareserva_productos_principales", Tools.callServiceXML(request, "obtenerListadoProductosPrincipales.action?servicio=obtenerListadoProductosPrincipales"));
					
					String venta_perfiles= Tools.callServiceXML(request, "obtenerVentaPorIdConPerfiles.action?servicio=obtenerVentaPorId&xml=<int>"+idVenta+"</int>"); 
					JSONObject json_venta_perfiles = XML.toJSONObject(venta_perfiles);
					if (json_venta_perfiles.has("error")) throw new Exception(json_venta_perfiles.getString("error"));

					model.addObject("ventareserva_venta_perfiles_json",json_venta_perfiles.toString(4));

					
					
					model.setViewName("app.ventareserva.ventareserva.busquedas.ventareserva.venta.show_lineas");
					

					return model;
				}
				//******************************************************************
			
				@RequestMapping("/ajax/ventareserva/busqueda/venta/show_edit_sesion.do")
				public ModelAndView showEditSesion(
						@RequestParam(value = "idproducto", required = false, defaultValue = "-1") String idProducto, 
						@RequestParam(value = "idtarifa", required = false, defaultValue = "-1") String idTarifa, 
						@RequestParam(value = "idtipoventa", required = false, defaultValue = "4") String idTipoVenta,
						@RequestParam(value = "idcanal", required = false, defaultValue = "2") String idCanal,
						@RequestParam(value = "fecha", required = false, defaultValue = "") String fecha,
						@RequestParam(value = "idcliente", required = false, defaultValue = "") String idCliente,
						@RequestParam(value = "idsesionlinea", required = false, defaultValue = "") String idsesionlinea,
						@RequestParam(value = "idlineadetalle", required = false, defaultValue = "") String idlineadetalle,						
						@RequestParam(value = "idxnumero", required = false, defaultValue = "") String idxNumero, 
						@RequestParam(value = "idxunitario", required = false, defaultValue = "") String idxUnitario,
						@RequestParam(value = "idxtotal", required = false, defaultValue = "") String idxTotal,
						@RequestParam(value = "idxbono", required = false, defaultValue = "") String idxBono, 
						@RequestParam(value = "idxtarifa_id", required = false, defaultValue = "") String idxTarifa_id,
						@RequestParam(value = "idxtarifa_txt", required = false, defaultValue = "") String idxTarifa_txt,
						@RequestParam(value = "idxdescuento_id", required = false, defaultValue = "") String idxDescuento_id,
						@RequestParam(value = "idxdescuento_txt", required = false, defaultValue = "") String idxDescuento_txt,
						@RequestParam(value = "idxperfil", required = false, defaultValue = "") String idxPerfil, 
						@RequestParam(value = "button", required = false, defaultValue = "") String buttonEdit,
						@RequestParam(value = "list", required = false, defaultValue = "") String list,
						//@RequestParam(value = "xml_perfiles_tarifas", required = false, defaultValue = "") String xml_perfiles,
						
						HttpServletRequest request) throws Exception {
				
					ModelAndView model = new ModelAndView();

					model.setViewName("app.ventareserva.ventareserva.busquedas.ventareserva.venta.edit_sesion");

					model.addObject("buttonEdit",buttonEdit);
					model.addObject("list",list);
					model.addObject("idProducto",idProducto);
					model.addObject("idCliente",idCliente);
					model.addObject("idTipoVentaLineas",idTipoVenta);
					model.addObject("idlineadetalle",idlineadetalle);					
					model.addObject("idTarifa",idTarifa);
					model.addObject("idTarifa",idTipoVenta);
					model.addObject("idxNumero",idxNumero);
					model.addObject("idxUnitario",idxUnitario);
					model.addObject("idxTotal",idxTotal);
					model.addObject("idxBono",idxBono);
					model.addObject("idxTarifa_id",idxTarifa_id);
					model.addObject("idxTarifa_txt",idxTarifa_txt);
					model.addObject("idxDescuento_id",idxDescuento_id);
					model.addObject("idxDescuento_txt",idxDescuento_txt);
					model.addObject("idxPerfil",idxPerfil);
				
										
					model.addObject("ventareserva_selector_descuentos", Tools.callServiceXML(request, "obtenerListadoDescuentosAplicables.action?servicio=obtenerListadoDescuentosAplicables&idtarifa="+idTarifa+"&idproducto="+idProducto+"&idcanal="+idCanal));
					return model;
				}
				
				
				///***************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/venta/editarPago.do")					
				public ModelAndView editarPago(
						@RequestParam(value = "id", required = false, defaultValue = "") String id,
						@RequestParam(value = "importetotalventa", required = false, defaultValue = "") String importetotalventa,
						@RequestParam(value = "importetotalpagado", required = false, defaultValue = "") String importetotalpagado,
						@RequestParam(value = "idcanal", required = false, defaultValue = "") String idcanal,
						@RequestParam(value = "nombretabla", required = false, defaultValue = "") String nombretabla,
						HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					
					model.addObject("motivosModificacion", Tools.callServiceXML(request, "obtenerListadoMotivosModificacion.action?servicio=obtenerListadoMotivosModificacion"));
					model.addObject("selectorFormasdePagoPorCanal", Tools.callServiceXML(request, "obtenerListadoFormasPagoPorCanal.action?servicio=obtenerListadoFormasPagoPorCanal&xml=<canal><idcanal>"+idcanal+"</idcanal></canal>"));
					model.addObject("importesParciales", Tools.callServiceXML(request, "obtenerImportesParcialesPorVenta.action?servicio=obtenerImportesParcialesPorVenta&xml=<int>"+id+"</int>"));
									
					model.addObject("id", id);
					
					float total= Float.parseFloat(importetotalventa);
					String importeTotal =String.format("%.2f", total);
					float pagado= Float.parseFloat(importetotalpagado);
					String importePagado= String.format("%.2f", pagado);
					float importeDiferencia = total-pagado;
					String  diferencia= String.format("%.2f", importeDiferencia);
					
					model.addObject("importeTotal",importeTotal);
					model.addObject("importePagado",importePagado);
					model.addObject("diferencia",diferencia);
					model.addObject("nombretabla",nombretabla);
					
					model.setViewName("app.ventareserva.ventareserva.busquedas.ventareserva.venta.edit_venta_pagos");
					
					return model;
				}
				//*****************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/ventareservas/actualizarLineasDetalle.do")
				public ResponseEntity<?> venta_actualizarLineasDetalle(
							@RequestParam(value = "xml_lineas_detalle", required = false, defaultValue = "") String xml_lineas,																		   
			 				HttpServletRequest request) throws Exception {

					String json = "";

					try {
						json = Tools.callServiceJSON(request, "actualizarLineadetallesVenta.action?servicio=actualizarLineadetallesVentaInformandoEntradasCanceladas&xml=" + URLEncoder.encode(xml_lineas, "UTF-8") );
					} catch (Exception ex) {
						throw new AjaxException(ex.getMessage());
					}

					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				
				}
				
				//*****************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/reserva/showLineas.do")
				public ModelAndView showLineas_reserva(
						HttpServletRequest request) throws Exception {
					ModelAndView model = new ModelAndView();
					model.addObject("motivosModificacion", Tools.callServiceXML(request, "obtenerListadoMotivosModificacion.action?servicio=obtenerListadoMotivosModificacion"));
					model.addObject("ventareserva_productos_principales", Tools.callServiceXML(request, "obtenerListadoProductosPrincipales.action?servicio=obtenerListadoProductosPrincipales"));
													
					model.setViewName("app.ventareserva.ventareserva.busquedas.ventareserva.reserva.show_lineas");
				
					return model;
				}
				//******************************************************** 
	
				@RequestMapping("/ajax/ventareserva/busqueda/reserva/editarPago.do")					
				public ModelAndView editarPago_reserva(
						@RequestParam(value = "id", required = false, defaultValue = "") String id,
						HttpServletRequest request) throws Exception {
					
					ModelAndView model = new ModelAndView();
					model.addObject("motivosModificacion", Tools.callServiceXML(request, "obtenerListadoMotivosModificacion.action?servicio=obtenerListadoMotivosModificacion"));
					model.addObject("id", id);
					model.setViewName("app.ventareserva.ventareserva.busquedas.ventareserva.venta.edit_reserva_pagos");
					
					return model;
				}
				// *****************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/modificar_lineasdetalle_reserva.do")
				public ResponseEntity<String> modificar_lineasdetalle_reserva(
							@RequestParam(value = "xmlModificacion", required = false, defaultValue = "") String xml,																		   
							HttpServletRequest request) throws Exception {

					
					String xmlResult = Tools.callServiceXML(request, "actualizarLineadetallesReserva.action?servicio=actualizarLineadetallesReserva&xml="+URLEncoder.encode(xml, "UTF-8"));
					
					if (xmlResult.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xmlResult), HttpStatus.BAD_REQUEST);
					}
					return new ResponseEntity<String>(HttpStatus.OK);
				}
				
				// *****************************************************************************************************
				@RequestMapping("/ajax/venta/actualizarUsosTabla.do")
				public ResponseEntity<String> actualiizar_usos_tabla(
							@RequestParam(value = "xml", required = false, defaultValue = "") String xml,																		   
							HttpServletRequest request) throws Exception {

					
					String xmlResult = Tools.callServiceXML(request, "actualizarUsosEntradas.action?servicio=actualizarUsosEntradas&xml="+URLEncoder.encode(xml, "UTF-8"));
					
					if (xmlResult.startsWith("<error>")) {
						return new ResponseEntity<String>(Tools.extractXMLError(xmlResult), HttpStatus.BAD_REQUEST);
					}
					return new ResponseEntity<String>(HttpStatus.OK);
				}
				
				
				
				
				// *****************************************************************************************************
				@RequestMapping("/venta/ventareserva/busqueda/ventareserva/editar_abono.do")
				public ModelAndView abono_editar(
						@RequestParam(value = "id", required = false, defaultValue = "") String id, HttpServletRequest request)
						throws Exception {

					ModelAndView model = new ModelAndView();
					
					String editar_venta = Tools.callServiceXML(request,"obtenerVentaPorId.action?servicio=obtenerVentaPorId&xml=<int>" + id +"</int>");
					model.addObject("editar_venta", editar_venta);
					log.error("Lo que llega del servicio obtenerVentaPorId: "+editar_venta);
					//Ponemos tambien el objeto en json
					JSONObject json = XML.toJSONObject(editar_venta);
					if (json.has("error")) throw new Exception(json.getString("error"));
						model.addObject("editar_venta_json", json);
					
					model.addObject("listado_estados_venta", Tools.callServiceXML(request,"obtenerListadoEstadosVenta.action?servicio=obtenerListadoEstadosVenta"));
					model.addObject("entradas_anuladas", Tools.callServiceXML(request,"obtenerEntradasAnuladasPorIdVenta.action?servicio=obtenerEntradasAnuladasPorIdVenta&xml=<int>" + id +"</int>"));
					model.addObject("id", id);
					model.setViewName("app.ventareserva.ventareserva.busquedas.abono.editar_venta");
					

					return model;
				}
				
				
				//******************************************************************
				
				@RequestMapping("/ajax/ventareserva/busqueda/venta/show_edit_sesion_abono.do")
				public ModelAndView showEditSesionAbono(
						@RequestParam(value = "idproducto", required = false, defaultValue = "-1") String idProducto, 
						@RequestParam(value = "idtarifa", required = false, defaultValue = "-1") String idTarifa, 
						@RequestParam(value = "idtipoventa", required = false, defaultValue = "4") String idTipoVenta,
						@RequestParam(value = "idcanal", required = false, defaultValue = "2") String idCanal,
						@RequestParam(value = "fecha", required = false, defaultValue = "") String fecha,
						@RequestParam(value = "idcliente", required = false, defaultValue = "") String idCliente,
						@RequestParam(value = "idsesionlinea", required = false, defaultValue = "") String idsesionlinea,
						@RequestParam(value = "idlineadetalle", required = false, defaultValue = "") String idlineadetalle,						
						@RequestParam(value = "idxnumero", required = false, defaultValue = "") String idxNumero, 
						@RequestParam(value = "idxunitario", required = false, defaultValue = "") String idxUnitario,
						@RequestParam(value = "idxtotal", required = false, defaultValue = "") String idxTotal,
						@RequestParam(value = "idxbono", required = false, defaultValue = "") String idxBono, 
						@RequestParam(value = "idxtarifa_id", required = false, defaultValue = "") String idxTarifa_id,
						@RequestParam(value = "idxtarifa_txt", required = false, defaultValue = "") String idxTarifa_txt,
						@RequestParam(value = "idxdescuento_id", required = false, defaultValue = "") String idxDescuento_id,
						@RequestParam(value = "idxdescuento_txt", required = false, defaultValue = "") String idxDescuento_txt,
						@RequestParam(value = "idxperfil", required = false, defaultValue = "") String idxPerfil, 
						@RequestParam(value = "button", required = false, defaultValue = "") String buttonEdit,
						@RequestParam(value = "list", required = false, defaultValue = "") String list,									
						HttpServletRequest request) throws Exception {
				 

					ModelAndView model = new ModelAndView();

					model.setViewName("app.ventareserva.ventareserva.busquedas.ventareserva.venta.edit_sesion_abono");

					model.addObject("buttonEdit",buttonEdit);
					model.addObject("list",list);
					model.addObject("idProducto",idProducto);
					model.addObject("idCliente",idCliente);
					model.addObject("idTipoVentaLineas",idTipoVenta);
					model.addObject("idlineadetalle",idlineadetalle);					
					model.addObject("idTarifa",idTarifa);
					model.addObject("idTarifa",idTipoVenta);
					model.addObject("idxNumero",idxNumero);
					model.addObject("idxUnitario",idxUnitario);
					model.addObject("idxTotal",idxTotal);
					model.addObject("idxBono",idxBono);
					model.addObject("idxTarifa_id",idxTarifa_id);
					model.addObject("idxTarifa_txt",idxTarifa_txt);
					model.addObject("idxDescuento_id",idxDescuento_id);
					model.addObject("idxDescuento_txt",idxDescuento_txt);
					model.addObject("idxPerfil",idxPerfil);
				
										
					model.addObject("ventareserva_selector_descuentos", Tools.callServiceXML(request, "obtenerListadoDescuentosAplicables.action?servicio=obtenerListadoDescuentosAplicables&idtarifa="+idTarifa+"&idproducto="+idProducto+"&idcanal="+idCanal));
																								
					return model;
				}
								

				//************************************************************************************************************
				@RequestMapping(value = "/ajax/ventareserva/busqueda/ventareservas/list_entrada_por_venta.do", method = RequestMethod.POST)
				public ResponseEntity<?> listEntradasPorVenta(
						@RequestParam(value = "idVenta", required = false, defaultValue = "") String idVenta,
						@RequestParam(value = "idLineaDetalleUsuos", required = false, defaultValue = "") String idLineaDetalleUsuos,
						@RequestParam(value = "start", required = false, defaultValue = "0") String start,
			      		HttpServletRequest request) throws Exception {

					
					String json="";
				       try {
				    	   
				    	   if(idLineaDetalleUsuos.equalsIgnoreCase(""))
			               		json = Tools.callServiceJSON(request, "obtenerEntradasPorVenta.action?servicio=obtenerEntradasPorVenta&xml=<dto><idventa>"+idVenta+"</idventa><numeroprimerregistro>"+start+"</numeroprimerregistro></dto>");
				    	   else
				    		   json = Tools.callServiceJSON(request, "obtenerEntradasPorLD.action?servicio=obtenerEntradasPorLD&xml=<dto><listaIds><int>"+idLineaDetalleUsuos+"</int></listaIds><numeroprimerregistro>"+start+"</numeroprimerregistro></dto>");
				       	   
				       		} catch (Exception ex) {
			               throw new AjaxException(ex.getMessage());
				        }		
					
					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}
				
				//************************************************************************************************************
				@RequestMapping(value = "/ajax/ventareserva/busqueda/ventareservas/list_entrada_por_reserva.do", method = RequestMethod.POST)
				public ResponseEntity<?> listEntradasPorReserva(
						@RequestParam(value = "idReserva", required = false, defaultValue = "") String idReserva,
						@RequestParam(value = "start", required = false, defaultValue = "0") String start,
			      		HttpServletRequest request) throws Exception {

					
					String json="";
				       try {
			               		json = Tools.callServiceJSON(request, "obtenerEntradasPorReserva.action?servicio=obtenerEntradasPorReserva&xml=<dto><idreserva>"+idReserva+"</idreserva><numeroprimerregistro>"+start+"</numeroprimerregistro></dto>");
			               		
				       	   } catch (Exception ex) {
			               throw new AjaxException(ex.getMessage());
				        }		
					
					HttpHeaders responseHeaders = new HttpHeaders();
					responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

					return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);
				}		
				
				//*****************************************************************************************************
				@RequestMapping("/ajax/ventareserva/busqueda/ventareservas/show_anular_reserva.do")
				public ModelAndView AnularReserva(@RequestParam(value = "id", required = false, defaultValue = "") String idReserva, 
											HttpServletRequest request) throws Exception {

					
					ModelAndView model = new ModelAndView();
					model.setViewName("app.ventareserva.ventareserva.busquedas.venta_reserva.anular.reserva");
					model.addObject("idReserva", idReserva);					
					
					model.addObject("motivosModificacion", Tools.callServiceXML(request, "obtenerListadoMotivosModificacion.action?servicio=obtenerListadoMotivosModificacion"));
					return model;
				}

			
}




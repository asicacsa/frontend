// -----------------------------------------------------------------------
// TODO: 
//
// Al Editar Caja existen un enlace a ventas que hay que investigar.
//
//	- Ver ventas: /getBuscarVenta.action?servicio=getXMLModel
//
// -----------------------------------------------------------------------

package es.cac.colossus.frontend.web.controllers;

import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import es.cac.colossus.frontend.utils.Tools;

@Controller
public class CajasController {
	
	@Autowired
	private MessageSource messageSource;	
	//*****************************************************************************************************
	@RequestMapping("ventareserva/operacionescaja/operacionescaja.do")
	public ModelAndView showOpcaja(HttpServletRequest request) throws Exception {
				
		ModelAndView model = new ModelAndView();
		String idUsuario=request.getSession().getAttribute("idUsuario").toString();
		
    	model.setViewName("app.ventareserva.operacionescaja.operacionescaja");
		model.addObject("title", messageSource.getMessage("ventareserva.operacionescaja.title", null, null));
		model.addObject("menu",Tools.getUserMainMenu(request));			

		model.addObject("operacionescaja_selector_canales",Tools.callServiceXML(request, "obtenerListadoCanales.action?servicio=obtenerListadoCanales"));
		model.addObject("operacionescaja_selector_usuarios",Tools.callServiceXML(request, "obtenerListadoUsuarios.action?servicio=obtenerListadoUsuarios"));
		String urlgestor= Tools.callServiceXMLSinReemplazo(request, "abrirInformeUsuario.action?servicio=abrirInformeUsuario&xml="+URLEncoder.encode("<java.lang.Integer>"+idUsuario+"</java.lang.Integer>","UTF-8")); 
    	model.addObject("operacionescaja_urlgestor",urlgestor.replaceAll("<[^>]*>", ""));
    			
    	return model;
	}
	
	// ***************************************************************************************************
	@RequestMapping(value = "ajax/ventareserva/operacionescaja/list_usuarios_canal.do", method = RequestMethod.POST)
	public ResponseEntity<?> listUsuariosCanal(@RequestParam(value = "data", required = true, defaultValue = "-1") String data,
									   HttpServletRequest request) throws Exception {

		String json="";
		if (data.equals("-1")) {
			  json=Tools.callServiceJSON(request, "obtenerListadoUsuarios.action?servicio=obtenerListadoUsuarios");
		}
		else {
			  json=Tools.callServiceJSON(request, "obtenerListadoUsuariosCanal.action?servicio=obtenerListadoUsuariosCanal&xml=<int>"+data+"</int>");
		}
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
	}
	
	// ***************************************************************************************************
	@RequestMapping(value = "ajax/ventareserva/operacionescaja/list_cajas.do", method = RequestMethod.POST)
	public ResponseEntity<?> operacionescajaListCajas(@RequestParam(value = "idusuario", required = false, defaultValue = "") String idusuario,
									   @RequestParam(value = "idcanal", required = false, defaultValue = "") String idcanal, 
									   @RequestParam(value = "fechaAperturaIni", required = false, defaultValue = "") String fechaAperturaIni,
									   @RequestParam(value = "fechaAperturaFin", required = false, defaultValue = "") String fechaAperturaFin,
									   @RequestParam(value = "numCajaIni", required = false, defaultValue = "") String numCajaIni,
									   @RequestParam(value = "numCajaFin", required = false, defaultValue = "") String numCajaFin,
									   @RequestParam(value = "start", required = false, defaultValue = "0") String startRecord,
									   @RequestParam(value = "length", required = false, defaultValue = "0") String maxLength,
									   HttpServletRequest request) throws Exception {

		String json=Tools.callServiceJSON(request, "postBuscarCaja.action?servicio=buscarCaja&xml="+URLEncoder.encode("<Buscarcajaparam><idusuario>"+idusuario+"</idusuario><idcanal>"+idcanal+"</idcanal><fechaAperturaIni>"+fechaAperturaIni+"</fechaAperturaIni><fechaAperturaFin>"+fechaAperturaFin+"</fechaAperturaFin><numCajaIni>"+numCajaIni+"</numCajaIni><numCajaFin>"+numCajaFin+"</numCajaFin><numeroprimerregistro>"+startRecord+"</numeroprimerregistro><maxResultados>"+maxLength+"</maxResultados></Buscarcajaparam>","UTF-8"));
				
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
	}

	// ***************************************************************************************************
	@RequestMapping(value = "ajax/ventareserva/operacionescaja/list_operaciones.do", method = RequestMethod.POST)
	public ResponseEntity<?> operacionescajaEditarCaja(@RequestParam(value = "data", required = false, defaultValue = "") String data,
									   HttpServletRequest request) throws Exception {

		String json="";
		if (data.length()>0) {
		  json=Tools.callServiceJSON(request, "obtenerCajaPorId.action?servicio=obtenerCajaPorId&xml="+URLEncoder.encode("<int>"+data+"</int>","UTF-8"));
		}
				
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
	}
	
	// ***************************************************************************************************
	@RequestMapping(value = "ajax/ventareserva/operacionescaja/darvistobueno.do", method = RequestMethod.POST)
	public ResponseEntity<?> operacionescajaDarVistoBueno(@RequestParam(value = "data", required = false, defaultValue = "") String data,
									   HttpServletRequest request) throws Exception {

		String json="";
		if (data.length()>0) {
		  json=Tools.callServiceJSON(request, "darVistoBueno.action?servicio=darVistoBueno&xml="+URLEncoder.encode("<list><OperacionVistoBueno><caja><idcaja>"+data+"</idcaja></caja></OperacionVistoBueno></list>","UTF-8"));
		}
				
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
	}
	
	// *****************************************************************************************************
	@RequestMapping("ajax/ventareserva/operacionescaja/cierre_caja.do")
	public ModelAndView operacionescajaCerrarCaja(@RequestParam(value = "id", required = false, defaultValue = "") String idCaja, 
								HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();
		model.setViewName("app.ventareserva.operacionescaja.cerrarcaja.dialog.cierre_caja");
		model.addObject("idCaja", idCaja);
		
		return model;
	}
	
	// ***************************************************************************************************
	@RequestMapping(value = "ajax/ventareserva/operacionescaja/cerrar_caja.do", method = RequestMethod.POST)
	public ResponseEntity<?> operacionescajaCerrar(@RequestParam(value = "idcaja", required = false, defaultValue = "") String idcaja,
			   						@RequestParam(value = "importe", required = false, defaultValue = "") String importe, 
		   							HttpServletRequest request) throws Exception {

		String json=Tools.callServiceJSON(request, "postOperacionesCaja.action?caja=<Caja><idcaja>"+idcaja+"</idcaja></Caja>&servicio=cerrarCaja&xml="+importe);
				
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
	}	
	
	// *****************************************************************************************************
	@RequestMapping("ajax/ventareserva/operacionescaja/edicion_op_caja.do")
	public ModelAndView operacionescajaEdicionOpCaja(@RequestParam(value = "id", required = false, defaultValue = "") String idopcaja, 
								HttpServletRequest request) throws Exception {

		ModelAndView model = new ModelAndView();
		model.setViewName("app.ventareserva.operacionescaja.editaropcaja.dialog.edicion_op_caja");
		model.addObject("idOperacionCaja", idopcaja);
		
		return model;
	}
	
	// ***************************************************************************************************
	@RequestMapping(value = "ajax/ventareserva/operacionescaja/editar_op_caja.do", method = RequestMethod.POST)
	public ResponseEntity<?> operacionescajaEditarOpCaja(@RequestParam(value = "idopcaja", required = false, defaultValue = "") String idopcaja,
			   						@RequestParam(value = "importe", required = false, defaultValue = "") String importe, 
		   							HttpServletRequest request) throws Exception {

		String json=Tools.callServiceJSON(request,"postOperacionesCaja.action?caja=<Operacioncaja><idoperacioncaja>"+idopcaja+"</idoperacioncaja></Operacioncaja>&servicio=rectificarOperacionCaja&xml="+importe);
				
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
	}	

	//*****************************************************************************************************
	//OPERACIÓN CAJA
	
	@RequestMapping("ventareserva/operacionescaja/abrircaja.do")
	public ModelAndView showAbrirCaja(HttpServletRequest request) throws Exception {
				
		ModelAndView model = new ModelAndView();
    	model.setViewName("app.ventareserva.operacionescaja.abrircaja");
		model.addObject("title", messageSource.getMessage("ventareserva.operacionescaja.abrircaja.title", null, null));
		model.addObject("menu",Tools.getUserMainMenu(request));			

    	return model;
	}

	// ***************************************************************************************************
	@RequestMapping(value = "ajax/ventareserva/operacionescaja/obtener_caja_abierta.do", method = RequestMethod.POST)
	public ResponseEntity<?> operacionescajaObtenerCajaAbierta(HttpServletRequest request) throws Exception {

		String idUsuario=request.getSession().getAttribute("idUsuario").toString();
		String json=Tools.callServiceJSON(request,"obtenerCajaAbiertaUsuario.action?servicio=obtenerCajaAbiertaUsuario&xml="+URLEncoder.encode("<java.lang.Integer>"+idUsuario+"</java.lang.Integer>","UTF-8"));
				
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
	}	
	
	// ***************************************************************************************************
	@RequestMapping(value = "ajax/ventareserva/operacionescaja/apertura_caja.do", method = RequestMethod.POST)
	public ResponseEntity<?> operacionescajaAbrirCaja(
				@RequestParam(value = "importe", required = false, defaultValue = "") String importe, 
				HttpServletRequest request) throws Exception {

		String json=Tools.callServiceJSON(request,"/postOperacionesCaja.action?servicio=abrirCaja&xml=" + importe);
				
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");

		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
	}	

	// ***************************************************************************************************
	@RequestMapping("ventareserva/operacionescaja/cerrarcaja.do")
	public ModelAndView showCerrarCaja(HttpServletRequest request) throws Exception {
				
		ModelAndView model = new ModelAndView();
    	model.setViewName("app.ventareserva.operacionescaja.cerrarcaja");
		model.addObject("title", messageSource.getMessage("ventareserva.operacionescaja.cerrarcaja.title", null, null));
		model.addObject("menu",Tools.getUserMainMenu(request));			

    	return model;
	}

	// ***************************************************************************************************
	@RequestMapping("ventareserva/operacionescaja/aportacioncaja.do")
	public ModelAndView showAportacionCaja(HttpServletRequest request) throws Exception {
				
		ModelAndView model = new ModelAndView();
    	model.setViewName("app.ventareserva.operacionescaja.aportacioncaja");
		model.addObject("title", messageSource.getMessage("ventareserva.operacionescaja.aportacioncaja.title", null, null));
		model.addObject("menu",Tools.getUserMainMenu(request));			

    	return model;
	}

	// ***************************************************************************************************
	@RequestMapping(value = "ajax/ventareserva/operacionescaja/aportar_caja.do", method = RequestMethod.POST)
	public ResponseEntity<?> operacionescajaAportar(@RequestParam(value = "idcaja", required = false, defaultValue = "") String idcaja,
			   						@RequestParam(value = "importe", required = false, defaultValue = "") String importe, 
		   							HttpServletRequest request) throws Exception {

		String json=Tools.callServiceJSON(request, "postOperacionesCaja.action?caja=<Caja><idcaja>"+idcaja+"</idcaja></Caja>&servicio=realizarAportacionParcial&xml="+importe);
				
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
	}	

	// ***************************************************************************************************
	@RequestMapping("ventareserva/operacionescaja/retiradacaja.do")
	public ModelAndView showRetiradaCaja(HttpServletRequest request) throws Exception {
				
		ModelAndView model = new ModelAndView();
    	model.setViewName("app.ventareserva.operacionescaja.retiradacaja");
		model.addObject("title", messageSource.getMessage("ventareserva.operacionescaja.retiradacaja.title", null, null));
		model.addObject("menu",Tools.getUserMainMenu(request));			

    	return model;
	}

	// ***************************************************************************************************
	@RequestMapping(value = "ajax/ventareserva/operacionescaja/retirar_caja.do", method = RequestMethod.POST)
	public ResponseEntity<?> operacionescajaRetirar(@RequestParam(value = "idcaja", required = false, defaultValue = "") String idcaja,
			   						@RequestParam(value = "importe", required = false, defaultValue = "") String importe, 
		   							HttpServletRequest request) throws Exception {

		String json=Tools.callServiceJSON(request, "postOperacionesCaja.action?caja=<Caja><idcaja>"+idcaja+"</idcaja></Caja>&servicio=realizarRetiradaParcial&xml="+importe);
				
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
	}	
	// ***************************************************************************************************
	@RequestMapping("ventareserva/operacionescaja/list_ventas_caja.do")
	public ModelAndView listVentasCaja(
			@RequestParam(value = "idcaja", required = false, defaultValue = "") String idcaja,
			HttpServletRequest request) throws Exception {
				
		ModelAndView model = new ModelAndView();
		String idUsuario=request.getSession().getAttribute("idUsuario").toString();
		
    	model.setViewName("app.ventareserva.operacionescaja.list_ventas_caja");
					

		model.addObject("selector_formas_pago",Tools.callServiceXML(request, "obtenerListadoFormasPago.action?servicio=obtenerListadoFormasPago"));
		model.addObject("idcaja",idcaja);
	
		return model;
	}
	
	
	// ***************************************************************************************************
	@RequestMapping(value = "ajax/ventareserva/operacionescaja/list_ventas_caja.do", method = RequestMethod.POST)
	public ResponseEntity<?> operacionescajaListVentasCaja(@RequestParam(value = "idusuario", required = false, defaultValue = "") String idusuario,
									   @RequestParam(value = "idcaja", required = false, defaultValue = "") String idcaja, 
									   @RequestParam(value = "referenciaventaini", required = false, defaultValue = "") String referenciaventaini,
									   @RequestParam(value = "referenciaventafin", required = false, defaultValue = "") String referenciaventafin,
									   @RequestParam(value = "idformapago", required = false, defaultValue = "") String idformapago,
									   @RequestParam(value = "start", required = false, defaultValue = "0") String startRecord,
									   @RequestParam(value = "length", required = false, defaultValue = "0") String maxLength,
									   HttpServletRequest request) throws Exception {

		
		String xml="<Buscarventasporcajaparam>";
		xml+="<idcaja>"+idcaja+"</idcaja>";
		xml+="<referenciaventaini>"+referenciaventaini+"</referenciaventaini>";
		xml+="<referenciaventafin>"+referenciaventafin+"</referenciaventafin>";
		xml+="<idformapago>"+idformapago+"</idformapago>";
		xml+="<numeroprimerregistro>"+startRecord+"</numeroprimerregistro>";
		xml+="</Buscarventasporcajaparam>";
		
		String json=Tools.callServiceJSON(request, "postBuscarVenta.action?servicio=buscarVentasPorCaja&xml="+URLEncoder.encode(xml,"UTF-8"));
				
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Type", "text/json; charset=ISO-8859-1");
		
		return new ResponseEntity<String>(json, responseHeaders, HttpStatus.OK);	
	}
	//*****************************************************************************************************
		@RequestMapping("ajax/ventareserva/operacionescaja/show_anular_venta.do")
		public ModelAndView operacionescajaShowAnularVenta(@RequestParam(value = "id", required = false, defaultValue = "") String idVenta, 
									@RequestParam(value = "parcial", required = false, defaultValue = "0") String parcial,
									HttpServletRequest request) throws Exception {

			
			ModelAndView model = new ModelAndView();
			model.setViewName("app.ventareserva.operacionescaja.dialog.ventas.anular_venta");
			model.addObject("idVenta", idVenta);
			model.addObject("parcial", parcial);
			
			model.addObject("motivosModificacion", Tools.callServiceXML(request, "obtenerListadoMotivosModificacion.action?servicio=obtenerListadoMotivosModificacion"));
			model.addObject("selectorFormasdePago", Tools.callServiceXML(request, "obtenerListadoFormasPago.action?servicio=obtenerListadoFormasPago"));
			model.addObject("importesParciales", Tools.callServiceXML(request, "obtenerVentaPorIdReducido.action?servicio=obtenerVentaPorIdReducido&xml=<int>"+idVenta+"</int>"));
			
			
			return model;
		}
  //*****************************************************************************************************
		@RequestMapping("/ajax/ventareserva/operacionescaja/show_formas_de_pago_venta.do")
		public ModelAndView showFormasDePagoVenta(		
				@RequestParam(value = "idVenta", required = false, defaultValue = "") String idVenta,
				HttpServletRequest request) throws Exception {
			
			
			ModelAndView model = new ModelAndView();
			
			model.addObject("selector_motivos", Tools.callServiceXML(request, "obtenerListadoMotivosModificacion.action?servicio=obtenerListadoMotivosModificacion"));
			model.addObject("selector_formas_pago", Tools.callServiceXML(request, "obtenerListadoFormasPago.action?servicio=obtenerListadoFormasPago"));
			model.addObject("datos_venta",Tools.callServiceJSON(request,  "obtenerImportesParcialesPorVenta.action?servicio=obtenerImportesParcialesPorVenta&xml=<int>"+idVenta+"</int>"));
			model.setViewName("app.ventareserva.operacionescaja.dialog.ventas.anular_venta.show_formas_de_pago_venta");
			
			return model;
		}
		
		//*****************************************************************************************************
		@RequestMapping("ajax/ventareserva/operacionescaja/show_anular_lineas.do")
		public ModelAndView operacionescajaShowAnularLineas(@RequestParam(value = "id", required = false, defaultValue = "") String idVenta, 
									HttpServletRequest request) throws Exception {

			
			ModelAndView model = new ModelAndView();
			model.setViewName("app.ventareserva.operacionescaja.dialog.ventas.anular_lineas");
			model.addObject("idVenta", idVenta);
					
			
			model.addObject("lineasDetalle", Tools.callServiceJSON(request, "obtenerLineadetallesPorIdsVenta.action?servicio=obtenerLineadetallesPorIdsVenta&xml=<int>"+idVenta+"</int>"));
					
			
			return model;
		}
		
}

package es.cac.colossus.frontend.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

public class Tools {

	private static final Log log = LogFactory.getLog(Tools.class);

	public static final String CAC_ERRORCODE = "CAC-ERRORCODE";
	public static final String CAC_ERRORMSG = "CAC-ERRORMSG";
	public static final String CAC_ERRORDETAIL = "CAC-ERRORDETAIL";
	public static final String CAC_ERRORSEVERITY = "CAC-ERRORSEVERITY";



	// **************************************************************************************************
	public static String callServiceXML(HttpServletRequest request, String serviceRequest) throws Exception {

		String s[]=serviceRequest.split("\\?");
		String service=s[0];
		String params="";
		if (s.length>1) {
				params=s[1];
		}
				
		String url = request.getRequestURL().toString();
		String baseUrl = url.substring(0, url.length() - request.getRequestURI().length()) + request.getContextPath() + "/";		
		
		Cookie cookies[] = request.getCookies();
		request.getSession();

		URL obj = new URL(baseUrl + "/" + service);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setDoOutput(true);		
		con.setRequestProperty("Connection", "Keep-Alive");
		con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");		

		// Pasar la sesión original a la llamada del servicio, si no da error
		// 401 de autenticación
		con.setRequestProperty("Cookie", "JSESSIONID=" + request.getSession().getId());

		con.connect();

		OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream(), "UTF-8");
		writer.write(params);
		writer.flush();
				
		int responseCode = 200;

		try {
			responseCode = con.getResponseCode();
			log.error(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			log.error("Call service: " + baseUrl + "/" + serviceRequest);
			log.error("Response code : " + responseCode);
			if (con.getHeaderField(CAC_ERRORCODE) != null)
				log.error("Error code : " + con.getHeaderField(CAC_ERRORCODE));
			if (con.getHeaderField(CAC_ERRORMSG) != null)
				log.error("Error message : " + con.getHeaderField(CAC_ERRORMSG));
			if (con.getHeaderField(CAC_ERRORDETAIL) != null)
				log.error("Error detail : " + con.getHeaderField(CAC_ERRORDETAIL));
			if (con.getHeaderField(CAC_ERRORSEVERITY) != null)
				log.error("Error severity : " + con.getHeaderField(CAC_ERRORSEVERITY));
			log.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		} catch (Exception ex) {
			log.error("Error en llamada al servicio: " + ex.getCause());
		}

		if (responseCode != 200) {
			log.error("ERROR DETECTADO: " + responseCode);
			throw new java.net.ConnectException(Integer.toString(responseCode));
		}

		if (con.getHeaderField(CAC_ERRORMSG) != null && con.getHeaderField(CAC_ERRORMSG).indexOf("no hay sesion para el usuario actual") >= 0) {
			log.error("ERROR DETECTADO: no hay sesión");
			throw new java.net.ConnectException("401-TIMEOUT");
		}

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		String xml = response.toString();
		xml = StringEscapeUtils.unescapeHtml(StringEscapeUtils.unescapeXml(xml));

		if (xml.equals("<error/>") && con.getHeaderField(CAC_ERRORMSG) != null) {
			String cadena = con.getHeaderField(CAC_ERRORMSG).replaceAll("<", "");
			cadena = cadena.replaceAll(">", "");
			
			xml = StringEscapeUtils.unescapeHtml("<error>" + StringUtils.capitalize(cadena) + "</error>");
			System.out.println("XML ERROR="+xml);
		}
		
		if (!xml.startsWith("<error>") && con.getHeaderField(CAC_ERRORMSG) != null && con.getHeaderField(CAC_ERRORMSG).length()>0) {
			log.error("ERROR DETECTADO: " +con.getHeaderField(CAC_ERRORMSG));
			throw new java.net.ConnectException(con.getHeaderField(CAC_ERRORMSG));
		}
			
		xml = xml.replaceAll("&", "");
		return (xml);
	}

	// **************************************************************************************************
	public static String callServiceXMLSinReemplazo(HttpServletRequest request, String serviceRequest) throws Exception {

		String s[]=serviceRequest.split("\\?");
		String service=s[0];
		String params="";
		if (s.length>1) {
				params=s[1];
		}
				
		String url = request.getRequestURL().toString();
		String baseUrl = url.substring(0, url.length() - request.getRequestURI().length()) + request.getContextPath() + "/";		
		
		Cookie cookies[] = request.getCookies();
		request.getSession();

		URL obj = new URL(baseUrl + "/" + service);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setDoOutput(true);		
		con.setRequestProperty("Connection", "Keep-Alive");
		con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");		

		// Pasar la sesión original a la llamada del servicio, si no da error
		// 401 de autenticación
		con.setRequestProperty("Cookie", "JSESSIONID=" + request.getSession().getId());

		con.connect();

		OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream(), "UTF-8");
		writer.write(params);
		writer.flush();
				
		int responseCode = 200;

		try {
			responseCode = con.getResponseCode();
			log.error(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			log.error("Call service: " + baseUrl + "/" + serviceRequest);
			log.error("Response code : " + responseCode);
			if (con.getHeaderField(CAC_ERRORCODE) != null)
				log.error("Error code : " + con.getHeaderField(CAC_ERRORCODE));
			if (con.getHeaderField(CAC_ERRORMSG) != null)
				log.error("Error message : " + con.getHeaderField(CAC_ERRORMSG));
			if (con.getHeaderField(CAC_ERRORDETAIL) != null)
				log.error("Error detail : " + con.getHeaderField(CAC_ERRORDETAIL));
			if (con.getHeaderField(CAC_ERRORSEVERITY) != null)
				log.error("Error severity : " + con.getHeaderField(CAC_ERRORSEVERITY));
			log.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		} catch (Exception ex) {
			log.error("Error en llamada al servicio: " + ex.getCause());
		}

		if (responseCode != 200) {
			log.error("ERROR DETECTADO: " + responseCode);
			throw new java.net.ConnectException(Integer.toString(responseCode));
		}

		if (con.getHeaderField(CAC_ERRORMSG) != null && con.getHeaderField(CAC_ERRORMSG).indexOf("no hay sesion para el usuario actual") >= 0) {
			log.error("ERROR DETECTADO: no hay sesión");
			throw new java.net.ConnectException("401-TIMEOUT");
		}

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		String xml = response.toString();
		xml = StringEscapeUtils.unescapeHtml(StringEscapeUtils.unescapeXml(xml));

		if (xml.equals("<error/>") && con.getHeaderField(CAC_ERRORMSG) != null) {
			String cadena = con.getHeaderField(CAC_ERRORMSG).replaceAll("<", "");
			cadena = cadena.replaceAll(">", "");
			
			xml = StringEscapeUtils.unescapeHtml("<error>" + StringUtils.capitalize(cadena) + "</error>");
			System.out.println("XML ERROR="+xml);
		}
		
		if (!xml.startsWith("<error>") && con.getHeaderField(CAC_ERRORMSG) != null && con.getHeaderField(CAC_ERRORMSG).length()>0) {
			log.error("ERROR DETECTADO: " +con.getHeaderField(CAC_ERRORMSG));
			throw new java.net.ConnectException(con.getHeaderField(CAC_ERRORMSG));
		}
			
		return (xml);
	}
	
	// **************************************************************************************************
	public static String callServiceXMLbyGET(HttpServletRequest request, String service) throws Exception {

		String url = request.getRequestURL().toString();
		String baseUrl = url.substring(0, url.length() - request.getRequestURI().length()) + request.getContextPath() + "/";

		Cookie cookies[] = request.getCookies();
		request.getSession();

		URL obj = new URL(baseUrl + "/" + service);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("GET");

		// Pasar la sesión original a la llamada del servicio, si no da error
		// 401 de autenticación
		con.setRequestProperty("Cookie", "JSESSIONID=" + request.getSession().getId());

		int responseCode = 200;

		try {
			responseCode = con.getResponseCode();
			log.error(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			log.error("Call service: " + baseUrl + "/" + service);
			log.error("Response code : " + responseCode);
			if (con.getHeaderField(CAC_ERRORCODE) != null)
				log.error("Error code : " + con.getHeaderField(CAC_ERRORCODE));
			if (con.getHeaderField(CAC_ERRORMSG) != null)
				log.error("Error message : " + con.getHeaderField(CAC_ERRORMSG));
			if (con.getHeaderField(CAC_ERRORDETAIL) != null)
				log.error("Error detail : " + con.getHeaderField(CAC_ERRORDETAIL));
			if (con.getHeaderField(CAC_ERRORSEVERITY) != null)
				log.error("Error severity : " + con.getHeaderField(CAC_ERRORSEVERITY));
			log.error("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		} catch (Exception ex) {
			log.error("Error en llamada al servicio: " + ex.getCause());
		}

		if (responseCode != 200) {
			log.error("ERROR DETECTADO: " + responseCode);
			throw new java.net.ConnectException(Integer.toString(responseCode));
		}

		if (con.getHeaderField(CAC_ERRORMSG) != null && con.getHeaderField(CAC_ERRORMSG).indexOf("no hay sesion para el usuario actual") >= 0) {
			log.error("ERROR DETECTADO: no hay sesión");
			throw new java.net.ConnectException("401-TIMEOUT");
		}

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		String xml = response.toString();
		xml = StringEscapeUtils.unescapeHtml(StringEscapeUtils.unescapeXml(xml));

		if (xml.equals("<error/>") && con.getHeaderField(CAC_ERRORMSG) != null) {
			String cadena = con.getHeaderField(CAC_ERRORMSG).replaceAll("<", "");
			cadena = cadena.replaceAll(">", "");
			xml = StringEscapeUtils.unescapeHtml("<error>" + StringUtils.capitalize(cadena) + "</error>");
			System.out.println("XML ERROR="+xml);
		}

		if (!xml.startsWith("<error>") && con.getHeaderField(CAC_ERRORMSG) != null && con.getHeaderField(CAC_ERRORMSG).length()>0) {
			log.error("ERROR DETECTADO: " +con.getHeaderField(CAC_ERRORMSG));
			throw new java.net.ConnectException(con.getHeaderField(CAC_ERRORMSG));
		}
						
		return (xml);
	}



	// **************************************************************************************************
	public static String callServiceJSON(HttpServletRequest request, String service) throws Exception {

		String xml = callServiceXML(request, service);

		JSONObject json = XML.toJSONObject(xml);

		if (json.has("error")) throw new Exception(json.getString("error"));
		
		json.append("iTotalRecords", "99999"); // para habilitar paginación
												// DataTable en Server-side
												// processing
		json.append("iTotalDisplayRecords", "99999");

		return (json.toString(4));
	}


	// **************************************************************************************************
	public static ListMultimap<String, String> callServiceMap(HttpServletRequest request, String service) throws Exception {

		String xml = callServiceXML(request, service);
		ListMultimap<String, String> multimap = null;

		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(new InputSource(new StringReader(xml)));

			multimap = xmlToMap(document.getDocumentElement(), "xml");

		} catch (Exception ex) {
			log.error("ERROR EN LLAMADA AL SERVICIO: " + service + " --- " + ExceptionUtils.getStackTrace(ex));
			log.error(xml);
			throw ex;
		}

		return (multimap);
	}



	// **************************************************************************************************
	public static Object getUserMainMenu(HttpServletRequest request) throws Exception {

		ListMultimap<String, String> multimap = callServiceMap(request, "obtenerMenuUsuario.action?servicio=obtenerMenuUsuario");

		return (multimap.asMap());
	}



	// **************************************************************************************************
	private static ListMultimap<String, String> xmlToMap(Node node, String key) {

		ListMultimap<String, String> multimap = ArrayListMultimap.create();

		String nodeName = node.getNodeName();
		short type = node.getNodeType();
		if (type == Node.TEXT_NODE) {
			String text = ((Text) node).getWholeText().trim();
			if (text.length() > 0) {
				multimap.put(key, text);
				log.info(key + "=" + text);
			}
		}

		NodeList list = node.getChildNodes();
		if (list.getLength() > 0) {
			for (int i = 0; i < list.getLength(); i++) {
				multimap.putAll(xmlToMap(list.item(i), key + "__" + nodeName));
			}
		}

		return (multimap);
	}



	// **************************************************************************************************
	public static Calendar toCalendar(String strdate) {
		Calendar calendar = null;

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = df.parse(strdate);
		} catch (ParseException e) {
		}

		if (date != null) {
			calendar = Calendar.getInstance();
			calendar.setTime(date);
		}

		return calendar;
	}



	// **************************************************************************************************
	public static String extractXMLError(String xml) {

		String error = xml.replace("<error>", "").replace("</error>", "");

		return error;
	}



	// **************************************************************************************************
	public static String generateSelect2TreeJSON(String xml, String rootTag, String nodeTag, String leafTag, String idNodeTag, String idLeafTag) throws Exception {

		String json = "";

		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		Document doc = documentBuilder.parse(new InputSource(new StringReader(xml)));
		doc.getDocumentElement().normalize();

		json = processChildNodes(doc.getElementsByTagName(rootTag).item(0), nodeTag, leafTag, idNodeTag, idLeafTag);

		return json;
	}



	private static String processChildNodes(Node node, String nodeTag, String leafTag, String idNodeTag, String idLeafTag) {

		String json = "";

		NodeList childs = node.getChildNodes();

		for (int i = 0; i < childs.getLength(); i++) {

			Node mynode = childs.item(i);

			if (!mynode.equals(node) && mynode.getNodeName().equals(nodeTag) && mynode.getParentNode().equals(node)) {

				String id = ((Element) mynode).getElementsByTagName(idNodeTag).item(0).getTextContent();
				String name = ((Element) mynode).getElementsByTagName("nombre").item(0).getTextContent();

				json += "{id:\"" + id + "\",text:\"" + name + "\",inc:[";

				json += processChildNodes(mynode, nodeTag, leafTag, idNodeTag, idLeafTag);

				NodeList leafs = ((Element) mynode).getElementsByTagName(leafTag);

				for (int j = 0; j < leafs.getLength(); j++) {

					Node myleaf = leafs.item(j);
					
					// Debo retroceder dos nodos para comprobar el parent porque una zona está dentro de un tag zonas
					if (myleaf.getNodeName().equals(leafTag) && myleaf.getParentNode().getParentNode().equals(mynode)) {
						String idLeaf = ((Element) myleaf).getElementsByTagName(idLeafTag).item(0).getTextContent();
						String nameLeaf = ((Element) myleaf).getElementsByTagName("nombre").item(0).getTextContent();
						json += "{id:\"" + idLeaf + "\",text:\"" + nameLeaf + "\"},";
					}
				}

				json += "]},";

			}
		}

		return (json);

	}



	// **************************************************************************************************
	
	public static void setCommonObjectsInSession(HttpServletRequest request) throws Exception {

		String json = callServiceJSON(request, "obtenerListadoIdiomas.action");

		request.getSession().setAttribute("i18nlangs", json);
		
		// Los códigos de idiomas para la internacionalización de encuestas no son los mismos que se usan para el resto de la aplicación
	
		
		try {
			String xml_encuestas = callServiceXML(request, "obtenerIdiomasValidos.action");
			xml_encuestas = xml_encuestas.replaceAll("<IdiomaEnc>", "<LabelValue default=\"0\">");
			xml_encuestas = xml_encuestas.replaceAll("</IdiomaEnc>", "</LabelValue>");
			xml_encuestas = xml_encuestas.replaceAll("<cod>", "<value>");
			xml_encuestas = xml_encuestas.replaceAll("</cod>", "</value>");
			xml_encuestas = xml_encuestas.replaceAll("<des>", "<label>");
			xml_encuestas = xml_encuestas.replaceAll("</des>", "</label>");
			
			

			JSONObject json_encuesta = XML.toJSONObject(xml_encuestas);

			if (json_encuesta.has("error")) throw new Exception(json_encuesta.getString("error"));
			
			json_encuesta.append("iTotalRecords", "99999"); // para habilitar paginación
													// DataTable en Server-side
													// processing
			json_encuesta.append("iTotalDisplayRecords", "99999");

			request.getSession().setAttribute("i18nlangsEncuesta", json_encuesta.toString(4));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}



	// **************************************************************************************************
	public static String generateI18nXMLFromForm(HashMap<String, String> i18n, String defaultNombre) {
		
		// OJO! El parámetro default nombre se pasa para cuando en un campo de tipo descripción internacionalizado no se pasa el nombre en ese idioma concreto.
		// Esto es una chapuza del servicio dado que requiere un nombre obligatoriamente, así que lo que se suele pasar es el nombre en el idioma por defecto.
		// Si no se pasa nada se pone la cadena "undefined"
		
		if (defaultNombre==null || defaultNombre.length()<=0) defaultNombre="undefined";

		// Los campos i18n se codifican en Javascript de la siguiente manera
		// id$~idioma~valor^id~idioma~valor|...

		// Se pasan a este método como un Hashmap en el cual la clave es el
		// nombre del campo internacionalizado

		HashMap<String, String> structure = new HashMap<String, String>();

		Iterator<String> keysIterator = i18n.keySet().iterator();

		while (keysIterator.hasNext()) {
			String key = keysIterator.next();
			String rawData = i18n.get(key);

			String[] rows = rawData.split("\\^");
			
			for (int i = 0; i < rows.length; i++) {

				String[] data = rows[i].split("~");

				String idi18n = "";
				
				if (data[0] != null)
					idi18n = data[0];
				
				String ididioma = "";
				if (data.length > 1)
					ididioma = data[1];
				
				String value = "";
				if (data.length > 2)
					value = data[2];

				String xml = structure.get(idi18n);
				if (xml == null) {
					xml = "<idi18n>" + idi18n + "</idi18n>";
					xml = xml + "<idioma><ididioma>" + ididioma + "</ididioma></idioma>";
				}
				
				
				if (value.length()>0) {					
					xml = xml + "<" + key + ">" + value + "</" + key + ">";
					xml = xml + "<dadodebaja>0</dadodebaja>";
				}
				else {
					xml = xml + "<" + key + ">null</" + key + ">";
					xml = xml + "<dadodebaja>1</dadodebaja>";
				}
				if(!ididioma.equalsIgnoreCase(""))
					structure.put(ididioma, xml);
			}
		}

		Iterator<Entry<String, String>> nodes = structure.entrySet().iterator();

		String xml = "";
		while (nodes.hasNext()) {
			Entry<String, String> node = nodes.next();

			// El nodo de internacionalización tiene que tener al menos un campo nombre, si no lo tiene se lo añadimos
			String xmlNode=node.getValue(); 
			
			if (xmlNode.indexOf("<nombre>")<0) xmlNode="<nombre>"+defaultNombre+"</nombre>"+xmlNode;
			
			xml += "<I18n>" + xmlNode + "</I18n>";
		}

		return xml;
	}
	
	// **************************************************************************************************
		public static String generateI18nXMLFromFormNombreDescripcion(HashMap<String, String> i18n,HashMap<String, String> i18nD, String defaultNombre) {
			
			// OJO! El parámetro default nombre se pasa para cuando en un campo de tipo descripción internacionalizado no se pasa el nombre en ese idioma concreto.
			// Esto es una chapuza del servicio dado que requiere un nombre obligatoriamente, así que lo que se suele pasar es el nombre en el idioma por defecto.
			// Si no se pasa nada se pone la cadena "undefined"
			
			if (defaultNombre==null || defaultNombre.length()<=0) defaultNombre="undefined";

			// Los campos i18n se codifican en Javascript de la siguiente manera
			// id$~idioma~valor^id~idioma~valor|...

			// Se pasan a este método como un Hashmap en el cual la clave es el
			// nombre del campo internacionalizado

			HashMap<String, String> structure = new HashMap<String, String>();

			Iterator<String> keysIterator = i18n.keySet().iterator();
			Iterator<String> keysIteratorDesc = i18nD.keySet().iterator();

			while (keysIterator.hasNext()) {
				String key = keysIterator.next();
				String keyD = keysIteratorDesc.next();
				String rawData = i18n.get("nombre");
				String rawDataD = i18nD.get("descripcion");
				
				String[] rows = rawData.split("\\^");
				
			
				String[] rowsD = rawDataD.split("\\^");
				
				for (int i = 0; i < rows.length; i++) {

					String[] data = rows[i].split("~");
					String[] dataD = rowsD[i].split("~");

					String idi18n = "";
					
					if (data[0] != null)
						idi18n = data[0];
					
					String ididioma = "";
					if (data.length > 1)
						ididioma = data[1];
					
					String value = "";
					if (data.length > 2)
						value = data[2];
					
					String valueD = "";
					if (dataD.length > 2)
						valueD = dataD[2];

					String xml = structure.get(idi18n);
					if (xml == null) {
						xml = "<idi18n>" + idi18n + "</idi18n>";
						xml = xml + "<idioma><ididioma>" + ididioma + "</ididioma></idioma>";
					}
					
					if (valueD.length()>0) {					
						xml = xml + "<descripcion>" + valueD + "</descripcion>";						
					}
					
					
					if (value.length()>0) {					
						xml = xml + "<" + key + ">" + value + "</" + key + ">";
						xml = xml + "<dadodebaja>0</dadodebaja>";
					}
					else {
						xml = xml + "<" + key + ">null</" + key + ">";
						xml = xml + "<dadodebaja>1</dadodebaja>";
					}
					if(!ididioma.equalsIgnoreCase(""))
						structure.put(ididioma, xml);
				}
			}

			Iterator<Entry<String, String>> nodes = structure.entrySet().iterator();

			String xml = "";
			while (nodes.hasNext()) {
				Entry<String, String> node = nodes.next();

				// El nodo de internacionalización tiene que tener al menos un campo nombre, si no lo tiene se lo añadimos
				String xmlNode=node.getValue(); 
				
				if (xmlNode.indexOf("<nombre>")<0) xmlNode="<nombre>"+defaultNombre+"</nombre>"+xmlNode;
				
				xml += "<I18n>" + xmlNode + "</I18n>";
			}

			return xml;
		}
	
	public static String cambiarHoras(String horaInicio,String horaAdicional,boolean sumar) {
		int resto = 0;
		int minuto = 0;
		int hora = 0;

		String[] h1 = horaInicio.split(":");
		String[] h2 = horaAdicional.split(":");
		
		if (sumar) {
			minuto = Integer.parseInt(h1[1])+Integer.parseInt(h2[1]);
			if (minuto > 60) {
			   minuto = minuto - 60;
			   resto = 1;
			}
			hora = Integer.parseInt(h1[0])+Integer.parseInt(h2[0])+resto;
		}
		else {
			minuto = Integer.parseInt(h1[1])-Integer.parseInt(h2[1]);
			if (minuto < 0) {
			   minuto = 60 - minuto;
			   resto = 1;
			}
			hora = Integer.parseInt(h1[0])-Integer.parseInt(h2[0])-resto;
		}
		
		return hora+":"+minuto;
	}

}

package es.cac.colossus.frontend.utils;

public class AjaxException extends Exception {

	private static final long serialVersionUID = 1L;

	public AjaxException(String message) {
		super(message);
	}



	public AjaxException(String message, Throwable throwable) {
		super(message, throwable);
	}

}

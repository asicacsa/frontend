<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${gestiondocumentos_selector_categorias}" var="gestiondocumentos_selector_categorias_xml" />
<x:parse xml="${gestiondocumentos_rutaweb}" var="gestiondocumentos_rutaweb_xml" />

<div class="row x_panel">
<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2><spring:message code="common.text.filter_list" /></h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">

		<form id="form_filter_list_documentos_gestion" class="form-horizontal form-label-left">
			<input type="hidden" name="rutaweb" id="rutaweb" value="<x:out select = "$gestiondocumentos_rutaweb_xml/string" />" />

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="documentacion.documentos.gestion.field.categoria" /></label>
				<div class="col-md-6 col-sm-6 col-xs-9">
					<select class="form-control" name="idcategoria" id="idcategoria" required="required">
						<option value=""></option>
							<x:forEach select="$gestiondocumentos_selector_categorias_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
				
			<div class="clearfix"></div>
			<div class="ln_solid"></div>
			<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
				<button id="button_filter_list_gestion_documentos" type="button" class="btn btn-success pull-right">
					<spring:message code="common.button.filter" />
				</button>
			</div>
			
			<input type="hidden" name="start" value=""/>
			<input type="hidden" name="length" value=""/>

		</form>

	</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="gestiondocumentos_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="documentacion.documentos.gestion.list.button.new" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="gestiondocumentos_view">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="documentacion.documentos.gestion.list.button.view" />"> <span class="fa fa-file-o"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="gestiondocumentos_delete">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="documentacion.documentos.gestion.list.button.delete" />"> <span class="fa fa-trash-o"></span>
			</span>
		</a>
	</div>

	<table id="datatable-list-documentos" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="documentacion.documentos.gestion.list.header.iddocumento" /></th>
				<th><spring:message code="documentacion.documentos.gestion.list.header.nombre" /></th>
				<th><spring:message code="documentacion.documentos.gestion.list.header.descripcion" /></th>
				<th><spring:message code="documentacion.documentos.gestion.list.header.fechainiciovigencia" /></th>
				<th><spring:message code="documentacion.documentos.gestion.list.header.fechafinvigencia" /></th>
				<th><spring:message code="documentacion.documentos.gestion.list.header.ruta" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
</div>

<script>

var consulta= ${soloConsulta};

if (consulta=="1") {
	$("#gestiondocumentos_new").hide();
	$("#gestiondocumentos_delete").hide();
}
	
    	 
var dt_listdocumentos=$('#datatable-list-documentos').DataTable( {
	serverSide: true,
	searching: false,
	ordering: false,
	pagingType: "simple",
	deferLoading: 0,
	info: false,
    ajax: {
        url: "<c:url value='/ajax/documentacion/documentos/list_documentos_gestion.do'/>",
        rowId: 'iddocumento',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Documento)); return(""); },
        error:function(err, status){
			dt_listdocumentos.processing(false);
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="documentacion.documentos.gestion.list.alert.ningun_filtro" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
        },
        data: function (params) {
        	$('input[name="start"]').val(params.start);
        	$('input[name="length"]').val(params.length);
        	return ($("#form_filter_list_documentos_gestion").serializeObject());
       	}
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listdocumentos.data().count()>0) dt_listdocumentos.columns.adjust().draw(); });
	},
	columnDefs: [
	             { "visible": false, "targets": 0 },
	             { "visible": false, "targets": 5 }
	         ],
    columns: [
        { data: "iddocumento", type: "numeric"}, 
        { data: "nombre", type: "spanish-string"}, 
        { data: "descripcion", type: "spanish-string"}, 
        { data: "fechainiciovigencia", className: "cell_centered", type: "date", defaultContent: "" },
        { data: "fechafinvigencia", className: "cell_centered", type: "date", defaultContent: "" },
        { data: "ruta", className: "cell_centered", type: "date", defaultContent: "" }
    ],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-documentos') },
    select: { style: 'single' },
	language: dataTableLanguage,
	processing: true
} );

insertSmallSpinner("#datatable-list-documentos_processing");

//********************************************************************************
$("#button_filter_list_gestion_documentos").on("click", function(e) { dt_listdocumentos.ajax.reload(); })

$("#gestiondocumentos_new").on("click", function(e) {
	showButtonSpinner("#gestiondocumentos_new");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/documentacion/documentos/nuevo_documento.do'/>", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "sm");
	});

});

//********************************************************************************
$("#gestiondocumentos_view").on("click", function(e) {
	var data = sanitizeArray(dt_listdocumentos.rows( { selected: true } ).data(),"ruta");

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: ' <spring:message code="documentacion.documentos.gestion.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	

	window.open($("#rutaweb").val()+data[0],"_blank");
});

//********************************************************************************
$("#gestiondocumentos_delete").on("click", function(e) { 

	var data = sanitizeArray(dt_listdocumentos.rows( { selected: true } ).data(),"iddocumento");

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: ' <spring:message code="documentacion.documentos.gestion.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	

	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="documentacion.documentos.gestion.list.confirm.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
  		  buttons: { closer: false, sticker: false	},
  		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
	
			dt_listdocumentos.processing(true);
		   
			$.ajax({
				contenttype: "application/json; charset=utf-8",
				type : "post",
				url : "<c:url value='/ajax/documentacion/documentos/remove_documentos.do'/>",
				timeout : 100000,
				data: { data: data.toString() }, 
				success : function(data) {
					dt_listdocumentos.processing(false);	
					dt_listdocumentos.rows( { selected: true } ).remove().draw();
				},
				error : function(exception) {
					dt_listdocumentos.processing(false);
					
					new PNotify({
					      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					      text: exception.responseText,
						  type: "error",		     
						  delay: 5000,
						  buttons: { sticker: false }
					   });			
				}
			});

	   }).on('pnotify.cancel', function() {
	   });		 

}); 

</script>

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form"/>
<tiles:insertAttribute name="modal_dialog" />

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!-- Pesta�as ------------------------------------------------------------->
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 maintab">
		<div class="" role="tabpanel" data-example-id="togglable-tabs">
			<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
				<li role="presentation" class="active"><a href="#tab_parametros" id="parametros-tab" role="tab" data-toggle="tab" aria-expanded="true"><spring:message code="administracion.configuracioninternet.tabs.parametrosinternet.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_zonas" role="tab" id="zonas-tab" data-toggle="tab" aria-expanded="false"><spring:message code="administracion.configuracioninternet.tabs.zonasinternet.title" /></a></li>
			</ul>
		</div>
	</div>
</div>

<!-- Area de trabajo ------------------------------------------------------>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel thin_padding">

			<div id="myTabContent" class="tab-content">
				<div role="tabpanel" class="tab-pane fade active in" id="tab_parametros" aria-labelledby="parametros-tab">
					<tiles:insertAttribute name="tab_parametros" />
				</div>

				<div role="tabpanel" class="tab-pane fade" id="tab_zonas" aria-labelledby="zonas-tab">
					<tiles:insertAttribute name="tab_zonas" />
				</div>
			</div>

			<div class="clearfix"></div>
		</div>
	</div>
</div>

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form"/>
<tiles:insertAttribute name="modal_dialog" />

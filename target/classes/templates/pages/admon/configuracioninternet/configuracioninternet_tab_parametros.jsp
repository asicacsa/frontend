<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${tabparametros_data}" var="tabparametros_data_xml" />
<c:set var="servicio">
	<x:out select="$tabparametros_data_xml/ConfiguracionInternet/servicioActivo" />
</c:set>




<div class="row">

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel thin_padding" id="x_panel">
			<form id="form_parametros_internet"
				class="form-horizontal form-label-left" method='post'>

				<!--*************************************************************************************************************** -->
				<input type="hidden" id="selectorZonas" name="selectorZonas"/>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Servicio
						Activo</label>
					<div class="iradio_flat-green" style="position: relative;">
						<input type="radio" class="flat" name="servicioActivo"
							id="servicioS" required="" data-parsley-multiple="servicio"
							style="position: absolute; opacity: 0;" value="true" />

					</div>
					Si
					<div class="iradio_flat-green" style="position: relative;">
						<input type="radio" class="flat" name="servicioActivo"
							id="servicioN" required="" data-parsley-multiple="servicio"
							style="position: absolute; opacity: 0;" value="false" /> </input>
					</div>
					No
				</div>

				<!--*************************************************************************************************************** -->
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Mensaje
						Inactividad </label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<input id="mensajeInactividad" name="mensajeInactividad"
							type="text" class="form-control" required="required"
							value="<x:out select="$tabparametros_data_xml/ConfiguracionInternet/mensajeInactividad" />">
					</div>
				</div>
				<!--*************************************************************************************************************** -->
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3">Ds_Merchant_Currency</label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input id="dsMerchantCurrency" name="dsMerchantCurrency"
							type="text" class="form-control" required="required"
							value="<x:out select="$tabparametros_data_xml/ConfiguracionInternet/dsMerchantCurrency" />">
					</div>

				</div>
				<!--*************************************************************************************************************** -->
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3">_Merchant_ProductDescription</label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input id="dsMerchantProductDescription"
							name="dsMerchantProductDescription" type="text"
							class="form-control" required="required"
							value="<x:out select="$tabparametros_data_xml/ConfiguracionInternet/dsMerchantProductDescription" />">
					</div>
				</div>
				<!--*************************************************************************************************************** -->
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3">Ds_Merchant_MerchantCode</label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input id="dsMerchantMerchantCode" name="dsMerchantMerchantCode" type="text" class="form-control" required="required"
							value="<x:out select="$tabparametros_data_xml/ConfiguracionInternet/dsMerchantMerchantCode" />">
					</div>
				</div>
				<!--*************************************************************************************************************** -->
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3">Ds_Merchant_MerchantURL</label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input id="dsMerchantMerchantURL" name="dsMerchantMerchantURL"
							type="text" class="form-control" required="required"
							value="<x:out select="$tabparametros_data_xml/ConfiguracionInternet/dsMerchantMerchantURL" />">
					</div>
				</div>
				<!--*************************************************************************************************************** -->
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3">Ds_Merchant_UrlOK</label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input id="dsMerchantUrlOK" name="dsMerchantUrlOK" type="text"
							class="form-control" required="required"
							value="<x:out select="$tabparametros_data_xml/ConfiguracionInternet/dsMerchantUrlOK" />">
					</div>
				</div>
				<!--*************************************************************************************************************** -->
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3">Ds_Merchant_UrlKO</label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input id="dsMerchantUrlKO" name="dsMerchantUrlKO" type="text"
							class="form-control" required="required"
							value="<x:out select="$tabparametros_data_xml/ConfiguracionInternet/dsMerchantUrlKO" />">
					</div>
				</div>
				<!--*************************************************************************************************************** -->
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3">Merchant_Name</label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input id="merchantName" name="merchantName" type="text"
							class="form-control" required="required"
							value="<x:out select="$tabparametros_data_xml/ConfiguracionInternet/merchantName" />">
					</div>
				</div>
				<!--*************************************************************************************************************** -->
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3">Merchant_Password</label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input id="merchantPassword" name="merchantPassword" type="text"
							class="form-control" required="required"
							value="<x:out select="$tabparametros_data_xml/ConfiguracionInternet/merchantPassword" />">
					</div>
				</div>
				<!--*************************************************************************************************************** -->
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3">Merchant_Terminal</label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input id="merchantTerminal" name="merchantTerminal" type="text"
							class="form-control" required="required"
							value="<x:out select="$tabparametros_data_xml/ConfiguracionInternet/merchantTerminal" />">
					</div>
				</div>
				<!--*************************************************************************************************************** -->
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3">Merchant_TransactionType</label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input id="merchantTransactionType" name="merchantTransactionType"
							type="text" class="form-control" required="required"
							value="<x:out select="$tabparametros_data_xml/ConfiguracionInternet/merchantTransactionType" />">
					</div>
				</div>
				<!--*************************************************************************************************************** -->
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3">contactoCAC</label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input id="contactoCAC" name="contactoCAC" type="text"
							class="form-control" required="required"
							value="<x:out select="$tabparametros_data_xml/ConfiguracionInternet/contactoCAC" />">
					</div>
				</div>
				<!--*************************************************************************************************************** -->
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3">N�mero
						m�ximo reservas</label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input id="nummaxReservasInternet" name="nummaxReservasInternet"
							type="text" class="form-control" required="required"
							value="<x:out select="$tabparametros_data_xml/ConfiguracionInternet/nummaxReservasInternet" />">
					</div>
				</div>
				<!--*************************************************************************************************************** -->
				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						<button type="button" id="save_parametros_button" class="btn btn-success save_data"><spring:message code="common.button.submit_data" /></button>
					</div>
				</div>

			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	jQuery(document)
			.ready(
					function($) {
						/*Valor del radio button*/
						<c:choose>
						<c:when test="${servicio == true}">
						$("#servicioS").attr('checked', 'checked');
						</c:when>
						<c:otherwise>
						$("#servicioN").attr('checked', 'checked');
						</c:otherwise>
						</c:choose>
						/*********************************/
						/*Validar y enviar formulario*/
						$(".save_data").click(function(event) {
							$("#form_parametros_internet").submit();
						});	

						/*Cuando hace el submit llama a la validacion*/
						$("#form_parametros_internet").validate({
							onfocusout : false,
							onkeyup : false,							
							unhighlight: function(element, errClass) {
					            $(element).popover('hide');
							},		
							errorPlacement : function(err, element) {
								err.hide();
								$(element).attr('data-content', err.text());
								$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
								$(element).popover('show');									
							},
							submitHandler : function(form) {
								saveParameters();
							}
						}
	);
					
					}); /*Esto cierra jQuery*/
	/*****************************************************************************************/
	function saveParameters() {
			
		// Obtener los datos antes de hace showSpinner() porque sino los campos est�n disabled y no recupera los datos
		var data = $("#form_parametros_internet").serializeObject();	
		
		//Muestra Cargando...
		showSpinner("#x_panel");

		$
				.ajax({
					type : "POST",
					url : "<c:url value='/admon/configuracioninternet/save_parametros_configuracion_internet.do'/>",
					data : data,
					timeout : 100000,
					success : function(data) {
						hideSpinner("#x_panel");

						new PNotify({
							title : '<spring:message code="common.dialog.text.operacion_realizada" />',
							text : '<spring:message code="common.dialog.text.datos_guardados" />',
							type : "success",
							delay : 5000,
							buttons : {
								closer : true,
								sticker : false
							}
						});	
						
						
					},
					error : function(data, textStatus, errorThrown) {
						hideSpinner("#x_panel");
						alert("ERROR");
					}
				});

	}
</script>

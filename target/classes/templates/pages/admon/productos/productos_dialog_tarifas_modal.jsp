<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="modal fade" id="modal_tarifas" tabindex="-1"
	role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close close_dialog" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">�</span>
				</button>
				<h4 class="modal-title">
						<spring:message code="administracion.productos.tabs.recintos.tarifas.title" />			
				</h4>	
			</div>

		<div class="modal-body">
	
			<form id="tarifas_editar" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
			<input type="hidden" id="idProductoTarifa" />
			<div class="col-md-12 col-sm-12 col-xs-12">
	
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.productos.field.tarifa" />*</label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						<select name="idtarifa" id="selector_tarifa" class="form-control" required="required">
							<option value=""></option>								
						</select>
					</div>
				</div>
	
				<div class="form-group date-picker">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.productos.field.tarifas.fecha" />*</label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						  <a type="button" class="btn btn-default btn-clear-date" id="button_apertura_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="ventareserva.operacionescaja.operacionescaja.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
						  </a>			
	                       <div class="input-prepend input-group">
	                         <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	                         <input type="text" name="fecha_tarifa" id="fecha_tarifa" class="form-control" value="" readonly/>
	                         <input type="hidden" required="required" id="fechainiciofecha_tarifa" name="fechainiciofecha_tarifa" />
	                         <input type="hidden" id="fechafinfecha_tarifa" name="fechafinfecha_tarifa" />
	                       </div>
					</div>
				</div>	
				
				
			
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.productos.field.tarifas.importe" />*</label>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<input type="text" name="importe" id="importe" class="form-control" required="required"/>								
					</div>
					<div class="col-md-5 col-sm-5 col-xs-5"></div>				
				</div>			    
	
	
			</div>
	
		</form>
	
		<div class="modal-footer">	
			<button id="save_cliente_button" type="button" class="btn btn-primary close_tarifa">
				<spring:message code="common.button.accept" />
			</button>
			<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
				<spring:message code="common.button.cancel" />
			</button>
		</div>
		
			</div>
			
		</div>
	</div>
</div>
<script>
	
	
	//********************************************************
	$('#importe').inputmask({alias: 'numeric', 
	                       allowMinus: false,  
	                       digits: 2, 
	                       max: 999.99});
	
	
	//*********************************************

		$today= moment().format("DD/MM/YYYY");
		//Ahora venta
		dia_inicio = $today;
		dia_fin = $today;
		
		var datatableModalTarifas;
		var creandoTarifa = true;
		$('#boton').attr("disabled", false);
		
		$('input[name="fecha_tarifa"]').val( dia_inicio + ' - ' + dia_fin);
		
		
			 
		$(".close_tarifa").on("click", function(e) { 
			$("#tarifas_editar").submit();						
		})
		
		
		
		$("#tarifas_editar").validate({
			onfocusout : false,
			onkeyup : false,
			unhighlight: function(element, errClass) {
	            $(element).popover('hide');
			},		
			errorPlacement : function(err, element) {
				err.hide();
				$(element).attr('data-content', err.text());
				$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
				$(element).popover('show');									
			},
			submitHandler : function(form) {
				guardarTarifa()
			}
		}
		);		
		
		
		function guardarTarifa()
		{
			
			
			$('#boton').attr("disabled", true);
			
				
			var idProductoTarifa = $("#idProductoTarifa").val();
		    
			if(!creandoTarifa)
				datatableModalTarifas
			    .rows( '.selected' )
			    .remove();	
			
			datatableModalTarifas.row.add( [  $("#idProductoTarifa").val(),$("#selector_tarifa").val(),$("#selector_tarifa option:selected").text(), ''+$("#fechainiciofecha_tarifa").val()+'',
			                        ''+$("#fechafinfecha_tarifa").val()+'',$("#importe").val()] )
			    	.draw();
			$("#modal_tarifas").modal('hide');

		}
		
		function limpiarModaltarifas(datatableNew)
		{
			datatableModalTarifas = datatableNew;
			$('#boton').attr("disabled", false);
			$("#idProductoTarifa").val("");
			$("#selector_tarifa").val("");
			$("#fechainiciofecha_tarifa").val($today);
			$("#fechafinfecha_tarifa").val($today);
			$("#importe").val("");
			$("#modal_tarifas").modal('show');
			$('input[name="fecha_tarifa"]').val(  $today+ ' - ' +  $today);
			$('input[name="fecha_tarifa"]').daterangepicker({
				autoUpdateInput: false,
				linkedCalendars: false,
				autoApply: true,
			  	locale: $daterangepicker_sp
				}, function(start,end) {
			  	  	 $('input[name="fecha_tarifa"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
			  	  	 $('input[name="fechainiciofecha_tarifa"]').val(start.format('DD/MM/YYYY'));
			  	  	 $('input[name="fechafinfecha_tarifa"]').val(end.format('DD/MM/YYYY'));
				 });
			
			setModalDialogSize("#modal_tarifas", "sm");	
			creandoTarifa = true;
		}
		
		function editarModaltarifas(datatableNew)
		{
			datatableModalTarifas = datatableNew;
			$('#boton').attr("disabled", false);
			var datos = datatableModalTarifas.rows( '.selected' ).data()[0];
			var creando = false;
			var idProductoTarifa=datos[0];
			var idTarifa=datos[1];
			var fechaInicio=datos[3];
			var fechaFin=datos[4];
			var importe=datos[5];
			
			$("#idProductoTarifa").val(datos[0]);
			$("#selector_tarifa").val(datos[1]);
			$("#fechainiciofecha_tarifa").val(datos[3]);
			$("#fechafinfecha_tarifa").val(datos[4]);
			$("#importe").val(datos[5]);
			$('input[name="fecha_tarifa"]').val( datos[3] + ' - ' + datos[4]);
		
			$("#modal_tarifas").modal('show');
			setModalDialogSize("#modal_tarifas", "sm");	
			creandoTarifa = false;
			
			$('input[name="fecha_tarifa"]').daterangepicker({
				autoUpdateInput: false,
				linkedCalendars: false,
				autoApply: true,
			  	locale: $daterangepicker_sp
				}, function(start,end) {
			  	  	 $('input[name="fecha_tarifa"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
			  	  	 $('input[name="fechainiciofecha_tarifa"]').val(start.format('DD/MM/YYYY'));
			  	  	 $('input[name="fechafinfecha_tarifa"]').val(end.format('DD/MM/YYYY'));
				 });
		}
</script>
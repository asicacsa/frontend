<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>




<x:parse xml="${editar_contenido}" var="editar_contenido_xml" />
<x:parse xml="${editarcontenido_selector_idiomas}" var="editarcontenido_selector_idiomas_xml" />
<x:parse xml="${editarcontenido_selector_tiposproductos}" var="editarcontenido_selector_tiposproductos_xml" />
<x:parse xml="${editarcontenido_selector_distribuidoras}" var="editarcontenido_selector_distribuidoras_xml" />
<x:parse xml="${editarcontenido_selector_marcas}" var="editarcontenido_selector_marcas_xml" />
<x:parse xml="${editarcontenido_selector_tarifasnovigentes}" var="editarcontenido_selector_tarifasnovigentes_xml" />
<x:parse xml="${editarcontenido_selector_tarifa}" var="editarcontenido_selector_tarifa_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<x:choose>
			<x:when select="not($editar_contenido_xml/Contenido)">
				<spring:message code="administracion.productos.tabs.contenidos.dialog.crear_contenido.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.productos.tabs.contenidos.dialog.editar_contenido.title" />
			</x:otherwise>
		</x:choose>		
	</h4>	
</div>



<div class="modal-body">
	<form id="form_contenido_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	<input id="idcontenido" name="idcontenido" type="hidden" value="<x:out select = "$editar_contenido_xml/Contenido/idcontenido" />" />
	<input id="i18nFieldNombre_contenido" name="i18nFieldNombre_contenido" type="hidden" value="<x:forEach select="$editar_contenido_xml/Contenido/i18ns/I18n" var="item"><x:out select="$item/idi18n" />~<x:out select="$item/idioma/ididioma" />~<x:out select="$item/nombre" />^</x:forEach>" />
	<input id="i18nFieldDescripcion_contenido" name="i18nFieldDescripcion_contenido" type="hidden" value="<x:forEach select="$editar_contenido_xml/Contenido/i18ns/I18n" var="item"><x:out select="$item/idi18n" />~<x:out select="$item/idioma/ididioma" />~<x:out select="$item/descripcion" />^</x:forEach>" />
	<input id="tarifasSeleccionadas" name="tarifasSeleccionadas" type="hidden" value="" />
	<input id="atributosSeleccionados" name="atributosSeleccionados" type="hidden" value="" />
	
	<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.contenidos.field.nombre" />*</label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editar_contenido_xml/Contenido/nombre" />">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.contenidos.field.descripcion" /></label>
				<div class="col-md-8 col-sm-8 col-xs-8">					
					<textarea name="descripcion" id="descripcion" class="form-control"  ><x:out select = "$editar_contenido_xml/Contenido/descripcion" /></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.contenidos.field.tipo" />*</label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<select name="idtipo" id="selector_tipo" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$editarcontenido_selector_tiposproductos_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.contenidos.field.aforo" /></label>
				<div class="col-md-8 col-sm-8 col-xs-8">					
					<input name="aforo" id="aforo" class="form-control"  data-inputmask="'mask': '9{0,10}'" value="<x:out select = "$editar_contenido_xml/Contenido/aforo" />"/>					
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.contenidos.field.duracion" /></label>
				<div class="col-md-3 col-sm-3 col-xs-3">	
					<input name="duracion" id="duracion" data-inputmask="'mask': '9{0,10}'" required="required" class="form-control"  value="<x:out select = "$editar_contenido_xml/Contenido/duracion" />"/>	
				
				</div>
				<div class="col-md-2 col-sm-2 col-xs-2">	
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.contenidos.field.minutos" />*</label>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-3"></div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.contenidos.field.orden" /></label>
				<div class="col-md-3 col-sm-3 col-xs-3">					
					<input name="orden" id="orden" data-inputmask="'mask': '9{0,10}'" class="form-control"  value="<x:out select = "$editar_contenido_xml/Contenido/orden" />"/>
				</div>
				<div class="col-md-5 col-sm-5 col-xs-5"></div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.contenidos.field.centroOrden" /></label>
				<div class="col-md-6 col-sm-6 col-xs-6">					
					<select name="escentrocoste" id="escentrocoste" class="form-control">
						<option value=""></option>
						<option value="1"><spring:message code="administracion.productos.tabs.contenidos.field.option.centro" /></option>
						<option value="0"><spring:message code="administracion.productos.tabs.contenidos.field.option.orden" /></option>
					</select>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-2">					
					<input name="referenciacoste" id="referenciacoste" class="form-control"  value="<x:out select = "$editar_contenido_xml/Contenido/referenciacoste" />"/>
				</div>
				
			</div>
						
	
	    	
			
	
			
			</div>
			
			<div class="col-md-6 col-sm-6 col-xs-12">
			
			  	<div class="color_form_edit">  
					<br/>			
						<div class="form-group">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="checkbox">
		                            <input type="checkbox" id= "cbDistribuidora" name= "cbDistribuidora" value="" >                          
		                    		<label class="control-label col-md-3 col-sm-3 col-xs-3">
		                    			<spring:message code="administracion.productos.tabs.contenidos.field.distribuidora" />                              	
		                            </label>
		                         	<div class="col-md-9 col-sm-9 col-xs-9">
			                            <select name="iddistribuidora" id="selector_distribuidora" class="form-control" disabled="disabled">
											<option value=""></option>
											<x:forEach select="$editarcontenido_selector_distribuidoras_xml/ArrayList/LabelValue" var="item">
												<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
											</x:forEach>
										</select>
									</div>
		                          </div>
		                    </div>
							<div class="col-md-12 col-sm-12 col-xs-12">
		                          <div class="checkbox">
		                            <input type="checkbox" id= "cbMarca" name= "cbMarca" value="" >
		                            <label class="control-label col-md-3 col-sm-3 col-xs-3">
		                            	<spring:message code="administracion.productos.tabs.contenidos.field.marca" />                               
		                            </label>
		                            <div class="col-md-9 col-sm-9 col-xs-9">
			                             <select name="idmarca" id="selector_marca" class="form-control"  disabled="disabled">
											<option value=""></option>
											<x:forEach select="$editarcontenido_selector_marcas_xml/ArrayList/LabelValue" var="item">
												<option value="<x:out select="$item/idmarca" />"><x:out select="$item/nombre" /></option>
											</x:forEach>
										</select>
									</div>
		                          </div>                        
		                        </div>						
							</div>
					</div>	
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.contenidos.field.idiomas" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<select name="idiomas[]" id="selector_idiomas" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
							<x:forEach select="$editarcontenido_selector_idiomas_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
						</select>
					</div>
				</div>
					
			<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.tipoproductos.field.atributos" /></label>
			<div class="col-md-8 col-sm-8 col-xs-8">			
					<div class="btn-group pull-right btn-datatable">
						<a type="button" class="btn btn-info" id="tab_atributos_new">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.tipoproductos.atributos.list.button.nuevo" />"> <span class="fa fa-plus"></span>
							</span>
						</a>
						<a type="button" class="btn btn-info" id="tab_atributos_edit">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.tipoproductos.atributos.list.button.editar" />"> <span class="fa fa-pencil"></span>
							</span>
						</a>
						<a type="button" class="btn btn-info" id="tab_atributos_remove">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.tipoproductos.atributos.list.button.eliminar" />"> <span class="fa fa-trash"></span>
							</span>
						</a>		
					</div>			
			</div>
				<table id="atributos_table" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
					<thead>
						<tr>
						  <th id="idatributoscontenido"></th>
						  <th id="idatributo"></th>
						  <th id="nombre"><spring:message code="administracion.productos.tabs.contenidos.field.nombre"/></th>
						  <th id="valor"><spring:message code="administracion.productos.tabs.contenidos.field.valor" /></th>
						</tr>
					</thead>
					<tbody>	
						<x:forEach select="$editar_contenido_xml/Contenido/atributoscontenidos/Atributoscontenido" var="item">
						   <tr class="seleccionable">
						   		<td id="idatributoscontenido" ><x:out select="$item/idatributoscontenido" /></td>
								<td id="idatributo"><x:out select="$item/atributo/idatributo" /></td>
								<td id="nombre"><x:out select="$item/atributo/nombre" />(<x:out select="$item/atributo/tipoatributo/nombre" />)</td>
								<td id="valor" ><x:out select="$item/valor" /></td>
							</tr>																
						</x:forEach>
						
					</tbody>
				</table>	
						
				


										
			</div>
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					
					
					<div class="col-md-6 col-sm-6 col-xs-6">
							<select class="form-control" name="tipoTarifas" id="tipoTarifas">
								<option value="0" selected="selected"><spring:message
										code="administracion.productos.tabs.contenidos.field.tarifasvigentes" /></option>
								<option value="1"><spring:message
										code="administracion.productos.tabs.contenidos.list.tarifasnovigentes" /></option>
							</select>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="btn-group pull-right btn-datatable">
							<a type="button" class="btn btn-info" id="tab_tarifas_contenidos_new">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.productos.tarifas.button.nuevo" />"> <span class="fa fa-plus"></span>
								</span>
							</a>
							<a type="button" class="btn btn-info" id="tab_tarifas_contenidos_edit">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.productos.tarifas.button.editar" />"> <span class="fa fa-pencil"></span>
								</span>
							</a>
							<a type="button" class="btn btn-info" id="tab_tarifas_contenidos_remove">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.productos.tarifas.button.eliminar" />"> <span class="fa fa-trash"></span>
								</span>
							</a>		
						</div>							
					</div>
						
					</div>
				
				<div class="form-group">		
					<div class="col-md-12 col-sm-12 col-xs-12 vigente" hidden>
						<table id="table-listaContenidostarifas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
							<thead>
								<tr>	
									<th class="id_tarifa_producto" ></th>
									<th class="id_tarifa"></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.nombretarifa" /></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.fechainicio" /></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.fechafin" /></th>				
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.importe" /></th>							
								</tr>
							</thead>
							<tbody>
								<x:forEach select="$editar_contenido_xml/Contenido/tarifaproductos/Tarifaproducto" var="item">
									<tr>
										<td><x:out select="$item/idtarifaproducto" /></td>
										<td><x:out select="$item/tarifa/idtarifa" /></td>
										<td><x:out select="$item/tarifa/nombre" /></td> 
										<td>
											<c:set var="fechainicionovigencia"><x:out select="$item/fechainiciovigencia"/></c:set>
											<fmt:parseDate value="${fechainicionovigencia}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadainicionovigencia"/>
											<fmt:formatDate value="${fechaformateadainicionovigencia}" pattern="dd/MM/yyyy" type="DATE"/>                      			     
	                          			</td>  										 
										<td>
											<c:set var="fechafinnovigencia"><x:out select="$item/fechafinvigencia"/></c:set>
											<fmt:parseDate value="${fechafinnovigencia}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadafinovigencia"/>
											<fmt:formatDate value="${fechaformateadafinovigencia}" pattern="dd/MM/yyyy" type="DATE"/>                      			     
	                          			</td>										
										<td><x:out select="$item/importe" /></td>
									</tr>
								</x:forEach>
							</tbody>
						</table>
					</div>			
					<div class="col-md-12 col-sm-12 col-xs-12 noVigente" hidden>
						<table id="table-listaProductostarifas-novigentes" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%" >
							<thead>
								<tr>	
									<th class="id_tarifa_producto" ></th>
									<th class="id_tarifa"></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.nombretarifa" /></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.fechainicio" /></th>
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.fechafin" /></th>				
									<th><spring:message code="administracion.productos.tabs.contenidos.list.header.importe" /></th>							
								</tr>
							</thead>
							<tbody>								
								<x:forEach select="$editarcontenido_selector_tarifasnovigentes_xml/ArrayList/Tarifaproducto" var="item">
									<tr>
										<td><x:out select="$item/idtarifaproducto" /></td>
										<td><x:out select="$item/tarifa/idtarifa" /></td>
										<td><x:out select="$item/tarifa/nombre" /></td> 
										<td>
											<c:set var="fechainicionovigencia"><x:out select="$item/fechainiciovigencia"/></c:set>
											<fmt:parseDate value="${fechainicionovigencia}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadainicionovigencia"/>
											<fmt:formatDate value="${fechaformateadainicionovigencia}" pattern="dd/MM/yyyy" type="DATE"/>                      			     
	                          			</td>  										 
										<td>
											<c:set var="fechafinnovigencia"><x:out select="$item/fechafinvigencia"/></c:set>
											<fmt:parseDate value="${fechafinnovigencia}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadafinovigencia"/>
											<fmt:formatDate value="${fechaformateadafinovigencia}" pattern="dd/MM/yyyy" type="DATE"/>                      			     
	                          			</td>										
										<td><x:out select="$item/importe" /></td>
									</tr>
								</x:forEach>										
																							
							</tbody>
						</table>
					
					
					
					
					</div>
				</div>
			</div>
 </form>
	
	<div class="modal-footer">

		<button id="save_ubicacion_button" type="button" class="btn btn-primary save_dialog_contenido">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>	
</div>	

<script>

	$(":input").inputmask();
	
	//********************************************************
	/*$('#referenciacoste').inputmask({alias: 'numeric', 
	                       allowMinus: false,  
	                       digits: 2, 
	                       max: 999.99});*/
	
	hideSpinner("#tab_contenidos_new");
	hideSpinner("#tab_contenidos_edit");


	$("#nombre").i18nField("#i18nFieldNombre_contenido",<c:out value="${sessionScope.i18nlangs}"  escapeXml="false" />);
	$("#descripcion").i18nField("#i18nFieldDescripcion_contenido",<c:out value="${sessionScope.i18nlangs}"  escapeXml="false" />);
	

	/*Para cambiar el aspecto de los selectores*/
	$(".select-select2").select2({
		language : "es",
		containerCssClass : "single-line"
	});	
		
	function autoajuste() {
		$(".vigente").show();
		dttarifas.columns.adjust().draw();		
	}

	
	
	var dttarifas,dtproductostarifasNoVigentes,dtatributos;
	
	$(document).ready(function() {
		dttarifas=$('#table-listaContenidostarifas').DataTable( {
			"scrollY":        "150px",
	        "scrollCollapse": true,
	        "paging":         false, 	
	         "info": false,
	         "searching": false,
	         select: { style: 'os'},
	         language: dataTableLanguage,
	         initComplete: function( settings, json ) {
	            	window.setTimeout(autoajuste, 1500);
	        	 	
	       		},
	         "columnDefs": [
	                         { "visible": false, "targets": [0,1]}
	                       ]
	         });
		
		dtproductostarifasNoVigentes=$('#table-listaProductostarifas-novigentes').DataTable( {
			"scrollY":        "150px",
	        "scrollCollapse": true,
	        "paging":         false, 	
	         "info": false,
	         "searching": false,
	         select: { style: 'os'},
	         language: dataTableLanguage,          
	         "columnDefs": [
	                         { "visible": false, "targets": [0,1]}
	                       ]
	         });
		
		dtatributos=$('#atributos_table').DataTable( {
			"paging": false,
			"info": false,
			"searching": false,
			select: { style: 'os'},
			language: dataTableLanguage,
			 "columnDefs": [
			                { "visible": false, "targets": [0,1]}
			              ]
			});
	} );
	
	
	
	
	 //****************************************************************//
	
	/*Si el Contenido tiene distribuidora...*/	
	var checkdistribuidora ="<x:out select = "$editar_contenido_xml/Contenido/distribuidora" />";
 	
 	if(checkdistribuidora!="")
 	{
		$("#cbDistribuidora").prop("checked",true);
		$("#selector_distribuidora").attr("disabled", false);	 
		$('#selector_distribuidora option[value="<x:out select = "$editar_contenido_xml/Contenido/distribuidora/iddistribuidora" />"]').attr("selected", "selected");
 	}
 
 	/*Si el Contenido tiene marca..*/
 	var checkmarca ="<x:out select = "$editar_contenido_xml/Contenido/marca" />";
 	
 	if(checkmarca!="")
 	{
		$("#cbMarca").prop("checked",true);
	 	$("#selector_marca").attr("disabled", false);	 
	 	$('#selector_marca option[value="<x:out select = "$editar_contenido_xml/Contenido/marca/idmarca" />"]').attr("selected", "selected");
 	}
	
	
	var selectedValues = new Array();
	<x:forEach select="$editar_contenido_xml/Contenido/idiomacontenidocontenidos/Idiomaprod/idiomacontenido" var="item">
	selectedValues.push('<x:out select="$item/ididiomacontenido" />');
	</x:forEach>
	$('#selector_idiomas').val(selectedValues);
	$('#selector_idiomas').trigger('change.select2');
	
	$('#selector_tipo option[value="<x:out select = "$editar_contenido_xml/Contenido/tipoproducto/idtipoproducto" />"]').attr("selected", "selected");
	
	
	$('#escentrocoste option[value="<x:out select = "$editar_contenido_xml/Contenido/escentrocoste" />"]').attr("selected", "selected");
	
	
	/*Inicialmente se muestran las tarifas vigentes*/	
     $("#tipoTarifas option[value=0]").attr("selected",true);           
     $(".vigentes").show();
     $(".novigentes").hide();	                	
                    
	
	/*Si selecciona el check distribuidora se activa el selector distribuidora*/	
	$("#cbDistribuidora").on("click", function(e) {		
		if ($("#cbDistribuidora").prop("checked"))
			$("#selector_distribuidora").attr("disabled", false);		
		else{			
			$("#selector_distribuidora").attr("disabled", true);
			/*Si deselecciona el check borramos la selección del selector*/	
			$("#selector_distribuidora").val($("#selector_distribuidora option:first").val());			 
		}
	})

	
	/*Si selecciona el check marca se activa el selector marca*/	
	$("#cbMarca").on("click", function(e) {		
		if ($("#cbMarca").prop("checked"))
			$("#selector_marca").attr("disabled", false);		
		else{			
			$("#selector_marca").attr("disabled", true);
			/*Si deselecciona el check borramos la selección del selector*/	
			$("#selector_marca").val($("#selector_marca option:first").val());			 
		}
	})		
	
	/*Dependiendo de si selecciona tarifas vigentes o no vigentes se mostraran en la tabla las tarifas correspondientes. Los botones de añadir 
	y eliminar sólo estarán visibles para tarifas vigentes*/
	
	$( "#tipoTarifas").change(function() {
		if($("#tipoTarifas").val()==0){
        	$(".vigente").show();
        	$(".noVigente").hide();
            $("#tab_tarifas_contenidos_new").show();
            $("#tab_tarifas_contenidos_edit").show();
            $("#tab_tarifas_contenidos_remove").show();	                	
        }
        
        else{
        	$(".vigente").hide();
        	$(".noVigente").show();
        	dtproductostarifasNoVigentes.columns.adjust().draw();
            $("#tab_tarifas_contenidos_new").hide();
            $("#tab_tarifas_contenidos_edit").hide();
            $("#tab_tarifas_contenidos_remove").hide();                  	
        }
	})	
	
	
	$("#tab_tarifas_contenidos_remove").on("click", function(e) { 
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="administracion.productos.tabs.productos.tarifas.eliminar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
			  buttons: { closer: false, sticker: false	},
			  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
			   dttarifas
			    .rows( '.selected' )
			    .remove()
			    .draw();
		   })
		
		})
		
	$("#tab_atributos_remove").on("click", function(e) { 
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="administracion.productos.tabs.contenidos.atributos.eliminar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
			  buttons: { closer: false, sticker: false	},
			  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
			   dtatributos
			    .rows( '.selected' )
			    .remove()
			    .draw();
		   })
		
		})
	
		$("#tab_atributos_new").on("click", function(e) { 	
			var idtipoproducto = $("#selector_tipo").val();
			if(idtipoproducto!="")
			{
			var data = dtatributos.rows().data();
			var idsSeleccionados = "";
			data.each(function (value, index) {
				idsSeleccionados = idsSeleccionados + dtatributos.rows( index ).data()[0][1]+"|";
			})

			
			var url = "<c:url value='/ajax/admon/productos/contenidos/show_atributo.do'/>?idtipoproducto="+idtipoproducto+"&idsSeleccionados="+idsSeleccionados;
			
			$("#modal-dialog-form-2 .modal-content").load(url, function() {										  
				$("#modal-dialog-form-2").modal('show');
				setModalDialogSize("#modal-dialog-form-2", "sm");				
			});
			
			}
			else
			{
				new PNotify({
				      title: '<spring:message code="common.dialog.text.error" />',
				      text: '<spring:message code="administracion.productos.tabs.tipoproductos.atributos.list.alert.seleccione_tipo" />',
					  type: "error",
					  delay: 5000,
					  buttons: { sticker: false }
				   });
			}
		})
		
		
		function editarModalAtributo2()
	    {
		var datos = dtatributos.rows( '.selected' ).data()[0];
		var idatributoscontenido = datos[0];
		var idatributo = datos[1];
		var nombre = datos[2];
		var valor = datos[3];
					
		var url = encodeURI("<c:url value='/ajax/admon/productos/contenidos/show_atributo.do'/>?idatributoscontenido="+idatributoscontenido+"&idatributo="+idatributo+"&nombre="+nombre+"&valor="+valor);

		$("#modal-dialog-form-2 .modal-content").load(url, function() {										  
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "sm");				
		});
		
	    }
		
		
		
		
		$("#tab_atributos_edit").on("click", function(e) { 	
			
			if(dtatributos.rows( { selected: true } ).data().length==1)		
		    {
				editarModalAtributo2();
		    }
		    else
		    	{
		    	if(dtatributos.rows( { selected: true } ).data().length==0)
			    	{
					new PNotify({
					      title: '<spring:message code="common.dialog.text.error" />',
					      text: '<spring:message code="administracion.productos.tabs.tipoproductos.atributos.list.alert.ninguna_seleccion" />',
						  type: "error",
						  delay: 5000,
						  buttons: { sticker: false }
					   });
			    	}
			    	
				else
					{
					new PNotify({
					      title: '<spring:message code="common.dialog.text.error" />',
					      text: '<spring:message code="administracion.productos.tabs.tipoproductos.atributos.list.alert.seleccion_multiple" />',
						  type: "error",
						  delay: 5000,
						  buttons: { sticker: false }
					   });
					}
				}		


		})		

		$("#tab_tarifas_contenidos_new").on("click", function(e) { 	
			limpiarModaltarifas(dttarifas);
		})
		
		
	
		$("#tab_tarifas_contenidos_edit").on("click", function(e) { 
		
		if(dttarifas.rows( { selected: true } ).data().length==1)		
	    {
			editarModaltarifas(dttarifas);
	    }
	    else
	    	{
	    	if(dttarifas.rows( { selected: true } ).data().length==0)
		    	{
				new PNotify({
				      title: '<spring:message code="common.dialog.text.error" />',
				      text: '<spring:message code="administracion.productos.tabs.productos.tarifas.list.alert.ninguna_seleccion" />',
					  type: "error",
					  delay: 5000,
					  buttons: { sticker: false }
				   });
		    	}
		    	
			else
				{
				new PNotify({
				      title: '<spring:message code="common.dialog.text.error" />',
				      text: '<spring:message code="administracion.productos.tabs.productos.tarifas.list.alert.seleccion_multiple" />',
					  type: "error",
					  delay: 5000,
					  buttons: { sticker: false }
				   });
				}
			}
		})			
	
		//Cargamos el selector de tarifas
		$('#selector_tarifa')
 		   .find('option')
    		.remove()
    		.end()
    		.append('<option value=""></option>')
    		.val('whatever');
	
		<x:forEach select="$editarcontenido_selector_tarifa_xml/ArrayList/LabelValue" var="item">
			$("#selector_tarifa").append('<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>');
		</x:forEach>	
		
		
		function guardarDatosContenido()
		{
			 // Procesamos las tarifas
			 var data = dttarifas.rows().data();
			 var tarifas = "";
			 data.each(function (value, index) {
				var linea = ""+dttarifas.rows( index ).data()[0][0]+"|";
				linea = linea + dttarifas.rows( index ).data()[0][1]+"|";
				linea = linea + dttarifas.rows( index ).data()[0][3]+"|";
				linea = linea + dttarifas.rows( index ).data()[0][4]+"|";
				linea = linea + dttarifas.rows( index ).data()[0][5];
				if(tarifas!="")
					tarifas = tarifas + "$";
				tarifas = tarifas + linea;				
			 });
			 
			 $("#tarifasSeleccionadas").val(tarifas);
			 
			 // Procesamos los atributos
			 data = dtatributos.rows().data();
			 var atributos = "";
			 data.each(function (value, index) {
				var linea = ""+dtatributos.rows( index ).data()[0][0]+"|";
				linea = linea + dtatributos.rows( index ).data()[0][1]+"|";
				linea = linea + dtatributos.rows( index ).data()[0][3];
				if(atributos!="")
					atributos = atributos + "$";
				atributos = atributos + linea;				
			 });
			 $("#atributosSeleccionados").val(atributos);			 
					

			
			
			data = $("#form_contenido_data").serializeObject();
	
			showSpinner("#modal-dialog-form .modal-content");
	
			$.ajax({
				type : "post",
				url : "<c:url value='/ajax/admon/productos/productos/save_contenido.do'/>",
				timeout : 100000,
				data : data,
				success : function(data) {
					hideSpinner("#modal-dialog-form .modal-content");
					
					$("#modal-dialog-form").modal('hide');
					
					dt_listprodcontenidos.ajax.reload(null,false);
					
					new PNotify({
						title : '<spring:message code="common.dialog.text.operacion_realizada" />',
						text : '<spring:message code="common.dialog.text.datos_guardados" />',
						type : "success",
						delay : 5000,
						buttons : {
							closer : true,
							sticker : false
						}
					});
					
					
				},
				error : function(exception) {
					hideSpinner("#modal-dialog-form .modal-content");
	
					new PNotify({
						title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						text : exception.responseText,
						type : "error",
						delay : 5000,
						buttons : {
							closer : true,
							sticker : false
						}
					});
				}
			});

		}
		
		
		$("#form_contenido_data").validate({
			onfocusout : false,
			onkeyup : false,
			unhighlight: function(element, errClass) {
	            $(element).popover('hide');
			},		
			errorPlacement : function(err, element) {
				err.hide();
				$(element).attr('data-content', err.text());
				$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
				$(element).popover('show');									
			},
			submitHandler : function(form) {
				guardarDatosContenido()
			}
		}
		);		
		
		
		
		
		$(".save_dialog_contenido").on("click", function(e) {
			$("#form_contenido_data").submit();	
		})

</script>
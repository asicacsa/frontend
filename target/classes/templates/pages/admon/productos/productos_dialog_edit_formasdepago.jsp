<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editarformapago_datos_formapago}" var="editarformapago_datos_formapago_xml" />
<x:parse xml="${editarformapago_selector_tipoformapago}" var="editarformapago_selector_tipoformapago_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">x</span>
	</button>
	<h4 class="modal-title">
		<x:choose>
			<x:when select="not($editarformapago_datos_formapago_xml/Formapago)">
				<spring:message code="administracion.productos.tabs.formasdepago.dialog.crear_formadepago.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.productos.tabs.formasdepago.dialog.editar_formadepago.title" />
			</x:otherwise>
		</x:choose>
	</h4>
</div>

<div class="modal-body" id="principal">

	<form id="form_formapago_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

		<input id="idformapago" name="idformapago" type="hidden" value="<x:out select = "$editarformapago_datos_formapago_xml/Formapago/idformapago" />" />
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			 <div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.codigoSap" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="codigo" id="codigo" type="text" class="form-control" required="required" value="<x:out select = "$editarformapago_datos_formapago_xml/Formapago/codigosap" />">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.nombre" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<textarea class="form-control" rows="3" name="nombre" id="nombre" required="required"><x:out select="$editarformapago_datos_formapago_xml/Formapago/nombre" /></textarea>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.tipo" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="idtipoformapago" id="selector_tipoformapago" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$editarformapago_selector_tipoformapago_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.descripcion" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<textarea class="form-control" rows="3" name="descripcion" id="descripcion"><x:out select="$editarformapago_datos_formapago_xml/Formapago/descripcion" /></textarea>
				</div>
			</div>	
			
				<div class="form-group date-picker">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.fechacaducidad" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						  <a type="button" class="btn btn-default btn-clear-date" id="button_vigencia_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.formasdepago.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
						  </a>			
                        <div class="input-prepend input-group">
                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                          <input type="text" name="caducidad" id="caducidad" class="form-control" value="" readonly/>
                          <input type="hidden" required="required" name="fechacaducidad" value="<x:out select = "$editarformapago_datos_formapago_xml/Formapago/fechacaducidad" />"/>
                        </div>
					</div>
				</div>		
				
			
			
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.orden" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">					
					<input name="orden" id="orden" type="text" class="form-control"  data-inputmask="'mask': '9{0,10}'" value="<x:out select = "$editarformapago_datos_formapago_xml/Formapago/orden" />">
				</div>
			</div>					
					
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.credito" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input id="credito-switch" name="credito" type="checkbox" class="js-switch" <x:if select="$editarformapago_datos_formapago_xml/Formapago/credito='1'">checked=""</x:if> data-switchery="true" style="display: none;">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.formasdepago.field.admiteformas" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input id="formapago-switch" name="distintaformapago" type="checkbox" class="js-switch" <x:if select="$editarformapago_datos_formapago_xml/Formapago/distintaformapagodevolucion='1'">checked=""</x:if> data-switchery="true" style="display: none;">
				</div>
			</div>
			
			</div>
		  
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="modal-footer">
					<button id="save_perfil_button" type="submit" class="btn btn-primary save_dialog">
						<spring:message code="common.button.save" />
					</button>
					<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
						<spring:message code="common.button.cancel" />
					</button>
				</div>
			</div>
	</form>
</div>

<script>

$(":input").inputmask();
//*********************************************

hideSpinner("#tab_formasdepago_new");
hideSpinner("#tab_formasdepago_edit");
//********************************************************************************
$(".js-switch").each(function(index) {
	var s = new Switchery(this, {
		size : 'small'
	});
});
//********************************************************************************
$('#selector_tipoformapago option[value="<x:out select = "$editarformapago_datos_formapago_xml/Formapago/tipoformapago/idtipoformapago" />"]').attr("selected", "selected");
//********************************************************************************
//Caducidad
$today= moment().format("DD/MM/YYYY");
dia_inicio = $today;

if($('input[name="fechacaducidad"]').val()!='')
{	
dia_inicio = $('input[name="fechacaducidad"]').val().substring(0, 10);
}

$('input[name="caducidad"]').val( dia_inicio);

$('input[name="caducidad"]').daterangepicker({
	singleDatePicker: true,
	autoUpdateInput: false,
	linkedCalendars: false,
	showDropdowns: true,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start) {
  	  	 $('input[name="caducidad"]').val(start.format('DD/MM/YYYY'));
  	  	 $('input[name="fechacaducidad"]').val(start.format('DD/MM/YYYY'));
  	  	 
	 });
//*******************************************************************************
$("#button_vigencia_clear").on("click", function(e) {
    $('input[name="caducidad"]').val('');
    $('input[name="fechacaducidad"]').val('');    
});

//********************************************************************************
$("#form_formapago_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFormDataFormaPago();
		}
	}
	);
	//********************************************************************************
	
	function saveFormDataFormaPago() 
	{
		var data = $("#form_formapago_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/productos/formaspago/save_formaspago.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
			
				
				$("#modal-dialog-form").modal('hide');
				
				dt_listformaspago.ajax.reload(null,false);
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});		
	}
</script>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editartarifa_datos_tarifa}" var="editartarifa_datos_tarifa_xml" />
<x:parse xml="${editartarifa_selector_unidadnegocio}" var="editartarifa_selector_unidadnegocio_xml" />
<x:parse xml="${editartarifa_selector_perfilesvisitante}" var="editartarifa_selector_perfilesvisitante_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">x</span>
	</button>
	<h4 class="modal-title">
		<x:choose>
			<x:when select="not($editartarifa_datos_tarifa_xml/Tarifa)">
				<spring:message code="administracion.productos.tabs.tarifas.dialog.crear_tarifa.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.productos.tabs.tarifas.dialog.editar_tarifa.title" />
			</x:otherwise>
		</x:choose>
	</h4>
</div>
<div class="modal-body" id="principal">

	<form id="form_tarifa_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

		<input id="idtarifa" name="idtarifa" type="hidden" value="<x:out select = "$editartarifa_datos_tarifa_xml/Tarifa/idtarifa" />" />
		<input id="i18nFieldNombre_tarifa" name="i18nFieldNombre_tarifa" type="hidden" value="<x:forEach select="$editartarifa_datos_tarifa_xml/Tarifa/i18ns/I18n" var="item"><x:out select="$item/idi18n" />~<x:out select="$item/idioma/ididioma" />~<x:out select="$item/nombre" />^</x:forEach>" />
		<div class="col-md-6 col-sm-6 col-xs-12">

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.tarifas.field.nombre" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editartarifa_datos_tarifa_xml/Tarifa/nombre" />">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.tarifas.field.descripcion" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<textarea class="form-control" rows="3" name="descripcion" id="descripcion"><x:out select="$editartarifa_datos_tarifa_xml/Tarifa/descripcion" /></textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.tarifas.field.unidadnegocio" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="idunidadnegocio" id="selector_unidadnegocio" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$editartarifa_selector_unidadnegocio_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.tarifas.field.orden" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">					
					<input name="orden" id="orden" type="text" data-inputmask="'mask': '9{0,10}'" class="form-control"  value="<x:out select = "$editartarifa_datos_tarifa_xml/Tarifa/orden" />">
				</div>
			</div>
			
			
		</div>

		<div class="col-md-6 col-sm-6 col-xs-12">
			
			<div class="form-group" id="selector_perfilesvisitante_form_group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.tarifas.field.perfilesvisitante" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="perfilesvisitante[]" id="selector_perfilesvisitante" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
						<x:forEach select="$editartarifa_selector_perfilesvisitante_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.tarifas.field.disponibleabono" /></label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<input id="abono-switch" name="abono" type="checkbox" class="js-switch" <x:if select="$editartarifa_datos_tarifa_xml/Tarifa/abono='1'">checked=""</x:if> data-switchery="true" style="display: none;">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.tarifas.field.disponiblebono" /></label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<input id="bono-switch" name="bono" type="checkbox" class="js-switch" <x:if select="$editartarifa_datos_tarifa_xml/Tarifa/bono='1'">checked=""</x:if> data-switchery="true" style="display: none;">
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.tarifas.field.baseinvitaciones" /></label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<input id="base-switch" name="basesapinvitacion" type="checkbox" class="js-switch" <x:if select="$editartarifa_datos_tarifa_xml/Tarifa/basesapinvitacion='1'">checked=""</x:if> data-switchery="true" style="display: none;">
				</div>
			</div>
			
			
		

		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_tarifa_button" type="submit" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>

	</form>

</div>

<script>

	$(":input").inputmask();
	//*********************************************
	hideSpinner("#tab_tarifas_new");
	hideSpinner("#tab_tarifas_edit");
	//********************************************************************************
	$("#nombre").i18nField("#i18nFieldNombre_tarifa",<c:out value="${sessionScope.i18nlangs}"  escapeXml="false" />);
	//********************************************************************************
	$(".select-select2").select2({
		language : "es",
		containerCssClass : "single-line"
	});	
	//********************************************************************************
	$('#selector_unidadnegocio option[value="<x:out select = "$editartarifa_datos_tarifa_xml/Tarifa/unidadnegocio/idunidadnegocio" />"]').attr("selected", "selected");
	//********************************************************************************
	$(".js-switch").each(function(index) {
		var s = new Switchery(this, {
			size : 'small'
		});
	});
	//********************************************************************************
	var selectedValues = new Array();
	<x:forEach select="$editartarifa_datos_tarifa_xml/Tarifa/perfilvisitantes/Perfilvisitante" var="item">	
	selectedValues.push('<x:out select="$item/idperfilvisitante" />');
	</x:forEach>
	$('#selector_perfilesvisitante').val(selectedValues);
	$('#selector_perfilesvisitante').trigger('change.select2');	
	//********************************************************************************	
	$("#form_tarifa_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFormDataTarifa();
		}
	}
	);
	//********************************************************************************
	
	function saveFormDataTarifa() 
	{
		var data = $("#form_tarifa_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/productos/tarifas/save_tarifa.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
			
				
				$("#modal-dialog-form").modal('hide');
				
				dt_listtarifas.ajax.reload(null,false);
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});		
	}
</script>
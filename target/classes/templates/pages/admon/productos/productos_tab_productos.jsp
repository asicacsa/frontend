<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<x:parse xml="${tabproductos_data}" var="tabproductos_datas_xml" />

<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2>
			<spring:message code="common.text.filter_list" />
		</h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">

		<form id="form_filter_list_productos"class="form-horizontal form-label-left">	
			
			<div class="col-md-6 col-sm-6 col-xs-6">
				<div class="form-group">					
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message	code="administracion.productos.tabs.productos.field.unidad" /></label>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<select class="form-control" name="idunidadnegocio">
							<option value=""></option>
							<x:forEach select="$tabproductos_datas_xml/ArrayList/LabelValue"
								var="item">
								<option value="<x:out select="$item/value" />"><x:out
										select="$item/label" /></option>
							</x:forEach>
						</select>
					</div>
				</div>	
			</div>
			<div class="col-md-6 col-sm-6 col-xs-6">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.productos.field.mostrar" /></label>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<select class="form-control" name="caducados">
							<option value="0" selected="selected"><spring:message
									code="administracion.productos.tabs.productos.list.vigentes" /></option>
							<option value="1"><spring:message
									code="administracion.productos.tabs.productos.list.todos" /></option>
						</select>
					</div>
				</div>

			
			</div>


			<div class="clearfix"></div>
			<div class="ln_solid"></div>
			<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
				<button id="button_filter_list_productos" type="button" class="btn btn-success pull-right">
					<spring:message code="common.button.filter" />
				</button>
			</div>	
		</form>
	</div>
</div>

   <div class="col-md-12 col-sm-12 col-xs-12">
   
   <div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_productos_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.productos.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_productos_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.productos.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_productos_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.productos.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>		
	</div>
   
   
	<table id="datatable-lista-productos" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="administracion.productos.tabs.productos.list.header.nombre" /></th>
				<th><spring:message code="administracion.productos.tabs.productos.list.header.descripcion" /></th>
				<th><spring:message code="administracion.productos.tabs.productos.list.header.vigente" /></th>				
				<th><spring:message code="administracion.productos.tabs.productos.list.header.fecha" /></th>
				<th><spring:message code="administracion.productos.tabs.productos.list.header.recinto" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>


<script>
	var dt_listproductos=$('#datatable-lista-productos').DataTable( {	
	 ajax: {
	 url: "<c:url value='/ajax/admon/productos/productos/list_productos.do'/>",
	 rowId: 'idproducto',
	 type: 'POST',
	 dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Producto)); return(""); 
	 							},
	 							error: function (xhr, error, thrown) {
	 						         if (xhr.responseText=="403") {
	 						               $("#productos-tab").hide();
	 						         }     
	 						        else
       					      	 {
       					      	 	$("#datatable-lista-productos_processing").hide();
       					      		new PNotify({
       										title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
       										text : xhr.responseText,
       										type : "error",
       										delay : 5000,
       										buttons : {
       											closer : true,
       											sticker : false
       										}					
       									});
       					      	 }
	 						  },	 							
	 data: function (params) { return($("#form_filter_list_productos").serializeObject()); }
	 },
	 initComplete: function( settings, json ) {
	 $('a#menu_toggle').on("click", function () {if (dt_listproductos.data().count()>0) dt_listproductos.columns.adjust().draw(); });
	 },
	 columns: [
	 { data: "nombre", type: "spanish-string" ,  defaultContent:""} ,
	 { data: "descripcion", type: "spanish-string" ,  defaultContent:""},	 
	 { data: "fechainiciovigencia", type: "spanish-string" ,  defaultContent:"",
		 render: function ( data, type, row, meta ) {			 
    	  	  return data.substr(0, 10);}	 
	 } ,
	 { data: "fechafinvigencia", type: "spanish-string" ,  defaultContent:"",
		 render: function ( data, type, row, meta ) {			 
   	  	  return data.substr(0, 10);} 
	 
		 } ,
	 { data: "tipoprodproductos.Tipoprodproducto", className: "cell_centered",
     	  render: function ( data, type, row, meta ) { return showListInCell(data,"<spring:message code="administracion.productos.tabs.productos.list.text.recintos" />","tipoproducto.recinto.nombre"); }	
           }, 	 
	
	 ],    
	 drawCallback: function( settings ) {
		 activateTooltipsInTable('datatable-lista-productos')
	 },
	 select: { style: 'os', selector:'td:not(:last-child)'},
	 language: dataTableLanguage,
	 processing: true,
	 } );
	
	insertSmallSpinner("#datatable-lista-productos_processing");
	
	/*********************************************************BOT�N FILTRAR*******************************************************************/
	$("#button_filter_list_productos").on("click", function(e) {
		dt_listproductos.ajax.reload();
	})
	/***********************************************BOT�N ELIMIMAR***********************************/
  $("#tab_productos_remove").on("click", function(e) { 
		
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="administracion.productos.tabs.productos.list.confirm.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
  		  buttons: { closer: false, sticker: false	},
  		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
	
		   dt_listproductos.processing(true);
			
			var data = sanitizeArray(dt_listproductos.rows( { selected: true } ).data(),"idproducto");
		   
			$.ajax({
				contenttype: "application/json; charset=utf-8",
				type : "post",
				url : "<c:url value='/ajax/admon/productos/contenidos/remove_productos.do'/>",
				timeout : 100000,
				data: { data: data.toString() }, 
				success : function(data) {
					dt_listproductos.ajax.reload();					
				},
				error : function(exception) {
					dt_listproductos.processing(false);
					
					new PNotify({
					      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					      text: exception.responseText,
						  type: "error",		     
						  delay: 5000,
						  buttons: { sticker: false }
					   });			
				}
			});

	   }).on('pnotify.cancel', function() {
	   });		 

});
	
-	 $("#tab_productos_new").on("click", function(e) { 
			$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/productos/productos/show_producto.do'/>", function() {										  
				$("#modal-dialog-form").modal('show');
				setModalDialogSize("#modal-dialog-form", "lg");
			});
			showButtonSpinner("#tab_productos_new");
		})
		
	
 $("#tab_productos_edit").on("click", function(e) { 
	var data = sanitizeArray(dt_listproductos.rows( { selected: true } ).data(),"idproducto");
		
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.productos.tabs.productos.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.productos.tabs.productos.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#tab_productos_edit");
	$("#modal-dialog-form .modal-content:first").load("<c:url value='/ajax/admon/productos/productos/show_producto.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "lg");
	});
})
</script>

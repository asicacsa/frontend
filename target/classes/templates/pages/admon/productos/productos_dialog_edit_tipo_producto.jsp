<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<x:parse xml="${editar_tipo_producto}" var="editar_tipo_producto_xml" />
<x:parse xml="${editar_tipo_producto_selector_recintos}" var="editar_tipo_producto_selector_recintos_xml" />
<x:parse xml="${editar_tipo_producto_selector_atributo}" var="editar_tipo_producto_selector_atributo_xml" />



<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<x:choose>
			<x:when select="not($editar_tipo_producto_xml/Tipoproducto)">
				<spring:message code="administracion.productos.tabs.productos.dialog.crear_tipo_producto.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.productos.tabs.productos.dialog.editar_tipo_producto.title" />
			</x:otherwise>
		</x:choose>		
	</h4>	
</div>

<div class="modal-body">
	<form id="form_tipo_producto_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	   	    <input id="idtipoproducto" name="idtipoproducto" type="hidden" value="<x:out select = "$editar_tipo_producto_xml/Tipoproducto/idtipoproducto" />" />
	   	    <input id="atributosSeleccionados" name="atributosSeleccionados" type="hidden" value="" />
			<input id="i18nFieldNombre_tipoproducto" name="i18nFieldNombre_tipoproducto" type="hidden" value="<x:forEach select="$editar_tipo_producto_xml/Tipoproducto/i18ns/I18n" var="item"><x:out select="$item/idi18n" />~<x:out select="$item/idioma/ididioma" />~<x:out select="$item/nombre" />^</x:forEach>" />

	    	<div class="col-md-6 col-sm-6 col-xs-6">
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.tipoproductos.field.nombre" />*</label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<input name="nombre" id="nombre" type="text" class="form-control required" value="<x:out select = "$editar_tipo_producto_xml/Tipoproducto/nombre" />">
				</div>
			</div>	
			
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.tipoproductos.field.descripcion" /></label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<textarea name="descripcion" id="descripcion" class="form-control"  ><x:out select = "$editar_tipo_producto_xml/Tipoproducto/descripcion" /></textarea>
				</div>
			</div>	
		
			<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.tipoproductos.field.maxusos" /></label>
					<div class="col-md-2 col-sm-2 col-xs-2">
						<input name="nummaxusosentrada" id="nummaxusosentrada" type="text" data-inputmask="'mask': '9{0,10}'" class="form-control"  value="<x:out select = "$editar_tipo_producto_xml/Tipoproducto/nummaxusosentrada" />">						
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6"></div>
			</div>	
	
			<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.tipoproductos.field.orden" /></label>
					<div class="col-md-2 col-sm-2 col-xs-2">
						<input name="orden" id="orden" type="text" class="form-control" data-inputmask="'mask': '9{0,10}'" value="<x:out select = "$editar_tipo_producto_xml/Tipoproducto/orden" />">					
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6"></div>
			</div>	
			
	
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.tipoproductos.field.recinto" /></label>
				<div class="col-md-8 col-sm-8 col-xs-6">
					<select name="idrecinto" id="selector_recinto" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$editar_tipo_producto_selector_recintos_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>				
			
			</div>
		

		
		<div class="col-md-6 col-sm-6 col-xs-6">
			<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.tipoproductos.field.atributos" /></label>
			<div class="col-md-8 col-sm-8 col-xs-8">			
					<div class="btn-group pull-right btn-datatable">
						<a type="button" class="btn btn-info" id="tab_atributos_new">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.tipoproductos.atributos.list.button.nuevo" />"> <span class="fa fa-plus"></span>
							</span>
						</a>
						
						<a type="button" class="btn btn-info" id="tab_atributos_remove">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.tipoproductos.atributos.list.button.eliminar" />"> <span class="fa fa-trash"></span>
							</span>
						</a>		
					</div>			
			</div>
			<table id="atributos_table" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
					  <th id="idatributo"></th>
					  <th id="idtipoatributo"></th>
					  <th id="nombre"><spring:message code="administracion.productos.tabs.tipoproductos.field.nombre"/></th>
					  <th id="nombreatributo"><spring:message code="administracion.productos.tabs.tipoproductos.field.tipo" /></th>
					</tr>
				</thead>
				<tbody>	
					<x:forEach select="$editar_tipo_producto_xml/Tipoproducto/atributos/Atributo" var="item">
					   <tr class="seleccionable">
					   		<td id="idatributo" ><x:out select="$item/idatributo" /></td>
							<td id="idtipoatributo"><x:out select="$item/tipoatributo/idtipoatributo" /></td>
							<td id="nombre"><x:out select="$item/nombre" /></td>
							<td id="nombreTipo" ><x:out select="$item/tipoatributo/nombre" /></td>
						</tr>																
					</x:forEach>
					
				</tbody>
			</table>			
		
		</div>
	</form>
	<div class="modal-footer">

		<button id="save_ubicacion_button" type="button" class="btn btn-primary save_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>


<script>
	
$(":input").inputmask();
//*********************************************

	hideSpinner("#tab_tipoproductos_new");
	hideSpinner("#tab_tipoproductos_edit");
	
	$('#selector_recinto option[value="<x:out select = "$editar_tipo_producto_xml/Tipoproducto/recinto/idrecinto" />"]').attr("selected", "selected");
	
	$("#nombre").i18nField("#i18nFieldNombre_tipoproducto",<c:out value="${sessionScope.i18nlangs}"  escapeXml="false" />);

	var dtatributos;

	$(document).ready(function() {
		dtatributos=$('#atributos_table').DataTable( {
			"paging": false,
			"info": false,
			"searching": false,
			select: { style: 'os'},
			language: dataTableLanguage,
			 "columnDefs": [
			                { "visible": false, "targets": [0,1]}
			              ]
			});
	})


	$('#selector_atributo')
	    .find('option')
	    .remove()
	    .end()
	    .append('<option value=""></option>')
	    .val('whatever')
	;
	
	<x:forEach select="$editar_tipo_producto_selector_atributo_xml/ArrayList/LabelValue" var="item">
		$('#selector_atributo').append($('<option>', {
	   		value: '<x:out select="$item/value" />',
	    	text: '<x:out select="$item/label" />'
		}));	
	</x:forEach>	
	
	$("#tab_atributos_remove").on("click", function(e) { 
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="administracion.productos.tabs.contenidos.atributos.eliminar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
			  buttons: { closer: false, sticker: false	},
			  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
			   dtatributos
			    .rows( '.selected' )
			    .remove()
			    .draw();
		   })
		
		})
	
	
	
	$("#tab_atributos_new").on("click", function(e) { 	
		limpiarModalAtributo(dtatributos);
	})
	
	$("#tab_atributos_edit").on("click", function(e) { 	
		
		if(dtatributos.rows( { selected: true } ).data().length==1)		
	    {
			editarModalAtributo(dtatributos);
	    }
	    else
	    	{
	    	if(dtatributos.rows( { selected: true } ).data().length==0)
		    	{
				new PNotify({
				      title: '<spring:message code="common.dialog.text.error" />',
				      text: '<spring:message code="administracion.productos.tabs.tipoproductos.atributos.list.alert.ninguna_seleccion" />',
					  type: "error",
					  delay: 5000,
					  buttons: { sticker: false }
				   });
		    	}
		    	
			else
				{
				new PNotify({
				      title: '<spring:message code="common.dialog.text.error" />',
				      text: '<spring:message code="administracion.productos.tabs.tipoproductos.atributos.list.alert.seleccion_multiple" />',
					  type: "error",
					  delay: 5000,
					  buttons: { sticker: false }
				   });
				}
			}		
		
	})
	
	$(".save_dialog").on("click", function(e) { 	
		
		 $("#form_tipo_producto_data").submit();	
	})
	
	
	$("#form_tipo_producto_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			guardarTipoProducto()
		}
	}
	);
	
	
	
	
	function guardarTipoProducto()
	{
		
		// Procesamos los atributos
		 var data = dtatributos.rows().data();
		 var atributos = "";
		 data.each(function (value, index) {
			var linea = ""+dtatributos.rows( index ).data()[0][0]+"|";
			linea = linea + dtatributos.rows( index ).data()[0][1]+"|";
			linea = linea + dtatributos.rows( index ).data()[0][2];
			if(atributos!="")
				atributos = atributos + "$";
			atributos = atributos + linea;				
		 });
		 $("#atributosSeleccionados").val(atributos);
				
		
		var data = $("#form_tipo_producto_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/productos/productos/save_tipoproducto.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				
				$("#modal-dialog-form").modal('hide');
				
				dt_listtiposproductos.ajax.reload(null,false);
				
				recargar_tipos_producto()
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});

	}

	
</script>
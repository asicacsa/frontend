
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!-- Pesta�as ------------------------------------------------------------->
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 maintab">
		<div class="" role="tabpanel" data-example-id="togglable-tabs">
			<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
				<li role="presentation" class="active">
					<a href="#tab_contenidos" id="contenidos-tab" role="tab" data-toggle="tab" aria-expanded="true">
					<spring:message	code="administracion.productos.tabs.contenidos.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_productos" role="tab" id="productos-tab" data-toggle="tab" aria-expanded="false">
					<spring:message	code="administracion.productos.tabs.productos.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_tiposproductos"	role="tab" id="tiposproductos-tab" data-toggle="tab" aria-expanded="false">
					<spring:message	code="administracion.productos.tabs.tiposproductos.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_clasificacion" role="tab" id="clasificacion-tab" data-toggle="tab"	aria-expanded="false">
					<spring:message	code="administracion.productos.tabs.clasificacion.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_descuentos"	role="tab" id="descuentos-tab" data-toggle="tab" aria-expanded="false">
					<spring:message	code="administracion.productos.tabs.descuentos.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_tarifas"	role="tab" id="tarifas-tab" data-toggle="tab" aria-expanded="false">
					<spring:message	code="administracion.productos.tabs.tarifas.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_perfiles" role="tab" id="perfiles-tab" data-toggle="tab" aria-expanded="false">
					<spring:message	code="administracion.productos.tabs.perfiles.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_canales"	role="tab" id="canales-tab" data-toggle="tab" aria-expanded="false">
					<spring:message	code="administracion.productos.tabs.canales.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_formaspago"	role="tab" id="formaspago-tab" data-toggle="tab" aria-expanded="false">
					<spring:message code="administracion.productos.tabs.formaspago.title" /></a></li>
			</ul>
		</div>
	</div>
</div>

<!-- Area de trabajo ------------------------------------------------------>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel thin_padding">
			<div id="myTabContent" class="tab-content">				
				<div role="tabpanel" class="tab-pane fade active in" id="tab_contenidos" aria-labelledby="contenidos-tab">					
						<tiles:insertAttribute name="tab_contenidos" />
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab_productos" aria-labelledby="productos-tab">
					<tiles:insertAttribute name="tab_productos" />
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab_tiposproductos" aria-labelledby="tiposproductos-tab">				
					<tiles:insertAttribute name="tab_tiposproductos" />
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab_clasificacion" aria-labelledby="clasificacion-tab">
					<tiles:insertAttribute name="tab_clasificacion" />
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab_descuentos" aria-labelledby="descuentos-tab">
					<tiles:insertAttribute name="tab_descuentospromo" />
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab_tarifas" aria-labelledby="tarifas-tab">
					<tiles:insertAttribute name="tab_tarifas" />					
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab_perfiles" aria-labelledby="perfiles-tab">
					<tiles:insertAttribute name="tab_perfilesvisitante" />					
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab_canales" aria-labelledby="canales-tab">
					<tiles:insertAttribute name="tab_canales" />				
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab_formaspago" aria-labelledby="formaspago-tab">
					<tiles:insertAttribute name="tab_formasdepago" />					
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-2"/>
<tiles:insertAttribute name="modal_dialog" />

<tiles:insertAttribute name="modal_tarifas" />

<tiles:insertAttribute name="modal_atributos" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-buscar-cliente"/>
<tiles:insertAttribute name="modal_dialog" />

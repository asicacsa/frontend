<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${xmlcabecera}" var="xmlcabecera_xml" />
<x:parse xml="${tarifas_disponibles}" var="tarifas_disponibles_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="administracion.productos.tabs.productos.dialog.asignar-tarifas.title" />
	</h4>
</div>
<div class="modal-body">
	

<form id="form_productos_tarifas_vigentes" data-parsley-validate="" class="form-horizontal form-label-left" >
		
		<input id="xml_tarifas_actualizadas" name="xml_tarifas_actualizadas" type="hidden" />
		<table id="datatable-list-tarifas_vigentes" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th></th>
				<th><spring:message code="administracion.productos.tabs.tarifas.list.header.nombre" /></th>
				<th><spring:message code="administracion.productos.tabs.tarifas-vigentes.list.header.fecha-inicio" /></th>
				<th><spring:message code="administracion.productos.tabs.tarifas-vigentes.list.header.fecha-fin" /></th>
				<th><spring:message code="administracion.productos.tabs.tarifas-vigentes.list.header.importe" /></th>
				<x:forEach select="$xmlcabecera_xml/xmlCabecera/xmlNodoCabecera" var="item">
					<th>
						<x:out select="$item" />
					</th>					
				</x:forEach>	
				<x:forEach select="$xmlcabecera_xml/xmlCabecera/xmlNodoCabecera" var="item">
					<th></th>					
				</x:forEach>	
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	
	
	
		
		<div class="modal-footer">
			
		<button id="save_tarifas_producto_button" type="button" class="btn btn-primary save_tarifas_producto">
			<spring:message code="common.button.accept" />
		</button>	
			<button type="button" class="btn btn-cancel close_dialog2" data-dismiss="modal">
				<spring:message code="common.button.cancel"/>
			</button>
		</div>		
	</form>	

</div>
<script>

//**************************************************
hideSpinner("#tab_tarifas_productos_asignar");
hideSpinner("#tab_tarifas_bono_productos_asignar");
//***************************************************
var tarifas_disponibles_json=${tarifas_disponibles_json};
//***************************************************
//Cargar datos en la tabla
var ordenado = false;
var contador_def = 5; 
var tmp= 5+contadorProductos;

var dt_tarifas_vigentes=$('#datatable-list-tarifas_vigentes').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	  initComplete: function( settings, json ) {
	    	 window.setTimeout(CargarTarifasVigentes, 100);  
	    		    	 
	 	  },
	scrollCollapse: true,
	ordering:  true,
	paging: false,	
    select: { style: 'single' },
    columns: [
              {},
              {},               
              {},
              {},
              {}
              <x:forEach select="$xmlcabecera_xml/xmlCabecera/xmlNodoCabecera" var="item">			
             ,{ data: null, 
                  render: function ( data, type, row, meta ) {     
                	  //5 es por las columnas fijas antes de las columnas dinamicas de tipos de productos
                	 
                	  if(contador_def==5+contadorProductos)
                		  contador_def = 5;
                		  
                	  return '<input name="importe_producto" id="'+data[contador_def+contadorProductos]+'" type="text" value="'+data[contador_def++]+'" class="form-control importes" style="position: relative;">'      
                	  
                  }}		
			</x:forEach>
			<x:forEach select="$xmlcabecera_xml/xmlCabecera/xmlNodoCabecera" var="item">
			 ,{}
			</x:forEach>
              
        ],
        "columnDefs": [
						{ "visible": false, "targets": [0<x:forEach select="$xmlcabecera_xml/xmlCabecera/xmlNodoCabecera" var="item">,tmp++</x:forEach>]}
                     ]  
    
} );

//*********************************************

function CargarTarifasVigentes()
	{
	   
		var i = 0;
		var tarifasProducto =tarifas_disponibles_json.ArrayList
		if (tarifasProducto !=""){
			if (tarifasProducto.Tarifaproducto!="") {
		        var item=tarifasProducto;
		        if (item.length>0)
		        	{
		        	 $.each(item, function(key, tarifa){
			            	CargarTarifa(tarifa,dt_tarifas_vigentes);
			            });
		        	}
		           
		        else
		        	CargarTarifas(item,dt_tarifas_vigentes);
			}
		}
	
	}
//*****************************************************************************************
function CargarTarifas()
	{
		var i = 0;			
		var Tarifaproducto = tarifas_disponibles_json.ArrayList.Tarifaproducto;		
			if (Tarifaproducto !=""){
			    var item=Tarifaproducto;
			    if (item.length>0)
			    	{
			    	if(ordenado==false)
			    		{
			    		ordenado = true;
			    		$.each(item, function(key, tarifa){
			        		var fecha = tarifa.fechainiciovigencia.split("-")[0];
			        		var parts = fecha.split('/');
			        		parts.reverse();
			        		fecha_invertida = parts.join('');
			        		tarifa.fecha_invertida = fecha_invertida;			        		
			        	});
			        	item=sortJSON(item,"fecha_invertida",true);
			    		}			    	
		            $.each(item, function(key, tarifa){
		            	
		            	try {
		            		delete tarifa.fecha_invertida;
		            		}
		            		catch(error) {
		            		  console.error(error);		            		  
		            		}
		            	CargarTarifa(tarifa,dt_tarifas_vigentes);
		            });
			    	}
		        else
		        	CargarTarifa(item,dt_tarifas_vigentes);
		        }
			}
	//*****************************************************************************************
	function CargarTarifa(tarifa, tabla)
	{	
			var tarifa_existente = tarifa.tarifa.nombre;
			
			var id_tarifa = tarifa.idtarifaproducto;
			
			var fecha_ini_vig = tarifa.fechainiciovigencia.split("-")[0];
			var fecha_fin_vig =tarifa.fechafinvigencia.split("-")[0];
			var importe = tarifa.importe;					
			var importeTipos = new Array(contadorProductos);
			var idsTipos = new Array(contadorProductos);
			
			var index=0;
			
			var json_Tarifacombunidadnegocio = tarifa.tarifacombunidadnegocios.Tarifacombunidadnegocio;
			if (json_Tarifacombunidadnegocio !=""){			   
			    if (json_Tarifacombunidadnegocio.length>0)
			    	
		            $.each(json_Tarifacombunidadnegocio, function(key, tarifacombunidadnegocio){
		            	index=0;
		            	<x:forEach select="$xmlcabecera_xml/xmlCabecera/xmlNodoCabecera" var="item">
							var tipoCabecera = '<x:out select="$item" />';
							var tipoCabeceraJson = tarifacombunidadnegocio.tipoproducto.nombre;
							if(tipoCabecera == tipoCabeceraJson){								
								importeTipos[index]=tarifacombunidadnegocio.importeproporcional;	
								idsTipos[index]=id_tarifa+"_"+tarifacombunidadnegocio.tipoproducto.idtipoproducto;								
							}
							index++;			
						</x:forEach>	            	
		            	
		            });
			}
				
			
			var i = 0,j = 0;
			
			tabla.row.add([
							tarifa,
							tarifa_existente,
							fecha_ini_vig, 
							fecha_fin_vig,
							importe							
							<x:forEach select="$xmlcabecera_xml/xmlCabecera/xmlNodoCabecera" var="item">
							,importeTipos[i++]
				            </x:forEach>
							<x:forEach select="$xmlcabecera_xml/xmlCabecera/xmlNodoCabecera" var="item">
							,idsTipos[j++]
							</x:forEach>
						]).draw();
}
	//********************************************************
	function actualizarImportes()
	{
		var Tarifaproducto = tarifas_disponibles_json.ArrayList.Tarifaproducto;	
		if(Tarifaproducto.length>1)
		
		 $.each(Tarifaproducto, function(key, tarifa){
			 var id_tarifa = tarifa.idtarifaproducto;
			 var json_Tarifacombunidadnegocio = tarifa.tarifacombunidadnegocios.Tarifacombunidadnegocio;
			 $.each(json_Tarifacombunidadnegocio, function(key, tarifacombunidadnegocio){
				 var id = id_tarifa+"_"+tarifacombunidadnegocio.tipoproducto.idtipoproducto;				
				 var importe = $("#"+id+"").val();
				 if(importe!=undefined){
					 if(importe=="")
						 importe=0;
					tarifacombunidadnegocio.importeproporcional = importe;
				 }
				 
				 
			 })			 
         });
		
		else
			{
			tarifa = Tarifaproducto;
			 var id_tarifa = tarifa.idtarifaproducto;
			 var json_Tarifacombunidadnegocio = tarifa.tarifacombunidadnegocios.Tarifacombunidadnegocio;
			 $.each(json_Tarifacombunidadnegocio, function(key, tarifacombunidadnegocio){
				 var id = id_tarifa+"_"+tarifacombunidadnegocio.tipoproducto.idtipoproducto;				
				 var importe = $("#"+id+"").val();
				 if(importe!=undefined){
					 if(importe=="")
						 importe=0;
					tarifacombunidadnegocio.importeproporcional = importe;
				 }
				 
				 
			 })		
			
			}
	}
	//*****************************************************
	function guardarImportes()
	{
		//Pasamos el json a xml
		var aux = json2xml(tarifas_disponibles_json.ArrayList);
		$("#xml_tarifas_actualizadas").val("<list>"+aux+"</list>");		
		var data = $("#form_productos_tarifas_vigentes").serializeObject();

		showSpinner("#modal-dialog-form-2 .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/productos/productos/save_importes.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form-2 .modal-content");
				
				$("#modal-dialog-form-2").modal('hide');
				
								
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
	}
	
	//***********************************
	$( ".save_tarifas_producto" ).on("click", function(e) {	
		actualizarImportes()
		guardarImportes();	
	})
	//***********************************
	
</script>


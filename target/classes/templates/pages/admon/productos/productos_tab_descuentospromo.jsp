<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2>
			<spring:message code="common.text.filter_list" />
		</h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">

		<form id="form_filter_list_descuentospromo"class="form-horizontal form-label-left">	
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.productos.tabs.descuentospromo.field.mostrar" /></label>
					<div class="col-md-5 col-sm-5 col-xs-5">
						<select class="form-control" id="caducados" name="caducados">
							<option value="0" selected="selected"><spring:message code="administracion.productos.tabs.descuentospromo.list.vigentes" /></option>
							<option value="1"><spring:message code="administracion.productos.tabs.descuentospromo.list.todos" /></option>
						</select>
					</div>
				</div>			
			</div>
			<div class="clearfix"></div>
			<div class="ln_solid"></div>		
			<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
				<button id="button_filter_list_descuentospromo" type="button" class="btn btn-success pull-right">
					<spring:message code="common.button.filter" />
				</button>
			</div>	
		</form>
	</div>
</div>

   <div class="col-md-12 col-sm-12 col-xs-12">
   <div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_descuentos_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.descuentospromo.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_descuentos_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.descuentospromo.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_descuentos_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.descuentospromo.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>		
	</div>
   
	<table id="datatable-lista-descuentospromo" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>			
			    <th><spring:message code="administracion.productos.tabs.descuentospromo.list.header.orden" /></th>
				<th><spring:message code="administracion.productos.tabs.descuentospromo.list.header.nombre" /></th>
				<th><spring:message code="administracion.productos.tabs.descuentospromo.list.header.cliente" /></th>							
				<th><spring:message code="administracion.productos.tabs.descuentospromo.list.header.tipromocion" /></th>
				<th><spring:message code="administracion.productos.tabs.descuentospromo.list.header.desde"/></th>
				<th><spring:message code="administracion.productos.tabs.descuentospromo.list.header.hasta"/></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>


<script>
	var dt_listdescuentospromo=$('#datatable-lista-descuentospromo').DataTable( {	
	 ajax: {
	 url: "<c:url value='/ajax/admon/productos/descuentos/list_descuentospromocionales.do'/>",
	 rowId: 'iddescuentopromocional',
	 type: 'POST',
	 dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.descuentopromocional)); return(""); },
	 error: function (xhr, error, thrown) {
         if (xhr.responseText=="403") {
               $("#descuentos-tab").hide();
         }    
         else
      	 {
      	 	$("#datatable-lista-descuentospromo_processing").hide();
      		new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : xhr.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}					
				});
      	 }
  },
	 
	 data: function (params) { return($("#form_filter_list_descuentospromo").serializeObject()); }
	 },
	 initComplete: function( settings, json ) {
	 $('a#menu_toggle').on("click", function () {if (dt_listdescuentospromo.data().count()>0) dt_listdescuentospromo.columns.adjust().draw(); });
	 },
	 columns: [
	 { data: "orden", type: "spanish-string" ,  defaultContent:""} ,
	 { data: "nombre", type: "spanish-string" ,  defaultContent:""},	 
	 { data: "cliente.nombre", type: "spanish-string" ,  defaultContent:""},
	 { data: "tipopromocion.nombre", type: "spanish-string" ,  defaultContent:""},
	 { data: "fechainiciovigencia", type: "spanish-string" ,  defaultContent:"",
		 render: function ( data, type, row, meta ) {			 
    	  	  return data.substr(0, 10);}	 
	 } ,
	 { data: "fechafinvigencia", type: "spanish-string" ,  defaultContent:"",
		 render: function ( data, type, row, meta ) {			 
   	  	  return data.substr(0, 10);} 
	 
		 } ,  	 
	
	 ],    
	 drawCallback: function( settings ) {
	 $('[data-toggle="tooltip"]').tooltip();
	 },
	 select: { style: 'os'},
	 language: dataTableLanguage,
	 processing: true,
	 } );
	
	insertSmallSpinner("#datatable-lista-descuentospromo_processing");
	/*********************************************************BOT�N FILTRAR*******************************************************************/
	$("#button_filter_list_descuentospromo").on("click", function(e) {
		dt_listdescuentospromo.ajax.reload();
	})
	/***********************************************BOT�N NUEVO**************************************/
 $("#tab_descuentos_new").on("click", function(e) { 
	showButtonSpinner("#tab_descuentos_new");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/productos/descuentos/show_descuento.do'/>", function() {										  
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
})
/***********************************************BOT�N EDITAR*************************************/
 $("#tab_descuentos_edit").on("click", function(e) { 
	var data = sanitizeArray(dt_listdescuentospromo.rows( { selected: true } ).data(),"iddescuentopromocional");
		
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.productos.tabs.descuentospromo.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.productos.tabs.descuentospromo.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	showButtonSpinner("#tab_descuentos_edit");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/productos/descuentos/show_descuento.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
})
 
	/***********************************************BOT�N ELIMIMAR***********************************/
    $("#tab_descuentos_remove").on("click", function(e) { 
		
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="administracion.productos.tabs.descuentospromo.list.confirm.eliminar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
	  		  buttons: { closer: false, sticker: false	},
	  		  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
		
			   dt_listdescuentospromo.processing(true);
				
				var data = sanitizeArray(dt_listdescuentospromo.rows( { selected: true } ).data(),"iddescuentopromocional");
			   
				$.ajax({
					contenttype: "application/json; charset=utf-8",
					type : "post",
					url : "<c:url value='/ajax/admon/productos/descuentos/remove_descuentospromocionales.do'/>",
					timeout : 100000,
					data: { data: data.toString() }, 
					success : function(data) {
						dt_listdescuentospromo.ajax.reload();					
					},
					error : function(exception) {
						dt_listdescuentospromo.processing(false);
						
						new PNotify({
						      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						      text: exception.responseText,
							  type: "error",		     
							  delay: 5000,
							  buttons: { sticker: false }
						   });			
					}
				});

		   }).on('pnotify.cancel', function() {
		   });		 

	});   
</script>

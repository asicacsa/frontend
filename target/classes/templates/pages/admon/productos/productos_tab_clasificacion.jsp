<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>



<div class="col-md-12 col-sm-12 col-xs-12">
<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_clasificaciones_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.clasificacion.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_clasificaciones_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.clasificacion.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_clasificaciones_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.clasificacion.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>		
	</div>

	<table id="datatable-lista-clasificacion" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="administracion.productos.tabs.clasificacion.list.header.nombre" /></th>
				<th><spring:message code="administracion.productos.tabs.clasificacion.list.header.descripcion" /></th>
				<th><spring:message code="administracion.productos.tabs.clasificacion.list.header.productos" /></th>				
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>
var dt_listclasificaciones=$('#datatable-lista-clasificacion').DataTable( {	
    ajax: {
        url: "<c:url value='/ajax/admon/productos/clasificaciones/list_clasificacionesproductos.do'/>",
        rowId: 'idclasificacionproductos',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Clasificacionproductos)); return(""); },
       
        error: function (xhr, error, thrown) {
            if (xhr.responseText=="403") {
                  $("#clasificacion-tab").hide();
            }      
            else
       	 {
       	 	$("#datatable-lista-clasificacion_processing").hide();
       		new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : xhr.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}					
				});
       	 }
     },
        
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listclasificaciones.data().count()>0) dt_listclasificaciones.columns.adjust().draw(); });
	},
    columns: [
              { data: "nombre", type: "spanish-string" ,  defaultContent:""} ,
              { data: "descripcion", type: "spanish-string" ,  defaultContent:""},
              { data: "clasifprodprods.Clasifprodprod", className: "cell_centered",
             	  render: function ( data, type, row, meta ) { return showListInCell(data,"<spring:message code="administracion.productos.tabs.clasificacion.list.text.productos" />","producto.nombre"); }	
                   },           
              
    ],    
    drawCallback: function( settings ) {
    	 activateTooltipsInTable('datatable-lista-clasificacion')
    },
    select: { style: 'os', selector:'td:not(:last-child)'},
	language: dataTableLanguage,
	processing: true,
} );

insertSmallSpinner("#datatable-lista-clasificacion_processing");
/***********************************************BOT�N EDITAR*************************************/
$("#tab_clasificaciones_edit").on("click", function(e) {
	
	var data = sanitizeArray(dt_listclasificaciones.rows( { selected: true } ).data(),"idclasificacionproductos");
		
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.productos.tabs.clasificacion.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.productos.tabs.clasificacion.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
		
	showButtonSpinner("#tab_clasificaciones_edit");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/productos/clasificaciones/show_clasificacion.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "sm");
	});
})
/***********************************************BOT�N NUEVO  *********************************/
$("#tab_clasificaciones_new").on("click", function(e) {

	showButtonSpinner("#tab_clasificaciones_new");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/productos/clasificaciones/show_clasificacion.do'/>", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
});
/***********************************************BOT�N ELIMIMAR***********************************/
   $("#tab_clasificaciones_remove").on("click", function(e) { 
		
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="administracion.productos.tabs.clasificacion.list.confirm.eliminar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
	  		  buttons: { closer: false, sticker: false	},
	  		  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
		
			   dt_listclasificaciones.processing(true);
				
				var data = sanitizeArray(dt_listclasificaciones.rows( { selected: true } ).data(),"idclasificacionproductos");
			   
				$.ajax({
					contenttype: "application/json; charset=utf-8",
					type : "post",
					url : "<c:url value='/ajax/admon/productos/clasificaciones/remove_clasificacionesproductos.do'/>",
					timeout : 100000,
					data: { data: data.toString() }, 
					success : function(data) {
						dt_listclasificaciones.ajax.reload();					
					},
					error : function(exception) {
						dt_listclasificaciones.processing(false);
						
						new PNotify({
						      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						      text: exception.responseText,
							  type: "error",		     
							  delay: 5000,
							  buttons: { sticker: false }
						   });			
					}
				});

		   }).on('pnotify.cancel', function() {
		   });		 

	});
</script>


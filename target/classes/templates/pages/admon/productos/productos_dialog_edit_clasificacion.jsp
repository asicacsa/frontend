<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editarclasificacion_datos_clasificacion}" var="editarclasificacion_datos_clasificacion_xml" />
<x:parse xml="${editarclasificacion_selector_productos}" var="editarclasificacion_selector_productos_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">x</span>
	</button>
	<h4 class="modal-title">
		<x:choose>
			<x:when select="not($editarclasificacion_datos_clasificacion_xml/Clasificacionproductos)">
				<spring:message code="administracion.productos.tabs.clasificacion.dialog.crear_clasificacion.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.productos.tabs.clasificacion.dialog.editar_clasificacion.title" />
			</x:otherwise>
		</x:choose>
	</h4>
</div>

<div class="modal-body" id="principal">

	<form id="form_clasificacion_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	
	<input id="idclasificacion" name="idclasificacion" type="hidden" value="<x:out select = "$editarclasificacion_datos_clasificacion_xml/Clasificacionproductos/idclasificacionproductos" />" />
	
	<div class="col-md-12 col-sm-12 col-xs-12">

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.clasificacion.field.nombre" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editarclasificacion_datos_clasificacion_xml/Clasificacionproductos/nombre" />">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.clasificacion.field.descripcion" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<textarea class="form-control" rows="3" name="descripcion" id="descripcion"><x:out select="$editarclasificacion_datos_clasificacion_xml/Clasificacionproductos/descripcion" /></textarea>
				</div>
			</div>
			<div class="form-group" id="selector_productos_form_group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.clasificacion.field.productos" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">					
					<select name="productos[]" id="selector_productos" class="select-select2 select2_single_line select2_multiple form-control" multiple="multiple" style="width: 100%">
					<x:forEach select="$editarclasificacion_selector_productos_xml/ArrayList/LabelValue" var="item">
						<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
					</x:forEach>
				</select>					
				</div>
			</div>			
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="modal-footer">	
			<button id="save_red_button" type="submit" class="btn btn-primary save_dialog">
				<spring:message code="common.button.save" />
			</button>
			<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
				<spring:message code="common.button.cancel" />
			</button>
		</div>
		</div>
	</form>	
</div>


<script>
	hideSpinner("#tab_clasificaciones_new");
	hideSpinner("#tab_clasificaciones_edit");

	$(".select-select2").select2({
		language : "es",
		containerCssClass : "single-line"
	});
	
	//************************SELECCION PRODUCTOS DE LA CLASIFICACIÓN SELECCIONADA***************************//
	var selectedValues = new Array();
	<x:forEach select="$editarclasificacion_datos_clasificacion_xml/Clasificacionproductos/clasifprodprods/Clasifprodprod" var="item">
	selectedValues.push('<x:out select="$item/producto/idproducto"/>');
	$('#selector_productos option[value="<x:out select="$item/producto/idproducto"/>"]').attr("idclasifprodprod","<x:out select="$item/idclasifprodprod"/>");
	</x:forEach>
	$('#selector_productos').val(selectedValues);
	$('#selector_productos').trigger('change.select2');
	
	//************************************VALIDAR DATOS********************************************************//
	$("#form_clasificacion_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFormDataClasificacion();
		}
	});
	
	//**************************************GRABAR DATOS*************************************************//	
	function saveFormDataClasificacion() {

		$('#selector_productos option').each(function() {
			if($(this).attr("idclasifprodprod")!=null && typeof $(this).attr("idclasifprodprod")!="undefined") {
				$(this).val($(this).val()+"|"+$(this).attr("idclasifprodprod"));
			}
		});
		
		var data = $("#form_clasificacion_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/productos/clasificaciones/save_clasificacion.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				dt_listclasificaciones.ajax.reload(null,false);			
				$("#modal-dialog-form").modal('hide');
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
	}
	
	</script>
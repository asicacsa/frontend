<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="administracion.programacion.dialog.select_ver_afluencia.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_ver_afluencia" class="form-horizontal form-label-left">

		<div id="tabla-afluencia" class="col-md-12 col-sm-12 col-xs-12">
		
			<table id="datatable-list-ver-afluencia" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th><spring:message code="administracion.programacion.ver_afluencia.list.header.hora" /></th>
						<th><spring:message code="administracion.programacion.ver_afluencia.list.header.numpersonas" /></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			
		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.close" />
				</button>
			</div>
		</div>
		
	</form>
</div>


<script>
showSpinner('#tabla-afluencia');
var dt_verafluencia=$('#datatable-list-ver-afluencia').DataTable( {
	language: dataTableLanguage,
	processing: false,
	scrollY: "200px",
	scrollCollapse: true,
	paging: false,
	info: false,
	searching: false,
    select: { style: 'single'},
    ajax: {
        url: "<c:url value='/ajax/admon/programacion/list_ver_afluencia.do'/>",
        rowId: 'idpresentacionsesion',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Presentacionsesion)); return(""); },
        data: {
        	id: $("#idsesion").val()
        },
        error: function (xhr, error, code)
             {
                	new PNotify({
 					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
 					text : xhr.responseText,
 					type : "error",
 					delay : 5000,
 					buttons : {
 						closer : true,
 						sticker : false
 					}					
 				});
             }
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_verafluencia.data().count()>0) dt_verafluencia.columns.adjust().draw(); });
        hideSpinner('#tabla-afluencia');
	},
    columns: [
        { data: "hora", className: "cell_centered", type: "spanish-string", defaultContent: "" },
        { data: "numpersonas", className: "cell_centered", type: "spanish-string", defaultContent: "" }
    ],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-horas-presentacion') }
} );

function ajustar_cabeceras_datatable()
{
	$('#datatable-list-ver-afluencia').DataTable().columns.adjust().draw();
}

setTimeout(ajustar_cabeceras_datatable, 200);

</script>

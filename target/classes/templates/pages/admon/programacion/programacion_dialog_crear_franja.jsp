<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">x</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="administracion.programacion.programacion.dialog.crear_franja.title" />
	</h4>
</div>
<div class="modal-body" id="principal">

	<form id="form_franja_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

		<div class="col-md-12 col-sm-12 col-xs-12">

			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.programacion.programacion.dialog.crear_franja.field.nombre" />*</label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.programacion.programacion.dialog.crear_franja.field.horainicio" />*</label>
				<div class="col-md-3 col-sm-3 col-xs-8">
					<input name="horainicio" id="horainicio" type="text" class="form-control" required="required" data-inputmask="'mask': '99:99'" value="">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="administracion.programacion.programacion.dialog.crear_franja.field.horafin" />*</label>
				<div class="col-md-3 col-sm-3 col-xs-8">
					<input name="horafin" id="horafin" type="text" class="form-control" required="required" data-inputmask="'mask': '99:99'" value="">
				</div>
			</div>

		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_franja_button" type="submit" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>

	</form>

</div>

<script>
	hideSpinner("#button_franjas_new");
	
	$(":input").inputmask();

	$("#form_franja_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFranjaData();
		}
	});

	//********************************************************************************	
	function saveFranjaData() {
		
		var data = $("#form_franja_data").serializeObject();

		showSpinner("#modal-dialog-form-2 .modal-content");

		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/admon/programacion/save_franja.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form-2 .modal-content");
				dt_listfranjas.ajax.reload(null,false);						
				$("#modal-dialog-form-2").modal('hide');
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
	}
</script>

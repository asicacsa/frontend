<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${crearbloqueo_selector_motivos}" var="crearbloqueo_selector_motivos_xml" />
<x:parse xml="${crearbloqueo_selector_zonas}" var="crearbloqueo_selector_zonas_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">x</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="administracion.programacion.programacion.dialog.crear_bloqueo.title" />
	</h4>
</div>

<div class="modal-body" id="principal">

	<form id="form_bloqueo_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		
		<input name="idsesion" id="idsesion" value="${idSesion}" type="hidden">
		<input name="idsesionmultiple" id="idsesionmultiple" value="${idSesionMultiple}" type="hidden">
		
		<div class="col-md-12 col-sm-12 col-xs-12">

			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="administracion.programacion.programacion.dialog.crear_bloqueo.field.numbloqueadas" />*</label>
				<div class="col-md-3 col-sm-3 col-xs-3">
					<input name="numbloqueadas" id="numbloqueadas" type="text" class="form-control" required="required" value="">
				</div>
			</div>
			
			<div class="form-group">
				<input name="motivobloqueo" id="motivobloqueo" type="hidden">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="administracion.programacion.programacion.dialog.crear_bloqueo.field.motivo" />*</label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<select class="form-control" name="motivo" id="motivo" required="required">
						<option value=""></option>
							<x:forEach select="$crearbloqueo_selector_motivos_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="administracion.programacion.programacion.dialog.crear_bloqueo.field.usuario" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input name="idusuario" id="idusuario" type="hidden"value="${sessionScope.idUsuario}">
					<input name="usuario" id="usuario" type="text" class="form-control" readonly value="${sessionScope.login}">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="administracion.programacion.programacion.dialog.crear_bloqueo.field.zona" />*</label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<select class="form-control" name="zona" id="zona" required="required">
						<option value=""></option>
						<x:forEach select="$crearbloqueo_selector_zonas_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="administracion.programacion.programacion.dialog.crear_bloqueo.field.observaciones" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<textarea class="form-control" rows="3" name="observaciones" id="observaciones"></textarea>
				</div>
			</div>
			
	
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="administracion.programacion.programacion.dialog.crear_bloqueo.field.solo_resp" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input id="activada-switch" name="activada" type="checkbox" class="js-switch-activacion" data-switchery="true" style="display: none;">
				</div>
			</div>

		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_bloqueo_button" type="submit" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>

	</form>

</div>

<script>
	hideSpinner("#${buttonBloqueo}");
		
	$('input[name="motivobloqueo"]').val($("#motivo").text().trim());
	
	var idzona= "${idZona}";
	
	if (idzona.length>0) {
		$("#zona").val(idzona);
		$("#zona").attr("disabled",true);
	}
	
	$("#activada-switch").each(function(index) {
		var s = new Switchery(this, {
			size : 'small'
		});
	});	

	$("#form_bloqueo_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveBloqueoData();
		}
	});

	//********************************************************************************	
	function saveBloqueoData() {
		
		var data = $("#form_bloqueo_data").serializeObject(),
			numbloqueadas= data.numbloqueadas;
		data['zona']= $("#zona").val();
		
		showSpinner("#modal-dialog-form-2 .modal-content");
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/admon/programacion/save_bloqueo.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {				
				hideSpinner("#modal-dialog-form-2 .modal-content");				
				if (${esMultiple}) {
					var data = $("#form_bloqueo_data").serializeObject();
					${listaBloqueos}.row.add( [ data.numbloqueadas, data.motivo, data.usuario, data.observaciones] ).draw( false );
				}
				else {
					if (${esProgramacion}) /* Se llama desde programación */
					{
						${listaBloqueos}.ajax.reload(null,false);
					}
					else { /* Se llama desde Venta y Reserva */					
						${listaBloqueos}.rows( { selected: true } ).data()[0][${idxLibres}]=parseInt(${listaBloqueos}.rows( { selected: true } ).data()[0][${idxLibres}])-parseInt(numbloqueadas);
						${listaBloqueos}.rows( { selected: true } ).data()[0][${idxBloqueadas}]=parseInt(${listaBloqueos}.rows( { selected: true } ).data()[0][${idxBloqueadas}])+parseInt(numbloqueadas);
						${listaBloqueos}.rows().invalidate().draw();
					}	
				}
				$("#modal-dialog-form-2").modal('hide');
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
			},
			error: function(exception) {
				hideSpinner("#modal-dialog-form-2 .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
	}
</script>

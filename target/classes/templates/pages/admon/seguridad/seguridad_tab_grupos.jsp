<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_grupos_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.seguridad.tabs.grupos.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_grupos_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.seguridad.tabs.grupos.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_grupos_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.seguridad.tabs.grupos.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>
	</div>

	<table id="datatable-list-grupos" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="administracion.seguridad.tabs.grupos.list.header.idgrupo" /></th>
				<th><spring:message code="administracion.seguridad.tabs.grupos.list.header.nombre" /></th>
				<th><spring:message code="administracion.seguridad.tabs.grupos.list.header.usuarios" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>

var dt_listgrupos=$('#datatable-list-grupos').DataTable( {
    ajax: {
        url: "<c:url value='/ajax/admon/seguridad/seguridad/list_grupos.do'/>",
        rowId: 'idgrupo',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Grupo)); return(""); },
        error: function (xhr, error, thrown) {
    		if (xhr.responseText=="403") {
    			$("#grupos-tab").hide();
    		}        		
    	}        
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listgrupos.data().count()>0) dt_listgrupos.columns.adjust().draw(); });
	},
	columnDefs: [
        { "visible": false, "targets": 0 }
    ],
    columns: [
        { data: "idgrupo", type: "numeric", defaultContent: "" }, 
        { data: "nombre", type: "spanish-string", defaultContent: ""}, 
        { data: "grupousuarios.Grupousuario", className: "cell_centered",
       	  render: function ( data, type, row, meta ) { return showListInCell(data,"<spring:message code="administracion.seguridad.tabs.grupos.list.text.usuarios" />","usuario.nombre"); }	
        }        
    ],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-grupos') },
    select: { style: 'os', selector:'td:not(:last-child)'},
	language: dataTableLanguage,
	processing: true,
} );


insertSmallSpinner("#datatable-list-grupos_processing");

//********************************************************************************
$("#tab_grupos_new").on("click", function(e) {
	
	showButtonSpinner("#tab_grupos_new");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/seguridad/seguridad/show_grupo.do'/>", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "sm");
	});
});

//********************************************************************************
$("#tab_grupos_edit").on("click", function(e) {
	
	var data = sanitizeArray(dt_listgrupos.rows( { selected: true } ).data(),"idgrupo");
	
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.seguridad.tabs.grupos.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.seguridad.tabs.grupos.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	

	showButtonSpinner("#tab_grupos_edit");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/seguridad/seguridad/show_grupo.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "sm");
	});
});

//********************************************************************************
$("#tab_grupos_remove").on("click", function(e) { 
		
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="administracion.seguridad.tabs.grupos.list.confirm.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
  		  buttons: { closer: false, sticker: false	},
  		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
	
			dt_listgrupos.processing(true);
			
			var data = sanitizeArray(dt_listgrupos.rows( { selected: true } ).data(),"idgrupo");
		   
			$.ajax({
				contenttype: "application/json; charset=utf-8",
				type : "post",
				url : "<c:url value='/ajax/admon/seguridad/seguridad/remove_grupos.do'/>",
				timeout : 100000,
				data: { data: data.toString() }, 
				success : function(data) {
					dt_listgrupos.processing(false);	
					dt_listgrupos.rows( { selected: true } ).remove().draw();
				},
				error : function(exception) {
					dt_listgrupos.processing(false);
					
					new PNotify({
					      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					      text: exception.responseText,
						  type: "error",		     
						  delay: 5000,
						  buttons: { sticker: false }
					   });			
				}
			});

	   }).on('pnotify.cancel', function() {
	   });		 

}); 


</script>


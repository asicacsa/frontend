<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editarpunto_datos_punto}" var="editarpunto_datos_punto_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">
		<x:choose>
			<x:when select="not($editarpunto_datos_punto_xml/Puntosrecogida)">
				<spring:message code="administracion.recintos.tabs.puntos.recogida.dialog.crear_punto.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="administracion.recintos.tabs.puntos.recogida.dialog.editar_punto.title" />
			</x:otherwise>
		</x:choose>
	</h4>
</div>

<div class="modal-body">
	<form id="form_punto_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	    <input id="idPuntoRecogida" name="idPuntoRecogida" type="hidden" value="<x:out select = "$editarpunto_datos_punto_xml/Puntosrecogida/idpuntosrecogida" />" />
	    <input id="i18nFieldNombre_puntorecogida" name="i18nFieldNombre_puntorecogida" type="hidden" value="<x:forEach select="$editarpunto_datos_punto_xml/Puntosrecogida/i18ns/I18n" var="item"><x:out select="$item/idi18n" />~<x:out select="$item/idioma/ididioma" />~<x:out select="$item/nombre" />^</x:forEach>" />
	
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="administracion.recintos.tabs.puntos.recogida.field.nombre" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<input name="nombre" id="nombre" required="required" type="text" class="form-control required" required="required" value="<x:out select = "$editarpunto_datos_punto_xml/Puntosrecogida/nombre" />">
				</div>
			</div>	
	</form>

	<div class="modal-footer">
		<button id="save_punto_button" type="button" class="btn btn-primary save_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>

</div>
<script>
//********************************************************************************
hideSpinner("#tab_puntos_recogida_new");
hideSpinner("#tab_puntos_recogida_edit");
//********************************************************************************				
	$("#nombre").i18nField("#i18nFieldNombre_puntorecogida",<c:out value="${sessionScope.i18nlangs}"  escapeXml="false" />);
	
	$("#form_punto_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFormData();
		}
	}
	);
	
	
	
	$("#save_punto_button").on("click", function(e) {
		$("#form_punto_data").submit();	
	})
	

	//********************************************************************************	
	function saveFormData() 
	{
		var data = $("#form_punto_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/admon/recintos/puntos_recogida/save_puntoRecogida.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				
				$("#modal-dialog-form").modal('hide');
				
				dt_listpuntos.ajax.reload(null,false);
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});

	}


	
</script>
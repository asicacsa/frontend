<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>



<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_taquillas_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.taquillas.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_taquillas_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.taquillas.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_taquillas_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.recintos.tabs.taquillas.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>
		
	</div>


	<table id="datatable-lista-taquillas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="administracion.recintos.tabs.taquillas.list.header.nombre" /></th>
				<th><spring:message code="administracion.recintos.tabs.taquillas.list.header.recinto" /></th>
				<th><spring:message code="administracion.recintos.tabs.taquillas.list.header.ubicacion" /></th>
				<th><spring:message code="administracion.recintos.tabs.taquillas.list.header.ip" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>

</div>

<script>

var dt_listtaquillas=$('#datatable-lista-taquillas').DataTable( {
    ajax: {
        url: "<c:url value='/ajax/admon/recintos/taquillas/list_taquillas.do'/>",
        rowId: 'idtaquilla',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Taquilla)); return(""); }    
	,

error: function (xhr, error, thrown) {
            if (xhr.responseText=="403") {
                  $("#tab_taquillas").hide();
            }                   
     }
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listtaquillas.data().count()>0) dt_listtaquillas.columns.adjust().draw(); });
	},
    columns: [
        { data: "nombre", type: "spanish-string" ,  defaultContent:""} ,
        { data: "recinto.nombre", type: "spanish-string" ,  defaultContent:""},
        { data: "ubicacion.nombre", type: "spanish-string" ,  defaultContent:""}, 
        { data: "dirip", type: "spanish-string" ,  defaultContent:""},   
        
    ],    
    drawCallback: function( settings ) {
    	$('[data-toggle="tooltip"]').tooltip();
    },
    select: { style: 'os'},
	language: dataTableLanguage,
	processing: true,
} );

insertSmallSpinner("#datatable-lista-taquillas_processing");

$("#tab_taquillas_edit").on("click", function(e) { 
	showButtonSpinner("#tab_taquillas_edit");
	var data = sanitizeArray(dt_listtaquillas.rows( { selected: true } ).data(),"idtaquilla");
	
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.recintos.tabs.taquillas.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		hideSpinner("#tab_taquillas_edit");
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.recintos.tabs.taquillas.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		hideSpinner("#tab_taquillas_edit");
		return;
	}	
		
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/recintos/taquillas/show_taquilla.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "sm");
	});
})


$("#tab_taquillas_new").on("click", function(e) { 
	
	showButtonSpinner("#tab_taquillas_new");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/admon/recintos/taquillas/show_taquilla.do'/>", function() {										  
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "sm");
	});
})

//********************************************************************************
$("#tab_taquillas_remove").on("click", function(e) { 
	showButtonSpinner("#tab_taquillas_remove");
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="administracion.recintos.tabs.taquillas.list.confirm.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
  		  buttons: { closer: false, sticker: false	},
  		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
	
		   dt_listtaquillas.processing(true);
			
			var data = sanitizeArray(dt_listtaquillas.rows( { selected: true } ).data(),"idtaquilla");
		   
			$.ajax({
				contenttype: "application/json; charset=utf-8",
				type : "post",
				url : "<c:url value='/ajax/admon/recintos/taquillas/remove_taquillas.do'/>",
				timeout : 100000,
				data: { data: data.toString() }, 
				success : function(data) {
					dt_listtaquillas.ajax.reload();		
					hideSpinner("#tab_taquillas_remove");
				},
				error : function(exception) {
					dt_listtaquillas.processing(false);
					hideSpinner("#tab_taquillas_remove");
					new PNotify({
					      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					      text: exception.responseText,
						  type: "error",		     
						  delay: 5000,
						  buttons: { sticker: false }
					   });			
				}
			});

	   }).on('pnotify.cancel', function() {
	   });		 

}); 
</script>


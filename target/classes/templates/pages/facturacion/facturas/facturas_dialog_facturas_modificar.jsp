<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${datos_factura}" var="datos_factura_xml" />
<x:parse xml="${editar_cliente_direcciones_envio}" var="editar_cliente_direcciones_envio_xml" />
<x:parse xml="${editar_cliente_forma_pago}" var="editar_cliente_forma_pago_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	 <h4 class="modal-title">	
		<spring:message code="facturacion.facturas.tabs.facturas.dialog.modificar_factura.title" />				
	</h4>	
</div>


<div class="modal-body">
	<form id="form_factura_modificar_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input type="hidden" id="idFactura" name="idFactura" value="<x:out select = "$datos_factura_xml/Factura/idfactura" />"/>
		
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="facturacion.facturas.tabs.factura.field.datosfiscales" /></label>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<x:out select = "$datos_factura_xml/Factura/datosfiscales/id" /> - <x:out select = "$datos_factura_xml/Factura/datosfiscales/razonsocial" />
					</div>
				</div>	
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="facturacion.facturas.tabs.factura.field.sucursal" /></label>
					<div class="col-md-6 col-sm-6 col-xs-6"><x:out select = "$datos_factura_xml/Factura/datoscliente/razonsocial" /></div>
				</div>	
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="facturacion.facturas.tabs.factura.field.contacto" /></label>
					<div class="col-md-6 col-sm-6 col-xs-6"><x:out select = "$datos_factura_xml/Factura/datoscliente/nombrecontacto" /></div>
				</div>	
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="facturacion.facturas.tabs.clientes.field.direccionFacturacion" />*</label>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<select name="idFormaenvio" id="idFormaenvio"  class="form-control" required="required">
							<option> </option>
							<x:forEach select="$editar_cliente_direcciones_envio_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
						</select>
					</div>
				</div>		
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="facturacion.facturas.tabs.clientes.field.formaPago" />*</label>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<select name="idFormapago" id="idFormapago"  class="form-control" required="required">
							<option> </option>
							<x:forEach select="$editar_cliente_forma_pago_xml/ArrayList/Formapago" var="item">
								<option value="<x:out select="$item/idformapago" />"><x:out select="$item/nombre" /></option>
							</x:forEach>					
						</select>
					</div>
				</div>			
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			
				<div class="form-group date-picker" id="fechafact">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.factura.field.fechageneracion" />:</label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						  <a type="button" class="btn btn-default btn-clear-date" id="button_fechafacturacion_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.gestionfacturas.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
						  </a>			
                        <div class="input-prepend input-group">
                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                          <input type="text" name="fechageneracion" id="fechageneracion" class="form-control" value="<x:out select = "$datos_factura_xml/Factura/fechageneracion" />" readonly/>
                          <input type="hidden" required="required" name="fechageneraciontexto" value="<x:out select = "$datos_factura_xml/Factura/fechageneracion" />"/>                          
                        </div>
					</div>
				</div>
				<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.factura.field.nimpresiones" /></label>
						<div class="col-md-8 col-sm-8 col-xs-8">
							<input name="numImpresiones" id="numImpresiones" type="text" class="form-control" required="required" value="<x:out select = "$datos_factura_xml/Factura/numimpresiones" />">
						</div>
				</div>	
				<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.factura.field.observaciones" /></label>
						<div class="col-md-8 col-sm-8 col-xs-8">					
							<textarea name="observaciones" id="observaciones" class="form-control"  ><x:out select = "$datos_factura_xml/Factura/observaciones" /></textarea>
						</div>
				</div>				
							
				
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.factura.field.rectificativas" /></label>
			</div>				
			<div class="form-group">
				<div class="col-md-8 col-sm-8 col-xs-6">
					<select name="rectificativas" id="rectificativas" class="form-control" multiple="multiple"  style="width: 100%">
						<x:forEach select="$datos_factura_xml/Factura/rectificativas/Factura" var="item">
							<option value="<x:out select="$item/idfactura" />"><x:out select="$item/serieynumero" /></option>
						</x:forEach>
					</select>
				</div>
			</div>			
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12">
		<table id="datatable-list-modificar_factura_lineas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th><spring:message code="facturacion.facturas.tabs.factura.tab.lineas.producto" /></th>
					<th><spring:message code="facturacion.facturas.tabs.factura.tab.lineas.cantidad" /></th>
					<th><spring:message code="facturacion.facturas.tabs.factura.tab.lineas.descuento_promo" /></th>
					<th><spring:message code="facturacion.facturas.tabs.factura.tab.lineas.descutnto_cliente" /></th>
					<th><spring:message code="facturacion.facturas.tabs.factura.tab.lineas.iva" /></th>
					<th><spring:message code="facturacion.facturas.tabs.factura.tab.lineas.importe_iva" /></th>
					<th><spring:message code="facturacion.facturas.tabs.factura.tab.lineas.importe" /></th>
				</tr>
			</thead>
			<tbody>
				<x:forEach select="$datos_factura_xml/Factura/lineafacturas/Lineafactura" var="item">
					<tr>
						<td><x:out select="$item/producto" /></td>
						<td><x:out select="$item/cantidad" /></td>
						<td><x:out select="$item/importedescuentopromo" /></td>
						<td><x:out select="$item/importedescuentocliente" /></td>
						<td><x:out select="$item/porcentajeiva" /></td>
						<td><x:out select="$item/importeiva" /></td>
						<td><x:out select="$item/importe" /></td>
					</tr>
				</x:forEach>				
			</tbody>
		</table>
		</div>
	</form>
	<div class="modal-footer">
		<button id="modificar_factura_button" type="button" class="btn btn-primary save_modificar_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>
<script>




hideSpinner("#tab_gestion_factura_modificar");

$today= moment().format("DD/MM/YYYY");
var dia_inicio = $today;
var dia_fin = $today;
if($('input[name="fechageneraciontexto"]').val()!='')
{	
dia_inicio = $('input[name="fechageneraciontexto"]').val().substring(0, 10);
}

$('input[name="fechageneracion"]').val(dia_inicio);
$('input[name="fechageneraciontexto"]').val( dia_inicio);



$('input[name="fechageneracion"]').daterangepicker({
	singleDatePicker: true,
	autoUpdateInput: false,
	linkedCalendars: false,
	showDropdowns: true,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start) {
  	  	 $('input[name="fechageneracion"]').val(start.format('DD/MM/YYYY'));
  	  	 $('input[name="fechageneraciontexto"]').val(start.format('DD/MM/YYYY'));
  	  	 
	 });
	 
var dtlineafactura;

function autoajuste() {
	dtlineafactura.columns.adjust().draw();	
}

var idformaPago = "<x:out select = "$datos_factura_xml/Factura/formapago/idformapago" />";
$('#idFormapago option[value="'+idformaPago+'"]').attr("selected", "selected");

var iddireccionenvio = "<x:out select = "$datos_factura_xml/Factura/direccionenvio/iddireccionenvio" />";
$('#idFormaenvio option[value="'+iddireccionenvio+'"]').attr("selected", "selected");



$(document).ready(function() {
	dtlineafactura=$('#datatable-list-modificar_factura_lineas').DataTable( {
		"scrollY":        "150px",
        "scrollCollapse": true,
        "paging":         false, 	
         "info": false,
         "searching": false,
         select: { style: 'os'},
         language: dataTableLanguage,
         initComplete: function( settings, json ) {
            	window.setTimeout(autoajuste, 500);
        	 	
       		}
         });
})



$("#modificar_factura_button").on("click", function(e) {
	GuardarDatosFactura();
})



function GuardarDatosFactura()
{
	 var data = $("#form_factura_modificar_data").serializeObject();
	 showSpinner("#modal-dialog-form .modal-content");

	 
	 $.ajax({
			type : "post",
			url : "<c:url value='/ajax/factura/factura/factura_modificar.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				
				$("#modal-dialog-form").modal('hide');
				
				dt_listgestionfacturas.ajax.reload(null,false);
						
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
}
</script>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>




<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_correo_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.correo.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_correo_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.correo.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>				
		<a type="button" class="btn btn-info" id="tab_correo_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.correo.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>		
	</div>

	<table id="datatable-list-correo" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th></th>
				<th><spring:message code="facturacion.facturas.tabs.correo.list.header.nombre" /></th>
				<th><spring:message code="facturacion.facturas.tabs.correo.list.header.descripcion" /></th>				
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script>

var dt_listcorreo=$('#datatable-list-correo').DataTable( {
    ajax: {
        url: "<c:url value='/ajax/facturacion/facturas/correo/list_modelosdecorreo.do'/>",
        rowId: 'idmodelocorreo',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Modelocorreo)); return(""); },
        
        error: function (xhr, error, thrown) {
            if (xhr.responseText=="403") {
                  $("#correo-tab").hide();
            }                   
     },
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {dt_listcorreo.columns.adjust().draw(); });
	},
    columns: [	
		{ data: "idmodelo", type: "spanish-string", defaultContent: ""},               
		{ data: "nombre", type: "spanish-string", defaultContent: ""}, 
		{ data: "descripcion", type: "spanish-string", defaultContent: ""},			     
    ],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-correo') },
    select: { style: 'os'},
	language: dataTableLanguage,
	"columnDefs": [
                   { "visible": false, "targets": [0]}
                 ],
	processing: true,
} );

//***********************************************************************************************
insertSmallSpinner("#datatable-list-correo_processing");
//*********************************************BOT�N NUEVO*************************************
$("#tab_correo_new").on("click", function(e) {
	 window.open("<c:url value='/resources/jsp/facturas_edicionAlta.jsp?titulo=Alta de Modelo de correo&idmodelo=&nombre=&descripcion=&remitente=&asunto=&asunto=&contenido='/>","Modelo correo","width=700,height=500");
})
/***********************************************BOT�N EDITAR*************************************/
 	$("#tab_correo_edit").on("click", function(e) { 
	var data = sanitizeArray(dt_listcorreo.rows( { selected: true } ).data(),"idmodelo");
		
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="facturacion.facturas.tabs.correo.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="facturacion.facturas.tabs.correo.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	showButtonSpinner("#tab_gestion_factura_modificar");
	//********************
	
			$.ajax({
				type : "post",
				url : "<c:url value='/ajax/facturacion/facturas/correo/edit_modelodecorreo.do'/>?id="+data[0],
				timeout : 100000,
				data : data,
				success : function(data) {
					
					hideSpinner("tab_gestion_factura_modificar");
					
					var idmodelo=data.Modelocorreo.idmodelo
					var nombre=data.Modelocorreo.nombre
					var descripcion=data.Modelocorreo.descripcion
					var asunto=data.Modelocorreo.asunto
					var remitente=data.Modelocorreo.remitente
					var contenido=data.Modelocorreo.contenido
					
										
					window.open("<c:url value='/resources/jsp/facturas_edicionAlta.jsp?titulo=Alta de Modelo de correo&idmodelo="+idmodelo+"&nombre="+nombre+"&descripcion="+descripcion+"&remitente="+remitente+"&asunto="+asunto+"&contenido="+contenido+"   '/>","Modelo correo","width=700,height=500");
					
				},
				error : function(exception) {
					hideSpinner("tab_gestion_factura_modificar");
				}
			});


})
//***********************************************BOT�N ELIMIMAR***********************************
$("#tab_correo_remove").on("click", function(e) { 
			new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="facturacion.facturas.tabs.correo.list.confirm.eliminar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
	  		  buttons: { closer: false, sticker: false	},
	  		  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
		
			   dt_listcorreo.processing(true);
				
				var data = sanitizeArray(dt_listcorreo.rows( { selected: true } ).data(),"idmodelocorreo");
			   
				$.ajax({
					contenttype: "application/json; charset=utf-8",
					type : "post",
					url : "<c:url value='/ajax/facturacion/facturas/correo/remove_modelosdecorreo.do'/>",
					timeout : 100000,
					data: { data: data.toString() }, 
					success : function(data) {
						dt_listcorreo.ajax.reload();					
					},
					error : function(exception) {
						dt_listcorreo.processing(false);
						
						new PNotify({
						      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						      text: exception.responseText,
							  type: "error",		     
							  delay: 5000,
							  buttons: { sticker: false }
						   });			
					}
				});

		   }).on('pnotify.cancel', function() {
		   });		 

	});
	//***********************************************************************************************************
</script>

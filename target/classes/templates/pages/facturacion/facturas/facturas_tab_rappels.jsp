<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>




<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_rappels_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.rappels.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_rappels_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.rappels.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_rappels_generacion_facturas">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.rappels.list.button.facturas" />"> <span class="fa fa-file-text-o"></span>
			</span>
		</a>		
		<a type="button" class="btn btn-info" id="tab_rappels_definir_periodo">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.rappels.list.button.periodo" />"> <span class="fa fa-calendar"></span>
			</span>
		</a>				
		<a type="button" class="btn btn-info" id="tab_rappels_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.rappels.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>
	</div>

	<table id="datatable-list-rappels" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="facturacion.facturas.tabs.rappels.list.header.nombre" /></th>
				<th><spring:message code="facturacion.facturas.tabs.rappels.list.header.descripcion" /></th>
				<th><spring:message code="facturacion.facturas.tabs.rappels.list.header.criterio" /></th>
				<th><spring:message code="facturacion.facturas.tabs.rappels.list.header.acumulativo" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script>

var dt_listrappels=$('#datatable-list-rappels').DataTable( {
    ajax: {
        url: "<c:url value='/ajax/facturacion/facturas/rappels/list_rappels.do'/>",
        rowId: 'idtiporappel',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Tiporappel)); return(""); },
        
        error: function (xhr, error, thrown) {
            if (xhr.responseText=="403") {
                  $("#rappels-tab").hide();
            }                   
     },
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {dt_listrappels.columns.adjust().draw(); });
	},
    columns: [			
		{ data: "nombre", type: "spanish-string", defaultContent: ""}, 
		{ data: "descripcion", type: "spanish-string", defaultContent: ""},	
		{ data: "criteriorappel", className: "cell_centered",
    		render: function ( data, type, row, meta ) {
  	  	  	if (data==0) return '<spring:message code="facturacion.facturas.tabs.rappels.field.importe" />'; else return '<spring:message code="facturacion.facturas.tabs.rappels.field.volumen" />';	
    	}},		
		{ data: "acumulativo", className: "text_icon cell_centered",
    		render: function ( data, type, row, meta ) {
  	  	  	if (data==1) return '<i class="fa fa-check" aria-hidden="true"></i>'; else return '<i class="fa fa-close" aria-hidden="true"></i>';	
    	}},	     
    ],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-rappels') },
    select: { style: 'os'},
	language: dataTableLanguage,
	processing: true,
} );


insertSmallSpinner("#datatable-list-rappels_processing");
//*********************************************BOT�N NUEVO*************************************
$("#tab_rappels_new").on("click", function(e) {
	 showButtonSpinner("#tabrappels_new");
	 $("#modal-dialog-form .modal-content").load("<c:url value='/ajax/facturacion/facturas/rappels/show_rappel.do'/>", function() {										  
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "sm");
	});
})
/***********************************************BOT�N EDITAR*************************************/
 	$("#tab_rappels_edit").on("click", function(e) { 
	var data = sanitizeArray(dt_listrappels.rows( { selected: true } ).data(),"idtiporappel");
		
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="facturacion.facturas.tabs.rappels.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="facturacion.facturas.tabs.rappels.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#tab_rappels_edit");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/facturacion/facturas/rappels/show_rappel.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "sm");
	});
})

//***********************************************BOT�N ELIMIMAR***********************************
$("#tab_rappels_remove").on("click", function(e) { 
		
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="facturacion.facturas.tabs.rappels.list.confirm.eliminar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
	  		  buttons: { closer: false, sticker: false	},
	  		  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
		
			   dt_listrappels.processing(true);
				
				var data = sanitizeArray(dt_listrappels.rows( { selected: true } ).data(),"idtiporappel");
			   
				$.ajax({
					contenttype: "application/json; charset=utf-8",
					type : "post",
					url : "<c:url value='/ajax/facturacion/facturas/rappels/remove_rappels.do'/>",
					timeout : 100000,
					data: { data: data.toString() }, 
					success : function(data) {
						dt_listrappels.ajax.reload();					
					},
					error : function(exception) {
						dt_listrappels.processing(false);
						
						new PNotify({
						      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						      text: exception.responseText,
							  type: "error",		     
							  delay: 5000,
							  buttons: { sticker: false }
						   });			
					}
				});

		   }).on('pnotify.cancel', function() {
		   });		 

	});
//*********************************************BOT�N DEFINIR PERIODO DE CALCULO*************************************
$("#tab_rappels_definir_periodo").on("click", function(e) {
	 showButtonSpinner("#tab_rappels_definir_periodo");
	 $("#modal-dialog-form .modal-content").load("<c:url value='/ajax/facturacion/facturas/rappels/show_definir_periodo.do'/>", function() {										  
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "xs");
	});
})
//*********************************************BOT�N GENERACI�N FACTURAS*************************************
$("#tab_rappels_generacion_facturas").on("click", function(e) {
	 showButtonSpinner("#tab_rappels_generacion_facturas");
	 $("#modal-dialog-form .modal-content").load("<c:url value='/ajax/facturacion/facturas/rappels/show_generar_factura.do'/>", function() {										  
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "xs");
	});
})
//*******************************************************************************


</script>

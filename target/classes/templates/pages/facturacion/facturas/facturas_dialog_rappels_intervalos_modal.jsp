<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="administracion.productos.tabs.productos.atributos.title" />
	</h4>	
</div>

	<div class="modal-body">

		<form id="form_rappels_intervalos" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		
		<div class="col-md-12 col-sm-12 col-xs-12">

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.rappels.dialog.intervalos.field.orden" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" data-inputmask="'mask': '9{0,10}'" name="orden_intervalo" id="orden_intervalo" required="required" class="form-control" value=""/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.rappels.dialog.intervalos.field.lim_inferior" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" data-inputmask="'mask': '9{0,10}'" name="lim_inferior_intervalo" id="lim_inferior_intervalo" required="required" class="form-control" value=""/>
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.rappels.dialog.intervalos.field.lim_superior" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" data-inputmask="'mask': '9{0,10}'" name="lim_superior_intervalo" id="lim_superior_intervalo" required="required" class="form-control" value=""/>
				</div>
				
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.rappels.dialog.intervalos.field.rappel" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<input type="text" data-inputmask="'mask': '9{0,10}'" name="rappel_intervalo" id="rappel_intervalo" class="form-control" value=""/>										
				</div>
				<label class="control-label col-md-2 col-sm-2 col-xs-2">%</label>
			</div>
		</div>
	</form>
</div>
</div>
<div class="modal-footer">
		<button id="save_intervalo_button" type="button" class="btn btn-primary save_intervalo">
			<spring:message code="common.button.accept" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>
<script>
$(":input").inputmask();
//************************************************
$(".save_intervalo").on("click", function(e) {
	$("#form_rappels_intervalos").submit();	
})
//************************************************
$("#form_rappels_intervalos").validate({
	
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {			
		saveFormIntervalos();
	}
}
);

//*********************************************
function saveFormIntervalos()
{		
	if($("#lim_inferior_intervalo").val()>=$("#lim_superior_intervalo").val())
		{
		new PNotify({
			title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			text : '<spring:message code="facturacion.facturas.tabs.rappels.dialog.intervalos.alert.intervalos" />',
			type : "error",
			delay : 5000,
			buttons : {
				closer : true,
				sticker : false
			}
		});
		return;
		}
	dtintervalos.row.add( ["", $("#orden_intervalo").val(),"Intervalo "+$("#orden_intervalo").val(),$("#lim_inferior_intervalo").val(), $("#lim_superior_intervalo").val(),$("#rappel_intervalo").val(),$("#rappel_intervalo").val()+" %"] ).draw();
	$("#modal-dialog-form-2").modal('hide');
	}
//*********************************************
</script>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${disponibles_datos_sesiones}" var="disponibles_datos_sesiones_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.disponibles.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_establecer_disponibles" class="form-horizontal form-label-left">
	
		<div class="form-group">
		
			<x:forEach select = "$disponibles_datos_sesiones_xml/ArrayList/ArrayList" var = "item">
	
				<div id="tabla-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />" class="col-md-12 col-sm-12 col-xs-12 disponibles">
				
					<div class="col-md-10 col-sm-10 col-xs-12">
	
						<div class="form-group encabezado-disponibilidad">
							<label class="control-label col-md-8 col-sm-8 col-xs-12 label-disponibilidad">Disponibilidad <span class="nombre-producto"><x:out select = "$item/Tipoproducto/nombre" /> </span></label>
							<label class="control-label col-md-1 col-sm-1 col-xs-4""><spring:message code="venta.ventareserva.dialog.disponibles.field.fecha" /></label>
							<div class="col-md-3 col-sm-3 col-xs-8" id="div-fecha-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />">
			                      <div class="input-prepend input-group">
			                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
			                        <input type="text" name="fecha_disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />" id="fecha_disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />" class="form-control" readonly value="<x:out select = "$item/String" />"/>
			                      </div>
							</div>
						</div>
					</div>
				
					<div class="btn-group pull-right btn-datatable">
						<a type="button" class="btn btn-info" id="button_disponibles_lock-<x:out select = "$item/Tipoproducto/idtipoproducto" />">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.bloquear" />"> <span class="fa fa-lock"></span>
							</span>
						</a>
						<a type="button" class="btn btn-info" id="button_disponibles_unlock-<x:out select = "$item/Tipoproducto/idtipoproducto" />">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.desbloquear" />"> <span class="fa fa-unlock"></span>
							</span>
						</a>
					</div>
				
					<div class="col-md-12 col-sm-12 col-xs-12">
						<table id="datatable-list-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />" class="table table-striped table-bordered dt-responsive sesiones" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.idsesion" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.fecha" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.hora" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.overbooking" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.contenido" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.idzona" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.zona" /></th>
									<th class="disponibles"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.disponibles" /></th>
									<th class="bloqueadas"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.bloqueadas" /></th>
									<th class="reservadas"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.reservadas" /></th>
									<th class="vendidas"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.vendidas" /></th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
						<span>&nbsp;</span>
					</div>
				</div>
				
			</x:forEach>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_disponibles_button" type="button" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
			
	</form>
	
</div>

<script>
hideSpinner("#button_add_${buttonAdd}");
hideSpinner("#button_${buttonAdd}_sesiones");

var json_sesiones= [ <c:out value="${disponibles_datos_sesiones_json}"  escapeXml="false" /> ];

function check_selected(tabla) {
	sesiones= tabla.rows().data();
	$.each(sesiones, function(key_sesiones,fila_sesiones) {
		if(${list}.rows({selected: true}).data()[0]!=undefined)
		{
		var	item = ${list}.rows({selected: true}).data()[0][0].lineadetallezonasesions.Lineadetallezonasesion;
		if(item.length>0)
		 	{
				$.each(item, function(key_seleccionadas,fila_seleccionadas) {
					id_zona_sel = fila_seleccionadas.zonasesion.zona.idzona;
					id_sesion_sel =fila_seleccionadas.zonasesion.sesion.idsesion;
					if (fila_sesiones[1]==id_sesion_sel && fila_sesiones[6]==id_zona_sel) tabla.row(key_sesiones).select().draw();
				});
		 	}
		else
			{
			id_zona_sel = item.zonasesion.zona.idzona;
			id_sesion_sel =item.zonasesion.sesion.idsesion;
			if (fila_sesiones[1]==id_sesion_sel && fila_sesiones[6]==id_zona_sel) tabla.row(key_sesiones).select().draw();
			}
		}
	});
}

function get_sesion(idtipoproducto,idsesion,idzona,sesionesdata) {
	var result= null;
	if (sesionesdata==null) sesionesdata= json_sesiones[0].ArrayList;
	if (sesionesdata!="") {
		var item= sesionesdata.ArrayList;
		if (typeof item!="undefined")
			if (item.length>0)
			    $.each(item, function(key, val){
			    	if (val.Tipoproducto.idtipoproducto==idtipoproducto) {
			    		if (val.sesiones!="") {
				   			var sesiones= val.sesiones.Sesion;
				   			if (sesiones.length>0) 
				   			    $.each(sesiones, function(key, val){
				   			    	//if (typeof val.Sesion!="undefined")
					   			    	//if (val.Sesion.idsesion==idsesion) result= val.Sesion
					   			    	if (val.idsesion==idsesion && val.zonasesions.Zonasesion.zona.idzona==idzona) result= val
				   			    });
			   			    else if (sesiones.idsesion==idsesion && sesiones.zonasesions.Zonasesion.zona.idzona==idzona) result= sesiones;
				    	}
			    	}
			    })
			else if (item.Tipoproducto.idtipoproducto==idtipoproducto) {
				if (item.sesiones!="") {
		   			var sesion= item.sesiones.Sesion;
		   			if (sesion.length>0) 
		   			    $.each(sesion, function(key, val){
		   			    	if (val.idsesion==idsesion && val.zonasesions.Zonasesion.zona.idzona==idzona) result= val
		   			    });
	   			    else if (sesion.idsesion==idsesion && sesion.zonasesions.Zonasesion.zona.idzona==idzona) result= sesion;
				}
			}
	}
	return result;
}

<x:forEach select = "$disponibles_datos_sesiones_xml/ArrayList/ArrayList" var = "item">

	
	$('input[name="fecha_disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />"]').daterangepicker({
	    singleDatePicker: true,
	    showDropdowns: true,
		locale: $daterangepicker_sp
	});

	var dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />=$('#datatable-list-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />').DataTable( {
		language: dataTableLanguage,
		info: false,
		searching: false,
		scrollY: "200px",
		scrollCollapse: true,
		paging: false,
	    select: { style: 'single' },
		columnDefs: [
            { "targets": 0, "visible": false },
            { "targets": 1, "visible": false },
            { "targets": 2, "visible": false },
            { "targets": 6, "visible": false }
	    ],
	    "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
            /* Append the grade to the default row class name */
            var sesion = aData[0];
            if(sesion.bloqueado==1)
            	$('td', nRow).css('color', 'Red');
            
        },
	    drawCallback: function( settings ) { 
	    	activateTooltipsInTable('datatable-list-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />');
	   	}
	} );
	
	function ajustar_cabeceras_datatable_<x:out select = "$item/Tipoproducto/idtipoproducto" />()
	{
		$('#datatable-list-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />').DataTable().columns.adjust().draw();
	}
	
	insertSmallSpinner("#datatable-list-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />_processing");
	
	<x:forEach select = "$item/sesiones/Sesion" var="sesion">
		dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.row.add([
	         get_sesion("<x:out select = "$item/Tipoproducto/idtipoproducto" />","<x:out select = "$sesion/idsesion" />","<x:out select = "$sesion/zonasesions/Zonasesion/zona/idzona" />",null), 
	         "<x:out select = "$sesion/idsesion" />", 
	         "<x:out select = "$sesion/fecha" />",         
	         "<x:out select = "$sesion/horainicio" />", 
	         "<x:out select = "$sesion/overbookingventa" />", 
	         "<span title='<x:out select = "$sesion/contenido/descripcion" />'> <x:out select = "$sesion/contenido/nombre" /> </span>",
	         "<x:out select = "$sesion/zonasesions/Zonasesion/zona/idzona" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/zona/nombre" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/numlibres" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/numbloqueadas" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/numreservadas" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/numvendidas" />" 
	    ]);
	</x:forEach>
	dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.draw();
	setTimeout(ajustar_cabeceras_datatable_<x:out select = "$item/Tipoproducto/idtipoproducto" />, 500);
	check_selected(dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />);

	//********************************************************************************
		
	$('input[name="fecha_disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />"]').on("change", function() { 
		//dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.processing(true);
		showFieldSpinner("#div-fecha-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />");
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/venta/ventareserva/list_tipos_producto.do'/>",
			timeout : 100000,
			data: {
	  			   idtipoproducto: "<x:out select = "$item/Tipoproducto/idtipoproducto" />",
		    	   fecha: $('input[name="fecha_disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />"]').val()
				  }, 
			success : function(data) {
				if (data.ArrayList!="") {
					dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.rows().remove().draw();
					var item= data.ArrayList.sesiones.Sesion;
					if (typeof item!="undefined")
						if (item.length>0)
						    $.each(item, function(key, sesion){
								dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.row.add([
								     sesion,							                                                                                   
							         sesion.idsesion, 
							         sesion.fecha, 
							         sesion.horainicio, 
							         sesion.overbookingventa, 
							         "<span title='"+sesion.contenido.descripcion+"'> "+sesion.contenido.nombre+" </span>",
							         sesion.zonasesions.Zonasesion.zona.idzona, 
							         sesion.zonasesions.Zonasesion.zona.nombre, 
							         sesion.zonasesions.Zonasesion.numlibres, 
							         sesion.zonasesions.Zonasesion.numbloqueadas, 
							         sesion.zonasesions.Zonasesion.numreservadas, 
							         sesion.zonasesions.Zonasesion.numvendidas 
							    ]);
						    });
						else
							dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.row.add([
							     item,                                                                              			
						         item.idsesion, 
						         item.fecha, 
						         item.horainicio, 
						         item.overbookingventa, 
						         item.contenido.nombre, 
						         item.zonasesions.Zonasesion.zona.idzona, 
						         item.zonasesions.Zonasesion.zona.nombre, 
						         item.zonasesions.Zonasesion.numlibres, 
						         item.zonasesions.Zonasesion.numbloqueadas, 
						         item.zonasesions.Zonasesion.numreservadas, 
						         item.zonasesions.Zonasesion.numvendidas 
						    ]);
						
					//dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.processing(false);
					hideSpinner("#div-fecha-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />");
				    dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.draw();
				}
			},
			error : function(exception) {
				//dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.processing(false);
				hideSpinner("#div-fecha-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />");
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",
					  buttons: { sticker: false }				  
				   });		
			}
		});	
	});

	//********************************************************************************
	$("#button_disponibles_lock-<x:out select = "$item/Tipoproducto/idtipoproducto" />").on("click", function(e) {
		var data = dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.rows( { selected: true } ).data();

		if (data.length<=0) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="administracion.programacion.programacion.list.alert.localidades.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}	

		var button_bloqueos= "button_disponibles_lock-<x:out select = "$item/Tipoproducto/idtipoproducto" />",
			lista_bloqueos="dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />";
		
		showButtonSpinner("#"+button_bloqueos);
		$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/show_new_bloqueo.do'/>?id="+data[0][0]+"&button="+button_bloqueos+"&lista="+lista_bloqueos+"&zona="+data[0][5]+"&es_programacion=false&idx_libres=7&idx_bloqueadas=8", function() {
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "sm");
		});
	});
	
	//********************************************************************************
	$("#button_disponibles_unlock-<x:out select = "$item/Tipoproducto/idtipoproducto" />").on("click", function(e) { 
		
		var data = dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.rows( { selected: true } ).data();
		
		if (data.length<=0) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="administracion.programacion.programacion.list.alert.localidades.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}	
		
		var button_bloqueos= "button_disponibles_unlock-<x:out select = "$item/Tipoproducto/idtipoproducto" />",
		lista_bloqueos="dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />";
		
		showButtonSpinner("#button_programacion_bloqueos");
		$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/show_bloqueos.do'/>?id="+data[0][0]+"&button="+button_bloqueos+"&lista="+lista_bloqueos+"&zona="+data[0][5]+"&es_programacion=false&idx_libres=7&idx_bloqueadas=8", function() {
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "md");
		});

	});	

</x:forEach>

function obtenerIdTarifa(tarifas,idperfilvisitante) {
	for (var i=0; i<tarifas.length; i++) 
		if (tarifas[i].idperfilvisitante==idperfilvisitante) return(tarifas[i].tarifa.idtarifa); 
	return("");
}

function construir_xml_sesiones(sesiones_sel) {
	var xml_sesiones= "<sesiones>";
	for (var i=0; i<sesiones_sel.length; i++) { 
		xml_sesiones+="<Sesion>";
		xml_sesiones+=json2xml(sesiones_sel[i][0],"");
		xml_sesiones+="</Sesion>";
	}
	xml_sesiones+="</sesiones>";
	return(xml_sesiones);
}

$("#save_disponibles_button").on("click", function(e) {
	var sesiones= $(".sesiones").DataTable().rows( { selected: true } ).data();
	if (sesiones.length < $(".sesiones.dtr-inline").length) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	for (var i=0; i<sesiones.length; i++) 
	{
		if(sesiones[i][0].bloqueado==1)
			{
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="venta.ventareserva.list.alert.productos.seleccion.bloqueado" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
			}			
	}
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/list_datos_sesiones.do'/>",
		timeout : 100000,
		data: {
				idproducto: "${idProducto}",
				idcliente: "${idCliente}",
				idtipoventa: "${idTipoVenta}",
				idcanal: "${sessionScope.idcanal}",
				fecha: "${fecha}",
				xml: construir_xml_sesiones(sesiones)
			  }, 
		success : function(data) {
			
			var fechas= "", contenidos= "", disponibles= "",
				retorno= false;
			
			for (var i=0; i<sesiones.length; i++) {
				if (retorno) {
					fechas+="</br>";
					contenidos+= "</br>";
					disponibles+= "</br>";
				}
				fechas+= sesiones[i][2].split("-")[0],
				contenidos+= sesiones[i][3]+" "+sesiones[i][5];
				disponibles+= sesiones[i][8];
				retorno= true;
			}
			var edicion=${edicion},
				bono= "";
			if (edicion==1) {
				sesion_data=$.extend(true,[],dt_listventaindividual.rows( { selected: true } ).data()[0][0]);
				sesion_data.lineadetallezonasesions= data.Lineadetalle.lineadetallezonasesions;
				bono= dt_listventaindividual.rows( { selected: true } ).data()[0][12];
				dt_listventaindividual.rows( { selected: true } ).remove(); // Se borra la línea si se edita la sesión
			}
			else
				sesion_data= data.Lineadetalle;
			var perfil = ""+sesion_data.perfilvisitante;
			
			if(perfil!="")
				perfil = perfil.nombre;
			
			
			dt_listventaindividual.row.add([sesion_data,
			                 "${idProducto}", 
	                         "${producto}",
				             fechas, // fecha
				             contenidos, // contenido
				             disponibles, // disponibles
				             sesion_data.cantidad,  // Nº Etd
				             typeof perfil=="undefined"?"":perfil, // Perfil/Tarifa
				             "", // Descuentos
				             sesion_data.tarifaproducto.importe, // I. Unitario
				             sesion_data.importe,  // Importe total
				             sesiones, // Datos de las sesiones
				             bono, // Bono
				             typeof sesion_data.perfiles.Perfilvisitante=="undefined"?"":obtenerIdTarifa(sesion_data.perfiles.Perfilvisitante,sesion_data.perfilvisitante.idperfilvisitante),
				             "",  // iddescuento
				             sesion_data.perfilvisitante.idperfilvisitante   // idperfil
			                ]).draw();
			
			var idcliente = $("#idcliente_rectificacion").val();
			var idtipoventa = $("#idTipoVenta_rectificacion").val(); 
			
			obtener_totales_venta_temporal(dt_listventaindividual,idcliente,idtipoventa);
			
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});	

	$("#modal-dialog-form-3").modal('hide');
});	

</script>


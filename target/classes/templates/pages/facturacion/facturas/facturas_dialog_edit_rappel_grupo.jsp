<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_rappel_grupo}" var="editar_rappel_grupo_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<x:choose>
			<x:when select="not($idRappel)">
				<spring:message code="facturacion.facturas.tabs.clientes.dialog.crear_rappel_grupo.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="facturacion.facturas.tabs.clientes.dialog.editar_rappel_grupo.title" />
			</x:otherwise>
		</x:choose>		
	</h4>	
</div>

<div class="modal-body">
	<form id="form_rappel_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input id="idtiporappel" name="idtiporappel" type="hidden" value="${idtiporappel}" />    
		<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.grupos.field.nombre_rappel" />*</label>
				<div class="col-md-8 col-sm-8 col-xs-8">
					<select name="idtiporappel" id="selector_tipo_rappel" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$editar_rappel_grupo_xml/ArrayList/Tiporappel" var="item">
							<option value="<x:out select="$item/idtiporappel" />"><x:out select="$item/nombre" /></option>
						</x:forEach>
					</select>
				</div>
		</div>
		
		<div class="form-group date-picker">
			<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.grupos.field.fecha" />*</label>
			<div class="col-md-8 col-sm-8 col-xs-8">
				  <a type="button" class="btn btn-default btn-clear-date" id="button_apertura_clear">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="ventareserva.operacionescaja.operacionescaja.list.button.clear" />"> <span class="fa fa-trash"></span>
						</span>
				  </a>			
                      <div class="input-prepend input-group">
                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                        <input type="text" name="fecha_rappel" id="fecha_rappel" class="form-control" value="" readonly/>
                        <input type="hidden" required="required" id="fechainiciorappel" name="fechainiciorappel"/>
                        <input type="hidden" id="fechafinrappel" name="fechafinrappel" />
                      </div>
			</div>
		</div>			


	</form>

	<div class="modal-footer">
		<button id="save_canal_button" type="button" class="btn btn-primary save_grupo_rapel_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>		

</div>

<script>

		$today= moment().format("DD/MM/YYYY");
		$tomorrow = moment().add("days",1).format("DD/MM/YYYY");
			

		
		//Ahora venta
		dia_inicio = $today;
		dia_fin = $tomorrow;
		
		var creando="${creando}";
		
		if(creando=="false")
			{
			var idtipoRappel = "${idRappel}";
			dia_inicio = "${fechainicio}";
			dia_fin = "${fechafin}";
			$('#selector_tipo_rappel option[value="'+idtipoRappel+'"]').attr("selected", "selected");
			}
		
		$('input[name="fecha_rappel"]').val(dia_inicio + ' - ' + dia_fin);
     	$('input[name="fechainiciorappel"]').val(dia_inicio);
 	  	$('input[name="fechafinrappel"]').val(dia_fin);

		$('input[name="fecha_rappel"]').val( dia_inicio + ' - ' + dia_fin);
		
		$('input[name="fecha_rappel"]').daterangepicker({
			autoUpdateInput: false,
			linkedCalendars: false,
			autoApply: true,
		  	locale: $daterangepicker_sp
			}, function(start,end) {
		  	  	 $('input[name="fecha_rappel"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
		  	  	 $('input[name="fechainiciorappel"]').val(start.format('DD/MM/YYYY'));
		  	  	 $('input[name="fechafinrappel"]').val(end.format('DD/MM/YYYY'));
			 });
			 
		
		$(".save_grupo_rapel_dialog").on("click", function(e) {	
			
			if(creando=="false")
				{
				dtrappels.rows( '.selected' ).remove();
				}
							
			dtrappels.row.add( [ ""+$("#id").val(),$("#selector_tipo_rappel").val(), $("#selector_tipo_rappel option:selected").text(),
			                   $("#fechainiciorappel").val(),$("#fechafinrappel").val()] )
		    .draw();
		    $("#modal-dialog-form-2").modal('hide');	
			
		});
</script>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>




<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_periodos_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.periodos.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_periodos_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.periodos.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>				
		<a type="button" class="btn btn-info" id="tab_periodos_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.periodos.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>
	</div>

	<table id="datatable-list-periodos" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="facturacion.facturas.tabs.periodos.list.header.nombre" /></th>
				<th><spring:message code="facturacion.facturas.tabs.periodos.list.header.descripcion" /></th>
				<th><spring:message code="facturacion.facturas.tabs.periodos.list.header.tipoperiodo" /></th>				
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script>

var dt_listperiodos=$('#datatable-list-periodos').DataTable( {
    ajax: {
        url: "<c:url value='/ajax/facturacion/facturas/periodos/list_periodos.do'/>",
        rowId: 'idperiodofacturacion',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Periodofacturacion)); return(""); },
        
        error: function (xhr, error, thrown) {
            if (xhr.responseText=="403") {
                  $("#periodos-tab").hide();
            } 
            else
            	{
            	new PNotify({
 					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
 					text : xhr.responseText,
 					type : "error",
 					delay : 5000,
 					buttons : {
 						closer : true,
 						sticker : false
 					}					
 				});
            	}
     },
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {dt_listperiodos.columns.adjust().draw(); });
	},
    columns: [			
		{ data: "nombre", type: "spanish-string", defaultContent: ""}, 
		{ data: "descripcion", type: "spanish-string", defaultContent: ""},	
		{ data: "tipoperiodo", type: "spanish-string", defaultContent: ""},			     
    ],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-periodos') },
    select: { style: 'os'},
	language: dataTableLanguage,
	processing: true,
} );


insertSmallSpinner("#datatable-list-periodos_processing");
//*********************************************BOT�N NUEVO*************************************
$("#tab_periodos_new").on("click", function(e) {
	 showButtonSpinner("#tab_periodos_new");
	 $("#modal-dialog-form .modal-content").load("<c:url value='/ajax/facturacion/facturas/periodos/show_periodo.do'/>", function() {										  
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
})
/***********************************************BOT�N EDITAR*************************************/
 	$("#tab_periodos_edit").on("click", function(e) { 
	var data = sanitizeArray(dt_listperiodos.rows( { selected: true } ).data(),"idperiodofacturacion");
		
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="facturacion.facturas.tabs.periodos.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="facturacion.facturas.tabs.periodos.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#tab_periodos_edit");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/facturacion/facturas/periodos/show_periodo.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
})
//***********************************************BOT�N ELIMIMAR***********************************
$("#tab_periodos_remove").on("click", function(e) { 
		
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="facturacion.facturas.tabs.periodos.list.confirm.eliminar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
	  		  buttons: { closer: false, sticker: false	},
	  		  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
		
			   dt_listperiodos.processing(true);
				
				var data = sanitizeArray(dt_listperiodos.rows( { selected: true } ).data(),"idperiodofacturacion");
			   
				$.ajax({
					contenttype: "application/json; charset=utf-8",
					type : "post",
					url : "<c:url value='/ajax/facturacion/facturas/periodos/remove_periodos.do'/>",
					timeout : 100000,
					data: { data: data.toString() }, 
					success : function(data) {
						dt_listperiodos.ajax.reload();					
					},
					error : function(exception) {
						dt_listperiodos.processing(false);
						
						new PNotify({
						      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						      text: exception.responseText,
							  type: "error",		     
							  delay: 5000,
							  buttons: { sticker: false }
						   });			
					}
				});

		   }).on('pnotify.cancel', function() {
		   });		 

	});
	//***********************************************
</script>

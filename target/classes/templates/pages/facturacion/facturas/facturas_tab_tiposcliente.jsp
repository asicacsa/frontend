<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>



<div class="col-md-12 col-sm-12 col-xs-12">
<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_tiposcliente_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.tiposcliente.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_tiposcliente_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.tiposcliente.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_tiposcliente_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.tiposcliente.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>		
	</div>

	<table id="datatable-lista-tiposclientes" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="facturacion.facturas.tabs.clientes.list.header.nombre" /></th>
				<th><spring:message code="facturacion.facturas.tabs.clientes.list.header.contrato" /></th>
				<th><spring:message code="facturacion.facturas.tabs.clientes.list.header.descuento" /></th>				
				<th><spring:message code="facturacion.facturas.tabs.clientes.list.header.comision" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<script>

var dt_listtiposcliente=$('#datatable-lista-tiposclientes').DataTable( {	
    ajax: {
        url: "<c:url value='/ajax/facturacion/facturas/tiposcliente/list_tiposcliente.do'/>",
        rowId: 'idtipocliente',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Tipocliente)); return(""); },
       
        error: function (xhr, error, thrown) {
            if (xhr.responseText=="403") {
                  $("#tiposcliente-tab").hide();
            }          
            else
            	{
            	
            	new PNotify({
 					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
 					text : xhr.responseText,
 					type : "error",
 					delay : 5000,
 					buttons : {
 						closer : true,
 						sticker : false
 					}					
 				});
            	}
     },
        
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {dt_listtiposcliente.columns.adjust().draw(); });
	},
    columns: [
              { data: "nombre", type: "spanish-string" ,  defaultContent:""} ,
              { data: "condicionpagoultima.credito", className: "cell_centered",
          		render: function ( data, type, row, meta ) {
        	  	  	if (data==1) return '<spring:message code="facturacion.facturas.tabs.clientes.field.credito" />'; else return '<spring:message code="facturacion.facturas.tabs.clientes.field.prepago" />';	
          	}},
              { data: "porcentajedescuento", type: "spanish-string" ,  defaultContent:""} ,
              { data: "porcentajecomision", type: "spanish-string" ,  defaultContent:""} ,
              
    ],    
    drawCallback: function( settings ) {
    	 activateTooltipsInTable('datatable-lista-tiposclientes')
    },
    select: { style: 'os'},
	language: dataTableLanguage,
	processing: true,
} );

insertSmallSpinner("#datatable-lista-tiposcliente_processing");


//********************************************************************************
$("#tab_tiposcliente_new").on("click", function(e) {
	 showButtonSpinner("#tab_tiposcliente_new");
	 $("#modal-dialog-form .modal-content").load("<c:url value='/ajax/facturacion/facturas/clientes/show_tipo_cliente.do'/>", function() {										  
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "lg");
	});
})

	/***********************************************BOT�N EDITAR*************************************/
 	$("#tab_tiposcliente_edit").on("click", function(e) { 
	var data = sanitizeArray(dt_listtiposcliente.rows( { selected: true } ).data(),"idtipocliente");
		
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="facturacion.facturas.tabs.tipo.clientes.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="facturacion.facturas.tabs.tipo.clientes.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#tab_tiposcliente_edit");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/facturacion/facturas/clientes/show_tipo_cliente.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "lg");
	});
})

//***********************************************BOT�N ELIMIMAR***********************************
$("#tab_tiposcliente_remove").on("click", function(e) { 
		
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="facturacion.facturas.tabs.tipo.clientes.list.confirm.eliminar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
	  		  buttons: { closer: false, sticker: false	},
	  		  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
		
			   dt_listtiposcliente.processing(true);
				
				var data = sanitizeArray(dt_listtiposcliente.rows( { selected: true } ).data(),"idtipocliente");
			   
				$.ajax({
					contenttype: "application/json; charset=utf-8",
					type : "post",
					url : "<c:url value='/ajax/facturacion/facturas/tipoCliente/remove_tipoCliente.do'/>",
					timeout : 100000,
					data: { data: data.toString() }, 
					success : function(data) {
						dt_listtiposcliente.ajax.reload();					
					},
					error : function(exception) {
						dt_listtiposcliente.processing(false);
						
						new PNotify({
						      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						      text: exception.responseText,
							  type: "error",		     
							  delay: 5000,
							  buttons: { sticker: false }
						   });			
					}
				});

		   }).on('pnotify.cancel', function() {
		   });		 

	}); 

</script>

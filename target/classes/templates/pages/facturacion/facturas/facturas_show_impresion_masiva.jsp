<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>




<div class="modal-header">

	<div class="modal-header">
		<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">�</span>
		</button>
		 <h4 class="modal-title">	
					<spring:message code="facturacion.facturas.tabs.gestionfacturas.impresionmasiva.title" />
	 	</h4>	
	</div>
	
	<div class="modal-body">
			<form id="form_impresion_masiva" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12"><spring:message code="facturacion.facturas.tabs.gestionfacturas.impresionmasiva.text" />
					</div>
				</div>
				<fieldset id="impresion">
					<div class="form-group">
						<div class="checkbox col-md-12 col-sm-12 col-xs-12">
							<input type="radio" name="impresion" id="impresionSI" checked value="1" data-parsley-multiple="periodicidad" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.tabs.gestionfacturas.impresionmasiva.opcionSI.title" /></strong>
						</div>		
					</div>
					<div class="form-group">
						<div class="checkbox col-md-12 col-sm-12 col-xs-12">
							<input type="radio" name="impresion" id="impresionNO" value="2" data-parsley-multiple="periodicidad" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.tabs.gestionfacturas.impresionmasiva.opcionno.title" /></strong>
						</div>		
					</div>		
				</fieldset>
			</form>
	</div>

	<div class="modal-footer">
		<button id="finalizarImpresion" type="button" class="btn btn-primary finalizarImpresion">
			<spring:message code="common.button.accept" />
		</button>		
	</div>	
</div>
<script>
if ($("input.flat")[0]) {
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });
}

//********************************************
$("#finalizarImpresion").on("click", function(e) {
	if ($("#impresionSI:checked").length)
	 {
		var id_grupo = $("#id_grupo_impresion").val();
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/facturacion/actualizar_impresion_masiva.do'/>",
			timeout : 100000,
			data: {
				id_grupo: id_grupo
			}, 
			success : function(data) {
				},
			error : function(exception) {
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",		     
					  delay: 5000,
					  buttons: { sticker: false }
				   });			
			}
		});
	 }
	
	$("#modal-dialog-form").modal('hide');	
})

</script>


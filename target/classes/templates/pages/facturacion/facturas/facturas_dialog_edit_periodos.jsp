<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_periodo}" var="editar_periodo_xml" />
<x:parse xml="${editar_periodo_tipoperiodo}" var="editar_periodo_tipoperiodo_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	 <h4 class="modal-title">	
		<x:choose>
			<x:when select="not($editar_periodo_xml/Periodofacturacion)">
				<spring:message code="facturacion.facturas.tabs.periodos.dialog.crear_periodo.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="facturacion.facturas.tabs.periodos.dialog.editar_periodo.title" />
			</x:otherwise>
		</x:choose>		
	</h4>	
</div>


<div class="modal-body">
	<form id="form_periodo_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input id="idperiodofacturacion" name="idperiodofacturacion" type="hidden" value="<x:out select = "$editar_periodo_xml/Periodofacturacion/idperiodofacturacion" />" />
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.periodos.field.nombre" />*</label>
			<div class="col-md-9 col-sm-9 col-xs-9">
				<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editar_periodo_xml/Periodofacturacion/nombre" />">
			</div>
		</div>			
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.periodos.field.descripcion" />*</label>
			<div class="col-md-9 col-sm-9 col-xs-9">	
				<input name="descripcion" id="descripcion" class="form-control"  value="<x:out select = "$editar_periodo_xml/Periodofacturacion/descripcion" />"/>	
			</div>							
		</div>		
		<div id="grupo-periodicidad">	
			<div class="x_title">
				<h5><spring:message code="facturacion.facturas.tabs.periodos.field.periodicidad" /></h5>
			<div class="clearfix"></div>
		</div>				
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-8 col-sm-8 col-xs-8">
			<fieldset id="periodicidad">
				<div class="form-group">
					<div class="checkbox col-md-4 col-sm-4 col-xs-4">
						<input type="radio" name="periodicidad" id="prestacion" checked value="6" data-parsley-multiple="periodicidad" class="flat" style="position: absolute; opacity: 0;">
						<strong><spring:message code="facturacion.facturas.tabs.periodos.field.prestacion" /></strong>
					</div>		
				</div>		
				<div class="form-group">
					<div class="checkbox col-md-4 col-sm-4 col-xs-4">
						<input type="radio" name="periodicidad" id="diaria" value="1" data-parsley-multiple="periodicidad" class="flat" style="position: absolute; opacity: 0;">
						<strong><spring:message code="facturacion.facturas.tabs.periodos.field.diaria" /></strong>
					</div>
					<label class="control-label col-md-1 col-sm-1 col-xs-1"><spring:message code="facturacion.facturas.tabs.periodos.field.cada" /></label>
						<div class="col-md-4 col-sm-4 col-xs-4">
							<input name="numdias" id="numdias"  type="text" data-inputmask="'mask': '9{0,10}'" class="form-control" value="<x:out select = "$editar_periodo_xml/Periodofacturacion/numdias" />">
						</div>
							<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.periodos.field.dias" /></label>
				</div>
				<div class="form-group">
							<div class="checkbox col-md-5 col-sm-5 col-xs-5">
							<input type="radio" name="periodicidad" id="semanal" value="2" data-parsley-multiple="periodicidad" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.tabs.periodos.field.semanal" /></strong>
						</div>
						<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.periodos.field.el" /></label>
						<div class="col-md-2 col-sm-2 col-xs-2">
							<select name="diasemanal" id="diasemanal" class="form-control">
								<option value=""></option>
								<option value="1"><spring:message code="common.dialog.text.weekdays.mo" /></option>
								<option value="2"><spring:message code="common.dialog.text.weekdays.tu" /></option>
								<option value="3"><spring:message code="common.dialog.text.weekdays.we" /></option>
								<option value="4"><spring:message code="common.dialog.text.weekdays.th" /></option>
								<option value="5"><spring:message code="common.dialog.text.weekdays.fr" /></option>
								<option value="6"><spring:message code="common.dialog.text.weekdays.sa" /></option>
								<option value="7"><spring:message code="common.dialog.text.weekdays.su" /></option>
							</select>
						</div>
						<label class="control-label col-md-1 col-sm-1 col-xs-1"><spring:message code="facturacion.facturas.tabs.periodos.field.cada" /></label>												
						<div class="col-md-1 col-sm-1 col-xs-1">
							<input name="numsemanas" id="numsemanas" type="text" data-inputmask="'mask': '9{0,10}'" class="form-control" value="<x:out select = "$editar_periodo_xml/Periodofacturacion/numsemanas" />">
						</div>
						<label class="control-label col-md-1 col-sm-1 col-xs-1"><spring:message code="facturacion.facturas.tabs.periodos.field.semanas" /></label>					
					</div>	
					<div class="form-group">
						<div class="checkbox col-md-5 col-sm-5 col-xs-5">
							<input type="radio" name="periodicidad" id="mensual" value="3" data-parsley-multiple="periodicidad" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.tabs.periodos.field.mensual" /></strong>
						</div>
						<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.periodos.field.eldia" /></label>
						<div class="col-md-2 col-sm-2 col-xs-2">
							<input name="diamensual" id="diamensual" type="text" data-inputmask="'mask': '9{0,10}'" class="form-control" value="<x:out select = "$editar_periodo_xml/Periodofacturacion/dia" />">	
						</div>
						<label class="control-label col-md-1 col-sm-1 col-xs-1"><spring:message code="facturacion.facturas.tabs.periodos.field.cada" /></label>												
						<div class="col-md-1 col-sm-1 col-xs-1">
							<input name="mesmensual" id="mesmensual" type="text" data-inputmask="'mask': '9{0,10}'" class="form-control" value="<x:out select = "$editar_periodo_xml/Periodofacturacion/nummeses" />">
						</div>
						<label class="control-label col-md-1 col-sm-1 col-xs-1"><spring:message code="facturacion.facturas.tabs.periodos.field.meses" /></label>						
					</div>
					
					
					<div class="form-group">
						<div class="checkbox col-md-4 col-sm-4 col-xs-4">
							<input type="radio" name="periodicidad" id="mensualxdia" value="4" data-parsley-multiple="periodicidad" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.tabs.periodos.field.mensualxdia" /></strong>
						</div>
						<label class="control-label col-md-1 col-sm-1 col-xs-1"><spring:message code="facturacion.facturas.tabs.periodos.field.el" /></label>
						<div class="col-md-2 col-sm-2 col-xs-2">
							<select name="ordinaldiasemana" id="ordinaldiasemana" class="form-control">
								<option value=""></option>
								<option value="1"><spring:message code="facturacion.facturas.tabs.periodos.field.option.primero" /></option>
								<option value="2"><spring:message code="facturacion.facturas.tabs.periodos.field.option.segundo" /></option>
								<option value="3"><spring:message code="facturacion.facturas.tabs.periodos.field.option.tercero" /></option>
								<option value="4"><spring:message code="facturacion.facturas.tabs.periodos.field.option.cuarto" /></option>
								<option value="5"><spring:message code="facturacion.facturas.tabs.periodos.field.option.ultimo" /></option>								
							</select>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-2">
							<select name="diasemana" id="diasemana" class="form-control">
								<option value=""></option>
								<option value="1"><spring:message code="common.dialog.text.weekdays.mo" /></option>
								<option value="2"><spring:message code="common.dialog.text.weekdays.tu" /></option>
								<option value="3"><spring:message code="common.dialog.text.weekdays.we" /></option>
								<option value="4"><spring:message code="common.dialog.text.weekdays.th" /></option>
								<option value="5"><spring:message code="common.dialog.text.weekdays.fr" /></option>
								<option value="6"><spring:message code="common.dialog.text.weekdays.sa" /></option>
								<option value="7"><spring:message code="common.dialog.text.weekdays.su" /></option>
							</select>
						</div>
						<label class="control-label col-md-1 col-sm-1 col-xs-1"><spring:message code="facturacion.facturas.tabs.periodos.field.cada" /></label>												
						<div class="col-md-1 col-sm-1 col-xs-1">
							<input name="cadameses" id="cadameses" type="text" data-inputmask="'mask': '9{0,10}'" class="form-control" value="<x:out select = "$editar_periodo_xml/Periodofacturacion/nummeses" />">
						</div>
						<label class="control-label col-md-1 col-sm-1 col-xs-1"><spring:message code="facturacion.facturas.tabs.periodos.field.meses" /></label>						
					</div>
					
					
					<div class="form-group">
						<div class="checkbox col-md-4 col-sm-4 col-xs-4">
							<input type="radio" name="periodicidad" id="anual" value="5" data-parsley-multiple="periodicidad" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.tabs.periodos.field.anual" /></strong>
						</div>
						<label class="control-label col-md-1 col-sm-1 col-xs-1"><spring:message code="facturacion.facturas.tabs.periodos.field.el" /></label>
						<div class="col-md-3 col-sm-3 col-xs-3">
							<input name="dia" id="dia" type="text" data-inputmask="'mask': '9{0,10}'" class="form-control" value="<x:out select = "$editar_periodo_xml/Periodofacturacion/dia" />">
						</div>
						<label class="control-label col-md-1 col-sm-1 col-xs-1"><spring:message code="facturacion.facturas.tabs.periodos.field.de" /></label>
						<div class="col-md-3 col-sm-3 col-xs-3">
							<select name="nummeses" id="nummeses" class="form-control">
								<option value=""></option>
								<option value="1"><spring:message code="common.dialog.text.month.jan" /></option>
								<option value="2"><spring:message code="common.dialog.text.month.feb" /></option>
								<option value="3"><spring:message code="common.dialog.text.month.mar" /></option>
								<option value="4"><spring:message code="common.dialog.text.month.apr" /></option>
								<option value="5"><spring:message code="common.dialog.text.month.may" /></option>
								<option value="6"><spring:message code="common.dialog.text.month.jun" /></option>
								<option value="7"><spring:message code="common.dialog.text.month.jul" /></option>
								<option value="8"><spring:message code="common.dialog.text.month.aug" /></option>
								<option value="9"><spring:message code="common.dialog.text.month.sep" /></option>
								<option value="10"><spring:message code="common.dialog.text.month.oct" /></option>
								<option value="11"><spring:message code="common.dialog.text.month.nov" /></option>
								<option value="12"><spring:message code="common.dialog.text.month.dec" /></option>								
							</select>
						</div>
					</div>
				</fieldset>				
			</div>
			<div class="col-md-4 col-sm-4 col-xs-4">
				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">					
						<textarea name="descripciontipoperiodo" id="descripciontipoperiodo" class="form-control"  ></textarea>
					</div>
				</div>
			</div>
		</div>			
	</div>
</div>	
</form>
	<div class="modal-footer">
		<button id="save_periodo_button" type="button" class="btn btn-primary save_periodo_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>	

<script>
$(document).ready(function() {
hideSpinner("#tab_periodos_new");
hideSpinner("#tab_periodos_edit");
//********************************************^
var descripciones={};
//Cargar el xml en el hasmap
  var mapDescripciones = new Map()
	<x:forEach select="$editar_periodo_tipoperiodo_xml/ArrayList/Tipoperiodofacturacion" var="item">
		mapDescripciones.set('<x:out select="$item/@id" />','<x:out select="$item/descripcion" />');            
	</x:forEach>
//********************************************
$(":input").inputmask();
//*********************************************
$('#ordinaldiasemana option[value="<x:out select = "$editar_periodo_tipoperiodo_xml/Periodofacturacion/ordinaldiasemana" />"]').attr("selected", "selected");
//*********************************************

if ($("input.flat")[0]) {
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });
}
		//**********************************************
		var seleccion = '<x:out select= "$editar_periodo_xml/Periodofacturacion/tipoperiodofacturacion/idtipoperiodofacturacion" />';
		$("input[name=periodicidad][value='"+seleccion+"']").prop("checked",true);
		
		//**********************************************		
		
		 if ($("#prestacion:checked").length)
		 {
			 habilitarCamposPrestaciones();
		 }
		 if ($("#diaria:checked").length)
		 {
			 habilitarCamposDiaria();
		 }
		 if ($("#semanal:checked").length)
		 {
			 habilitarCamposSemanal();
		 }
		 if ($("#mensual:checked").length)
		 {
			 habilitarCamposMensual();
		 }
		 if ($("#mensualxdia:checked").length)
		 {
			 habilitarCamposMensualXDia();
		 }
		 if ($("#anual:checked").length)
		 {
			 habilitarCamposAnual();
		 }

//*********************************************
  $("#prestacion").on("ifChanged", function(e) { 
 	if ($("#prestacion:checked").length)
		{	
	 		habilitarCamposPrestaciones();	
		}
 }); 
 //********************************************
 $("#diaria").on("ifChanged", function(e) { 	
	 if ($("#diaria").length){
		 habilitarCamposDiaria();
		}   
 }); 
 
//********************************************
 $("#semanal").on("ifChanged", function(e) {	 	
	 if ($("#semanal").length){
		 habilitarCamposSemanal();
		}   
 }); 
//********************************************
 $("#mensual").on("ifChanged", function(e) {	 
	 if ($("#mensual").length){
		 habilitarCamposMensual();
		}   
}); 
//********************************************

 $("#mensualxdia").on("ifChanged", function(e) {	 
	 if ($("#mensualxdia").length){
		 habilitarCamposMensualXDia();	
	}   
	
}); 
//********************************************

 $("#anual").on("ifChanged", function(e) {	 
 	if ($("#anual:checked").length){
	 	habilitarCamposAnual();
	 }
 }); 
//*******************************************
function borrarDatos()
{
	 				
} 
function habilitarCamposPrestaciones()
{
	$('input[name="numdias"]').val('');
 	$("#diasemanal").prop('selectedIndex', -1);
	$('input[name="numsemanas"]').val('');
	$('input[name="diamensual"]').val('');
	$('input[name="mesmensual"]').val('');
	$("#ordinaldiasemana").prop('selectedIndex', -1);
	$("#diasemana").prop('selectedIndex', -1);
	$('input[name="cadameses"]').val('');
	$('input[name="dia"]').val('');
	$("#nummeses").prop('selectedIndex', -1);
	
	$('input[name="numdias"]').attr('disabled', 'disabled');
	$('#diasemanal').prop('disabled', 'disabled');
	$('input[name="numsemanas"]').attr('disabled', 'disabled');
	$('input[name="diamensual"]').attr('disabled', 'disabled');
	$('input[name="mesmensual"]').attr('disabled', 'disabled');
	$('#ordinaldiasemana').prop('disabled', 'disabled');
	$('#diasemana').prop('disabled', 'disabled');
	$('input[name="cadameses"]').attr('disabled', 'disabled');
	$('input[name="dia"]').attr('disabled', 'disabled');
	$('#nummeses').prop('disabled', 'disabled');
	$("textarea#descripciontipoperiodo").val(mapDescripciones.get("6"));
}	
function habilitarCamposDiaria()
{
	$('input[name="numdias"]').removeAttr('disabled');
	$('#diasemanal').prop('disabled', 'disabled');
	$('input[name="numsemanas"]').attr('disabled', 'disabled');
	$('input[name="diamensual"]').attr('disabled', 'disabled');
	$('input[name="mesmensual"]').attr('disabled', 'disabled');
	$('#ordinaldiasemana').prop('disabled', 'disabled');
	$('#diasemana').prop('disabled', 'disabled');
	$('input[name="cadameses"]').attr('disabled', 'disabled');
	$('input[name="dia"]').attr('disabled', 'disabled');
	$('#nummeses').prop('disabled', 'disabled');
	$("textarea#descripciontipoperiodo").val(mapDescripciones.get("1"));
	
	$("#diasemanal").prop('selectedIndex', -1);
	$('input[name="numsemanas"]').val('');
	$('input[name="diamensual"]').val('');
	$('input[name="mesmensual"]').val('');
	$("#ordinaldiasemana").prop('selectedIndex', -1);
	$("#diasemana").prop('selectedIndex', -1);
	$('input[name="cadameses"]').val('');
	$('input[name="dia"]').val('');
	$("#nummeses").prop('selectedIndex', -1);		
}

function habilitarCamposSemanal()
{
	
	$('input[name="numdias"]').attr('disabled', 'disabled');
	$('#diasemanal').removeAttr('disabled');
	$('input[name="numsemanas"]').removeAttr('disabled');			
	$('input[name="diamensual"]').attr('disabled', 'disabled');
	$('input[name="mesmensual"]').attr('disabled', 'disabled');
	$('#ordinaldiasemana').prop('disabled', 'disabled');
	$('#diasemana').prop('disabled', 'disabled');
	$('input[name="cadameses"]').attr('disabled', 'disabled');
	$('input[name="dia"]').attr('disabled', 'disabled');
	$('#nummeses').prop('disabled', 'disabled');
	$("textarea#descripciontipoperiodo").val(mapDescripciones.get("2"));
	
	$('#diasemanal option[value="<x:out select = "$editar_periodo_xml/Periodofacturacion/diasemana" />"]').attr("selected", "selected");
	
	$('input[name="numdias"]').val('');
 	$('input[name="diamensual"]').val('');
	$('input[name="mesmensual"]').val('');
	$("#ordinaldiasemana").prop('selectedIndex', -1);
	$("#diasemana").prop('selectedIndex', -1);
	$('input[name="cadameses"]').val('');
	$('input[name="dia"]').val('');
	$("#nummeses").prop('selectedIndex', -1);		
	
	}
	
	function habilitarCamposMensual()
	{
		
		
		$('input[name="numdias"]').attr('disabled', 'disabled');
		$('#diasemanal').prop('disabled', 'disabled');
		$('input[name="numsemanas"]').attr('disabled', 'disabled');
		$('input[name="diamensual"]').removeAttr('disabled');
		$('input[name="mesmensual"]').removeAttr('disabled');
		$('#ordinaldiasemana').prop('disabled', 'disabled');
		$('#diasemana').prop('disabled', 'disabled');
		$('input[name="cadameses"]').attr('disabled', 'disabled');
		$('input[name="dia"]').attr('disabled', 'disabled');
		$('#nummeses').prop('disabled', 'disabled');
		$("textarea#descripciontipoperiodo").val(mapDescripciones.get("3"));
		
		$('input[name="numdias"]').val('');
	 	$("#diasemanal").prop('selectedIndex', -1);
		$('input[name="numsemanas"]').val('');	
		$("#ordinaldiasemana").prop('selectedIndex', -1);
		$("#diasemana").prop('selectedIndex', -1);
		$('input[name="cadameses"]').val('');
		$('input[name="dia"]').val('');
		$("#nummeses").prop('selectedIndex', -1);		
	}
	
	function habilitarCamposMensualXDia()
	{
		
		$('input[name="numdias"]').attr('disabled', 'disabled');
		$('#diasemanal').prop('disabled', 'disabled');
		$('input[name="numsemanas"]').attr('disabled', 'disabled');										
		$('input[name="diamensual"]').attr('disabled', 'disabled');
		$('input[name="mesmensual"]').attr('disabled', 'disabled');
		$('#ordinaldiasemana').removeAttr('disabled');
		$('#diasemana').removeAttr('disabled');
		$('input[name="cadameses"]').removeAttr('disabled');	
		$('input[name="dia"]').attr('disabled', 'disabled');
		$('#nummeses').prop('disabled', 'disabled');
		$("textarea#descripciontipoperiodo").val(mapDescripciones.get("4"));
		
		$('#ordinaldiasemana option[value="<x:out select = "$editar_periodo_xml/Periodofacturacion/ordinaldiasemana" />"]').attr("selected", "selected");
		$('#diasemana option[value="<x:out select = "$editar_periodo_xml/Periodofacturacion/diasemana" />"]').attr("selected", "selected");
		
		
		$('input[name="numdias"]').val('');
	 	$("#diasemanal").prop('selectedIndex', -1);
		$('input[name="numsemanas"]').val('');
		$('input[name="diamensual"]').val('');
		$('input[name="mesmensual"]').val('');
		$('input[name="dia"]').val('');
		$("#nummeses").prop('selectedIndex', -1);		
		
	}
	
	function habilitarCamposAnual()
	{
		
		 $('input[name="numdias"]').attr('disabled', 'disabled');
			$('#diasemanal').prop('disabled', 'disabled');
			$('input[name="numsemanas"]').attr('disabled', 'disabled');
			$('input[name="diamensual"]').attr('disabled', 'disabled');
			$('input[name="mesmensual"]').attr('disabled', 'disabled');
			$('#ordinaldiasemana').prop('disabled', 'disabled');
			$('#diasemana').prop('disabled', 'disabled');
			$('input[name="cadameses"]').attr('disabled', 'disabled');
			$('input[name="dia"]').removeAttr('disabled');	
			$('#nummeses').removeAttr('disabled');	
			$("textarea#descripciontipoperiodo").val(mapDescripciones.get("5"));
			
			$('#nummeses option[value="<x:out select = "$editar_periodo_xml/Periodofacturacion/nummeses" />"]').attr("selected", "selected");
			
			$('input[name="numdias"]').val('');
		 	$("#diasemanal").prop('selectedIndex', -1);
			$('input[name="numsemanas"]').val('');
			$('input[name="diamensual"]').val('');
			$('input[name="mesmensual"]').val('');
			$("#ordinaldiasemana").prop('selectedIndex', -1);
			$("#diasemana").prop('selectedIndex', -1);
			$('input[name="cadameses"]').val('');			
	}
//********************************************
$(".save_periodo_dialog").on("click", function(e) {
	$("#form_periodo_data").submit();	
})
//*********************************************		
$("#form_periodo_data").validate({
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		guardarPeriodo()
	}
})
//********************************************
	function guardarPeriodo()
	{
		var data = $("#form_periodo_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/facturacion/facturas/periodos/save_periodo.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");
				
				$("#modal-dialog-form").modal('hide');
				dt_listperiodos.ajax.reload(null,false);
								
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});

	}
	
	
	
	//**************************

	
});

</script>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2><spring:message code="common.text.filter_list" /></h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">

		<form id="form_filter_list_gestionfacturas" class="form-horizontal form-label-left">
			<input type="hidden" name="id_grupo_impresion" id="id_grupo_impresion"/>
			<div class="col-md-5 col-sm-5 col-xs-12">
				 <div class="form-group button-dialog">
	           		<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.gestionfacturas.field.codigocliente" />:</label>	           		
		           		
		           			<div class="col-md-4 col-sm-4 col-xs-12">
	                			<input name="codcliente1" id="codcliente1" type="text" class="form-control" >
	                		</div>
	                		<div class="col-md-1 col-sm-1 col-xs-12">
		                		<label class="control-label"><spring:message code="facturacion.facturas.tabs.gestionfacturas.field.a" /></label>
		                	</div>
		                	<div class="col-md-4 col-sm-4 col-xs-12">	
								<input name="codcliente2" id="codcliente2" type="text" class="form-control" >
							</div>
		                	            
	           </div>
            	            		            	            	
			
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.gestionfacturas.field.seriefactura" />:</label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="seriefactura" id="seriefactura" type="text" class="form-control">
					</div>
				</div>
				
					<div class="form-group" id="refventa">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.gestionfacturas.field.numfactura" /></label>					
					<div class="col-md-4 col-sm-4 col-xs-4">
	                	<input type="text" name="numfactura1" id="numfactura1" class="form-control" value=""/>
					</div>
					<label class="control-label col-md-1 col-sm-1 col-xs-1"><spring:message code="facturacion.facturas.tabs.gestionfacturas.field.a" /></label>
					<div class="col-md-4 col-sm-4 col-xs-4">
	                	<input type="text" name="numfactura2" id="numfactura2" class="form-control" value=""/>
					</div>
				</div>			
				
				
				
				<div class="form-group" id="refventa">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.gestionfacturas.field.refventa" />:</label>					
					<div class="col-md-4 col-sm-4 col-xs-4">
	                	<input type="text" name="refventainicial" id="refventainicial" class="form-control" value=""/>
					</div>
					<label class="control-label col-md-1 col-sm-1 col-xs-1"><spring:message code="facturacion.facturas.tabs.gestionfacturas.field.a" /></label>
					<div class="col-md-4 col-sm-4 col-xs-4">
	                	<input type="text" name="refventafinal" id="refventafinal" class="form-control" value=""/>
					</div>
				</div>			
				
				<div class="form-group" id="refreserva">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.gestionfacturas.field.refreserva" />:</label>					
					<div class="col-md-9 col-sm-9 col-xs-9">
	                	<input type="text" name="refreserva" id="refreserva" class="form-control" value=""/>
					</div>					
				</div>
								
			</div>
						

			<div class="col-md-7 col-sm-7 col-xs-12">
				<div class="form-group date-picker" id="fechafact">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.gestionfacturas.field.fechafacturacion" />:</label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						  <a type="button" class="btn btn-default btn-clear-date" id="button_fechafacturacion_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.gestionfacturas.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
						  </a>			
                        <div class="input-prepend input-group">
                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                          <input type="text" name="facturacion" id="facturacion" class="form-control" value="" readonly/>
                          <input type="hidden" required="required" name="fechainiciofacturacion" value=""/>
                          <input type="hidden" required="required" name="fechafinfacturacion" value=""/>
                        </div>
					</div>
				</div>
				<div class="form-group date-picker" id="fechavent">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.gestionfacturas.field.fechaventa" />:</label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						  <a type="button" class="btn btn-default btn-clear-date" id="button_fechaventa_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.gestionfacturas.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
						  </a>			
                        <div class="input-prepend input-group">
                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                          <input type="text" name="venta" id="venta" class="form-control" value="" readonly/>
                          <input type="hidden" required="required" name="fechainicioventa" value=""/>
                          <input type="hidden" required="required" name="fechafinventa" value=""/>
                        </div>
					</div>
				</div>
				<div class="form-group date-picker" id="fechareser">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.gestionfacturas.field.fechareserva" />:</label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						  <a type="button" class="btn btn-default btn-clear-date" id="button_fechareserva_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.gestionfacturas.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
						  </a>			
                        <div class="input-prepend input-group">
                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                          <input type="text" name="reserva" id="reserva" class="form-control" value="" readonly/>
                          <input type="hidden" required="required" name="fechainicioreserva" value=""/>
                          <input type="hidden" required="required" name="fechafinreserva" value=""/>
                        </div>
					</div>
				</div>
				<div class="form-group date-picker" id="fechaenv">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.gestionfacturas.field.fechaenvio" />:</label>
					<div class="col-md-8 col-sm-8 col-xs-8">
						  <a type="button" class="btn btn-default btn-clear-date" id="button_fechaenvio_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.gestionfacturas.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
						  </a>			
                        <div class="input-prepend input-group">
                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                          <input type="text" name="envio" id="envio" class="form-control" value="" readonly/>
                          <input type="hidden" required="required" name="fechainicioenvio" value=""/>
                          <input type="hidden" required="required" name="fechafinenvio" value=""/>
                        </div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.gestionfacturas.field.rectificativas" />:</label>					
					<div class="col-md-8 col-sm-8 col-xs-8">
	                	<input type="text" name="rectificativas" id="rectificativas" class="form-control" value=""/>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.gestionfacturas.field.localizadorag" />:</label>					
					<div class="col-md-8 col-sm-8 col-xs-8">
	                	<input type="text" name="localizador" id="localizador" class="form-control" value=""/>
					</div>					
				</div>
				
			</div>

			<div class="col-md-9 col-sm-9 col-xs-12">
			
				<fieldset id="tipo-facturas">				
					<div class="form-group">
						<div class="checkbox col-md-4 col-sm-4 col-xs-4">
							<input type="radio" name="tipo-facturas" id="tiponormales" checked value="1" data-parsley-multiple="facturas" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.dialog.gestionfacturas.field.tiponormales" /></strong>
						</div>
						<div class="checkbox col-md-4 col-sm-4 col-xs-4">
							<input type="radio" name="tipo-facturas" id="tiporappels" value="2" data-parsley-multiple="facturas" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.dialog.gestionfacturas.field.rappels" /></strong>
						</div>						
					</div>				
				</fieldset>		
				
				<fieldset id="tipoEmitidas">				
					<div class="form-group">
						<div class="checkbox col-md-4 col-sm-4 col-xs-4">
							<input type="radio" name="tipo-emitidas" id="todasemitidas" checked value="1" data-parsley-multiple="tipo-emitidas" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.dialog.gestionfacturas.field.todas" /></strong>
						</div>
						<div class="checkbox col-md-4 col-sm-4 col-xs-4">
							<input type="radio" name="tipo-emitidas" id="emitidas" value="2" data-parsley-multiple="tipo-emitidas" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.dialog.gestionfacturas.field.emitidas" /></strong>
						</div>
						<div class="checkbox col-md-4 col-sm-4 col-xs-4">
							<input type="radio" name="tipo-emitidas" id="sinemitir" value="3" data-parsley-multiple="tipo-emitidas" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.dialog.gestionfacturas.field.sinemitir" /></strong>
						</div>							
					</div>				
				</fieldset>
				
				<fieldset id="tipoValidadas">				
					<div class="form-group">
						<div class="checkbox col-md-4 col-sm-4 col-xs-4">
							<input type="radio" name="tipo-validadas" id="todasvalidadas" checked value="1" data-parsley-multiple="tipo-validadas" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.dialog.gestionfacturas.field.todas" /></strong>
						</div>
						<div class="checkbox col-md-4 col-sm-4 col-xs-4">
							<input type="radio" name="tipo-validadas" id="validadas" value="2" data-parsley-multiple="tipo-validadas" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.dialog.gestionfacturas.field.validadas" /></strong>
						</div>
						<div class="checkbox col-md-4 col-sm-4 col-xs-4">
							<input type="radio" name="tipo-validadas" id="sinvalidar" value="3" data-parsley-multiple="tipo-validadas" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.dialog.gestionfacturas.field.sinvalidar" /></strong>
						</div>							
					</div>				
				</fieldset>			
					
				<fieldset id="tipoEnviadas">				
					<div class="form-group">
						<div class="checkbox col-md-4 col-sm-4 col-xs-4">
							<input type="radio" name="tipo-enviadas" id="todasenviadas" checked value="1" data-parsley-multiple="tipo-enviadas" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.dialog.gestionfacturas.field.todas" /></strong>
						</div>
						<div class="checkbox col-md-4 col-sm-4 col-xs-4">
							<input type="radio" name="tipo-enviadas" id="enviadas" value="2" data-parsley-multiple="tipo-enviadas" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.dialog.gestionfacturas.field.enviadas" /></strong>
						</div>
						<div class="checkbox col-md-4 col-sm-4 col-xs-4">
							<input type="radio" name="tipo-enviadas" id="sinenviar" value="3" data-parsley-multiple="tipo-enviadas" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="facturacion.facturas.dialog.gestionfacturas.field.sinenviar" /></strong>
						</div>							
					</div>				
				</fieldset>				

			</div>

			<div class="clearfix"></div>
			<div class="ln_solid"></div>
			<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
				<button id="button_clean_gest_facturas" type="button" class="btn btn-success pull-right">
						<spring:message code="venta.ventareserva.tabs.canje-bono.button.clean" />
				</button>
				<button id="button_filter_list_gestionfacturas" type="button" class="btn btn-success pull-right">
					<spring:message code="common.button.filter" />
				</button>
			</div>
			<input type="hidden" name="start" value=""/>
			<input type="hidden" name="length" value=""/>
		</form>
	</div>
</div>



<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_gestion_factura_rectificar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.gestionfacturas.list.button.rectificar" />"> <span class="fa fa-edit"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_gestion_factura_ver">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.gestionfacturas.list.button.ver" />"> <span class="fa fa-eye"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_gestion_factura_modificar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.gestionfacturas.list.button.modificar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_gestion_factura_validar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.gestionfacturas.list.button.validar" />"> <span class="fa fa-check-square"></span>
			</span>
		</a>		
		<a type="button" class="btn btn-info" id="tab_gestion_factura_imprimir">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.gestionfacturas.list.button.imprimir" />"> <span class="fa fa-print"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_gestion_factura_imprimir_masivo">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.gestionfacturas.list.button.imprimirMasivo" />"> <span class="fa fa-server"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_facturas_enviar_factura">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.gestionfacturas.list.button.enviar" />"> <span class="fa fa-envelope"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_gestion_factura_anular">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.gestionfacturas.list.button.anular" />"> <span class="fa fa-trash"></span>
			</span>
		</a>		
	</div>

	<table id="datatable-list-gestionfacturas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th></th>
				<th></th>
				<th><spring:message code="facturacion.facturas.tabs.gestionfacturas.list.header.numfactura" /></th>
				<th><spring:message code="facturacion.facturas.tabs.gestionfacturas.list.header.codcliente" /></th>
				<th><spring:message code="facturacion.facturas.tabs.gestionfacturas.list.header.cliente" /></th>
				<th><spring:message code="facturacion.facturas.tabs.gestionfacturas.list.header.fechageneracion" /></th>
				<th><spring:message code="facturacion.facturas.tabs.gestionfacturas.list.header.importetotal" /></th>
				<th><spring:message code="facturacion.facturas.tabs.gestionfacturas.list.header.impresiones" /></th>
				<th><spring:message code="facturacion.facturas.tabs.gestionfacturas.list.header.rectificativas" /></th>
				<th><spring:message code="facturacion.facturas.tabs.gestionfacturas.list.header.val" /></th>
				<th><spring:message code="facturacion.facturas.tabs.gestionfacturas.list.header.env" /></th>
				<th><spring:message code="facturacion.facturas.tabs.gestionfacturas.list.header.anul" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script>






var  dt_listgestionfacturas;

$(document).ready(function() {
	
		
    dt_listgestionfacturas=$('#datatable-list-gestionfacturas').DataTable( {
	serverSide: true,
	searching: false,
	ordering: false,
	pagingType: "simple",
	info: false,
	deferLoading: 0,
    ajax: {
        url: "<c:url value='/ajax/facturacion/facturas/gestionfacturas/list_gestionfacturas.do'/>",
        rowId: 'Factura.idfactura',
        type: 'POST',
        dataSrc: function (json) {
        
        	if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Factura)); return(""); },
        data: function (params) { 
        	$('#form_filter_list_gestionfacturas input[name="start"]').val(params.start);
        	$('#form_filter_list_gestionfacturas input[name="length"]').val(params.length);
        	return($("#form_filter_list_gestionfacturas").serializeObject());
    	
        },
        
        error: function (xhr, error, thrown) {
            if (xhr.responseText=="403") {
                  $("#gestionfacturas-tab").hide();
            }                   
            else
            	{
            	new PNotify({
 					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
 					text : xhr.responseText,
 					type : "error",
 					delay : 5000,
 					buttons : {
 						closer : true,
 						sticker : false
 					}					
 				});
            	$("#datatable-list-gestionfacturas_processing").hide();
            	}
     },
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listgestionfacturas.data().count()>0) dt_listgestionfacturas.columns.adjust().draw(); });
	},
    columns: [
		{ data: null, type: "spanish-string", defaultContent: "", render: function ( data, type, row, meta ) { return data; }}, 
		{ data: "ventafacturas.Ventafactura.venta.tipoventa.idtipoventa", type: "spanish-string", defaultContent: ""}, 
		{ data: "serieynumero", type: "spanish-string", defaultContent: ""}, 
		{ data: "cliente.idcliente", type: "spanish-string", defaultContent: ""}, 
		{ data: "cliente.nombrecompleto", type: "spanish-string", defaultContent: "" },		 
		{ data: "fechageneracion", type: "spanish-string" ,  defaultContent:"",
			 render: function ( data, type, row, meta ) {			 
	    	  	  return data.substr(0, 10);}	 
		 } ,
		{ data: "importetotal", type: "spanish-string", defaultContent: ""},                
		{ data: "numimpresiones", type: "spanish-string", defaultContent: ""},			
		{ data: "rectificativas.Factura", className: "cell_centered",
       	  render: function ( data, type, row, meta ) {       		
       		  return showListInCell(data,"<spring:message code="facturacion.facturas.tabs.gestionfacturas.list.text.rectificativas" />","serieynumero"); }	
        },  
        { data: null, type: "spanish-string", defaultContent: "",
            render: function ( data, type, row, meta ) {
            	if (data.validada!='') return '<span class="list_data" data-toggle="tooltip-on-click" title="<span>'+data.fechavalidacion.substr(0, 10)+'</span><span>'+data.usuariovalidacion.login+'</span>"><spring:message code="facturacion.facturas.tabs.gestionfacturas.list.text.si" /></span>'; else  return '<spring:message code="facturacion.facturas.tabs.gestionfacturas.list.text.no" />';	
         }},  
         { data: null, type: "spanish-string", defaultContent: "",
             render: function ( data, type, row, meta ) {
             	if (data.enviada!='') return '<span class="list_data" data-toggle="tooltip-on-click" title="<span>'+data.fechaenvio.substr(0, 10)+'</span><span>'+data.usuarioenvio.login+'</span>"><spring:message code="facturacion.facturas.tabs.gestionfacturas.list.text.si" /></span>'; else  return '<spring:message code="facturacion.facturas.tabs.gestionfacturas.list.text.no" />';	
          }},
          { data: "anulada", type: "spanish-string", defaultContent: "",
              render: function ( data, type, row, meta ) {
              	if (data!='') return '<spring:message code="facturacion.facturas.tabs.gestionfacturas.list.text.si" />'; else  return '<spring:message code="facturacion.facturas.tabs.gestionfacturas.list.text.no" />';	
           }},
		
    ],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-gestionfacturas') },
    select: { style: 'os',selector:'td:not(:nth-child(7)):not(:nth-child(8)):not(:nth-child(9))'},
	language: dataTableLanguage,
	processing: true,         
    "columnDefs": [
                   { "visible": false, "targets": [0,1]}
                 ]
   });

insertSmallSpinner("#datatable-list-gestionfacturas_processing");
//*******************************************************************************
$("#button_filter_list_gestionfacturas").on("click", function(e) {	
		dt_listgestionfacturas.ajax.reload();
})
//*******************************************************************************
$("#principal-switch").each(function(index) {
	var s = new Switchery(this, {
		size : 'small'
	});
});
//********************************************************************************


$today= moment().format("DD/MM/YYYY");
var dia_inicio = $today;
var dia_fin = $today;

//Fecha facturaci�n:
$('input[name="fechainiciofacturacion"]').val(dia_inicio);
$('input[name="fechafinfacturacion"]').val(dia_fin);
$('input[name="facturacion"]').val( dia_inicio + ' - ' + dia_fin);


$('input[name="facturacion"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
  	  	 $('input[name="facturacion"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('input[name="fechainiciofacturacion"]').val(start.format('DD/MM/YYYY'));
  	  	 $('input[name="fechafinfacturacion"]').val(end.format('DD/MM/YYYY'));
	 });

$("#button_fechafacturacion_clear").on("click", function(e) {
    $('input[name="facturacion"]').val('');
    $('input[name="fechainiciofacturacion"]').val('');
    $('input[name="fechafinfacturacion"]').val('');
});


//Fecha venta
$('input[name="fechainicioventa"]').val(dia_inicio);
$('input[name="fechafinventa"]').val(dia_fin);
$('input[name="venta"]').val( dia_inicio + ' - ' + dia_fin);


$('input[name="venta"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
  	  	 $('input[name="venta"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('input[name="fechainicioventa"]').val(start.format('DD/MM/YYYY'));
  	  	 $('input[name="fechafinventa"]').val(end.format('DD/MM/YYYY'));
	 });

$("#button_fechaventa_clear").on("click", function(e) {
    $('input[name="venta"]').val('');
    $('input[name="fechainicioventa"]').val('');
    $('input[name="fechafinventa"]').val('');
});

//Fecha reserva
$('input[name="fechainicioreserva"]').val(dia_inicio);
$('input[name="fechafinreserva"]').val(dia_fin);
$('input[name="reserva"]').val( dia_inicio + ' - ' + dia_fin);


$('input[name="reserva"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
  	  	 $('input[name="reserva"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('input[name="fechainicioreserva"]').val(start.format('DD/MM/YYYY'));
  	  	 $('input[name="fechafinreserva"]').val(end.format('DD/MM/YYYY'));
	 });

$("#button_fechareserva_clear").on("click", function(e) {
    $('input[name="reserva"]').val('');
    $('input[name="fechainicioreserva"]').val('');
    $('input[name="fechafinreserva"]').val('');
});

//Fecha envio
$('input[name="fechainicioenvio"]').val(dia_inicio);
$('input[name="fechafinenvio"]').val(dia_fin);
$('input[name="envio"]').val( dia_inicio + ' - ' + dia_fin);


$('input[name="envio"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
  	  	 $('input[name="envio"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('input[name="fechainicioenvio"]').val(start.format('DD/MM/YYYY'));
  	  	 $('input[name="fechafinenvio"]').val(end.format('DD/MM/YYYY'));
	 });

$("#button_fechaenvio_clear").on("click", function(e) {
    $('input[name="envio"]').val('');
    $('input[name="fechainicioenvio"]').val('');
    $('input[name="fechafinenvio"]').val('');
});

//*****************************************************************************************
$("#tiponormales").on("ifChanged", function(e) {	
	
	if($("input[name='tipo-facturas']:checked").val() == 1)
	{
		$("#refventa").show();
		$("#refreserva").show();
		$("#fechafact").show();
		$("#fechavent").show();
		$("#fechareser").show();
		$("#fechaenv").show();
	}else
	{		
   		$("#refventa").hide();
   		$("#refreserva").hide();
   		$("#fechafact").hide();
		$("#fechavent").hide();
		$("#fechareser").hide();
		$("#fechaenv").hide();
	} 
	
});


$("#codcliente1").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>", "", "#codcliente1");
$("#codcliente2").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>", "", "#codcliente2");


//*************************************************************************
vaciarFechas();	
//*****************************************************************************************
function vaciarFechas()
{
	
	 $('input[name="facturacion"]').val('');
	 $('input[name="fechainiciofacturacion"]').val('');
	 $('input[name="fechafinfacturacion"]').val('');
	 $('input[name="venta"]').val('');
	 $('input[name="fechainicioventa"]').val('');
	 $('input[name="fechafinventa"]').val('');
	 $('input[name="reserva"]').val('');
	 $('input[name="fechainicioreserva"]').val('');
	 $('input[name="fechafinreserva"]').val('');	
	 $('input[name="envio"]').val('');
	 $('input[name="fechainicioenvio"]').val('');
	 $('input[name="fechafinenvio"]').val(''); 
	}
//*****************************************************************************************
});



/***********************************************BOT�N VER*************************************/
$("#tab_gestion_factura_ver").on("click", function(e) { 
var data = sanitizeArray(dt_listgestionfacturas.rows( { selected: true } ).data(),"idfactura");

if (data.length>1) {
	new PNotify({
	      title: '<spring:message code="common.dialog.text.error" />',
	      text: '<spring:message code="facturacion.facturas.tabs.factura.list.ver.alert.seleccion_multiple" />',
		  type: "error",
		  delay: 5000,
		  buttons: { sticker: false }
	   });
	return;
}

if (data.length<=0) {
	new PNotify({
	      title: '<spring:message code="common.dialog.text.error" />',
	      text: '<spring:message code="facturacion.facturas.tabs.factura.list.ver.alert.ninguna_seleccion" />',
		  type: "error",
		  delay: 5000,
		  buttons: { sticker: false }
	   });
	return;
}	


idfactura= dt_listgestionfacturas.rows( { selected: true } ).data()[0].idfactura[0];
var ventaFacturas = dt_listgestionfacturas.rows( { selected: true } ).data()[0].ventafacturas.Ventafactura;
var ventaFactura=undefined;

if(""+ventaFacturas !="undefined")
	{
	if(ventaFacturas.length>0)
		ventaFactura = ventaFacturas[0];
	else
		ventaFactura = ventaFacturas;	
	}

   
if(ventaFactura!=undefined)
    {
	
	tipo = ventaFactura.venta.tipoventa.idtipoventa;

	if (tipo==4){	
		window.open("../../jasper.post?idFactura="+idfactura+"&format=pdf&viewOnly=1&report=FacturaNormalYRectificativa","_blank");
		}
	else if (tipo==2){
		//Si el tipo de venta es Venta de bonos prepago la llamada es:
		window.open("../../jasper.post?idFactura="+idfactura+"&format=pdf&viewOnly=1&report=FacturaBonoPrepagoYRectificativa","_blank");
		}
    }
 else
	 {
	 window.open("../../jasper.post?idFactura="+idfactura+"&format=pdf&viewOnly=1&report=FacturaBonoCreditoYRectificativa","_blank");
	 }
})



/***********************************************BOT�N Imprimir*************************************/
$("#tab_gestion_factura_imprimir").on("click", function(e) { 
var data = sanitizeArray(dt_listgestionfacturas.rows( { selected: true } ).data(),"idfactura");
	
if (data.length>1) {
	new PNotify({
	      title: '<spring:message code="common.dialog.text.error" />',
	      text: '<spring:message code="facturacion.facturas.tabs.factura.list.imprimir.alert.seleccion_multiple" />',
		  type: "error",
		  delay: 5000,
		  buttons: { sticker: false }
	   });
	return;
}

if (data.length<=0) {
	new PNotify({
	      title: '<spring:message code="common.dialog.text.error" />',
	      text: '<spring:message code="facturacion.facturas.tabs.factura.list.imprimir.alert.ninguna_seleccion" />',
		  type: "error",
		  delay: 5000,
		  buttons: { sticker: false }
	   });
	return;
}	

idfactura = data[0][0];
var ventaFacturas = dt_listgestionfacturas.rows( { selected: true } ).data()[0].ventafacturas.Ventafactura;
var ventaFactura=undefined;

if(""+ventaFacturas !="undefined")
	{
	if(ventaFacturas.length>0)
		ventaFactura = ventaFacturas[0];
	else
		ventaFactura = ventaFacturas;	
	}

   
if(ventaFactura!=undefined)
    {
	
	tipo = ventaFactura.venta.tipoventa.idtipoventa;

	if (tipo==4){	
		window.open("../../jasper.post?idFactura="+idfactura+"&format=pdf&viewOnly=0&report=FacturaNormalYRectificativa","_blank");
		}
	else if (tipo==2){
		//Si el tipo de venta es Venta de bonos prepago la llamada es:
		window.open("../../jasper.post?idFactura="+idfactura+"&format=pdf&viewOnly=0&report=FacturaBonoPrepagoYRectificativa","_blank");
		}
    }
 else
	 {
	 window.open("../../jasper.post?idFactura="+idfactura+"&format=pdf&viewOnly=0&report=FacturaBonoCreditoYRectificativa","_blank");
	 }



})







/***********************************************BOT�N rectificar*************************************/
$("#tab_gestion_factura_rectificar").on("click", function(e) { 
var data = sanitizeArray(dt_listgestionfacturas.rows( { selected: true } ).data(),"idfactura");

if (data.length>1) {
	new PNotify({
	      title: '<spring:message code="common.dialog.text.error" />',
	      text: '<spring:message code="facturacion.facturas.tabs.factura.list.rectificar.alert.seleccion_multiple" />',
		  type: "error",
		  delay: 5000,
		  buttons: { sticker: false }
	   });
	return;
}

if (data.length<=0) {
	new PNotify({
	      title: '<spring:message code="common.dialog.text.error" />',
	      text: '<spring:message code="facturacion.facturas.tabs.factura.list.rectificar.alert.ninguna_seleccion" />',
		  type: "error",
		  delay: 5000,
		  buttons: { sticker: false }
	   });
	return;
}

var anulada=""+dt_listgestionfacturas.rows( { selected: true } ).data()[0].anulada;

if (anulada!="0") {
	new PNotify({
	      title: '<spring:message code="common.dialog.text.error" />',
	      text: '<spring:message code="facturacion.facturas.tabs.factura.list.rectificar.alert.anulada" />',
		  type: "error",
		  delay: 5000,
		  buttons: { sticker: false }
	   });
	return;
}	

var esrectificativa = dt_listgestionfacturas.rows( { selected: true } ).data()[0].esrectificativa;

if(esrectificativa==1)
	new PNotify({
	      title: '<spring:message code="common.dialog.text.error" />',
	      text: '<spring:message code="facturacion.facturas.tabs.factura.list.rectificar.alert.rectificativa" />',
		  type: "error",
		  delay: 5000,
		  buttons: { sticker: false }
	   });
else
{
		showButtonSpinner("#tab_gestion_factura_rectificar");
		$("#modal-dialog-form .modal-content:first").load("<c:url value='/ajax/facturacion/facturas/facturas/rectificar.do'/>?idFactura="+data[0][0], function() {
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "lg");
		});}
})

$("#tab_facturas_enviar_factura").on("click", function(e) { 
	
	$("#modal-dialog-envio-entradas").modal('show');
	setModalDialogSize("#modal-dialog-envio-entradas", "xs");
})

/***********************************************BOT�N EDITAR*************************************/
$("#tab_gestion_factura_validar").on("click", function(e) { 
var data = sanitizeArray(dt_listgestionfacturas.rows( { selected: true } ).data(),"idfactura");
	


if (data.length<=0) {
	new PNotify({
	      title: '<spring:message code="common.dialog.text.error" />',
	      text: '<spring:message code="facturacion.facturas.tabs.factura.list.validar.alert.ninguna_seleccion" />',
		  type: "error",
		  delay: 5000,
		  buttons: { sticker: false }
	   });
	return;
}	

xmlFacturas = "<list>";

if(data.length==1)
	xmlFacturas+="<Factura><idfactura>"+data[0][0]+"</idfactura></Factura>"; 
else
	{
	for(i=0;i<data.length;i++)
		{
		xmlFacturas+="<Factura><idfactura>"+data[i][0]+"</idfactura></Factura>"; 
		}
	}


xmlFacturas+="</list>";









$.ajax({
	contenttype: "application/json; charset=utf-8",
	type : "post",
	url : "<c:url value='/ajax/facturacion/factura/validar_factura.do'/>",
	timeout : 100000,
	data: { xmlFacturas: xmlFacturas }, 
	success : function(data) {
		dt_listgestionfacturas.ajax.reload();					
	},
	error : function(exception) {
		dt_listgestionfacturas.processing(false);
		
		new PNotify({
		      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
		      text: exception.responseText,
			  type: "error",		     
			  delay: 5000,
			  buttons: { sticker: false }
		   });			
	}
});
})



/***********************************************BOT�N EDITAR*************************************/
$("#tab_gestion_factura_modificar").on("click", function(e) { 
var data = sanitizeArray(dt_listgestionfacturas.rows( { selected: true } ).data(),"idfactura");
	
if (data.length>1) {
	new PNotify({
	      title: '<spring:message code="common.dialog.text.error" />',
	      text: '<spring:message code="facturacion.facturas.tabs.factura.list.modificar.alert.seleccion_multiple" />',
		  type: "error",
		  delay: 5000,
		  buttons: { sticker: false }
	   });
	return;
}

if (data.length<=0) {
	new PNotify({
	      title: '<spring:message code="common.dialog.text.error" />',
	      text: '<spring:message code="facturacion.facturas.tabs.factura.list.modificar.alert.ninguna_seleccion" />',
		  type: "error",
		  delay: 5000,
		  buttons: { sticker: false }
	   });
	return;
}	

showButtonSpinner("#tab_gestion_factura_modificar");

$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/facturacion/facturas/factura_modificar.do'/>?idFactura="+data[0][0], function() {
	$("#modal-dialog-form").modal('show');
	setModalDialogSize("#modal-dialog-form", "lg");
});
})

function imprimirMasivo()
{
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/facturacion/agruparFacturasParaImprimir.do'/>",
		timeout : 100000,
		data: {}, 
		success : function(data) {
			idgrupo = data;
			if(idgrupo!=0)
				{
				$("#id_grupo_impresion").val(idgrupo);
				$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/facturacion/show_mensaje_impresion_masiva.do'/>", function() {
					$("#modal-dialog-form").modal('show');
					setModalDialogSize("#modal-dialog-form", "xs");
				});
				window.open("http://${url_impresion}/jasperserver/flow.html?_flowId=viewReportFlow&reportUnit=/Facturacion/FacturasCAC/LISTA_FACTURAS_SIN_EMITIR&groupflag="+idgrupo,"impresion");	
				}
			else
				{
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: '<spring:message code="facturacion.facturas.tabs.gestionfacturas.impresionmasiva.noImprimir" />',
					  type: "error",		     
					  delay: 5000,
					  buttons: { sticker: false }
				   });	
				}
			
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
}

/***********************************************BOT�N Anular*************************************/
$("#tab_gestion_factura_imprimir_masivo").on("click", function(e) {

	// Esto hace que se valide el usuario en el servidor de impresi�n 
	// para que no pida credenciales cuando se imprima
	
	window.open("${url_validacion_impresion}","impresion");
	
	window.setTimeout(imprimirMasivo, 1000);	
	
})

/***********************************************BOT�N Anular*************************************/
$("#tab_gestion_factura_anular").on("click", function(e) { 
var data = sanitizeArray(dt_listgestionfacturas.rows( { selected: true } ).data(),"idfactura");
	
if (data.length<=0) {
	new PNotify({
	      title: '<spring:message code="common.dialog.text.error" />',
	      text: '<spring:message code="facturacion.facturas.tabs.factura.list.anular.alert.ninguna_seleccion" />',
		  type: "error",
		  delay: 5000,
		  buttons: { sticker: false }
	   });
	return;
}	

showButtonSpinner("#tab_gestion_factura_anular");
$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/facturacion/facturas/factura_pantalla_anular.do'/>", function() {
	$("#modal-dialog-form").modal('show');
	setModalDialogSize("#modal-dialog-form", "sm");
});
})
	

/***********************************************BOT�N EDITAR VENTA/RESERVA  *********************************/
$("#button_clean_gest_facturas").on("click", function(e) {
	$('input[name="codcliente1"]').val('');
	$('input[name="codcliente2"]').val('');
	$('input[name="seriefactura"]').val('');
	$('input[name="numfactura1"]').val('');
	$('input[name="numfactura2"]').val('');
	$('input[name="refventainicial"]').val('');
	$('input[name="refventafinal"]').val('');
	$('input[name="refreserva"]').val('');
	$('input[name="facturacion"]').val('');
	$('input[name="venta"]').val('');
	$('input[name="reserva"]').val('');
	$('input[name="envio"]').val('');
	$('input[name="rectificativas"]').val('');
	$('input[name="localizador"]').val('');
})


</script>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_cliente}" var="editar_cliente_xml" />
<x:parse xml="${editar_cliente_pais}" var="editar_cliente_pais_xml" />
<x:parse xml="${editar_cliente_categorias_actividad}" var="editar_cliente_categorias_actividad_xml" />
<x:parse xml="${editar_cliente_tipos_documento}" var="editar_cliente_tipos_documento_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<x:choose>
			<x:when select="not($editar_cliente_xml/Cliente)">
				<spring:message code="facturacion.facturas.tabs.clientes.dialog.crear_cliente.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="facturacion.facturas.tabs.clientes.dialog.editar_cliente.title" /> <x:out select = "$editar_cliente_xml/Cliente/idcliente" /> 
				<x:if select="$editar_cliente_xml/Cliente/cliente/idcliente!=''">
					sucursal de <x:out select = "$editar_cliente_xml/Cliente/cliente/idcliente" />
				</x:if> 
			</x:otherwise>
		</x:choose>		
	</h4>	
</div>


<!-- Pesta�as ------------------------------------------------------------->
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 maintab">
		<div class="" role="tabpanel" data-example-id="togglable-tabs">
			<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
				<li role="presentation" class="active"><a href="#tab_datos" id="datos-tab" role="tab" data-toggle="tab" aria-expanded="true"><spring:message code="facturacion.facturas.tabs.clientes.datos.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_direccion" role="tab" id="direccion-tab" data-toggle="tab" aria-expanded="false"><spring:message code="facturacion.facturas.tabs.clientes.direccion.origen.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_direccion_fiscal" role="tab" id="fiscal-tab" data-toggle="tab" aria-expanded="false"><spring:message code="facturacion.facturas.tabs.clientes.direccion_fiscal.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_direccion_auxiliar" role="tab" id="auxiliar-tab" data-toggle="tab" aria-expanded="false"><spring:message code="facturacion.facturas.tabs.clientes.direccion_auxiliar.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_condiciones_cliente" role="tab" id="condiciones_cliente-tab" data-toggle="tab" aria-expanded="false"><spring:message code="facturacion.facturas.tabs.clientes.condiciones_clientes.title" /></a></li>
			</ul>
		</div>
	</div>
</div>

  

<div class="modal-body">
	<form id="form_cliente_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

		<!-- Area de trabajo ------------------------------------------------------>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel thin_padding">
		
					<div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade active in" id="tab_datos" aria-labelledby="datos-tab">
							<tiles:insertAttribute name="datos_cliente" />
						</div>	
						<div role="tabpanel" class="tab-pane fade" id="tab_direccion" aria-labelledby="direccion-tab">
							<c:set var = "direccion" scope = "request" value = "origen"/>
							<x:set var = "direccion_xml" select = "$editar_cliente_xml/Cliente/direccionorigen" scope="request"/>
							<tiles:insertAttribute name="datos_direccion" />						
						</div>	
						<div role="tabpanel" class="tab-pane fade" id="tab_direccion_fiscal" aria-labelledby="fiscal-tab">
							<c:set var = "direccion" scope = "request" value = "fiscal"/>
							<x:set var = "direccion_xml" select = "$editar_cliente_xml/Cliente/direccionfiscal" scope="request"/>
							<tiles:insertAttribute name="datos_direccion" />						
						</div>							
						<div role="tabpanel" class="tab-pane fade" id="tab_direccion_auxiliar" aria-labelledby="auxiliar-tab">
							<c:set var = "direccion" scope = "request" value = "auxiliar"/>
							<x:set var = "direccion_xml" select = "$editar_cliente_xml/Cliente/direccionauxiliar" scope="request"/>
							<tiles:insertAttribute name="datos_direccion" />						
						</div>						
						
						<div role="tabpanel" class="tab-pane fade" id="tab_condiciones_cliente" aria-labelledby="condiciones_cliente-tab">
							<tiles:insertAttribute name="condiciones" />
						</div>
					</div>
		
					<div class="clearfix"></div>
				</div>
			</div>
		</div>


	</form>
	
	<div class="modal-footer">

		<button id="save_cliente_button" type="button" class="btn btn-primary save_cliente_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>
<tiles:insertAttribute name="nuevo_cp" />

<script>

hideSpinner("#tab_clientes_new");
hideSpinner("#tab_clientes_edit");
hideSpinner("#tab_cliente_editar");


function generarXmlDireccion(tipoDireccion)
{
  var xml="<direccion"+tipoDireccion+">";
  xml+="<iddireccion>"+$("#idDireccion_"+tipoDireccion).val()+"</iddireccion>";
  xml+="<personacontacto>"+$("#personacontacto_"+tipoDireccion).val()+"</personacontacto>";
  var tipo_via = $("#tipovia_"+tipoDireccion+" option:selected").text();
  xml+="<tipovia>"+tipo_via+"</tipovia>";
  xml+= "<direccion>"+$("#direccion_"+tipoDireccion).val()+"</direccion>";
  var pais = $("#selector_pais_"+tipoDireccion).val();
  if(pais=='')
	  {
	  pais = '472';
	  }
  xml+="<pais><idpais>"+pais+"</idpais>";
  var nombrePais = $("#selector_pais_"+tipoDireccion+" option:selected").text();
  xml+="<nombre>"+nombrePais+"</nombre></pais>";
  if(pais='472')
	  xml+="<provincia><idprovincia>"+$("#idProvincia_"+tipoDireccion).val()+"</idprovincia>";  
  else
	  xml+="<provincia><idprovincia></idprovincia>";
  xml+="<nombreprovincia>"+$("#filter_provincia_"+tipoDireccion).val()+"</nombreprovincia></provincia>";
  
  if(pais='472')
	  xml+="<municipio><idmunicipio>"+$("#idMunicipio_"+tipoDireccion).val()+"</idmunicipio>";  
  else
	  xml+="<municipio><idmunicipio></idmunicipio>";
  
  xml+="<nombre>"+$("#filter_poblacion_"+tipoDireccion).val()+"</nombre></municipio>"; 
  
  var idcp = $("#idCp_"+tipoDireccion).val();
  var cp = $("#filter_cp_"+tipoDireccion).val();
  
  if(pais='472')
	  {
	  idcp = $("#idCp_"+tipoDireccion).val();
	  cp = $("#filter_cp_"+tipoDireccion).val();
	  }
  else
	  {
	  idcp = "";
	  cp = $("#cpExtanjero_"+tipoDireccion).val();
	  }
  
 /*if(idcp==undefined)
	  idcp = "";
  else
	  {
	  if(cp=="")
		  {
		  cp = $("#cpExtanjero_"+tipoDireccion).val();
		  }
	  }*/
  
  xml+="<cp><idcp>"+idcp+"</idcp>";	 
  xml+="<cp>"+cp+"</cp></cp>";  
  
 
  xml+="<telefonofijo>"+$("#telefonofijo_"+tipoDireccion).val()+"</telefonofijo>";
  xml+="<telefonomovil>"+$("#telefonomovil_"+tipoDireccion).val()+"</telefonomovil>";
  xml+="<fax>"+$("#fax_"+tipoDireccion).val()+"</fax>";
  xml+="<correoelectronico>"+$("#correoelectronico_"+tipoDireccion).val()+"</correoelectronico>";  
  xml+="</direccion"+tipoDireccion+">";
  return xml;
}




function filtrarProvincia(nombre,idPais,provincia) {
    
    
    if(nombre!='' && nombre.length>2)
    {
    	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/facturacion/facturas/clientes/select_list_provincias.do?idPais='/>"+idPais+"&nombre="+nombre,
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				provincia.find('option').remove();
				provincia.append('<option selected value="">Seleccione provincia</option>');
				if (data.ArrayList!="") {
                    var item=data.ArrayList.LabelValue;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	provincia.append('<option value="' + val.value + '">' + val.label + '</option>');
                        });
                    else
                    	{
                    	provincia.append('<option value="' + item.value + '">' + item.label + '</option>');
                    	}
             }

				provincia.show();
			
			}
		});

    }
}




function filtrarPoblacion(nombre,idPais,idProvincia,poblacion) {
    
    if(nombre!='' && nombre.length>3)
    {

    	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/facturacion/facturas/clientes/select_list_poblaciones.do?idPais='/>"+idPais+"&idProvincia="+idProvincia+"&nombre="+nombre,
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				poblacion.find('option').remove();	
				poblacion.append('<option selected value="">Seleccione poblaci�n</option>');
				if (data.ArrayList!="") {
                    var item=data.ArrayList.LabelValue;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	poblacion.append('<option value="' + val.value + '">' + val.label + '</option>');
                        });
                    else
                    	poblacion.append('<option value="' + item.value + '">' + item.label + '</option>');
             }				
				poblacion.show();				
			}
		});

    }
}





function filtrarCP(nombreCp,idPais,idProvincia,idMunicipio,cp,idCp) {
	nombre = nombreCp.val();
    if(nombre!='' && nombre.length==5)
    {
     	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/facturacion/facturas/clientes/select_list_CP.do?idPais='/>"+idPais.val()+"&idProvincia="+idProvincia+"&idMunicipio="+idMunicipio+"&nombre="+nombre,
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				
				cp.find('option').remove();	
				if (data.ArrayList!="") {
                    var item=data.ArrayList.MunicipioCP;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	cp.append('<option value="' + val.value + '">' + val.label + '</option>');
                        });
                    else
                    	{
                    	console.log(item);                    	
                    	nombreCp.val(item.texto);
                    	idCp.val(item.idcp);
                    	idPais.val(item.idpais).prop('selected',true);
                    	returnidpoblacion.val(item.idmunicipio);
                    	returnidprovincia.val(item.idprovincia);
                    	returnfilterpoblacion.val(item.texto);
                    	returnfilterprovincia.val(item.texto);
                    	
                    	//$("#"+strIdPais+" ")


                    	cp.append('<option selected value="' + item.idcp + '">' + item.texto + '</option>');
                    	}
                    
             }				
			// cp.show();
				
			}
		});

    }
}

$(".save_cliente_dialog").on("click", function(e) {
	var xml_origen= generarXmlDireccion("origen"); 
	var xml_fiscal= generarXmlDireccion("fiscal"); 
	var xml_auxiliar= generarXmlDireccion("auxiliar"); 	
	$("#xmlDireccionOrigen").val(xml_origen);
	$("#xmlDireccionFiscal").val(xml_fiscal);
	$("#xmlDireccionAuxiliar").val(xml_auxiliar);
	GuardarDatosCliente();
})

var tipo = "${tipo}";

var idCliente = "<x:out select = "$editar_cliente_xml/Cliente/idcliente" />";



//Ocultamos los campos que no se ven a todos
if(tipo=="1")
	{
	
	var tipo_envio = ""+'<x:out select = "$editar_cliente_xml/Cliente/direccionenvio/iddireccionenvio"/>';
	// Solo dejamos la direcci�n necesaria, aqu� origen
	if((tipo_envio=="1")||(tipo_envio=="0")||(tipo_envio==""))		{
		$("#auxiliar-tab").hide();
		$("#fiscal-tab").hide();
		}
	//Aqu� fiscal
	if(tipo_envio=="2")
		{
		$("#direccion-tab").hide();
		$("#auxiliar-tab").hide();
		}	
	//Aqu� auxiliar
	if(tipo_envio=="3")
		{
		$("#direccion-tab").hide();
		$("#fiscal-tab").hide();
		}
	

	
	$(".facturacion_ocultar").hide();
	$("#div_selector_sucursales").hide();
	$("#div_sucursal").hide();
	$("#div_periodo_facturacion").hide();
	$("#div_idtiporappel").hide();
	$("#div_facturable_electronico").hide();
	$("#div_idperiodoenvio").hide();
	$("#div_canal").hide();
	$("#div_grupos_empresas").hide();
	$("#div_aparecepersonaenfactura").hide();
	if(idCliente!="")
		$("#save_cliente_button").hide();	
	
	}

function GuardarDatosCliente()
{
	 $('#identificador').removeAttr('disabled');
	    $('#idtipoidentificador').removeAttr('disabled');
	    $('#ciffiscal').removeAttr('disabled');
		$("#personafisica").removeAttr( "disabled");
		$("#idcategoriacategoriaactividad").removeAttr( "disabled");
		$("#selector_actividad").removeAttr( "disabled");	
		$("#tipo_identificador").removeAttr( "disabled");	
		$('#idcanal').removeAttr( "disabled");
		$('#descripcion').removeAttr( "disabled");
		$('#principal').removeAttr( "disabled");
		$('#sucursal').removeAttr( "disabled");
		$('#facturable_electronico').removeAttr( "disabled");
		$('#tab_direccion_fiscal select').removeAttr( "disabled");
		$('#tab_direccion_fiscal input').removeAttr( "disabled");
		$('#tab_direccion_fiscal button').removeAttr( "disabled");
		$('#tab_direccion_auxiliar select').removeAttr( "disabled");
		$('#tab_direccion_auxiliar input').removeAttr( "disabled");
		$('#tab_direccion_auxiliar button').removeAttr( "disabled");
		$('#tab_condiciones_cliente select').removeAttr( "disabled");
		$('#tab_condiciones_cliente input').removeAttr( "disabled");
		$('#tab_condiciones_cliente button').removeAttr( "disabled");
	 var nombre = ""+$("#nombre").val();
	 
	 if(nombre=="")
		 $("#nombre").val($("#razonSocial").val());
		 
	 var data = $("#form_cliente_data").serializeObject();
	 showSpinner("#modal-dialog-form .modal-content");

	
	 
	 $.ajax({
			type : "post",
			url : "<c:url value='/ajax/facturacion/facturas/clientes/save_cliente.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");				
				var nombre_completo = ""+$("#nombre").val()+" "+$("#apellido1").val()+" "+$("#apellido2").val();
				var cadena;
				if(data.ok!="")
					{
					$("#codigo_cliente_buscador").val(data.ok.Integer);
					$("#codigo").val(data.ok.Integer);
					cadena = "Se ha dado de alta el cliente "+nombre_completo+" con c�digo "+data.ok.Integer;
					}
				else
					cadena = "Los datos del cliente se han guardado correctamente";
				
				if(tipo=="0")
					{
					$("#modal-dialog-form").modal('hide');					
					dt_listclientes.ajax.reload(null,false);
					}
				else
					{
					$("#modal-dialog-form-editar-cliente").modal('hide');
					buscadorClientesReloadTabla();					
					}
				
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : cadena,
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
}


</script>


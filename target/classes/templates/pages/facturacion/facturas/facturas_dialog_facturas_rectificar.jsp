<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${datos_factura}" var="datos_factura_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	 <h4 class="modal-title">	
		<spring:message code="facturacion.facturas.tabs.facturas.dialog.rectificar_factura.title" />				
	</h4>	
</div>

<div class="modal-body" style="overflow-y: scroll;  max-height: 80vh;" id="capa_rectificar">
	<form id="form_factura_rectificar_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input id="idcliente_rectificacion" name="idcliente_rectificacion" type="hidden" value="<x:out select = "$datos_factura_xml/Factura/cliente/idcliente" />" />
		<input type="hidden" id="motivo" name="motivo"/>
		<input type="hidden" id="listIds" name="listIds"/>
		<input type="hidden" id="fechaOperacion" name="fechaOperacion" value="<x:out select = "$datos_factura_xml/Factura/fechageneracion" />"/>
		<input type="hidden" id="otros" name="otros"/>
		<input type="hidden" id="idTipoVenta_rectificacion" name="idTipoVenta" value="<x:out select = "$datos_factura_xml/Factura/ventafacturas/Ventafactura/venta/tipoventa/idtipoventa" />"/>
		<input type="hidden" id="lineasDetalleXML" name="lineasDetalleXML"/>
		<input type="hidden" id="xml_anulacion" name="xml_anulacion"/>
		<input type="hidden" id="xml_venta_rectificacion" name="xml_venta"/>
		
		 
		<input type="hidden" name="idFactura" value="<x:out select = "$datos_factura_xml/Factura/idfactura" />"/> 
		<div class="col-md-12 col-sm-12 col-xs-12">		
			<div class="col-md-6 col-sm-6 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="facturacion.facturas.tabs.factura.rectificar.fecha" />:</label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$datos_factura_xml/Factura/fechageneracion" /></div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="facturacion.facturas.tabs.factura.rectificar.nimpresiones" />:</label>
				<div class="col-md-6 col-sm-6 col-xs-6 texto-resumen"><x:out select = "$datos_factura_xml/Factura/numimpresiones" /></div>
			</div>
			<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="checkbox col-md-1 col-sm-1 col-xs-1 ">
							<input type="radio" name="tipo_rectificacion" id="rectificacion_cliente" checked value="1" data-parsley-multiple="tipo_identificacion" class="flat" style="position: absolute; opacity: 0;"/>			
						</div>
						<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="facturacion.facturas.tabs.factura.rectificar.cliente" /></label>
						<br/><br/>
						<div class="col-md-5 col-sm-5 col-xs-5 "></div>
						<div class="form-group"  id="div_cliente">
							<div class="col-md-8 col-sm-8 col-xs-8">
                				<input name="cliente_rectificacion" id="cliente_rectificacion" type="text" class="form-control" readonly value="<x:out select = "$datos_factura_xml/Factura/cliente/nombrecompleto" />">
	             			</div>				
						</div>						

					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="checkbox col-md-1 col-sm-1 col-xs-1 ">
							<input type="radio" name="tipo_rectificacion" id="rectificacion_ventas"  value="2" data-parsley-multiple="tipo_identificacion" class="flat" style="position: absolute; opacity: 0;"/>			
						</div>
						<label class="control-label col-md-8 col-sm-8 col-xs-8"><spring:message code="facturacion.facturas.tabs.factura.rectificar.ventas" /></label>
					    <div class="col-md-3 col-sm-3 col-xs-3">
					    </div>
						<div class="col-md-12 col-sm-12 col-xs-12 rectificar_botones_venta">			
											<div class="btn-group pull-right btn-datatable">
												<a type="button" class="btn btn-info" id="tab_factura_venta_new">
													<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.factura.rectificar.detalle_ventas.anadir" />"> <span class="fa fa-plus"></span>
													</span>
												</a>
												<a type="button" class="btn btn-info" id="tab_factura_venta_remove">
													<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.factura.rectificar.detalle_ventas.eliminar" />"> <span class="fa fa-trash"></span>
													</span>
												</a>		
											</div>			
						</div>					
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6"></div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<table id="datatable-list-detalle-venta" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th><spring:message code="facturacion.facturas.tabs.generacionfacturas.list.header.refventa" /></th>
									<th></th>
									<th><spring:message code="facturacion.facturas.tabs.generacionfacturas.list.header.cliente" /></th>
									<th><spring:message code="facturacion.facturas.tabs.generacionfacturas.list.header.fact" /></th>
									<th><spring:message code="facturacion.facturas.tabs.generacionfacturas.list.header.fechaventa" /></th>
									<th><spring:message code="facturacion.facturas.tabs.generacionfacturas.list.header.importe" /></th>	
								</tr>
							</thead>
							<tbody>
								<x:forEach select="$datos_factura_xml/Factura/ventasagrupadas/Venta" var="item">
								   <tr class="seleccionable">
								   		<td><x:out select="$item/idventa" /></td>
								   		<td><x:out select="$item/cliente/idcliente" /></td>
								   		<td><x:out select="$item/cliente/nombre" /></td>
								   		<td>0</td>
								   		<td><x:out select="$item/fechayhoraventa" /></td>
								   		<td><x:out select="$item/importetotalventa" /></td>
								   </tr>
								</x:forEach>
							</tbody>
						</table>
					</div>															
				</div>
				<div class="form-group">
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="checkbox col-md-1 col-sm-1 col-xs-1 ">
							<input type="radio" name="tipo_rectificacion" id="rectificacion_detalles"  value="3" data-parsley-multiple="tipo_identificacion" class="flat" style="position: absolute; opacity: 0;"/>			
						</div>
						<label class="control-label col-md-8 col-sm-8 col-xs-8"><spring:message code="facturacion.facturas.tabs.factura.rectificar.detalle_ventas" /></label>
										
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6 rectificar_botones_detalles">			
											<div class="btn-group pull-right btn-datatable">
												<a type="button" class="btn btn-info" id="tab_detalles_venta_edit">
													<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.factura.rectificar.detalle_ventas.edit" />"> <span class="fa fa-pencil"></span>
													</span>
												</a>
												<a type="button" class="btn btn-info" id="tab_detalles_venta_imprimir">
													<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.factura.rectificar.detalle_ventas.imprimir" />"> <span class="fa fa-print"></span>
													</span>
												</a>		
											</div>			
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-9 col-sm-9 col-xs-9">
				<spring:message code="facturacion.facturas.tabs.facturas.dialog.rectificar_factura.rectificar.detalle.lineas" />
				</div>
			</div>
			
					<table id="datatable-list-detalle-lineas-venta" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th></th>
									<th><spring:message code="facturacion.facturas.tabs.rectificarfacturas.list.header.venta" /></th>
									<th><spring:message code="facturacion.facturas.tabs.rectificarfacturas.list.header.linea_detalle" /></th>
									<th><spring:message code="facturacion.facturas.tabs.rectificarfacturas.list.header.producto" /></th>
									<th><spring:message code="facturacion.facturas.tabs.rectificarfacturas.list.header.detalle" /></th>
									<th><spring:message code="facturacion.facturas.tabs.rectificarfacturas.list.header.cantidad" /></th>
									<th><spring:message code="facturacion.facturas.tabs.rectificarfacturas.list.header.tarifa" /></th>
									<th><spring:message code="facturacion.facturas.tabs.rectificarfacturas.list.header.descuento" /></th>
									<th><spring:message code="facturacion.facturas.tabs.rectificarfacturas.list.header.importe" /></th>	
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
								
							</thead>
							<tbody>
								
							</tbody>
						</table>
							
		
	
	</form>
	<div class="modal-footer">
		<button id="rectificar_factura_save_button" type="button" class="btn btn-primary rectificar_dialog-save">
			<spring:message code="facturacion.facturas.tabs.rectificarfacturas.generar" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>
<script>

hideSpinner("#tab_gestion_factura_rectificar");


function calcularSesionLinea(linea,retorno)
{
	var fechasLinea="";

	var sesiones = linea.lineadetallezonasesions.Lineadetallezonasesion;
	if(sesiones!=undefined)
		if (sesiones.length>0)		 
		 {
			for (var i=0; i<sesiones.length; i++) {
				if (retorno) {
					fechasLinea+="</br>";				
				}				
				var f= sesiones[i].zonasesion.sesion.fecha.split("-")[0];
				fechasLinea+= f+" "+sesiones[i].zonasesion.sesion.horainicio
							
				retorno= true;
			}
		 }
		 else
		 {
			 fechasLinea = linea.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.fecha.split("-")[0] +" "+ linea.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.horainicio;
				
		 }
	
	return fechasLinea;
}

var idVentaRectificar,importeVenta,idCanal;
esconderBotones();

var json = ${datos_factura_json};
var jsonVenta = json.Factura.ventasdetalladasagrupadas.Venta;




function esconderBotones()
{
$(".rectificar_botones_detalles").hide();
$(".rectificar_botones_venta").hide();
}

var dtlistadoventas=$('#datatable-list-detalle-venta').DataTable( {
	"scrollCollapse": true,
    "paging":         false, 	
     "info": false,
     "searching": false,
     select: { style: 'os'},
     language: dataTableLanguage,
     "columnDefs": [
                    { "visible": false, "targets": [1,3]}
                  ]
     });
     
var dtlistadodetalles=$('#datatable-list-detalle-lineas-venta').DataTable( {
	"scrollCollapse": true,
    "paging":         false, 	
     "info": false,
     initComplete: function( settings, json ) {
    	 window.setTimeout(CargarLineasDetalle, 100);    	 
 	  },
     "searching": false,
     select: { style: 'os'},
     language: dataTableLanguage,
     columns: [
               {},              
               { className: "cell_centered" },
               { className: "cell_centered" },
               { className: "cell_centered" },
               { className: "cell_centered" },
               { className: "cell_centered" },
               { className: "cell_centered" },
               { className: "cell_centered" },
               { className: "cell_centered" },
               { className: "cell_centered" },
               { className: "cell_centered" },
               { className: "cell_centered" },
               { className: "cell_centered" },
         ],
     "columnDefs": [
                    { "visible": false, "targets": [0,9,10,11,12]}
                  ]
     
     });     


if ($("input.flat")[0]) {
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });
}


$("#cliente_rectificacion").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>", "#cliente_rectificacion", "#idcliente_rectificacion");


//*********************************************BOT�N NUEVO*************************************
$("#rectificar_factura_save_button").on("click", function(e) {
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/facturacion/facturas/facturas/rectificar_motivos.do'/>", function() {										  
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "sm");
	});
})

$("#tab_factura_venta_new").on("click", function(e) {
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/facturacion/facturas/facturas/buscarVentasDeCliente.do'/>?idcliente=<x:out select = "$datos_factura_xml/Factura/cliente/idcliente" />", function() {										  
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "md");
	});
})

$("#tab_factura_venta_remove").on("click", function(e) {
	dtlistadoventas
    .rows( '.selected' )
    .remove()
    .draw();
})


var tipoVenta;

$("#tab_detalles_venta_edit").on("click", function(e) {
	
	var ventas = dtlistadodetalles.rows( '.selected' ).data();
	
	if (ventas.length<=0) {
		new PNotify({
			title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			text : '<spring:message code="facturacion.facturas.tabs.rectificarfacturas.mensaje.seleccion_no" />',
			type : "error",
			delay : 5000,
			buttons : {
				closer : true,
				sticker : false
			}
		});
		return;
	}
	
	if (ventas.length>1) {
		new PNotify({
			title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			text : '<spring:message code="facturacion.facturas.tabs.rectificarfacturas.mensaje.seleccion_multiple" />',
			type : "error",
			delay : 5000,
			buttons : {
				closer : true,
				sticker : false
			}
		});
		return;
	}	
	
	var venta = ventas[0];
	
	
	
	showButtonSpinner("#tab_detalles_venta_edit");	
	idVentaRectificar = venta[1];
	importeVenta = venta[9];
	idCanal =  venta[10];
	tipoVenta =  venta[11];
	
	
	
	if(tipoVenta!=2)
		$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/facturacion/facturas/facturas/rectificarLineas.do'/>", function() {
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "lg");
		});		
	else
		$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/facturacion/bono/modificarLineas.do'/>", function() {
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "lg");
		});
	

	})
	
	$("tab_detalles_venta_imprimir	").on("click", function(e) {
	})	
	

var json = ${datos_factura_json};
	function CargarLineasDetalle()
	{
		
		var i = 0;
		
		var ventasdetalladasagrupadas = json.Factura.ventasdetalladasagrupadas;
				

		if (ventasdetalladasagrupadas.Venta!="") {
	        var item=ventasdetalladasagrupadas.Venta;
	        if (item.length>0)
	            $.each(item, function(key, venta){
	            	CargarLineasDetalleVenta(venta)
	            });
	        else
	        	{
	        	CargarLineasDetalleVenta(item)
	        	}
	        	
	}
	
		
	

		
	function CargarLineasDetalleVenta(jsonVenta)
	{
	    var strIdsLineas="";
	    var strProductos="";
	    var strCantidad ="";
	    var tarifa="";
	    var fechas ="";
	    var descuento = "";
	    var importeApagar="";
	    var idCanal= jsonVenta.canalByIdcanal.idcanal;
	    var tipoVenta = jsonVenta.tipoventa.idtipoventa;
	    
	    var lineas = jsonVenta.lineadetalles.Lineadetalle;

	    var retorno= false;
	    if (lineas.length>0)
	    	{
	    	for (var i=0; i<lineas.length; i++) {
	    		var linea = lineas[i];
	    		
	    		 if (retorno) {
	    			 strIdsLineas+="</br>";		
	    			 strProductos+="</br>";		
	    			 strCantidad+="</br>";	
	    			 tarifa+="</br>";
	    			 descuento+="</br>";
	    			 importeApagar+="</br>";
	    			 fechas+="</br>";
				}	
	    		retorno = true;	    		
	    		strIdsLineas+= linea.idlineadetalle;	    		
	    		strProductos+= linea.producto.nombre;
	    		
	    		strCantidad+= linea.cantidad;
	    		tarifa+=linea.perfilvisitante.tarifa.nombre;
		    	if(linea.descuentopromocional!="")
					descuento = linea.descuentopromocional.nombre; 
		    	importeApagar += linea.importe;
		    	fechas+=calcularSesionLinea(linea,retorno)
	         };
	    	}
	    else
	    	{
	    	strIdsLineas = lineas.idlineadetalle;
	    	strProductos+= lineas.producto.nombre;
	    	
	    	strCantidad+= lineas.cantidad;
	    	tarifa+=lineas.perfilvisitante.tarifa.nombre;
	    	
	    	if(lineas.descuentopromocional!="")
				descuento = lineas.descuentopromocional.nombre;  
	    	
	    	importeApagar = 1.0 * lineas.importe;
	    	fechas=calcularSesionLinea(lineas,retorno);
	    	}		
		
		dtlistadodetalles.row.add([jsonVenta,jsonVenta.idventa[0],strIdsLineas,strProductos,fechas,strCantidad,tarifa,descuento,importeApagar,"",idCanal,tipoVenta,"0"]).draw();
		
	}
	
	function CargarLineaDetalle(venta,lineadetalles)
	{
		
		var descuento="";
		var importe = lineadetalles.importe;
		var retorno= false;
		var fechas= "", contenidos= "";
		var tipoVenta = venta.tipoventa.idtipoventa;
		var porcentajeDescuento = lineadetalles.descuento.porcentajedescuento / 100.0 	
		var importeApagar = 1.0 * importe;
		descuento = ""+lineadetalles.descuento;
		
		if(descuento!="")
			{
			importeApagar = importe *(1.0 - porcentajeDescuento);
			}	
		

		var importeVenta = venta.importetotalventa;
		var idCanal= venta.canalByIdcanal.idcanal;
		
		if(lineadetalles.lineadetallezonasesions!="")
			{
			var sesiones = lineadetalles.lineadetallezonasesions.Lineadetallezonasesion;
			if (sesiones.length>0)		 
			 {
				for (var i=0; i<sesiones.length; i++) {
					if (retorno) {
						fechas+="</br>";
						contenidos+= "</br>";
						
					}				
					var f= sesiones[i].zonasesion.sesion.fecha.split("-")[0];
					fechas+= f;					
					var hi=sesiones[i].zonasesion.sesion.horainicio;
					var  no=sesiones[i].zonasesion.sesion.contenido.nombre;
					contenidos+= hi+" "+no;			
					retorno= true;
				}
			 }
			 else
			 {
				fechas= lineadetalles.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.fecha.split("-")[0];
				contenidos= lineadetalles.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.horainicio+" "+lineadetalles.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.contenido.nombre;
					
			 }
		
		}
		else
		{
			contenidos= lineadetalles.producto.nombre;
		}
		if(lineadetalles.descuentopromocional!="")
			descuento = lineadetalles.descuentopromocional.nombre;  		
		dtlistadodetalles.row.add([lineadetalles,""+venta.idventa[0],""+lineadetalles.idlineadetalle,""+contenidos,""+fechas,""+lineadetalles.cantidad,""+lineadetalles.perfilvisitante.tarifa.nombre,descuento,importeApagar.toFixed(2),importeVenta,idCanal,tipoVenta]).draw();
	}
}



$("input[name='tipo_rectificacion']").on("ifChanged", function(e) {	
	esconderBotones();
	if ($("input[name='tipo_rectificacion']:checked").val()=="2")
		$(".rectificar_botones_venta").show();	
	if ($("input[name='tipo_rectificacion']:checked").val()=="3")
		$(".rectificar_botones_detalles").show();	
});

//********************************************************************************
function construir_xml_rectificaryRectificar() {
	
	var xml_venta= "<list>";
	
	var ventas = dtlistadodetalles.rows().data();
	
	for (var k=0; k<ventas.length; k++) 
	   {
		if(dtlistadodetalles.rows().data()[k][12]=="1")
			{
			jsonVenta = dtlistadodetalles.rows().data()[k][0];	
			xml_venta+="<Venta idventa=\""+jsonVenta.idventa[0]+"\">";
			xml_venta+="<idventa>"+jsonVenta.idventa[0]+"</idventa>";
		    xml_venta+="<fechayhoraventa>"+jsonVenta.fechayhoraventa+"</fechayhoraventa>";
		    xml_venta+="<importetotalventa>"+jsonVenta.importetotalventa+"</importetotalventa>";
		    xml_venta+="<importeparcialtotalventa>"+jsonVenta.importeparcialtotalventa+"</importeparcialtotalventa>";
		    xml_venta+="<cliente>"+json2xml(jsonVenta.cliente)+"</cliente>";
		    xml_venta+="<facturada>"+jsonVenta.facturada+"</facturada>";
		    xml_venta+="<tipoventa>"+json2xml(jsonVenta.tipoventa)+"</tipoventa>";
		    xml_venta+="<canalByIdcanal>"+json2xml(jsonVenta.canalByIdcanal)+"</canalByIdcanal>";
		    xml_venta+="<canalByCanalventa>"+json2xml(jsonVenta.canalByCanalventa)+"</canalByCanalventa>";
		    xml_venta+="<porcentajesDescuentoLD>"+json2xml(jsonVenta.porcentajesDescuentoLD)+"</porcentajesDescuentoLD>";
		    xml_venta+="<lineadetalles>";
		    
		    lineasDetalle = jsonVenta.lineadetalles.Lineadetalle;
		    if(lineasDetalle.length>0)
		    	{
		    	$.each(lineasDetalle, function(key, lineaDetalle)
		    		    {
		    			   xml_venta+="<Lineadetalle>"+json2xml(lineaDetalle)+"</Lineadetalle>";
		    		    })
		    	}	    	
		    else
		    	xml_venta+="<Lineadetalle>"+json2xml(lineasDetalle)+"</Lineadetalle>";
		    
		    
		    xml_venta+="</lineadetalles>";
		    if(tipoVenta!=2)
		       xml_venta+="<modificacions>"+$("#xml_anulacion").val()+"</modificacions>";
		   	xml_venta+="</Venta>";
			}
		}

	showSpinner("#capa_rectificar");
   	
   	xml_venta+="</list>";
   	$("#xml_venta_rectificacion").val(xml_venta);
   	$("#modal-dialog-form-2").modal('hide');
   	var data = $("#form_factura_rectificar_data").serializeObject();
   	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/facturacion/factura_rectificar.do'/>",
		timeout : 100000,
		data: data, 
		success : function(data) {
			var xml = json2xml(data.ArrayList,''); 	
			dt_listgestionfacturas.ajax.reload();	
			
			var url = "<c:url value='/ajax/facturacion/facturas/generacionfacturas/show_listadofacturas.do'/>?"+encodeURI("facturas="+xml);
			$("#modal-dialog-form .modal-content").load(url, function() {
				hideSpinner("#capa_rectificar");
				$("#modal-dialog-form").modal('show');
				setModalDialogSize("#modal-dialog-form", "md");
			});						
		},
		error : function(exception) {
			hideSpinner("#capa_rectificar");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
   
   	
	
	return(xml_venta);
}
</script>
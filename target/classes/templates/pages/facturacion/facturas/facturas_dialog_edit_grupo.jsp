<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_grupo}" var="editar_grupo_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<x:choose>
			<x:when select="not($editar_grupo_xml/Grupoempresas)">
				<spring:message code="facturacion.facturas.tabs.clientes.dialog.crear_grupo.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="facturacion.facturas.tabs.clientes.dialog.editar_grupo.title" />
			</x:otherwise>
		</x:choose>		
	</h4>	
</div>

<div class="modal-body">
	<form id="form_grupo_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input id="idgrupo" name="idgrupo" type="hidden" value="<x:out select = "$editar_grupo_xml/Grupoempresas/idgrupoempresas" />" />
		<input id="idcliente" name="idcliente" type="hidden" value="" />
		<input name="cliente" id="cliente" type="hidden" class="form-control" readonly value="">
		<input name="cif" id="cif" type="hidden" class="form-control" readonly value="">
		<input name="xmlRappels" id="xmlRappels" type="hidden" class="form-control" readonly value="">
		<input name="xmlEmpresas" id="xmlEmpresas" type="hidden" class="form-control" readonly value="">
				
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.field.nombre" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editar_grupo_xml/Grupoempresas/nombre" />">
				</div>
			</div>		
			<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.grupos.field.descripcion" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">					
						<textarea name="descripcion" id="descripcion" class="form-control"  ><x:out select = "$editar_grupo_xml/Grupoempresas/descripcion" /></textarea>
					</div>
			</div>	
			

					
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="form-group" id="div_rappels_grupo">
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.grupos.field.rappels" /></label>
				<div class="col-md-10 col-sm-10 col-xs-10">			
					<div class="btn-group pull-right btn-datatable">
						<a type="button" class="btn btn-info" id="tab_rappels_grupo_new">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.grupos.rappels.list.button.nuevo" />"> <span class="fa fa-plus"></span>
							</span>
						</a>
						<a type="button" class="btn btn-info" id="tab_rappels_grupo_edit">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.grupos.rappels.list.button.editar" />"> <span class="fa fa-pencil"></span>
							</span>
						</a>
						<a type="button" class="btn btn-info" id="tab_rappels_grupo_remove">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.grupos.rappels.list.button.eliminar" />"> <span class="fa fa-trash"></span>
							</span>
						</a>		
					</div>			
			    </div>
			
					<table id="rappels_grupos_table" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
							  <th id="id"></th>
							  <th id="idtiporappel"></th>
							  <th id="nombre"><spring:message code="facturacion.facturas.tabs.grupos.field.nombre_rappel"/></th>
							  <th id="fechainiciovigencia"><spring:message code="facturacion.facturas.tabs.grupos.field.fechainiciovigencia" /></th>
							  <th id="fechafinvigencia"><spring:message code="facturacion.facturas.tabs.grupos.field.fechafinvigencia" /></th>							 
							</tr>
						</thead>
						<tbody>				
							<x:forEach select="$editar_grupo_xml/Grupoempresas/tiporappelgrupoempresass/Tiporappelgrupoempresas" var="item">
							   <tr class="seleccionable">
							   		<td id="id" ><x:out select="$item/idempresagrupo" /></td>
							   		<td id="idtiporappel" ><x:out select="$item/tiporappel/idtiporappel" /></td>
							   		<td id="nombre" ><x:out select="$item/tiporappel/nombre" /></td>
									<td id="fechainiciovigencia">
										<c:set var="fechainiciovigencia"><x:out select="$item/fechainiciovigencia"/></c:set>
										<fmt:parseDate value="${fechainiciovigencia}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadainiciovigencia"/>
										<fmt:formatDate value="${fechaformateadainiciovigencia}" pattern="dd/MM/yyyy" type="DATE"/>									
									</td>									
									<td id="fechafinvigencia">
										<c:set var="fechafinvigencia"><x:out select="$item/fechafinvigencia"/></c:set>
										<fmt:parseDate value="${fechafinvigencia}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateadafinvigencia"/>
										<fmt:formatDate value="${fechaformateadafinvigencia}" pattern="dd/MM/yyyy" type="DATE"/>									
									</td>																											
								</tr>																
							</x:forEach>
						</tbody>				
					</table>				
			</div>
		</div>
		
			<div class="form-group" id="div_empresas">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.grupos.field.empresas" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">			
					<div class="btn-group pull-right btn-datatable">
						<a type="button" class="btn btn-info" id="tab_empresas_new">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.grupos.empresas.list.button.nuevo" />"> <span class="fa fa-plus"></span>
							</span>
						</a>
						<a type="button" class="btn btn-info" id="tab_empresas_remove">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.grupos.empresas.list.button.eliminar" />"> <span class="fa fa-trash"></span>
							</span>
						</a>		
					</div>			
			    </div>
			
					<table id="empresas_table" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
							  <th id="id"></th>
							  <th id="idcliente"></th>
							  <th id="nombre"><spring:message code="facturacion.facturas.tabs.grupos.field.nombre_emprresa"/></th>
							  <th id="identificador"><spring:message code="facturacion.facturas.tabs.grupos.field.identificador" /></th>
							  <th id="fechaincorporacion"><spring:message code="facturacion.facturas.tabs.grupos.field.fechaincorporacion" /></th>							  						 
							</tr>
						</thead>
						<tbody>				
							<x:forEach select="$editar_grupo_xml/Grupoempresas/empresagrupos/Empresagrupo" var="item">
							   <tr class="seleccionable">
							   		<td id="id" ><x:out select="$item/idempresagrupo" /></td>
							   		<td id="idcliente" ><x:out select="$item/cliente/idcliente" /></td>
							   		<td id="nombre" ><x:out select="$item/cliente/nombre" /></td>
									<td id="identificador"><x:out select="$item/cliente/identificador" /></td>
									<td id="fechaincorporacion">
										<div class="form-group date-picker">
											<div class="col-md-12 col-sm-12 col-xs-12">
												  <a type="button" class="btn btn-default btn-clear-date" id="button_vigencia_clear">
														<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.formasdepago.list.button.clear" />"> <span class="fa fa-trash"></span>
														</span>
												  </a>			
						                        <div class="input-prepend input-group">
						                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
						                          <input type="text" name="incorporacion<x:out select="$item/cliente/idcliente" />" id="incorporacion<x:out select="$item/cliente/idcliente" />" class="form-control" value="<x:out select="$item/fechaincorporacion"/>" readonly/>
						                          <input type="hidden" required="required" name="fechaincorporacion<x:out select="$item/cliente/idcliente" />" value="<x:out select="$item/fechaincorporacion"/>"/>
						                        </div>
											</div>
										</div>
									</td>									
								</tr>																
							</x:forEach>
						</tbody>				
					</table>				
			</div>
		
	</form>
	<div class="modal-footer">
		<button id="list_empresas_button" type="button" class="btn btn-primary list_empresas_button">
			<spring:message code="facturacion.facturas.tabs.grupos.dialog.grupo.button_list_empresas.text" />
		</button>
	
		<button id="save_canal_button" type="button" class="btn btn-primary save_grupo_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>		

</div>

<script>

hideSpinner("#tab_grupos_new");
hideSpinner("#tab_grupos_edit");


//Caducidad
$today= moment().format("DD/MM/YYYY");


<x:forEach select="$editar_grupo_xml/Grupoempresas/empresagrupos/Empresagrupo" var="item">

if($('input[name="fechaincorporacion<x:out select="$item/cliente/idcliente" />"]').val()!='')
	{	
	dia_inicio = $('input[name="fechaincorporacion<x:out select="$item/cliente/idcliente" />"]').val().substring(0, 10);
	}
else
	dia_inicio = $today;

$('input[name="incorporacion<x:out select="$item/cliente/idcliente" />"]').val( dia_inicio);

$('input[name="incorporacion<x:out select="$item/cliente/idcliente" />"]').daterangepicker({
	singleDatePicker: true,
	autoUpdateInput: false,
	linkedCalendars: false,
	showDropdowns: true,
	autoApply: true,
	locale: $daterangepicker_sp
	}, function(start) {
	  	 $('input[name="incorporacion<x:out select="$item/cliente/idcliente" />"]').val(start.format('DD/MM/YYYY'));
	  	 $('input[name="fechaincorporacion<x:out select="$item/cliente/idcliente" />"]').val(start.format('DD/MM/YYYY'));
	  	 
	 });	 
</x:forEach>

var dtempresas,dtrappels;

$(document).ready(function() {
	
	dtempresas=$('#empresas_table').DataTable( {
		"paging": false,
		"info": false,
		"searching": false,
		select: { style: 'os'},
		language: dataTableLanguage,
		 "columnDefs": [
		                { "visible": false, "targets": [0,1]}
		              ]
		});
	
	dtrappels=$('#rappels_grupos_table').DataTable( {
		"paging": false,
		"info": false,
		"searching": false,
		select: { style: 'os'},
		language: dataTableLanguage,
		 "columnDefs": [
		                { "visible": false, "targets": [0,1]}
		              ]
		});
})

$("#tab_empresas_remove").on("click", function(e) {	
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="facturacion.facturas.tabs.grupos.field.empresas.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
		  buttons: { closer: false, sticker: false	},
		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
		   dtempresas
		    .rows( '.selected' )
		    .remove()
		    .draw();
	   })	
});

$("#tab_rappels_grupo_new").on("click", function(e) {	
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/facturacion/facturas/grupos/show_rappel_grupo.do'/>", function() {										  
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "sm");
	});	
});

$("#tab_rappels_grupo_edit").on("click", function(e) {	
	if(dtrappels.rows( { selected: true } ).data().length==1)		
    {
		var datos = dtrappels.rows( '.selected' ).data()[0];
		var creando = false;
		var id=datos[0];
		var idRappel=datos[1];
		var fechainicio=datos[3];
		var fechafin=datos[4];
		
		var url = "<c:url value='//ajax/facturacion/facturas/grupos/show_rappel_grupo.do'/>?"+encodeURI("creando="+creando+"&id="+id+"&idRappel="+idRappel+"&fechainicio="+fechainicio+"&fechafin="+fechafin);

		$("#modal-dialog-form-2 .modal-content").load(url, function() {										  
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "sm");
		});
    }
    else
    	{
    	if(dtrappels.rows( { selected: true } ).data().length==0)
	    	{
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="facturacion.facturas.tabs.grupos.rappels.list.alert.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
	    	}
	    	
		else
			{
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="facturacion.facturas.tabs.grupos.rappels.list.alert.seleccion_multiple" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			}
		}});

$("#tab_rappels_grupo_remove").on("click", function(e) {	
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="facturacion.facturas.tabs.grupos.field.rappels.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
		  buttons: { closer: false, sticker: false	},
		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
		   dtrappels
		    .rows( '.selected' )
		    .remove()
		    .draw();
	   })		
});

$("#tab_empresas_new").on("click", function(e) {	
	
	

	$( "#cif").unbind( "click" );
	$("#modal-dialog-form-buscar-cliente .modal-content").load("<c:url value='/ajax/show_buscadorcliente.do'/>", function() {                                                                  
           $("#modal-dialog-form-buscar-cliente").modal('show');
	       	$("#modal-dialog-form-buscar-cliente input#campoBuscadorClienteDevueltoNombre").val("#cliente");
	       	$("#modal-dialog-form-buscar-cliente input#campoBuscadorClienteDevueltoId").val("#idcliente");
	       	$("#modal-dialog-form-buscar-cliente input#campoBuscadorClienteDevueltoCif").val("#cif");
           setModalDialogSize("#modal-dialog-form-buscar-cliente", "lg");
    });

    
});



//A�ade una fila a la emprersa cuando cambia el cif del cliente
$( "#cif" ).change(function() {
	var html_datapaicker = '<div class="form-group date-picker">';
	html_datapaicker +=    '<div class="col-md-12 col-sm-12 col-xs-12">';
	html_datapaicker +=    '<a type="button" class="btn btn-default btn-clear-date" id="button_vigencia_clear">';
	html_datapaicker +=    '<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.formasdepago.list.button.clear" />"> <span class="fa fa-trash"></span>';
	html_datapaicker +=    '</span>';
	html_datapaicker +=    '</a>';		
	html_datapaicker +=    '<div class="input-prepend input-group">';
	html_datapaicker +=    '<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>';
	html_datapaicker +=    '<input type="text" name="incorporacion'+$("#idcliente").val()+'" id="incorporacion'+$("#idcliente").val()+'" class="form-control" value="" readonly/>';
	html_datapaicker +=    '<input type="hidden" required="required" name="fechaincorporacion'+$("#idcliente").val()+'" value=""/>';
	html_datapaicker +=    '</div>';
	html_datapaicker +=    '</div>';
	html_datapaicker +=    '</div>';
	dtempresas.row.add( $('<tr><td></td><td>'+$("#idcliente").val()+'</td><td>'+$("#cliente").val()+'</td><td>'+$("#cif").val()+'</td><td>'+html_datapaicker+'</td></tr>')[0] ).draw();
	
	$('input[name="incorporacion'+$("#idcliente").val()+'"]').val($today);		
	
	$('input[name="incorporacion'+$("#idcliente").val()+'"]').daterangepicker({
		singleDatePicker: true,
		autoUpdateInput: false,
		linkedCalendars: false,
		autoApply: true,
	  	locale: $daterangepicker_sp
		}, function(start) {
	  	  	 $('input[name="incorporacion'+$("#idcliente").val()+'"]').val(start.format('DD/MM/YYYY'));
	  	  	 $('input[name="fechaincorporacion'+$("#idcliente").val()+'"]').val(start.format('DD/MM/YYYY'));	  	  	
	  	  	 
	  	  	 
		 });	
	
	
});

$(".save_grupo_dialog").on("click", function(e) {		
	$("#form_grupo_data").submit();	
});

$("#form_grupo_data").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		guardarGrupo()
	}
})

function guardarGrupo()
	{
	// Se genera el xml de empreesas 
    var asignaciones_empresas= dtempresas.rows().data();
    var empresas_xml= "<empresagrupos>";
    
    asignaciones_empresas.each(function (value, index) {
    	var idEmpresaGrupo = dtempresas.rows(index).data()[0][0];
    	var idEmpresa = dtempresas.rows(index).data()[0][1];
    	var fecha = $('input[name="incorporacion'+idEmpresa+'"]').val().substring(0, 10);
    	empresas_xml+= "<Empresagrupo>";
    	empresas_xml+= "<idempresagrupo>"+idEmpresaGrupo+"</idempresagrupo>";
    	empresas_xml+= "<cliente><idcliente>"+idEmpresa+"</idcliente></cliente>";
    	empresas_xml+= "<fechaincorporacion>"+fecha+"</fechaincorporacion>";
    	empresas_xml+= "</Empresagrupo>";
    });
    empresas_xml+= "</empresagrupos>";
    
    $("#xmlEmpresas").val(empresas_xml);
    
   
    
 	// Se genera el xml de rappels 
    var asignaciones_rappels= dtrappels.rows().data();
    var rappels_xml= "<tiporappelgrupoempresass>";
    
    asignaciones_rappels.each(function (value, index) {
    	rappels_xml+= "<Tiporappelgrupoempresas>";
    	rappels_xml+= "<tiporappel><idtiporappel>"+dtrappels.rows(index).data()[0][1]+"</idtiporappel></tiporappel>";
    	rappels_xml+= "<fechainiciovigencia>"+dtrappels.rows(index).data()[0][3]+"</fechainiciovigencia>";
    	rappels_xml+= "<fechafinvigencia>"+dtrappels.rows(index).data()[0][4]+"</fechafinvigencia>";
    	rappels_xml+= "</Tiporappelgrupoempresas>";
    });
    rappels_xml+= "</tiporappelgrupoempresass>";
    $("#xmlRappels").val(rappels_xml);
    
    var data = $("#form_grupo_data").serializeObject();
    showSpinner("#modal-dialog-form .modal-content");

	$.ajax({
		type : "post",
		url : "<c:url value='/ajax/facturacion/facturas/grupos/save_grupo.do'/>",
		timeout : 100000,
		data : data,
		success : function(data) {
			hideSpinner("#modal-dialog-form .modal-content");
			dt_listgrupos.ajax.reload(null,false);			
			$("#modal-dialog-form").modal('hide');				
			
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
			
		},
		error : function(exception) {
			hideSpinner("#modal-dialog-form .modal-content");

			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : exception.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		}
	});
    
    
}


$(".list_empresas_button").on("click", function(e) {		
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/facturacion/facturas/grupos/show_empresas_grupo.do'/>?id="+$("#idgrupo").val(), function() {										  
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "sm");
	});	
});



</script>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2><spring:message code="common.text.filter_list" /></h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">

		<form id="form_filter_list_clientes" class="form-horizontal form-label-left">

			<div class="col-md-5 col-sm-5 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.field.codigocliente" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input id="codigo_cliente_buscador" name="codigocliente" type="text" class="form-control">
					</div>
				</div>
			
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.field.mombre" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="nombre" type="text" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.field.apellido1" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="apellido1" type="text" class="form-control">
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.field.apellido2" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="apellido2" type="text" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.field.dni" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="dni" type="text" class="form-control">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.field.telefono" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="telefono" type="text" class="form-control" style="position: relative;">
					</div>
				</div>
			</div>


			<div class="col-md-7 col-sm-7 col-xs-12">
			<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.field.fax" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="fax" type="text" class="form-control" style="position: relative;">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.field.direccion" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="direccion" type="text" class="form-control" style="position: relative;">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.field.localidad" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="localidad" type="text" class="form-control" style="position: relative;">
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.field.cp" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input name="cp" type="text" class="form-control" style="position: relative;">
					</div>
				</div>
				
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.field.principal" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input id="prin-switch" name="prin" type="checkbox" class="js-switch-prin"  data-switchery="true" style="display: none;">
					</div>
				</div>
				
			</div>

			<div class="clearfix"></div>
			<div class="ln_solid"></div>
			<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
				<button id="button_clean_cliente" type="button" class="btn btn-success pull-right">
						<spring:message code="venta.ventareserva.tabs.canje-bono.button.clean" />
				</button>
				<button id="button_filter_list_clientes" type="button" class="btn btn-success pull-right">
					<spring:message code="common.button.filter" />
				</button>
			</div>
			<input type="hidden" name="start" value=""/>
			<input type="hidden" name="length" value=""/>
		</form>


	</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_clientes_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.clientes.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_clientes_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.clientes.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_clientes_send_mail">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.clientes.list.button.enviar_mail" />"> <span class="fa fa-envelope"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_clientes_tarifas">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.clientes.list.button.tarifas" />"> <span class="fa fa-euro"></span>
			</span>
		</a>		
		<a type="button" class="btn btn-info" id="tab_clientes_lock">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.clientes.list.button.bloquear" />"> <span class="fa fa-lock"></span>
			</span>
		</a>		
		<a type="button" class="btn btn-info" id="tab_clientes_remove">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.clientes.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>
	</div>

	<table id="datatable-list-clientes" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="facturacion.facturas.tabs.clientes.list.header.ppal" /></th>
				<th><spring:message code="facturacion.facturas.tabs.clientes.list.header.cod" /></th>
				<th><spring:message code="facturacion.facturas.tabs.clientes.list.header.nombre" /></th>
				<th><spring:message code="facturacion.facturas.tabs.clientes.list.header.apellido1" /></th>
				<th><spring:message code="facturacion.facturas.tabs.clientes.list.header.apellido2" /></th>
				<th><spring:message code="facturacion.facturas.tabs.clientes.list.header.contacto" /></th>
				<th><spring:message code="facturacion.facturas.tabs.clientes.list.header.telefono" /></th>
				<th><spring:message code="facturacion.facturas.tabs.clientes.list.header.fact" /></th>
				<th><spring:message code="facturacion.facturas.tabs.clientes.list.header.tipo" /></th>
				<th><spring:message code="facturacion.facturas.tabs.clientes.list.header.bloq" /></th>
				<th style="display:none;"></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script>
var dt_listclientes =$('#datatable-list-clientes').DataTable( {
	serverSide: true,
	searching: false,
	ordering: false,
	pagingType: "simple",
	info: false,
	deferLoading: 0,
    ajax: {
        url: "<c:url value='/ajax/facturacion/facturas/clientes/list_clientes.do'/>",
        rowId: 'idcliente',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Cliente)); return(""); },
        data: function (params) {
        	$('#form_filter_list_clientes input[name="start"]').val(params.start);
        	$('#form_filter_list_clientes input[name="length"]').val(params.length);
        	return ($("#form_filter_list_clientes").serializeObject());
       	},		        
        error: function (xhr, error, thrown) {
            if (xhr.responseText=="403") {
                  $("#clientes-tab").hide();
            }                   
     },
    },
    initComplete: function( settings, json ) {
    	
        $('a#menu_toggle').on("click", function () {if (dt_listclientes.data().count()>0) dt_listclientes.columns.adjust().draw(); });
	},
    columns: [
		{ data: "principal", className: "text_icon cell_centered",
    		render: function ( data, type, row, meta ) {
  	  	  	if (data==1) return '<i class="fa fa-check" aria-hidden="true"></i>'; else return '<i class="fa fa-close" aria-hidden="true"></i>';
    	}},		
		{ data: "idcliente", type: "spanish-string", defaultContent: ""}, 
		{ data: "nombre", type: "spanish-string", defaultContent: ""}, 
		{ data: "apellido1", type: "spanish-string", defaultContent: "" }, 
		{ data: "apellido2", type: "spanish-string", defaultContent: ""}, 
		{ data: "personacontactogeneral", type: "spanish-string", defaultContent: ""},                
		{ data: "direccionfiscal.telefonofijo", type: "spanish-string", defaultContent: ""},
		{ data: "facturable", className: "text_icon cell_centered",
    		render: function ( data, type, row, meta ) {
  	  	  	if ((data==1)||(data==2)) return '<i class="fa fa-check" aria-hidden="true"></i>'; else return '<i class="fa fa-close" aria-hidden="true"></i>';	
    	}},
		{ data: "tipoclienteactivo.nombre", type: "spanish-string", defaultContent: ""},
		{ data: "bloqueado", className: "text_icon cell_centered",
    		render: function ( data, type, row, meta ) {
  	  	  	if ((data==1)) return '<i class="fa fa-check" aria-hidden="true"></i>'; else return '<i class="fa fa-close" aria-hidden="true"></i>';	
    	}},	   
    	{ data: "bloqueado", visible:false, defaultContent: ""},
    ],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-clientes') },
    select: { style: 'os'},
	language: dataTableLanguage,
	processing: true	
} );

setTimeout(ajustar_cabeceras_cliente, 1000);

function ajustar_cabeceras_cliente()
{
	$('#datatable-list-disponibles-${count}').DataTable().columns.adjust().draw();
}

//*******************************************************************************

$("#prin-switch").each(function(index) {
	var s = new Switchery(this, {
		size : 'small'
	});
});

//********************************************************************************
$("#button_filter_list_clientes").on("click", function(e) {
	insertSmallSpinner("#datatable-list-clientes_processing");
	dt_listclientes.ajax.reload(); 
})
//********************************************************************************
$("#tab_clientes_new").on("click", function(e) {
	 showButtonSpinner("#tab_clientes_new");
	 $("#modal-dialog-form .modal-content:first").load("<c:url value='/ajax/facturacion/facturas/clientes/show_cliente.do'/>", function() {
		 
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "lg");
	});
})

	/***********************************************BOT�N EDITAR*************************************/
 	$("#tab_clientes_edit").on("click", function(e) { 
 		
 	if(dt_listclientes==null)
 		{
 		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="facturacion.facturas.tabs.clientes.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;	
 		}
	var data = sanitizeArray(dt_listclientes.rows( { selected: true } ).data(),"idcliente");
		
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="facturacion.facturas.tabs.clientes.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="facturacion.facturas.tabs.clientes.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#tab_clientes_edit");
	$("#modal-dialog-form .modal-content:first").load("<c:url value='/ajax/facturacion/facturas/clientes/show_cliente.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "lg");
	});
})

$("#tab_clientes_lock").on("click", function(e) { 
	
	if(dt_listclientes==null)
		{
		new PNotify({
	      title: '<spring:message code="common.dialog.text.error" />',
	      text: '<spring:message code="facturacion.facturas.tabs.clientes.list.alert.seleccion_multiple.bloqueo" />',
		  type: "error",
		  delay: 5000,
		  buttons: { sticker: false }
	   });
	return;	
		}
	var data = sanitizeArray(dt_listclientes.rows( { selected: true } ).data(),"idcliente");
	
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="facturacion.facturas.tabs.clientes.list.alert.ninguna_seleccion.bloqueo" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="facturacion.facturas.tabs.clientes.list.alert.seleccion_multiple.bloqueo" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	

	
	
	var bloqueado =dt_listclientes.rows( '.selected' ).data()[0].bloqueado;
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/facturacion/facturas/clientes/pantalla_bloqueo.do'/>?id="+data[0]+"&bloqueado="+bloqueado, function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
})

$("#tab_clientes_remove").on("click", function(e) { 
	var data = sanitizeArray(dt_listclientes.rows( { selected: true } ).data(),"idcliente");
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="facturacion.facturas.tabs.clientes.list.alert.ninguna_seleccion.eliminar" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	var ids="";
	
	for(i=0;i<data.length;i++)
		{
		if(i>0)
		 ids+=",";
		ids +=data[i];
		}
	     
		
	
	$("#modal-dialog-form .modal-content:first").load("<c:url value='/ajax/facturacion/facturas/clientes/pantalla_eliminacion.do'/>?ids="+ids, function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "sm");
	});
	
})

/***********************************************BOT�N enviar mail*************************************/
	$("#tab_clientes_send_mail").on("click", function(e) {
		var data = sanitizeArray(dt_listclientes.rows( { selected: true } ).data(),"idcliente");
		if (data.length<=0) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="facturacion.facturas.tabs.clientes.list.alert.ninguna_seleccion.email" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}	
		
		var ids="";
		
		for(i=0;i<data.length;i++)
			{
			if(i>0)
			 ids+=",";
			ids +=data[i];
			}
		     
			
		
		$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/facturacion/facturas/clientes/pantalla_enviar_correo.do'/>?ids="+ids, function() {
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "sm");
		});
	})


/***********************************************BOT�N EDITAR*************************************/
	$("#tab_clientes_tarifas").on("click", function(e) { 
		
	if(dt_listclientes==null)
		{
		new PNotify({
	      title: '<spring:message code="common.dialog.text.error" />',
	      text: '<spring:message code="facturacion.facturas.tabs.clientes.list.alert.ninguna_seleccion" />',
		  type: "error",
		  delay: 5000,
		  buttons: { sticker: false }
	   });
	return;	
		}
var data = sanitizeArray(dt_listclientes.rows( { selected: true } ).data(),"idcliente");
	
if (data.length>1) {
	new PNotify({
	      title: '<spring:message code="common.dialog.text.error" />',
	      text: '<spring:message code="facturacion.facturas.tabs.clientes.list.alert.seleccion_multiple.asignar" />',
		  type: "error",
		  delay: 5000,
		  buttons: { sticker: false }
	   });
	return;
}

if (data.length<=0) {
	new PNotify({
	      title: '<spring:message code="common.dialog.text.error" />',
	      text: '<spring:message code="facturacion.facturas.tabs.clientes.list.alert.ninguna_seleccion.asignar" />',
		  type: "error",
		  delay: 5000,
		  buttons: { sticker: false }
	   });
	return;
}	

showButtonSpinner("#tab_clientes_tarifas");
$("#modal-dialog-form .modal-content:first").load("<c:url value='/ajax/facturacion/facturas/clientes/asignar_tarifas.do'/>?id="+data[0], function() {
	$("#modal-dialog-form").modal('show');
	setModalDialogSize("#modal-dialog-form", "md");
});
})


/***********************************************BOT�N EDITAR VENTA/RESERVA  *********************************/
$("#button_clean_cliente").on("click", function(e) {
	$('input[name="nombre"]').val('');
	$('input[name="codigocliente"]').val('');
	$('input[name="apellido1"]').val('');
	$('input[name="apellido2"]').val('');
	$('input[name="dni"]').val('');
	$('input[name="telefono"]').val('');
	$('input[name="fax"]').val('');
	$('input[name="localidad"]').val('');
	$('input[name="cp"]').val('');
	$('input[name="direccion"]').val('');
	
})




</script>

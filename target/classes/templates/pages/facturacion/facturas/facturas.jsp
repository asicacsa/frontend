<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!-- Pesta�as ------------------------------------------------------------->
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 maintab">
		<div class="" role="tabpanel" data-example-id="togglable-tabs">
			<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
				<li role="presentation" class="active">
					<a href="#tab_clientes" id="clientes-tab" role="tab" data-toggle="tab" aria-expanded="true">
					<spring:message	code="facturacion.facturas.tabs.clientes.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_tiposcliente" role="tab" id="tiposcliente-tab" data-toggle="tab" aria-expanded="false">
					<spring:message	code="facturacion.facturas.tabs.tipocliente.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_grupos"	role="tab" id="grupos-tab" data-toggle="tab" aria-expanded="false">
					<spring:message	code="facturacion.facturas.tabs.grupos.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_gestionfacturas" role="tab" id="gestionfacturas-tab" data-toggle="tab"	aria-expanded="false">
					<spring:message	code="facturacion.facturas.tabs.gestionfacturas.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_generacionfacturas"	role="tab" id="generacionfacturas-tab" data-toggle="tab" aria-expanded="false">
					<spring:message	code="facturacion.facturas.tabs.generacionfacturas.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_series"	role="tab" id="series-tab" data-toggle="tab" aria-expanded="false">
					<spring:message	code="facturacion.facturas.tabs.series.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_periodos" role="tab" id="periodos-tab" data-toggle="tab" aria-expanded="false">
					<spring:message	code="facturacion.facturas.tabs.periodos.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_iva"	role="tab" id="iva-tab" data-toggle="tab" aria-expanded="false">
					<spring:message	code="facturacion.facturas.tabs.iva.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_royalties"	role="tab" id="royalties-tab" data-toggle="tab" aria-expanded="false">
					<spring:message code="facturacion.facturas.tabs.royalties.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_rappels"	role="tab" id="rappels-tab" data-toggle="tab" aria-expanded="false">
					<spring:message code="facturacion.facturas.tabs.rappels.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_correo"	role="tab" id="correo-tab" data-toggle="tab" aria-expanded="false">
					<spring:message code="facturacion.facturas.tabs.correo.title" /></a></li>	
			</ul>
		</div>
	</div>
</div>

<!-- Area de trabajo ------------------------------------------------------>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel thin_padding">
			<div id="myTabContent" class="tab-content">				
				<div role="tabpanel" class="tab-pane fade active in" id="tab_clientes" aria-labelledby="clientes-tab">					
						<tiles:insertAttribute name="tab_clientes" />
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab_tiposcliente" aria-labelledby="tiposcliente-tab">
						<tiles:insertAttribute name="tab_tiposcliente" />
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab_grupos" aria-labelledby="grupos-tab">				
						<tiles:insertAttribute name="tab_grupos" />
				</div>				
				<div role="tabpanel" class="tab-pane fade" id="tab_gestionfacturas" aria-labelledby="generacionfacturas-tab">
					<tiles:insertAttribute name="tab_gestionfacturas" />
					 
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab_generacionfacturas" aria-labelledby="generacionfacturas-tab">
					<tiles:insertAttribute name="tab_generacionfacturas" />			
					  
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab_series" aria-labelledby="series-tab">
					<tiles:insertAttribute name="tab_series" />					
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab_periodos" aria-labelledby="periodos-tab">
					<tiles:insertAttribute name="tab_periodos" />					
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab_iva" aria-labelledby="iva-tab">
					<tiles:insertAttribute name="tab_iva" />			
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab_royalties" aria-labelledby="royalties-tab">
					<tiles:insertAttribute name="tab_royalties" />					
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab_rappels" aria-labelledby="rappels-tab">
					<tiles:insertAttribute name="tab_rappels" />				
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab_correo" aria-labelledby="correo-tab">
					<tiles:insertAttribute name="tab_correo" />									
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-2"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-3"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-4"/>
<tiles:insertAttribute name="modal_dialog" />

<tiles:insertAttribute name="modal_porcentajes" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-buscar-cliente"/>
<tiles:insertAttribute name="modal_dialog" />

<tiles:insertAttribute name="modal_enviar_factura" />



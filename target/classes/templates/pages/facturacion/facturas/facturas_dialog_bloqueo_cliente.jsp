<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${historico_bloqueos}" var="historico_bloqueos_xml" />
<x:parse xml="${motivos_bloqueo}" var="motivos_bloqueo_xml" />



<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">
	     <c:if test="${bloqueado eq '0'}">
			<spring:message code="facturacion.facturas.tabs.clientes.dialog.list_bloqueos.bloquear.title" />
		 </c:if>
		 <c:if test="${bloqueado eq '1'}">
			<spring:message code="facturacion.facturas.tabs.clientes.dialog.list_bloqueos.desbloquear.title" />
		 </c:if>		 
	</h4>	
</div>

<div class="modal-body">
			<div class="col-md-12 col-sm-12 col-xs-12">
			 <c:if test="${bloqueado eq '0'}">
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.clientes.bloqueo.field.motivos" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="motivos_bloqueo" id="motivos_bloqueo" class="form-control">
						<option value=""> </option>
						<x:forEach select="$motivos_bloqueo_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>					
				</div>				
			</div>
			
			<br/><br/>
			</c:if>
			</div>
			
			

					<table id="bloqueos_usuarios_table" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
							  <th id="motivo"><spring:message code="facturacion.facturas.tabs.clientes.dialog.list_bloqueos.field.motivo"/></th>
							  <th id="bloqueo"><spring:message code="facturacion.facturas.tabs.clientes.dialog.list_bloqueos.field.bloqueo"/></th>
							  <th id="desbloqueo"><spring:message code="facturacion.facturas.tabs.clientes.dialog.list_bloqueos.field.desbloqueo" /></th>							  							  						 
							</tr>
						</thead>
						<tbody>				
							<x:forEach select="$historico_bloqueos_xml/ArrayList/Bloqueocliente" var="item">
							   <tr class="seleccionable">
							   		<td id="motivo" ><x:out select="$item/motivobloqueo" /></td>
							   		<td id="bloqueo" ><x:out select="$item/fechabloqueo" /> - <x:out select="$item/usuario/nombre" /> <x:out select="$item/usuario/apellidos" /></td>
							   		<td id="desbloqueo" ><x:out select="$item/fechadesbloqueo" /></td>																										
								</tr>																
							</x:forEach>
						</tbody>				
					</table>				


	<div class="modal-footer">
	
		<button id="save_cliente_button" type="button" class="btn btn-primary close_bloqueo_cliente">
			<spring:message code="common.button.accept" />
		</button>	
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>		

</div>

<script>

$('#bloqueos_usuarios_table').DataTable( {
	"paging": false,
	"info": false,
	"searching": false,
	select: { style: 'os'},
	language: dataTableLanguage	 
	});

$( ".close_bloqueo_cliente" ).on("click", function(e) {	
	var motivo =  $("#motivos_bloqueo option:selected").text();
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/facturacion/facturas/cliente/desbloquear.do'/>?bloqueado=${bloqueado}&id=${idCliente}&motivo="+encodeURI(motivo),
		timeout : 100000,
		data: {  }, 
		success : function(data) {
			if(dt_listclientes.rows( '.selected' ).data()[0].bloqueado == 0)
				dt_listclientes.rows( '.selected' ).data()[0].bloqueado = 1
			else
				dt_listclientes.rows( '.selected' ).data()[0].bloqueado = 0;
			dt_listclientes.draw( false );
			 
			//Actualizamos la tabla sin cambiar la p�gina actual
			$("#modal-dialog-form").modal('hide');	
			
			
			
		},
		error : function(exception) {
			dt_listclientes.processing(false);
			
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
})


</script>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${tipos_serie_rappel}" var="tipos_serie_rappel_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="facturacion.facturas.tabs.rappels.dialog.definir_periodos_rappel.title" />
	</h4>	
</div>

<div class="modal-body">
	<form id="form_generar_factura_rappel_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input id="idcliente_rappel" name="idcliente_rappel" type="hidden" value="" />
		<div class="col-md-12 col-sm-12 col-xs-12">		
		  <div class="form-group button-dialog">
           <label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="administracion.productos.tabs.descuentospromo.field.cliente" /></label>
           	<div class="col-md-9 col-sm-9 col-xs-9">
               	<input name="cliente_rappel" id="cliente_rappel" type="text" class="form-control" readonly value="">
              </div>
            </div>			
	  	<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.rappels.field.serie" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<select name="idserie" id="selector_serie" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$tipos_serie_rappel_xml/ArrayList/Tipofactura" var="item">
							<option value="<x:out select="$item/idtipofactura" />"><x:out select="$item/nombre" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			<div class="form-group date-picker">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="facturacion.facturas.tabs.rappels.field.periodo" />:</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<a type="button" class="btn btn-default btn-clear-date" id="button_periodogeneracion_clear">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="facturacion.facturas.tabs.rappels.list.button.clear" />"> <span class="fa fa-trash"></span>
						</span>
					</a>			
                    <div class="input-prepend input-group">
                    	<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                        <input type="text" name="periodogeneracion" id="periodogeneracion" class="form-control" value="" readonly/>
                        <input type="hidden" required="required" name="fechainicioperiodogeneracion" value=""/>
                        <input type="hidden" required="required" name="fechafinperiodogeneracion" value=""/>
                    </div>
				</div>
			</div>			
      
        		
		
		</div>
	</form>
	<div class="modal-footer">
		<button id="save_generar_rappel_button" type="button" class="btn btn-primary save_generar_rappel">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>	


<script>
hideSpinner("#tab_rappels_generacion_facturas");
//******************************************************************
$("#cliente_rappel").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>", "#cliente_rappel", "#idcliente_rappel");
//******************************************************************

//Fecha fin/inicio periodo
var dia_inicio = $today;
var dia_fin = $today;

if($('input[name="fechainicioperiodogeneracion"]').val()!='')
{	
dia_inicio = $('input[name="fechainicioperiodogeneracion"]').val().substring(0, 10);
}
if($('input[name="fechafinperiodogeneracion"]').val()!='')
{	
dia_fin = $('input[name="fechafinperiodogeneracion"]').val().substring(0, 10);
}


$('input[name="periodogeneracion"]').val( dia_inicio + ' - ' + dia_fin);
$('input[name="fechainicioperiodogeneracion"]').val( dia_inicio);
$('input[name="fechafinperiodogeneracion"]').val( dia_fin);


$('input[name="periodogeneracion"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
  	  	 $('input[name="periodogeneracion"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('input[name="fechainicioperiodogeneracion"]').val(start.format('DD/MM/YYYY'));  	  	 
  	  	 $('input[name="fechafinperiodogeneracion"]').val(end.format('DD/MM/YYYY'));
	 });

$("#button_periodogeneracion_clear").on("click", function(e) {
    $('input[name="periodogeneracion"]').val('');
    $('input[name="fechainicioperiodogeneracion"]').val('');
    $('input[name="fechafinperiodogeneracion"]').val('');
});


//******************************GRABAR GENERACION*************************************
$(".save_generar_rappel").on("click", function(e) { 
	var data = $("#form_generar_factura_rappel_data").serializeObject();
	showSpinner("#modal-dialog-form .modal-content");

	$.ajax({
		type : "post",
		url : "<c:url value='/ajax/facturacion/facturas/rappels/save_generar_factura_rappel.do'/>",
		timeout : 100000,
		data : data,
		success : function(data) {
			hideSpinner("#modal-dialog-form .modal-content");
			$("#modal-dialog-form").modal('hide');				
			
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
			
		},
		error : function(exception) {
			hideSpinner("#modal-dialog-form .modal-content");

			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : exception.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		}
	});
		
})
//***************************************************************************************
</script>
	
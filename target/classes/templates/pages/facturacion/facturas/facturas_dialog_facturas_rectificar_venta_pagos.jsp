<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${motivosModificacion}" var="motivosModificacion_xml" />
<x:parse xml="${selectorFormasdePagoPorCanal}" var="selectorFormasdePagoPorCanal_xml" />
<x:parse xml="${importesParciales}" var="importesParciales_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">	
				<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.entradas.anular.venta.title" /> 
	</h4>	
</div>

<div class="modal-body">
	<form id="form_rectificar_lineas_pagos" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input type="hidden" name="id_anulacion" id="id_anulacion" value="${id}" />
		<input type="hidden" name="xml_entradas" id="xml_entradas_anulacion" />
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
				<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.motivos" /></label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<select name="selector_motivos_anulacion" id="selector_motivos_anulacion" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$motivosModificacion_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>	
			<div class="form-group">
				<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.observaciones" /></label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<textarea name="observacion_anulacion" id="observacion_anulacion" class="form-control"  ></textarea>
				</div>
			</div>				
		</div>
		<div id="grupo-datosimportes">	
			<div class="x_title">
				<div class="clearfix"></div>
			</div>		
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.importetotalventa" /></label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input name="importetotalventa_anulacion" id="importetotalventa_anulacion" type="text" disabled="disabled" class="form-control" value="<x:out select = "$importeTotal" />" >
					</div>
				</div>
				<div class="form-group">
					<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.importetotalpagado" /></label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input name="importetotalpagado_anulacion" id="importetotalpagado_anulacion" type="text" disabled="disabled" class="form-control" value="<x:out select = "$importePagado" />">						
					</div>
				</div>	
				<div class="form-group" id="diferencia" name="diferencia">
					<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.diferencia" /></label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input name="diferencia_anulacion" id="diferencia_anulacion" type="text" disabled="disabled" class="form-control" value="<x:out select = "$diferencia" />">						
					</div>
				</div>		
			</div>
		</div>
		<div id="grupo-datoscobro">	
			<div class="x_title">
				<div class="clearfix"></div>
			</div>		
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
						<div class="checkbox col-md-12 col-sm-12 col-xs-12">
							<input type="checkbox" name="devolucion_cobro" id="devolucion_cobro" value="" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.devolucion_cobro" /></strong>
						</div>
					
					<div class="form-group " id="operacion_anulacion_titulo">
						<label  class="control-label col-md-3 col-sm-3 col-xs-12"><strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.operacion" /></strong></label>
						<label  class="control-label col-md-3 col-sm-3 col-xs-12"><strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.formasdepago" /></strong></label>
						<label  class="control-label col-md-3 col-sm-3 col-xs-12"><strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.importe" /></strong></label>		
					</div>
					<div class="form-group " id="operacion_anulacion_1">
						<div class="col-md-3 col-sm-3 col-xs-12">
							<select name="selector_operacion1" id="selector_operacion1" class="form-control" >
								<option value="0"></option>
								<option value="1"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.cobro" /></option>
								<option value="2"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.devolucion" /></option>								
							</select>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<select name="selector_forma_pago1" id="selector_forma_pago1" class="form-control" >
								<option value=""></option>
									<x:forEach select="$selectorFormasdePagoPorCanal_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
							</select>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input name="importe1" id="importe1" type="text" class="form-control" value="" >							 
						</div>		
						<div class="col-md-1 col-sm-1 col-xs-12">
							<label  class="control-label"><strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.euro" /></strong></label>							 
						</div>
						<div class="col-md-1 col-sm-1 col-xs-12">
							<a type="button" class="btn btn-default btn-new-operacion1" id="button_new_operacion1">
									<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.list.button.nueva_devolucion" />"> <span class="fa fa-plus"></span>
									</span>
							  </a>
						</div>
					</div>
					<div class="form-group " id="operacion_anulacion_2">
							<div class="col-md-3 col-sm-3 col-xs-12">
							<select name="selector_operacion2" id="selector_operacion2" class="form-control" >
								<option value="0"></option>
								<option value="1"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.cobro" /></option>
								<option value="2"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.devolucion" /></option>								
							</select>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<select name="selector_forma_pago2" id="selector_forma_pago2" class="form-control" >
								<option value=""></option>
									<x:forEach select="$selectorFormasdePagoPorCanal_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
							</select>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input name="importe2" id="importe2" type="text"  class="form-control" value="" >							 
						</div>		
						<div class="col-md-1 col-sm-1 col-xs-12">
							<label  class="control-label"><strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.euro" /></strong></label>							 
						</div>
						<div class="col-md-1 col-sm-1 col-xs-12">
							<a type="button" class="btn btn-default btn-new-operacion2" id="button_new_operacion2">
									<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.list.button.nueva_devolucion" />"> <span class="fa fa-plus"></span>
									</span>
							  </a>
						</div>		
					</div>
					<div class="form-group " id="operacion_anulacion_3">
						<div class="col-md-3 col-sm-3 col-xs-12">
							<select name="selector_operacion3" id="selector_operacion3" class="form-control" >
								<option value="0"></option>
								<option value="1"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.cobro" /></option>
								<option value="2"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.devolucion" /></option>								
							</select>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<select name="selector_forma_pago3" id="selector_forma_pago3" class="form-control" >
								<option value=""></option>
									<x:forEach select="$selectorFormasdePagoPorCanal_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
							</select>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input name="importe3" id="importe3" type="text" class="form-control" value="" >							 
						</div>		
						<div class="col-md-1 col-sm-1 col-xs-12">
							<label  class="control-label"><strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.euro" /></strong></label>							 
						</div>
									
					</div>
				</div>				
			</div>
		</div>
			<div >
				<div class="clearfix"></div>
			</div>		
			<div id="checks" class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<div class="checkbox col-md-2 col-sm-2 col-xs-12">
						<input type="checkbox" name="recibo_anular" id="recibo_anular" value="" class="flat" style="position: absolute; opacity: 0;">
						<strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.recibo" /></strong>
					</div>
				
					<div class="checkbox col-md-2 col-sm-2 col-xs-12">
						<input type="checkbox" name="copia_anular" id="copia_anular" value="" class="flat" style="position: absolute; opacity: 0;">
						<strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.copia" /></strong>
					</div>
				
					<div class="checkbox col-md-2 col-sm-2 col-xs-12">
						<input type="checkbox" name="entradas_anular" id="entradas_anular" value="" class="flat" style="position: absolute; opacity: 0;">
						<strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.entradas" /></strong>
					</div>
				
					<div class="checkbox col-md-4 col-sm-4 col-xs-12">
						<input type="checkbox" name="entradas_defecto" id="entradas_defecto" value="" class="flat" style="position: absolute; opacity: 0;">
						<strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.estradas_defecto" /></strong>
					</div>
				</div>
			</div>

	<div class="modal-footer">
		<button id="ver_formas_pago_button" type="button" class="btn btn-primary ver_formas_pago_dialog">
			<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.list.button.ver_formas_pago" />
		</button>	
		<button id="actualizarCamposLineas_button" type="button" class="btn btn-primary anular_venta_reserva_dialog">
					<spring:message code="common.button.accept" />
				</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>		
	</form>
</div>
<script>

hideSpinner("#tab_entradas_anular");


$("#operacion_anulacion_titulo").hide();
$("#operacion_anulacion_1").hide();
$("#operacion_anulacion_2").hide();
$("#operacion_anulacion_3").hide();
//$("#diferencia").hide();


$('input[name="copia_anular"]').iCheck('disable');
var importe= parseFloat(($("#diferencia_anulacion").val()).replace(",","."));

if(importe<=0)
	$("#selector_operacion1 option[value=2]").attr("selected",true);
else
	$("#selector_operacion1 option[value=1]").attr("selected",true);


$('input[name="importe1"]').val(importe);

$("#selector_operacion2 option[value=0]").attr("selected",true);
$("#selector_operacion3 option[value=0]").attr("selected",true);


//********************************************************
$('#importe1').inputmask({alias: 'numeric', 
                       allowMinus: false,  
                       digits: 2, 
                       max: 999.99});

//*****************************************************
$('#importe2').inputmask({alias: 'numeric', 
                       allowMinus: false,  
                       digits: 2, 
                       max: 999.99});
//*****************************************************
$('#importe3').inputmask({alias: 'numeric', 
                       allowMinus: false,  
                       digits: 2, 
                       max: 999.99});
//****************************************************
if ($("input.flat")[0]) {
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });
}
//****************************************************
 
 var contador=0;
 var tipo_forma_pago;
 
 
<x:forEach select="$importesParciales_xml/ArrayList/Importeparcial" var="item">
	contador=contador+1;
	tipo_forma_pago= '<x:out select="$item/formapago/idformapago" />';
</x:forEach> 
if (contador==1)
	$("#selector_forma_pago1 option[value="+tipo_forma_pago+"]").attr("selected",true);

//****************************************************
  $("#devolucion_cobro").on("ifChanged", function(e) {
    	if ($('#operacion_anulacion_titulo').is(':hidden')){ 
    		$("#operacion_anulacion_titulo").show();
    		$("#operacion_anulacion_1").show();
    	}
        else {
        	$("#operacion_anulacion_titulo").hide();
        	$("#operacion_anulacion_1").hide();
        	$("#operacion_anulacion_2").hide();
        	$("#operacion_anulacion_3").hide();
        }
    });
 //***************************************************
 $("#button_new_operacion1").on("click", function(e) {
	 var diferencia = parseFloat($("#diferencia_anulacion").val().replace(",","."));
     
     var importe1 = parseFloat($("#importe1").val());
          
     var importe2;
     
     if ($('select[name=selector_operacion1]').val()=="2")
    	 importe2 = diferencia+importe1;
     else
    	 importe2 = diferencia-importe1;
     

	 $("#importe2").val(importe2);
	
	 if (importe2> 0)
	 	 $("#selector_operacion2 option[value=1]").attr("selected",true);	
	 else 	 
	 	 $("#selector_operacion2 option[value=2]").attr("selected",true);
	 
	
	 $("#operacion_anulacion_2").show();
});

//****************************************************
$("#button_new_operacion2").on("click", function(e) {
	
	 var diferencia = parseFloat($("#diferencia_anulacion").val().replace(",","."));
     
     var importe1 = parseFloat($("#importe1").val());
          
     var importe2 = parseFloat($("#importe2").val());
	
	 //Devolución
	if ($('select[name=selector_operacion1]').val()=="1")
		importe1=(-1.0) * importe1;	
	
	if ($('select[name=selector_operacion2]').val()=="1")
		importe2=(-1.0) * importe2;
	
	resul = diferencia+importe1+importe2;
	
	$("#importe3").val(resul);
	
	if (resul>0)
		$("#selector_operacion3 option[value=2]").attr("selected",true);
	else
		$("#selector_operacion3 option[value=1]").attr("selected",true);
	
	$("#operacion_anulacion_3").show();
	 
});
//****************************************************
$("#recibo_anular").on("ifChanged", function(e) {
	if ($('input[name="copia_anular"]').attr('disabled')) 		
		$('input[name="copia_anular"]').iCheck('enable');
	else{
		$('input[name="copia_anular"]').iCheck('disable');
		$('input[name="copia_anular"]').iCheck('uncheck');
	}		
    });
//****************************************************
$("#ver_formas_pago_button").on("click", function(e) {
	var data = jsonVenta.idventa[0];
	showButtonSpinner("#ver_formas_pago_button");
	$("#modal-dialog-form-4 .modal-content").load("<c:url value='/venta/ventareserva/busqueda/entrada/ver_formas_pago.do'/>?id=${id}", function() {
		$("#modal-dialog-form-4").modal('show');
		setModalDialogSize("#modal-dialog-form-4", "xs");
	});
	
	
});

$("#actualizarCamposLineas_button").on("click", function(e) {
	$("#form_rectificar_lineas_pagos").submit();		
})




//****************************************************
$("#form_rectificar_lineas_pagos").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		comprobarTipoFormaPago_no_vacio()	
	}
}
);




//****************************************************
function actualizarModificacion(){	
		
		$("#modal-dialog-form-3").modal('hide');
		$("#modal-dialog-form-2").modal('hide');
		var xml;
		xml="<Modificacion>";
		xml = xml+"<idmodificacion/>";
		xml = xml+"<motivomodificacion>"+$("#selector_motivos_anulacion option:selected").text()+"</motivomodificacion>";
		xml = xml+"<observaciones>"+$("#observacion_anulacion").val()+"</observaciones>";
		xml = xml+"<venta>";
			xml = xml+"<idventa>${id}</idventa>";
			var entr=false;
			if(	$("#entradas_anular").is(':checked'))
				entr=true;
			xml = xml+"<entradasimpresas>"+entr+"</entradasimpresas>";
			
			var valor;
			if(	$("#recibo_anular").is(':checked')){
				if ($("#copia_anular").is(':checked')){
					valor=2;				
				}else valor=1
			}else valor=0;
				
			xml = xml+"<reciboimpreso>"+valor+"</reciboimpreso>";		
			var entr_def=false;
			if(	$("#entradas_defecto").is(':checked'))
				entr_def=true;
			xml = xml+"<imprimirSoloEntradasXDefecto>"+entr_def+"</imprimirSoloEntradasXDefecto>";		
		xml = xml+"</venta>";
		
		xml = xml+"<reserva>";
			xml = xml+"<idreserva/>";
		xml = xml+"</reserva>";
		xml = xml+"<modificacionimporteparcials>";
		if(	$("#devolucion_cobro").is(':checked')){
			xml = xml+"<Modificacionimporteparcial>";
			xml = xml+"<importeparcial>";
				xml = xml+"<formapago>";
					xml = xml+"<idformapago>"+$("#selector_forma_pago1").val()+"</idformapago>";
				xml = xml+"</formapago>";
				xml = xml+"<bono>";
					xml = xml+"<numeracion/>";
				xml = xml+"</bono>";				
				if ($("#selector_operacion1").val()==2)
					xml = xml+"<importe>"+"-"+$("#importe1").val()+"</importe>";
				else if($("#selector_operacion1").val()==1)
					xml = xml+"<importe>"+$("#importe1").val()+"</importe>";
				else
					if($("#selector_forma_pago1").val()!=0)
						xml = xml+"<importe>NaN</importe>";	
				xml = xml+"<rts/>";
				xml = xml+"<numpedido/>";
				xml = xml+"<anulado/>";
			xml = xml+"</importeparcial>";
		xml = xml+"</Modificacionimporteparcial>";
		xml = xml+"<Modificacionimporteparcial>";
			xml = xml+"<importeparcial>";
				xml = xml+"<formapago>";
					xml = xml+"<idformapago>"+$("#selector_forma_pago2").val()+"</idformapago>";
				xml = xml+"</formapago>";
				xml = xml+"<bono>";
					xml = xml+"<numeracion/>";
				xml = xml+"</bono>";
				if ($("#selector_operacion2").val()==2)
					xml = xml+"<importe>"+"-"+$("#importe2").val()+"</importe>";
				else if($("#selector_operacion2").val()==1)
					xml = xml+"<importe>"+$("#importe2").val()+"</importe>";
				else
					if($("#selector_forma_pago2").val()!=0)
						xml = xml+"<importe>NaN</importe>";
				xml = xml+"<rts/>";
				xml = xml+"<numpedido/>";
				xml = xml+"<anulado/>";
			xml = xml+"</importeparcial>";
		xml = xml+"</Modificacionimporteparcial>";
		xml = xml+"<Modificacionimporteparcial>";
			xml = xml+"<importeparcial>";
				xml = xml+"<formapago>";
					xml = xml+"<idformapago>"+$("#selector_forma_pago3").val()+"</idformapago>";
				xml = xml+"</formapago>";
				xml = xml+"<bono>";
					xml = xml+"<numeracion/>";
				xml = xml+"</bono>";
				if ($("#selector_operacion3").val()==2)
					xml = xml+"<importe>"+"-"+$("#importe3").val()+"</importe>";
				else if($("#selector_operacion3").val()==1)
					xml = xml+"<importe>"+$("#importe3").val()+"</importe>";
				else
					if($("#selector_forma_pago3").val()!=0)
						xml = xml+"<importe>NaN</importe>";
				xml = xml+"<rts/>";
				xml = xml+"<numpedido/>";
				xml = xml+"<anulado/>";
			xml = xml+"</importeparcial>";
		xml = xml+"</Modificacionimporteparcial>";
	xml = xml+"</modificacionimporteparcials>";
		}else
		{
			xml = xml+"<Modificacionimporteparcial>";
			xml = xml+"<importeparcial>";
				xml = xml+"<formapago>";
					xml = xml+"<idformapago></idformapago>";
				xml = xml+"</formapago>";
				xml = xml+"<bono>";
					xml = xml+"<numeracion/>";
				xml = xml+"</bono>";
				xml = xml+"<importe></importe>";
				xml = xml+"<rts/>";
				xml = xml+"<numpedido/>";
				xml = xml+"<anulado/>";
			xml = xml+"</importeparcial>";
		xml = xml+"</Modificacionimporteparcial>";
		xml = xml+"<Modificacionimporteparcial>";
			xml = xml+"<importeparcial>";
				xml = xml+"<formapago>";
					xml = xml+"<idformapago></idformapago>";
				xml = xml+"</formapago>";
				xml = xml+"<bono>";
					xml = xml+"<numeracion/>";
				xml = xml+"</bono>";
				xml = xml+"<importe></importe>";
				xml = xml+"<rts/>";
				xml = xml+"<numpedido/>";
				xml = xml+"<anulado/>";
			xml = xml+"</importeparcial>";
		xml = xml+"</Modificacionimporteparcial>";
		xml = xml+"<Modificacionimporteparcial>";
			xml = xml+"<importeparcial>";
				xml = xml+"<formapago>";
					xml = xml+"<idformapago></idformapago>";
				xml = xml+"</formapago>";
				xml = xml+"<bono>";
					xml = xml+"<numeracion/>";
				xml = xml+"</bono>";
				xml = xml+"<importe></importe>";
				xml = xml+"<rts/>";
				xml = xml+"<numpedido/>";
				xml = xml+"<anulado/>";
			xml = xml+"</importeparcial>";
		xml = xml+"</Modificacionimporteparcial>";
	xml = xml+"</modificacionimporteparcials>";
		}
			xml+="<modificacionlineadetalles>";
			
			
			var lineaDetalles = dt_listventaindividual.rows().data();
			
			var idsLineas = "";
			var productos= "";
			var fechas = "";
			var cantidades = "";
			var perfiles= "";
			var descuentos="";
			var importes="";
			
			var retorno = false;

			if(lineaDetalles.length>1)
			{
				var arrayLineasDetalles = new Array();
				$.each(lineaDetalles, function(key, fila){
					lineaDetalle = fila[0];
					if(retorno)
						{
						idsLineas+="</br/>";
						fechas+="</br/>";
						cantidades+="</br/>";
						perfiles+="</br/>";
						productos+="</br/>";
						descuentos+="</br/>";
						importes+="</br/>";
						}
					idsLineas+=lineaDetalle.idlineadetalle;
					fechas+=calcularSesionLinea(lineaDetalle,retorno);
					cantidades+=lineaDetalle.cantidad;
					perfiles+=lineaDetalle.perfilvisitante.tarifa.nombre;
					productos+=lineaDetalle.producto.nombre;
					
					if (lineaDetalle.descuento.porcentajedescuento !="")
						descuentos+=lineaDetalle.descuento.porcentajedescuento  ;					 

					importes+=lineaDetalle.importe;
					retorno = true;
					arrayLineasDetalles.push(lineaDetalle);
					xml+="<Modificacionlineadetalle>";
					xml+="<tipomodificacion>0</tipomodificacion>";
					xml+="<lineadetalle>"+json2xml(lineaDetalle)+"</lineadetalle>";
					xml+="</Modificacionlineadetalle>";
	            });
				
				dtlistadodetalles.rows( '.selected' ).data()[0][0].lineadetalles.Lineadetalle= arrayLineasDetalles;				
			}
			else
			{
				var lineaDetalle = dt_listventaindividual.rows().data()[0][0];
				idsLineas+=lineaDetalle.idlineadetalle;
				fechas=calcularSesionLinea(lineaDetalle,retorno);
				cantidades=lineaDetalle.cantidad;
				perfiles+=lineaDetalle.perfilvisitante.tarifa.nombre;
				productos+=lineaDetalle.producto.nombre;
				descuentos+=lineaDetalle.descuento.porcentajedescuento  ;
				importes+=lineaDetalle.importe;
				dtlistadodetalles.rows( '.selected' ).data()[0][0].lineadetalles.Lineadetalle=lineaDetalle;
				xml+="<Modificacionlineadetalle>";
				xml+="<tipomodificacion>0</tipomodificacion>";
				xml+="<lineadetalle>"+json2xml(lineaDetalle)+"</lineadetalle>";
				xml+="</Modificacionlineadetalle>";
			}

	
			dtlistadodetalles.rows( '.selected' ).data()[0][2]=idsLineas;
			dtlistadodetalles.rows( '.selected' ).data()[0][3]=productos;
	        retorno = false;
			dtlistadodetalles.rows( '.selected' ).data()[0][4]= fechas;
			
		
					
			dtlistadodetalles.rows( '.selected' ).data()[0][5]=cantidades;
			dtlistadodetalles.rows( '.selected' ).data()[0][6]=perfiles;
			dtlistadodetalles.rows( '.selected' ).data()[0][7]=descuentos;
			dtlistadodetalles.rows( '.selected' ).data()[0][8]=importes;
			dtlistadodetalles.rows( '.selected' ).data()[0][12]="1";
			//dtlistadodetalles.rows( '.selected' ).data()[0][9]=importeVenta;
					
			dtlistadodetalles.rows().invalidate().draw();	
			
			xml+="</modificacionlineadetalles>";
			xml = xml+"<anulaciondesdecaja/>";
			xml = xml+"</Modificacion>";
	  		$("#xml_anulacion").val(xml);
     		
			
	}
//*****************************************************
function comprobarTipoFormaPago_no_vacio(){	
	if(	$("#devolucion_cobro").is(':checked')){
		if ($("#importe1").val()==""){
			if($("#selector_forma_pago1").val()!=0){
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : '<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.text.campos_obligatorios_importe" />',
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				return false;
			}
		}else
		{
			if($("#selector_forma_pago1").val()==0){
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : '<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.text.campos_obligatorios_forma_pago" />',
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				return false;
			}
		}
		
		if ($("#importe2").val()==""){
			if($("#selector_forma_pago2").val()!=0){
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : '<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.text.campos_obligatorios_importe" />',
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				return false;
			}
		}else
		{
			if($("#selector_forma_pago2").val()==0){
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : '<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.text.campos_obligatorios_forma_pago" />',
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				return false;
			}
		}
		
		if ($("#importe3").val()==""){
			if($("#selector_forma_pago3").val()!=0){
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : '<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.text.campos_obligatorios_importe" />',
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				return false;
			}
		}else
		{
			if($("#selector_forma_pago3").val()==0){
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : '<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.text.campos_obligatorios_forma_pago" />',
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				return false;
			}
		}		
		
		  actualizarModificacion();
	}else{		
		actualizarModificacion();
		}
	}
</script>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${empresas_grupo}" var="empresas_grupo_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="facturacion.facturas.tabs.clientes.dialog.list_empresas_grupo.title" />		
	</h4>	
</div>

<div class="modal-body">

					<table id="empresasHistorico_table" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
							  <th id="cliente"><spring:message code="facturacion.facturas.tabs.clientes.dialog.list_empresas_grupo.field.empresa"/></th>
							  <th id="dni"><spring:message code="facturacion.facturas.tabs.clientes.dialog.list_empresas_grupo.field.dni"/></th>
							  <th id="falta"><spring:message code="facturacion.facturas.tabs.clientes.dialog.list_empresas_grupo.field.falta" /></th>
							  <th id="fbaja"><spring:message code="facturacion.facturas.tabs.clientes.dialog.list_empresas_grupo.field.fbaja" /></th>							  						 
							</tr>
						</thead>
						<tbody>				
							<x:forEach select="$empresas_grupo_xml/ArrayList/Empresagrupo" var="item">
							   <tr class="seleccionable">
							   		<td id="cliente" ><x:out select="$item/cliente/nombre" /></td>
							   		<td id="dni" ><x:out select="$item/cliente/identificador" /></td>
							   		<td id="falta" ><x:out select="$item/fechaincorporacion" /></td>
									<td id="fbaja"><x:out select="$item/fechabaja" /></td>																	
								</tr>																
							</x:forEach>
						</tbody>				
					</table>				


	<div class="modal-footer">
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>		

</div>

<script>

</script>

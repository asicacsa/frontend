<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${motivos_rectificacion}" var="motivos_rectificacion_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	 <h4 class="modal-title">	
		<spring:message code="facturacion.facturas.tabs.facturas.dialog.confirmar_rectificacion.title" />				
	</h4>	
</div>

<div class="modal-body" >
	<form id="form_motivos_rectificacion" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-12"><spring:message code="facturacion.facturas.tabs.facturas.dialog.rectificar_factura.rectificar.texto" />*</label>				
					<div class="col-md-6 col-sm-6 col-xs-6">
						<select name="idMotivoRectificacion" id="idMotivoRectificacion"  class="form-control" required="required">
							<option> </option>
							<x:forEach select="$motivos_rectificacion_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
						</select>
						<br/>
						</div>
					</div>
					<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<label class="control-label col-md-5 col-sm-5 col-xs-12"><spring:message code="facturacion.facturas.tabs.facturas.dialog.anular_factura.motivo.otros" /></label>				
						<div class="col-md-6 col-sm-6 col-xs-12">
							<textarea id="otros_rectificacion" name="otros" enabled="true"></textarea>
						</div>
						</div>
					</div>						
		</form>
		<div class="modal-footer">
			<button id="rectificar_factura_button" type="button" class="btn btn-primary save_modificar_dialog">
				<spring:message code="common.button.save" />
			</button>
			<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
				<spring:message code="common.button.cancel" />
			</button>
		</div>
	</div>
<script>

hideSpinner("#tab_gestion_factura_anular");

if ($("input.flat")[0]) {
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });
}


$("#rectificar_factura_button").on("click", function(e) {
	RectificarFactura();
})

function RectificarFactura()
{
	
	 
	var motivo = $("#idMotivoRectificacion").val();
	$("#motivo").val(motivo);
	var otros = $("#otros_rectificacion").val();
	$("#otros").val(otros);
	tipo_rectificacion = $("input[name='tipo_rectificacion']:checked").val();
	
	if(tipo_rectificacion=="3")
		{
		construir_xml_rectificaryRectificar();
		}
	else	
	{
	$("#listIds").val("");
    
     var data = dtlistadoventas.rows().data();
	 var listIds = "<list>";      
	 data.each(function (value, index) {
		 var linea = dtlistadoventas.rows( index ).data()[0];
		 var idVenta = linea[0];
		 var idCliente = linea[1];
		 var facturada = linea[3];	
		 var fecha = linea[4];
		 var importe = linea[5];
		 venta ="<Venta><idventa>"+idVenta+"</idventa>";
		 venta +="<fechayhoraventa>"+fecha+"</fechayhoraventa>"
		 venta +="<importetotalventa>"+importe+"</importetotalventa>"
		 venta +="<importeparcialtotalventa>"+importe+"</importeparcialtotalventa>"
		 venta +="<cliente><idcliente>"+idCliente+"</idcliente></cliente>";
		 venta +="<facturada>"+facturada+"</facturada>";
		 venta +="</Venta>";
		 listIds+=venta;		 
	 })
	 
	 listIds +="</list>";
	 var tipo_rectificacion = $('input:radio[name=tipo_rectificacion]:checked').val();
	 
	 if(tipo_rectificacion=="2")
		 {
		 $("#listIds").val(listIds);
		 }
	
	var data = $("#form_factura_rectificar_data").serializeObject();
	
	showSpinner("#capa_rectificar");
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/facturacion/factura_rectificar.do'/>",
		timeout : 100000,
		data: data, 
		success : function(data) {
			
			var xml = json2xml(data.ArrayList,''); 	
			
			
			var url = "<c:url value='/ajax/facturacion/facturas/generacionfacturas/show_listadofacturas.do'/>?"+encodeURI("facturas="+xml);
			$("#modal-dialog-form .modal-content").load(url, function() {
				hideSpinner("#capa_rectificar");
				$("#modal-dialog-form-2").modal('hide');
				dt_listgestionfacturas.ajax.reload();	
				$("#modal-dialog-form").modal('show');
				setModalDialogSize("#modal-dialog-form", "md");
			});						
		},
		error : function(exception) {
			hideSpinner("#capa_rectificar");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
	}
}

$("#Otros").on("change", function(e) {
	if('$("#idMotivoRectificacion").val()'=='Otros')
	{
		
	}})


</script>
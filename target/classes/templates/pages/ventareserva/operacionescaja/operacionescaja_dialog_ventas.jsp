<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${selector_formas_pago}" var="selectorFormasdePago_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">x</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="ventareserva.operacionescaja.dialog.ver_ventas.title" />
	</h4>
</div>
<div class="modal-body">
	<form id="form_ventas_caja_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input type="hidden" id="idcaja" name="idcaja" value="${idcaja}">
		<div class="form-group">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-2 col-sm-2 col-xs-12"><spring:message code="ventareserva.operacionescaja.dialog.ver_ventas.field.refventa" /></label>
				<div class="col-md-2 col-sm-2 col-xs-12">
					<input name="referenciaventaini" id="referenciaventaini" type="text" class="form-control" >
				</div>
				<label class="control-label col-md-1 col-sm-1 col-xs-12"><spring:message code="venta.ventareserva.tabs.canje_bono.field.hasta" /></label>
				<div class="col-md-2 col-sm-2 col-xs-12">
					<input name="referenciaventafin" id="referenciaventafin" type="text" class="form-control" >
				</div>			
				<label class="control-label col-md-2 col-sm-2 col-xs-12"><spring:message code="ventareserva.operacionescaja.dialog.ver_ventas.field.refventa.field.formaspago" />*</label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<select name="idformapago" id="selector_idformaPago" class="form-control" >
						<option value=""></option>
						<x:forEach select="$selectorFormasdePago_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="ln_solid"></div>
			<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
				<button id="button_filter_list_ventas_caja" type="button" class="btn btn-success pull-right">
					<spring:message code="common.button.filter" />
				</button>
			</div>
		
			<input type="hidden" name="start" value=""/>
			<input type="hidden" name="length" value=""/>		
	</form>
	
	<div class="col-md-12 col-sm-12 col-xs-12">
	
		<div class="btn-group pull-right btn-datatable">
			<a type="button" class="btn btn-info" id="button_anular_ventas">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="ventareserva.operacionescaja.dialog.ver_ventas.list.button.anular_venta" />"> <span class="fa fa-times"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="button_ver_lineas">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="ventareserva.operacionescaja.dialog.ver_ventas.list.button.ver_lineas" />"> <span class="fa fa-list"></span>
				</span>
			</a>
		</div>
	</div>
	
	
	<table id="datatable-list-ventas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="ventareserva.operacionescaja.editarcaja.list.header.refventa" /></th>
				<th><spring:message code="ventareserva.operacionescaja.editarcaja.list.header.fechaHora" /></th>
				<th><spring:message code="ventareserva.operacionescaja.editarcaja.list.header.productos" /></th>
				<th><spring:message code="ventareserva.operacionescaja.editarcaja.list.header.formasPago" /></th>
				<th><spring:message code="ventareserva.operacionescaja.editarcaja.list.header.importe" /></th>
				<th><spring:message code="ventareserva.operacionescaja.editarcaja.list.header.estado" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	
		
	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="modal-footer">
			<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
						<spring:message code="common.button.close" />
			</button>			
		</div>	
	</div>
</div>


<script>
hideSpinner("#button_return_ventas");


var dt_listventas=$('#datatable-list-ventas').DataTable( {
	serverSide: true,
	searching: false,
	ordering: false,
	pagingType: "simple",
	info: false,
	deferLoading: 0,
    ajax: {
        url: "<c:url value='/ajax/ventareserva/operacionescaja/list_ventas_caja.do'/>",
        rowId: 'idcaja',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Venta)); return(""); },
        data: function (params) {
        	$('input[name="start"]').val(params.start);
        	$('input[name="length"]').val(params.length);
        	return ($("#form_ventas_caja_data").serializeObject());
       	},
        error: function (xhr, error, code)
        {
           	new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : xhr.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}					
			});
        }
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listventas.data().count()>0) dt_listventas.columns.adjust().draw(); });
	},
    columns: [
        { data: "idventa", type: "numeric"}, 
        { data: "fechayhora", type: "spanish-string"}, 
        { data: "lineadetallesconanuladas.Lineadetalle", className: "cell_centered",
           	  render: function ( data, type, row, meta ) { return showListInCell(data,"<spring:message code="ventareserva.operacionescaja.dialog.ver_ventas.list.text.productos" />","producto.nombre"); }	
        },  
        { data: "formasdepagos.formapago", className: "cell_centered",
         	  render: function ( data, type, row, meta ) { return showListInCell(data,"<spring:message code="ventareserva.operacionescaja.dialog.ver_ventas.list.text.formas_de_pago" />","nombre"); }	
      	},  
        { data: "importe", type: "spanish-string"}, 
        { data: "estadooperacion.nombre", type: "spanish-string"}, 
        
    ],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-ventas') },
    select: { style: 'single', selector:'td:not(:nth-child(3)):not(:nth-child(4))'},
	language: dataTableLanguage,
	processing: true
} );


insertSmallSpinner("#datatable-list-ventas_processing");
//********************************************************************************
$("#button_filter_list_ventas_caja").on("click", function(e) { dt_listventas.ajax.reload(); })
//********************************************************************************
$("#button_anular_ventas").on("click", function(e) { 
	

var data= dt_listventas.rows( { selected: true } ).data();

if(data.length==1)
	{
	
	showButtonSpinner("#button_anular_ventas");	
	var idVenta=data[0].idventa;

	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/ventareserva/operacionescaja/show_anular_venta.do'/>?id="+idVenta, function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "md");
	});
	}
})

//********************************************************************************
$("#button_ver_lineas").on("click", function(e) { 
	

var data= dt_listventas.rows( { selected: true } ).data();

if(data.length==1)
	{	
	showButtonSpinner("#button_ver_lineas");	
	var idVenta=data[0].idventa;

	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/ventareserva/operacionescaja/show_anular_lineas.do'/>?id="+idVenta, function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "md");
	});
	}
})
//*********************************************************************************

</script>
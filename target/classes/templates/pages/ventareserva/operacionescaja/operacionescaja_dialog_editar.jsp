<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="ventareserva.operacionescaja.editaropcaja.dialog.editar.title" />
	</h4>	
</div>

<div class="modal-body">
	<form id="form_editaropcaja_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	    <input id="idopcaja" name="idopcaja" type="hidden" value="" />
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="ventareserva.operacionescaja.editaropcaja.field.importe" /></label>
			<div class="col-md-9 col-sm-9 col-xs-9">
				<input name="importe" id="importe" required="required" type="text" class="form-control required" required="required" value="">
                <span class="fa fa-euro form-control-feedback right" aria-hidden="true"></span>
			</div>
		</div>
			
	</form>

	<div class="modal-footer">
		<button id="save_editaropcaja_button" type="button" class="btn btn-primary save_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>

</div>

<script>

$("#save_editaropcaja_button").on("click", function(e) {
	$("#form_editaropcaja_data").submit();
})

$("#form_editaropcaja_data").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		saveFormData();		
	}
});

//********************************************************************************	
function saveFormData() {
	$('input[name="idopcaja"]').val("${idOperacionCaja}");
	var data = $("#form_editaropcaja_data").serializeObject();

	showSpinner("#modal-dialog-form-2 .modal-content");

	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/operacionescaja/editar_op_caja.do'/>",
		timeout : 100000,
		data: data, 
		success : function(data) {
			hideSpinner("#modal-dialog-form-2 .modal-content");
			dt_listoperaciones.ajax.reload(null,false);
			$("#modal-dialog-form-2").modal('hide');
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : { sticker : false	}
			});			
		},
		error : function(exception) {
			hideSpinner("#modal-dialog-form-2 .modal-content");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});		
}	

</script>
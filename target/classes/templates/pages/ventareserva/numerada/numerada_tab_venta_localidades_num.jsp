<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${numerada_tipos_producto}" var="numerada_tipos_producto_xml" />



<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2><spring:message code="venta.numerada.tabs.bloquear_localidades.representacion_bloquear" /></h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">
	
		<form id="form_selector_venta_localidades" class="form-horizontal form-label-left">
			<input type="hidden" name="id_recinto" id="id_recinto"/>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.numerada.tabs.bloquear_localidades.field.temporada" /></label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<select class="form-control" name="idtemporadaVenta" id="idtemporadaVenta">
							<option value=""></option>							
						</select>
					</div>					
				</div>
			</div>
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.numerada.tabs.bloquear_localidades.field.tipo_producto" /></label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<select class="form-control" name="idtipo_productoVenta" id="idtipo_productoVenta">
							<option value=""></option>
							<x:forEach select="$numerada_tipos_producto_xml/ArrayList/tipoproducto" var="item">
								<option value="<x:out select="$item/idtipoproducto" />"><x:out select="$item/nombre" /></option>
							</x:forEach>				
						
						</select>
					</div>					
				</div>
			</div>
			
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.numerada.tabs.bloquear_localidades.field.contenido" /></label>
					<div id="espectaculo_temporada_programacion_div" class="col-md-4 col-sm-4 col-xs-12">
						<select class="form-control" name="idcontenidoVenta" id="idcontenidoVenta">
							<option value=""></option>							
						</select>
					</div>					
				</div>
			</div>
		</form>
		<div class="clearfix"></div>
		<div class="ln_solid"></div>		
		<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
				<button id="button_filter_sesiones_venta" type="button" class="btn btn-success pull-right">
					<spring:message code="common.button.filter" />
				</button>
		</div>	
		
	</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_vender_localidades_imprimir">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada.tabs.vender_localidades.list.button.imprimir" />"> <span class="fa fa-print"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_vender_localidades_automatica">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada.tabs.vender_localidades.list.button.automatico" />"> <span class="fa fa-desktop"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_vender_localidades_manual">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada.tabs.vender_localidades.list.button.manual" />"> <span class="fa fa-hand-paper-o"></span>
			</span>
		</a>
				
	</div>

	<table id="datatable-lista-sesiones-ventas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th></th>
				<th><spring:message code="venta.numerada.tabs.bloquear_localidades.list.header.contenido" /></th>
				<th><spring:message code="venta.numerada.tabs.bloquear_localidades.list.header.fecha" /></th>
				<th><spring:message code="venta.numerada.tabs.bloquear_localidades.list.header.hora" /></th>				
				<th><spring:message code="venta.numerada.tabs.bloquear_localidades.list.header.bloq" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>


<script>

var temporadas_json = ${numerada_temporadas_programacion};
var temporadas_json_array = temporadas_json.ArrayList.LabelValue;
var contenidos;
 
var temporadasVentas = $("#idtemporadaVenta"); 

dtsesionesVentas=$('#datatable-lista-sesiones-ventas').DataTable( {
	"paging": true,
	"pageLength": 10,
	"info": false,
	"searching": false,
	select: { style: 'single'},
	columnDefs: [
	             { "targets": [0], "visible": false }
	             ],
	language: dataTableLanguage
	});


if(temporadas_json_array.length>1)
{
temporadas.append("<option value=''></option>");
for (k = 0; k < temporadas_json_array.length; k++)
	{
	temporadasVentas.append("<option value='" + temporadas_json_array[k].value+ "'>" + temporadas_json_array[k].label + "</option>");
	}		
}
else
{
temporadasVentas.append("<option value='" + temporadas_json_array.value+ "'>" + temporadas_json_array.label + "</option>");
}


 //********************************************************************************
$("#idcontenidoVenta").on("change", function(e) {
	
	dtsesionesVentas.clear();
	var indice =$("#idcontenidoVenta").prop('selectedIndex');
	var contenido = contenidos;
	if(contenidos.length>0)
	  contenido = contenido[indice-1];	
	

	$("#id_recinto").val(contenido.tipoproducto.recinto.idrecinto);
	
	var sesiones = contenido.sesions.Sesion;	
	
		
	$.each(sesiones, function(key, sesion){					
		var bloqueado = "";
		if (sesion.bloqueado==1) 
			bloqueado = '<i class="fa fa-check-circle" aria-hidden="true"></i>';

		
		dtsesionesVentas.row.add( [ sesion, contenido.nombre, sesion.fecha.split("-")[0], sesion.horainicio, bloqueado ] )
	    .draw()
	    });
	
})
 
 
 
 //**********************************************************************
$("#idtemporadaVenta").on("change", function(e) {
	
	showFieldSpinner("#espectaculo_temporada_programacion_div");	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/list_contenidos_temporada.do'/>",
		timeout : 100000,
		data: {
				idtemporada: $("#idtemporadaVenta").val()
			  }, 
		success : function(data) {
			hideSpinner("#espectaculo_temporada_programacion_div");
			var $select=$("#idcontenidoVenta");
			$select.html('');
			$select.prepend("<option value='' selected='selected'></option>");
			contenidos = data.ArrayList.Contenido;
			if(contenidos!=undefined)
			{
				if(contenidos.length>0)
				{
						$.each(contenidos, function(key, val){					
					      $select.append('<option value="' + val.idcontenido + '">' + val.nombre + '</option>');
					    });
				}	
				else
					$select.append('<option selected value="' + contenidos.idcontenido + '">' + contenidos.nombre + '</option>');
			}
				
		},
		error : function(exception) {
			hideSpinner("#espectaculo_temporada_programacion_div");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,				  
				  buttons: { sticker: false }				  
			   });		
		}
	});
	
});




//**********************************************************************
$("#idtipo_productoVenta").on("change", function(e) {
	showFieldSpinner("#espectaculo_temporada_programacion_div");	
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/list_contenidos_by_tipo.do'/>",
		timeout : 100000,
		data: {
				idtipo_producto: $("#idtipo_productoVenta").val(),
				idtemporada: $("#idtemporadaVenta").val()
			  }, 
		success : function(data) {
			hideSpinner("#espectaculo_temporada_programacion_div");
			var $select=$("#idcontenidoVenta");
			$select.html('');
			$select.prepend("<option value=''></option>");			
			contenidos = data.ArrayList.Contenido;
			if(contenidos!=undefined)
			{
				if(contenidos.length>0)
				{
						$.each(contenidos, function(key, val){					
					      $select.append('<option value="' + val.idcontenido + '">' + val.nombre + '</option>');
					    });
				}	
				else
				{
					$select.append('<option value="' + contenidos.idcontenido + '">' + contenidos.nombre + '</option>');
				}
			}
		},
		error : function(exception) {
			hideSpinner("#espectaculo_temporada_programacion_div");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,				  
				  buttons: { sticker: false }				  
			   });		
		}
	});
	
});



//********************************************************************
$("#tab_vender_localidades_manual").on("click", function(e) 
{ 
	
	var id_recinto = $("#id_recinto").val();
	var id_sesion = dtsesionesVentas.rows( { selected: true } ).data()[0][0].idsesion;
	
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_seleccion_localidadeds_venta_numerada.do'/>?idSesion="+id_sesion+"&idrecinto="+id_recinto+"&vender=1", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});	
	
	
})

//********************************************************************



$("#tab_vender_localidades_automatica").on("click", function(e) 
{ 
	
	var id_recinto = $("#id_recinto").val();
	var id_sesion = dtsesionesVentas.rows( { selected: true } ).data()[0][0].idsesion;
	
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_seleccion_localidades_venta_numerada_automatica.do'/>?idSesion="+id_sesion+"&idrecinto="+id_recinto, function() {
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "xs");
	});	
})



</script>


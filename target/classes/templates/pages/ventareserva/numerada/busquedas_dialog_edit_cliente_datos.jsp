<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_cliente}" var="editar_cliente_xml" />
<x:parse xml="${editar_cliente_categorias_actividad}" var="editar_cliente_categorias_actividad_xml" />
<x:parse xml="${editar_cliente_tipos_documento}" var="editar_cliente_tipos_documento_xml" />
	
	<input id="idCliente" name="idCliente" type="hidden" value="<x:out select = "$editar_cliente_xml/Cliente/idcliente" />" />
	
	<div class="col-md-7 col-sm-7 col-xs-7">
			<input id="idprincipal" name="idprincipal" type="hidden" value="<x:out select = "$editar_cliente_xml/Cliente/cliente/idcliente" />" />
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.field.persona" />*</label>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<select name="personafisica" id="personafisica"  class="form-control" required="required">
						<option value="1">Fisica</option>
						<option value="0">Juridica</option>
					</select>
				</div>
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.field.nombre" />*</label>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editar_cliente_xml/Cliente/nombre" />">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.field.apellido1" />*</label>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<input name="apellido1" id="apellido1" type="text" class="form-control" required="required" value="<x:out select = "$editar_cliente_xml/Cliente/apellido1" />">
				</div>
			
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.field.apellido2" />*</label>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<input name="apellido2" id="apellido2" type="text" class="form-control" required="required" value="<x:out select = "$editar_cliente_xml/Cliente/apellido2" />">
				</div>
			</div>	
			<div class="form-group">
				<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="facturacion.facturas.tabs.clientes.field.actividad" />*</label>
				<div class="col-md-5 col-sm-5 col-xs-5">
					<select name="idcategoriacategoriaactividad" id=idcategoriacategoriaactividad  class="form-control" required="required">
						<option> </option>
						<x:forEach select="$editar_cliente_categorias_actividad_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-3 col-sm-3 col-xs-12">
					<strong><spring:message code="facturacion.facturas.tabs.clientes.field.identificacion" /></strong>
				</div>			
				<div class="col-md-4 col-sm-4 col-xs-4">
					<select name="idtipoidentificador" id=idtipoidentificador  class="form-control" required="required">
						<option> </option>
						<x:forEach select="$editar_cliente_tipos_documento_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<input name="identificador" id="identificador" type="text" class="form-control" required="required" value="<x:out select = "$editar_cliente_xml/Cliente/identificador"/>"/>
				</div>
			</div>
						
	</div>
	<div class="col-md-5 col-sm-5 col-xs-5">
			<div class="form-group">
				<label class="control-label col-md-4 col-sm-4 col-xs-4"><spring:message code="facturacion.facturas.tabs.clientes.field.observaciones" /></label>
				<div class="col-md-8 col-sm-8 col-xs-8">					
					<textarea name="descripcion" id="descripcion" class="form-control"  style="height: 150px;"><x:out select = "$editar_cliente_xml/Cliente/observaciones" /></textarea>
				</div>
			</div>		 			
	</div>
	
<script>
hideSpinner("#tab_clientes_edit");

var personafisica = '<x:out select = "$editar_cliente_xml/Cliente/personafisica" />';
$('#personafisica option[value="'+personafisica+'"]').attr("selected", "selected");

var tipoIdentificador = '<x:out select = "$editar_cliente_xml/Cliente/tipoidentificador/idtipoidentificador" />';
$('#idtipoidentificador option[value="'+tipoIdentificador+'"]').attr("selected", "selected");

var principal = '<x:out select = "$editar_cliente_xml/Cliente/facturarsucursal" />';
$("input[name=principal][value='" + principal + "']").attr('checked', 'checked');

var idactividad = '<x:out select = "$editar_cliente_xml/Cliente/actividad/idactividad" />';
$('#idcategoriacategoriaactividad option[value="'+idactividad+'"]').attr("selected", "selected");

</script>
	
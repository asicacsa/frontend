<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

 

<!-- Pestañas ------------------------------------------------------------->
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 maintab">
		<div class="" role="tabpanel" data-example-id="togglable-tabs">
			<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
				<li role="presentation" class="active"><a href="#tab_bloquear_localidades" role="tab" id="bloquear-localidades-tab" data-toggle="tab" aria-expanded="true"><spring:message code="venta.numerada.tabs.bloquear_localidades.title" /></a></li>																																																
				<li role="presentation" class=""><a href="#tab_venta_localidades_num" role="tab" id="venta-localidades-num-tab" data-toggle="tab" aria-expanded="false"><spring:message code="venta.numerada.tabs.venta_localidades_num.title" /></a></li>				
			</ul>
		</div>
	</div>
</div>

<!-- Area de trabajo ------------------------------------------------------>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div id="main-numerada" class="x_panel thin_padding">
			<div id="myTabContent" class="tab-content">
				<div role="tabpanel" class="tab-pane fade active in" id="tab_bloquear_localidades" aria-labelledby="bloquear-localidades-tab">
					<tiles:insertAttribute name="tab_bloquear_localidades" /> 					
				</div>

				<div role="tabpanel" class="tab-pane fade" id="tab_venta_localidades_num" aria-labelledby="venta-localidades-num-tab">
					<%-- <tiles:insertAttribute name="tab_venta_localidades_num" /> --%>
					<tiles:insertAttribute name="tab_venta_localidades_num" />
				</div>
			</div>
			<div class="clearfix"></div>		
		</div>
	</div>
</div>

<script>
</script>

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-2"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-3"/>
<tiles:insertAttribute name="modal_dialog" />


<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-buscar-cliente"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-editar-cliente"/>
<tiles:insertAttribute name="modal_dialog" />

<tiles:insertAttribute name="modal_desbloquear_localidades" />








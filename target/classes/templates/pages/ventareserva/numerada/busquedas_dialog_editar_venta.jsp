
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_venta}" var="editar_venta_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.numerada.tabs.venta_edit.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_numerada_editar_venta_dialog" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	<%-- <input type="hidden" name="observaciones_venta" id="observaciones_venta" value="<x:out select = "$editar_venta_xml/Venta/observaciones" />">--%>
	<input type="hidden" name="idVentaEditar" id="idVentaEditar" value="<x:out select = "$editar_venta_xml/Venta/idventa" />">
	<input type="hidden" name="importeTotalVenta" id="importeTotalVenta" value="<x:out select = "$editar_venta_xml/Venta/importetotalventa" />">
	<input type="hidden" name="idcliente_venta" id="idcliente_venta" value=""/>	 
	<input type="hidden" name="idcliente_fiscal" id="idcliente_fiscal" value=""/>
		<div id="columna-izquierda" class="col-md-5 col-sm-12 col-xs-12">		
			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.numerada.tabs.venta_edit.field.venta" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$editar_venta_xml/Venta/idventa" /></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.fechaventa" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$editar_venta_xml/Venta/fechayhoraventa" /></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.usuario" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$editar_venta_xml/Venta/usuario/nombre" />&nbsp;<x:out select = "$editar_venta_xml/Venta/usuario/apellidos" /></div>
			</div>
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.numerada.tabs.venta_edit.field.descuento" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen">?????</div>
			</div>
			<div class="form-group">				
				<label class="control-label col-md-5 col-sm-5 col-xs-12"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico_canje_bono.field.estado" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$editar_venta_xml/Venta/estadooperacion/nombre" /></div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.numerada.tabs.venta_edit.field.observaciones" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen">					
					<textarea name="observaciones" id="observaciones" class="form-control"  ><x:out select = "$editar_venta_xml/Venta/observaciones" /></textarea>
				</div>
			</div>
		</div>

		<div id="columna-central" class="col-md-5 col-sm-12 col-xs-12">			
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-12"><spring:message code="venta.numerada.tabs.venta_edit.field.cliente_venta" /></label>
				<div class="col-md-7 col-sm-7 col-xs-12">
					<input name="cliente_venta" id="cliente_venta" type="text" class="form-control" readonly>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-12"><spring:message code="venta.numerada.tabs.venta_edit.field.cliente_fiscal" /></label>
				<div class="col-md-7 col-sm-7 col-xs-12">
					<input name="cliente_fiscal" id="cliente_fiscal" type="text" class="form-control" readonly>
				</div>
			</div>			
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-12"><spring:message code="venta.numerada.tabs.venta_edit.field.cp" /></label>
				<div class="col-md-7 col-sm-7 col-xs-12">
					<input id="cp_callcenter" name="cp_callcenter" type="text" class="form-control" required="required" value="<x:out select = "$editar_venta_xml/Venta/cp" />" />
				</div>				
			</div>

			<div class="form-group">	
				<label class="control-label col-md-5 col-sm-5 col-xs-12"><spring:message code="venta.numerada.tabs.venta_edit.field.telefono" /></label>
				<div class="col-md-7 col-sm-7 col-xs-12">
					<input type="text" name="telefono" id="telefono" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/telefono" />"  />
				</div>								
			</div>
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-12"><spring:message code="venta.numerada.tabs.venta_edit.field.movil" /></label>
				<div class="col-md-7 col-sm-7 col-xs-12">
					<input type="text" name="movil" id="movil" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/telefonomovil" />"  />
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-12"><spring:message code="venta.numerada.tabs.venta_edit.field.nombre_cliente" /></label>
				<div class="col-md-7 col-sm-7 col-xs-12">
					<input type="text" name="nombre_cliente" id="nombre_cliente" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/cliente/nombre" />" disabled="disabled"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-12"><spring:message code="venta.numerada.tabs.venta_edit.field.mail_cliente" /></label>
				<div class="col-md-7 col-sm-7 col-xs-12">
					<input type="text" name="email" id="email" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/email" />"   />
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-12"><spring:message code="venta.numerada.tabs.venta_edit.field.nif_cliente" /></label>
				<div class="col-md-7 col-sm-7 col-xs-12">
					<input type="text" name="nif" id="nif" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/nif" />"   />
				</div>
			</div>
		</div>		
		<div id="columna-derecha" class="col-md-2 col-sm-12 col-xs-12">
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<span class="fa" id="check-facturada"/>&nbsp;<spring:message code="venta.ventareserva.dialog.venta_resumen.field.facturada" />
				</div>
			</div>			
		</div>
		
	<div class="col-md-12 col-sm-12 col-xs-12">			
		<div class="btn-group pull-right btn-datatable">
		 	<a type="button" class="btn btn-info" id="tabs-venta_edit-imprimir_entradas">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada.tabs.venta_edit.button.imprimir_entradas" />"> <span class="fa fa-print"></span>
				</span>
			</a>	
			<a type="button" class="btn btn-info" id="tabs_venta_edit_carta_confirmacion">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada.tabs.venta_edit.button.carta_confirmacion" />"> <span class="fa fa-envelope"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="tabs_venta_edit_editar_linea_detalle">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada.tabs.venta_edit.button.editar_linea_detalle" />"> <span class="fa fa-pencil"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="tabs_venta_edit_anular_linea_detalle">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada.tabs.venta_edit.button.anular_linea_detalle" />"> <span class="fa fa-close"></span>
				</span>
			</a>		
			<a type="button" class="btn btn-info" id="tabs_venta_edit_anular_entradas_abonos">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada.tabs.venta_edit.button.anular_entradas_abonos" />"> <span class="fa fa-minus-circle"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="tabs_venta_edit_anadir_linea_detalle">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada.tabs.venta_edit.button.anadir_linea_detalle" />"> <span class="fa fa-plus"></span>
				</span>
			</a>	
			<a type="button" class="btn btn-info" id="tabs_venta_edit_ver_formas_pago">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada.tabs.venta_edit.button.ver_formas_pago" />"> <span class="fa fa-euro"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="tabs_venta_edit_ver_anuladas">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada.tabs.venta_edit.button.ver_anuladas" />"> <span class="fa fa-bookmark"></span>
				</span>
			</a>			
		</div>
		
		<table id="datatable-list-edit_venta_numerada" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th></th>
				<th><spring:message code="venta.numerada.tabs.venta_edit.list.header.num_entrada" /></th>
				<th><spring:message code="venta.numerada.tabs.venta_edit.list.header.contenido" /></th>
				<th><spring:message code="venta.numerada.tabs.venta_edit.list.header.sesion" /></th>
				<th><spring:message code="venta.numerada.tabs.venta_edit.list.header.area" /></th>
				<th><spring:message code="venta.numerada.tabs.venta_edit.list.header.fila" /></th>
				<th><spring:message code="venta.numerada.tabs.venta_edit.list.header.butaca" /></th>
				<th><spring:message code="venta.numerada.tabs.venta_edit.list.header.importe" /></th>
				<th><spring:message code="venta.numerada.tabs.venta_edit.list.header.num_i" /></th>				
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
		
		
		
			
		
</div>	

<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_venta_button" type="submit" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
		
	</form>

</div>


<script>


//************************************************
hideSpinner("#tab_ventas_edit");
//************************************************
 var dt_listventanumerada=$('#datatable-list-edit_venta_numerada').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	  initComplete: function( settings, json ) {
	    	window.setTimeout(CargarLineasDetalle, 100);  
	    },
	scrollCollapse: true,
	ordering:  false,
	paging: false,	
    select: { style: 'os' },
    columns: [
              {},              
              {},
              {},
              {},
              {},
              {},
              {},
              {},
              {}
              
              
        ],
        "columnDefs": [
                       { "visible": false, "targets": [0]}
                     ],
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('datatable-list-edit_venta_numerada');
   	}
    
} ); 
//****************************************************************
 function CargarLineasDetalle()
	{
		var json = ${editar_venta_json};
			
		var i = 0;
		
		var lineasdetalle = json.Venta.lineadetalles;
				
		if (lineasdetalle !=""){
		if (lineasdetalle.Lineadetalle!="") {
	        var item=lineasdetalle.Lineadetalle;
	        if (item.length>0)
	            $.each(item, function(key, lineadetalle){
	            	CargarLineaDetalle(lineadetalle,dt_listventanumerada)
	            });
	        else
	        	CargarLineaDetalle(item,dt_listventanumerada)
	}
	}
		dt_listventanumerada.rows().invalidate().draw();
	} 
		

	//*****************************************************************************************
	 function CargarLineaDetalle(lineadetalle, tabla)
	{	
		
		var fechas= "", contenidos= "", disponibles= "",retorno= false;
		var area="",fila="",butaca="";
		var sesiones = lineadetalle.estadolocalidads.Estadolocalidad;
		
		
		var impreso = lineadetalle.entradasimpresas+"/"+lineadetalle.entradasnoanuladas;		
		var descuento_nombre=""+lineadetalle.descuentopromocional.nombre;
		var numbonoagencia= lineadetalle.numerobonoagencia;
			
		
		if(descuento_nombre=="undefined")
			descuento_nombre =" ";
		
		var contenidos;
		
		 if (sesiones.length>0)		 
		 {
				for (var i=0; i<sesiones.length; i++) {
					if (retorno) {
						fechas+="</br>";
						horas+="</br>";
						contenidos+= "</br>";	
						fila+="</br>";
						butaca+="</br>";
						area+="</br>";
					}				
					var f= sesiones[i].sesion.fecha.split("-")[0];
					var hi=sesiones[i].sesion.horainicio;
					fechas+= f+"-"+hi;					
					var hi=sesiones[i].sesion.horainicio;
					var  no=sesiones[i].sesion.contenido.nombre;
					
					fila+= sesiones[i].localidad.fila;
					butaca+= sesiones[i].localidad.butaca;
					area+= sesiones[i].localidad.nombrearea;
					contenidos+=" "+no;					
					retorno= true;
				}	 
				
		 }
		 else
		 {
			fechas= sesiones.sesion.fecha.split("-")[0]+"-"+sesiones.sesion.horainicio;
			contenidos= lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.contenido.nombre;
			fila+= sesiones.localidad.fila;
			butaca+= sesiones.localidad.butaca;
			area+= sesiones.localidad.nombrearea;
				
		 }
				tabla.row.add([
					lineadetalle,          
					lineadetalle.entradas.Entrada.identrada,					
					contenidos, 
					fechas,         
					area, 
					fila, 
					butaca, 				    
				    ""+lineadetalle.tarifaproducto.importe,
				    ""+lineadetalle.entradasimpresas				    
				]);
} 

//***************************************************************
function ajustar_cabeceras_datatable()
{
	$('#datatable-list-detalle-venta').DataTable().columns.adjust().draw();
}
//**********************************************************************
$("#cliente_venta").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>", "#cliente_venta", "#idcliente_venta");
$("#cliente_fiscal").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>", "#cliente_fiscal", "#idcliente_fiscal");
//********************************************************************
facturada= Number("<x:out select = "$editar_venta_xml/Venta/facturada" />"),
	
$("#check-facturada").addClass(facturada>0?'fa-check':'fa-close');
$("#check-facturada").css("color",facturada>0?'green':'red');
//********************************************************************************
setTimeout(ajustar_cabeceras_datatable, 3000);
//********************************************************************
$("#tabs-venta_edit-imprimir_entradas").on("click", function(e) 
{ 
	var data = dt_listventanumerada.rows( { selected: true } ).data();

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.numerada_busquedas.tabs.ventas.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	var xml="<list>";
	for (var i=0; i<data.length; i++)
	{
		xml = xml+ "<identrada>"+data[i][1] +"</identrada>";
	}
	
	xml=xml+"</list>";
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/mostrar_dialogo_imprimir_entradas.do'/>",
		timeout : 100000,
		data: {
				xml: xml
			  }, 
		success : function(data) {
			if (data.Integer==1) {
				$("#modal-dialog-ventas-imprimir-entradas").modal('show');
				setModalDialogSize("#modal-dialog-ventas-imprimir-entradas", "md");
			}
			else {
				busquedasNumeradaVentasImprimirEntradas();			
			}
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,				  
				  buttons: { sticker: false }				  
			   });		
		}
	});
					
})
//********************************************************************
$("#tabs_venta_edit_carta_confirmacion").on("click", function(e) 
{ 
	showButtonSpinner("#tabs_venta_edit_carta_confirmacion");

	var id="<x:out select = "$editar_venta_xml/Venta/idventa" />";
	
	
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/venta/busqueda/show_carta_confirmacion_venta.do'/>?id="+id, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "xs");
	});	
})
//********************************************************************
$("#tabs_venta_edit_editar_linea_detalle").on("click", function(e) 
{ 
	
})
//********************************************************************
$("#tabs_venta_edit_anular_linea_detalle").on("click", function(e) 
{ 
	var data = dt_listventanumerada.rows( { selected: true } ).data();
	var importeDevolucion = 0;	
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.numerada_busquedas.tabs.ventas.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	$.each( data, function( index, linea ){
		importeDevolucion = importeDevolucion + parseFloat(linea[0].importe);
		
		});
			
	showButtonSpinner("#tabs_venta_edit_anular_linea_detalle");
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/venta/numerada/anular_linea_detalle.do'/>?id="+$("#idVentaEditar").val()+"&importeTotalVenta="+$("#importeTotalVenta").val()+"&importeDevolucion="+importeDevolucion, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "md");
	});	
})
//********************************************************************
$("#tabs_venta_edit_anular_entradas_abonos").on("click", function(e) 
{ 
	var data = dt_listventanumerada.rows( { selected: true } ).data();
	var importeDevolucion = 0;	
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.numerada_busquedas.tabs.ventas.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="venta.numerada_busquedas.tabs.ventas.list.alert.entradas.confirm" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
		  buttons: { closer: false, sticker: false	},
		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
		  var xml="<list>";
		  var numero = data.length;
		  if(numero>0)
			  {
			  $.each( data, function( index, entrada ){
				  xml+="<int>"+entrada[1]+"</int>";
				})
			  }
		  else
			  {
			  xml+="<int>"+data[1]+"</int>";
			  }
		 
		  xml+="</list>";
		  
		  $.ajax({
				contenttype: "application/json; charset=utf-8",
				type : "post",
				url : "<c:url value='/ajax/venta/numerada/eliminarEntradas.do'/>",
				timeout : 100000,
				data: {
						xml: xml
					  }, 
				success : function(data) {
					
				},
				error : function(exception) {
					new PNotify({
					      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					      text: exception.responseText,
						  type: "error",
						  delay: 5000,				  
						  buttons: { sticker: false }				  
					   });		
				}
			});		  
		  
	   })
})
//********************************************************************
$("#tabs_venta_edit_anadir_linea_detalle").on("click", function(e) 
{ 
	
})
//********************************************************************
$("#tabs_venta_edit_ver_formas_pago").on("click", function(e) 
{ 
	var id="<x:out select = "$editar_venta_xml/Venta/idventa" />";
	
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/venta/numerada/editarVenta/FormasPago.do'/>?id="+id, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "lg");
	});
})
//********************************************************************
$("#tabs_venta_edit_ver_anuladas").on("click", function(e) 
{ 
	showButtonSpinner("#tabs_venta_edit_ver_anuladas");
	var id="<x:out select = "$editar_venta_xml/Venta/idventa" />";
	

	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/venta/busqueda/show_anuladas.do'/>?id="+id, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "lg");
	});
})
//********************************************************************
function operacionesNumeradaVentasImprimirEntradas() {

	showButtonSpinner("#tabs-venta_edit-imprimir_entradas");
	
	var data = dt_listventanumerada.rows( { selected: true } ).data();
		
	var entradas=data[0].lineadetalles.Lineadetalle;
	var xml="<list>";
	for (var i = 0; i < entradas.length; i++) {
		xml=xml+"<identrada>"+entradas[i].entradas.Entrada.identrada+"</identrada>";
	}
	xml=xml+"</list>";

	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/imprimir_entradas.do'/>",
		timeout : 100000,
		data: {
				xml: xml
			  }, 
		success : function(data) {
			hideSpinner("#tabs-venta_edit-imprimir_entradas");

			new PNotify({
			      title: '<spring:message code="common.dialog.text.impresion_realizada" />',
			      text: '<spring:message code="venta.numerada_busquedas.tabs.ventas.list.alert.impresion_realizada" />',
				  type: "info",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
		},
		error : function(exception) {
			hideSpinner("#tab_ventas_print_ticket");
			
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,				  
				  buttons: { sticker: false }				  
			   });		
		}
	});
	
}			


</script>






<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>



<div class="modal-body">
	<form id="form_selector_venta_individual" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	  	<div class="col-md-12 col-sm-12 col-xs-12">
		 	
		  <div class="btn-group pull-right btn-datatable">
			<a type="button" class="btn btn-info" id="button_venta_individual_bloquear">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.venta_individual.list.button.bloquear" />"> <span class="fa fa-lock"></span>
				</span>
			</a>			
			<a type="button" class="btn btn-info" id="button_venta_individual_cancelar">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.venta_individual.list.button.cancelar" />"> <span class="fa fa-close"></span>
				</span>
			</a>			
		  </div>
	  </div>
	  <table id="datatable-list-lineas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th></th>
				<th><spring:message code="venta.numerada.tabs.lineas.list.contenido" /></th>
				<th><spring:message code="venta.numerada.tabs.lineas.list.fila" /></th>
				<th><spring:message code="venta.numerada.tabs.lineas.list.butaca" /></th>
				<th><spring:message code="venta.numerada.tabs.lineas.list.zona" /></th>
				<th><spring:message code="venta.numerada.tabs.lineas.list.perfil" /></th>
				<th><spring:message code="venta.numerada.tabs.lineas.list.descuento" /></th>
				<th><spring:message code="venta.numerada.tabs.lineas.list.fecha" /></th>
				<th><spring:message code="venta.numerada.tabs.lineas.list.hora" /></th>
				<th><spring:message code="venta.numerada.tabs.lineas.list.importe" /></th>				
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	</form>
</div>

<script>

$("#cliente_venta_individual").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>?editable=1", "#cliente_venta_individual", "#idcliente_venta_individual", "#cifcliente_venta_individual", "#cpcliente_venta_individual", "#emailcliente_venta_individual", "#telefonocliente_venta_individual");

//********************************************************************************
var dt_listlineas=$('#datatable-list-lineas').DataTable( {
	searching: false,
	ordering: false,
	deferLoading: 0,
	pageLength: 50,
	lengthChange: false,
	pagingType: "simple",
	info: false,
    select: { style: 'os'},
	language: dataTableLanguage,
	 initComplete: function( settings, json ) {
    	 window.setTimeout(CargarLineasDetalle, 100);  
	 },	
	"columnDefs": [
                   { "visible": false, "targets": [0]}
                 ]
} );

var json= ${localidades};

function CargarLineasDetalle()
{
	var lineasdetalle = json.ArrayList;
	
	if (lineasdetalle !=""){
	if (lineasdetalle.Lineadetalle!="") {
        var item=lineasdetalle.Lineadetalle;
        if (item.length>0)
            $.each(item, function(key, lineadetalle){
            	CargarLineaDetalle(lineadetalle,dt_listlineas)
            });
        else
        	CargarLineaDetalle(item,dt_listlineas)
}
}
}

//*****************************************************************************************
function CargarLineaDetalle(lineadetalle, tabla)
{	
	var descuento_nombre="";
	var perfilvisitante_nombre="";
	
	
	tabla.row.add([
				   lineadetalle,
				   lineadetalle.producto.nombre, 
				   lineadetalle.estadolocalidads.Estadolocalidad.localidad.fila,         
				   lineadetalle.estadolocalidads.Estadolocalidad.localidad.butaca, 
				   lineadetalle.estadolocalidads.Estadolocalidad.localidad.zona.nombre,
				   perfilvisitante_nombre,
				   descuento_nombre, 				    
				   lineadetalle.estadolocalidads.Estadolocalidad.sesion.fecha.split("-")[0],
				   lineadetalle.estadolocalidads.Estadolocalidad.sesion.horainicio, 
				   lineadetalle.importe				    
				]).draw();
}

 






$("#button_venta_individual_cancelar").on("click", function(e) {
	//cambiarZona();
	$("#modal-dialog-form-2").modal('hide');	
})


//********************************************************************************
$("#button_venta_individual_bloquear").on("click", function(e) {
	
	if (dt_listlineas.rows().data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.venta_vacia" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	showButtonSpinner("#button_venta_individual_vender");	

	var data = $("#form_selector_venta_individual").serializeObject();

	var xml_lineasdetalle= "<list>";
	
	var lineasdetalle = json.ArrayList;
	
	if (lineasdetalle !=""){
	if (lineasdetalle.Lineadetalle!="") {
        var item=lineasdetalle.Lineadetalle;
        if (item.length>0)
            $.each(item, function(key, lineadetalle){
            	xml_lineasdetalle+=json2xml(lineadetalle.estadolocalidads,"");
            });
        else
        	xml_lineasdetalle+=json2xml(item.estadolocalidads,"");
	}
	}
	xml_lineasdetalle+="</list>";

	
	//showSpinner("#modal-dialog-form-2 .modal-content");
    
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/bloquear_localidades_numerada.do'/>",
		timeout : 100000,
		data: {
			xml: xml_lineasdetalle
		},
		success : function(data) {
			hideSpinner("#modal-dialog-form .modal-content");
			showSpinner("#main-ventareserva");
			
			$("#modal-dialog-form-2").modal('hide');			
			$("#modal-dialog-form").modal('hide');

			new PNotify({
			      title: '<spring:message code="venta.numerada.realizada" />',
			      text: '<spring:message code="venta.numerada.realizada.bloqueo" /> ',
			      type : "success",
				  buttons: { sticker: false }				  
			   });
			
			
			
			},
		error : function(exception) {
			hideSpinner("#modal-dialog-form .modal-content");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});	
	
});







</script>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_formaspago}" var="ventareserva_formaspago_xml" />
<x:parse xml="${ventareserva_selector_idiomas}" var="ventareserva_selector_idiomas_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.venta_numerada.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_venta_individual_dialog" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

		<div id="columna-izquierda" class="col-md-6 col-sm-12 col-xs-12">
		
			<div class="callcenter">
			
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.nombre" />*</label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input type="text" name="nombre" id="nombre" class="form-control" value=""/>
					</div>
				</div>
				
				<div class="form-group button-dialog capa_cp_dialog_venta_individual">
	
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="venta.ventareserva.dialog.venta_individual.field.nif" /></label>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<input type="text" name="nif" id="nif" class="form-control" value="" />
						</div>
					</div>
				
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.cp" />*</label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<!-- <button id='callcenter-search-button' type='button' class='btn btn-default btn-i18n'>
								<span class='docs-tooltip' data-toggle='tooltip' title='' data-original-title='Buscar CP'><span class='fa fa-search'></span></span>
							</button> -->
							<!-- <div class="input-prepend"> -->
								<input id="cp_callcenter" type="text" class="form-control" value=""/>
							<!-- </div> -->
						</div>
					</div>
					
				</div>		
				
				
				<div class="form-group">
	
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="venta.ventareserva.dialog.venta_individual.field.telefono" /></label>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<input type="text" name="telefono" id="telefono" class="form-control" value="" />
						</div>
					</div>
				
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.movil" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<input type="text" name="movil" id="movil" class="form-control" value="" />
						</div>
					</div>
					
				</div>		
	
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.email" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input type="text" name="email" id="email" class="form-control" value="" />
					</div>
				</div>
					
			</div>

			<div id="taquilla">
			
				<div class="form-group button-dialog capa_cp_dialog_venta_individual">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.cp" />*</label>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<button id='taquilla-search-button' type='button' class='btn btn-default btn-i18n'>
							<span class='docs-tooltip' data-toggle='tooltip' title='' data-original-title='Buscar CP'><span class='fa fa-search'></span></span>
						</button>
						<div class="input-prepend">
							<input id="cp_taquilla" type="text" class="form-control" value=""/>
						</div>
					</div>
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.pais" /></label>
					<div class="col-md-2 col-sm-2 col-xs-2">
						<input id="pais_dialog_venta_individual" type="text" class="form-control" value=""/>
					</div>
				</div>		
			</div>
				
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.observaciones" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<textarea class="form-control" rows="3" name="observaciones" id="observaciones"></textarea>
				</div>
			</div>

			<div class="x_title">
				<h5><spring:message code="venta.ventareserva.dialog.venta_individual.field.opciones_impresion" /></h5>
				<div class="clearfix"></div>
			</div>

			<div class="form-group">
				<div class="checkbox col-md-3 col-sm-3 col-xs-3">
					<input type="checkbox" name="selector" id="oimprecibo" value="" class="flat" style="position: absolute; opacity: 0;">
					<strong><spring:message code="venta.ventareserva.dialog.venta_individual.field.recibo" /></strong>
				</div>
				<div class="checkbox col-md-3 col-sm-3 col-xs-3">
					<input type="checkbox" name="selector" id="oimpcopia" value="" class="flat" style="position: absolute; opacity: 0;">
					<strong><spring:message code="venta.ventareserva.dialog.venta_individual.field.copia" /></strong>
				</div>
				<div class="checkbox col-md-3 col-sm-3 col-xs-3">
					<input type="checkbox" name="selector" id="oimpentradas" value="" class="flat" style="position: absolute; opacity: 0;">
					<strong><spring:message code="venta.ventareserva.dialog.venta_individual.field.entradas" /></strong>
				</div>
			</div>

			<div class="form-group">
				<div class="checkbox col-md-12 col-sm-12 col-xs-12">
					<input type="checkbox" name="selector" id="oimpentradasdef" value="" class="flat" checked style="position: absolute; opacity: 0;">
					<strong><spring:message code="venta.ventareserva.dialog.venta_individual.field.entradas_defecto" /></strong>
				</div>
			</div>
					
		</div>

		<div id="columna-derecha" class="col-md-6 col-sm-12 col-xs-12">

			<div class="x_title">
				<h5><spring:message code="venta.ventareserva.dialog.venta_individual.field.forma_pago" /></h5>
				<div class="clearfix"></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 forma-pago">
   				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
			            <label class="control-label col-md-4 col-sm-4 col-xs-4">
			            	<spring:message code="venta.ventareserva.dialog.venta_individual.field.imp_cobrar" />                               
			            </label>
			            <label class="control-label col-md-8 col-sm-8 col-xs-8 euros">
							<span class="total-lbl"><span id="total-cobrar">0.00</span>&euro;</span>		
			            </label>
		            </div>
		        </div>
   				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
	                   <label class="control-label col-md-4 col-sm-4 col-xs-12">
                       		<spring:message code="venta.ventareserva.dialog.venta_individual.field.imp_entregado" />                               
                       </label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<div class="input-prepend">
								<input name="importe1_text" id="importe1_text" type="text" class="form-control importe" disabled="disabled">
							</div>
							<span class="importe">&euro;</span>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12">
	                       <select name="importe1" id="selector_importe1" class="form-control">
								<option value=""></option>
								<x:forEach select="$ventareserva_formaspago_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
					       </select>
						</div>                       
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
	                   <label class="control-label col-md-4 col-sm-4 col-xs-12">
                       		<spring:message code="venta.ventareserva.dialog.venta_individual.field.imp_entregado" />                               
                       </label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input name="importe2_text" id="importe2_text" type="text" class="form-control importe" disabled="disabled">
							<span class="importe">&euro;</span>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12">
	                       <select name="importe2" id="selector_importe2" class="form-control">
								<option value=""></option>
								<x:forEach select="$ventareserva_formaspago_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
					       </select>
						</div>                       
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
	                   <label class="control-label col-md-4 col-sm-4 col-xs-12">
                       		<spring:message code="venta.ventareserva.dialog.venta_individual.field.imp_entregado" />                               
                       </label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input name="importe3_text" id="importe3_text" type="text" class="form-control importe" disabled="disabled">
							<span class="importe">&euro;</span>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12">
	                       <select name="importe3" id="selector_importe3" class="form-control" >
								<option value=""></option>
								<x:forEach select="$ventareserva_formaspago_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
					       </select>
						</div>                       
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
			            <label class="control-label col-md-4 col-sm-4 col-xs-4">
			            	<spring:message code="venta.ventareserva.dialog.venta_individual.field.imp_devolver" />                               
			            </label>
			            <label class="control-label col-md-8 col-sm-8 col-xs-8 euros">
							<span class="total-lbl"><span id="total-devolver">0.00</span>&euro;</span>		
			            </label>
		            </div>
		        </div>
				
			</div>
			
			<div class="callcenter">

				<div class="x_title">
					<h5>&nbsp;</h5>
					<div class="clearfix"></div>
				</div>

				<div class="form-group">
					<div class="checkbox col-md-6 col-sm-6 col-xs-12">
						<input type="checkbox" name="selector" id="carta_confirmacion" value="" class="flat" style="position: absolute; opacity: 0;">
						<span style="float: right;"><strong><spring:message code="venta.ventareserva.dialog.venta_individual.field.carta_confirmacion" /></strong></span>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
                       <select name="carta_confirmacion_select" id="carta_confirmacion_select" class="form-control">
							<option value="pdf">pdf</option>
							<option value="email">email</option>
							<option value="fax">pdf</option>
				       </select>
					</div>                       
				</div>
			
				<div class="form-group">
                   <label class="control-label col-md-6 col-sm-6 col-xs-12">
                  		<spring:message code="venta.ventareserva.dialog.venta_individual.field.idioma_carta" />                               
                    </label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select name="idiomas[]" id="selector_idiomas" class="form-control" style="width: 100%">
							<%-- <x:forEach select="$ventareserva_selector_idiomas_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach> --%>
						</select>
					</div>                       
				</div>

			</div>

		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_venta_button" type="button" class="btn btn-primary save_dialog">
					<spring:message code="common.button.confirm" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
		
	</form>

</div>

<script>

hideSpinner("#button_venta_individual_vender");

cargarSelectorIdiomas("selector_idiomas");

var importe_total=Number($(".totales_venta_individual #total-val").text()),
	importe_devolver= -importe_total,
	importe_pendiente=0,
	idpais= $("#pais_dialog_venta_individual").val(),
	tipocanal= "${sessionScope.idTipocanal}";
	
if (idpais=="") idpais= "472";  // Si no se indica pais se establece España por defecto

// Si el tipo de canal es "taquilla" se muestran los campos correspondientes

if (tipocanal=="1") {
	$(".taquilla").show();
	$(".callcenter").hide();
	$("#cp_taquilla").prop('required',true);
	$("#oimprecibo").prop('checked',true);
	$("#oimpentradas").prop('checked',true);
}
else {
	$(".taquilla").hide();
	$(".callcenter").show();
	$("#cp_callcenter").prop('required',true);
	$("#nombre").prop('required',true);
}


$("#total-cobrar").text(importe_total.toFixed(2));
$("#total-devolver").text(importe_devolver.toFixed(2));

function calcularPendiente() {
	var suma= Number(isNaN($("#importe1_text").val())?"0":$("#importe1_text").val())+Number(isNaN($("#importe2_text").val())?"0":$("#importe2_text").val())+Number(isNaN($("#importe3_text").val())?"0":$("#importe3_text").val());
	importe_pendiente= importe_total-suma;
	return;	
}

function calcularDevolver() {
	var suma= Number(isNaN($("#importe1_text").val())?"0":$("#importe1_text").val())+Number(isNaN($("#importe2_text").val())?"0":$("#importe2_text").val())+Number(isNaN($("#importe3_text").val())?"0":$("#importe3_text").val());
	importe_devolver= suma-importe_total;
	$("#total-devolver").text(importe_devolver.toFixed(2));
	return;	
}

/* Se rellena el formulario de CallCenter si hay un cliente seleccionado */

if ($("#idcliente_venta_individual").val()!="") {
	$("#cp_callcenter").val($("#cpcliente_venta_individual").val());
	$("#nombre").val($("#cliente_venta_individual").val());
	$("#nif").val($("#cifcliente_venta_individual").val());
	$("#telefono").val($("#telefonocliente_venta_individual").val());
	$("#movil").val($("#telefonomovilcliente_venta_individual").val());
	$("#email").val($("#emailcliente_venta_individual").val());
}

$("#cp_taquilla").val($("#cpcliente_venta_individual").val());

if ($("input.flat")[0]) {
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });
}

/* Se agrupan las funciones relativas a la modificación de los importes ya que son comunes 
 * para las tres.
 */

function selectorImporteChange(nombre) {

	$("#selector_"+nombre).on("change", function(e) {
		$("#"+nombre+"_text").val("");
		calcularPendiente();
		if ($("#selector_"+nombre+" :selected").text()=="") {
			$('input[name="'+nombre+'_text"]').val("");
	    	$('input[name="'+nombre+'_text"]').attr('disabled', 'disabled');
		} else {
			$('input[name="'+nombre+'_text"]').removeAttr('disabled');
	    	$("#"+nombre+"_text").val(importe_pendiente.toFixed(2));
		}
		calcularDevolver();
	});

	$("#"+nombre+"_text").on("change", function(e) {
		calcularDevolver();	
	});

	$("#"+nombre+"_text").on("focusout", function(e) {
		var valor= Number($("#"+nombre+"_text").val());
		$("#"+nombre+"_text").val(valor.toFixed(2));
	});
	
}

selectorImporteChange("importe1");
selectorImporteChange("importe2");
selectorImporteChange("importe3");

//********************************************************************************
function construir_xml_venta_numerada() {
	var cp_txt= (tipocanal=="1")?$("#cp_taquilla").val():$("#cp_callcenter").val(),
		imp_recibo= ($("#oimprecibo").is(":checked"))?1:0,
		imp_copia= ($("#oimpcopia").is(":checked"))?1:0,
		imp_entradas= ($("#oimpentradas").is(":checked"))?1:0,
		imp_entradas_def= ($("#oimpentradasdef").is(":checked"))?1:0;

	var xml_lineasdetalle="<getRealizarVentaLocalidadesNumeradas><Venta>";
	xml_lineasdetalle+="<canalByCanalventa>";	
	//El día 14 de Junio se comenta el canal porquieda problemas y se pasa el canl vacio
	//xml_lineasdetalle+="<idcanal>${sessionScope.idcanal}</idcanal>";
	xml_lineasdetalle+="<idcanal></idcanal>";
	xml_lineasdetalle+="</canalByCanalventa>";
	xml_lineasdetalle+="<cliente>";
	xml_lineasdetalle+="<idcliente>"+$("#idcliente_venta_individual").val()+"</idcliente>";
	xml_lineasdetalle+="</cliente>";
	xml_lineasdetalle+="<cp>"+cp_txt+"</cp>";
	xml_lineasdetalle+="<pais>";
	xml_lineasdetalle+="<idpais>"+idpais+"</idpais>";
	xml_lineasdetalle+="<nombre/>";
	xml_lineasdetalle+="</pais>";
	xml_lineasdetalle+="<financiada>false</financiada>";
	xml_lineasdetalle+="<dadodebaja>0</dadodebaja>";
	xml_lineasdetalle+="<email>"+$("#email").val()+"</email>";
	xml_lineasdetalle+="<entradasimpresas>"+imp_entradas+"</entradasimpresas>";
	xml_lineasdetalle+="<imprimirSoloEntradasXDefecto>"+imp_entradas_def+"</imprimirSoloEntradasXDefecto>";
	xml_lineasdetalle+="<estadooperacion/>";
	xml_lineasdetalle+="<facturada/>";
	xml_lineasdetalle+="<fechayhoraventa/>";
	xml_lineasdetalle+="<idventa/>";	
	xml_lineasdetalle+="<isBono>false</isBono>";	
	xml_lineasdetalle+="<importeparcials>"; 
	xml_lineasdetalle+="<Importeparcial>";
	xml_lineasdetalle+="<formapago>";
	xml_lineasdetalle+="<idformapago>"+$("#selector_importe1").val()+"</idformapago>";
	xml_lineasdetalle+="</formapago>";
	xml_lineasdetalle+="<importe>"+$("#importe1_text").val()+"</importe>";
	xml_lineasdetalle+="<rts/><numpedido/>";
	xml_lineasdetalle+="<bono><numeracion/>";
	xml_lineasdetalle+="<cliente><idcliente/></cliente>";
	xml_lineasdetalle+="</bono>";
	xml_lineasdetalle+="</Importeparcial>";
	xml_lineasdetalle+="<Importeparcial>";
	xml_lineasdetalle+="<formapago>";
	xml_lineasdetalle+="<idformapago>"+$("#selector_importe2").val()+"</idformapago>";
	xml_lineasdetalle+="</formapago>";
	xml_lineasdetalle+="<importe>"+$("#importe2_text").val()+"</importe>";
	xml_lineasdetalle+="<rts/><numpedido/>";
	xml_lineasdetalle+="<bono><numeracion/>";
	xml_lineasdetalle+="<cliente><idcliente/></cliente>";
	xml_lineasdetalle+="</bono>";
	xml_lineasdetalle+="</Importeparcial>";
	xml_lineasdetalle+="<Importeparcial>";	
	xml_lineasdetalle+="<formapago>";
	xml_lineasdetalle+="<idformapago>"+$("#selector_importe3").val()+"</idformapago>";
	xml_lineasdetalle+="</formapago>";
	xml_lineasdetalle+="<importe>"+$("#importe3_text").val()+"</importe>";
	xml_lineasdetalle+="<rts/><numpedido/>";
	xml_lineasdetalle+="<bono><numeracion/>";
	xml_lineasdetalle+="<cliente><idcliente/></cliente>";
	xml_lineasdetalle+="</bono>";
	xml_lineasdetalle+="</Importeparcial>";
	xml_lineasdetalle+="</importeparcials>";
	xml_lineasdetalle+="<importetotalventa>"+importe_total+"</importetotalventa>";
	xml_lineasdetalle+= construir_xml_lineasdetalle(dt_listlineas.rows().data());
	xml_lineasdetalle+="<modificacions/>";
	xml_lineasdetalle+= "<nif>"+$("#nif").val()+"</nif>";
	xml_lineasdetalle+= "<nombre>"+$("#nombre").val()+"</nombre>";
	xml_lineasdetalle+= "<observaciones>"+$("#observaciones").val()+"</observaciones>";
	xml_lineasdetalle+="<operacioncaja/>";
	xml_lineasdetalle+="<orden/>";
	xml_lineasdetalle+= "<reciboimpreso>"+imp_recibo+"</reciboimpreso>";
	xml_lineasdetalle+="<reserva><localizadoragencia/></reserva>";
	xml_lineasdetalle+= "<telefono>"+$("#telefono").val()+"</telefono>";
	xml_lineasdetalle+= "<telefonomovil>"+$("#movil").val()+"</telefonomovil>";
	xml_lineasdetalle+="<usuario/>";
	xml_lineasdetalle+="</Venta></getRealizarVentaLocalidadesNumeradas>";
	return(xml_lineasdetalle);
}

//********************************************************************************	

function saveFormVentaIndividual() {
	showSpinner("#modal-dialog-form .modal-content");
    
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/realizar_venta_numerada.do'/>",
		timeout : 100000,
		data: {
			xml: construir_xml_venta_numerada()
		},
		success : function(data) {
			hideSpinner("#modal-dialog-form .modal-content");
			showSpinner("#main-ventareserva");
			if(data.ok==undefined)
				new PNotify({
				      title: '<spring:message code="venta.numerada.realizada" />',
				      text: '<spring:message code="venta.numerada.realizada.bloqueo" /> ',
				      type : "success",
					  buttons: { sticker: false }				  
				   });
			else
				new PNotify({
				      title: '<spring:message code="venta.numerada.realizada" />',
				      text: '<spring:message code="venta.numerada.realizada.correctamente" /> '+data.ok.Integer,
				      type : "success",
					  buttons: { sticker: false }				  
				   });
			$("#modal-dialog-form-3").modal('hide');
			$("#modal-dialog-form-2").modal('hide');
			
			try {
			    // Si estamos desbloqueando
				dt_listlocalidades.ajax.reload();
			   
			} catch (e) {
			   //Si estamos vendientdo directamente
			   $("#modal-dialog-form").modal('hide');
			}
			
			
			
			if ($("#carta_confirmacion").is(":checked"))
				window.open("../../cartaConfirmacion.post?medioEnvio=&format="+$("#carta_confirmacion_select").val()+"&idioma="+$("#selector_idiomas").val()+"&idVenta="+data.ok.int,"_blank");						
		},
		error : function(exception) {
			hideSpinner("#modal-dialog-form .modal-content");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});		
}	

$("#save_venta_button").on("click", function(e) {
	$("#form_venta_individual_dialog").submit();
})

$("#form_venta_individual_dialog").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		saveFormVentaIndividual();		
	}
});


/* Función genérica para obtener el xml de las lineas de detalle en las operaciones de Venta y Reserva */
function construir_xml_lineasdetalle(lineas_detalle_sel) {
	var xml_lineasdetalle= "<lineadetalles>";
	for (var i=0; i<lineas_detalle_sel.length; i++) { 
		xml_lineasdetalle+="<Lineadetalle>";
		xml_lineasdetalle+=json2xml(lineas_detalle_sel[i][0],"");
		xml_lineasdetalle+="</Lineadetalle>";
	}
	xml_lineasdetalle+="</lineadetalles>";
	return(xml_lineasdetalle);
}

</script>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!-- Pesta�as ------------------------------------------------------------->
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 maintab">
		<div class="" role="tabpanel" data-example-id="togglable-tabs">
			<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
				<li role="presentation" class="active"><a href="#tab_ventas" id="ventas-tab" role="tab" data-toggle="tab" aria-expanded="true"><spring:message code="venta.numerada_busquedas.tabs.ventas.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_abonos" role="tab" id="abonos-tab" data-toggle="tab" aria-expanded="false"><spring:message code="venta.numerada_busquedas.tabs.abonos.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_clientes" role="tab" id="clientes-tab" data-toggle="tab" aria-expanded="false"><spring:message code="venta.numerada_busquedas.tabs.clientes.title" /></a></li>
				<li role="presentation" class=""><a href="#tab_localidades" role="tab" id="localidades-tab" data-toggle="tab" aria-expanded="false"><spring:message code="venta.numerada_busquedas.tabs.localidades.title" /></a></li>
			</ul>
		</div>
	</div>
</div>

<!-- Area de trabajo ------------------------------------------------------>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel thin_padding">

			<div id="myTabContent" class="tab-content">
				<div role="tabpanel" class="tab-pane fade active in" id="tab_ventas" aria-labelledby="ventas-tab">
					<tiles:insertAttribute name="tab_ventas" />
				</div>

				<div role="tabpanel" class="tab-pane fade" id="tab_abonos" aria-labelledby="abonos-tab">
					<tiles:insertAttribute name="tab_abonos" />
				</div>

				<div role="tabpanel" class="tab-pane fade" id="tab_clientes" aria-labelledby="clientes-tab">
					<tiles:insertAttribute name="tab_clientes" />
				</div>

				<div role="tabpanel" class="tab-pane fade" id="tab_localidades" aria-labelledby="localidades-tab">
					<tiles:insertAttribute name="tab_localidades" />
				</div>
			</div>

			<div class="clearfix"></div>
			
		
		</div>
	</div>
</div>

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-2"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-3"/>
<tiles:insertAttribute name="modal_dialog" />



<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-buscar-cliente"/>
<tiles:insertAttribute name="modal_dialog" />

<tiles:insertAttribute name="modal_desbloquear_localidades" />

<tiles:insertAttribute name="modal_imprimir_entradas" />

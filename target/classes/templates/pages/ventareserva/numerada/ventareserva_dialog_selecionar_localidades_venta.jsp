<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>		
</div>

<div class="modal-body">
	<form id="seleccionar_butacas" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input type="hidden" name="vender" id="vender" value="${vender}"/>
		<input type="hidden" name="idSesion" id="idSesion" value="${idSesion}"/>
		<div class="col-md-12 col-sm-12 col-xs-12">	
			<div class="form-group">
					<label class="control-label col-md-2 col-sm-12 col-xs-12"><spring:message code="venta.numerada.tabs.butacas.select.zonas" /></label>
					<div class="col-md-5 col-sm-12 col-xs-12">
						<select class="form-control" name="idzona" id="idzona">
							<option value=""></option>							
						</select>
					</div>
					<div class="btn-group pull-right btn-datatable">					
				<a type="button" class="btn btn-info" id="tab_localidades_sell">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada.vender" />"> <span class="fa fa-shopping-cart"></span>
					</span>
				</a>	
				<a type="button" class="btn btn-info" id="tab_localidades_lock">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada.bloquear" />"> <span class="fa fa-lock"></span>
					</span>
				</a>
				<a type="button" class="btn btn-info" id="tab_localidades_unlock">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada_busquedas.tabs.localidades.button.desbloquear" />"> <span class="fa fa-unlock"></span>
					</span>
				</a>		
			</div>
			</div>	
			
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12">		
			<div id='Pantalla' style='height:auto;width:900px;background-color:#F0EEEE'>				
			</div>
		</div>
	</form>
</div>
<script>

var zonas = ${zonas};
var precios = ${precios};
var zonasNombre = ${zonasNombre};
var map = new Object();
var localidades;
var mapActual = new Object();

var vender = $("#vender").val();


if(vender=="1")
	{
	$("#tab_localidades_lock").hide();
	$("#tab_localidades_unlock").hide();
	}
else
	$("#tab_localidades_sell").hide();

cargarZonas();

function cargarZonas()
{
	
	
	var $select=$("#idzona");
	
	var disponibilidad = zonas.ArrayList.DisponibilidadZonaGrafica;
	if(disponibilidad.length>1)
		$.each(zonas.ArrayList.DisponibilidadZonaGrafica, function(key, val){
			  
			  $select.append('<option value="' + val.zonafisica + '">' + " zona "+conseguirNombreZona(val.zonafisica)+" libres " +val.libres + '</option>');
		    });
	else
		{
		
		$select.append('<option value="' + disponibilidad.zonafisica + '">' + " zona "+conseguirNombreZona(disponibilidad.zonafisica)+" libres " +disponibilidad.libres + '</option>');
		}
}

//**********************************************
function conseguirNombreZona(zona)
{
	var zona = ""+zona.trim();	
	var valor="";	
	var zonasIter = zonasNombre.ArrayList.ZonaFisicaNombreArea;
	if(zonasIter.length>1)
		{
		$.each(zonasNombre.ArrayList.ZonaFisicaNombreArea, function(key, valNombre){
			var zonaNombreZona =  ""+valNombre.zonafisica.trim();
		    if(zonaNombreZona==zona)	    	
				valor=valNombre.nombrearea;	    	
		    });  	
		}
	else
		{
		valor=zonasIter.nombrearea;	 
		}
 
	return valor;
}

//**********************************************
function conseguirLocalidad(idLocalidad)
{
	var idLocalidad = ""+idLocalidad;	
		var valor="";
		
	$.each(localidades.ArrayList.localidad, function(key, localidad){		
					
	    var idLocalidadBucle =  ""+localidad.id;
	    if(idLocalidadBucle==idLocalidad)	    	
			valor=localidad	    	
	    });   
	return valor;
}

//**********************************************
function conseguirPrecioZona(idzona)
{
	var valor="";	
	$.each(precios.ArrayList.ItemZonaLeyenda, function(key, valPrecio){					
	   
	    if(valPrecio.idzona==idzona)	    	
			valor=valPrecio.importe;	    	
	    });   
	return valor;
}

//********************************************************************
$("#tab_localidades_sell").on("click", function(e) 
{ 
	//Si el HashMap no tiene elementos es que no hay butacas seleccionadas
	var num=Object.keys(mapActual).length;	 
	
	

	if (num<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.numerada_busquedas.tabs.localidades.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	VenderLocalidades()
})

//********************************************************************
$("#tab_localidades_lock").on("click", function(e) 
{ 
	//Si el HashMap no tiene elementos es que no hay butacas seleccionadas
	var num=Object.keys(mapActual).length;	 
	
	

	if (num<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.numerada_busquedas.tabs.localidades.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	BloquearLocalidades()
})



//**********************************************
function generarXmlLocalidades()
{
	var idSesion = "${idSesion}";
	var xml="<list>";
	map = $.extend({}, map, mapActual);	
	
	for (var idLoclidad in map)
		{
		  xml+="<Estadolocalidad><sesion><idsesion>"+idSesion+"</idsesion></sesion><localidad><idlocalidad>"+idLoclidad+"</idlocalidad></localidad></Estadolocalidad>";
		}
		
	xml=xml+"</list>";
	
	return xml;
}
//**********************************************
function BloquearLocalidades()
{
	
	var xml=generarXmlLocalidades();	
	xml+="";
	$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='//ajax/venta/numerada/bloquear_localidades.do'/>?"+encodeURI("xml="+xml), function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "lg");
	});
}
//**********************************************
function VenderLocalidades()
{	
	var xml=generarXmlLocalidades();	
	xml+="";
	$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='//ajax/venta/numerada/vender_localidades_bloqueadas.do'/>?"+encodeURI("xml="+xml), function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "lg");
	});
}

//**********************************************
function conseguirEstado(idEstado)
{
	idEstado = ""+idEstado;
	if(idEstado=="1")
		return "Libre";
	if(idEstado=="2")
		return "Reservada";
	if(idEstado=="3")
		return "Prereservada";
	if(idEstado=="4")
		return "Bloqueada Abono";
	if(idEstado=="5")
		return "Vendida Abono";
	if(idEstado=="6")
		return "No disponible";
	if(idEstado=="7")
		return "Bloqueada";
	if(idEstado=="8")
		return "Vendida";

}
//**********************************************
function cambiarZona()
{
	var idZona = ""+$("#idzona").val();
	

	
	var idSesion = "${idSesion}";
	if(idZona!="")
	{
			
	$.ajax({
		type : "post",
		url : "<c:url value='/ajax/venta/numerada/obtener_localidades_porSesionYZona.do'/>",
		timeout : 100000,
		data: {
			idzonafisica: idZona,
			idSesion: idSesion
		  }, 
		success : function(data) {

			$(".localidades").remove();
			var zona = conseguirNombreZona(idZona);
			var maxX=0,maxY=0,minX=2000,minY=2000;
			var nfilMax=0,nfilMin=2000,ncolMax=0,ncolMin=2000;
			localidades=data;		
			$.each(data.ArrayList.localidad, function(key, valLocalidad){	
				if(maxX<valLocalidad.x)
					maxX=valLocalidad.x;
				if(maxY<valLocalidad.y)
					maxY=valLocalidad.y;	
				if(minX>valLocalidad.x)
					minX=valLocalidad.x;
				if(minY>valLocalidad.y)
					minY=valLocalidad.y;		
				if(nfilMax<valLocalidad.f)
					nfilMax=valLocalidad.f;
				if(nfilMin>valLocalidad.f)
					nfilMin=valLocalidad.f;	
				if(ncolMax<valLocalidad.b)
					ncolMax=valLocalidad.b;
				if(ncolMin>valLocalidad.b)
					ncolMin=valLocalidad.b;
			})
			
			
			var amplitudX = maxX - minX;
		    var mulX = 800.0/amplitudX;		    
		    
		   	
			
			var amplitudY = maxY - minY;
			var mulY = 800.0/amplitudY;
			var nfilas = nfilMax - nfilMin;
			
			var nColumnas = ncolMax - ncolMin;
			var altoButaca  = (780.0/(1.0*nColumnas))-10.0;	
			altoButaca = 30;
			
			var topCapa = 30+40*(nColumnas+1);
			$("#Pantalla").css("height",""+topCapa+"px");
			
			var anchoButaca = (750.0/(1.0*nfilas))-10.0;
			
			$.each(data.ArrayList.localidad, function(key, valLocalidad){	
				var precio =conseguirPrecioZona(valLocalidad.idzona);
				var idEstado = valLocalidad.estado;
				var estado = conseguirEstado(idEstado);

			    var topButaca = 30+40*(valLocalidad.b-ncolMin);
			    var url="<c:url value='/resources/images/butacas/libre.png'/>";
			    if(idEstado==4 || idEstado==7)
			    	url = "<c:url value='/resources/images/butacas/butaca_bloqueada_localidad.gif'/>";
				if(idEstado==5 || idEstado==8)
				   	url = "<c:url value='/resources/images/butacas/butaca_vendida_localidad.gif'/>";
				var capa = '<div l="Y" estadoAntiguo="'+idEstado+'" estado="'+idEstado+'" id="'+valLocalidad.id+'" a="62" z="326" zc="N" class="localidades" style=" height: '+altoButaca+'px;max-width:35;width: '+anchoButaca+'px;position: absolute; top:' + topButaca +'px; left: '+ (-8+mulX*valLocalidad.x) +'px;" ro="-69.327896" e="o" n="D-38" v="100" alt="Fila:D-Asiento:38" title="Precio: '+precio+'  zona : '+zona+'- Fila: '+ valLocalidad.f +' Asiento: '+ valLocalidad.b +' Estado: '+estado+'" >';
				
				capa=capa+'<img style=" width:90%; height:90%;" src="'+url+'" /></div>';
				jQuery('#Pantalla').append(capa)
				
			});
			
			
						
			$(".localidades").on("click", function(e) 
					{ 						
						var icon = $(this).find("img");
						
						if($(this).attr('estado')!="-1")
							{
							var estado = ""+$(this).attr('estado');
							if(estado!="8")
								{
								var url_seleccionado="<c:url value='/resources/images/butacas/seleccionado.png'/>";
								mapActual[$(this).attr('id')] = $(this).attr('id');	
								$(this).attr('estado',"-1");								
								icon.attr("src", url_seleccionado);
								}						
							}
						else
							{
							var estado_antiguo = $(this).attr('estadoAntiguo');
							
							delete mapActual[$(this).attr('id')];		
						    var url="<c:url value='/resources/images/butacas/libre.png'/>";
						    if(estado_antiguo=='4' || estado_antiguo=='7')
						    	url = "<c:url value='/resources/images/butacas/butaca_bloqueada_localidad.gif'/>";
							if(estado_antiguo=='5' || estado_antiguo=='8')
							   	url = "<c:url value='/resources/images/butacas/butaca_vendida_localidad.gif'/>";
							$(this).attr('estado',estado_antiguo);	

							icon.attr("src", url);
							}						
					})

			
			
			
			
			
		},
		error : function(exception) {
			hideSpinner("#modal-dialog-form .modal-content");

			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : exception.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		}
	});		
	}
}

//**********************************************************************
$("#idzona").on("change", function(e) {
	var numElements = Object.keys(mapActual).length;	
	if(numElements>0)
		 new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="venta.numerada.tabs.butacas.prereservar.text" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
	 		  buttons: { closer: false, sticker: false	},
	 		  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {	
			   map = $.extend({}, map, mapActual);				   
			   mapActual = new Object();
			   cambiarZona();

		   }).on('pnotify.cancel', function() {			   
			   mapActual = new Object();
			   map = new Object();
			   cambiarZona()
		   });
	else
		cambiarZona()
	
	
	
})

//********************************************************************************
$("#tab_localidades_unlock").on("click", function(e) { 
	
	//Si el HashMap no tiene elementos es que no hay butacas seleccionadas
	var num=Object.keys(mapActual).length;	 
	
	

	if (num<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.numerada_busquedas.tabs.localidades.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	var vendidas=false;
	//for (var i = 0; i < data.length; i++) {
	//	if (data[i].estado.idestado==8) vendidas=true; 
	//}

	if (vendidas) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.numerada_busquedas.tabs.localidades.list.alert.seleccion_vendida" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	
	$("#modal-dialog-localidades-desbloquear").modal('show');
	setModalDialogSize("#modal-dialog-localidades-desbloquear", "md");
}); 

	

</script>
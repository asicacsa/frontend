<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_resumen}" var="ventareserva_resumen_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.venta_resumen.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_venta_resumen_dialog" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input type="hidden" name="idcliente_fiscal" id="idcliente_fiscal" value="<x:out select = "$ventareserva_resumen_xml/Venta/clientefiscal/idcliente" />">

		<div id="columna-izquierda" class="col-md-5 col-sm-12 col-xs-12">
		
			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.refventa" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$ventareserva_resumen_xml/Venta/idventa" /></div>
			</div>
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.fechaventa" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$ventareserva_resumen_xml/Venta/fechayhoraventa" /></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.usuario" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$ventareserva_resumen_xml/Venta/usuario/nombre" />&nbsp;<x:out select = "$ventareserva_resumen_xml/Venta/usuario/apellidos" /></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.canal" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$ventareserva_resumen_xml/Venta/canalByIdcanal/nombre" /></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.estado" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$ventareserva_resumen_xml/Venta/estadooperacion/nombre" /></div>
			</div>

			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.emision_bono.field.observaciones" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<textarea class="form-control" rows="3" name="observaciones" id="observaciones" disabled="disabled"><x:out select = "$ventareserva_resumen_xml/Venta/observaciones" /></textarea>
				</div>
			</div>					
		</div>

		<div id="columna-central" class="col-md-5 col-sm-12 col-xs-12">
		
			<div class="callcenter">
			
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.cliente" /></label>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<input type="text" name="idcliente" id="idcliente" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/cliente/idcliente" />" disabled="disabled"/>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<input type="text" name="nombrecliente" id="nombrecliente" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/cliente/nombre" />"  disabled="disabled"/>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.emision_bono.field.nombre" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input type="text" name="nombre" id="nombre" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/nombre" />" disabled="disabled"/>
					</div>
				</div>
				
				<div class="form-group button-dialog capa_cp_dialog_emision_bono">
	
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="venta.ventareserva.dialog.emision_bono.field.nif" /></label>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<input type="text" name="nif" id="nif" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/nif" />"  disabled="disabled" />
						</div>
					</div>
				
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.emision_bono.field.cp" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<input id="cp_callcenter" type="text" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/cp" />"  disabled="disabled" />
						</div>
					</div>
					
				</div>		
				
				
				<div class="form-group">
	
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="venta.ventareserva.dialog.emision_bono.field.telefono" /></label>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<input type="text" name="telefono" id="telefono" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/telefono" />" disabled="disabled" />
						</div>
					</div>
				
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.emision_bono.field.movil" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<input type="text" name="movil" id="movil" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/movil" />" disabled="disabled" />
						</div>
					</div>
					
				</div>		
	
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.emision_bono.field.email" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input type="text" name="email" id="email" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/email" />"  disabled="disabled" />
					</div>
				</div>
					
			</div>

			<div id="taquilla" style="display:none;">
			
				<div class="form-group button-dialog capa_cp_dialog_emision_bono">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.emision_bono.field.cp" /></label>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<input id="cp_taquilla" type="text" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/cp" />" disabled="disabled"/>
					</div>
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.emision_bono.field.pais" /></label>
					<div class="col-md-2 col-sm-2 col-xs-2">
						<input id="pais_dialog_emision_bono" type="text" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/pais/nombre" />"  disabled="disabled" />
					</div>
				</div>		
			</div>
					
		</div>
		
		<div id="columna-derecha" class="col-md-2 col-sm-12 col-xs-12">
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<span class="fa" id="check-facturada"/>&nbsp;<spring:message code="venta.ventareserva.dialog.venta_resumen.field.facturada" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<span class="fa" id="check-recibo"/>&nbsp;<spring:message code="venta.ventareserva.dialog.venta_resumen.field.recibo" />
				</div>
			</div>
		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
		
			<div class="totales-group totales_venta_resumen">
				Total: <span class="total-lbl"><span id="total-val">0.00</span>&euro;</span>&nbsp;<span class="descuento-lbl">Descuento: <span id="total-desc">0.00</span>&euro;&nbsp;(<span id="total-prc">0.00</span>%)</span>
			</div>
		
			<div class="btn-group pull-right btn-datatable">
				<a type="button" class="btn btn-info" id="button_venta_resumen_recibo">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.dialog.venta_resumen.button.recibo" />"> <span class="fa fa-print"></span>
					</span>
				</a>
			</div>
		
			<table id="datatable-list-venta-resumen" class="table table-striped table-bordered dt-responsive ventareserva" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th></th>
						<th><spring:message code="venta.ventareserva.tabs.emision_bono.list.header.producto" /></th>
						<th><spring:message code="venta.ventareserva.tabs.emision_bono.list.header.fechainicio" /></th>
						<th><spring:message code="venta.ventareserva.tabs.emision_bono.list.header.fechafin" /></th>
						<th><spring:message code="venta.ventareserva.tabs.emision_bono.list.header.bonos" /></th>
						<th><spring:message code="venta.ventareserva.tabs.emision_bono.list.header.perfil" /></th>
						<th><spring:message code="venta.ventareserva.tabs.emision_bono.list.header.tipo" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.importeunitario" /></th>
						<th><spring:message code="venta.ventareserva.tabs.emision_bono.list.header.importe" /></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>		

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="checkin_button" type="button" class="btn save_dialog">
					<spring:message code="common.button.checkin" />
				</button>
				<button id="accept_button" type="button" class="btn btn-primary close_dialog" data-dismiss="modal">
					<spring:message code="common.button.accept" />
				</button>
			</div>
		</div>
		
	</form>

</div>

<script>
hideSpinner("#main-ventareserva");

var dt_listventaresumen=$('#datatable-list-venta-resumen').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	scrollCollapse: true,
	ordering:  false,
	paging: false,
    select: { style: 'single' },
    "columnDefs": [
                   { "visible": false, "targets": [0]}
                 ],
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('datatable-list-venta-resumen');
   	}
} );

function ajustar_cabeceras_datatable()
{
	$('#datatable-list-venta-resumen').DataTable().columns.adjust().draw();
}

/*
 * Funciones para cargar las tablas en las ventas de resumen
 */

 function CargarLineasDetalle()	{
	var json = ${ventareserva_resumen_json};
	var i = 0;
	
	var lineasdetalle = json.Venta.lineadetalles;
			
	if (lineasdetalle !=""){
		if (lineasdetalle.Lineadetalle!="") {
	        var item=lineasdetalle.Lineadetalle;
	        if (item.length>0)
	            $.each(item, function(key, lineadetalle){
	            	CargarLineaDetalle(lineadetalle,dt_listventaresumen,json)
	            });
	        else
	        	CargarLineaDetalle(item,dt_listventaresumen,json)
		}
	}
}

function CargarLineaDetalle(lineadetalle,tabla,json) {	
	var lineaFalla= lineadetalle;
	console.log(lineadetalle);
	var Bonos = lineadetalle.bonosForIdlineadetalle.Bono;
	
	if (Bonos.length>1)
	  Bonos = Bonos[0];
	console.log(Bonos);
	tabla.row.add([
		lineadetalle,
        lineadetalle.producto.nombre,
        Bonos.fechaemision.split("-")[0], // Fecha inicio
        Bonos.fechacaducidad.split("-")[0], // Fecha fin
        lineadetalle.cantidad,  // Nº bonos
        lineadetalle.perfilvisitante.nombre, // Perfil/Tarifa
        Bonos.tipobono.nombre, // Tipo bono
        "", // Importe unitario (no viene en el xml, pero en el diálogo está puesto. Se deja en blanco)
        typeof lineadetalle.importe=="undefined"?lineadetalle.tarifaproducto.importe:lineadetalle.importe // Importe
	]);
}

//*****************************************************************************************

var facturada= Number("<x:out select = "$ventareserva_resumen_xml/Venta/facturada" />"),
	reciboimpreso= Number("<x:out select = "$ventareserva_resumen_xml/Venta/reciboimpreso" />");
	
$("#check-facturada").addClass(facturada>0?'fa-check':'fa-close');
$("#check-facturada").css("color",facturada>0?'green':'red');
$("#check-recibo").addClass(reciboimpreso>0?'fa-check':'fa-close');
$("#check-recibo").css("color",reciboimpreso>0?'green':'red');

CargarLineasDetalle();

$('#datatable-list-venta-resumen').DataTable().columns.adjust().draw();
setTimeout(ajustar_cabeceras_datatable, 3000);
//obtener_totales_venta_temporal("venta_resumen","emision_bono",$("#idcliente_emision_bono").val(),4,true);

// No funciona obtener totales venta temporal para Bonos. Pongo los mismos valores que en la pantalla origen de emisión de bono

$(".totales_venta_resumen #total-val").text($(".totales_emision_bono #total-val").text());
$(".totales_venta_resumen #total-desc").text($(".totales_emision_bono #total-desc").text());
$(".totales_venta_resumen #total-prc").text($(".totales_emision_bono #total-prc").text());

// Lanza la impresión de los bonos de la venta. No sé si es necesario, de momento se deja comentado

/*$.ajax({
	contenttype: "application/json; charset=utf-8",
	type : "post",
	url : "<c:url value='/ajax/venta/ventareserva/imprimir_bono_venta.do'/>",
	timeout : 100000,
	data: {
		idventa: "<x:out select = "$ventareserva_resumen_xml/Venta/idventa" />"
	},
	success : function(data) {

	},
	error : function(exception) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
		      text: exception.responseText,
			  type: "error",
			  delay: 5000,
			  buttons: { closer:true, sticker: false }			  
		   });		
	}
});*/		   

//********************************************************************************
$("#button_venta_resumen_recibo").on("click", function(e) { 
	showButtonSpinner("#button_venta_resumen_recibo");
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/imprimir_recibo.do'/>",
		timeout : 100000,
		data: {
			idventa: "<x:out select = "$ventareserva_resumen_xml/Venta/idventa" />"
		},
		success : function(data) {
			hideSpinner("#button_venta_resumen_recibo");
		},
		error : function(exception) {
			hideSpinner("#button_venta_resumen_recibo");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});		   
}); 

//********************************************************************************
$("#checkin_button").on("click", function(e) {
	new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="venta.ventareserva.dialog.venta_resumen.confirm.facturar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
	  		  buttons: { closer: false, sticker: false	},
	  		  history: { history: false	}
    }).get().on('pnotify.confirm', function() {
    	showSpinner("#modal-dialog-form .modal-content");
		$.ajax({
	          contenttype: "application/json; charset=utf-8",
	          type : "post",
	          url : "<c:url value='/ajax/venta/ventareserva/generar_factura.do'/>",
	          timeout : 100000,
	          data: {
	      	   		idventa: "<x:out select = "$ventareserva_resumen_xml/Venta/idventa" />"
	          },
	          success : function(data) {
	       		 	hideSpinner("#modal-dialog-form .modal-content");
	                var idFactura = data.ArrayList.ResultadoFacturacion.factura.idfactura
	                window.open("../../jasper.post?idFactura="+idFactura+"&format=pdf&viewOnly=0&report=FacturaBonoPrepagoYRectificativa","_blank");
	                $("#modal-dialog-form .modal-content").load("<c:url value='/venta/ventareserva/busqueda/ventareserva/editar_venta_bono.do'/>?id="+idVenta, function() {
	                });
	          },
	          error : function(exception) {
	     		 	 hideSpinner("#modal-dialog-form .modal-content");
	                new PNotify({
	                      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
	                      text: exception.responseText,
	                         type: "error",                
	                         delay: 5000,
	                         buttons: { sticker: false }
	                });              
	          }
	    });
    });
	hideSpinner("#modal-dialog-form .modal-content");
});

//********************************************************************************
$("#accept_button").on("click", function(e) {
    //dt_listventaindividual.rows( { selected: true } ).remove().draw();
    dt_listemisionbono.rows().remove().draw();
    obtener_totales_venta_temporal("emision_bono","emision_bono",$("#idcliente_emision_bono").val(),4,true);
});

</script>
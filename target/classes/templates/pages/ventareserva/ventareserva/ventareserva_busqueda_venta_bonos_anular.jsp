<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${motivosModificacion}" var="motivosModificacion_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal"
		aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.busqueda.tabs.venta-bono.anular.title" />					
	</h4>
</div>
<div class="modal-body">
	<form id="form_anular_venta_bono" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	<input type="hidden" name="idVenta" id="idVenta" value="${idVenta}"> 
	<input type="hidden" name="xmlVentaBonos" id="xmlVentaBonos" > 
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="form-group">
			<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.busqueda.tabs.venta-bonos.list.field.motivosAnulacion" /></label>
			<div class="col-md-8 col-sm-8 col-xs-6">
				<select class="form-control" name="motivo_anulacion_venta_bono" id="motivo_anulacion_venta_bono">
					<x:forEach select="$motivosModificacion_xml/ArrayList/LabelValue" var="item">
						<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
					</x:forEach>
				</select>
			</div>
		</div>	
		
		<div class="form-group">
			<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.entradas.list.field.observaciones" /></label>
			<div class="col-md-8 col-sm-8 col-xs-6">
				<textarea class="form-control"   id ="motivos_anulacion_bono_obs" name="motivos_anulacion_bono_obs"></textarea>	
			</div>
		</div>	
	</div>
	</form>
	
	<div class="modal-footer">
		<button type="button" id="anular_venta_bono" class="btn aceptar_activar_dialog"
			data-dismiss="modal">
			<spring:message code="common.button.ok" />
		</button>
		<button type="button" class="btn btn-success close_dialog"
			data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>

</div>
			


<script>
//*****************************************************
$("#anular_venta_bono").on("click", function(e) {
	
	var accion = $("#bonoMotivosAction").val();
	
	var xml = "";
		
	xml="<Modificacion><idmodificacion/>";
	xml+="<motivomodificacion>"+$("#motivo_anulacion_venta_bono option:selected").text()+"</motivomodificacion>";
	xml+= "<observaciones>"+$("#motivos_anulacion_bono_obs").val()+"</observaciones>";
	xml+="<venta><idventa>"+$("#idVenta").val()+"</idventa>";
    xml+="<entradasimpresas>false</entradasimpresas><reciboimpreso>0</reciboimpreso><imprimirSoloEntradasXDefecto>false</imprimirSoloEntradasXDefecto></venta>";
	xml+="<reserva><idreserva/></reserva>";
	xml+="<modificacionimporteparcials><Modificacionimporteparcial><importeparcial><formapago><idformapago></idformapago></formapago><bono><numeracion/></bono><importe></importe><rts/><numpedido/><anulado/></importeparcial></Modificacionimporteparcial><Modificacionimporteparcial><importeparcial><formapago><idformapago></idformapago></formapago><bono><numeracion/></bono><importe></importe><rts/><numpedido/><anulado/></importeparcial></Modificacionimporteparcial><Modificacionimporteparcial><importeparcial><formapago><idformapago></idformapago></formapago><bono><numeracion/></bono><importe></importe><rts/><numpedido/><anulado/></importeparcial></Modificacionimporteparcial></modificacionimporteparcials><modificacionlineadetalles><Modificacionlineadetalle><lineadetalle><idlineadetalle/></lineadetalle>";
	xml+="</Modificacionlineadetalle></modificacionlineadetalles><anulaciondesdecaja/>";
    xml+="</Modificacion>"
	
    
    $("#xmlVentaBonos").val(xml);
    
	var data = $("#form_anular_venta_bono").serializeObject();
	
	
	
	
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='//ajax/ventareserva/ventabono/anular.do'/>",
		timeout : 100000,
		data: data, 
		success : function(data) {
			dt_listventabono.ajax.reload();
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
}) 
</script>
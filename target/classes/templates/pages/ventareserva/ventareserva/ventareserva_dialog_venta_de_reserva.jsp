<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_formaspago}" var="ventareserva_formaspago_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.venta_de_reserva.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_venta_individual_dialog" data-parsley-validate="" autocomplete="off" class="form-horizontal form-label-left" novalidate="">
		<input type="hidden" id="nif" name="nif"/>
		<div id="columna-izquierda" class="col-md-6 col-sm-12 col-xs-12">
			<div id="taquilla">			
				<div class="form-group button-dialog capa_cp_dialog_venta_individual">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.cp" />*</label>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<button id='taquilla-search-button' type='button' class='btn btn-default btn-i18n'>
							<span class='docs-tooltip' data-toggle='tooltip' title='' data-original-title='Buscar CP'><span class='fa fa-search'></span></span>
						</button>
						<div class="input-prepend">
							<input id="cp_taquilla_reserva" name="cp_taquilla_reserva" type="text" class="form-control" value=""/>
						</div>
					</div>
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.pais" /></label>
					<div class="col-md-2 col-sm-2 col-xs-2">
						<input id="pais_reserva" name="pais_reserva" type="text" class="form-control" value=""/>
					</div>
				</div>		
			</div>
				
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.observaciones" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<textarea class="form-control" rows="3" name="observaciones_reserva" id="observaciones_reserva"></textarea>
				</div>
			</div>

			<div class="x_title">
				<h5><spring:message code="venta.ventareserva.dialog.venta_individual.field.opciones_impresion" /></h5>
				<div class="clearfix"></div>
			</div>

			<div class="form-group">
				<div class="checkbox col-md-3 col-sm-3 col-xs-3">
					<input type="checkbox" name="selector" id="oimprecibo" value="" class="flat" style="position: absolute; opacity: 0;">
					<strong><spring:message code="venta.ventareserva.dialog.venta_individual.field.recibo" /></strong>
				</div>
				<div class="checkbox col-md-3 col-sm-3 col-xs-3">
					<input type="checkbox" name="selector" id="oimpcopia" value="" class="flat" style="position: absolute; opacity: 0;">
					<strong><spring:message code="venta.ventareserva.dialog.venta_individual.field.copia" /></strong>
				</div>
				<div class="checkbox col-md-3 col-sm-3 col-xs-3">
					<input type="checkbox" name="selector" id="oimpentradas" value="" class="flat" style="position: absolute; opacity: 0;">
					<strong><spring:message code="venta.ventareserva.dialog.venta_individual.field.entradas" /></strong>
				</div>
			</div>

			<div class="form-group">
				<div class="checkbox col-md-12 col-sm-12 col-xs-12">
					<input type="checkbox" name="selector" id="oimpentradasdef" value="" class="flat" checked style="position: absolute; opacity: 0;">
					<strong><spring:message code="venta.ventareserva.dialog.venta_individual.field.entradas_defecto" /></strong>
				</div>
			</div>
					
		</div>

		<div id="columna-derecha" class="col-md-6 col-sm-12 col-xs-12">

			<div class="x_title">
				<h5><spring:message code="venta.ventareserva.dialog.venta_individual.field.forma_pago" /></h5>
				<div class="clearfix"></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 forma-pago">
   				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
			            <label class="control-label col-md-4 col-sm-4 col-xs-4">
			            	<spring:message code="venta.ventareserva.dialog.venta_individual.field.imp_cobrar" />                               
			            </label>
			            <label class="control-label col-md-8 col-sm-8 col-xs-8 euros">
							<span class="total-lbl"><span id="total-cobrar">0.00</span>&euro;</span>		
			            </label>
		            </div>
		        </div>
   				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
	                   <label class="control-label col-md-4 col-sm-4 col-xs-12">
                       		<spring:message code="venta.ventareserva.dialog.venta_individual.field.imp_entregado" />                               
                       </label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<div class="input-prepend">
								<input name="importe1_text" id="importe1_text" type="text" class="form-control importe" disabled="disabled">
							</div>
							<span class="importe">&euro;</span>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12">
	                       <select name="importe1" id="selector_importe1" class="form-control">
								<option value=""></option>
								<x:forEach select="$ventareserva_formaspago_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
					       </select>
						</div>                       
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
	                   <label class="control-label col-md-4 col-sm-4 col-xs-12">
                       		<spring:message code="venta.ventareserva.dialog.venta_individual.field.imp_entregado" />                               
                       </label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input name="importe2_text" id="importe2_text" type="text" class="form-control importe" disabled="disabled">
							<span class="importe">&euro;</span>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12">
	                       <select name="importe2" id="selector_importe2" class="form-control">
								<option value=""></option>
								<x:forEach select="$ventareserva_formaspago_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
					       </select>
						</div>                       
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
	                   <label class="control-label col-md-4 col-sm-4 col-xs-12">
                       		<spring:message code="venta.ventareserva.dialog.venta_individual.field.imp_entregado" />                               
                       </label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input name="importe3_text" id="importe3_text" type="text" class="form-control importe" disabled="disabled">
							<span class="importe">&euro;</span>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12">
	                       <select name="importe3" id="selector_importe3" class="form-control" >
								<option value=""></option>
								<x:forEach select="$ventareserva_formaspago_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
					       </select>
						</div>                       
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
			            <label class="control-label col-md-4 col-sm-4 col-xs-4">
			            	<spring:message code="venta.ventareserva.dialog.venta_individual.field.imp_devolver" />                               
			            </label>
			            <label class="control-label col-md-8 col-sm-8 col-xs-8 euros">
							<span class="total-lbl"><span id="total-devolver">0.00</span>&euro;</span>		
			            </label>
		            </div>
		        </div>
				
			</div>
			
			<div class="callcenter">

				<div class="x_title">
					<h5>&nbsp;</h5>
					<div class="clearfix"></div>
				</div>

				<div class="form-group">
					<div class="checkbox col-md-6 col-sm-6 col-xs-12">
						<input type="checkbox" name="selector" id="carta_reserva" value="" class="flat" style="position: absolute; opacity: 0;">
						<span style="float: right;"><strong><spring:message code="venta.ventareserva.dialog.venta_individual.field.carta_reserva" /></strong></span>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
                       <select name="carta_reserva_select" id="carta_reserva_select" class="form-control">
							<option value="pdf"><spring:message code="venta.ventareserva.dialog.venta_individual.field.pdf" /></option>
							<option value="email"><spring:message code="venta.ventareserva.dialog.venta_individual.field.email" /></option>
							<option value="fax"><spring:message code="venta.ventareserva.dialog.venta_individual.field.fax" /></option>
				       </select>
					</div>                       
				</div>
				
				
				<div class="form-group">
					<div class="checkbox col-md-6 col-sm-6 col-xs-12">
						<input type="checkbox" name="selector" id="carta_venta" value="" class="flat" style="position: absolute; opacity: 0;">
						<span style="float: right;"><strong><spring:message code="venta.ventareserva.dialog.venta_individual.field.carta_venta" /></strong></span>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
                       <select name="carta_venta_select" id="carta_venta_select" class="form-control">
							<option value="pdf"><spring:message code="venta.ventareserva.dialog.venta_individual.field.pdf" /></option>
							<option value="email"><spring:message code="venta.ventareserva.dialog.venta_individual.field.email" /></option>
							<option value="fax"><spring:message code="venta.ventareserva.dialog.venta_individual.field.fax" /></option>
				       </select>
					</div>                       
				</div>
			
				<div class="form-group">
                   <label class="control-label col-md-6 col-sm-6 col-xs-12">
                  		<spring:message code="venta.ventareserva.dialog.venta_individual.field.idioma_carta" />                               
                    </label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select name="selector_idiomas_confirmar" id="selector_idiomas_confirmar" class="form-control" style="width: 100%">
													
						</select>
						
					</div>                       
				</div>

			</div>

		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_venta_button" type="button" class="btn btn-primary save_dialog">
					<spring:message code="common.button.confirm" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
		
	</form>

</div>

<script>

hideSpinner("#tab_venta_reserva_confirmar");
hideSpinner("#confirm_button");
hideSpinner("#main-ventareserva");
hideSpinner("#main-busquedas");
var json_reserva = ${json_reserva};


cargarSelectorIdiomas("selector_idiomas_confirmar");

//var importe_total=Number($(".totales_venta_individual #total-val").text()),
//var importe_total=Number(dt_listbusqueda.rows( { selected: true } ).data()[0].importe),
var importe_total=Number(json_reserva.Venta.reserva.importetotalreserva),
	importe_devolver= -importe_total,
	importe_pendiente=0,
	idpais= json_reserva.Venta.pais.idpais,
	tipocanal= "${sessionScope.idTipocanal}";
	
if (idpais=="") idpais= "472";  // Si no se indica pais se establece España por defecto


// Si el tipo de canal es "taquilla" se muestran los campos correspondientes

//if (tipocanal=="1") {
	//$(".taquilla").show();
	//$(".callcenter").hide();
	//$("#cp_taquilla_reserva").prop('required',true);
	//$("#oimprecibo").prop('checked',true);
	//$("#oimpentradas").prop('checked',true);
//}
//else {
	//$(".taquilla").hide();
	//$(".callcenter").show();
	$("#cp_callcenter_reserva").prop('required',true);
//	$("#nombre_reserva").prop('required',true);
//}


$("#total-cobrar").text(importe_total.toFixed(2));
$("#total-devolver").text(importe_devolver.toFixed(2));

function calcularPendiente() {
	var suma= Number(isNaN($("#importe1_text").val())?"0":$("#importe1_text").val())+Number(isNaN($("#importe2_text").val())?"0":$("#importe2_text").val())+Number(isNaN($("#importe3_text").val())?"0":$("#importe3_text").val());
	importe_pendiente= importe_total-suma;
	return;	
}

function calcularDevolver() {
	var suma= Number(isNaN($("#importe1_text").val())?"0":$("#importe1_text").val())+Number(isNaN($("#importe2_text").val())?"0":$("#importe2_text").val())+Number(isNaN($("#importe3_text").val())?"0":$("#importe3_text").val());
	importe_devolver= suma-importe_total;
	$("#total-devolver").text(importe_devolver.toFixed(2));
	return;	
}

//******************************* Se rellena el formulario de CallCenter si hay un cliente seleccionado 


//if (json_reserva.Venta.reserva.cliente.idcliente!="") {
	
	
	//$("#cp_callcenter").val($("#cpcliente_venta_individual").val());
	//$("#cp_callcenter_reserva").val(json_reserva.Venta.reserva.cp);	
	//$("#nombre_reserva").val(json_reserva.Venta.reserva.nombre);
	//$("#nif_reserva").val(json_reserva.Venta.reserva.nif);
	//$("#telefono_reserva").val(json_reserva.Venta.reserva.telefono);
	//$("#email_reserva").val(json_reserva.Venta.reserva.email);
//}

var dir_fiscal = ""+json_reserva.Venta.reserva.cliente.direccionfiscal;
var cp_reserva = json_reserva.Venta.reserva.cp;

/*
if(dir_fiscal=="undefined")
	cp_reserva = ""+json_reserva.Venta.reserva.cp;
else
	cp_reserva =""+json_reserva.Venta.reserva.cliente.direccionfiscal.cp.cp;
*/
$("#cp_taquilla_reserva").val(json_reserva.Venta.reserva.cp);
$("#nif").val(json_reserva.Venta.reserva.nif);

if ($("input.flat")[0]) {
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });
}
//***************************************************************************************
function selectorImporteChange(nombre) {
	$("#selector_"+nombre).on("change", function(e) {
		$("#"+nombre+"_text").val("");
		calcularPendiente();
		if ($("#selector_"+nombre+" :selected").text()=="") {
			$('input[name="'+nombre+'_text"]').val("");
	    	$('input[name="'+nombre+'_text"]').attr('disabled', 'disabled');
		} else {
			$('input[name="'+nombre+'_text"]').removeAttr('disabled');
	    	$("#"+nombre+"_text").val(importe_pendiente.toFixed(2));
		}
		calcularDevolver();
	});

	$("#"+nombre+"_text").on("change", function(e) {
		calcularDevolver();	
	});

	$("#"+nombre+"_text").on("focusout", function(e) {
		var valor= Number($("#"+nombre+"_text").val());
		$("#"+nombre+"_text").val(valor.toFixed(2));
	});
	
}

selectorImporteChange("importe1");
selectorImporteChange("importe2");
selectorImporteChange("importe3");

//********************************************************************************
 function construir_xml_venta_de_reserva() {
	//var cp_txt= (tipocanal=="1")?$("#cp_taquilla_reserva").val():$("#cp_callcenter").val(),
		var cp_txt= $("#cp_taquilla_reserva").val(),
		imp_recibo= ($("#oimprecibo").is(":checked"))?1:0,
		imp_copia= ($("#oimpcopia").is(":checked"))?1:0,
		imp_entradas= ($("#oimpentradas").is(":checked"))?1:0,
		imp_entradas_def= ($("#oimpentradasdef").is(":checked"))?1:0,
		xml_lineasdetalle= "<Venta>";
		
	if((imp_copia==1) && (imp_recibo==1))
		imp_recibo = 2;
		
	xml_lineasdetalle+="<canalByCanalventa>";
	//xml_lineasdetalle+="<idcanal>${sessionScope.idcanal}</idcanal>";
	xml_lineasdetalle+="<idcanal></idcanal>";
	xml_lineasdetalle+="</canalByCanalventa>";
	xml_lineasdetalle+="<cliente>";
	var id_cliente = ""+json_reserva.Venta.reserva.cliente.idcliente;
	
	if(id_cliente=="undefined")
		id_cliente="";
	xml_lineasdetalle+="<idcliente>"+id_cliente+"</idcliente>";
	//xml_lineasdetalle+="<idcliente></idcliente>";
	xml_lineasdetalle+="</cliente>";
	xml_lineasdetalle+="<cp>"+cp_txt+"</cp>";
	xml_lineasdetalle+="<pais>";
	xml_lineasdetalle+="<idpais>"+idpais+"</idpais>";
	xml_lineasdetalle+="</pais>";
	xml_lineasdetalle+="<financiada>false</financiada>";
	xml_lineasdetalle+="<dadodebaja>0</dadodebaja>";
	xml_lineasdetalle+="<email>"+json_reserva.Venta.reserva.email+"</email>";
	xml_lineasdetalle+="<entradasimpresas>"+imp_entradas+"</entradasimpresas>";
	xml_lineasdetalle+="<imprimirSoloEntradasXDefecto>"+imp_entradas_def+"</imprimirSoloEntradasXDefecto>";
	xml_lineasdetalle+="<isBono>false</isBono>";
	xml_lineasdetalle+="<importeparcials>"; 
	xml_lineasdetalle+="<Importeparcial>";
	xml_lineasdetalle+="<formapago>";
	xml_lineasdetalle+="<idformapago>"+$("#selector_importe1").val()+"</idformapago>";
	xml_lineasdetalle+="</formapago>";
	xml_lineasdetalle+="<importe>"+$("#importe1_text").val()+"</importe>";
	xml_lineasdetalle+="</Importeparcial>";
	xml_lineasdetalle+="<Importeparcial>";
	xml_lineasdetalle+="<formapago>";
	xml_lineasdetalle+="<idformapago>"+$("#selector_importe2").val()+"</idformapago>";
	xml_lineasdetalle+="</formapago>";
	xml_lineasdetalle+="<importe>"+$("#importe2_text").val()+"</importe>";
	xml_lineasdetalle+="</Importeparcial>";
	xml_lineasdetalle+="<Importeparcial>";
	xml_lineasdetalle+="<formapago>";
	xml_lineasdetalle+="<idformapago>"+$("#selector_importe3").val()+"</idformapago>";
	xml_lineasdetalle+="</formapago>";
	xml_lineasdetalle+="<importe>"+$("#importe3_text").val()+"</importe>";
	xml_lineasdetalle+="</Importeparcial>";
	xml_lineasdetalle+="</importeparcials>";
	xml_lineasdetalle+="<importetotalventa>"+importe_total+"</importetotalventa>";
	//xml_lineasdetalle+= construir_xml_lineasdetalle($("#datatable_list_venta_individual").DataTable().rows().data());
	
	xml_lineasdetalle+= "<nif>"+$("#nif").val()+"</nif>";
	xml_lineasdetalle+= "<nombre>"+json_reserva.Venta.reserva.nombre+"</nombre>";
	xml_lineasdetalle+= "<observaciones>"+$("#observaciones_reserva").val()+"</observaciones>";
	//
	xml_lineasdetalle+= "<reciboimpreso>"+imp_recibo+"</reciboimpreso>";
	//xml_lineasdetalle+= "<telefono>"+$("#telefono").val()+"</telefono>";
	//xml_lineasdetalle+= "<telefonomovil>"+$("#movil").val()+"</telefonomovil>";
	//*********AÑADIMOS LA RESERVA*****
	xml_lineasdetalle+= "<reserva>";
	xml_lineasdetalle+= "<idreserva>"+json_reserva.Venta.reserva.idreserva+"</idreserva>";
	xml_lineasdetalle+= "<importetotalreserva>"+json_reserva.Venta.reserva.importetotalreserva+"</importetotalreserva>";
	xml_lineasdetalle+= "<grupo>"+json_reserva.Venta.reserva.grupo+"</grupo>";
	xml_lineasdetalle+= "<fechayhorareserva>"+json_reserva.Venta.reserva.fechayhorareserva+"</fechayhorareserva>";
	xml_lineasdetalle+= "<fecharecogida/>";
	xml_lineasdetalle+= "<observaciones/>";
	xml_lineasdetalle+= "<ventas/>";
	xml_lineasdetalle+= "<modificacions/>";
	xml_lineasdetalle+= "<nombre>"+json_reserva.Venta.reserva.nombre+"</nombre>";
	xml_lineasdetalle+= "<composiciongrupo/>";
	xml_lineasdetalle+= "<telefono>"+json_reserva.Venta.reserva.telefono+"</telefono>";
	xml_lineasdetalle+= "<telefonomovil>"+json_reserva.Venta.reserva.telefonomovil+"</telefonomovil>";
	xml_lineasdetalle+= "<email>"+json_reserva.Venta.reserva.email+"</email>";
	xml_lineasdetalle+= "<personacontacto/>";
	xml_lineasdetalle+= "<cursoescolar/>";
	xml_lineasdetalle+= "<cp>"+json_reserva.Venta.reserva.cp+"</cp>";
	xml_lineasdetalle+= "<nif>"+json_reserva.Venta.reserva.nif+"</nif>";
	xml_lineasdetalle+= "<edadgrupo/>";
	xml_lineasdetalle+= "<localizadoragencia/>";
	var xnlCliente="";
	if(json_reserva.Venta.reserva.cliente!="")
	 xnlCliente = json2xml(json_reserva.Venta.reserva.cliente);
	xml_lineasdetalle+= "<cliente>";
	xml_lineasdetalle+= xnlCliente;
	xml_lineasdetalle+= "</cliente>";
	/*
	xml_lineasdetalle+= "<cliente>";
	xml_lineasdetalle+= "<idcliente>"+json_reserva.Venta.reserva.cliente.idcliente+"</idcliente>";
	xml_lineasdetalle+= "<nombre>"+json_reserva.Venta.reserva.cliente.nombre+"</nombre>";
	xml_lineasdetalle+= "<apellido1/><apellido2/>";
	var idDireccionOrigen = "0"; 
	var idpaisOrigen = "34";
	if(json_reserva.Venta.reserva.cliente!="")
		{
		idDireccionOrigen = json_reserva.Venta.reserva.cliente.direccionorigen.iddireccion;
	    idpaisOrigen = idDireccionOrigen = json_reserva.Venta.reserva.cliente.direccionorigen.iddireccion;
		}
	xml_lineasdetalle+= "<direccionorigen>";
	xml_lineasdetalle+= "<iddireccion>"+idDireccionOrigen+"</iddireccion><pais><idpais>"+idpaisOrigen+"</idpais></pais><cp><cp>"+json_reserva.Venta.reserva.cp+"</cp></cp>";
	xml_lineasdetalle+= "</direccionorigen>";
	if(json_reserva.Venta.reserva.cliente!="")
		{
		idDireccionOrigen = json_reserva.Venta.reserva.cliente.direccionfiscal.iddireccion;
	    idpaisOrigen = idDireccionOrigen = json_reserva.Venta.reserva.cliente.direccionfiscal.iddireccion;
		}
	
	xml_lineasdetalle+= "<direccionfiscal>";
	xml_lineasdetalle+= "<iddireccion>"+idDireccionOrigen+"</iddireccion><pais><idpais>"+idpaisOrigen+"</idpais></pais><cp><cp>"+json_reserva.Venta.reserva.cp+"</cp></cp>";
	xml_lineasdetalle+= "</direccionfiscal>";
	xml_lineasdetalle+= "<direccionauxiliar/>";
	xml_lineasdetalle+= "</cliente>";
	*/
	xml_lineasdetalle+= "<canal>";
	xml_lineasdetalle+= "<idcanal>"+json_reserva.Venta.reserva.canal.idcanal+"</idcanal>";
	xml_lineasdetalle+= "<nombre>"+json_reserva.Venta.reserva.canal.nombre+"</nombre>";
	xml_lineasdetalle+= "</canal>";
	xml_lineasdetalle+= "<usuario>";
	xml_lineasdetalle+= "<idusuario>"+json_reserva.Venta.reserva.usuario.idusuario+"</idusuario><login>"+json_reserva.Venta.reserva.usuario.login+"</login><nombre>"+json_reserva.Venta.reserva.usuario.nombre+"</nombre><apellidos>"+json_reserva.Venta.reserva.usuario.apellidos+"</apellidos>";
	xml_lineasdetalle+= "</usuario>";
	xml_lineasdetalle+= "<estadooperacion>";
	xml_lineasdetalle+= "<idestadooperacion>"+json_reserva.Venta.reserva.estadooperacion.idestadooperacion+"</idestadooperacion>";
	xml_lineasdetalle+= "<nombre>"+json_reserva.Venta.reserva.estadooperacion.nombre+"</nombre>";
	xml_lineasdetalle+= "</estadooperacion>";
	xml_lineasdetalle+= "<puntosrecogida/>";
	xml_lineasdetalle+= "<numeroentradassinanular>"+json_reserva.Venta.reserva.numeroentradassinanular+"</numeroentradassinanular>";
	xml_lineasdetalle+= "<numeroentradasimpresas>"+json_reserva.Venta.reserva.numeroentradasimpresas+"</numeroentradasimpresas>";
	//xml_lineasdetalle+= construir_xml_lineasdetalleVentaReserva();
	xml_lineasdetalle+="<lineadetallessinventa>";
	xml_lineasdetalle+=json2xml(json_reserva.Venta.reserva.lineadetallessinventa);
	xml_lineasdetalle+="</lineadetallessinventa>";
	xml_lineasdetalle+="<lineadetalles>";
	xml_lineasdetalle+=json2xml(json_reserva.Venta.reserva.lineadetalles);
	
	
	xml_lineasdetalle+="</lineadetalles>";

	xml_lineasdetalle+= "<lineadetallesanuladas/>";
	xml_lineasdetalle+= "</reserva>";
	
	
	
	xml_lineasdetalle+="</Venta>";
	return(xml_lineasdetalle);
}


//********************************************************************************	
function enviar_carta_confirmacion(id) {	
	window.open("../../cartaConfirmacion.post?medioEnvio=&format="+$("#carta_reserva").val()+"&idioma="+$("#selector_idiomas_confirmar").val()+"&idReserva="+id,"_blank");
}	

//********************************************************************************	
function saveFormVentaDeReserva() {
	showSpinner("#modal-dialog-form .modal-content");

	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/realizar_venta_de_reserva.do'/>",
		timeout : 100000,
		data: {
			xml: construir_xml_venta_de_reserva()
		},
		success : function(data) {
			hideSpinner("#modal-dialog-form .modal-content");
			
			$("#modal-dialog-form-3").modal('hide');
			
			showSpinner("#main-ventareserva");
			showSpinner("#main-busquedas");
			$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_resumen_venta_de_reserva.do?idventa="+data.ok.int+"'/>", function() {
				$("#modal-dialog-form-2").modal('show');
				setModalDialogSize("#modal-dialog-form-2", "md");
			});
			
			if ($("#carta_reserva").is(":checked")) 
				enviar_carta_confirmacion(json_reserva.Venta.reserva.idreserva);		
			
			if ($("#carta_venta").is(":checked"))
				enviar_carta_confirmacion(json_reserva.Venta.reserva.idreserva);
				//window.open("../../cartaConfirmacion.post?medioEnvio=&format="+$("#carta_venta_select").val()+"&idioma="+$("#selector_idiomas_confirmar").val()+"&idVenta="+data.ok.int,"_blank");
						
			if ($("#oimpentradas").is(":checked"))
				imprimirEntradasReserva(data.ok.int);
			
			try{ dt_listbusqueda.ajax.reload();	}
			catch(err) {}
			
			if(confirmar_desde_dentro)
			{
				$("#modal-dialog-form .modal-content").load("<c:url value='/venta/ventareserva/busqueda/ventareserva/editar_reserva.do'/>?id="+idReserva, function() {
					$("#modal-dialog-form").modal('show');
					setModalDialogSize("#modal-dialog-form", "md");
				});
			}
		},
		error : function(exception) {
			hideSpinner("#modal-dialog-form .modal-content");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});		
}	

function imprimirEntradasReserva(idVenta)
{
	var imp_entradas_def= ($("#oimpentradasdef").is(":checked"))?1:0
	var xml="<PeticionImpresionSinRespuesta><idVenta>"+idVenta+"</idVenta>";
	xml+="<soloEntradasXDefecto>"+imp_entradas_def+"</soloEntradasXDefecto>"	
	xml+="</PeticionImpresionSinRespuesta>";
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/imprimirEntradasVenta.do'/>",
		timeout : 100000,
		data: {
			xmlImpresion: xml
		},
		success : function(data) {},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });
		}
	});
}

$("#save_venta_button").on("click", function(e) {
	$("#form_venta_individual_dialog").submit();
})

$("#form_venta_individual_dialog").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		saveFormVentaDeReserva();		
	}
});  


</script>

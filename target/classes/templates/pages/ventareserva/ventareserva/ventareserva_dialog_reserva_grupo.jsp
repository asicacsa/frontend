<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_selector_idiomas}" var="ventareserva_selector_idiomas_xml" />
<x:parse xml="${ventareserva_selector_puntos_recogida}" var="ventareserva_selector_puntos_recogida_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.reserva_grupo.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_reserva_grupo_dialog" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

		<div id="columna-izquierda" class="col-md-6 col-sm-12 col-xs-12">
		
			<div class="callcenter">
			
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.reserva_grupo.field.nombre" />*</label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input type="text" name="nombre" id="nombre" class="form-control" value="" required="required"/>
					</div>
				</div>
				
				<div class="form-group">
					<div class="checkbox col-md-4 col-sm-4 col-xs-4">
						<input type="checkbox" name="grupo-escolar" id="grupo-escolar" value="" data-parsley-multiple="inicio_sesion" class="flat" style="position: absolute; opacity: 0;">
						<strong><spring:message code="venta.ventareserva.dialog.reserva_grupo.field.grupo_escolar" /></strong>
					</div>
					<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="venta.ventareserva.dialog.reserva_grupo.field.edad_grupo" /></label>
					<div class="col-md-2 col-sm-2 col-xs-2">
						<input name="edadgrupo" id="edadgrupo" type="text" class="form-control" disabled="disabled" value="" />
					</div>
					<label class="control-label col-md-2 col-sm-2 col-xs-2"><spring:message code="venta.ventareserva.dialog.reserva_grupo.field.curso_grupo" /></label>
					<div class="col-md-2 col-sm-2 col-xs-2">
						<input name="cursoescolar" id="cursoescolar" type="text" class="form-control" disabled="disabled" value="" />
					</div>
				</div>
							
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.dialog.reserva_grupo.field.composiciongrupo" /></label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<input type="text" name="composiciongrupo" id="composiciongrupo" class="form-control" value="" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.dialog.reserva_grupo.field.puntorecogida" /></label>
					<div class="col-md-9 col-sm-9 col-xs-12">
                       <select name="puntorecogida_select" id="puntorecogida_select" class="form-control">
					   		<option></option>
							<x:forEach select="$ventareserva_selector_puntos_recogida_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
				       </select>
					</div>    					
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.dialog.reserva_grupo.field.localizadoragencia" /></label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="localizadoragencia" id="localizadoragencia" class="form-control" value="" />
					</div>
				</div>
				
				<div class="x_title">
					<h5><spring:message code="venta.ventareserva.dialog.venta_individual.field.opciones_impresion" /></h5>
					<div class="clearfix"></div>
				</div>

				<div class="form-group">
					<div class="checkbox col-md-6 col-sm-6 col-xs-12">
						<input type="checkbox" name="selector" id="carta_confirmacion" value="" class="flat" style="position: absolute; opacity: 0;">
						<span style="float: right;"><strong><spring:message code="venta.ventareserva.dialog.venta_individual.field.carta_confirmacion" /></strong></span>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
                       <select name="carta_confirmacion_select" id="carta_confirmacion_select" class="form-control">
							<option value="pdf">pdf</option>
							<option value="email">email</option>
							<option value="fax">pdf</option>
				       </select>
					</div>                       
				</div>
			
				<div class="form-group">
                   <label class="control-label col-md-6 col-sm-6 col-xs-12">
                  		<spring:message code="venta.ventareserva.dialog.venta_individual.field.idioma_carta" />                               
                    </label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select name="idiomas[]" id="selector_idiomas" class="form-control" style="width: 100%">
							<%-- <x:forEach select="$ventareserva_selector_idiomas_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach> --%>
						</select>
					</div>                       
				</div>
				
			</div>
					
		</div>

		<div id="columna-derecha" class="col-md-6 col-sm-12 col-xs-12">
				
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.reserva_grupo.field.personacontacto" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="text" name="personacontacto" id="personacontacto" class="form-control" value="" />
				</div>
			</div>

			<div class="form-group">

				<div class="col-md-6 col-sm-6 col-xs-6">
					<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="venta.ventareserva.dialog.venta_individual.field.telefono" /></label>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<input type="text" name="telefono" id="telefono" class="form-control" value="" />
					</div>
				</div>
			
				<div class="col-md-6 col-sm-6 col-xs-6">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.movil" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input type="text" name="movil" id="movil" class="form-control" value="" />
					</div>
				</div>
				
			</div>		

			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.email" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input type="email" name="email" id="email" class="form-control" value="" />
				</div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.observaciones" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<textarea class="form-control" rows="3" name="observaciones" id="observaciones"></textarea>
				</div>
			</div>
			
		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_reserva_button" type="button" class="btn btn-primary save_dialog">
					<spring:message code="common.button.book" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
		
	</form>

</div>

<script>

hideSpinner("#button_reserva_grupo_reservar");

cargarSelectorIdiomas("selector_idiomas");

if ($("input.flat")[0]) {
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });
}


$("#grupo-escolar").on("ifChanged", function(e) {
	if ($('input[name="edadgrupo"]').attr('disabled')) { 
		$('input[name="edadgrupo"]').removeAttr('disabled');
		$('input[name="cursoescolar"]').removeAttr('disabled');
	}
    else { 
    	$('input[name="edadgrupo"]').attr('disabled', 'disabled');
    	$('input[name="cursoescolar"]').attr('disabled', 'disabled');
    }
});

/* Se rellena el formulario si hay un cliente seleccionado */

if ($("#idcliente_reserva_grupo").val()!="") {
	$("#cp").val($("#cpcliente_reserva_grupo").val());
	$("#nombre").val($("#cliente_reserva_grupo").val());
	$("#nif").val($("#cifcliente_reserva_grupo").val());
	$("#telefono").val($("#telefonocliente_reserva_grupo").val());
	$("#email").val($("#emailcliente_reserva_grupo").val());
}


//********************************************************************************
function construir_xml_reserva_grupo() {
	var xml_lineasdetalle= "<Reserva>";
	xml_lineasdetalle+="<canal>";
	xml_lineasdetalle+="<idcanal>${sessionScope.idcanal}</idcanal>";
	xml_lineasdetalle+="</canal>";
	xml_lineasdetalle+="<puntosrecogida><idpuntosrecogida>"+$("#puntorecogida_select").val()+"</idpuntosrecogida></puntosrecogida>";
	xml_lineasdetalle+="<presentacionsesion><idpresentacionsesion/></presentacionsesion>";
	xml_lineasdetalle+="<cliente>";
	xml_lineasdetalle+="<idcliente>"+$("#idcliente_reserva_grupo").val()+"</idcliente>";
	xml_lineasdetalle+="</cliente>";
	xml_lineasdetalle+="<fecharecogida/>";
	xml_lineasdetalle+="<personacontacto>"+$("#personacontacto").val()+"</personacontacto>";
	xml_lineasdetalle+="<edadgrupo>"+$("#edadgrupo").val()+"</edadgrupo>";
	xml_lineasdetalle+="<cursoescolar>"+$("#cursoescolar").val()+"</cursoescolar>";
	xml_lineasdetalle+="<observaciones>"+$("#observaciones").val()+"</observaciones>";
	xml_lineasdetalle+="<telefono>"+$("#telefono").val()+"</telefono>";
	xml_lineasdetalle+="<email>"+$("#email").val()+"</email>";
	xml_lineasdetalle+="<nif>"+$("#cifcliente_reserva_grupo").val()+"</nif>";
	xml_lineasdetalle+="<nombre>"+$("#nombre").val()+"</nombre>";
	xml_lineasdetalle+="<cp></cp>";
	xml_lineasdetalle+=construir_xml_lineasdetalle($("#datatable_list_reserva_grupo").DataTable().rows().data());
	xml_lineasdetalle+="<telefonomovil>"+$("#movil").val()+"</telefonomovil>";
	xml_lineasdetalle+="<grupo>1</grupo>";
	xml_lineasdetalle+="<localizadoragencia>"+$("#localizadoragencia").val()+"</localizadoragencia>";
	xml_lineasdetalle+="<composiciongrupo>"+$("#composiciongrupo").val()+"</composiciongrupo>";
	xml_lineasdetalle+="</Reserva>";
	return(xml_lineasdetalle);
}

function enviarEmailCartaConfirmacion(idioma,idreserva)
{

	var url_email = "<c:url value='../../cartaConfirmacion.post?medioEnvio=email&format=email'/>"+"&idioma="+idioma+"&anulada=0&idReserva="+idreserva;
	console.log(url_email);
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : url_email,
		timeout : 100000,
		data: {
			
		},
		success : function(data) {
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="venta.ventareserva.dialog.venta.cartaConfirmacion.enviada.message" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});	
		},
		error : function(exception) {				
		}
	});
}

//********************************************************************************	
function saveFormReservaIndividual() {
	showSpinner("#modal-dialog-form .modal-content");

	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/realizar_reserva_grupo.do'/>",
		timeout : 100000,
		data: {
			xml: construir_xml_reserva_grupo()
		},
		success : function(data) {
			var idreserva = data.ok.Integer;			
			hideSpinner("#modal-dialog-form .modal-content");
			if ($("#carta_confirmacion").is(":checked")) //enviar_carta_confirmacion(data.ok.int,"idReserva");	
			{
				var format= ""+$("#carta_confirmacion_select").val();
				var idioma= ""+$("#selector_idiomas").val();	
				if(format=="email")
					{
					enviarEmailCartaConfirmacion(idioma,idreserva);
					//window.open("../../cartaConfirmacion.post?medioEnvio="+format+"&format="+format+"&idioma="+idioma+"&anulada=0&idReserva="+idreserva, "_blank");
					}					
				else
					window.open("../../cartaConfirmacion.post?medioEnvio=&format="+format+"&idioma="+idioma+"&anulada=0&idReserva="+idreserva, "_blank");
			} 			
			
			$("#modal-dialog-form").modal('hide');
			showSpinner("#main-ventareserva");
			$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_resumen_reserva_grupo.do?idreserva="+idreserva+"'/>", function() {
				$("#modal-dialog-form-2").modal('show');
				setModalDialogSize("#modal-dialog-form-2", "md");
			});
			obtener_totales_venta_temporal("reserva_grupo","reserva_grupo",$("#idcliente_reserva_grupo").val(),4,true);
			
		},
		error : function(exception) {
			hideSpinner("#modal-dialog-form .modal-content");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});		
}	

$("#save_reserva_button").on("click", function(e) {
	$("#form_reserva_grupo_dialog").submit();
})

$("#form_reserva_grupo_dialog").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
      $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		saveFormReservaIndividual();		
	}
});

</script>

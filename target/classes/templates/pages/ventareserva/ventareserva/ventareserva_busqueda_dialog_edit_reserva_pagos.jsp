<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${motivosModificacion}" var="motivosModificacion_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="venta.numerada.tabs.venta_edit.editar.formas.title" />				
	</h4>	
</div>

<div class="modal-body">
	<form id="form_modificacion_reserva_guardar" data-parsley-validate="" class="form-horizontal form-label-left" >
		<input type="hidden" id="xmlModificacion" name="xmlModificacion" />
		<input type="hidden" id="strMotivo" name="strMotivo" />
		<div class="form-group">			
			<label class="control-label col-md-6 col-sm-6 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.motivos" /></label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<select required="required" class="form-control" name="idmotivo_modificacion" id="idmotivo_modificacion">
						<option value=""></option>
							<x:forEach select="$motivosModificacion_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out
											select="$item/label" /></option>
								</x:forEach>
					</select>								
				</div>		
		</div>	
		<div class="form-group">
				<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.observaciones" /></label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<textarea name="observacion_modificacion" id="observacion_modificacion" class="form-control"  ></textarea>
				</div>
			</div>			
		
		<div class="modal-footer">
			
			<button id="aceptar_modificacion_button_reserva"  class="btn btn-primary">
						<spring:message code="common.button.accept" />
			</button>
			<button type="button" class="btn btn-cancel close_dialog2" data-dismiss="modal">
				<spring:message code="common.button.cancel"/>
			</button>
		</div>		
	</form>
</div>

<script>
hideSpinner("#tab_busqueda_ver_formas_pago");

$("#aceptar_modificacion_button_reserva").on("click", function(e) {	
	$("#form_modificacion_reserva_guardar").validate();	
})

$("#form_modificacion_reserva_guardar").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {			
			guardarModificacionReserva()			
		}
	}
)

//***************************************************
var xml="";

function guardarModificacionReserva()
{
	
	var idReserva = $("#idreserva").val();
	xml = "<Modificacion><idmodificacion/>";
	xml+="<observaciones>"+$("#observacion_modificacion").val()+"</observaciones>";
	xml+="<motivomodificacion>"+$("#idmotivo_modificacion option:selected").text()+"</motivomodificacion>";
	xml+="<venta>";
	xml+="<idventa/>";
	xml+="<entradasimpresas>false</entradasimpresas>";
	xml+="<reciboimpreso>0</reciboimpreso>";
	xml+="<imprimirSoloEntradasXDefecto>false</imprimirSoloEntradasXDefecto>";
	xml+="</venta>";
	
	xml+="<reserva><idreserva>"+idReserva+"</idreserva></reserva>";
	xml+="<modificacionimporteparcials>";
	xml+="<Modificacionimporteparcial><importeparcial><formapago><idformapago></idformapago></formapago><bono><numeracion/></bono><importe></importe><rts/><numpedido/><anulado/></importeparcial></Modificacionimporteparcial>";
	xml+="<Modificacionimporteparcial><importeparcial><formapago><idformapago></idformapago></formapago><bono><numeracion/></bono><importe></importe><rts/><numpedido/><anulado/></importeparcial></Modificacionimporteparcial>";
	xml+="<Modificacionimporteparcial><importeparcial><formapago><idformapago></idformapago></formapago><bono><numeracion/></bono><importe></importe><rts/><numpedido/><anulado/></importeparcial></Modificacionimporteparcial>";
	xml+="<anulaciondesdecaja/>";
	xml+="</modificacionimporteparcials>";
	var xml_lineasdetalle = construir_xml_lineasdetalle_pagos(dt_listventaindividual.rows().data());
	xml = xml+xml_lineasdetalle;
	xml+="</Modificacion>";
	$("#xmlModificacion").val(xml);
	
	var data = $("#form_modificacion_reserva_guardar").serializeObject();
	
	$.ajax({
		type : "post",
		url : "<c:url value='/ajax/ventareserva/busqueda/modificar_lineasdetalle_reserva.do'/>",
		timeout : 100000,
		data : data,
		success : function(data) {		
			$("#modal-dialog-form-3").modal('hide');
			$("#modal-dialog-form-2").modal('hide');
			
			
		 	$("#modal-dialog-form .modal-content").load("<c:url value='/venta/ventareserva/busqueda/ventareserva/editar_reserva.do'/>?id="+$("#idreserva").val(), function() {
				$("#modal-dialog-form").modal('show');
				setModalDialogSize("#modal-dialog-form", "md");
			}); 
			
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
			$("#modal-dialog-form-2").modal('hide');
		},
		error : function(exception) {
			

			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : exception.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		}
	}); 

}


 function procesarPago(fila)
{


	if(fila[1]=="")
	{
		var jsonFila = fila[0];
		
		
		xml+="<Modificacionimporteparcial>";
		xml+="<tipomodificacion>0</tipomodificacion>";
		xml+="<importeparcial isnew=\"false\" anulado=\"0\">";
		xml+="<idimporteparcial>"+jsonFila.idimporteparcial+"</idimporteparcial>";
		xml+="<formapago>"+json2xml(jsonFila.formapago)+"</formapago>";
		xml+="<importe>"+jsonFila.importe+"</importe>";
		xml+="<modificacionimporteparcials>"+json2xml(jsonFila.modificacionimporteparcials)+"</modificacionimporteparcials>";
		
		
		if (jsonFila.venta!=""){
			xml+="<venta>";
			xml+="<idventa>"+ $("#idVentaEditar").val()+"</idventa>";
			xml+="<operacioncaja>";
			xml+="<idoperacioncaja>"+jsonFila.venta.operacioncaja.idoperacioncaja+"</idoperacioncaja>";
			xml+="<caja>";
			xml+="<idcaja>"+jsonFila.venta.operacioncaja.caja.idcaja+"</idcaja>";
			xml+="</caja>";
			xml+="</operacioncaja>";
			xml+="</venta>";
		}else{
			xml+="<venta/>";
		}	
		
		xml+="<bono/><rts/><numpedido/>";
		xml+="</importeparcial>";			
		xml+="</Modificacionimporteparcial>";
	}
else
	{
		xml+="<Modificacionimporteparcial>";
		xml+="<tipomodificacion>0</tipomodificacion>";
		xml+="<importeparcial isnew=\"true\" anulado=\"0\">";
		xml+="<formapago>";
		xml+="<idformapago>"+fila[1]+"</idformapago>";
		xml+="<nombre></nombre>";
		xml+="</formapago>";
		xml+="<importe>"+fila[3]+"</importe>";
		xml+="<bono><idbono/></bono><rts/><numpedido/><anulado>0</anulado>";
		xml+="</importeparcial>";
		xml+="</Modificacionimporteparcial>";
	}
}
 
//*****************************************************
 function construir_xml_lineasdetalle_pagos(lineas_detalle_sel) {
 	
 	var xml_lineasdetalle= "<modificacionlineadetalles>";
 	xml_lineasdetalle+= "<Modificacionlineadetalle>";
 	xml_lineasdetalle+= "<lineadetalle>";
 	xml_lineasdetalle+= "<idlineadetalle/>";
 	xml_lineasdetalle+= "</lineadetalle>";
 	xml_lineasdetalle+= "</Modificacionlineadetalle>";

 	for (var i=0; i<lineas_detalle_sel.length; i++) { 
 		xml_lineasdetalle+="<Modificacionlineadetalle>";
 		xml_lineasdetalle+="<tipomodificacion>0</tipomodificacion>";
 		xml_lineasdetalle+="<lineadetalle>";
 		var lineaLoop = lineas_detalle_sel[i][0];
 		var strLineaNueva = ""+lineaLoop.Lineadetalle;
 		if(strLineaNueva!='undefined')
 			lineaLoop = lineaLoop.Lineadetalle;
 		xml_lineasdetalle+=json2xml(lineaLoop,"");
 		xml_lineasdetalle+="</lineadetalle>";
 		xml_lineasdetalle+="</Modificacionlineadetalle>";
 		
 	}
 	
 	for(i=0;i<lineasDetallesEliminadas.length;i++)
	   {
		lineaDetalle = lineasDetallesEliminadas[i];
		xml_lineasdetalle+="<Modificacionlineadetalle>";
		xml_lineasdetalle+="<tipomodificacion>1</tipomodificacion>";
		xml_lineasdetalle+="<lineadetalle><idlineadetalle>"+lineaDetalle.idlineadetalle+"</idlineadetalle></lineadetalle>";
		xml_lineasdetalle+="</Modificacionlineadetalle>";
	   }
 	
 	
 	xml_lineasdetalle+="</modificacionlineadetalles>";
 	
 	
 	return(xml_lineasdetalle);
 }
</script>

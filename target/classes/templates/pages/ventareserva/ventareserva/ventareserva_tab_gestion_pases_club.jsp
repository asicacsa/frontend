<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_productos_disponibles}" var="ventareserva_productos_disponibles_xml" />
<x:parse xml="${ventareserva_canales_indirectos}" var="ventareserva_canales_indirectos_xml" />

<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2><spring:message code="common.text.filter_list" /></h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">
		<form id="form_gestion_pases_club" class="form-horizontal form-label-left">
			<input type="hidden" name="idcliente_gestion_pases_club" value=""/>			
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="form-group">		 	
					<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.field.nombre" /></label>
					<div class="col-md-8 col-sm-8 col-xs-12">
						<input name="nombre_gestion_pases_club" id="nombre_gestion_pases_club" type="text" class="form-control" >
					</div>
				</div>				
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.field.apellido1" /></label>
					<div class="col-md-8 col-sm-8 col-xs-12">
						<input name="apellido1_gestion_pases_club" id="apellido1_gestion_pases_club" type="text" class="form-control" >
					</div>
				</div>	
			</div>			
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.field.apellido2" /></label>
					<div class="col-md-8 col-sm-8 col-xs-12">
						<input name="apellido2_gestion_pases_club" id="apellido2_gestion_pases_club" type="text" class="form-control" >
					</div>
				</div>	
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.field.dni" /></label>
					<div class="col-md-8 col-sm-8 col-xs-12">
						<input name="dni_gestion_pases_club" id="dni_gestion_pases_club" type="text" class="form-control" >
					</div>
				</div>			
			</div>			
			<div id="grupo-datos">	
				<div class="x_title">
					<div class="clearfix"></div>
				</div>	
				<div class="form-group">
					<label class="control-label col-md-2 col-sm-2 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.field.producto" /></label>
					<div class="col-md-3 col-sm-3 col-xs-12">
						<select class="form-control" name="idproducto_gestion_pases_club" id="idproducto_gestion_pases_club">
							<option value=""></option>
							<x:forEach select="$ventareserva_productos_disponibles_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach>
						</select>
					</div>
				</div>
				<div class="form-group date-picker">
					<label class="control-label col-md-2 col-sm-2 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.field.iniciovigencia" /></label>
					<div class="col-md-3 col-sm-3 col-xs-12">
						<a type="button" class="btn btn-default btn-clear-date" id="button_iniciovigenciapasesclub_clear">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.gestion_pases_club.list.button.clear" />"> <span class="fa fa-trash"></span>
							</span>
					  	</a>			
                       	<div class="input-prepend input-group">
                       		<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                       		<input type="text" name="iniciovigenciapasesclub" id="iniciovigenciapasesclub" class="form-control" value="" readonly/>
                       		<input type="hidden" required="required" name="fechainiciovigenciadepasesclub" value=""/>
                       		<input type="hidden" required="required" name="fechainiciovigenciahastapasesclub" value=""/>
                       	</div>
					</div>
				</div>	
				<div class="form-group date-picker">
					<label class="control-label col-md-2 col-sm-2 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.field.finvigencia" /></label>
					<div class="col-md-3 col-sm-3 col-xs-12">
						<a type="button" class="btn btn-default btn-clear-date" id="button_finvigenciapasesclub_clear">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.gestion_pases_club.list.button.clear" />"> <span class="fa fa-trash"></span>
							</span>
					  	</a>			
                       	<div class="input-prepend input-group">
                       		<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                       		<input type="text" name="finvigenciapasesclub" id="finvigenciapasesclub" class="form-control" value="" readonly/>
                       		<input type="hidden" required="required" name="fechafinvigenciadepasesclub" value=""/>
                       		<input type="hidden" required="required" name="fechafinvigenciahastapasesclub" value=""/>
                       	</div>
					</div>
				</div>		
			</div>
			<input type="hidden" name="start" value=""/>
			<input type="hidden" name="length" value=""/>			
		</form>	
		<div class="clearfix"></div>
			<div class="ln_solid"></div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<button id="button_search_pases_club" type="button" class="btn btn-success pull-right">
					<spring:message code="common.button.filter" />
				</button>
				<button id="button_clean_pases_club" type="button" class="btn pull-right">
					<spring:message code="venta.ventareserva.tabs.gestion_pases_club.button.clean" />
				</button>
			</div>	
			
		</div>
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="btn-group pull-right btn-datatable">
			<a type="button" class="btn btn-info" id="tab_pases_club_new">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.gestion_pases_club.button.new" />">
					<span class="fa fa-plus"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="tab_pases_club_edit">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.gestion_pases_club.button.editar" />"> <span class="fa fa-pencil"></span>
				</span>
			</a>		
		</div>
	 	<table id="datatable-list-pases_club" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th><spring:message code="venta.ventareserva.tabs.gestion_pases_club.list.header.grupo" /></th>
					<th><spring:message code="venta.ventareserva.tabs.gestion_pases_club.list.header.titular" /></th>
					<th><spring:message code="venta.ventareserva.tabs.gestion_pases_club.list.header.fechaalta" /></th>
					<th><spring:message code="venta.ventareserva.tabs.gestion_pases_club.list.header.pasesasociados" /></th>
				</tr>
			</thead>
			<tbody>	</tbody>
		</table> 
	</div>

<script>
var direccion="";
var list_abonos;
var str_titular="<titularGrupo><idpase/><esAbono/></titularGrupo>";
var renovacion="";

 var dt_listpasesclub =$('#datatable-list-pases_club').DataTable( {
	serverSide: true,
	ordering: false,
	pagingType: "simple",
	info: false,
	deferLoading: 0,
    ajax: {
        url: "<c:url value='/ajax/venta/ventareserva/pases_club/list_pasesclub.do'/>",
        rowId: 'grupo',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.TreeSet.PaseDtoSalida)); return(""); },
        data: function (params) {
        	$('#form_gestion_pases_club input[name="start"]').val(params.start);
        	$('#form_gestion_pases_club input[name="length"]').val(params.length);
        	return ($("#form_gestion_pases_club").serializeObject());
       	},		        
        error: function (xhr, error, thrown) {
            if (xhr.responseText=="403") {
                  $("#tab_gestion_pases_club").hide();
            }    
            else
            	{
            	new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : xhr.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}					
				});
            	}
     },
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {dt_listpasesclub.columns.adjust().draw(); });
	},
    columns: [
	{ data: "grupo", type: "spanish-string", defaultContent: ""},
	{ data: "nombreTitular", type: "spanish-string", defaultContent: ""}, 
	{ data: "fechaAlta", type: "spanish-string" ,  defaultContent:"",
		 render: function ( data, type, row, meta ) {			 
    	  	  return data.substr(0, 10);}	 
	 } ,
	{ data: "pasesAsociados", type: "spanish-string", defaultContent: ""},
	],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-pases_club') },
    select: { style: 'single'},
	language: dataTableLanguage,
	processing: true,
} );
 //***************************************************
insertSmallSpinner("#datatable-list-pases_club_processing");
//*****************************************************
$("#button_search_pases_club").on("click", function(e) {
	dt_listpasesclub.ajax.reload();
	})
//***************************************************************
$("#cliente_gestion_pases_club").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>?editable=1", "#cliente_gestion_pases_club", "#idcliente_gestion_pases_club");
//***************************************************************
$('input[name="iniciovigenciapasesclub"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
  	  	 $('input[name="iniciovigenciapasesclub"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('input[name="fechainiciovigenciadepasesclub"]').val(start.format('DD/MM/YYYY'));  	  	 
  	  	 $('input[name="fechainiciovigenciahastapasesclub"]').val(end.format('DD/MM/YYYY'));
	 });

$("#button_iniciovigenciapasesclub_clear").on("click", function(e) {
    $('input[name="iniciovigenciapasesclub"]').val('');
    $('input[name="fechainiciovigenciadepasesclub"]').val('');
    $('input[name="fechainiciovigenciahastapasesclub"]').val('');
});

//****************************************************
$('input[name="finvigenciapasesclub"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
  	  	 $('input[name="finvigenciapasesclub"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('input[name="fechafinvigenciadepasesclub"]').val(start.format('DD/MM/YYYY'));  	  	 
  	  	 $('input[name="fechafinvigenciahastapasesclub"]').val(end.format('DD/MM/YYYY'));
	 });

$("#button_finvigenciapasesclub_clear").on("click", function(e) {
    $('input[name="finvigenciapasesclub"]').val('');
    $('input[name="fechafinvigenciadepasesclub"]').val('');
    $('input[name="fechafinvigenciahastapasesclub"]').val('');
});

//***************************************************************
$("#button_clean_pases_club").on("click", function(e) {
	 $('input[name="nombre_gestion_pases_club"]').val('');
	 $('input[name="apellido1_gestion_pases_club"]').val('');
	 $('input[name="apellido2_gestion_pases_club"]').val('');
	 $('input[name="dni_gestion_pases_club"]').val('');
	 $('#idproducto_gestion_pases_club').val('');
	 $('input[name="iniciovigenciapasesclub"]').val('');
	 $('input[name="finvigenciapasesclub"]').val('');		 
	})	
//***************************************************************
$("#tab_pases_club_new").on("click", function(e) {
	showButtonSpinner("#tab_pases_club_new");	
	
	list_abonos= []; 
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/pases_club/new_abono.do'/>", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
});
//***************************************************************
$("#tab_pases_club_edit").on("click", function(e) {

	var data = sanitizeArray(dt_listpasesclub.rows( { selected: true } ).data(),"grupo");
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#tab_pases_club_edit");	
	
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/pases_club/editar_grupo_abono.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
});
//***************************************************************
</script>

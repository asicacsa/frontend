<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_formaspago}" var="ventareserva_formaspago_xml" />
<x:parse xml="${ventareserva_selector_idiomas}" var="ventareserva_selector_idiomas_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.venta_grupos_abono.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_venta_individual_dialog" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">

		<div id="columna-izquierda" class="col-md-6 col-sm-12 col-xs-12">
		
			<div class="callcenter">
			
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.nombre" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input type="text" name="nombre" id="nombre" class="form-control" value=""/>
					</div>
				</div>
				
				<div class="form-group button-dialog capa_cp_dialog_venta_individual">
	
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="venta.ventareserva.dialog.venta_individual.field.nif" /></label>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<input type="text" name="nif" id="nif" class="form-control" value="" />
						</div>
					</div>
				
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.cp" />*</label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<!-- <button id='callcenter-search-button' type='button' class='btn btn-default btn-i18n'>
								<span class='docs-tooltip' data-toggle='tooltip' title='' data-original-title='Buscar CP'><span class='fa fa-search'></span></span>
							</button> -->
							<!-- <div class="input-prepend"> -->
								<input id="cp_callcenter" type="text" class="form-control" value=""/>
							<!-- </div> -->
						</div>
					</div>
					
				</div>		
				
				
				<div class="form-group">	
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="venta.ventareserva.dialog.venta_individual.field.telefono" /></label>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<input type="text" name="telefono" id="telefono" class="form-control" value="" />
						</div>
					</div>
				
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.movil" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<input type="text" name="movil" id="movil" class="form-control" value="" />
						</div>
					</div>
					
				</div>		
	
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.email" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input type="text" name="email" id="email" class="form-control" value="" />
					</div>
				</div>
					
			</div>

			<!-- <div id="taquilla">
			
				<div class="form-group button-dialog capa_cp_dialog_venta_individual">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.cp" />*</label>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<button id='taquilla-search-button' type='button' class='btn btn-default btn-i18n'>
							<span class='docs-tooltip' data-toggle='tooltip' title='' data-original-title='Buscar CP'><span class='fa fa-search'></span></span>
						</button>
						<div class="input-prepend">
							<input id="cp_taquilla" type="text" class="form-control" value=""/>
						</div>
					</div>
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.pais" /></label>
					<div class="col-md-2 col-sm-2 col-xs-2">
						<input id="pais_dialog_venta_individual" type="text" class="form-control" value=""/>
					</div>
				</div>		
			</div> -->
				
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.observaciones" /></label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<textarea class="form-control" rows="3" name="observaciones" id="observaciones"></textarea>
				</div>
			</div>

			<div class="x_title">
				<h5><spring:message code="venta.ventareserva.dialog.venta_individual.field.opciones_impresion" /></h5>
				<div class="clearfix"></div>
			</div>

			<div class="form-group">
				<div class="checkbox col-md-6 col-sm-6 col-xs-12">
					<input type="checkbox" name="selector" id="oimprecibo" value="" class="flat" style="position: absolute; opacity: 0;">
					<strong><spring:message code="venta.ventareserva.dialog.venta_individual.field.recibo" /></strong>
				</div>
				<div class="checkbox col-md-6 col-sm-6 col-xs-12">
					<input type="checkbox" name="selector" id="oimpcopia" value="" class="flat" style="position: absolute; opacity: 0;">
					<strong><spring:message code="venta.ventareserva.dialog.venta_individual.field.copia" /></strong>
				</div>				
			</div>

			<div class="form-group">
				<div class="checkbox col-md-6 col-sm-6 col-xs-12">
					<input type="checkbox" name="selector" id="oimpcontrato" value="" class="flat" checked style="position: absolute; opacity: 0;">
					<strong><spring:message code="venta.ventareserva.dialog.venta_individual.field.imprimir_contrato" /></strong>
				</div>	
			
			
				<div class="checkbox col-md-6 col-sm-6 col-xs-12">
					<input type="checkbox" name="selector" id="opases" value="" class="flat" style="position: absolute; opacity: 0;">
					<strong><spring:message code="venta.ventareserva.dialog.venta_individual.field.pases" /></strong>
				</div>
			</div>
					
		</div>

		<div id="columna-derecha" class="col-md-6 col-sm-12 col-xs-12">

			<div class="x_title">
				<h5><spring:message code="venta.ventareserva.dialog.venta_individual.field.forma_pago" /></h5>
				<div class="clearfix"></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12 forma-pago">
   				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
			            <label class="control-label col-md-4 col-sm-4 col-xs-4">
			            	<spring:message code="venta.ventareserva.dialog.venta_individual.field.imp_cobrar" />                               
			            </label>
			            <label class="control-label col-md-8 col-sm-8 col-xs-8 euros">
							<span class="total-lbl"><span id="total-cobrar">0.00</span>&euro;</span>		
			            </label>
		            </div>
		        </div>
   				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
	                   <label class="control-label col-md-4 col-sm-4 col-xs-12">
                       		<spring:message code="venta.ventareserva.dialog.venta_individual.field.imp_entregado" />                               
                       </label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<div class="input-prepend">
								<input name="importe1_text" id="importe1_text" type="text" class="form-control importe" disabled="disabled">
							</div>
							<span class="importe">&euro;</span>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12">
	                       <select name="importe1" id="selector_importe1" class="form-control">
								<option value=""></option>
								<x:forEach select="$ventareserva_formaspago_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
					       </select>
						</div>                       
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
	                   <label class="control-label col-md-4 col-sm-4 col-xs-12">
                       		<spring:message code="venta.ventareserva.dialog.venta_individual.field.imp_entregado" />                               
                       </label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input name="importe2_text" id="importe2_text" type="text" class="form-control importe" disabled="disabled">
							<span class="importe">&euro;</span>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12">
	                       <select name="importe2" id="selector_importe2" class="form-control">
								<option value=""></option>
								<x:forEach select="$ventareserva_formaspago_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
					       </select>
						</div>                       
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
	                   <label class="control-label col-md-4 col-sm-4 col-xs-12">
                       		<spring:message code="venta.ventareserva.dialog.venta_individual.field.imp_entregado" />                               
                       </label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input name="importe3_text" id="importe3_text" type="text" class="form-control importe" disabled="disabled">
							<span class="importe">&euro;</span>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-12">
	                       <select name="importe3" id="selector_importe3" class="form-control" >
								<option value=""></option>
								<x:forEach select="$ventareserva_formaspago_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
					       </select>
						</div>                       
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12">
			            <label class="control-label col-md-4 col-sm-4 col-xs-4">
			            	<spring:message code="venta.ventareserva.dialog.venta_individual.field.imp_devolver" />                               
			            </label>
			            <label class="control-label col-md-8 col-sm-8 col-xs-8 euros">
							<span class="total-lbl"><span id="total-devolver">0.00</span>&euro;</span>		
			            </label>
		            </div>
		        </div>
				
			</div>
			
			<div class="callcenter">

				<div class="x_title">
					<h5>&nbsp;</h5>
					<div class="clearfix"></div>
				</div>

				<div class="form-group">
					<div class="checkbox col-md-6 col-sm-6 col-xs-12">
						<input type="checkbox" name="selector" id="carta_confirmacion" value="" class="flat" style="position: absolute; opacity: 0;">
						<span style="float: right;"><strong><spring:message code="venta.ventareserva.dialog.venta_individual.field.carta_confirmacion" /></strong></span>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
                       <select name="carta_confirmacion_select" id="carta_confirmacion_select" class="form-control">
							<option value="pdf">pdf</option>
							<option value="email">email</option>
							<option value="fax">pdf</option>
				       </select>
					</div>                       
				</div>
			
				<div class="form-group">
                   <label class="control-label col-md-6 col-sm-6 col-xs-12">
                  		<spring:message code="venta.ventareserva.dialog.venta_individual.field.idioma_carta" />                               
                    </label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select name="idiomas[]" id="selector_idiomas" class="form-control" style="width: 100%">
							<%-- <x:forEach select="$ventareserva_selector_idiomas_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
							</x:forEach> --%>
						</select>
					</div>                       
				</div>

			</div>

		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_venta_button" type="button" class="btn btn-primary save_dialog">
					<spring:message code="common.button.confirm" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
		
	</form>

</div>

<script>

hideSpinner("#button_venta_individual_vender");

cargarSelectorIdiomas("selector_idiomas");

var importe_total=Number($(".totales_venta_abono #total-val").text()),
	importe_devolver= -importe_total,
	importe_pendiente=0,
	idpais= $("#pais_dialog_venta_individual").val(),
	tipocanal= "${sessionScope.idTipocanal}";
	
if (idpais=="") idpais= "472";  // Si no se indica pais se establece España por defecto


var datos_titular = dt_listventaabono.rows().data()[0][0].LineadetalleVentaPasesDTO.datosClienteClub;

$("#nombre").val(datos_titular.nombrecompleto);
$("#nif").val(datos_titular.nif);
$("#cp_callcenter").val(datos_titular.cp);
$("#telefono").val(datos_titular.fijo);
$("#movil").val(datos_titular.movil);
$("#email").val(datos_titular.email);


$("#total-cobrar").text(importe_total.toFixed(2));
$("#total-devolver").text(importe_devolver.toFixed(2));

function calcularPendiente() {
	var suma= Number(isNaN($("#importe1_text").val())?"0":$("#importe1_text").val())+Number(isNaN($("#importe2_text").val())?"0":$("#importe2_text").val())+Number(isNaN($("#importe3_text").val())?"0":$("#importe3_text").val());
	importe_pendiente= importe_total-suma;
	return;	
}

function calcularDevolver() {
	var suma= Number(isNaN($("#importe1_text").val())?"0":$("#importe1_text").val())+Number(isNaN($("#importe2_text").val())?"0":$("#importe2_text").val())+Number(isNaN($("#importe3_text").val())?"0":$("#importe3_text").val());
	importe_devolver= suma-importe_total;
	$("#total-devolver").text(importe_devolver.toFixed(2));
	return;	
}

/* Se rellena el formulario de CallCenter si hay un cliente seleccionado */

if ($("#idcliente_venta_individual").val()!="") {
	$("#cp_callcenter").val($("#cpcliente_venta_individual").val());
	$("#nombre").val($("#cliente_venta_individual").val());
	$("#nif").val($("#cifcliente_venta_individual").val());
	$("#telefono").val($("#telefonocliente_venta_individual").val());
	$("#movil").val($("#telefonomovilcliente_venta_individual").val());
	$("#email").val($("#emailcliente_venta_individual").val());
}

$("#cp_taquilla").val($("#cpcliente_venta_individual").val());

if ($("input.flat")[0]) {
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });
}

/* Se agrupan las funciones relativas a la modificación de los importes ya que son comunes 
 * para las tres.
 */

function selectorImporteChange(nombre) {

	$("#selector_"+nombre).on("change", function(e) {
		$("#"+nombre+"_text").val("");
		calcularPendiente();
		if ($("#selector_"+nombre+" :selected").text()=="") {
			$('input[name="'+nombre+'_text"]').val("");
	    	$('input[name="'+nombre+'_text"]').attr('disabled', 'disabled');
		} else {
			$('input[name="'+nombre+'_text"]').removeAttr('disabled');
	    	$("#"+nombre+"_text").val(importe_pendiente.toFixed(2));
		}
		calcularDevolver();
	});

	$("#"+nombre+"_text").on("change", function(e) {
		calcularDevolver();	
	});

	$("#"+nombre+"_text").on("focusout", function(e) {
		var valor= Number($("#"+nombre+"_text").val());
		$("#"+nombre+"_text").val(valor.toFixed(2));
	});
	
}

selectorImporteChange("importe1");
selectorImporteChange("importe2");
selectorImporteChange("importe3");

//********************************************************************************
function construir_xml_venta_individual() {
	var cp_txt= (tipocanal=="1")?$("#cp_taquilla").val():$("#cp_callcenter").val(),
		imp_recibo= ($("#oimprecibo").is(":checked"))?1:0,
		imp_copia= ($("#oimpcopia").is(":checked"))?1:0,
		imp_entradas= ($("#oimpentradas").is(":checked"))?1:0,
		xml_lineasdetalle= "<VentaPaseParam>";
	
    xml_lineasdetalle+= str_titular;
    xml_lineasdetalle+= "<lineadetalles>";
    
    var lineas_detalle = dt_listventaabono.rows().data();
    for (var i=0; i<lineas_detalle.length; i++) { 		
		xml_lineasdetalle+=json2xml(lineas_detalle[i][0],"");		
	}
    xml_lineasdetalle+= "</lineadetalles>";
	
    xml_lineasdetalle+="<importeparcials>"; 
	xml_lineasdetalle+="<Importeparcial>";
	xml_lineasdetalle+="<formapago>";
	xml_lineasdetalle+="<idformapago>"+$("#selector_importe1").val()+"</idformapago>";
	xml_lineasdetalle+="</formapago>";
	xml_lineasdetalle+="<importe>"+$("#importe1_text").val()+"</importe>";
	xml_lineasdetalle+="</Importeparcial>";
	xml_lineasdetalle+="<Importeparcial>";
	xml_lineasdetalle+="<formapago>";
	xml_lineasdetalle+="<idformapago>"+$("#selector_importe2").val()+"</idformapago>";
	xml_lineasdetalle+="</formapago>";
	xml_lineasdetalle+="<importe>"+$("#importe2_text").val()+"</importe>";
	xml_lineasdetalle+="</Importeparcial>";
	xml_lineasdetalle+="<Importeparcial>";
	xml_lineasdetalle+="<formapago>";
	xml_lineasdetalle+="<idformapago>"+$("#selector_importe3").val()+"</idformapago>";
	xml_lineasdetalle+="</formapago>";
	xml_lineasdetalle+="<importe>"+$("#importe3_text").val()+"</importe>";
	xml_lineasdetalle+="</Importeparcial>";
	xml_lineasdetalle+="</importeparcials>";
	
	xml_lineasdetalle+= "<reciboimpreso>"+imp_recibo+"</reciboimpreso>";
	
	
	xml_lineasdetalle+= "<nombre>"+$("#nombre").val()+"</nombre>";
	xml_lineasdetalle+= "<nif>"+$("#nif").val()+"</nif>";
	xml_lineasdetalle+="<cp>"+$("#cp_callcenter").val()+"</cp>";
	xml_lineasdetalle+= "<telefono>"+$("#telefono").val()+"</telefono>";
	xml_lineasdetalle+="<email>"+$("#email").val()+"</email>";
	xml_lineasdetalle+= "<observaciones>"+$("#observaciones").val()+"</observaciones>";
	xml_lineasdetalle+= "<telefonomovil>"+$("#movil").val()+"</telefonomovil>";
	xml_lineasdetalle+= "<renovacion>"+renovacion+"</renovacion>";
	xml_lineasdetalle+="<importetotalventa>"+importe_total+"</importetotalventa>";
	xml_lineasdetalle+="</VentaPaseParam>";
	
	
	return(xml_lineasdetalle);
}

//********************************************************************************	

function saveFormAbono() {
	showButtonSpinner("#save_venta_button");
    
	
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/realizar_venta_abono.do'/>",
		timeout : 100000,
		data: {
			xml: construir_xml_venta_individual()
		},
		success : function(data) {			
			if ($("#oimpcontrato").is(":checked"))
				window.open("../../jasper.post?idcliente=&report=carta_contrato_club_2016&medioEnvio=&format=pdf&idioma=4&idVenta="+data.ok.Venta.idventa,"_blank");
		
			if ($("#opases").is(":checked"))
				{
					var xml_pases = "<ParamSolicitudDatosImpresion><idventa>"+data.ok.Venta.idventa+"</idventa></ParamSolicitudDatosImpresion>";
					
					$.ajax({
						contenttype: "application/json; charset=utf-8",
						type : "post",
						url : "<c:url value='/ajax/venta/ventareserva/pases_club/listado_impresion.do'/>",
						timeout : 100000,
						data: {
							xml: xml_pases
						},
						success : function(data) {
							
							json= data;
							
							$("#modal-dialog-form-4 .modal-content").load("<c:url value='/ajax/venta/ventareserva/pases_club/pantalla_impresion.do'/>?xml="+xml_pases, function() {
								$("#modal-dialog-form-4").modal('show');
								setModalDialogSize("#modal-dialog-form-4", "xs");
							});			
						},
						error : function(exception) {
							new PNotify({
							      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
							      text: exception.responseText,
								  type: "error",		     
								  delay: 5000,
								  buttons: { sticker: false }
							   });			
						}
					});
				}

			
			hideSpinner("#modal-dialog-form .modal-content");
			$("#modal-dialog-form-3").modal('hide');
			$("#modal-dialog-form-2").modal('hide');
			$("#modal-dialog-form").modal('hide');
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="venta.ventareserva.dialog.venta_grupos_abono..text.venta_realizada" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});					
		},
		error : function(exception) {
			hideSpinner("#modal-dialog-form .modal-content");
			hideSpinner("#save_venta_button");
			
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});		
}	

$("#save_venta_button").on("click", function(e) {
	$("#form_venta_individual_dialog").submit();
})

$("#form_venta_individual_dialog").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		saveFormAbono();		
	}
});


</script>

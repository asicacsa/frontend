<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${newabono_selector_productosdisponibles}" var="newabono_selector_productosdisponibles_xml" />
<x:parse xml="${newabono_selector_modelospase}" var="newabono_selector_modelospase_xml" />
<x:parse xml="${newabono_selector_tiposidentificador}" var="newabono_selector_tiposidentificador_xml" />
<x:parse xml="${newabono_selector_sexos}" var="newabono_selector_sexos_xml" />
<x:parse xml="${newabono_selector_parentesco}" var="newabono_selector_parentesco_xml" />
				



<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.nuevo_abono.title" />
	</h4>
</div>
<div class="modal-body">
		
<form id="form_new_abono" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.producto" />:</label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<select name="selector_producto_pases_bono" id="selector_producto_pases_bono" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$newabono_selector_productosdisponibles_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.modelo" />:</label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<select name="selector_modelo_pases_bono" id="selector_modelo_pases_bono" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$newabono_selector_modelospase_xml/ArrayList/CacModeloPase" var="item">
							<option value="<x:out select="$item/idModelo" />"><x:out select="$item/descripcion" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			<div class="form-group col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-3 col-sm-3 col-xs-12">
					<select name="selector_identificador_pases_bono" id="selector_identificador_pases_bono" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$newabono_selector_tiposidentificador_xml/ArrayList/Tipoidentificador" var="item">
							<option value="<x:out select="$item/idtipoidentificador" />"><x:out select="$item/nombre" /></option>
						</x:forEach>
					</select>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12">
						<input name="identificador_pases_club" id="identificador_pases_club" type="text" class="form-control"  value="">
					</div>
					<button type="button" class="btn btn-success btn-xs col-md-4 col-sm-4 col-xs-12" id="tab_pases_club_new_validar">
						<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.validar"/>
					</button>				
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.nombre" /></label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<input name="nombre_new_pases_club" id="nombre_new_pases_club" type="text" class="form-control" required="required" value="">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.apellido1" /></label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<input name="apellido1_new_pases_club" id="apellido1_new_pases_club" type="text" class="form-control" required="required" value="">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.apellido2" /></label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<input name="apellido2_new_pases_club" id="apellido2_new_pases_club" type="text" class="form-control" value="">
				</div>
			</div>	
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.sexo" />:</label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<select name="selector_sexo_pases_bono" id="selector_sexo_pases_bono" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$newabono_selector_sexos_xml/ArrayList/CacSexo" var="item">
							<option value="<x:out select="$item/codSexo" />"><x:out select="$item/descripcion" /></option>
						</x:forEach>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.perfil" /></label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<select name="selector_perfil_pases_bono" id="selector_perfil_pases_bono" class="form-control" required="required">
						<option value=""></option>						
					</select>
				</div>
			</div>	
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.fechanacimiento" /></label>
				<div class="col-md-9 col-sm-9 col-xs-12">
	            	<div class="form-group date-picker">
						<div class="col-md-6 col-sm-6 col-xs-12">
						  <a type="button" class="btn btn-default btn-clear-date" id="button_fecha_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.productos.tabs.formasdepago.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
						  </a>			
                        <div class="input-prepend input-group">
                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                          <input type="text" name="fecha_nacimiento_pases" id="fecha_nacimiento_pases" class="form-control" value="" readonly/>
                          <input type="hidden" required="required" name="fecha_nacimiento_pases_club" />
                        </div>
					</div>
				</div>		
				</div>
			</div>


			
			
			

			
						
	</div>
<div class="col-md-6 col-sm-6 col-xs-12">
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.tfnofijo" /></label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<input name="tfno_fijo_new_pases_club" id="tfno_fijo_new_pases_club" data-inputmask="'mask': '9{0,10}'" data-mask type="text" class="form-control" value="">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.tfnomovil" /></label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<input name="tfno_movil_new_pases_club" id="tfno_movil_new_pases_club" data-inputmask="'mask': '9{0,10}'" data-mask type="text" class="form-control" value="">
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">	
			<input name="informacionmovil" type="checkbox" class="js-switch" data-switchery="true" style="display: none;">
				<strong><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.informacion" /></strong>
		</div>
	</div>		
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.email" /></label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<input name="email_new_pases_club" id="email_new_pases_club" type="text" class="form-control" value="">
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">	
			<input name="informacionemail" type="checkbox" class="js-switch" data-switchery="true" style="display: none;">
			<strong><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.informacion" /></strong>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.rol" />:</label>
		<div class="col-md-8 col-sm-8 col-xs-12">
			<select name="selector_rol_pases_bono" id="selector_rol_pases_bono" class="form-control" required="required">
				<option value=""></option>
				<x:forEach select="$newabono_selector_parentesco_xml/ArrayList/CacParentesco" var="item">
					<option value="<x:out select="$item/codParentesco" />"><x:out select="$item/descripcion" /></option>
				</x:forEach>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.nombreFoto" />:</label>
		<div class="col-md-9 col-sm-9 col-xs-12">
			<input name="nombrefoto_miembro" id="nombrefoto_miembo" type="text" class="form-control" value="">
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-1 col-sm-1 col-xs-12">&nbsp;</div>
		    <div class="col-md-10 col-sm-10 col-xs-12">	
			<br/><canvas id="myCanvas" width="380" height="350"></canvas><br/>	
			</div>
			<div class="col-md-1 col-sm-1 col-xs-12">&nbsp;</div>	    
		</div>		
		<div class="btn-group pull-right  btn-datatable">	
		<button type="button" class="btn btn-success" id="tab_pases_club_foto">
			<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.foto"/>
		</button>
		</div>
	</div>	
</div>
</form>
	<div class="modal-footer">
		<button id="finalizar_button"  type="button" class="btn btn-primary save_dialog">
					<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.finalizar" />
				</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
		<button id="grabar_anadir_button" type="button" class="btn btn-primary save_dialog">
			<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.field.grabar" />
		</button>
	</div>		

</div>

<script>
//***************************************************************
var json_datos;

var tipoVia="";
var numero="";
var anadir_miembro = false;
var data;
//***************************************************************
$("#finalizar_button").hide();
$("#grabar_anadir_button").hide();
//***************************************************************
hideSpinner("#grabar_anadir_button");



//***************************************************************
$("#tab_pases_club_new_validar").on("click", function(e) {
	
	if( $('#selector_producto_pases_bono').val()=="")
	{
		new PNotify({
			title: '<spring:message code="common.dialog.text.error" />',
			text: '<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.alert.ningun_producto" />',
			type: "error",
			delay: 5000,
			buttons: { sticker: false }
			});
	 }else
	 {
		 if( $('#selector_identificador_pases_bono').val()=="")
			{
				new PNotify({
					title: '<spring:message code="common.dialog.text.error" />',
					text: '<spring:message code="venta.ventareserva.tabs.gestion_pases_club.dialog.alert.ningun_identificador" />',
					type: "error",
					delay: 5000,
					buttons: { sticker: false }
					});
			 }else
				 {	
				  var data = $("#form_new_abono").serializeObject();
				  $("#finalizar_button").show();
				  $("#grabar_anadir_button").show();
				  var identificador =""+ $("#identificador_pases_club").val()
				  console.log("identificador"+identificador)
				  if(identificador!="") 
				  {
				  showSpinner("#modal-dialog-form .modal-content");			
					$.ajax({
						type : "post",
						url : "<c:url value='/ajax/venta/ventareserva/pases_club/validar_contra_sim.do'/>",
						timeout : 100000,
						data : data,
						success : function(data) {
							 hideSpinner("#modal-dialog-form .modal-content");
						     json_datos=data;							 
							 $("#nombre_new_pases_club").val(json_datos.DatosClienteClubDTOCompleto.nombre);
							 $("#apellido1_new_pases_club").val(json_datos.DatosClienteClubDTOCompleto.apellido1);
							 $("#apellido2_new_pases_club").val(json_datos.DatosClienteClubDTOCompleto.apellido2);
							 $('#selector_sexo_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompleto.CacSexo.codSexo+'"]').attr("selected", "selected");
							 $('#selector_perfil_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompleto.Perfilvisitante+'"]').attr("selected", "selected");
							 $("#tfno_fijo_new_pases_club").val(json_datos.DatosClienteClubDTOCompleto.fijo);
							 $("#tfno_movil_new_pases_club").val(json_datos.DatosClienteClubDTOCompleto.movil);
							 $("#email_new_pases_club").val(json_datos.DatosClienteClubDTOCompleto.email);
							 $('#selector_rol_pases_bono option[value="'+json_datos.DatosClienteClubDTOCompleto.CacParentesco+'"]').attr("selected", "selected");
							 
							 tipoVia  = json_datos.DatosClienteClubDTOCompleto.CacTipoVia.codTipoVia;
							 numero = json_datos.DatosClienteClubDTOCompleto.numvia;
							 

							 
							 
							
						},
						error : function(exception) {
							
							hideSpinner("#modal-dialog-form .modal-content");
			
							new PNotify({
								title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
								text : exception.responseText,
								type : "error",
								delay : 5000,
								buttons : {
									closer : true,
									sticker : false
								}
							});
						}
					});
				  }
				 }//fin else
	 }
});
//**************************************************************
$( "#selector_producto_pases_bono" ).change(function() {
	new_abono_cargar_selector_perfiles();
})	
//**************************************************************
function new_abono_cargar_selector_perfiles()
{	
	var producto = $("#selector_producto_pases_bono").val();
	$("#selector_perfil_pases_bono").find("option").remove();
		
	$.ajax({
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/pases_club/select_list_perfiles.do'/>?id="+producto,
		timeout : 100000,
		data : null,
		dataType : 'json',
		success : function(data) {
			if (data.ArrayList.LabelValue!=null && typeof data.ArrayList.LabelValue!="undefined") {
			$.each(data.ArrayList.LabelValue, function(i, item) {
			    $("#selector_perfil_pases_bono").append('<option value="'+item.value[0]+'">'+item.label+'</option>');			    
			});
			}
		}
	});
}
//**************************************************************
$today= moment().format("DD/MM/YYYY");
dia_inicio = $today;

if($('input[name="fecha_nacimiento_pases_club"]').val()!='')
{	
dia_inicio = $('input[name="fecha_nacimiento_pases_club"]').val().substring(0, 10);
}

$('input[name="fecha_nacimiento_pases"]').val( dia_inicio);

$('input[name="fecha_nacimiento_pases"]').daterangepicker({
	singleDatePicker: true,
	autoUpdateInput: false,
	linkedCalendars: false,
	showDropdowns: true,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start) {
  	  	 $('input[name="fecha_nacimiento_pases"]').val(start.format('DD/MM/YYYY'));
  	  	 $('input[name="fecha_nacimiento_pases_club"]').val(start.format('DD/MM/YYYY'));
  	  	 
	 });
//*******************************************************************************
$("#button_fecha_clear").on("click", function(e) {
    $('input[name="fecha_nacimiento_pases"]').val('');
    $('input[name="fecha_nacimiento_pases_club"]').val('');    
});


//**************************************************************
$(".js-switch").each(function(index) {
	var s = new Switchery(this, {
		size : 'small'
	});
});
//***************************************************************
$(":input").inputmask();
//*********************************************

$("#tab_pases_club_foto").on("click", function(e) {
	var identificador =$("#identificador_pases_club").val();
	var nombre= $("#nombre_new_pases_club").val();
	var apellido =$("#apellido1_new_pases_club").val();
	var nom_foto =identificador.concat(nombre,apellido);
	$("#nombrefoto_miembo").val(nom_foto);	
	
	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/venta/ventareserva/tomar_foto.do'/>", function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "xs");
	});     
});

///**************************************************************************************
$("#finalizar_button").on("click", function(e) { 
	anadir_miembro = false;	
	$("#form_new_abono").submit();			
	})
//***************************************************************************************
$("#form_new_abono").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFormDataFinalizar();
		}
	}
	);

//**************************************************************
$("#grabar_anadir_button").on("click", function(e) {
	anadir_miembro = true;
	$("#form_new_abono").submit();   
});

//******************************************************************

function saveFormDataFinalizar()
{
	data = $("#form_new_abono").serializeObject();

	 var parametros = "titular=0&"; 
	 renovacion="";
	 
     parametros+="codParentesco="+data.selector_rol_pases_bono+"&";
     var deseamovil = data.informacionmovil;
 	 if(deseamovil==undefined)
 		parametros+="deseamovil=0&";
 	 else
 		parametros+="deseamovil=1&"; 	
 	 var deseaemail = data.informacionemail;
 	 if(deseaemail==undefined)
 		parametros+="deseaemail=0&"; 
 	 else
 		parametros+="deseaemail=1&";  
     parametros+="email="+data.email_new_pases_club+"&";
     parametros+="movil="+data.tfno_movil_new_pases_club+"&";
     parametros+="fijo="+data.tfno_fijo_new_pases_club+"&";    
     parametros+="fechanacimiento="+data.fecha_nacimiento_pases_club+"&";
     parametros+="codSexo="+data.selector_sexo_pases_bono+"&";
     parametros+="apellido2="+data.apellido2_new_pases_club+"&";
     parametros+="apellido1="+data.apellido1_new_pases_club+"&";
     parametros+="nombre="+data.nombre_new_pases_club+"&";
     parametros+="idModelo="+data.selector_modelo_pases_bono+"&";
     parametros+="nif="+data.identificador_pases_club+"&";
     parametros+="isLdGratuita=false&";
     parametros+="idLineadetalle=&";
     parametros+="tipoLineadetalle=LineadetalleVentaPasesDTO&";
     parametros+="idtipoventa=5&";
     parametros+="idtipoidentificador="+data.selector_identificador_pases_bono+"&";
     parametros+="idcanal=2&";
     parametros+="idperfilvisitante="+data.selector_perfil_pases_bono+"&";
     parametros+="idproducto="+data.selector_producto_pases_bono+"&";
     parametros+="numvia="+numero+"&";
     parametros+="codTipoVia="+tipoVia;


  	if(anadir_miembro)
		showButtonSpinner("#grabar_anadir_button");		
	else
		showButtonSpinner("#finalizar_button");	
  
     var url = "<c:url value='/ajax/venta/ventareserva/list_productos_abono.do'/>?"+encodeURI(parametros);
     $.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : url,
			timeout : 100000,
			data: { 
				
				}, 
			success : function(data) {
				guardarFoto(data.LineadetalleVentaPasesDTO.datosClienteClub.nombrefoto);
				list_abonos.push(data);
				if(anadir_miembro)
					{
					$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/pases_club/new_abono_miembro.do'/>", function() {					
					}); 
					}
				else
					{
					$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_productos.do'/>", function() {
						$("#modal-dialog-form-2").modal('show');
						$("#modal-dialog-form").modal('hide');
						setModalDialogSize("#modal-dialog-form-2", "md");
					}); 
					}
			},
			error : function(exception) {
				hideSpinner("#finalizar_button");
				hideSpinner("#grabar_anadir_button");			
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",		     
					  delay: 5000,
					  buttons: { sticker: false }
				   });			
			}
		});

	
	
	
	
}

//********************************************************
function guardarFoto(nombreFoto)
{
	var contenidoImagen = $(document).find('#myCanvas').get(0).toDataURL();
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/savePhoto.do'/>",
		timeout : 100000,
		data: {
			nombreFoto: nombreFoto,
			contenido:contenidoImagen
		}, 
		success : function(data) {

				
		},
		error : function(exception) {
		}
	});
	
	
}

//********************************************************
$("#identificador_pases_club").change(function() {
  $("#finalizar_button").hide();
  $("#grabar_anadir_button").hide();  
})
</script>
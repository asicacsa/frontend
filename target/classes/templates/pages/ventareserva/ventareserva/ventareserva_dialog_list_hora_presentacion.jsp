<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${hora_presentacion_datos}" var="hora_presentacion_datos_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.hora_presentacion.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_listar_hora_presentacion" class="form-horizontal form-label-left">
	
		<x:forEach select = "$hora_presentacion_datos_xml/ArrayList/tipoproducto" var = "item">
			<x:forEach select = "$item/sesions/sesion" var = "sesion">
				<div class="form-group">
					<div class="col-md-12 col-sm-12 col-xs-12 sesion" id="<x:out select = "$sesion/idsesion" />">
						<label class="control-label col-md-6 col-sm-6 col-xs-12"><x:out select = "$sesion/nombre" /></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
		                      <select name="presentacionsesion_select_<x:out select = "$sesion/idsesion" />" id="presentacionsesion_select_<x:out select = "$sesion/idsesion" />" class="form-control">
						   		<option></option>
								<x:forEach select="$sesion/presentacionsesions/presentacionsesion" var="presentacion">
									<option value="<x:out select="$presentacion/idpresentacionsesion" />"><x:out select="$presentacion/label" /></option>
								</x:forEach>
					       </select>
						</div>    					
					</div>
				</div>
			</x:forEach>
		</x:forEach>
		
	</form>

	<div class="modal-footer">
		<button id="button_listar_hora_presentacion_aceptar" type="button" class="btn btn-primary save_dialog">
			<spring:message code="common.button.accept" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>
	
</div>

<script>
function cargarHoraPresentacion() {
	for (var i=0; i<${list}.rows().data().length; i++) {
		var data=${list}.rows().data()[i][0];
		var idsesion= data.Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.idsesion;
		$("#presentacionsesion_select_"+idsesion).val(data.Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.presentacionsesion.idpresentacionsesion);
	}
}

function asignarHoraPresentacion(idsesion,idpresentacionsesion) {
	for (var i=0; i<${list}.rows().data().length; i++) {
		var data=${list}.rows().data()[i][0];
		if (data.Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.idsesion==idsesion)
			data.Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.presentacionsesion.idpresentacionsesion=idpresentacionsesion;
	}
}

hideSpinner("#button_${buttonAdd}");
cargarHoraPresentacion();

$("#button_listar_hora_presentacion_aceptar").on("click", function(e) {
	showButtonSpinner("#button_listar_hora_presentacion_aceptar");
	$.each($(".sesion"),function() {
	    var div_id= $(this).prop("id");
	    asignarHoraPresentacion(div_id,$("#presentacionsesion_select_"+div_id+" :selected").val());
	});	
	hideSpinner("#button_listar_hora_presentacion_aceptar");
	$("#modal-dialog-form").modal('hide');
});

</script>


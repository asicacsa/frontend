<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${tiposAbono}" var="tiposAbono_xml" />
<x:parse xml="${productos}" var="productos_xml" />
<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2><spring:message code="common.text.filter_list" /></h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">
	  		<form id="form_filter_list_venta_abonos" class="form-horizontal form-label-left">
	  			<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.busqueda.tabs.venta_abono.venta" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<input name="refventa" id="refventa_abono" type="text" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.busqueda.tabs.venta_abono.abono" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<input name="refabono" id="refabono" type="text" class="form-control">
						</div>
					</div>
					<div class="form-group date-picker">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.busqueda.tabs.venta_abono.field.fechas" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<a type="button" class="btn btn-default btn-clear-date" id="button_fechas_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada_busquedas.tabs.ventas.field.fechas.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
							</a>
							<div class="input-prepend input-group">
								<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span> 
								<input type="text" name="fechas" id="fechas" class="form-control" value="" readonly /> 
								<input type="hidden" name="fechaoperacioninicial" value="" /> 
								<input type="hidden" name="fechaoperacionfinal" value="" />
							</div>
						</div>
					</div>				
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.busqueda.tabs.venta_abono.producto" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<select id="idproducto" name="idproducto" class="form-control">
								<option> </option>
								<x:forEach select="$productos_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>								
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.busqueda.tabs.venta_abono.tipo_abono" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<select id="idtipoabono" name="idtipoabono" class="form-control">
								<option> </option>
								<x:forEach select="$tiposAbono_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>								
							</select>							
						</div>
					</div>	
																	
										
				</div>
				
				<div class="clearfix"></div>
				<div class="ln_solid"></div>
				<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
					<button id="button_clean_venta_abono" type="button" class="btn btn-success pull-right">
						<spring:message code="venta.ventareserva.tabs.canje-bono.button.clean" />
					</button>
					<button id="button_filter_list_ventas_abonos" type="button" class="btn btn-success pull-right">
						<spring:message code="common.button.filter" />
					</button>
				</div>	
				<input type="hidden" name="idcliente" value=""/>
				<input type="hidden" name="start" value=""/>
				<input type="hidden" name="length" value=""/>			
	  		</form>
	</div>
</div>


<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_abono_editar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.venta_abono.lists.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_abono_anular">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.venta_abono.lists.button.anular" />"> <span class="fa fa-times"></span>
			</span>
		</a>
	</div>

	<table id="datatable-list-venta_abonos" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="venta.busquedas.tabs.abonos.list.header.ref_venta" /></th>
				  <th><spring:message code="venta.busquedas.tabs.abonos.list.header.fecha" /></th>
				<th><spring:message code="venta.busquedas.tabs.abonos.list.header.cliente" /></th>
				<th><spring:message code="venta.busquedas.tabs.abonos.list.header.imp_total" /></th>
				<th><spring:message code="venta.busquedas.tabs.abonos.list.header.imp_pagado" /></th>
				<th><spring:message code="venta.busquedas.tabs.abonos.list.header.imp_pendiente" /></th>
				<th><spring:message code="venta.busquedas.tabs.abonos.list.header.anulado" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>


<script>
$today= moment().format("DD/MM/YYYY");
var dia_inicio = $today;
//***************************************************************************************
$('input[name="fechas"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
		 $('#form_filter_list_venta_abonos input[name="fechas"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('#form_filter_list_venta_abonos input[name="fechaoperacioninicial"]').val(start.format('DD/MM/YYYY'));
  	  	 $('#form_filter_list_venta_abonos input[name="fechaoperacionfinal"]').val(end.format('DD/MM/YYYY'));
	 });
//*************************************************************************************** 
$("#button_fechas_clear").on("click", function(e) {
	$('#form_filter_list_venta_abonos input[name="fechas"]').val('');
	$('#form_filter_list_venta_abonos input[name="fechaoperacioninicial"]').val('');
	$('#form_filter_list_venta_abonos input[name="fechaoperacionfinal"]').val('');
});

//***************************************************************************************

var dt_listventaAbonos=$('#datatable-list-venta_abonos').DataTable( {	
	serverSide: true,
	ordering: false,
	lengthMenu:[[25,50,100],[25,50,100]],
	"pageLength":25,
	pagingType: "simple",
	info: false,
	deferLoading: 0,
    ajax: {
        url: "<c:url value='/ajax/venta/ventareserva/venta_abono/list_venta_abono.do'/>",
        rowId: 'idventa',
        type: 'POST',
        dataSrc: function (json) {         	
        	if (json!=null && typeof json!="undefined") 
        		{

        		return (sanitizeJSON(json.ArrayList.Venta));
        		}
        	else
        		{
        	
        		return(""); 
        		}
        	},
        data: function (params) { 
        	$('#form_filter_list_venta_abonos input[name="start"]').val(params.start);
        	$('#form_filter_list_venta_abonos input[name="length"]').val(params.length);
        	return($("#form_filter_list_venta_abonos").serializeObject()); }
        	,
            error: function (xhr, error, code)
                 {
                    	new PNotify({
     					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
     					text : xhr.responseText,
     					type : "error",
     					delay : 5000,
     					buttons : {
     						closer : true,
     						sticker : false
     					}					
     				});
                 }
    },
    initComplete: function( settings, json ) {
    	$('#datatable-list-venta_abonos_length').hide();
    	$('#datatable-list-venta_abonos_filter').hide();
        $('a#menu_toggle').on("click", function () {if (dt_listventaAbonos.data().count()>0) dt_listventaAbonos.columns.adjust().draw(); });
	},
    columns: [
		{ data: "idventa", type: "spanish-string", defaultContent: ""},
		{ data: "fechayhoraventa", type: "spanish-string", defaultContent: ""},
		{ data: "nombre", type: "spanish-string", defaultContent: ""},
		{ data: "importetotalventa", type: "spanish-string", defaultContent: ""},
		{ data: "importeparcialtotalventa", type: "spanish-string", defaultContent: ""},
		{ data: "importependiente", type: "spanish-string", defaultContent: "", render: function ( data, type, row, meta ) 
			{
			var importe = row.importetotalventa;
			var importepagado = row.importeparcialtotalventa;
			var importeRestante = importe - importepagado;
			return importeRestante.toFixed(2);
			
			return Math.round((importe - importepagado)*100)/100; 
			}},
		{ data: "estadooperacion.idestadooperacion", className: "text_icon cell_centered",
			render: function ( data, type, row, meta ) {
		  	  	if (data==4) return '<i class="fa fa-check" aria-hidden="true"></i>'; else return '<i class="fa fa-close" aria-hidden="true"></i>';	
		}}
    ],    
   
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('datatable-list-venta_abonos', 'right');   
    	},
    
    select: { style: 'os'},
	language: dataTableLanguage,
	processing: true,
} );


insertSmallSpinner("#datatable-list-venta_abonos_processing");
//********************************************************************************
$("#button_filter_list_ventas_abonos").on("click", function(e) {
	dt_listventaAbonos.ajax.reload(); 
})


//******************************************BOT�N ANULAR****************************
$("#tab_abono_anular").on("click", function(e) {
	
	var data = sanitizeArray(dt_listventaAbonos.rows( { selected: true } ).data(),"idventa");
	var tipo ="A";	
	var  importetotalventa =sanitizeArray(dt_listventaAbonos.rows( { selected: true } ).data(),"importetotalventa");
	var importetotalpagado=sanitizeArray(dt_listventaAbonos.rows( { selected: true } ).data(),"importeparcialtotalventa");
	if (data.length<=0 || data.length>1)  {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: ' <spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.alert.anular.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
			return;
		}else
		{
				var estado = dt_listventaAbonos.rows( { selected: true } ).data()[0].estadooperacion.idestadooperacion;
				if(estado==4)
		    		{
		    		new PNotify({
					      title: '<spring:message code="common.dialog.text.error" />',
					      text: ' <spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.alert.anular.ya_anuladada" />',
						  type: "error",
						  delay: 5000,
						  buttons: { sticker: false }
					   });		    		
					return;
		    		}
				showButtonSpinner("#tab_abono_anular");
				$("#modal-dialog-form .modal-content").load("<c:url value='/venta/ventareserva/busqueda/ventareserva/anular.do'/>?id="+data+"&tipo="+tipo+"&importetotalventa="+importetotalventa+"&importetotalpagado="+importetotalpagado+"&idcanal=${sessionScope.idcanal}", function() {
					$("#modal-dialog-form").modal('show');
					setModalDialogSize("#modal-dialog-form", "sm");
				});
					
		}
	
});


/***********************************************BOT�N EDITAR VENTA/RESERVA  *********************************/
$("#tab_abono_editar").on("click", function(e) {
	
	var data = sanitizeArray(dt_listventaAbonos.rows( { selected: true } ).data(),"idventa");
	
		
	if (data.length<=0 ||data.length>1)  {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: ' <spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.alert.editar.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}else
		{		
			showButtonSpinner("#tab_venta_reserva_editar");
							
			$("#modal-dialog-form .modal-content").load("<c:url value='/venta/ventareserva/busqueda/ventareserva/editar_abono.do'/>?id="+data, function() {
				$("#modal-dialog-form").modal('show');
				setModalDialogSize("#modal-dialog-form", "md");
			});			
	}
});




/***********************************************BOT�N EDITAR VENTA/RESERVA  *********************************/
$("#button_clean_venta_abono").on("click", function(e) {
	$("#fechas").val("");
	$("#refventa_abono").val("");
	$("#refabono").val("");
	
    $("#idtipoabono option:first").prop("selected", "selected");
    $("#idproducto option:first").prop("selected", "selected");
	
})




</script>
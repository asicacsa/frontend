<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${motivos_modificacion}" var="motivosModificacion_xml" />
<x:parse xml="${listado_formas_pago_por_canal}" var="selectorFormasdePagoPorCanal_xml" />
<x:parse xml="${datos_reserva_xml}" var="formas_pago_reserva_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="venta.tabs.venta_edit.reserva.editar.formas.title" />	 ${id}			
	</h4>	
</div>

<div class="modal-body">
	<form id="form_formas_pago_guardar" data-parsley-validate="" class="form-horizontal form-label-left" >
	<input type="hidden" id="xmlPagos" name="xmlPagos" />
	<input type="hidden" id="strMotivo" name="strMotivo" />
	
	<x:forEach select = "$formas_pago_reserva_xml/ArrayList/Venta" var = "item">
			<br/>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<spring:message code="venta.tabs.venta_edit.reserva.editar.formas.venta" /> <x:out select = "$item/idventa" />
				<div class="btn-group pull-right btn-datatable">	
					<div class="col-md-1 col-sm-1 col-xs-12">
						<a type="button" class="btn btn-default btn-new-operacion1" id="button_new_forma-<x:out select = "$item/idventa" />">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.numerada.tabs.venta_formaPago.button.nuevo_forma" />"> <span class="fa fa-plus"></span>
							</span>
						 </a>
					</div>
				</div>
			</div>
		
			<table id="datatable-list-edit_forma_pago-<x:out select = "$item/idventa" />" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th></th>
						<th></th>
						<th><spring:message code="venta.numerada.tabs.venta_formaPago.list.header.num_operacion" /></th>
						<th><spring:message code="venta.numerada.tabs.venta_formaPago.list.header.importe" /></th>
						<th><spring:message code="venta.numerada.tabs.venta_formaPago.list.header.formaPago" /></th>
						<th><spring:message code="venta.numerada.tabs.venta_formaPago.list.header.num_caja" /></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</x:forEach>
	
	
		<div class="form-group">			
			<label class="control-label col-md-6 col-sm-6 col-xs-12"><spring:message	code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.motivos" /></label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<select required="required" class="form-control" name="idmotivo_formapago" id="idmotivo_formapago">
						<option value=""></option>
							<x:forEach select="$motivosModificacion_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out
											select="$item/label" /></option>
								</x:forEach>
					</select>								
				</div>		
		</div>	
		
		<div class="modal-footer">
			<button id="aceptar_formas_button"  class="btn btn-primary">
						<spring:message code="common.button.accept" />
			</button>
			<button type="button" class="btn btn-cancel close_dialog2" data-dismiss="modal">
				<spring:message code="common.button.cancel"/>
			</button>
		</div>		
	</form>
</div>

<script>
hideSpinner("#tab_busqueda_ver_formas_pago_reserva");

var json = ${datos_reserva};
var idVenta = json.ArrayList.Venta.idventa;

var ventas_reserva = json.ArrayList.Venta;

   function crearDatatable(nombreTabla)
   {
 
		  var dt_listformaspago=$(nombreTabla).DataTable( {
				language: dataTableLanguage,
				info: false,
				searching: false,
				  initComplete: function( settings, json ) {
				    	 
				    },
				    scrollCollapse: true,
				ordering:  false,
				paging: false,	
			    select: { style: 'os' },
			    columns: [
			              {},   
			              {},
			              {},
			              {},
			              {},
			              {}      
			              
			        ],
			        "columnDefs": [
			                       { "visible": false, "targets": [0,1]}
			                     ],
			} ); 
	  
	  
   }


   
   if(ventas_reserva.length>=1)
	   {
		   $.each(ventas_reserva, function(key_ventas,venta_reserva) {
		   		var nombre_tabla = '#datatable-list-edit_forma_pago-' + venta_reserva.idventa;
		   		crearDatatable(nombre_tabla)
		   })
	   }
   else
	   {
		   var nombre_tabla = '#datatable-list-edit_forma_pago-' + ventas_reserva.idventa;
	  		crearDatatable(nombre_tabla)
	   }
   
   window.setTimeout(CargarFormasPago, 500);
  
   function CargarFormaPagoVenta(datatable,numero_caja,pagoVenta)
   {
	  
	   datatable.row.add([
   						pagoVenta,          
   						"",		
   						pagoVenta.idimporteparcial,
   						pagoVenta.importe, 
   						pagoVenta.formapago.nombre,         
   						numero_caja								    
   					]).draw();
   }
   
   
   function CargarFormasPagoVenta(pagosVenta)
   {
   	var nombre_tabla = '#datatable-list-edit_forma_pago-' + pagosVenta.idventa;
   	var datatable = $(nombre_tabla).DataTable();
   	var importePartials = pagosVenta.importeparcials.Importeparcial;
   	var numeroCaja = "";
    if(pagosVenta.operacioncaja.caja!=undefined)
    	numeroCaja = pagosVenta.operacioncaja.caja.idcaja;
   	if(importePartials.length>=0)
   		$.each(importePartials, function(key_importes,importePartial) {
   			CargarFormaPagoVenta(datatable,numeroCaja,importePartial);			
   		});
   	else
   		CargarFormaPagoVenta(datatable,numeroCaja,importePartials);
   }

   function CargarFormasPago()
   {
   	
   	if(ventas_reserva.length>=0)
   		$.each(ventas_reserva, function(key_ventas,venta_reserva) {
   			CargarFormasPagoVenta(venta_reserva);			
   		});
   	else
   		{
   		CargarFormasPagoVenta(ventas_reserva);
   		}
   }
   
   
   var dt_listformaspago;
   //Creamos los funcionamientos de los botones
   if(ventas_reserva.length>=0)   
		$.each(ventas_reserva, function(key_ventas,venta_reserva) {
			//****************************************************
			$("#button_new_forma-" + venta_reserva.idventa).on("click", function(e) {
			    
				var nombre_tabla = '#datatable-list-edit_forma_pago-' + venta_reserva.idventa;
				dt_listformaspago = $(nombre_tabla).DataTable();
	
				$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/venta/numerada/editarVenta/FormasPagoAnadir.do'/>", function() {
						$("#modal-dialog-form-3").modal('show');
						setModalDialogSize("#modal-dialog-form-3", "xs");
					});
			});	
		});
   else
	  {
	    idVenta = ventas_reserva.idventa;
	 	$("#button_new_forma-" + idVenta).on("click", function(e) {
	 		var nombre_tabla = '#datatable-list-edit_forma_pago-' + idVenta;
	 		dt_listformaspago = $(nombre_tabla).DataTable();

			$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/venta/numerada/editarVenta/FormasPagoAnadir.do'/>", function() {
					$("#modal-dialog-form-3").modal('show');
					setModalDialogSize("#modal-dialog-form-3", "xs");
				});
		});	
	  }
   
 //****************************************************
   $("#aceptar_formas_button").on("click", function(e) {
   	$("#form_formas_pago_guardar").validate();
   })

   $("#form_formas_pago_guardar").validate({
   		onfocusout : false,
   		onkeyup : false,
   		unhighlight: function(element, errClass) {
               $(element).popover('hide');
   		},		
   		errorPlacement : function(err, element) {
   			err.hide();
   			$(element).attr('data-content', err.text());
   			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
   			$(element).popover('show');									
   		},
   		submitHandler : function(form) {			
   			guardarFormasPago()			
   		}
   	}
   );
 
   	var xml="";
   	
   	function procesarVenta(idVenta)
   	{
   		dt_listformaspago = $('#datatable-list-edit_forma_pago-' + idVenta).DataTable();
   		xml+="<Venta><idventa>"+idVenta+"</idventa><operacioncaja/>";
   		xml+="<importeparcials>";   		
   		var pagos = dt_listformaspago.rows().data();
   		var numero = pagos.length;
   		if(numero>0)
   			for(i=0;i<numero;i++)
   				{
   				proscesarPago(pagos[i]);		
   				}
   		else
   			proscesarPago(fpagos);   		
   		
   		xml+="</importeparcials></Venta>";   		
   	}

   	function guardarFormasPago()
   	{
   		$("#strMotivo").val($("#idmotivo_formapago option:selected").text());
   		xml="<ArrayList>";
   		
   		if(ventas_reserva.length>=1)
	 	   {
	 		   $.each(ventas_reserva, function(key_ventas,venta_reserva) {
	 			  	procesarVenta(venta_reserva.idventa);	 		   		
	 		   })
	 	   }
	    	else
	 	   {
	 		  procesarVenta(ventas_reserva.idventa);
	 	   }
   		
   		
  		xml+="</ArrayList>";
   		$("#xmlPagos").val(xml);
   		
   		var data = $("#form_formas_pago_guardar").serializeObject();
   		
   		$.ajax({
   			type : "post",
   			url : "<c:url value='/ajax/ventareserva/busqueda/save_formas_de_pago_reserva.do'/>",
   			timeout : 100000,
   			data : data,
   			success : function(data) {		
   				new PNotify({
   					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
   					text : '<spring:message code="common.dialog.text.datos_guardados" />',
   					type : "success",
   					delay : 5000,
   					buttons : {
   						closer : true,
   						sticker : false
   					}
   				});
   				$("#modal-dialog-form-2").modal('hide');
   			},
   			error : function(exception) {
   				

   				new PNotify({
   					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
   					text : exception.responseText,
   					type : "error",
   					delay : 5000,
   					buttons : {
   						closer : true,
   						sticker : false
   					}
   				});
   			}
   		});
   		

   	}
   	
   	function proscesarPago(fila)
   	{
   		if(fila[1]=="")
   		{

   		var jsonFila = fila[0];
   		xml+="<Importeparcial isnew=\"false\" anulado=\"0\">";
   		xml+="<idimporteparcial>"+jsonFila.idimporteparcial+"</idimporteparcial>";
   		xml+="<formapago>"+json2xml(jsonFila.formapago)+"</formapago>";
   		xml+="<importe>"+jsonFila.importe+"</importe>";
   		xml+="<bono/><modificacionimporteparcials/>";
   		xml+="</Importeparcial>";
   		}
   	else
   		{
   		xml+="<Importeparcial isnew=\"true\" anulado=\"0\">";
   		xml+="<formapago>";
   		xml+="<idformapago>"+fila[1]+"</idformapago>";
   		xml+="<nombre></nombre>";
   		xml+="</formapago>";
   		xml+="<importe>"+fila[3]+"</importe>";
   		xml+="<idimporteparcial/><rts/><numpedido/><anulado/>";
   		xml+="</Importeparcial>";
   		}
   	}
   
   


</script>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


	<div class="modal-header">
		<button type="button" class="close close_dialog" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">�</span>
		</button>
		<h4 class="modal-title">
			<spring:message code="venta.ventareserva.busqueda.tabs.bono.editar.title" />					
		</h4>
	</div>
	<div class="modal-body">
		<form id="form_editar_uso" class="form-horizontal form-label-left">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
					<label class="control-label col-md-5 col-sm-5 col-xs-12"><spring:message code="venta.ventareserva.busqueda.tabs.bono.editar.tornos.usos" /></label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input name="usos" id="usos" type="text" class="form-control"  value="${uso}">
					</div>
				</div>						
		</div>
		
		</form>
	<div class="modal-footer">
			<button type="button" id="guardar_uso" class="btn aceptar_activar_dialog"
				data-dismiss="modal">
				<spring:message code="common.button.accept" />
			</button>
			<button type="button" class="btn btn-success close_dialog"
				data-dismiss="modal">
				<spring:message code="common.button.cancel" />
			</button>
		</div>

	</div>
	
	<script>
	
	//***********************************************************
	$("#guardar_uso").on("click", function(e) {
		var usos = $("#usos").val();
		
		dttornos
		  .rows({ selected: true })
		  .every(function (rowIdx, tableLoop, rowLoop) {
			  dttornos.cell(rowIdx,4).data(usos);					  
		  })
		  .draw();
	})

	
	
	
	
	
	</script>

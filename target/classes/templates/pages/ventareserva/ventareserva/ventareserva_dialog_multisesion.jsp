<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${disponibles_datos_sesiones}" var="disponibles_datos_sesiones_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.disponibles.title" />
	</h4>
</div>
<c:set var="count" value="0" />
<div class="modal-body">

	<form id="form_establecer_disponibles" class="form-horizontal form-label-left">
	
		<div class="form-group">
		
			<x:forEach select = "$disponibles_datos_sesiones_xml/ArrayList/NombreProductoTipoProducto" var = "producto" varStatus="count1">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<x:out select = "$producto/nombreProducto" />
				</div>
				
				<x:forEach select = "$producto/listTiposProductoSesiones/ArrayList" var = "item"  varStatus="count2">
					<c:set var="count" value="${count+1}" />

					<div id="tabla-disponibles-${count}" class="col-md-12 col-sm-12 col-xs-12 disponibles">
					
						<div class="col-md-10 col-sm-10 col-xs-12">
		
							<div class="form-group encabezado-disponibilidad">
								<label class="control-label col-md-8 col-sm-8 col-xs-12 label-disponibilidad">Disponibilidad <span class="nombre-producto"><x:out select = "$item/Tipoproducto/nombre" /></span></label>
								<label class="control-label col-md-1 col-sm-1 col-xs-4""><spring:message code="venta.ventareserva.dialog.disponibles.field.fecha" /></label>
								<div class="col-md-3 col-sm-3 col-xs-8" id="div-fecha-disponibles-${count}">
				                      <div class="input-prepend input-group">
				                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
				                        <input type="text" name="fecha_disponibles-${count}" id="fecha_disponibles-${count}" class="form-control" readonly value="<x:out select = "$item/String" />"/>
				                      </div>
								</div>
							</div>
						</div>
					
						<div class="btn-group pull-right btn-datatable">
							<a type="button" class="btn btn-info" id="button_disponibles_lock-${count}">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.bloquear" />"> <span class="fa fa-lock"></span>
								</span>
							</a>
							<a type="button" class="btn btn-info" id="button_disponibles_unlock-${count}">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.desbloquear" />"> <span class="fa fa-unlock"></span>
								</span>
							</a>
						</div>
					
						<div class="col-md-12 col-sm-12 col-xs-12">
							<table id="datatable-list-disponibles-${count}" class="table table-striped table-bordered dt-responsive sesiones" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th></th>
										<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.idsesion" /></th>
										<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.fecha" /></th>
										<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.hora" /></th>
										<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.overbooking" /></th>
										<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.contenido" /></th>
										<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.idzona" /></th>
										<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.zona" /></th>
										<th class="disponibles"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.disponibles" /></th>
										<th class="bloqueadas"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.bloqueadas" /></th>
										<th class="reservadas"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.reservadas" /></th>
										<th class="vendidas"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.vendidas" /></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
							<span>&nbsp;</span>
						</div>
					</div>
				
				</x:forEach>
			</x:forEach>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_disponibles_button" type="button" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
			
	</form>
	
</div>

<script>
hideSpinner("#button_add_${buttonAdd}");
hideSpinner("#button_${buttonAdd}_sesiones");

var ntablas = ${count};


var json_multisesion = ${disponibles_datos_sesiones_json};



var json_sesiones= [ <c:out value="${disponibles_datos_sesiones_json}"  escapeXml="false" /> ];
var fecha_seleccionada="${fecha}";

function check_selected(tabla) {
	sesiones= tabla.rows().data();
	
	$.each(sesiones, function(key_sesiones,fila_sesiones) {
		if (${list}.rows({selected: true}).data().length>0) {
			var item= ${list}.rows({selected: true}).data()[0][11];
			if (item.length>0) {
				$.each(item, function(key_seleccionadas,fila_seleccionadas) {
					if (fila_sesiones[1]==fila_seleccionadas[1] && fila_sesiones[6]==fila_seleccionadas[6]) tabla.row(key_sesiones).select().draw();
				});
			}
		}
			
	});
}

function get_sesion(idtipoproducto,idsesion,idzona,sesionesdata) {
	var result= null;
	if (sesionesdata==null) sesionesdata= json_sesiones[0].ArrayList;
	if (sesionesdata!="") {
		var item= sesionesdata.ArrayList;
		if (typeof item!="undefined")
			if (item.length>0)
			    $.each(item, function(key, val){
			    	if (val.Tipoproducto.idtipoproducto==idtipoproducto) {
			    		if (val.sesiones!="") {
				   			var sesiones= val.sesiones.Sesion;
				   			if (sesiones.length>0) 
				   			    $.each(sesiones, function(key, val){
				   			    	//if (typeof val.Sesion!="undefined")
					   			    	//if (val.Sesion.idsesion==idsesion) result= val.Sesion
					   			    	if (val.idsesion==idsesion && val.zonasesions.Zonasesion.zona.idzona==idzona) result= val
				   			    });
			   			    else if (sesiones.idsesion==idsesion && sesiones.zonasesions.Zonasesion.zona.idzona==idzona) result= sesiones;
				    	}
			    	}
			    })
			else if (item.Tipoproducto.idtipoproducto==idtipoproducto) {
				if (item.sesiones!="") {
		   			var sesion= item.sesiones.Sesion;
		   			if (sesion.length>0) 
		   			    $.each(sesion, function(key, val){
		   			    	if (val.idsesion==idsesion && val.zonasesions.Zonasesion.zona.idzona==idzona) result= val
		   			    });
	   			    else if (sesion.idsesion==idsesion && sesion.zonasesions.Zonasesion.zona.idzona==idzona) result= sesion;
				}
			}
	}
	return result;
}

<c:set var="count" value="0" />


var productos = json_multisesion.ArrayList.NombreProductoTipoProducto;


var nTabla = 0;





	


<x:forEach select = "$disponibles_datos_sesiones_xml/ArrayList/NombreProductoTipoProducto/listTiposProductoSesiones/ArrayList" var = "item">
	<c:set var="count" value="${count+1}" />
	
	$('input[name="fecha_disponibles-${count}"]').daterangepicker({
	    singleDatePicker: true,
	    showDropdowns: true,
		minDate: moment(),
	  	locale: $daterangepicker_sp
	});

	var dt_listdisponibles_${count}=$('#datatable-list-disponibles-${count}').DataTable( {
		language: dataTableLanguage,
		info: false,
		searching: false,
		scrollY: "200px",
		scrollCollapse: true,
		paging: false,
	    select: { style: 'single' },
		columnDefs: [
            { "targets": 0, "visible": false },
            { "targets": 1, "visible": false },
            { "targets": 2, "visible": false },
            { "targets": 6, "visible": false },
            { "targets": 12, "visible": false }
	    ],
	    drawCallback: function( settings ) { 
	    	activateTooltipsInTable('datatable-list-disponibles-${count}');
	   	}
	} );
	
	function ajustar_cabeceras_datatable_${count}()
	{
		$('#datatable-list-disponibles-${count}').DataTable().columns.adjust().draw();
	}
	
	insertSmallSpinner("#datatable-list-disponibles-${count}_processing");
	
	
	<x:forEach select = "$item/Sesion" var="sesion">
		
	/*	dt_listdisponibles_${count}.row.add([
	         get_sesion("<x:out select = "$item/Tipoproducto/idtipoproducto" />","<x:out select = "$sesion/idsesion" />","<x:out select = "$sesion/zonasesions/Zonasesion/zona/idzona" />",null), 
	         "<x:out select = "$sesion/idsesion" />", 
	         "<x:out select = "$sesion/fecha" />",         
	         "<x:out select = "$sesion/horainicio" />", 
	         "<x:out select = "$sesion/overbookingventa" />", 
	         "<span style='white-space: nowrap' title='<x:out select = "$sesion/contenido/descripcion" />'> <x:out select = "$sesion/contenido/nombre" /> </span>",
	         "<x:out select = "$sesion/zonasesions/Zonasesion/zona/idzona" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/zona/nombre" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/numlibres" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/numbloqueadas" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/numreservadas" />", 
	         "<x:out select = "$sesion/zonasesions/Zonasesion/numvendidas" />",
	         "<span style='white-space: nowrap' title='<x:out select = "$sesion/contenido/descripcion" />'> <x:out select = "$sesion/horainicio" /> <x:out select = "$sesion/contenido/nombre" /> </span>",
	    ]);*/
	</x:forEach>
	dt_listdisponibles_${count}.draw();
	setTimeout(ajustar_cabeceras_datatable_${count}, 1000);
	check_selected(dt_listdisponibles_${count});

	//********************************************************************************
		
	$('input[name="fecha_disponibles-${count}"]').on("change", function() { 
		//dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.processing(true);
		showFieldSpinner("#div-fecha-disponibles-${count}");
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/venta/ventareserva/list_tipos_producto.do'/>",
			timeout : 100000,
			data: {
	  			   idtipoproducto: "<x:out select = "$item/Tipoproducto/idtipoproducto" />",
		    	   fecha: $('input[name="fecha_disponibles-${count}"]').val()
				  }, 
			success : function(data) {

				if (data.ArrayList!="") {
					dt_listdisponibles_${count}.rows().remove().draw();
					var item= data.ArrayList.sesiones.Sesion;
					if (typeof item!="undefined")
						if (item.length>0) {
						    $.each(item, function(key, sesion){
								dt_listdisponibles_${count}.row.add([
									 sesion,								                                                                                   
							         sesion.idsesion, 
							         sesion.fecha, 
							         sesion.horainicio, 
							         sesion.overbookingventa, 
							         "<span title='"+sesion.contenido.descripcion+"'> "+sesion.contenido.nombre+" </span>", 
							         sesion.zonasesions.Zonasesion.zona.idzona, 
							         sesion.zonasesions.Zonasesion.zona.nombre, 
							         sesion.zonasesions.Zonasesion.numlibres, 
							         sesion.zonasesions.Zonasesion.numbloqueadas, 
							         sesion.zonasesions.Zonasesion.numreservadas, 
							         sesion.zonasesions.Zonasesion.numvendidas,
							         "<span style='white-space: nowrap' title='"+sesion.contenido.descripcion+"'> "+sesion.horainicio+" "+sesion.contenido.nombre+" </span>"					         
							    ]);
						    });
						}
						else {
							dt_listdisponibles_${count}.row.add([
							     item,                                                                              			
						         item.idsesion, 
						         item.fecha, 
						         item.horainicio, 
						         item.overbookingventa, 
						         "<span title='"+item.contenido.descripcion+"'> "+item.contenido.nombre+" </span>", 
						         item.zonasesions.Zonasesion.zona.idzona, 
						         item.zonasesions.Zonasesion.zona.nombre, 
						         item.zonasesions.Zonasesion.numlibres, 
						         item.zonasesions.Zonasesion.numbloqueadas, 
						         item.zonasesions.Zonasesion.numreservadas, 
						         item.zonasesions.Zonasesion.numvendidas,
						         "<span style='white-space: nowrap' title='"+item.contenido.descripcion+"'> "+item.horainicio+" "+item.contenido.nombre+" </span>"					         
						    ]);
						}
						
					//dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.processing(false);
					hideSpinner("#div-fecha-disponibles-${count}");
				    dt_listdisponibles_${count}.draw();
				}
			},
			error : function(exception) {
				//dt_listdisponibles_<x:out select = "$item/Tipoproducto/idtipoproducto" />.processing(false);
				hideSpinner("#div-fecha-disponibles-<x:out select = "$item/Tipoproducto/idtipoproducto" />");
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",
					  buttons: { sticker: false }				  
				   });		
			}
		});	
	});

	//********************************************************************************
	$("#button_disponibles_lock-${count}").on("click", function(e) {
		var data = dt_listdisponibles_${count}.rows( { selected: true } ).data();
		if (data.length<=0) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="administracion.programacion.programacion.list.alert.localidades.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}	

		var button_bloqueos= "button_disponibles_lock-<x:out select = "$item/Tipoproducto/idtipoproducto" />",
			lista_bloqueos="dt_listdisponibles_${count}";
		
		showButtonSpinner("#"+button_bloqueos);
		$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/show_new_bloqueo.do'/>?id="+data[0][1]+"&button="+button_bloqueos+"&lista="+lista_bloqueos+"&zona="+data[0][6]+"&es_programacion=false&idx_libres=8&idx_bloqueadas=9", function() {
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "sm");
		});
	});
	
	//********************************************************************************
	$("#button_disponibles_unlock-${count}").on("click", function(e) { 
		
		var data = dt_listdisponibles_${count}.rows( { selected: true } ).data();
		
		if (data.length<=0) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="administracion.programacion.programacion.list.alert.localidades.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}	
		
		var button_bloqueos= "button_disponibles_unlock-<x:out select = "$item/Tipoproducto/idtipoproducto" />",
		lista_bloqueos="dt_listdisponibles_${count}";
		
		showButtonSpinner("#button_programacion_bloqueos");
		$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/show_bloqueos.do'/>?id="+data[0][1]+"&button="+button_bloqueos+"&lista="+lista_bloqueos+"&zona="+data[0][6]+"&es_programacion=false&idx_libres=8&idx_bloqueadas=9", function() {
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "md");
		});

	});	

</x:forEach>

function obtenerIdTarifa(tarifas,idperfilvisitante) {
	for (var i=0; i<tarifas.length; i++) 
		if (tarifas[i].idperfilvisitante==idperfilvisitante) return(tarifas[i].tarifa.idtarifa); 
	return("");
}

function construir_xml_sesiones(sesiones_sel) {
	var xml_sesiones= "<sesiones>";
	for (var i=0; i<sesiones_sel.length; i++) { 
		xml_sesiones+="<Sesion>";
		xml_sesiones+=json2xml(sesiones_sel[i][0],"");
		xml_sesiones+="</Sesion>";
	}
	xml_sesiones+="</sesiones>";
	return(xml_sesiones);
}


$("#save_disponibles_button").on("click", function(e) {
	
	var sesiones= $(".sesiones").DataTable().rows( { selected: true } ).data();
	
	var filas = tabla.rows().data();
	
	if (sesiones.length < nTabla) {
	new PNotify({
	      title: '<spring:message code="common.dialog.text.error" />',
	      text: '<spring:message code="venta.ventareserva.list.alert.productos.seleccion" />',
		  type: "error",
		  delay: 5000,
		  buttons: { sticker: false }
	   });
	return;
	}
	
	var k = 0;	
	
	for(i=0;i<filas.length;i++)
		{
		var linea = filas[i];
		var lineaDetalle = linea[0].Lineadetalle;	
		var sesionesLinea = lineaDetalle.lineadetallezonasesions.Lineadetallezonasesion;
		if (sesionesLinea.length>0)
			{
			var fechas= "", contenidos= "", disponibles= "",
			retorno= false;
			for(j=0;j<sesionesLinea.length;j++)
				{
				if (retorno) {
					fechas+="</br>";
					contenidos+= "</br>";
					disponibles+= "</br>";
				}
				fechas+= sesiones[k][2].split("-")[0],
				contenidos+= sesiones[k][12];
				disponibles+= sesiones[k][8];
				sesionesLinea[j].zonasesion.sesion.fecha = sesiones[k][2];
				sesionesLinea[j].zonasesion.sesion.horainicio = sesiones[k][12];
				sesionesLinea[j].zonasesion.sesion.idsesion=sesiones[k][1];
				sesionesLinea[j].zonasesion.numlibres = sesiones[k][8];				
				linea[3] = fechas;
				linea[4] = contenidos;
				linea[5] = disponibles;
				retorno= true;
				k++;
				}
			}
		else
			{
			
			sesionesLinea.zonasesion.sesion.fecha = sesiones[k][2];
			sesionesLinea.zonasesion.sesion.horainicio = sesiones[k][12];
			sesionesLinea.zonasesion.sesion.idsesion=sesiones[k][1];
			sesionesLinea.zonasesion.numlibres = sesiones[k][8];
			linea[3] = sesiones[k][2].split("-")[0];
			linea[4] = sesiones[k][12];
			linea[5] = sesiones[k][8];
			k++;			
			}
		}
	tabla.rows().invalidate().draw();	
	$("#modal-dialog-form").modal('hide');
});	


cargarContenidos();

function cargarZonaSesionesContenido(table,sesion)
{
	var zonaSesions = sesion.zonasesions.Zonasesion;

	
	if(zonaSesions.length>0)
		{
		$.each(zonaSesions, function(key, zonaSesion)
			{
			table.row.add([
							 sesion,								                                                                                   
					         sesion.idsesion, 
					         sesion.fecha, 
					         sesion.horainicio, 
					         sesion.overbookingventa, 
					         "<span title='"+sesion.contenido.descripcion+"'> "+sesion.contenido.nombre+" </span>", 
					         zonaSesion.zona.idzona, 
					         zonaSesion.zona.nombre, 
					         zonaSesion.numlibres, 
					         zonaSesion.numbloqueadas, 
					         zonaSesion.numreservadas, 
					         zonaSesion.numvendidas,
					         "<span style='white-space: nowrap' title='"+sesion.contenido.descripcion+"'> "+sesion.horainicio+" "+sesion.contenido.nombre+" </span>"					         
					    ]);
			})
		}
	else
		{
		table.row.add([
						 sesion,								                                                                                   
				         sesion.idsesion, 
				         sesion.fecha, 
				         sesion.horainicio, 
				         sesion.overbookingventa, 
				         "<span title='"+sesion.contenido.descripcion+"'> "+sesion.contenido.nombre+" </span>", 
				         sesion.zonasesions.Zonasesion.zona.idzona, 
				         sesion.zonasesions.Zonasesion.zona.nombre, 
				         sesion.zonasesions.Zonasesion.numlibres, 
				         sesion.zonasesions.Zonasesion.numbloqueadas, 
				         sesion.zonasesions.Zonasesion.numreservadas, 
				         sesion.zonasesions.Zonasesion.numvendidas,
				         "<span style='white-space: nowrap' title='"+sesion.contenido.descripcion+"'> "+sesion.horainicio+" "+sesion.contenido.nombre+" </span>"					         
				    ]);
		}
}

function cargarSesionesContenido(contenido)
{
	nTabla++;
	var sesiones = contenido.Sesion;
	
	var nombreTabla = '#datatable-list-disponibles-'+nTabla;
	var table = $(nombreTabla).DataTable();
	
	
	if(sesiones.length>0)
		{
		$.each(sesiones, function(key, sesion)
			{
			cargarZonaSesionesContenido(table,sesion)			
			});
		}
	else
		{
		sesion = sesiones;
		cargarZonaSesionesContenido(table,sesion);		
		}
	table.draw();
	
}

function cargarSesionesProductos(producto)
{
	var contenidos = producto.listTiposProductoSesiones.ArrayList;
	if(contenidos.length>0)
		{
		$.each(contenidos, function(key, contenido){			
			cargarSesionesContenido(contenido);
			});
		}
	else
		{
		contenido = contenidos;
		cargarSesionesContenido(contenido);	
		}
}

function cargarContenidos()
{
	if(productos.length>0)
	{
		$.each(productos, function(key, producto){
			cargarSesionesProductos(producto);
			});
	}
	else
		{
		
		var producto = productos;
		cargarSesionesProductos(producto);		
		}
}


</script>


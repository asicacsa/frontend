<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${disponibles_datos_sesiones}" var="disponibles_datos_sesiones_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.disponibles.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_establecer_disponibles" class="form-horizontal form-label-left">
	    <input type="hidden" name="xmlCanje" id="xmlCanje"/>
		<div class="form-group">
		
			<x:forEach select = "$disponibles_datos_sesiones_xml/ArrayList/ArrayList" var = "item">
				<x:if select="$item/Tipoproducto/idtipoproducto='376'">
							
				
				<div id="tabla-disponibles" class="col-md-12 col-sm-12 col-xs-12 disponibles">
				
					<div class="col-md-10 col-sm-10 col-xs-12">
	
						<div class="form-group encabezado-disponibilidad">
							<label class="control-label col-md-8 col-sm-8 col-xs-12 label-disponibilidad">Disponibilidad <span class="nombre-producto"><x:out select = "$item/Tipoproducto/nombre" /></span></label>
							<label class="control-label col-md-1 col-sm-1 col-xs-4""><spring:message code="venta.ventareserva.dialog.disponibles.field.fecha" /></label>
							<div class="col-md-3 col-sm-3 col-xs-8" id="div-fecha-disponibles">
			                      <div class="input-prepend input-group">
			                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
			                        <input type="text" name="fecha_disponibles" id="fecha_disponibles" class="form-control" readonly value="<x:out select = "$item/String" />"/>
			                      </div>
							</div>
						</div>
					</div>
				
					<div class="btn-group pull-right btn-datatable">
						<a type="button" class="btn btn-info" id="button_disponibles_lock">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.bloquear" />"> <span class="fa fa-lock"></span>
							</span>
						</a>
						<a type="button" class="btn btn-info" id="button_disponibles_unlock">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="administracion.programacion.programacion.list.button.desbloquear" />"> <span class="fa fa-unlock"></span>
							</span>
						</a>
					</div>
				
					<div class="col-md-12 col-sm-12 col-xs-12">
						<table id="datatable-list-disponibles" class="table table-striped table-bordered dt-responsive sesiones" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.idsesion" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.fecha" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.hora" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.overbooking" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.contenido" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.idzona" /></th>
									<th><spring:message code="venta.ventareserva.dialog.disponibles.list.header.zona" /></th>
									<th class="disponibles"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.disponibles" /></th>
									<th class="bloqueadas"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.bloqueadas" /></th>
									<th class="reservadas"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.reservadas" /></th>
									<th class="vendidas"><spring:message code="venta.ventareserva.dialog.disponibles.list.header.vendidas" /></th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
						<span>&nbsp;</span>
					</div>
				</div>
				</x:if>
			</x:forEach>
		</div>
		
		<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3">Observaciones</label>
				<div class="col-md-9 col-sm-9 col-xs-12">
					<textarea name="observacionesCanje" id="observacionesCanje"  class="form-control" ></textarea>
				</div>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_disponibles_button" type="button" class="btn btn-primary save_dialog">
					<spring:message code="venta.ventareserva.dialog.disponibles.list.canjear.bono" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
			
	</form>
	
</div>

<script>

hideSpinner("#tab_bono_canjear");




var json_sesiones= [ <c:out value="${disponibles_datos_sesiones_json}"  escapeXml="false" /> ];
var fecha_seleccionada="${fecha}";

function check_selected(tabla) {
	
	
	sesiones= tabla.rows().data();

	
	$.each(sesiones, function(key_sesiones,fila_sesiones) {
		//if (${list}.rows({selected: true}).data().length>0) {
			if (dt_listbono.rows({selected: true}).data().length>0) {
			var item= dt_listbono.rows({selected: true}).data()[0][11];
			if (item.length>0) {
				$.each(item, function(key_seleccionadas,fila_seleccionadas) {
					if (fila_sesiones[1]==fila_seleccionadas[1] && fila_sesiones[6]==fila_seleccionadas[6]) tabla.row(key_sesiones).select().draw();
				});
			}
		}
			
	});
}

function get_sesion(idtipoproducto,idsesion,idzona,sesionesdata) {
	var result= null;
	if (sesionesdata==null) sesionesdata= json_sesiones[0].ArrayList;
	if (sesionesdata!="") {
		var item= sesionesdata.ArrayList;
		if (typeof item!="undefined")
			if (item.length>0)
			    $.each(item, function(key, val){
			    	if (val.Tipoproducto.idtipoproducto==idtipoproducto) {
			    		if (val.sesiones!="") {
				   			var sesiones= val.sesiones.Sesion;
				   			if (sesiones.length>0) 
				   			    $.each(sesiones, function(key, val){
				   			    	//if (typeof val.Sesion!="undefined")
					   			    	//if (val.Sesion.idsesion==idsesion) result= val.Sesion
					   			    	if (val.idsesion==idsesion && val.zonasesions.Zonasesion.zona.idzona==idzona) result= val
				   			    });
			   			    else if (sesiones.idsesion==idsesion && sesiones.zonasesions.Zonasesion.zona.idzona==idzona) result= sesiones;
				    	}
			    	}
			    })
			else if (item.Tipoproducto.idtipoproducto==idtipoproducto) {
				if (item.sesiones!="") {
		   			var sesion= item.sesiones.Sesion;
		   			if (sesion.length>0) 
		   			    $.each(sesion, function(key, val){
		   			    	if (val.idsesion==idsesion && val.zonasesions.Zonasesion.zona.idzona==idzona) result= val
		   			    });
	   			    else if (sesion.idsesion==idsesion && sesion.zonasesions.Zonasesion.zona.idzona==idzona) result= sesion;
				}
			}
	}
	return result;
}

<x:forEach select = "$disponibles_datos_sesiones_xml/ArrayList/ArrayList" var = "item">
var tipo = <x:out select = "$item/Tipoproducto/idtipoproducto" />;

if(tipo==376)
{
	
$('input[name="fecha_disponibles"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
	minDate: moment(),
  	locale: $daterangepicker_sp
});

var dt_listdisponibles_376=$('#datatable-list-disponibles').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	scrollY: "200px",
	scrollCollapse: true,
	paging: false,
    select: { style: 'single' },
	columnDefs: [
        { "targets": 0, "visible": false },
        { "targets": 1, "visible": false },
        { "targets": 2, "visible": false },
        { "targets": 6, "visible": false }
    ],
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('datatable-list-disponibles');
   	}
   
} );

insertSmallSpinner("#datatable-list-disponibles_processing");

<x:forEach select = "$item/sesiones/Sesion" var="sesion">
	dt_listdisponibles_376.row.add([
         get_sesion("<x:out select = "$item/Tipoproducto/idtipoproducto" />","<x:out select = "$sesion/idsesion" />","<x:out select = "$sesion/zonasesions/Zonasesion/zona/idzona" />",null), 
         "<x:out select = "$sesion/idsesion" />", 
         "<x:out select = "$sesion/fecha" />",         
         "<x:out select = "$sesion/horainicio" />", 
         "<x:out select = "$sesion/overbookingventa" />", 
         "<span title='<x:out select = "$sesion/contenido/descripcion" />'> <x:out select = "$sesion/contenido/nombre" /> </span>",
         "<x:out select = "$sesion/zonasesions/Zonasesion/zona/idzona" />", 
         "<x:out select = "$sesion/zonasesions/Zonasesion/zona/nombre" />", 
         "<x:out select = "$sesion/zonasesions/Zonasesion/numlibres" />", 
         "<x:out select = "$sesion/zonasesions/Zonasesion/numbloqueadas" />", 
         "<x:out select = "$sesion/zonasesions/Zonasesion/numreservadas" />", 
         "<x:out select = "$sesion/zonasesions/Zonasesion/numvendidas" />" 
    ]);
</x:forEach>
dt_listdisponibles_376.draw();
setTimeout(ajustar_cabeceras_datatable, 500);

}
</x:forEach>

function ajustar_cabeceras_datatable()
{
	dt_listdisponibles_376.columns.adjust().draw();
}

//********************************************************************************
$("#button_disponibles").on("click", function(e) {
	var data = dt_listdisponibles_376.rows( { selected: true } ).data();

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.programacion.programacion.list.alert.localidades.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	

	var button_bloqueos= "button_disponibles_lock",
		lista_bloqueos="dt_listdisponibles_376";
	
	
	
	showButtonSpinner("#"+button_bloqueos);
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/show_new_bloqueo.do'/>?id="+data[0][0].idsesion+"&button="+button_bloqueos+"&lista="+lista_bloqueos+"&zona="+data[0][0].zonasesions.Zonasesion.zona.idzona+"&es_programacion=false&idx_libres=8&idx_bloqueadas=9", function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "sm");
	});
});

//********************************************************************************
$("#button_disponibles_unlock").on("click", function(e) { 
	
	var data = dt_listdisponibles_376.rows( { selected: true } ).data();
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.programacion.programacion.list.alert.localidades.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	var button_bloqueos= "button_disponibles_unlock",
	lista_bloqueos="dt_listdisponibles_376";
	
	showButtonSpinner("#button_programacion_bloqueos");
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/show_bloqueos.do'/>?id="+data[0][0].idsesion+"&button="+button_bloqueos+"&lista="+lista_bloqueos+"&zona="+data[0][0].zonasesions.Zonasesion.zona.idzona+"&es_programacion=false&idx_libres=8&idx_bloqueadas=9", function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "md");
	});

});


$("#save_disponibles_button").on("click", function(e) {
	
    if(dt_listdisponibles_376==undefined)
    	{
    	new PNotify({
		      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
		      text: '<spring:message code="venta.ventareserva.dialog.disponibles.canje.alert.hemisferic" />',
			  type: "error",		     
			  delay: 5000,
			  buttons: { sticker: false }
		   });	
    	return;
    	
    	}
    
    if(dt_listdisponibles_376.rows(".selected").data().length==0)
    	{
	    	new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: '<spring:message code="venta.ventareserva.dialog.disponibles.canje.alert.no.session" />',
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });	
	  		return;
    	}
    	

	var observaciones = $("#observacionesCanje").val();
	var xmlCanje="<list>";
	var bonos = dt_listbono.rows( { selected: true } ).data();
	
	for (var i=0; i<bonos.length; i++) 
	{ 
		bono = bonos[i];	
		idBono = bono.idbono;
		idLineaDetalle = bono.lineacanje.idlineadetalle;
		
		xmlCanje+="<Canjebono><observaciones>"+observaciones+"</observaciones>";
		xmlCanje+="<bono><idbono>"+idBono+"</idbono></bono>";
		xmlCanje+="<lineadetallezonasesion>";
		xmlCanje+="<lineadetalle><idlineadetalle>"+idLineaDetalle+"</idlineadetalle><canjebono/></lineadetalle>";
		var zonaSesion = dt_listdisponibles_376.rows(".selected").data()[0][0];
		xmlCanje+="<zonasesion>";
		xmlCanje+="<idzonasesion>"+zonaSesion.zonasesions.Zonasesion.idzonasesion+"</idzonasesion>";
		xmlCanje+="<numvendidas>"+zonaSesion.zonasesions.Zonasesion.numvendidas+"</numvendidas>";
		xmlCanje+="<numreservadas>"+zonaSesion.zonasesions.Zonasesion.numreservadas+"</numreservadas>";
		xmlCanje+="<numlibres>"+zonaSesion.zonasesions.Zonasesion.numlibres+"</numlibres>";
		xmlCanje+="<numbloqueadas>"+zonaSesion.zonasesions.Zonasesion.numbloqueadas+"</numbloqueadas>";
		xmlCanje+="<zona>"+ json2xml(zonaSesion.zonasesions.Zonasesion.zona,"")+"</zona>";
		xmlCanje+="<sesion>";
		xmlCanje+="<idsesion>"+zonaSesion.idsesion+"</idsesion>";
		xmlCanje+="<horainicio>"+zonaSesion.horainicio+"</horainicio>";
		xmlCanje+="<fecha>"+zonaSesion.fecha+"</fecha>";
		xmlCanje+="<contenido>"+json2xml(zonaSesion.contenido,"")+"</contenido>";
		xmlCanje+="<overbookingventa>"+zonaSesion.overbookingventa+"</overbookingventa>";
		xmlCanje+="<bloqueado>"+zonaSesion.bloqueado+"</bloqueado>";
		xmlCanje+="</sesion>";
		xmlCanje+="</zonasesion>";	
		xmlCanje+="</lineadetallezonasesion>";
		xmlCanje+="</Canjebono>";
	}
	
	xmlCanje+="</list>";
	
	$("#xmlCanje").val(xmlCanje);
	
	var data = $("#form_establecer_disponibles").serializeObject();

	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/busqueda/bono/canjear_bono.do'/>",
		timeout : 100000,
		data: data, 
		success : function(data) {
			$("#modal-dialog-form").modal('hide');
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		   						
			dt_listbono.ajax.reload();
			
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
});	





$('input[name="fecha_disponibles"]').on("change", function() { 
	showFieldSpinner("#div-fecha-disponibles");
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/list_tipos_producto.do'/>",
		timeout : 100000,
		data: {
  			   idtipoproducto: "376",
	    	   fecha: $('input[name="fecha_disponibles"]').val()
			  }, 
		success : function(data) {
			if (data.ArrayList!="") {
				dt_listdisponibles_376.rows().remove().draw();
				var item= data.ArrayList.sesiones.Sesion;
				if (typeof item!="undefined")
					if (item.length>0)
					    $.each(item, function(key, sesion){
							dt_listdisponibles_376.row.add([
								 sesion,								                                                                                   
						         sesion.idsesion, 
						         sesion.fecha, 
						         sesion.horainicio, 
						         sesion.overbookingventa, 
						         sesion.contenido.nombre, 
						         sesion.zonasesions.Zonasesion.zona.idzona, 
						         sesion.zonasesions.Zonasesion.zona.nombre, 
						         sesion.zonasesions.Zonasesion.numlibres, 
						         sesion.zonasesions.Zonasesion.numbloqueadas, 
						         sesion.zonasesions.Zonasesion.numreservadas, 
						         sesion.zonasesions.Zonasesion.numvendidas 
						    ]);
					    });
					else
						dt_listdisponibles_376.row.add([
						     item,                                                                              			
					         item.idsesion, 
					         item.fecha, 
					         item.horainicio, 
					         item.overbookingventa, 
					         item.contenido.nombre, 
					         item.zonasesions.Zonasesion.zona.idzona, 
					         item.zonasesions.Zonasesion.zona.nombre, 
					         item.zonasesions.Zonasesion.numlibres, 
					         item.zonasesions.Zonasesion.numbloqueadas, 
					         item.zonasesions.Zonasesion.numreservadas, 
					         item.zonasesions.Zonasesion.numvendidas 
					    ]);
					
				
				hideSpinner("#div-fecha-disponibles");
			    dt_listdisponibles_376.draw();
			}
		},
		error : function(exception) {
			
			hideSpinner("#div-fecha-disponibles");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});	
});


//********************************************************************************
$("#button_disponibles_lock").on("click", function(e) {
	var data = dt_listdisponibles_376.rows( { selected: true } ).data();

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="administracion.programacion.programacion.list.alert.localidades.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	

	var button_bloqueos= "button_disponibles_lock",
		lista_bloqueos="dt_listdisponibles_376";
	
	showButtonSpinner("#"+button_bloqueos);
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/admon/programacion/show_new_bloqueo.do'/>?id="+data[0][0].idsesion+"&button="+button_bloqueos+"&lista="+lista_bloqueos+"&zona="+data[0][0].zonasesions.Zonasesion.zona.idzona+"&es_programacion=false&idx_libres=8&idx_bloqueadas=9", function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "sm");
	});
});



</script>


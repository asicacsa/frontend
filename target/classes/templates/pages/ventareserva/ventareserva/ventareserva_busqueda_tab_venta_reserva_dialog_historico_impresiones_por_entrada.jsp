<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${historico_impresiones_entrada}" var="historico_impresiones_entrada_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico_entradas.title" /> ${id}
	</h4>
</div>

<div class="modal-body">
	<table id="historico_entrada_venta_table" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
			  <th id="fecha_entrada"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico_entradas.field.fecha"/></th>
			  <th id="nombre_entrada"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico_entradas.field.nombre"/></th>
			  <th id="reimpresion_entrada"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico_entradas.field.reimpresion" /></th>
			</tr>
		</thead>
		<tbody>				
			<x:forEach select="$historico_impresiones_entrada_xml/ArrayList/Impresionentradas" var="item">
		   		<tr class="seleccionable">
					<td id="fecha_entrada">
						<c:set var="fecha"><x:out select="$item/fecha"/></c:set>
						<fmt:parseDate value="${fecha}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateada"/>
						<fmt:formatDate value="${fechaformateada}" pattern="dd/MM/yyyy" type="DATE"/>									
					</td>						
					<td id="nombre_entrada" ><x:out select="$item/usuario/nombrecompleto"/></td>
					<td id="reimpresion_entrada"><x:out select="$item/reimpresion" /></td>																	
				</tr>																
			</x:forEach>
		</tbody>				
	</table>			

	<div class="modal-footer">
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.accept"/>
		</button>
	</div>		

</div>

<script>
//*****************************************************
hideSpinner("#tab_entradas_venta_impresiones");
hideSpinner("#tab_entradas_reserva_impresiones");

//*****************************************************


var dt_historico_entradas_venta=$('#historico_entrada_venta_table').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	scrollCollapse: true,
	ordering:  false,
	paging: false,	
    select: { style: 'single' },
    columns: [
              {},
              {},             
              { className: "cell_centered",
                render: function ( data, type, row, meta ) {
                              if (data==1) return '<i class="fa fa-check" style="color: green;" aria-hidden="true"></i>'; 
                              else return '<i class="fa fa-close" style="color: red;" aria-hidden="true"></i>'
         }}
          
        ],

    drawCallback: function( settings ) { 
    	activateTooltipsInTable('historico_entrada_venta_table');
   	}
    
} );


</script>
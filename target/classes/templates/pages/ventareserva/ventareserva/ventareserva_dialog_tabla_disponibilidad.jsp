<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<div class="col-md-12 col-sm-12 col-xs-12" id="div-fecha-disponibilidad" />
			<label class="control-label col-md-3 col-sm-3 col-xs-4 titulo-disponibilidad"><spring:message code="venta.ventareserva.dialog.disponibilidad.title" /></label>
			<div class="col-md-2 col-sm-2 col-xs-8">
	            <div class="input-prepend input-group">
	              <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	              <input type="text" name="fecha_disponibilidad" id="fecha_disponibilidad" class="form-control" readonly value="${fecha}"/>
	            </div>
            </div>
		</div>
	</h4>
</div>

<div class="modal-body">

	<form id="form_mostrar_disponibilidad" class="form-horizontal form-label-left">

		<div id="disponibilidad" class="form-group disponibilidad">
	
		</div>
	
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.close" />
				</button>
			</div>
		</div>
		
	</form>
	
</div>

<script>

function mostrarRecintos(recinto) {
	var recinto_html= "";
	recinto_html+= "<div class='col-md-12 col-sm-12 col-xs-12'>";
	recinto_html+= "<div class='form-group recinto-disponibilidad'>";
	recinto_html+= "<span class='nombre-recinto'>"+recinto.nombre+"</span>";
	recinto_html+= "</div>";
	recinto_html+= "</div>";
	
	recinto_html+= "<div class='col-md-12 col-sm-12 col-xs-12'>";
	
	recinto_html+= "<table class='table table-striped table-bordered dt-responsive sesiones' cellspacing='0' width='100%'>";
	recinto_html+= "<thead>";
	recinto_html+= "<tr>";
	recinto_html+= "<th width='10%'><spring:message code='venta.ventareserva.dialog.disponibles.list.header.hora' /></th>";
	recinto_html+= "<th width='80%'><spring:message code='venta.ventareserva.dialog.disponibles.list.header.contenido' /></th>";
	recinto_html+= "<th width='10%'><spring:message code='venta.ventareserva.tabs.reserva_grupo.list.header.disp' /></th>";
	recinto_html+= "</tr>";
	recinto_html+= "</thead>";
	recinto_html+= "<tbody>";
	
	// Sesiones

	var sesiones= recinto.sesiones.sesion;
	if (typeof sesiones!="undefined" && sesiones.length>0) {
		sortjsonarray(sesiones,"hora","asc");
		for (var i=0; i<sesiones.length; i++) {
			recinto_html+= "<tr>";
			recinto_html+= "<td>"+sesiones[i].hora+"</td>";
			recinto_html+= "<td>"+sesiones[i].contenido.nombre+"</td>";
			recinto_html+= "<td style='text-align:center'>"+sesiones[i].numlibres+"</td>";
			recinto_html+= "</tr>";
		}
	}
	else {
		recinto_html+= "<tr>";
		recinto_html+= "<td colspan='3' style='text-align:center;'><spring:message code='venta.ventareserva.dialog.disponibilidad.no_sesiones' /></td>";
		recinto_html+= "</tr>";	
	}

	recinto_html+= "</tbody>";
	recinto_html+= "</table>";
	recinto_html+= "</div>";
	
	return(recinto_html);
};

function mostrarUnidades(unidad) {
	var unidad_html= "";
	unidad_html+= "<div class='unidadNegocio'>";
	unidad_html+= "<div class='col-md-12 col-sm-12 col-xs-12'>";
	unidad_html+= "<div class='form-group unidad-disponibilidad'>";
	unidad_html+= "<span class='nombre-unidad'>"+unidad.nombre+"</span>";
	unidad_html+= "</div>";
	unidad_html+= "</div>";
	
	// Recintos
	
	var recintos= unidad.recintos.Recinto;
	if (typeof recintos!="undefined" && recintos.length>0) {
		for (var i=0; i<recintos.length; i++) unidad_html+= mostrarRecintos(recintos[i])
	}
	else {
		unidad_html+= "<p><spring:message code='venta.ventareserva.dialog.disponibilidad.no_recintos' /></p>";
	}
	
	unidad_html+= "</div>";
	
	return(unidad_html);
}

function mostrarTabla(fecha) {
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/list_datos_disponibilidad.do'/>",
		timeout : 100000,
		data: {
			fecha: fecha
		},
		success : function(data) {
			var unidades= data.ArrayList.UnidadNegocio;
			var unidades_html= "";
			if (typeof unidades!="undefined" && unidades.length>0) {
				for (var i=0; i<unidades.length; i++) unidades_html+= mostrarUnidades(unidades[i])
			}
			document.getElementById('disponibilidad').innerHTML= unidades_html;
			hideSpinner("#modal-dialog-form .modal-content");
		},
		error : function(exception) {
			hideSpinner("#modal-dialog-form .modal-content");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});
	
}

hideSpinner("#button_${buttonAdd}");

$('input[name="fecha_disponibilidad"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
	minDate: moment(),
  	locale: $daterangepicker_sp
});

showSpinner("#modal-dialog-form .modal-content");
mostrarTabla(moment().format("DD/MM/YYYY"));

$('input[name="fecha_disponibilidad"]').on("change", function() {
	showSpinner("#modal-dialog-form .modal-content");
	mostrarTabla($("#fecha_disponibilidad").val());
});

</script>


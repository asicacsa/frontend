<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${historico_cambios_reserva}" var="historico_cambios_reserva_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico-cambios-reserva.title" /> <x:out select="$historico_cambios_reserva_xml/Reserva/idreserva"/>
	</h4>
</div>
<div class="modal-body">
	<table id="historico_cambios_venta_table" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
			  <th id="fechaReserva"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico-cambiosventa.field.fecha"/></th>
			  <th id="usuarioReserva"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico-cambiosventa.field.usuario"/></th>
			  <th id="motivoModificacion"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico-cambiosventa.field.motivo" /></th>
			  <th id="observaciones"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico-cambiosventa.field.observaciones" /></th>							  						 
			</tr>
		</thead>
		<tbody>				
			<x:forEach select="$historico_cambios_reserva_xml/Reserva/modificacions/Modificacion" var="item">
		   		<tr class="seleccionable">
					<td id="fechaReserva">
						<c:set var="fecha"><x:out select="$item/fecha"/></c:set>
						<fmt:parseDate value="${fecha}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateada"/>
						<fmt:formatDate value="${fechaformateada}" pattern="dd/MM/yyyy" type="DATE"/>									
					</td>						
					<td id="usuarioReserva" ><x:out select="$item/usuario/login"/></td>
					<td id="motivoModificacion" ><x:out select="$item/motivomodificacion" /></td> 
					<td id="observaciones"><x:out select="$item/observaciones" /></td>																	
				</tr>																
			</x:forEach>
		</tbody>				
	</table>			

	<div class="modal-footer">
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.close"/>
		</button>
	</div>		

</div>

<script>
hideSpinner("#tab_busqueda_historico_cambios_reserva");
</script>
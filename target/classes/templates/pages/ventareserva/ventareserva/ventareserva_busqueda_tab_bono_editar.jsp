<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editarBono}" var="editarBono_xml" />

	<div class="modal-header">
		<button type="button" class="close close_dialog" data-dismiss="modal"
			aria-label="Close">
			<span aria-hidden="true">�</span>
		</button>
		<h4 class="modal-title">
			<spring:message code="venta.ventareserva.busqueda.tabs.bono.editar.title" />					
		</h4>
	</div>
	<div class="modal-body">
		<form id="form_edit_bono" class="form-horizontal form-label-left">
				<input type="hidden" name="idRecinto" id="idRecinto" value="<x:out select = "$editarBono_xml/Bono/entradatornoses/Entradatornos/recinto/idrecinto" />"/>
				<input type="hidden" name="idBono" id="idBono" value="<x:out select = "$editarBono_xml/Bono/idbono" />"/>
				<input type="hidden" name="idCanje" id="idCanje" value=""/>
				<input type="hidden" name="idtarifa" id="idtarifa" value="<x:out select = "$editarBono_xml/Bono/tarifa/idtarifa" />"/>
				<input type="hidden" name="idtipobono" id="idtipobono" value="<x:out select = "$editarBono_xml/Bono/tipobono/idtipobono" />"/>
				<input type="hidden" name="lineacanje" id="lineacanje" value="<x:out select = "$editarBono_xml/Bono/lineacanje" />"/>
				<input type="hidden" name="xmlGuardarBono" id="xmlGuardarBono" value=""/>
				<input type="hidden" name="entradatornoses" id="entradatornoses" value="<x:out select = "$editarBono_xml/Bono/entradatornoses" />"/>
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="form-group">
							<label class="control-label col-md-5 col-sm-5 col-xs-12"><spring:message code="venta.ventareserva.busqueda.tabs.bono.editar.inicioVigencia" /></label>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<input name="fechaemisionCanje" id="fechaemisionCanje" type="text" class="form-control" data-inputmask="'mask': '99/99/9999'" value="<x:out select = "$editarBono_xml/Bono/fechaemision" />">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-5 col-sm-5 col-xs-12"><spring:message code="venta.ventareserva.busqueda.tabs.bono.editar.finVigencia" /></label>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<input name="fechacaducidadCanje" id="fechacaducidadCanje" type="text" class="form-control" data-inputmask="'mask': '99/99/9999'" value="<x:out select = "$editarBono_xml/Bono/fechacaducidad" />">
							</div>
						</div>	
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<label class="control-label col-md-5 col-sm-5 col-xs-12"><spring:message code="venta.ventareserva.busqueda.tabs.bono.editar.perfil" /></label>
							<div class="col-md-7 col-sm-7 col-xs-12 texto-resumen"><x:out select = "$editarBono_xml/Bono/tarifa/nombre" /></div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<label class="control-label col-md-5 col-sm-5 col-xs-12"><spring:message code="venta.ventareserva.busqueda.tabs.bono.editar.tipo" /></label>
							<div class="col-md-7 col-sm-7 col-xs-12 texto-resumen"><x:out select = "$editarBono_xml/Bono/tipobono/nombre" /></div>
						</div>
					</div>
					<div class="col-md-8 col-sm-8 col-xs-12">
						<div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.busqueda.tabs.bono.editar.informacionTornos" /></label>
						</div>					
					</div>						
					<div class="col-md-4 col-sm-4 col-xs-12">			
					<div class="btn-group pull-right btn-datatable">
						<a type="button" class="btn btn-info" id="tab_torno_new">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.bono.editar.nuevo.torno" />"> <span class="fa fa-plus"></span>
							</span>
						</a>
						<a type="button" class="btn btn-info" id="tab_editar_uso">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.bono.editar.modificar.uso" />"> <span class="fa fa-edit"></span>
							</span>
						</a>						
						<a type="button" class="btn btn-info" id="tab_torno_remove">
							<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.bono.editar.eliminar.torno" />"> <span class="fa fa-trash"></span>
							</span>
						</a>		
					</div>			
			    </div>
					
					<table id="datatable-list-entrada_torno" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th><spring:message code="venta.ventareserva.busqueda.tabs.bono.editar.tornos.recintos" /></th>
								<th><spring:message code="venta.ventareserva.busqueda.tabs.bono.editar.tornos.usos" /></th>
								<th><spring:message code="venta.ventareserva.busqueda.tabs.bono.editar.tornos.sesion" /></th>
							</tr>
						</thead>
						<tbody>						
						</tbody>					
					</table>
					<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.busqueda.tabs.bono.editar.observaciones" /></label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<textarea name="observaciones_bono" id="observaciones_bono"  class="form-control" ><x:out select="$editarBono_xml/Bono/observaciones" /></textarea>
							</div>
					</div>				
				</div>
		</form>
		
		<div class="modal-footer">
			<button id="listado_canje" type="button" class="btn btn-primary listado_canje">
					<spring:message code="venta.ventareserva.busqueda.tabs.bono.editar.listado.canjes" />
				</button>	
			<button type="button" id="guardar_bono" class="btn aceptar_activar_dialog"
				data-dismiss="modal">
				<spring:message code="common.button.accept" />
			</button>
			<button type="button" class="btn btn-success close_dialog"
				data-dismiss="modal">
				<spring:message code="common.button.cancel" />
			</button>
		</div>

	</div>
<script>

dttornos=$('#datatable-list-entrada_torno').DataTable( {
	"paging": false,
	"info": false,
	"searching": false,
	initComplete: function( settings, json ) {
    	window.setTimeout(cargaInicialTornos, 100);
	},
	select: { style: 'os'},
	columnDefs: [
	             { "targets": [0,1,2], "visible": false }
	             ],
	language: dataTableLanguage
	});

var json = ${editarBonoJson};
var idLineaDetalle = json.Bono.lineacanje.idlineadetalle;
var idBono = json.Bono.idbono;

function cargaInicialTornos()
{
	jsonEntradas = json.Bono.entradatornoses.Entradatornos;
	if(jsonEntradas.length>0)
		$.each(jsonEntradas, function(key, entrada){
			var idcanjebono = ""+entrada.canjebono.idcanjebono;
			
			dttornos.row.add([
	          				 entrada,	
	          				 idLineaDetalle,
	          				 idcanjebono,
	          				 entrada.recinto.nombre, 
	          				 entrada.usos, 
	          				 entrada.canjebono.lineadetallezonasesion.zonasesion.sesion.fechayhora		           		       
	          		    ]).draw();
		})
	else
		{
		var idcanjebono = ""+jsonEntradas.canjebono.idcanjebono;
		if(idcanjebono=="undefined")
			idcanjebono = "";
		dttornos.row.add([
	          				jsonEntradas,
	          				idLineaDetalle,
	          				idcanjebono,
	          				jsonEntradas.recinto.nombre, 
	          				jsonEntradas.usos, 
	          				jsonEntradas.canjebono.lineadetallezonasesion.zonasesion.sesion.fechayhora		           		       
	          		    ]).draw();
		}
}

$("#tab_torno_new").on("click", function(e) {	
	
	var data = $("#form_edit_bono").serializeObject();
	
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		dataType: "json",
		url : "<c:url value='/ajax/ventareserva/busqueda/bono/listadoTiposProducto.do'/>",
		timeout : 100000,
		data: data, 
		success : function(data) {
			var idtipoproducto=data.ArrayList.Tipoproducto.idtipoproducto;
			$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/ventareserva/busqueda/bono/show_disponibles.do'/>?idtipoproducto="+idtipoproducto+"&producto=<x:out select = '$editarBono_xml/Bono/producto/nombre' />&fecha=", function() {
	            $("#modal-dialog-form-2").modal('show');
	            setModalDialogSize("#modal-dialog-form-2", "md");
	    	 });
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
	
})


$("#tab_editar_uso").on("click", function(e) {
	var sesion = ""+dttornos.rows('.selected').data()[0][5];
	
	if(sesion == "---")
		{
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.dialog.bono.error.no.canjeado" />',
			  type: "warning",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		}
	else
		{
		var uso  = dttornos.rows('.selected').data()[0][4];
		$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/ventareserva/busqueda/bono/editarUso.do'/>?uso="+uso, function() {
	        $("#modal-dialog-form-2").modal('show');
	        setModalDialogSize("#modal-dialog-form-2", "xs");
		 });
		}

})

$("#guardar_bono").on("click", function(e) {	
	var xml = "<obtenerBonoPorId><Bono><cliente/>";
	xml+="<idbono>"+json.Bono.idbono+"</idbono>";
	xml+="<fechaemision>"+$("#fechaemisionCanje").val()+"</fechaemision>";
	xml+="<fechacaducidad>"+$("#fechacaducidadCanje").val()+"</fechacaducidad>"
	xml+="<producto>"+json2xml(json.Bono.producto,"")+"</producto>";
	xml+="<tarifa>"+json2xml(json.Bono.tarifa,"")+"</tarifa>";
	xml+="<tipobono>"+json2xml(json.Bono.tipobono,"")+"</tipobono>";
	xml+="<anulado>"+json.Bono.anulado+"</anulado>";
	xml+="<observaciones>"+$("#observaciones_bono").val()+"</observaciones>";
	xml+="<lineacanje>"+json2xml(json.Bono.lineacanje,"")+"</lineacanje>";
	xml+="<canjebonos>"+json2xml(json.Bono.canjebonos,"")+"</canjebonos>";
	xml+="<entradatornoses>";
	
	var entradas = dttornos.rows().data();
	
	if(entradas.length>0)
		{
			for(i=0;i<entradas.length;i++)
				{
				var entrada = entradas[i];
				var json_entrada = entrada[0];
				xml+="<Entradatornos>";
				xml+="<identradasvalidas>"+json_entrada.identradasvalidas+"</identradasvalidas>";
				xml+="<recinto>"+json2xml(json_entrada.recinto,"")+"</recinto>";
				xml+="<usos>"+entrada[4]+"</usos>";
				xml+="<entrada>"+json_entrada.entrada+"</entrada>";
				xml+="<canjebono>"+json2xml(json_entrada.canjebono,"")+"</canjebono>";
				xml+="</Entradatornos>";
				}
		}
	else
		{
		var json_entrada = json.Bono.entradatornoses.Entradatornos;
		xml+="<Entradatornos>";
		xml+="<identradasvalidas>"+json_entrada.identradasvalidas+"</identradasvalidas>";
		xml+="<recinto>"+json2xml(json_entrada.recinto,"")+"</recinto>";
		xml+="<usos>"+dttornos.rows().data()[0][4]+"</usos>";
		xml+="<entrada>"+json_entrada.entrada+"</entrada>";
		xml+="<canjebono>"+json2xml(json_entrada.canjebono,"")+"</canjebono>";
		xml+="</Entradatornos>";
		}
	

	
	xml+="</entradatornoses></Bono></obtenerBonoPorId>";
	$("#xmlGuardarBono").val(xml);
	
	
	var data = $("#form_edit_bono").serializeObject();
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		dataType: "json",
		url : "<c:url value='/ajax/ventareserva/busqueda/bono/actualizarBono.do'/>",
		timeout : 100000,
		data: data, 
		success : function(data) {
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});

		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
	
})






$("#tab_torno_remove").on("click", function(e) {	
	
	
	var data = dttornos.rows( { selected: true } ).data();
	
	
	
	if (data.length<=0 || data.length>1)  {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: ' <spring:message code="venta.ventareserva.busqueda.tabs.bono.editar.tornos.recintos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}else
	{
		
		var idcanje = data[0][2];
		
	//	console.log(data[0]);
	//	console.log(data[0][2]);
		if(idcanje!="")
			{
		$("#idCanje").val(idcanje);
		var data = $("#form_edit_bono").serializeObject();
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			dataType: "json",
			url : "<c:url value='/ajax/ventareserva/busqueda/bono/cancelarCanjeBono.do'/>",
			timeout : 100000,
			data: data, 
			success : function(data) {
				$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/ventareserva/busqueda/bono/editar.do'/>?id="+idBono, function() {
				});
	
			},
			error : function(exception) {
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",		     
					  delay: 5000,
					  buttons: { sticker: false }
				   });			
			}
		});
			}
	}
	
	
})

//**************************************************************
$("#listado_canje").on("click", function(e) {
	showButtonSpinner("#listado_canje");
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/ventareserva/busqueda/bono/historico_canjes.do'/>?idBono="+idBono, function() {
        $("#modal-dialog-form-2").modal('show');
        setModalDialogSize("#modal-dialog-form-2", "md");
	 });	
})
//**************************************************************



$(":input").inputmask();
</script>			

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal"
		aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.tabs.ventareserva.dialog.observaciones_venta.title" /> ${id}					
	</h4>
</div>
<div class="modal-body">
	<form id="form_observaciones_venta" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	<input type="hidden" name="idVenta" id="idVenta" value="${id}"> 
	 
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="form-group">
			<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.observaciones_venta.field.observaciones" /></label>
			<div class="col-md-8 col-sm-8 col-xs-6">
				<textarea class="form-control" id ="observaciones_venta_dialog" name="observaciones_venta_dialog">${observaciones}</textarea>	
			</div>
		</div>	
	</div>
	</form>
	
	<div class="modal-footer">
		<button type="button" id="aceptar_observaciones_venta" class="btn aceptar_activar_dialog"
			data-dismiss="modal">
			<spring:message code="common.button.accept" />
		</button>
		<button type="button" class="btn btn-success close_dialog"
			data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>

</div>
			


<script>
hideSpinner("#tab_busqueda_observaciones");
$("#observaciones_venta_dialog").val($("#observaciones_venta").val())

//*****************************************************
$("#aceptar_observaciones_venta").on("click", function(e) {
	
	var data = $("#form_observaciones_venta").serializeObject();	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/busqueda/save_observaciones.do'/>",
		timeout : 100000,
		data: data, 
		success : function(data) {
			$("#observaciones_venta").val($("#observaciones_venta_dialog").val());
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
}) 
</script>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_selector_descuentos}" var="ventareserva_selector_descuentos_xml" />


 <div class="modal-header">
 	<button type="button" id="cerrar_con_x" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button> 
</div>

<div class="modal-body">

	<form id="form_edit_sesion" class="form-horizontal form-label-left">

		<div class="col-md-12 col-sm-12 col-xs-12">
		
			<div class="form-group">
				<label class="control-label col-md-1 col-sm-1 col-xs-12"><spring:message code="venta.ventareserva.dialog.edit_sesion.field.numero" /></label>
				<div class="col-md-1 col-sm-1 col-xs-12">
					<input name="cantidad" id="cantidad" type="text" class="form-control" required="required" value="${numero}">
				</div>
				
				<label class="control-label col-md-1 col-sm-1 col-xs-12"><spring:message code="venta.ventareserva.dialog.edit_sesion.field.tarifa" /></label>
				<div class="col-md-2 col-sm-2 col-xs-12">
					<select class="form-control" name="idtarifa" id="idtarifa">
						<option value=""></option>
					</select>
				</div>		
			
				<label class="control-label col-md-1 col-sm-1 col-xs-12"><spring:message code="venta.ventareserva.dialog.edit_sesion.field.descuentos" /></label>
				<div id="descuentos_div" class="col-md-2 col-sm-2 col-xs-12">
					<select class="form-control" name="iddescuento" id="iddescuento">
						<option value=""></option>
						<x:forEach select="$ventareserva_selector_descuentos_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
	
				<div class="col-md-2 rigth_col">
					<a type="button" class="btn btn-info" id="save_edit_sesion">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.venta_individual.list.button.guardar.vender" />"> <span class="fa fa-floppy-o"></span>
						</span>
					</a>	
					<a type="button" class="btn btn-info" data-dismiss="modal" id="cerrar_ediccion">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="common.button.cancel" />"> <span class="fa fa-remove"></span>
						</span>
					</a>
				</div>
			</div>
		</div>
	</form>
</div>


<script>



hideSpinner("#button_${buttonEdit}_editar");
//*******************************************
var sesion_data=$.extend(true,[],dt_listventaindividual.rows( { selected: true } ).data()[0][0]), 
	cerrar_dialogo= false, actualizando= false;
//*******************************************
$("#cantidad").val(sesion_data.cantidad);

//**********************************************
$('#iddescuento option[value="'+ dt_listventaindividual.rows( { selected: true } ).data()[0][14] +'"]').attr("selected", "selected");
//******************************************************************
var item=sesion_data.perfiles.Perfilvisitante;
if (typeof item!="undefined") {
	var $select= $("#idtarifa");
	$select.html('');
	$select.prepend("<option value='' selected='selected'></option>");
	if (item.length>0)
	    $.each(item, function(key, val){
	      $select.append('<option value="' + val.idperfilvisitante + '" idtarifa="' + val.tarifa.idtarifa + '" importe="' + val.tarifa.Tarifaproducto.importe + '">' + val.nombre + '</option>');
	    })
	else
		$select.append('<option value="' + item.idperfilvisitante + '" idtarifa="' + item.tarifa.idtarifa + '" importe="' + item.tarifa.Tarifaproducto.importe + '">' + item.nombre + '</option>');
}
//*****************************************************************
if (typeof sesion_data.perfilvisitante.idperfilvisitante!="undefined") {
	$('#idtarifa option[value="'+ sesion_data.perfilvisitante.idperfilvisitante +'"]').attr("selected", "selected");
}
//******************************************************************
function cerrarActualizarTabla() {

	dt_listventaindividual.rows( { selected: true } ).data()[0][0]= sesion_data;
	dt_listventaindividual.rows( { selected: true } ).data()[0][6]= $("#cantidad").val();
	if ($("#idtarifa :selected").val()!="") {
		dt_listventaindividual.rows( { selected: true } ).data()[0][13]= $("#idtarifa :selected").attr("idtarifa");
		dt_listventaindividual.rows( { selected: true } ).data()[0][15]= $("#idtarifa :selected").val();
		dt_listventaindividual.rows( { selected: true } ).data()[0][7]= $("#idtarifa :selected").text();
		dt_listventaindividual.rows( { selected: true } ).data()[0][14]= $("#iddescuento :selected").val();
		dt_listventaindividual.rows( { selected: true } ).data()[0][8]= $("#iddescuento :selected").text();
		dt_listventaindividual.rows( { selected: true } ).data()[0][9]= $("#idtarifa :selected").attr("importe");
		dt_listventaindividual.rows( { selected: true } ).data()[0][10]= sesion_data.importe;
	}
	dt_listventaindividual.rows().invalidate().draw();
	$("#modal-dialog-form-3").modal('hide');
	var idcliente = $("#idcliente_rectificacion").val();
	var idtipoventa = $("#idTipoVenta_rectificacion").val();
    obtener_totales_venta_temporal(dt_listventaindividual,idcliente,idtipoventa);
    
	
}

function calcularImporteTotal() {
	var total;	
	
	var importe=$("#idtarifa :selected").attr("importe");
	if (""+importe!="undefined") {		
		total= Number($("#idtarifa :selected").attr("importe"))*parseInt($("#cantidad").val());
		return total.toFixed(2);	
	}else{		
		new PNotify({
		      title: '<spring:message code="venta.ventareserva.dialog.edit_sesion.field.numero_incorrecto" />',
		      text: "",
			  type: "error",
			  buttons: { sticker: false }				  
		   });		
		return "";
	}
	
}

function actualizarLineaDetalle() {
	actualizando= true;
	if ($("#iddescuento").val()!="") {
		showFieldSpinner("#descuentos_div");
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/venta/ventareserva/aplicar_descuentos.do'/>",
			timeout : 100000,
			data: {
					idcliente: "${idCliente}",
					lineadetalle: "<Lineadetalle>"+json2xml(sesion_data,"")+"</Lineadetalle>"
				  }, 
			success : function(data) {
				hideSpinner("#descuentos_div");			    
				sesion_data.importe= data.Lineadetalle.importe;
				if (cerrar_dialogo) cerrarActualizarTabla();
				actualizando= false;
			},
			error : function(exception) {
				hideSpinner("#descuentos_div");
				actualizando= false;
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",
					  buttons: { sticker: false }				  
				   });		
			}
		});
	}
	else {
		sesion_data.importe= calcularImporteTotal();
		actualizando= false;
	}
}

$("#cantidad").on("change", function(e) {
	sesion_data.cantidad= $("#cantidad").val();
	actualizarLineaDetalle();
});

function obtenerPerfilVisitante(tarifas,idperfilvisitante) {
	for (var i=0; i<tarifas.length; i++) 
		if (tarifas[i].idperfilvisitante==idperfilvisitante) return(tarifas[i]); 
	return("");
}

$("#idtarifa").on("change", function(e) {
	
	var perfilVisitante= obtenerPerfilVisitante(sesion_data.perfiles.Perfilvisitante,$("#idtarifa").val());
	sesion_data.perfilvisitante.nombre= perfilVisitante.nombre;
	sesion_data.perfilvisitante.idperfilvisitante= perfilVisitante.idperfilvisitante;
	sesion_data.perfilvisitante.tarifa.nombre= perfilVisitante.tarifa.nombre;
	var importeLinea = calcularImporteTotal();
	sesion_data.importe= importeLinea;
	showFieldSpinner("#descuentos_div");
	$select=$("#iddescuento");
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/list_descuentos.do'/>",
		timeout : 100000,
		data: {
				idproducto: "${idProducto}",
				idcanal: "${sessionScope.idcanal}",
				idtarifa: $("#idtarifa :selected").attr("idtarifa")
			  }, 
		success : function(data) {
			hideSpinner("#descuentos_div");
			$select.html('');
			$select.prepend("<option value='' selected='selected'></option>");
			if (data.ArrayList!="") {
				var item=data.ArrayList.LabelValue;
				if (item.length>0)
				    $.each(item, function(key, val){
				      $select.append('<option value="' + val.value + '">' + val.label + '</option>');
				    });
				else
			      	$select.append('<option value="' + item.value + '">' + item.label + '</option>');
			}
		},
		error : function(exception) {
			hideSpinner("#descuentos_div");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
		}
	});	
		
}); 

$("#iddescuento").on("change", function(e) {
	sesion_data.descuentopromocional.iddescuentopromocional= $("#iddescuento").val();
	sesion_data.descuentopromocional.nombre= $("#iddescuento").text();
	actualizarLineaDetalle();		
});



$("#save_edit_sesion").on("click", function(e) {
	$("#form_edit_sesion").submit();
});

$("#form_edit_sesion").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		saveSesionData();		
	}
});

function saveSesionData() {
	cerrar_dialogo= true;
	//if (!actualizando)
	cerrarActualizarTabla();
	dt_listventaindividual.rows().deselect();
}
//**********************************
$("#cerrar_ediccion").on("click", function(e) {
	dt_listventaindividual.rows().deselect();
});
//**********************************
$("#cerrar_con_x").on("click", function(e) {
	dt_listventaindividual.rows().deselect();
});
	


</script>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_canales_indirectos}" var="ventareserva_canales_indirectos_xml" />
<x:parse xml="${ventareserva_productos_canal}" var="ventareserva_productos_canal_xml" />
<x:parse xml="${ventareserva_tarifas_bonos}" var="ventareserva_tarifas_bonos_xml" />
<x:parse xml="${ventareserva_tipos_bono}" var="ventareserva_tipos_bono_xml" />



<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2><spring:message code="common.text.filter_list" /></h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
		<div class="x_content" style="display: block;">
		<form id="form_busqueda_bono" class="form-horizontal form-label-left">
			<!-- <input type="hidden" name="idcliente2_canje_bono" id="idcliente2_canje_bono" value=""/>-->	 
			<input type="hidden" name="xmlBonos" id="xmlBonos" value=""/>
			<div id="grupo-datos1">	
				
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.canje_bono.field.dni" /></label>
							<div class="col-md-8 col-sm-8 col-xs-12">
								<input name="dniCliente" id="dniCliente" type="text" class="form-control" >
							</div>					
					</div>
					
				</div>
				<div class="col-md-5 col-sm-5 col-xs-12">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.canje_bono.field.cliente" /></label>
						<div class="col-md-2 col-sm-2 col-xs-12">
								<input name="idcliente2_canje_bono" id="idcliente2_canje_bono" style="height:10" type="text" class="form-control"><!--  <a id="buscarClienteIndividual">v</button>-->
						</div>
						<div class="col-md-7 col-sm-7 col-xs-12">
							<input name="cliente2_canje_bono" id="cliente2_canje_bono" type="text" class="form-control" readonly>
						</div>
					</div>					
				</div>		
				
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.canje_bono.field.tarifa" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<select class="form-control" name="idPerfil" id="idPerfil">
								<option value=""></option>
								<x:forEach select="$ventareserva_tarifas_bonos_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
							</select>
						</div>
					</div>
				</div>	
			</div>
			
			<div id="grupo-datos2">	
				<div class="x_title">
					<div class="clearfix"></div>
				</div>				
				<div class="col-md-6 col-sm-6 col-xs-12">	
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.canje_bono.field.identificador" /></label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input name="idBonoIni" id="idBonoIni" type="text" class="form-control" >
						</div>
						<label class="control-label col-md-1 col-sm-1 col-xs-12"><spring:message code="venta.ventareserva.tabs.canje_bono.field.hasta" /></label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input name="idBonoFin" id="idBonoFin" type="text" class="form-control" >
						</div>
					</div>	
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.canje_bono.field.tipobono" /></label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<select class="form-control" name="idtipobono" id="idtipobono">
								<option value=""></option>
								<x:forEach select="$ventareserva_tipos_bono_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.canje_bono.field.referencia" /></label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input name="idventa" id="idventa" type="text" class="form-control" >
						</div>						
					</div>	
										
				</div>				
				<div class="col-md-6 col-sm-6 col-xs-12">	
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.canje_bono.field.producto" /></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select class="form-control" name="idProducto" id="idProducto">
								<option value=""></option>
								<x:forEach select="$ventareserva_productos_canal_xml/ArrayList/LabelValue" var="item">
									<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
							</select>
						</div>
					</div>	
					<div class="form-group date-picker">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.canje_bono.field.iniciovigencia" /></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<a type="button" class="btn btn-default btn-clear-date" id="button_iniciovigenciacanje_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.canje_bono.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
						  	</a>			
                        	<div class="input-prepend input-group">
                          		<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                          		<input type="text" name="iniciovigenciacanje" id="iniciovigencia_bono" class="form-control" value="" readonly/>
                          		<input type="hidden" required="required" name="fechainiciovigenciadecanje" value=""/>
                          		<input type="hidden" required="required" name="fechainiciovigenciahastacanje" value=""/>
                        	</div>
						</div>
					</div>				
					<div class="form-group date-picker">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.canje_bono.field.finvigencia" /></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<a type="button" class="btn btn-default btn-clear-date" id="button_finvigenciabono_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.canje_bono.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
						  	</a>			
                        	<div class="input-prepend input-group">
                          		<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                          		<input type="text" name="finvigenciacanje" id="finvigenciacanje_bono" class="form-control" value="" readonly/>
                          		<input type="hidden" required="required" name="fechafinvigenciadecanje" value=""/>
                          		<input type="hidden" required="required" name="fechafinvigenciahastacanje" value=""/>
                        	</div>
						</div>
					</div>					
				</div>				
			 </div>
			<input type="hidden" name="start" value=""/>
			<input type="hidden" name="length" value="25"/>
			<input type="hidden" id="idlineadetalleVentaBono" name="idlineadetalleVentaBono" value=""/>
			 
		</form>
		
		<div class="clearfix"></div>
		<div class="ln_solid"></div>
		<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
			<button id="button_search_canje_bono" type="button" class="btn btn-success pull-right">
				<spring:message code="common.button.filter" />
			</button>
			<button id="button_clean_canje_bono" type="button" class="btn btn-success pull-right">
				<spring:message code="venta.ventareserva.tabs.canje-bono.button.clean" />
			</button>
		</div>
	
	</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_bono_historico">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.button.historico" />"> <span class="fa fa-mail-reply"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_bono_canjear">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.button.canjear" />"> <span class="fa fa-user-times"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_bono_reimprimir">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.button.reimprimir" />"> <span class="fa fa-paint-brush"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_bono_imprimir">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.button.imprimir" />"> <span class="fa fa-print"></span>
			</span>
		</a>	
		<a type="button" class="btn btn-info" id="tab_bono_entradas">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.button.entradas" />"> <span class="fa fa-ticket"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_bono_editar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>	
		<a type="button" class="btn btn-info" id="tab_bono_anular">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.button.anular" />"> <span class="fa fa-trash"></span>
			</span>
		</a>	
		<a type="button" class="btn btn-info" id="tab_bono_activar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.button.activar" />"> <span class="fa fa-check"></span>
			</span>
		</a>		
	</div>

	<table id="datatable-list-bono" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="venta.ventareserva.tabs.canje-bono.list.header.idbono" /></th>
				<th><spring:message code="venta.ventareserva.tabs.canje-bono.list.header.cliente" /></th>
				<th><spring:message code="venta.ventareserva.tabs.canje-bono.list.header.dni" /></th>
				<th><spring:message code="venta.ventareserva.tabs.canje-bono.list.header.inivigencia" /></th>
				<th><spring:message code="venta.ventareserva.tabs.canje-bono.list.header.finvigencia" /></th>
				<th><spring:message code="venta.ventareserva.tabs.canje-bono.list.header.producto" /></th>
				<th><spring:message code="venta.ventareserva.tabs.canje-bono.list.header.anulado" /></th>
				<th><spring:message code="venta.ventareserva.tabs.canje-bono.list.header.canjeado" /></th>
				<th><spring:message code="venta.ventareserva.tabs.canje-bono.list.header.facturado" /></th>
				<th><spring:message code="venta.ventareserva.tabs.canje-bono.list.header.entrimpr" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>


<script>
var dt_listbono =$('#datatable-list-bono').DataTable( {
	serverSide: true,
	searching: false,
	ordering: false,
	lengthMenu:[[10,25,50,100],[10,25,50,100]]
	, "pageLength":25,
	pagingType: "simple",
	info: false,
	deferLoading: 0,
    ajax: {
        url: "<c:url value='/ajax/venta/ventareserva/canje_bono/list_canjebono.do'/>",
        rowId: 'idbono',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Bono)); return(""); },
        data: function (params) {
        	$('#form_busqueda_bono input[name="start"]').val(params.start);
        	$('#form_busqueda_bono input[name="length"]').val(params.length);
        	return ($("#form_busqueda_bono").serializeObject());
       	},		        
        error: function (xhr, error, thrown) {
            if (xhr.responseText=="403") {
                  $("#tab_canje_bono").hide();
            }
            else
            	{
            	new PNotify({
 					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
 					text : xhr.responseText,
 					type : "error",
 					delay : 5000,
 					buttons : {
 						closer : true,
 						sticker : false
 					}					
 				});
            	}
     },
    },
    initComplete: function( settings, json ) {
    	//$('#datatable-list-bono_length').hide();
    	//$('#datatable-list-bono_filter').hide();
        $('a#menu_toggle').on("click", function () {dt_listbono.columns.adjust().draw(); });
	},
    columns: [
	{ data: "idbono", type: "spanish-string", defaultContent: ""},
	{ data: "lineaventa.venta.cliente.nombre", type: "spanish-string", defaultContent: ""}, 
	{ data: "lineaventa.venta.cliente.identificador", type: "spanish-string", defaultContent: "" },		 
	{ data: "fechaemision", type: "spanish-string" ,  defaultContent:"",
		 render: function ( data, type, row, meta ) {			 
    	  	  return data.substr(0, 10);}	 
	 } ,
	 { data: "fechacaducidad", type: "spanish-string" ,  defaultContent:"",
		 render: function ( data, type, row, meta ) {			 
    	  	  return data.substr(0, 10);}	 
	 } ,
	{ data: "producto.nombre", type: "spanish-string", defaultContent: ""},
	{ data: "anulado", className: "text_icon cell_centered",
		render: function ( data, type, row, meta ) {
	  	  	if (data==1) return '<i class="fa fa-check" aria-hidden="true"></i>'; else return '<i class="fa fa-close" aria-hidden="true"></i>';	
	}},
	
	{ data: "canjebonos.Canjebono", className: "text_icon cell_centered",
		render: function ( data, type, row, meta ) {			
	  	  	if  (data != null && typeof data != "undefined") return '<i class="fa fa-check" aria-hidden="true"></i>'; else return '<i class="fa fa-close" aria-hidden="true"></i>';	
	}},
	{ data: "facturado", className: "text_icon cell_centered",
		render: function ( data, type, row, meta ) {
	  	  	if (data==1) return '<i class="fa fa-check" aria-hidden="true"></i>'; else return '<i class="fa fa-close" aria-hidden="true"></i>';	
	}},
	{ data: "entradatornoses.entrada", className: "text_icon cell_centered",
		render: function ( data, type, row, meta ) {
			if  (data != null && typeof data != "undefined") return '<i class="fa fa-check" aria-hidden="true"></i>'; else return '<i class="fa fa-close" aria-hidden="true"></i>';	
	}},
	
    ],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-bono') },
    select: { style: 'os'},
	language: dataTableLanguage,
	processing: true,
} );

insertSmallSpinner("#datatable-list-bono_processing");
//***************************************************************
$("#button_search_canje_bono").on("click", function(e) {
	dt_listbono.ajax.reload();
	})
	
function borrarFormularioTabBono()
{
	 $('input[name="dniCliente"]').val('');
	 $('#idcliente2_canje_bono').val('');	 
	 $('input[name="cliente2_canje_bono"]').val('');
	 $('#idPerfil').val('');
	 $('input[name="idBonoIni"]').val('');
	 $('input[name="idBonoFin"]').val('');
	 $('#idProducto').val('');
	 $('#idtipobono').val('');
	 $('input[name="iniciovigenciacanje"]').val('');
	 $('input[name="finvigenciacanje"]').val('');
	 $('input[name="idventa"]').val('');	
}
	

//***************************************************************
$("#button_clean_canje_bono").on("click", function(e) {
	borrarFormularioTabBono();	 
	})
	
//***************************************************************
$("#cliente2_canje_bono").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>?editable=1", "#cliente2_canje_bono", "#idcliente2_canje_bono");
//***************************************************************
//Fecha inicio vigencia
$('input[name="iniciovigenciacanje"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
  	  	 $('input[name="iniciovigenciacanje"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('input[name="fechainiciovigenciadecanje"]').val(start.format('DD/MM/YYYY'));  	  	 
  	  	 $('input[name="fechainiciovigenciahastacanje"]').val(end.format('DD/MM/YYYY'));
	 });
//***************************************************************
$("#button_iniciovigenciacanje_clear").on("click", function(e) {
    $('input[name="iniciovigenciacanje"]').val('');
    $('input[name="fechainiciovigenciadecanje"]').val('');
    $('input[name="fechainiciovigenciahastacanje"]').val('');
});
//****************************************************
$('input[name="finvigenciacanje"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
  	  	 $('input[name="finvigenciacanje"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('input[name="fechafinvigenciadecanje"]').val(start.format('DD/MM/YYYY'));  	  	 
  	  	 $('input[name="fechafinvigenciahastacanje"]').val(end.format('DD/MM/YYYY'));
	 });
//****************************************************
$("#button_finvigenciabono_clear").on("click", function(e) {
    $('input[name="finvigenciacanje"]').val('');
    $('input[name="fechafinvigenciadecanje"]').val('');
    $('input[name="fechafinvigenciahastacanje"]').val('');
});

//*****************************************************
$("#tab_bono_historico").on("click", function(e) { 
	
var data = sanitizeArray(dt_listbono.rows( { selected: true } ).data(),"idbono");
	
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.tabs.canje-bono.list..alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.tabs.canje-bono.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	showButtonSpinner("#tab_canjebono_historico");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_historico_canje_bono.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "lg");
	});

});

//*****************************************************
$("#tab_bono_imprimir").on("click", function(e) {
	var data = sanitizeArray(dt_listbono.rows( { selected: true } ).data(),"idbono");
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.ninguna_seleccion.imprimir" />',		      
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	var xml = "<list>";
	
	for(i=0;i<data.length;i++)
	{
	xml+= "<int>"+data[i]+"</int>";
	}
	xml +="</list>";
	
	$("#xmlBonos").val(xml);
	
	var data = $("#form_busqueda_bono").serializeObject();
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/busqueda/bono/imprimir.do'/>",
		timeout : 100000,
		data: data, 
		success : function(data) {
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
	
	
	
})



//*****************************************************
$("#tab_bono_reimprimir").on("click", function(e) {
	var data = sanitizeArray(dt_listbono.rows( { selected: true } ).data(),"idbono");
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.ninguna_seleccion.reimprimir" />',		      
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	var xml = "<listidsbono>";
	
	for(i=0;i<data.length;i++)
	{
	xml+= "<int>"+data[i]+"</int>";
	}
	xml +="</listidsbono>";
	
	
	$("#xmlBonos").val(xml);
	
	var data = $("#form_busqueda_bono").serializeObject();
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/busqueda/bono/reimprimir.do'/>",
		timeout : 100000,
		data: data, 
		success : function(data) {
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
	
	
	
})

//*****************************************************
$("#tab_bono_activar").on("click", function(e) {
	
	var data = sanitizeArray(dt_listbono.rows( { selected: true } ).data(),"idbono");
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.ninguna_seleccion.activar" />',		      
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	//Ponemos que es una activacion
	$("#bonoMotivosAction").val("2");
	
	setModalDialogSize("#motivos_bonos", "sm");
	$("#motivos_bonos").modal('show');
})

//*****************************************************
$("#tab_bono_anular").on("click", function(e) {
	
	var data = sanitizeArray(dt_listbono.rows( { selected: true } ).data(),"idbono");
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.ninguna_seleccion.anular" />',		      
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	//Ponemos que es una anulacion
	$("#bonoMotivosAction").val("1");
	
	setModalDialogSize("#motivos_bonos", "sm");
	$("#motivos_bonos").modal('show');
})

//*****************************************************
$("#tab_bono_editar").on("click", function(e) {
	
	var data = sanitizeArray(dt_listbono.rows( { selected: true } ).data(),"idbono");
	
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.seleccion_multiple.editar" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.ninguna_seleccion.editar" />',		      
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/ventareserva/busqueda/bono/editar.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});	
	
	
	
})



//*****************************************************
$("#tab_bono_entradas").on("click", function(e) {
	var data = dt_listbono.rows( { selected: true } ).data();
	
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.seleccion_multiple.entradas" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.ninguna_seleccion.entradas" />',		      
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	$("#modal-dialog-form .modal-content").load("<c:url value='/venta/ventareserva/busqueda/Bonos/pantalla_entradas.do'/>?id="+data[0].lineacanje.idlineadetalle, function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form" ,"lg");
	});	
})

var idBono;
var idLineaDetalle;
//*****************************************************
$("#tab_bono_canjear").on("click", function(e) {
	var data = dt_listbono.rows( { selected: true } ).data();
	/*if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.seleccion_multiple.entradas_canje" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}*/
	
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.ninguna_seleccion.entradas_canje" />',		      
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	
	var bonoCanjeado= data[0].canjebonos;
	if( bonoCanjeado==""){
	
	idBono = dt_listbono.rows( { selected: true } ).data()[0].idbono;
	idLineaDetalle = dt_listbono.rows( { selected: true } ).data()[0].lineacanje.idlineadetalle
	
	
	showButtonSpinner("#tab_bono_canjear");	
	var formdata = $("#form_busqueda_bono").serializeObject();
	var f = new Date();
	var fecha = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
		
	var idproducto = dt_listbono.rows( { selected: true } ).data()[0].producto.idproducto;
	var producto = dt_listbono.rows( { selected: true } ).data()[0].producto.nombre;
	
	
	
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/ventareserva/busqueda/show_disponibles_bono_canje.do'/>?idproducto="+idproducto+"&idcliente="+formdata.idcliente2_canje_bono+"&idtipoventa=4&producto="+producto+"&fecha="+fecha+"&button=tab_bono_canjear&list=dt_listbono", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
	}else
	{
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.bono_canjeado" />',		      
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
		
	}
	
})

$("#idcliente2_canje_bono").blur(function() {
	var idCliente =""+$("#idcliente2_canje_bono").val(); 
	
	if(idCliente!="")
	{
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/facturacion/buscarClientePorId.do'/>",
		timeout : 100000,
		data: {
			idcliente: idCliente,    			
		},
		success : function(data) {    		     
				$("#cliente2_canje_bono").val(data.Cliente.nombrecompleto); 
				
				
				
		},
		error : function(exception) {    	
			$("#idcliente2_canje_bono").val("");
	    	$("#cliente2_canje_bono").val("");
	    	

			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});
	}
	else
		$("#cliente2_canje_bono").val("");
})



</script>



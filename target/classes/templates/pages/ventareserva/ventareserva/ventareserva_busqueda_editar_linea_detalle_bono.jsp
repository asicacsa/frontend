<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_productos_principales}" var="ventareserva_productos_principales_xml" />
<x:parse xml="${ventareserva_canales_indirectos}" var="ventareserva_canales_indirectos_xml" />
<x:parse xml="${ventareserva_perfiles_tarifas}" var="ventareserva_perfiles_tarifas_xml" />
<x:parse xml="${ventareserva_venta_bono}" var="ventareserva_venta_bono_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>		
</div>

<div class="modal-body">
		<form id="form_selector_emision_bono" class="form-horizontal form-label-left">
			<input type="hidden" id="idcliente_emision_bono" name="idcliente_emision_bono" value=""/>
			<input type="hidden" id="cpcliente_emision_bono" name="cpcliente_emision_bono" value=""/>
			<input type="hidden" id="emailcliente_emision_bono" name="emailcliente_emision_bono" value=""/>

			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label class="control-label"><spring:message code="venta.ventareserva.tabs.emision_bono.field.unidad" /></label>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<select class="form-control ventareserva" name="idunidad_emision_bono" id="idunidad_emision_bono" size="5" required="required">
							<x:forEach select="$ventareserva_productos_principales_xml/ArrayList/Entry/unidadnegocio" var="item">
								<option value="<x:out select="$item/idunidadnegocio" />"><x:out select="$item/nombre" /></option>
							</x:forEach>
						</select>
					</div>
				</div>
			</div>
				
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label class="control-label"><spring:message code="venta.ventareserva.tabs.emision_bono.field.producto" /></label>
					<div class="col-md-12 col-sm-12 col-xs-12" id="div_producto_emision_bono">
						<select class="form-control ventareserva" name="idproducto_emision_bono" id="idproducto_emision_bono" size="5" required="required">
							<option value=""></option>
						</select>
					</div>
				</div>				
			</div>
		<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
			<button id="button_add_emision_bono" type="button" class="btn btn-success pull-right">
				<spring:message code="venta.ventareserva.tabs.emision_bono.button.add" />
			</button>
		</div>		
		</form>
	

	
<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="totales-group totales_emision_bono">
		Total: <span class="total-lbl"><span id="total-val">0.00</span>&euro;</span>&nbsp;<span class="descuento-lbl">Descuento: <span id="total-desc">0.00</span>&euro;&nbsp;(<span id="total-prc">0.00</span>%)</span>
	</div>

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="button_emision_bono_editar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.emision_bono.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="button_emision_bono_duplicar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.emision_bono.list.button.duplicar" />"> <span class="fa fa-clone"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="button_emision_bono_eliminar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.emision_bono.list.button.eliminar" />"> <span class="fa fa-trash"></span>
			</span>
		</a>		
	</div>

	<table id="datatable_list_emision_bono" class="table table-striped table-bordered dt-responsive ventareserva" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th></th>
				<th><spring:message code="venta.ventareserva.tabs.emision_bono.list.header.idproducto" /></th>
				<th><spring:message code="venta.ventareserva.tabs.emision_bono.list.header.producto" /></th>
				<th><spring:message code="venta.ventareserva.tabs.emision_bono.list.header.recinto" /></th>
				<th><spring:message code="venta.ventareserva.tabs.emision_bono.list.header.fechainicio" /></th>
				<th><spring:message code="venta.ventareserva.tabs.emision_bono.list.header.fechafin" /></th>
				<th><spring:message code="venta.ventareserva.tabs.emision_bono.list.header.bonos" /></th>
				<th><spring:message code="venta.ventareserva.tabs.emision_bono.list.header.perfil" /></th>
				<th><spring:message code="venta.ventareserva.tabs.emision_bono.list.header.tipo" /></th>
				<th><spring:message code="venta.ventareserva.tabs.emision_bono.list.header.importe" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<div class="modal-footer">
			<button id="save_venta_bono_lineas_detalle_button" type="button" class="btn btn-primary rectificar_dialog-save">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>
<script>

var json_venta_lineas_bono=${json_venta_lineas_bono};
var json_venta_lineas_descuento = 0;

var unidadesnegocio = new Array();
var productos= new Array();

<x:forEach select="$ventareserva_productos_principales_xml/ArrayList/Entry" var="item">
productos=[];
<x:forEach select="$item/productos/Producto" var="producto">
	productos.push({value: '<x:out select="$producto/value" />', label: '<x:out select="$producto/label" />'});
</x:forEach>
unidadesnegocio['<x:out select="$item/unidadnegocio/nombre" />']= productos;
</x:forEach>

var lineasDetallesEliminadas = [];

var porcentaje_descuento_cliente= 0; 

$('input[name="fecha_emision_bono"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
	minDate: moment(),
  	locale: $daterangepicker_sp
});

var dt_listemisionbono=$('#datatable_list_emision_bono').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	scrollCollapse: true,
	paging: false,
    select: { style: 'os' },
    initComplete: function( settings, json ) {
      	 window.setTimeout(cargarLineasParaVenta, 100);    	 
   	},
	columnDefs: [
        { "targets": 0, "visible": false },
        { "targets": 1, "visible": false },
        { "targets": 3, "visible": false }
    ],
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('datatable_list_emision_bono');
   	}
} );

//********************************************************************************
function construir_xml_emisionbono() {
	var lineas_detalle= $("#datatable_list_emision_bono").DataTable().rows().data();
	var xml_lineasdetalle= "<listaDto>";
	for (var i=0; i<lineas_detalle.length; i++) { 
		xml_lineasdetalle+="<ObtenerPerfilesy1Tarifaparam>";
		xml_lineasdetalle+="<idcliente>"+$("#idcliente_emision_bono").val()+"</idcliente>";
		xml_lineasdetalle+="<idtipoventa>2</idtipoventa>";
		xml_lineasdetalle+="<idcanal>${sessionScope.idcanal}</idcanal>";
		xml_lineasdetalle+="<idproducto>"+lineas_detalle[i][0].producto.idproducto +"</idproducto>";
		xml_lineasdetalle+="</ObtenerPerfilesy1Tarifaparam>";
	}
	xml_lineasdetalle+="</listaDto>";
	return(xml_lineasdetalle);
}

//********************************************************************************
function poner_totales_emision_bono() {
		var lineas_detalle= $("#datatable_list_emision_bono").DataTable().rows().data();
	
	var porcentaje_descuento_cliente = $("#descuentoBono").val();
	
	if(porcentaje_descuento_cliente=="")
		{
		porcentaje_descuento_cliente = 0.0;
		}
	
	var total= 0;
	var error= false;
	
	for (var i=0; i<lineas_detalle.length; i++) {
		var importe= lineas_detalle[i][9];
		total+= Number(importe);		
	}
	
	
	if (error) {
		$(".totales_emision_bono #total-val").text("0.00");
		$(".totales_emision_bono #total-desc").text("0.00");
	}
	else {
		var descuento= porcentaje_descuento_cliente>0?porcentaje_descuento_cliente*total/100:0;
		total= total-descuento;
		
		$(".totales_emision_bono #total-val").text(total.toFixed(2));
		$(".totales_emision_bono #total-desc").text(descuento.toFixed(2));
		$(".totales_emision_bono #total-prc").text(porcentaje_descuento_cliente);
		
	}
	
}

//********************************************************************************
function obtener_descuento_emision_bono() {
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/obtener_descuento_cliente.do'/>",
		timeout : 100000,
		data: {
			xml: construir_xml_emisionbono()
		},
		success : function(data) {
			if (data.ArrayList!="") {
				var item=data.ArrayList.PerfilesYDescuento;
				porcentaje_descuento_cliente= item.length>0?Number(item[0].descuento.porcentajedescuento):Number(item.descuento.porcentajedescuento);
				poner_totales_emision_bono();
			}
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});	
}

//********************************************************************************
function actualizar_totales_emision_bono() {
	var lineas_detalle= $("#datatable_list_emision_bono").DataTable().rows().data();    
	if (lineas_detalle.length>0 && $("#idcliente_emision_bono")!="" && porcentaje_descuento_cliente==0) 
		obtener_descuento_emision_bono();
	else
		poner_totales_emision_bono();
}
//********************************************************************************
$("#cliente_emision_bono").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>", "#cliente_emision_bono", "#idcliente_emision_bono", "", "#cpcliente_emision_bono", "#emailcliente_emision_bono");

$("#cliente_emision_bono").on("change", function(e) {
	var lineas_detalle= $("#datatable_list_emision_bono").DataTable().rows().data();

	if (lineas_detalle.length>0) obtener_descuento_emision_bono();
});

$("#idunidad_emision_bono").on("change", function(e) {
	showFieldSpinner("#div_producto_emision_bono");

	var data= $("#idunidad_emision_bono :selected").val();
	var $select=$("#idproducto_emision_bono");
	
	/* Primero se a�aden los productos principales */
	
	$select.html('');
	unidadesnegocio[$("#idunidad_emision_bono :selected").text()].forEach( function(element) { 
	      $select.append('<option class="option-main-product" value="' + element.value + '">' + element.label + '</option>');
	});
	
	/* Luego se a�aden los subproductos por ajax */
	
	if (data!="") { // La unidad de negocio COMBINADAS no tiene id
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/venta/ventareserva/list_subproductos.do'/>",
			timeout : 100000,
			data: {
		    	   id: data.toString()
				  }, 
			success : function(data) {
				hideSpinner("#div_producto_emision_bono");
				if (data.ArrayList!="") {
					var item= data.ArrayList.LabelValue;
					if (item.length>0) {
						item=sortJSON(item,"label",true);
					    $.each(item, function(key, val){
					      $select.append('<option value="' + val.value + '">' + val.label + '</option>');
					    });
					}
					else
					      $select.append('<option value="' + item.value + '">' + item.label + '</option>');
				}
			},
			error : function(exception) {
				hideSpinner("#div_producto_emision_bono");
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",
					  buttons: { sticker: false }				  
				   });		
			}
		});	
	}
	else hideSpinner("#div_producto_emision_bono");

}); 	

//********************************************************************************
$("#button_add_emision_bono").on("click", function(e) { 
	
	if ($("#idproducto_emision_bono option:selected").length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	

	//showButtonSpinner("#button_add_emision_bono");
	
	var formdata = $("#form_selector_emision_bono").serializeObject();
				
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/get_producto.do'/>",
		timeout : 100000,
		data: {
			tipolineadetalle: "Lineadetallebonoparam",
			idcliente: formdata.idcliente_emision_bono,
			idcanal: "${sessionScope.idcanal}",
			idtipoventa: 2, // Tipo venta Bono
			idproducto: formdata.idproducto_emision_bono,
			fecha: formdata.fecha_emision_bono
		},
		success : function(data) {
			var recintos= "";
			var ponbr= false;
			item= data.Lineadetalle.producto.tipoprodproductos.Tipoprodproducto;
			if (item.length>0) {
			    $.each(item, function(key, val){
			    	if (ponbr) recintos= recintos+",<br/>"; else ponbr= true;		
			    	recintos=recintos+item[key].tipoproducto.recinto.nombre;
		    	});
	    	}
			else recintos= item.tipoproducto.recinto.nombre;   	
			    
			dt_listemisionbono.row.add([data.Lineadetalle,
			                            data.Lineadetalle.producto.idproducto, 
			                            data.Lineadetalle.producto.nombre,
			                            recintos,
			                            "", // Fecha inicio
			                            "", // Fecha fin
			                            data.Lineadetalle.cantidad,  // N� bonos
			                            typeof data.Lineadetalle.perfilvisitante.nombre!="undefined"?data.Lineadetalle.perfilvisitante.nombre:"", // Perfil/Tarifa
			                            "", // Tipo bono
			                            typeof data.Lineadetalle.importe!="undefined"?data.Lineadetalle.importe:"--"  // Importe
		                               ]).draw();
			actualizar_totales_emision_bono();
			hideSpinner("#button_add_emision_bono");
		},
		error : function(exception) {
			hideSpinner("#button_add_emision_bono");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});	

}); 

//********************************************************************************
$("#button_emision_bono_editar").on("click", function(e) {
	
	if (dt_listemisionbono.rows( { selected: true }).data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	if (dt_listemisionbono.rows( { selected: true }).data().length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#button_emision_bono_editar");	

	var data = $("#form_selector_emision_bono").serializeObject();

	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/venta/ventareserva/busqueda/show_edit_emision_bono.do'/>", function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "sm");
	});
});

//********************************************************************************
$("#button_emision_bono_duplicar").on("click", function(e) { 
		
	var data= $.extend(true,[],dt_listemisionbono.rows( { selected: true } ).data());

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	
	dt_listemisionbono.rows( { selected: true } ).data()[0][0].idlineadetalle="";

	
	dt_listemisionbono.rows.add(data).draw();
	actualizar_totales_emision_bono();

}); 

//********************************************************************************
$("#button_emision_bono_eliminar").on("click", function(e) { 
		
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.confirm.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
		  buttons: { closer: false, sticker: false	},
		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
		   var lineas_seleccionadas =  dt_listemisionbono.rows( { selected: true } );
		   for(i=0;i<lineas_seleccionadas.length;i++)
			   lineasDetallesEliminadas.push(lineas_seleccionadas.data()[i][0]);
		   
		   dt_listemisionbono.rows( { selected: true } ).remove().draw();
		   actualizar_totales_emision_bono();
	   }).on('pnotify.cancel', function() {
	   });		 

}); 

//********************************************************************************
$("#button_emision_bono_cancelar").on("click", function(e) { 
		
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="venta.ventareserva.tabs.emision_bono.list.confirm.cancelar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
		  buttons: { closer: false, sticker: false	},
		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
		   dt_listemisionbono.rows().remove().draw();
		   actualizar_totales_emision_bono();
	   }).on('pnotify.cancel', function() {
	   });		 

}); 

//********************************************************************************
$("#button_emision_bono_vender").on("click", function(e) {
	
	if (dt_listemisionbono.rows().data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.venta_vacia" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	if ($("#idcliente_emision_bono").val()=="") {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="venta.ventareserva.list.alert.productos.cliente_requerido" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
	}
	
	showButtonSpinner("#button_emision_bono_vender");	

	var data = $("#form_selector_emision_bono").serializeObject();

	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_vender_emision_bono.do'/>", function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});
});

//**********************************************************
function cargarLineasParaVenta()
{	
	
	var data = json_venta_lineas_bono.Venta.lineadetalles.Lineadetalle;

	
	if (data.length>0){
	for (var j=0; j<data.length; j++) {
		var lineadetalle = data[j];		
		cargarLinea(lineadetalle);		
	}	
	}else
		cargarLinea(data);
	
	
	actualizar_totales_emision_bono();	
}
//*******************************************
function cargarLinea(lineadetalle)
{
	var ponbr= false;
	var nombreproducto= lineadetalle.producto.nombre;	
	var nombrerecintos="";
	var item= lineadetalle.producto.tipoprodproductos.Tipoprodproducto;
	if (item.length>0) {
	    $.each(item, function(key, val){
	    	if (ponbr)
	    		nombrerecintos= nombrerecintos+",<br/>";
	    	else 
	    		ponbr= true;		
	    	nombrerecintos=nombrerecintos+item[key].tipoproducto.recinto.nombre;	    	
	    });
	}
	else nombrerecintos= item.tipoproducto.recinto.nombre;
	
	var bono = lineadetalle.bonosForIdlineadetalle.Bono;
	if(bono.length>0)
		bono = bono[0];
	var fechaInicio = "" + bono.fechaemision;
	fechaInicio = fechaInicio.substring(0,10);
	var fechaFin = ""+ bono.fechacaducidad;
	fechaFin = fechaFin.substring(0,10);
	tipoBono = bono.tipobono.nombre;
	
	// Versi�n antigua sacando el importe de la linea de detalle
	//var importe = lineadetalle.importe;
	
	//Con el calculo aplicando el descuento
	var importeUnitario = parseFloat(lineadetalle.tarifaproducto.importe);
	var cantidad = parseFloat(lineadetalle.cantidad);
	var importe = (1.0 - descuento ) * importeUnitario *cantidad;
	
	
	dt_listemisionbono.row.add([lineadetalle,
		                        lineadetalle.producto.idproducto, 
		                        nombreproducto,
		                        nombrerecintos,
		                        fechaInicio, // Fecha inicio
		                        fechaFin, // Fecha fin
		                        lineadetalle.cantidad,  // N� bonos
		                        typeof lineadetalle.perfilvisitante.nombre!="undefined"?lineadetalle.perfilvisitante.nombre:"", // Perfil/Tarifa
		                        tipoBono, // Tipo bono
		                        importe// Importe
	                            ]).draw();

}

//*******************************************
$("#save_venta_bono_lineas_detalle_button").on("click", function(e) {
	
	
	//Si alguno de los importes de alguna de las lineas de detalle es nulo no dejamos grabar
	
	for(i=0; i<dt_listemisionbono.rows().data().length;i++)
	{
		if(dt_listemisionbono.rows().data()[i][9]=="--"){
			new PNotify({
			      title: '<spring:message code="venta.ventareserva.dialog.edit_sesion.field.numero_lineadetalle" />',
			      text: "",
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
			return;
		}
	}
	
	
	
	
	var idventa = $("#idVentaEditarBono").val();
    var idcanal=$("#idcanal_venta_bono").val();
    
    var importetotalventa=$("#total-val").text();
    var importetotalpagado=$("#total-val_venta").text();

    
   
	var url= "<c:url value='/ajax/ventareserva/busqueda/venta/editarPago.do'/>?id="+idventa+"&importetotalventa="+importetotalventa+"&importetotalpagado="+importetotalpagado+"&idcanal="+idcanal+"&nombretabla=dt_listemisionbono";
	$("#modal-dialog-form-3 .modal-content").load(url, function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "sm");
	});
})


function actualizarResumen(id)
{
	$("#modal-dialog-form .modal-content").load("<c:url value='/venta/ventareserva/busqueda/ventareserva/editar_venta_bono.do'/>?id="+id, function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "md");
	});	
}


$( "#button_add_emision_bono" ).hide();

$( "#idproducto_emision_bono" ).click(function() {    	
	$( "#button_add_emision_bono" ).click();
	});


</script>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>



 <div class="x_panel filter_list thin_padding">
<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.tabs.venta_abono.title" />
	</h4>
</div>
 <div class="x_content" style="display: block;">	
		<form id="form_selector_venta_individual" class="form-horizontal form-label-left">		
			<input type="hidden" id="cifcliente_venta_individual" value=""/>
			<input type="hidden" id="cpcliente_venta_individual" value=""/>
			<input type="hidden" id="emailcliente_venta_individual" value=""/>
			<input type="hidden" id="telefonocliente_venta_individual" value=""/>
			<input type="hidden" id="telefonomovilcliente_venta_individual" value=""/>
		</form>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<button display="none"  id="button_vender" type="button" class="btn btn-success pull-right">
				<spring:message code="venta.ventareserva.tabs.venta_individual.list.button.vender" />
			</button>
			<button id="button_disp_venta_abono" type="button" class="btn pull-right">
				<spring:message code="venta.ventareserva.tabs.emision_bono.button.disp" />
			</button>
		</div>
	</div>
	
	<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="totales-group totales_venta_abono">
		Total: <span class="total-lbl"><span id="total-val">0.00</span>&euro;</span>&nbsp;<span class="descuento-lbl">Descuento: <span id="total-desc">0.00</span>&euro;&nbsp;</span>
	</div>



	<table id="datatable_list_venta_abono" class="table table-striped table-bordered dt-responsive ventareserva" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th></th>
				<th><spring:message code="venta.ventareserva.tabs.venta_abono.list.header.titular" /></th>
				<th><spring:message code="venta.ventareserva.tabs.venta_abono.list.header.nombre" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.producto" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.fecha" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.perfil" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.descuentos" /></th>
				<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.importe" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
</div>
<script>

var json_productos_abono;
hideSpinner("#finalizar_button");

var dt_listventaabono=$('#datatable_list_venta_abono').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	scrollCollapse: true,
	paging: false,
    select: { style: 'os' },
	columnDefs: [
        { "targets": 0, "visible": false },
    ],
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('datatable_list_venta_abono');
   	}
} );

list_abonos.forEach(function(elemento, indice) {	
	json_productos_abono=elemento;
	
	var titular;
	var nombre;
	var producto;
	var fecha;
	var perfil; 
	var descuento;
	var importe;
	titular = elemento.LineadetalleVentaPasesDTO.titular;
	if(titular!=undefined)
		titular = elemento.LineadetalleVentaPasesDTO.datosClienteClub.titular;
	
	nombre = elemento.LineadetalleVentaPasesDTO.datosClienteClub.nombrecompleto;
	producto = elemento.LineadetalleVentaPasesDTO.producto.nombre;
	fecha = moment().format("DD/MM/YYYY");
	perfil = elemento.LineadetalleVentaPasesDTO.perfilvisitante.nombre;
	descuento=
  		importe= elemento.LineadetalleVentaPasesDTO.importe;
	var img_titular="";
  	if(titular==1)
  			img_titular='<p align="center"> <i class="fa fa-check" aria-hidden="true"> </p>' ;
	else
			img_titular='<p align="center"><i class="fa fa-close" aria-hidden="true"> </p>';	
    dt_listventaabono.row.add([
                            elemento,
                            img_titular, 
                            nombre,
                            producto,
                            fecha,
                            perfil,
                            "",
                            importe 
                            ]).draw(); 
    
});

obtener_totales_venta_temporal();
//**********************************************************************************
var celda;
$("#datatable_list_venta_abono tbody").delegate("td", "click", function() {	
	celda=this;
	var posicionCelda=0;    	
	posicionCelda=$("td", celda).context.cellIndex;
	if ( posicionCelda=="4" || posicionCelda=="5"){    		
		window.setTimeout(editarLineaDetalleVentaAbono, 100);
	}
});   
//************************************************************************************
function editarLineaDetalleVentaAbono()
{	
	if (dt_listventaabono.rows( { selected: true }).data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.abonos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	if (dt_listventaabono.rows( { selected: true }).data().length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.abonos.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	var data = $("#form_selector_venta_individual").serializeObject(),
	rowdata= dt_listventaabono.rows( { selected: true }).data();
	var idproductolinea_detalle=dt_listventaabono.rows( { selected: true }).data()[0][0].LineadetalleVentaPasesDTO.producto.idproducto;
	var nombre_producto=dt_listventaabono.rows( { selected: true }).data()[0][0].LineadetalleVentaPasesDTO.producto.descripcion;
	var fecha=dt_listventaabono.rows( { selected: true }).data()[0][4];
	var idtarifa=dt_listventaabono.rows( { selected: true }).data()[0][0].LineadetalleVentaPasesDTO.perfilvisitante.idperfilvisitante;
	var idcliente = "1";
	
	
	 $("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/ventareserva/busqueda/venta/show_edit_sesion_abono.do'/>?idproducto="+idproductolinea_detalle+"&idcliente="+idcliente+"&producto="+encodeURIComponent(nombre_producto)+"&fecha="+fecha+"&idtarifa="+idtarifa+"&idcanal=${sessionScope.idcanal}&idxnumero=6&idxunitario=9&idxtotal=10&idxbono=12&&idxtarifa_txt=7&idxtarifa_id=13&idxdescuento_txt=8&idxdescuento_id=14&idxperfil=15&button=venta_individual&list=dt_listventaindividual", function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "md");
	}); 
}


function obtener_totales_venta_temporal() {
	var importe = 0;
	var descuento = 0;
	for(var i=0; i<dt_listventaabono.rows().data().length; i++)
	    {
		var linea = dt_listventaabono.rows().data()[i][0];
		var importe_parcial = linea.LineadetalleVentaPasesDTO.importe;
		var descuento_parcial = importe_parcial * parseFloat(linea.LineadetalleVentaPasesDTO.descuento.porcentajedescuento)/100.0;
		if(!isNaN(descuento_parcial))
			{
			importe_parcial = importe_parcial-descuento_parcial;
			descuento = descuento + descuento_parcial;
			}					
		importe = importe + importe_parcial;	
	    }
	
	
	$(".totales_venta_abono #total-val").text(importe);
	$(".totales_venta_abono #total-desc").text(descuento);
}

    
  //********************************************************************************
    $("#button_disp_venta_abono").on("click", function(e) {
    	
    	showButtonSpinner("#button_disp_venta_individual");	

    	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_tabla_disponibilidad.do'/>?button=disp_venta_individual", function() {
    		$("#modal-dialog-form-3").modal('show');
    		setModalDialogSize("#modal-dialog-form-3", "md");
    	});
    });
    //********************************************************************************
	$("#button_vender").on("click", function(e) {
	
	//showButtonSpinner("#button_venta_individual_vender");	
	var data = $("#form_selector_venta_individual").serializeObject();

	
	
	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_vender_grupo_abonos.do'/>", function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "md");
	});
});

	//********************************************************************************
    
</script>

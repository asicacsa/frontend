<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${entradas}" var="entradas_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal"
		aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.busqueda.tabs.bono.entradas.title" />					
	</h4>
</div>	

	<div class="modal-body">
				
			<div class="btn-group pull-right btn-datatable">
					<a type="button" class="btn btn-info" id="tab_entradas_bonos_impresiones">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.button.impresiones" />"> <span class="fa fa-mail-reply"></span>
						</span>
					</a>	
					<a type="button" class="btn btn-info" id="tab_entradas_bonos_modificar">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.button.modificar" />"> <span class="fa fa-pencil"></span>
						</span>
					</a>
					<a type="button" class="btn btn-info" id="tab_entradas_bonos_imprimir">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.button.imprimir" />"> <span class="fa fa-print"></span>
						</span>
					</a>	
					<a type="button" class="btn btn-info" id="tab_entradas_bonos_reimprimir">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.button.reimprimir" />"> <span class="fa fa-paint-brush"></span>
						</span>
					</a>
			</div>
						
			
			<table id="tab_entradasBono" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th id="identrada"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.ref_entrada" /></th>
						<th id="idventa"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.ref_venta" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.ref_reserva" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.ref_bono" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.producto" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.contenido" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.fecha" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.hora" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.recinto" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.usos" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.impr" /></th>
						<th><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.header.inhab" /></th>
					</tr>
				</thead>
				<tbody>
					<x:forEach select="$entradas_xml/ArrayList/Entrada" var="item">
						<tr>
							<td><x:out select="$item/identrada" /></td>
							<td></td>
							<td></td>
							<td><x:out select="$item/lineadetalle/idlineadetalle" /></td>
							<td><x:out select="$item/lineadetalle/producto/nombre" /></td>
							<td><x:out select="$item/entradatornoses/Entradatornos/sesion/contenido/nombre" /></td>
							<td>
								<c:set var="fecha"><x:out select="$item/entradatornoses/Entradatornos/sesion/fecha" /></c:set>
								<fmt:parseDate value="${fecha}" type="DATE" pattern="dd/MM/yyyy"  var="fechaformateada"/>
								<fmt:formatDate value="${fechaformateada}" pattern="dd/MM/yyyy" type="DATE"/>                      			     
							</td>
							<td><x:out select="$item/entradatornoses/Entradatornos/sesion/horainicio" /></td>
							<td><x:out select="$item/entradatornoses/Entradatornos/recinto/nombre" /></td>
							<td><x:out select="$item/entradatornoses/Entradatornos/usos" /></td>
							<td>
								<c:set var="impresion"><x:out select="$item/impresionentradases/Impresionentradas/idimpresionentradas" /></c:set>
	                            <c:choose>
		                            <c:when test="${impresion>0}"><i class="fa fa-check" aria-hidden="true"></i></c:when>
		                            <c:otherwise><i class="fa fa-close" aria-hidden="true"></i></c:otherwise>
	                            </c:choose>								
							</td>
							<td><i class="fa fa-close" aria-hidden="true"></i></td>
						</tr>
							
					</x:forEach>
				</tbody>				
			</table>
		
		
		
			
		<div class="modal-footer">
			<button type="button" class="btn btn-success close_dialog"
				data-dismiss="modal">
				<spring:message code="common.button.cancel" />
			</button>
		</div>

	</div>	
	
	
	

<script>
var dtentradasBono;

$(document).ready(function() {
	dtentradasBono=$('#tab_entradasBono').DataTable( {
		
        "paging":         false, 	
         "info": false,
         "searching": false,
         select: { style: 'os'},
         language: dataTableLanguage
         
         });
})


	//***************************************************************
	$("#tab_entradas_bonos_imprimir").on("click", function(e) {
		
		dtentradasBono.processing(true);
		
		var entradasSeleccionadas = dtentradasBono.rows(".selected").data();
   
		var data=[];
		
		entradasSeleccionadas.each(function (value, index) {
			data.push(entradasSeleccionadas.rows( index ).data()[0][0]); 
		 })
		
		 
		
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/entrada/imprimir.do'/>",
			timeout : 100000,
			data: { data: data.toString() }, 
			success : function(data) {
				//dt_listentradas.ajax.reload();
			},
			error : function(exception) {
				dtentradasBono.processing(false);
				
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",		     
					  delay: 5000,
					  buttons: { sticker: false }
				   });			
			}
		});
		
	})
	
//***************************************************************
$("#tab_entradas_bonos_modificar").on("click", function(e) {
	
	
	var data = dtentradasBono.rows(".selected").data();
	
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	var numUsos=data[0][9];
	showButtonSpinner("#tab_entradas_modificar");
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/ventareserva/busqueda/entrada/modificar.do'/>?tipoBonos=1&numUsos="+numUsos, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "xs");
	});
	})	
	
	
	//**********************
	$("#tab_entradas_bonos_impresiones").on("click", function(e) {
		var data = dtentradasBono.rows(".selected").data();
		
		if (data.length>1) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.alert.seleccion_multiple.listado_impresiones" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}

		if (data.length<=0) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.alert.ninguna_seleccion.listado_impresiones" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}	
		
		showButtonSpinner("#tab_entradas_modificar");
		$("#modal-dialog-form-2 .modal-content").load("<c:url value='/venta/ventareserva/busqueda/Bonos/entradas/impresiones.do'/>?id="+data[0][0], function() {
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "sm");
		});
	})
	
</script>



<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="modal-body">
	<div class="tarjeta" id="tarjeta">	
		<div class ="fotoTarjeta">
			<img id="foto" class="imprimir"></img>
		</div>
		<div class="codBarras" id="barcode1D"></div>			  
			<div class="col-md-6 col-sm-6 col-xs-12">
       			<div id="nombreCompoleto" class="nombrePase"></div> <br/>
       			<div id="fechaCaducidad" class="caducidadPase"></div> <br/>
   			</div>    
   </div> 	 

</div>
  	
<div class="modal-footer">
	<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
		<spring:message code="common.button.cancel"/>
	</button>
</div>	      
  	
  	
<script>
var imageObj;
mostrarPase();

function cargarImagen()
{
	 $("#tarjeta").print();
     $("#modal-dialog-form-5").modal('hide');
}




function mostrarPase()
{
	
	
	var settings = {
	        output:"bmp",
	        bgColor: '#FFFFFF',
	        color: '#000000',
	        barWidth: '1',
	        barHeight:'20',
	        moduleSize: '5',
	        posX: '10',
	        posY: '20',
	        addQuietZone: '1'
	      };
	
	$("#barcode1D").barcode(pase.codigobarras, "code128",settings); 
	$("#barcode1D").src = $("#barcode1D").html()

	setTimeout(cargarImagen, 1000);
	
	
	
	
	$("#nombreCompoleto").text(pase.nombreCompleto);
	$("#fechaCaducidad").text(pase.fechaCaducidad);
	var nombre_foto = pase.nombreFoto;	
	//Actualmente por tema de permisos no se puede acceder ni local ni al servidor 172.29.132.35
	//nombre_foto = "https://lucasleonsimon.files.wordpress.com/2015/03/alfonso-alonso.jpg";	
	
	 $('#foto').attr('src',nombre_foto);
	 $('#foto').width("100%"); 
	 $('#foto').height("100%");
	
	
}



</script>
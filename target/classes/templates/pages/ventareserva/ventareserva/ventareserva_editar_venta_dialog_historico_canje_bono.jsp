<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

 <x:parse xml="${historico_canje_bono}" var="historico_canje_bono_xml" /> 

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico-cenje-bono.title" /> : ${idBono}
	</h4>
</div>


 <div class="modal-body">
	<table id="historico_canje_bono" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
			  <th id="usuario"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico_canje_bono.field.usuario"/></th>
			  <th id="fechaHora"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico_canje_bono.field.fechaHora"/></th>
			  <th id="tipoProducto"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico_canje_bono.field.tipoProducto" /></th>
			  <th id="sesion"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico_canje_bono.field.sesion" /></th>							  						 
			  <th id="numEntrada"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico_canje_bono.field.numEntrada" /></th>
			  <th id="estado"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico_canje_bono.field.estado" /></th>
			  <th id="observaciones"><spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico_canje_bono.field.observaciones" /></th>
			</tr>
		</thead>
		<tbody>				
			<x:forEach select="$historico_canje_bono_xml/ArrayList/Canjebono" var="item">
		   		<tr class="seleccionable">
		   			<td id="usuario"><x:out select="$item/usuario/login" /></td>
					<td id="fechaHora"><x:out select="$item/fechayhoracanje"/></td>						
					<td id="tipoProducto" ><x:out select="$item/lineadetallezonasesion/zonasesion/sesion/contenido/tipoproducto/nombre"/></td>
					<td id="sesion" ><x:out select="$item/lineadetallezonasesion/zonasesion/sesion/contenido/nombre" />-<x:out select="$item/lineadetallezonasesion/zonasesion/sesion/horainicio" /></td> 
					<td id="numEntrada"><x:out select="$item/lineadetallezonasesion/lineadetalle/entradas" /></td>																	
					<td id="estado"><x:out select="$item/anulado" /></td>
					<td id="observaciones"><x:out select="$item/observaciones" /></td>
				</tr>																
			</x:forEach>
		</tbody>				
	</table>			

	<div class="modal-footer">
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.close"/>
		</button>
	</div>		

</div> 

<script>
hideSpinner("#listado_canje");

//**************************************
var dt_historico=$('#historico_canje_bono').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,	
	scrollCollapse: true,
	ordering:  false,
	paging: false,	
    select: { style: 'single' },
    columns: [
              {},              
              {},
              {},
              {},
              {},
              { data: "estado", type: "spanish-string", defaultContent: "",
                  render: function ( data, type, row, meta ) {
                  	if (data!='0') return '<spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico_canje_bono.list.text.anulado" />'; else  return '<spring:message code="venta.ventareserva.tabs.ventareserva.dialog.historico_canje_bono.list.text.no_anulado" />';	
               }},
              {},
              
        ]
    
    
} );
</script>
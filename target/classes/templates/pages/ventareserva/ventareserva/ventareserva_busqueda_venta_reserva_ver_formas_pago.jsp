<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${formas_pago}" var="formas_pago_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">	
		<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.formas_pago.title" />		
	</h4>	
</div>
<div class="modal-body">
	<table id="formaspago_table" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th id="numoperacion"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.formas_pago.field.num_operacion"/></th>
				<th id="importe"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.formas_pago.field.importe"/></th>
				<th id="formapago"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.formas_pago.field.formapago" /></th>
				<th id="numcaja"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.formas_pago.field.numcaja" /></th>							  						 
			</tr>
		</thead>
		<tbody>				
			<x:forEach select="$formas_pago_xml/ArrayList/Importeparcial" var="item">
				<tr class="seleccionable">
					<td id="numoperacion" ><x:out select="$item/venta/operacioncaja/idoperacioncaja" /></td>
					<td id="importe" ><x:out select="$item/importe" /></td>
					<td id="formapago" ><x:out select="$item/formapago/nombre" /></td>
					<td id="numcaja"><x:out select="$item/venta/operacioncaja/caja/idcaja" /></td>																	
				</tr>																
			</x:forEach>
		</tbody>				
	</table>				
	<div class="modal-footer">
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>		

</div>

<script>
hideSpinner("#ver_formas_pago_button");
</script>


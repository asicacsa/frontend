<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2><spring:message code="common.text.filter_list" /></h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">
		<form id="form_busqueda_venta_reserva" class="form-horizontal form-label-left">  
		<!--  <input id="idcliente_busqueda" name="idcliente_busqueda" type="hidden" value="" />-->
		<input id="idUsuario_sesion" name="idUsuario_sesion" type="hidden" value="${sessionScope.idusuario}" />   
	    	<div class="col-md-6 col-sm-6 col-xs-12">	
				<div class="form-group">
					<div class="form-group date-picker">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.foperacion" /></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<a type="button" class="btn btn-default btn-clear-date" id="button_foperacion_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.canje_bono.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
							</a>			
                        	<div class="input-prepend input-group">
                          		<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                          		<input type="text" name="foperacion_busqueda" id="foperacion_busqueda" class="form-control" value="" readonly/>
                          		<input type="hidden" required="required" id = "fechainiciobusquedaoperacion" name="fechainiciobusquedaoperacion" />
                          		<input type="hidden" required="required" id = "fechafinbusquedaoperacion" name="fechafinbusquedaoperacion" />
                        	</div>
						</div>
					</div>
				
		
					<div class="form-group date-picker">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.fvisita" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							  <a type="button" class="btn btn-default btn-clear-date" id="button_fvisita_busqueda_clear">
									<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.canje_bono.list.button.clear" />"> <span class="fa fa-trash"></span>
									</span>
							  </a>			
	                        <div class="input-prepend input-group">
	                          <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	                          <input type="text" name="fvisita_busqueda" id="fvisita_busqueda" class="form-control" value="" readonly/>
	                          <input type="hidden" required="required" id="fechainiciobusquedavisita" name="fechainiciobusquedavisita" value=""/>
	                          <input type="hidden" required="required" id="fechafinbusquedavisita" name="fechafinbusquedavisita" value=""/>
	                        </div>
						</div>
					</div>
				</div>	
			</div>				
			<div class="col-md-6 col-sm-6 col-xs-12">	
				<div class="form-group">					
				 <fieldset id="ventas-reservas">
						<div class="form-group">
							<div class="checkbox col-md-3 col-sm-3 col-xs-12">
								<input type="radio" name="ventas-reservas" id="ventas-reservas-ventas" checked value="1" data-parsley-multiple="ventas" class="flat" style="position: absolute; opacity: 0;">
								<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.ventas" /></strong>
							</div>
							<div class="checkbox col-md-4 col-sm-4 col-xs-12">
								<input type="radio" name="ventas-reservas" id="ventas-reservas-reservas"  value="0" data-parsley-multiple="reservas" class="flat" style="position: absolute; opacity: 0;">
								<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.reservas" /></strong>
							</div>
							<div class="checkbox col-md-5 col-sm-4 col-xs-12">
								<input type="radio" name="ventas-reservas" id="ventas-reservas-ambas" value="" data-parsley-multiple="ambas" class="flat" style="position: absolute; opacity: 0;">
								<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.ambas" /></strong>
							</div>							
						</div>				
					</fieldset>
					<fieldset id="ventas">
						<div class="form-group">
							<div class="checkbox col-md-3 col-sm-3 col-xs-12">
								<input type="radio" name="ventas" id="todas-ventas" checked value="" data-parsley-multiple="ventas" class="flat" style="position: absolute; opacity: 0;">
								<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.todas" /></strong>
							</div>
							<div class="checkbox col-md-4 col-sm-4 col-xs-12">
								<input type="radio" name="ventas" id="anuladas"  value="1" data-parsley-multiple="reservas" class="flat" style="position: absolute; opacity: 0;">
								<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.anuladas" /></strong>
							</div>
							<div class="checkbox col-md-5 col-sm-5 col-xs-12">
								<input type="radio" name="ventas" id="noanuladas" value="0" data-parsley-multiple="ambas" class="flat" style="position: absolute; opacity: 0;">
								<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.noanuladas" /></strong>
							</div>							
						</div>				
					</fieldset>
						<fieldset id="reservas">
						<div class="form-group">
							<div class="checkbox col-md-3 col-sm-3 col-xs-12">
								<input type="radio" name="reservas" id="todas-reservas" checked value="" data-parsley-multiple="ventas" class="flat" style="position: absolute; opacity: 0;">
								<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.todas" /></strong>
							</div>
							<div class="checkbox col-md-4 col-sm-4 col-xs-12">
								<input type="radio" name="reservas" id="individuales"  value="0" data-parsley-multiple="reservas" class="flat" style="position: absolute; opacity: 0;">
								<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.individuales" /></strong>
							</div>
							<div class="checkbox col-md-5 col-sm-5 col-xs-12">
								<input type="radio" name="reservas" id="grupo" value="1" data-parsley-multiple="ambas" class="flat" style="position: absolute; opacity: 0;">
								<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.grupo" /></strong>
							</div>							
						</div>				
					</fieldset>						
				</div>
	      	</div>
	      	<div id="grupo-datos2">	
				<div class="x_title">
					<div class="clearfix"></div>
				</div>				
				<div class="col-md-4 col-sm-4 col-xs-12">	
					<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.refventa" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input name="refventa_busqueda" id="refventa_busqueda" type="text" class="form-control" >
						</div>
					</div>
					<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.refreserva" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input name="refreserva_busqueda" id="refreserva_busqueda" type="text" class="form-control" >
						</div>
					</div>
					<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.refentrada" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input name="refentrada_busqueda" id="refentrada_busqueda" type="text" class="form-control" >
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">	
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.recinto" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<select name="selector_recinto" id="selector_recinto" class="form-control" required="required">
						   		<option value="" ></option>
							</select>
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.producto" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<select name="selector_producto" id="selector_producto" class="form-control" required="required">
						   		<option value="" ></option>
							</select>
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.contenido" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<select name="selector_contenido" id="selector_contenido" class="form-control" required="required">
						   		<option value="" ></option>
							</select>
						</div>						
					</div>
				</div>
				
					<div class="col-md-4 col-sm-4 col-xs-12">	
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.canal" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<select name="selector_canal" id="selector_canal" class="form-control" required="required">
						   		<option value="" ></option>
							</select>
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.usuario" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<select name="selector_usuario" id="selector_usuario" class="form-control" required="required">
						   		<option value="" ></option>
							</select>
						</div>						
					</div>					
				</div>
			</div>	
			
			<div id="grupo-datos3">	
				<div class="x_title">
					<div class="clearfix"></div>
				</div>		
				<div class="col-md-4 col-sm-4 col-xs-12">	
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.puntorecogida" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<select name="selector_puntorecogida" id="selector_puntorecogida" class="form-control" required="required">
						   		<option value="" ></option>
							</select>
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.formaspago" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<select name="selector_formaspago" id="selector_formaspago" class="form-control" required="required">
						   		<option value="" ></option>
							</select>
						</div>						
					</div>
					<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.localizador" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input name="localizador_busqueda" id="localizador_busqueda" type="text" class="form-control" >
						</div>
					</div>
				</div>
				<div class="col-md-5 col-sm-5 col-xs-12">	
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.cliente" /></label>
           					<div class="col-md-2 col-sm-2 col-xs-12">
								<input name="idcliente_busqueda" id="idcliente_busqueda" style="height:10" type="text" class="form-control"><!--  <button id="buscarClienteIndividual">v</button>-->
							</div>
           					<div class="col-md-6 col-sm-6 col-xs-12">
               					<input name="cliente_busqueda" id="cliente_busqueda" type="text" class="form-control" readonly value="">
              				</div>
            		</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.nombreventa" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input name="nombreventa_busqueda" id="nombreventa_busqueda" type="text" class="form-control" >
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.telefono" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input name="telefono_busqueda" id="telefono_busqueda" type="text" class="form-control" >
						</div>				
					</div>			
				</div>					
				<div class="col-md-3 col-sm-3 col-xs-12">	
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.dni" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input name="dni_busqueda" id="dni_busqueda" type="text" class="form-control" >
						</div>				
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.movil" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input name="movil_busqueda" id="movil_busqueda" type="text" class="form-control" >
						</div>						
					</div>					
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="ln_solid"></div>
			<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
				<button id="button_filter_list_busqueda_ventas_reserva" type="button" class="btn btn-success pull-right">
					<spring:message code="common.button.filter" />
				</button>
				<button id="button_clean_venta_reserva" type="button" class="btn btn-success pull-right">
					<spring:message code="venta.ventareserva.tabs.canje-bono.button.clean" />
				</button>
			</div>
			<input type="hidden" name="start" value=""/>
			<input type="hidden" name="length" value="25"/>
	      </form>
</div>	      
<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		
		<a type="button" class="btn btn-info" id="tab_venta_reserva_listado">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.button.listado" />"> <span class="fa fa-mail-reply"></span>
			</span>
		</a>	
		<a type="button" class="btn btn-info" id="tab_venta_reserva_confirmar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.button.reserva" />"> <span class="fa fa-check-square-o"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_venta_reserva_editar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>	
		<a type="button" class="btn btn-info" id="tab_venta_reserva_anular">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.button.anular" />"> <span class="fa fa-times"></span>
			</span>
		</a>			
	</div>

	<table id="datatable-list-venta-reservas_busqueda" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.header.tipo" /></th>
				<th><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.header.referencia" /></th>
				<th><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.header.nif" /></th>
				<th><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.header.cliente_nombre" /></th>
				<th><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.header.producto" /></th>
				<th><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.header.primeravisita" /></th>
				<th><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.header.imptotal" /></th>
				<th><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.header.imppagado" /></th>
				<th><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.header.imppendiente" /></th>
				<th><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.header.anulada" /></th>
				<th><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.header.impreso" /></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
</div>


<script>

var busquedaRealizada= false;

var url;

var dt_listbusqueda=$('#datatable-list-venta-reservas_busqueda').DataTable( {	
	serverSide: true,
	searching: false,
	ordering: false,
	lengthMenu:[[25,50,100],[25,50,100]]
	, "pageLength":25,
	pagingType: "simple",
	info: false,
	deferLoading: 0,
    ajax: {
        url: "<c:url value='/ajax/ventareserva/busqueda/ventareservas/list_venta_reservas_busqueda.do'/>",
        rowId: 'iditem',
        type: 'POST',
        dataSrc: function (json) { 
        	if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Item)); return(""); },
        data: function (params) { 
        	$('#form_busqueda_venta_reserva input[name="start"]').val(params.start);
        	$('#form_busqueda_venta_reserva input[name="length"]').val(params.length);        	
        	return($("#form_busqueda_venta_reserva").serializeObject()); },
       error: function (xhr, error, code)
            {
               	new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : xhr.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}					
				});
            }
    },
    initComplete: function( settings, json ) {
    	$('#datatable-list-venta-reservas_busqueda_length').hide();


    	
    	//$('#datatable-list-venta-reservas_busqueda_length option[value="25"]').attr("selected", "selected");
        $('a#menu_toggle').on("click", function () {if (dt_listbusqueda.data().count()>0) dt_listbusqueda.columns.adjust().draw(); });
	},
    columns: [
		{ data: "tipo", type: "spanish-string" ,  defaultContent:""} ,
		{ data: "iditem", type: "spanish-string" ,  defaultContent:""} ,
		{ data: "nif", type: "spanish-string" ,  defaultContent:""} ,
		{ data: "nombre", type: "spanish-string", defaultContent: "", render: function ( data, type, row, meta ) 
			{			
			if(row.cliente=="")
				return row.nombre;				
			else
				return ""+ row.cliente.nombre+ " "+ row.cliente.apellido1 +" "+row.cliente.apellido2;			
			}
		},
				
		{ data: "lineadetalles.Lineadetalle", className: "cell_centered",
       	  render: function ( data, type, row, meta ) { return showListInCell(data,"<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.text.producto" />","producto.nombre"); }	
        },  
             
		{ data: "primeravisita", type: "spanish-string" ,  defaultContent:"",
			 render: function ( data, type, row, meta ) {			 
	   	  	  return data.substr(0, 10);} 
		 
		} ,
		{ data: "importe", type: "spanish-string" ,  defaultContent:""} ,
		{ data: "importepagado", type: "spanish-string" ,  defaultContent:""} ,
		{ data: "importependiente", type: "spanish-string", defaultContent: "", render: function ( data, type, row, meta ) 
			{
			var importe = row.importe;
			var importepagado = row.importepagado;
			var importeRestante = importe - importepagado;
			return importeRestante.toFixed(2);
			}},
		{ data: "estadooperacion.idestadooperacion", className: "text_icon cell_centered",
			render: function ( data, type, row, meta ) {
		  	  	if (data==4) return '<i class="fa fa-check" aria-hidden="true"></i>'; else return '<i class="fa fa-close" aria-hidden="true"></i>';	
		}},
		{ data: "numeroentradasimpresas", type: "spanish-string", defaultContent: "", render: function ( data, type, row, meta ) 
			{
			var numeroentradasimpresas = ""+row.numeroentradasimpresas;
			var numeroentradassinanular =""+row.numeroentradassinanular;
		
			return (numeroentradasimpresas+"/"+numeroentradassinanular); 
			}},
			{ data: "estadooperacion.idestadooperacion", type: "spanish-string" ,  defaultContent:""} 
		
    ],    
    "columnDefs": [
                   { "visible": false, "targets": [11]}
                 ],
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-venta-reservas_busqueda', 'right');   },
    
    select: { style: 'os', selector:'td:not(:nth-child(5))'},
	language: dataTableLanguage,
	processing: true,
} );

insertSmallSpinner("#datatable-list-venta-reservas_busqueda_processing");

//**********************************************************************
$("#button_filter_list_busqueda_ventas_reserva").on("click", function(e) { 
	busquedaRealizada=true;
	dt_listbusqueda.ajax.reload(); 
})
//**********************************************************************
$("#cliente_busqueda").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>?editable=1", "#cliente_busqueda", "#idcliente_busqueda");
//********************************************************************************
$('input[name="foperacion_busqueda"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
  	  	 $('input[name="foperacion_busqueda"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('input[name="fechainiciobusquedaoperacion"]').val(start.format('DD/MM/YYYY'));  	  	 
  	  	 $('input[name="fechafinbusquedaoperacion"]').val(end.format('DD/MM/YYYY'));
	 });
//*******************************************************************************
$("#button_foperacion_clear").on("click", function(e) {
    $('input[name="foperacion_busqueda"]').val('');
    $('input[name="fechainiciobusquedaoperacion"]').val('');
    $('input[name="fechafinbusquedaoperacion"]').val('');
});
//*******************************************************************************
$('input[name="fvisita_busqueda"]').daterangepicker({
	autoUpdateInput: false,
	linkedCalendars: false,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start,end) {
  	  	 $('input[name="fvisita_busqueda"]').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
  	  	 $('input[name="fechainiciobusquedavisita"]').val(start.format('DD/MM/YYYY'));  	  	 
  	  	 $('input[name="fechafinbusquedavisita"]').val(end.format('DD/MM/YYYY'));
	 });
//*******************************************************************************
$("#button_fvisita_busqueda_clear").on("click", function(e) {
    $('input[name="fvisita_busqueda"]').val('');
    $('input[name="fechainiciobusquedavisita"]').val('');
    $('input[name="fechafinbusquedavisita"]').val('');
});
//**********************************************************************
$("#reservas").hide();
//**********************************************************************
cargarRecintos("#selector_recinto");
cargarProductos("#selector_producto");
cargarContenidos("#selector_contenido");
cargarCanales("#selector_canal");
cargarUsuarios("#selector_usuario");
cargarPuntosRecogida("#selector_puntorecogida");
cargarFormasPago("#selector_formaspago");

//**********************************************************************
$("#ventas-reservas").on("ifChanged", function(e) {	
	
	if($("input[name='ventas-reservas']:checked").val() == 1  || $("input[name='ventas-reservas']:checked").val() == "")
	{
		$("#reservas").hide();		
	}else if($("input[name='ventas-reservas']:checked").val() == 0 )
	{
		$("#reservas").show();		
	}			
})
//**********************************************************************
function cargarRecintos(id) {
	var recinto = $(id);	
    	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/ventareservas/listadoRecintos.do'/>",
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				recinto.find('option').remove();	
				recinto.append('<option selected value=""></option>');
				if (data.ArrayList!="") {
                    var item=data.ArrayList.LabelValue;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	recinto.append('<option value="' + val.value + '">' + val.label + '</option>');
                        });
                    else
                    	recinto.append('<option selected value="' + item.value + '">' + item.label + '</option>');
             }				
				recinto.show();				
			}
		});    
}
//*************************************************
$("#selector_recinto").change(function() {	
	
	var idRecinto= $("#selector_recinto").val();
	$("#selector_producto").find('option').remove();
	cargarProductosPorRecinto(idRecinto,"#selector_producto");
	cargarContenidosPorRecinto(idRecinto,"#selector_contenido");
	
});
//*************************************************
function cargarProductosPorRecinto(idRecinto,id) {
	
	var producto = $(id);  
    	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/ventareservas/listadoProductosPorRecinto.do'/>?idRecinto="+idRecinto,
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				producto.find('option').remove();	
				producto.append('<option selected value=""></option>');
				if (data.ArrayList!="") {
                    var item=data.ArrayList.LabelValue;
                    if (item.length>0)
                    	{
                    	item=sortJSON(item,"label",true);
                        $.each(item, function(key, val){
                        	producto.append('<option value="' + val.value + '">' + val.label + '</option>');
                        });
                    	}
                    else
                    	producto.append('<option value="' + item.value + '">' + item.label + '</option>');
             }				
								
			}
		});
 }
//********************************************************************
function cargarContenidosPorRecinto(idRecinto, id) {
	var contenido = $(id);
      	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/ventareservas/listadoContenidosPorRecinto.do'/>?idRecinto="+idRecinto,
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				contenido.find('option').remove();	
				contenido.append('<option selected value=""></option>');
				if (data.ArrayList!="") {
                    var item=data.ArrayList.LabelValue;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	contenido.append('<option value="' + val.value + '">' + val.label + '</option>');
                        });
                    else
                    	contenido.append('<option value="' + item.value + '">' + item.label + '</option>');
             }				
								
			}
		});
 }
//********************************************************************
$("#selector_producto").change(function() {	
	var idProducto= $("#selector_producto").val();
	$("#selector_contenido").find('option').remove();
	
	cargarContenidosPorProducto(idProducto,"#selector_contenido");
	
});
//********************************************************************
function cargarContenidosPorProducto(idProducto,id) {
	var contenido = $(id);
	
  		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/ventareservas/listadoContenidosPorProducto.do'/>?idProducto="+idProducto,
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				contenido.find('option').remove();	
				contenido.append('<option selected value=""></option>');
				if (data.ArrayList!="") {
                    var item=data.ArrayList.Contenido;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	contenido.append('<option value="' + val.idcontenido + '">' + val.nombre + '</option>');
                        });
                    else
                    	contenido.append('<option value="' + item.idcontenido + '">' + item.nombre + '</option>');
             }				
							
			}
		});
 }
//********************************************************************
function cargarCanales(id) {
	var canal = $(id);
	
    	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/ventareservas/listadoCanales.do'/>",
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				canal.find('option').remove();	
				canal.append('<option selected value=""></option>');
				if (data.ArrayList!="") {
                    var item=data.ArrayList.LabelValue;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	canal.append('<option value="' + val.value + '">' + val.label + '</option>');
                        });
                    else
                    	canal.append('<option selected value="' + item.value + '">' + item.label + '</option>');
             }				
								
			}
		});    
}
//**********************************************************************
function cargarUsuarios(id) {
	var usuario = $(id);
	
    	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/ventareservas/listadoUsuarios.do'/>",
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				usuario.find('option').remove();	
				usuario.append('<option selected value=""></option>');
				if (data.ArrayList!="") {
                    var item=data.ArrayList.LabelValue;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	usuario.append('<option value="' + val.value + '">' + val.login + '</option>');
                        });
                    else
                    	usuario.append('<option selected value="' + item.value + '">' + item.login + '</option>');
             }				
								
			}
		});    
}
//**********************************************************************

$("#selector_canal").change(function() {	
	var idCanal= $("#selector_canal").val();
	$("#selector_usuario").find('option').remove();
	cargarUsuariosPorCanal(idCanal);
	
	
});
//*************************************************
function cargarUsuariosPorCanal(idCanal) {
	var usuario = $('#selector_usuario');
  
    	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/ventareservas/listadoUsuariosCanal.do'/>?idCanal="+idCanal,
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				usuario.find('option').remove();	
				usuario.append('<option selected value=""></option>');
				if (data.ArrayList!="") {
                    var item=data.ArrayList.LabelValue;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	usuario.append('<option value="' + val.value + '">' + val.login + '</option>');
                        });
                    else
                    	usuario.append('<option selected value="' + item.value + '">' + item.login + '</option>');
             }				
								
			}
		});
 }
//********************************************************************
function cargarPuntosRecogida(id) {
	var puntoR = $(id);
	
    	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/ventareservas/listadopuntosrecogida.do'/>",
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				puntoR.find('option').remove();	
				puntoR.append('<option selected value=""></option>');
				if (data.ArrayList!="") {
                    var item=data.ArrayList.LabelValue;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	puntoR.append('<option value="' + val.value + '">' + val.label + '</option>');
                        });
                    else
                    	puntoR.append('<option selected value="' + item.value + '">' + item.label + '</option>');
             }				
								
			}
		});    
}
//********************************************************************
function cargarFormasPago(id) {
	var formaP = $(id);
	
    	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/ventareservas/formasdepago.do'/>",
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				formaP.find('option').remove();	
				formaP.append('<option selected value=""></option>');
				if (data.ArrayList!="") {
                    var item=data.ArrayList.Formapago;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	formaP.append('<option value="' + val.idformapago + '">' + val.nombre + '</option>');
                        });
                    else
                    	formaP.append('<option selected value="' + item.idformapago + '">' + item.nombre + '</option>');
             }				
								
			}
		});    
}
 //****************************************************
 
function cargarProductos(id) {
	var producto = $(id);
	   	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/ventareservas/listadoProductos.do'/>",
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				producto.find('option').remove();	
				producto.append('<option selected value=""></option>');
				if (data.ArrayList!="") {
                    var item=data.ArrayList.LabelValue;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	producto.append('<option value="' + val.value + '">' + val.label + '</option>');
                        });
                    else
                    	producto.append('<option selected value="' + item.value + '">' + item.label + '</option>');
             }				
								
			}
		});    
}
//****************************************************

function cargarContenidos(id) {
	var contenido = $(id);
	
    	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/ventareservas/listadoContenidos.do'/>",
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				contenido.find('option').remove();	
				contenido.append('<option selected value=""></option>');
				if (data.ArrayList!="") {
                    var item=data.ArrayList.Contenido;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	contenido.append('<option value="' + val.idcontenido + '">' + val.nombre + '</option>');
                        });
                    else
                    	contenido.append('<option selected value="' + item.idcontenido + '">' + item.nombre + '</option>');
             }				
								
			}
		});    
}
/***********************************************BOT�N LISTADO  *********************************/
$("#tab_venta_reserva_listado").on("click", function(e) {
	
	//window.open("${url_validacion}","listado");
	
	//Antes de mostrar el listado debe de haber realizado una b�squeda
	if (busquedaRealizada==false) {
	new PNotify({
	      title: '<spring:message code="common.dialog.text.error" />',
	      text: '<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.alert.ninguna_busqueda" />',
		  type: "error",
		  delay: 5000,
		  buttons: { sticker: false }
	   });
	return;
}	

	window.open(url_validacion,"listado");
	
	showButtonSpinner("#tab_venta_reserva_listado");
	
	var temp="";
		
	if ($("#dni_busqueda").val() != "")
		temp="p_identificadoronif="+$("#dni_busqueda").val();
	
	
	if($("input[name='ventas']:checked").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp = temp + "p_anuladas="+$("input[name='ventas']:checked").val();
	}
	
	if($("#fechafinbusquedaoperacion").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp = temp + "p_fechafinventa="+$("#fechafinbusquedaoperacion").val();
	}
	
	if($("#fechainiciobusquedaoperacion").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp=temp+"p_fechaventa="+$("#fechainiciobusquedaoperacion").val();
	}
	
	if($("#fechafinbusquedavisita").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp = temp + "p_fechafinvisita="+$("#fechafinbusquedavisita").val();
	}
	
	if($("#fechainiciobusquedavisita").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp = temp + "p_fechavisita="+$("#fechainiciobusquedavisita").val();
	}
	
	if($("#selector_canal").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp = temp +"p_idcanal="+$("#selector_canal").val();
	}
	
	if($("#idcliente_busqueda").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp = temp + "p_idcliente="+$("#idcliente_busqueda").val();
	}
	
	if($("#selector_producto").val() !=""){
		if (temp!="")
			temp = temp +"&"	
		temp = temp + "p_idproducto="+$("#selector_producto").val();
	}
	
	if($("#selector_contenido").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp = temp + "p_idcontenido="+$("#selector_contenido").val();
	}
		
	if($("#selector_usuario").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp = temp + "p_idusuario="+$("#selector_usuario").val();
	}
	
	if($("#selector_formaspago").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp = temp + "p_idformapago="+$("#selector_formaspago").val();
	}
	
	if($("#nombreventa_busqueda").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp = temp + "p_nombreventa="+$("#nombreventa_busqueda").val();
	}
	
	if($("#refentrada_busqueda").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp = temp + "p_identrada="+$("#refentrada_busqueda").val();
	}
	
	if($("#refreserva_busqueda").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp = temp + "p_idreserva="+$("#refreserva_busqueda").val();
	}
	
	if($("#refventa_busqueda").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp = temp + "p_idventa="+$("#refventa_busqueda").val();
	}
	
	if($("#input[name='ventas-reservas']:checked").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp = temp + "p_ventas="+$("input[name='ventas-reservas']:checked").val();
	}
	
	if($("input[name='ventas-reservas']:checked").val() == 0 ){
		if($("input[name='reservas']:checked").val() != ""){
			if (temp!="")
				temp = temp +"&"
			temp = temp + "p_grupo="+$("input[name='reservas']:checked").val();
		}
	}
	
	if($("#telefono_busqueda").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp = temp + "p_telefono="+$("#telefono_busqueda").val();
	}
	
	if($("#movil_busqueda").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp = temp + "p_telefonomovil="+$("#movil_busqueda").val();
	}
	
	if($("#selector_recinto").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp = temp + "p_idrecinto="+$("#selector_recinto").val();
	}
	
	if($("#localizador_busqueda").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp = temp + "p_localizador="+$("#localizador_busqueda").val();
	}
	
	if($("#selector_puntorecogida").val() !=""){
		if (temp!="")
			temp = temp +"&"
		temp = temp + "p_idpuntosrecogida="+$("#selector_puntorecogida").val();
	}
	
	url = "http://${url_impresion}/jasperserver/flow.html?_flowId=viewReportFlow&reportUnit=/Reports_Aplicacion/Ventas_y_Reservas/BusquedaVentasyReservas&"+temp;
	
	window.setTimeout(AbrirUrl, 1000);

	
	
	hideSpinner("#tab_venta_reserva_listado");
});

function AbrirUrl()
{
	window.open(url,"listado");
}

//******************************************BOT�N CONFIRMAR RESERVAR****************************
$("#tab_venta_reserva_confirmar").on("click", function(e) {
	showButtonSpinner("#tab_venta_reserva_confirmar");
	var data = sanitizeArray(dt_listbusqueda.rows( { selected: true } ).data(),"tipo");
	
	
	if (data.length<=0 ||data.length>1 || data!="R")  {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: ' <spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.alert.confirmar_ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			hideSpinner("#tab_venta_reserva_confirmar");
			return;
		}else 
			{
			var estado = dt_listbusqueda.rows( { selected: true } ).data()[0].estadooperacion.idestadooperacion;
			if (estado!="1")  {
			
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: ' <spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.alert.estado_confirmado" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			hideSpinner("#tab_venta_reserva_confirmar");
			return;
		}
	else
		{			
		hideSpinner("#tab_venta_reserva_confirmar");
		
		showSpinner("#main-busquedas");
			var data = $("#form_busqueda_venta_reserva").serializeObject();

			$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_vender_reserva.do'/>?idcanal=${sessionScope.idcanal}&idreserva="+dt_listbusqueda.rows( { selected: true } ).data()[0].referencia, function() {
				$("#modal-dialog-form-3").modal('show');
				setModalDialogSize("#modal-dialog-form-3", "md");
			});			
		}
	}
});
//******************************************BOT�N ANULAR****************************
$("#tab_venta_reserva_anular").on("click", function(e) {
	
	
	var data = sanitizeArray(dt_listbusqueda.rows( { selected: true } ).data(),"iditem");
	var tipo =sanitizeArray(dt_listbusqueda.rows( { selected: true } ).data(),"tipo");	
	var  importetotalventa =sanitizeArray(dt_listbusqueda.rows( { selected: true } ).data(),"importe");
	var importetotalpagado=sanitizeArray(dt_listbusqueda.rows( { selected: true } ).data(),"importepagado");
	if (data.length<=0 || data.length>1)  {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: ' <spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.alert.anular.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			hideSpinner("#tab_venta_reserva_anular");
			return;
		}else
		{
			//Desde aqu� s�lo se anulan ventas
			if (tipo =="V"){
				showButtonSpinner("#tab_venta_reserva_anular");
				$("#modal-dialog-form .modal-content").load("<c:url value='/venta/ventareserva/busqueda/ventareserva/anular.do'/>?id="+data+"&tipo="+tipo+"&importetotalventa="+importetotalventa+"&importetotalpagado="+importetotalpagado+"&idcanal=${sessionScope.idcanal}", function() {
					$("#modal-dialog-form").modal('show');
					setModalDialogSize("#modal-dialog-form", "sm");
				});
			}	
			else
				{
				showButtonSpinner("#tab_venta_reserva_anular");
				$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/ventareserva/busqueda/ventareservas/show_anular_reserva.do'/>?id="+data+"&tipo="+tipo+"&importetotalventa="+importetotalventa+"&importetotalpagado="+importetotalpagado+"&idcanal=${sessionScope.idcanal}", function() {
					$("#modal-dialog-form").modal('show');
					setModalDialogSize("#modal-dialog-form", "sm");
				});
				}
		}
	
});





/***********************************************BOT�N EDITAR VENTA/RESERVA  *********************************/
$("#tab_venta_reserva_editar").on("click", function(e) {
	
	var data = sanitizeArray(dt_listbusqueda.rows( { selected: true } ).data(),"iditem");
	var tipo =sanitizeArray(dt_listbusqueda.rows( { selected: true } ).data(),"tipo");	
		
	if (data.length<=0 ||data.length>1)  {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: ' <spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.list.alert.editar.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}else
		{		
			showButtonSpinner("#tab_venta_reserva_editar");
			if (tipo =="V"){
				
				$("#modal-dialog-form .modal-content").load("<c:url value='/venta/ventareserva/busqueda/ventareserva/editar_venta.do'/>?id="+data, function() {
					$("#modal-dialog-form").modal('show');
					setModalDialogSize("#modal-dialog-form", "md");
				});
			}else
			{
				//EDITAR RESERVA
			
				$("#modal-dialog-form .modal-content").load("<c:url value='/venta/ventareserva/busqueda/ventareserva/editar_reserva.do'/>?id="+data, function() {
					$("#modal-dialog-form").modal('show');
					setModalDialogSize("#modal-dialog-form", "md");
				});
			}
		}
});


$("#idcliente_busqueda").blur(function() {
	var idCliente =""+$("#idcliente_busqueda").val(); 
	
	if(idCliente!="")
	{
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/facturacion/buscarClientePorId.do'/>",
		timeout : 100000,
		data: {
			idcliente: idCliente,    			
		},
		success : function(data) {    		     
				$("#cliente_busqueda").val(data.Cliente.nombrecompleto); 
				
				
				
		},
		error : function(exception) {    	
			$("#idcliente_busqueda").val("");
	    	$("#cliente_busqueda").val("");
	    	

			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});
	}
	else
		$("#cliente_busqueda").val("");
})


/***********************************************BOT�N LIMPIAR VENTA/RESERVA  *********************************/
$("#button_clean_venta_reserva").on("click", function(e) {
	$("#refventa_busqueda").val("");
	$("#refreserva_busqueda").val("");	
	$("#refentrada_busqueda").val("");
    $('input[name="foperacion_busqueda"]').val('');
    $('input[name="fechainiciobusquedaoperacion"]').val('');
    $('input[name="fechafinbusquedaoperacion"]').val('');
    $('input[name="foperacion_busqueda"]').val('');
    $('input[name="fechainiciobusquedaoperacion"]').val('');
    $('input[name="fechafinbusquedaoperacion"]').val('');
    $('input[name="fvisita_busqueda"]').val('');
    $('input[name="fechainiciobusquedavisita"]').val('');
    $('input[name="fechafinbusquedavisita"]').val('');
    $('input[name="dni_busqueda"]').val('');
    $('input[name="movil_busqueda"]').val('');
    $('input[name="nombreventa_busqueda"]').val('');
    $('input[name="idcliente_busqueda"]').val('');
    $('input[name="cliente_busqueda"]').val('');
    $('input[name="telefono_busqueda"]').val('');
    $('input[name="localizador_busqueda"]').val('');
    
    
    
    $("#refventa_busqueda option:first").prop("selected", "selected");
    $("#selector_recinto option:first").prop("selected", "selected");
    $("#selector_canal option:first").prop("selected", "selected");
    $("#selector_producto option:first").prop("selected", "selected");
    $("#selector_usuario option:first").prop("selected", "selected");
    $("#selector_contenido option:first").prop("selected", "selected");    
    $("#selector_puntorecogida option:first").prop("selected", "selected");
    $("#selector_formaspago option:first").prop("selected", "selected");
    
    
    

    
    
    
})




//***************************************************************************************************
</script>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_venta}" var="editar_venta_xml" />
<x:parse xml="${listado_estados_venta}" var="listado_estados_venta_xml" />
<x:parse xml="${entradas_anuladas}" var="entradas_anuladas_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.venta_resumen.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_venta_reserva_editar_bono_dialog" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	<input type="hidden" name="observaciones_venta" id="observaciones_venta" value="<x:out select = "$editar_venta_xml/Venta/observaciones" />">
	<input type="hidden" name="idcliente_fiscal" id="idcliente_fiscal" value="<x:out select = "$editar_venta_xml/Venta/clientefiscal/idcliente" />">
	<input type="hidden" name="bono_editar_xml" id="venta_xml">
	<input type="hidden" id="idVentaEditarBono" name="idVentaEditarBono" value="<x:out select = "$editar_venta_xml/Venta/idventa" />"/>
	<input type="hidden" id="idventa" name="idventa" value="<x:out select = "$editar_venta_xml/Venta/idventa" />"/>
	<input type="hidden" id="idcanal_venta_bono" name="idcanal_venta_bono" value="<x:out select = "$editar_venta_xml/Venta/canalByIdcanal/idcanal" />"/>
	<input type="hidden" id="idtipo_venta_bono" name="idtipo_venta_bono" value="<x:out select = "$editar_venta_xml/Venta/tipoventa/idtipoventa" />"/>
	
	
	
	
	<input type="hidden" id="descuentoBono" name="descuentoBono" value="<x:out select = "$editar_venta_xml/Venta/porcentajesDescuentoLD/porcentajedescuento" />"/>
	<input type="hidden" id="idCanalEditarBono" name="idCanalEditarBono" value="<x:out select = "$editar_venta_xml/Venta/canalByIdcanal/idcanal" />"/>
	
	
		<div id="columna-izquierda" class="col-md-5 col-sm-12 col-xs-12">		
			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.refreserva" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen">&nbsp;</div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.refventa" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$editar_venta_xml/Venta/idventa" /></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.fechaventa" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$editar_venta_xml/Venta/fechayhoraventa" /></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.usuario" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$editar_venta_xml/Venta/usuario/nombre" />&nbsp;<x:out select = "$editar_venta_xml/Venta/usuario/apellidos" /></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.canal" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$editar_venta_xml/Venta/canalByIdcanal/nombre" /></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.estado" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$editar_venta_xml/Venta/estadooperacion/nombre" /></div>
			</div>
			
			<div class="form-group">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.emision_bono.field.observaciones" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7">
					<textarea class="form-control" rows="3" name="observaciones" id="observaciones" disabled="disabled"><x:out select = "$editar_venta_xml/Venta/observaciones" /></textarea>
				</div>
			</div>	
					
		</div>

		<div id="columna-central" class="col-md-5 col-sm-12 col-xs-12">
		
			<div class="callcenter">
			
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.cliente" /></label>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<input type="text" name="idcliente" id="idcliente" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/cliente/idcliente" />" disabled="disabled"/>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<input type="text" name="nombrecliente" id="nombrecliente" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/cliente/nombre" />"  disabled="disabled"/>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.nombre" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input type="text" name="nombre" id="nombre" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/nombre" />" disabled="disabled"/>
					</div>
				</div>
				
				<div class="form-group button-dialog capa_cp_dialog_venta_individual">
	
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="venta.ventareserva.dialog.venta_individual.field.nif" /></label>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<input type="text" name="nif" id="nif" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/nif" />"  disabled="disabled" />
						</div>
					</div>
				
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.cp" />*</label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<input id="cp_callcenter" name="cp_callcenter" type="text" class="form-control" required="required" value="<x:out select = "$editar_venta_xml/Venta/cp" />" />
						</div>
					</div>
					
				</div>		
				
				
				<div class="form-group">
	
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="venta.ventareserva.dialog.venta_individual.field.telefono" /></label>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<input type="text" name="telefono" id="telefono" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/telefono" />"  />
						</div>
					</div>
				
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.movil" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<input type="text" name="movil" id="movil" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/telefonomovil" />"  />
						</div>
					</div>
					
				</div>		
	
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.email" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input type="text" name="email" id="email" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/email" />"   />
					</div>
				</div>
					
			</div>

			<div id="taquilla" style="display:none;">
			
				<div class="form-group button-dialog capa_cp_dialog_venta_individual">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.cp" /></label>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<input id="cp_taquilla" type="text" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/cp" />" disabled="disabled"/>
					</div>
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.pais" /></label>
					<div class="col-md-2 col-sm-2 col-xs-2">
						<input id="pais_dialog_venta_individual" type="text" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/pais/nombre" />"  disabled="disabled" />
					</div>
				</div>		
			</div>
					
		</div>
		
		<div id="columna-derecha" class="col-md-2 col-sm-12 col-xs-12">
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<span class="fa" id="check-facturada"/>&nbsp;<spring:message code="venta.ventareserva.dialog.venta_resumen.field.facturada" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<span class="fa" id="check-entradas"/>&nbsp;<spring:message code="venta.ventareserva.dialog.venta_resumen.field.entradas" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<span class="fa" id="check-recibo"/>&nbsp;<spring:message code="venta.ventareserva.dialog.venta_resumen.field.recibo" />
				</div>
			</div>
		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">		
			<div class="totales-group totales_venta_resumen">
				Total: <span class="total-lbl"><span id="total-val_venta">0.00</span>&euro;</span>&nbsp;<span class="descuento-lbl">Descuento: <span id="total-desc">0.00</span>&euro;&nbsp;(<span id="total-prc">0.00</span>%)</span>
			</div>			
		</div>	
		
		<div class="btn-group pull-right btn-datatable">
					<a type="button" class="btn btn-info" id="tab_editar_bono_reimprimir">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.button.reimprimir" />"> <span class="fa fa-paint-brush"></span>
						</span>
					</a>
					<a type="button" class="btn btn-info" id="tab_editar_bono_imprimir">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.button.imprimir" />"> <span class="fa fa-print"></span>
						</span>
					</a>
					<a type="button" class="btn btn-info" id="tab_editar_bono_ver_formas_pago">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.button.formas_pago" />"> <span class="fa fa-euro"></span>
						</span>
					</a>
					<a type="button" class="btn btn-info" id="tab_editar_lineas_detalle">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.button.lineas_detalle" />"> <span class="fa fa-list"></span>
						</span>
					</a>
					<a type="button" class="btn btn-info" id="tab_editar_bono_ver">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.button.ver" />"> <span class="fa fa-eye"></span>
						</span>
					</a>
					<a type="button" class="btn btn-info" id="tab_editar_bono_facturar">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.button.facturar" />"> <span class="fa fa-file"></span>
						</span>
					</a>	
					<a type="button" class="btn btn-info" id="tab_busqueda_recibo_impreso_bono">
						<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.dialog.venta_resumen.button.recibo" />"> <span class="fa fa-print"></span>
						</span>
					</a>					
									
		</div>
		
<!-- Pestañas ------------------------------------------------------------->
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 maintab">
		<div class="" role="tabpanel" data-example-id="togglable-tabs">
			<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
				<li role="presentation" class="active">
					<a href="#tab_lineas_detalle_venta" id="lineas_detalle_venta-tab" role="tab" data-toggle="tab" aria-expanded="true">
					<spring:message	code="venta.ventareserva.tabs.busquedas.venta_reserva.dialog.editar_venta.tabs.linea_detalle_venta.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_lineas_detalle_anuladas" role="tab" id="lineas_detalle_anuladas-tab" data-toggle="tab" aria-expanded="false">
					<spring:message	code="venta.ventareserva.tabs.busquedas.venta_reserva.dialog.editar_venta.tabs.linea_detalle_anuladas.title" /></a></li>
						
			</ul>
		</div>
	</div>
</div>

<!-- Area de trabajo ------------------------------------------------------>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel thin_padding">
			<div id="myTabContent" class="tab-content">				
				<div role="tabpanel" class="tab-pane fade active in" name="tab_lineas_detalle_venta" id="tab_lineas_detalle_venta" aria-labelledby="lineas_detalle_venta-tab">
					<table id="datatable-list-detalle-venta" class="table table-striped table-bordered dt-responsive ventareserva" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th></th>			
										<th></th>									
										<th><spring:message code="venta.ventareserva.tabs.editarbono.list.header.producto" /></th>
										<th><spring:message code="venta.ventareserva.tabs.editarbono.list.header.fechaInicio" /></th>
										<th><spring:message code="venta.ventareserva.tabs.editarbono.list.header.fechafin" /></th>
										<th><spring:message code="venta.ventareserva.tabs.editarbono.list.header.tipobono" /></th>
										<th><spring:message code="venta.ventareserva.tabs.editarbono.list.header.etd" /></th>
										<th><spring:message code="venta.ventareserva.tabs.editarbono.list.header.perfil" /></th>
										<th><spring:message code="venta.ventareserva.tabs.editarbono.list.header.importeunitario" /></th>
										<th><spring:message code="venta.ventareserva.tabs.editarbono.list.header.importe" /></th>
									</tr>
								</thead>
							<tbody>
							</tbody>
						</table>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab_lineas_detalle_anuladas" name="tab_lineas_detalle_anuladas" aria-labelledby="lineas_detalle_anuladas-tab">
					<table id="datatable-list-detalles-anuladas" class="table table-striped table-bordered dt-responsive ventareserva" cellspacing="0" width="100%">
									<thead>
										<tr>
										<th></th>		
										<th></th>									
										<th><spring:message code="venta.ventareserva.tabs.editarbono.list.header.producto" /></th>
										<th><spring:message code="venta.ventareserva.tabs.editarbono.list.header.fechaInicio" /></th>
										<th><spring:message code="venta.ventareserva.tabs.editarbono.list.header.fechafin" /></th>
										<th><spring:message code="venta.ventareserva.tabs.editarbono.list.header.tipobono" /></th>
										<th><spring:message code="venta.ventareserva.tabs.editarbono.list.header.etd" /></th>
										<th><spring:message code="venta.ventareserva.tabs.editarbono.list.header.perfil" /></th>
										<th><spring:message code="venta.ventareserva.tabs.editarbono.list.header.importeunitario" /></th>
										<th><spring:message code="venta.ventareserva.tabs.editarbono.list.header.importe" /></th>
										</tr>
									</thead>
								<tbody>
								</tbody>
							</table>
				</div>				
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="save_venta_button" type="submit" class="btn btn-primary save_dialog">
					<spring:message code="common.button.save" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
		
	</form>

</div>


<script>

var venta_json=${editar_venta_json};
//************************************************
var json = ${editar_venta_json};
//************************************************
var tablas_inicializadas=0;
hideSpinner("#tab_venta_bono_editar");

showSpinner("#modal-dialog-form .modal-content");

//************************************************

function ocultar_spinner()
{
	hideSpinner("#modal-dialog-form .modal-content");
}

//************************************************
setTimeout(ocultar_spinner, 3000);

//***************************************************************
var dt_listdetalleventa=$('#datatable-list-detalle-venta').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	  initComplete: function( settings, json ) {
	    	 window.setTimeout(CargarLineasDetalle, 100);  
	    	 window.setTimeout(cargarBonotonesInicio, 1000);
	    	 
	 	  },
	scrollCollapse: true,
	ordering:  false,
	paging: false,	
    select: { style: 'single' },
    columns: [
              {},   
              {},
              {},
              {},
              {},
              {},
              {},
              {},
              {},
              {},              
        ],
        "columnDefs": [
                       { "visible": false, "targets": [0,1]}
                     ],
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('datatable-list-detalle-venta');
   	}
    
} );
//****************************************************************
function CargarLineasDetalle()
	{
		
		var i = 0;
		
		var lineasdetalle = json.Venta.lineadetalles;
				
		if (lineasdetalle !=""){
		if (lineasdetalle.Lineadetalle!="") {
	        var item=lineasdetalle.Lineadetalle;
	        if (item.length>0)
	            $.each(item, function(key, lineadetalle){
	            	CargarLineaDetalle(lineadetalle,dt_listdetalleventa)
	            });
	        else
	        	CargarLineaDetalle(item,dt_listdetalleventa)
	}
	}
	}
		

	//*****************************************************************************************
	function CargarLineaDetalle(lineadetalle, tabla)
	{	
		var bono = lineadetalle.bonosForIdlineadetalle.Bono;
	
		if(bono.length>0)
			bono = bono[0];
		var idbono = bono.idbono;
		var producto = lineadetalle.producto.nombre;
		var fechaInicio = "" + bono.fechaemision;
		fechaInicio = fechaInicio.substring(0,10);
		var fechaFin = ""+ bono.fechacaducidad;
		fechaFin = fechaFin.substring(0,10);
		var tipoBono = bono.tipobono.nombre;
		
		if(""+tipoBono=="Bono Crédito")
			$("#tab_editar_bono_ver_formas_pago").attr("disabled","disabled");
		
		var cantidad = parseFloat(lineadetalle.cantidad);
		var perfil = lineadetalle.perfilvisitante.nombre;
		var importeUnitario = parseFloat(lineadetalle.tarifaproducto.importe);
	
		var importeConDescuentos;
		var descuento="";
		descuento=json.Venta.porcentajesDescuentoLD;
		
		
		
		if (descuento!="undefined")
			descuento = parseFloat(json.Venta.porcentajesDescuentoLD.porcentajedescuento)/ 100.0;
		else
			descuento=0;
		
		var importe = (1.0 - descuento ) * importeUnitario *cantidad;
		importe = importe.toFixed(2);
		
		
	     		
	    if(lineadetalle.tarifaproducto!="")
	    	lineadetalle.tarifaproducto.importe;
	    tabla.row.add([
						lineadetalle,
						idbono,
						producto, 
						fechaInicio,         
						fechaFin, 
						tipoBono, 
						cantidad, 
						perfil, 
						importeUnitario,
					    importe				     
					]);
		
	}

//************************************************************************
	var dt_listdetallesanuladas=$('#datatable-list-detalles-anuladas').DataTable( {
		language: dataTableLanguage,
		info: false,
		searching: false,
		  initComplete: function( settings, json ) {
		    	 window.setTimeout(CargarLineasDetalleAnuladas, 200);    	 
		 	  },
		scrollCollapse: true,
		ordering:  false,
		paging: false,	
	    select: { style: 'single' },
	    columns: [
	              {}, 
	              {},
	              {},
	              {},
	              {},
	              {},
	              {},
	              {},
	              {},
	              {},              
	        ],
	          
	        "columnDefs": [
	                       { "visible": false, "targets": [0,1]}
	                     ],
	    drawCallback: function( settings ) { 
	    	activateTooltipsInTable('datatable-list-detalles-anuladas');
	   	}
	    
	} ); 
	
	//*******************************************************************
	
function CargarLineasDetalleAnuladas()
	{
		
		var i = 0;
		var lineasdetalle = json.Venta.lineadetallesanuladas;
		
		if (lineasdetalle !=""){
		if (lineasdetalle.Lineadetalle!="") {
	        var item=lineasdetalle.Lineadetalle;
	        if (item.length>0)
	            $.each(item, function(key, lineadetalle){
	            	CargarLineaDetalle(lineadetalle,dt_listdetallesanuladas)
	            });
	        else
	        	CargarLineaDetalle(item,dt_listdetallesanuladas)
	}
		}
	}
		



//***************************************************************

function ajustar_cabeceras_datatable()
{
	$('#datatable-list-detalle-venta').DataTable().columns.adjust().draw();
	$('#datatable-list-detalles-anuladas').DataTable().columns.adjust().draw();	
}

//********************************************************************

var fecha_sesion="",
contenido= "",
facturada= Number("<x:out select = "$editar_venta_xml/Venta/facturada" />"),
entradasimpresas= Number("<x:out select = "$editar_venta_xml/Venta/entradasimpresas" />"),
reciboimpreso= Number("<x:out select = "$editar_venta_xml/Venta/reciboimpreso" />");
	
$("#check-facturada").addClass(facturada>0?'fa-check':'fa-close');
$("#check-facturada").css("color",facturada>0?'green':'red');
$("#check-entradas").addClass(entradasimpresas>0?'fa-check':'fa-close');
$("#check-entradas").css("color",entradasimpresas>0?'green':'red');
$("#check-recibo").addClass(reciboimpreso>0?'fa-check':'fa-close');
$("#check-recibo").css("color",reciboimpreso>0?'green':'red');


//********************************************************************************

setTimeout(ajustar_cabeceras_datatable, 3000);
//********************************************************************************
obtener_totales_venta();
var descuento = parseFloat("<x:out select = "$editar_venta_xml/Venta/porcentajesDescuentoLD/porcentajedescuento" />") / 100.0;


//********************************************************************************
function obtener_totales_venta() {
	var porcentaje = "<x:out select = "$editar_venta_xml/Venta/porcentajesDescuentoLD/porcentajedescuento" />";
	var total =  "<x:out select = "$editar_venta_xml/Venta/importetotalventa" />";
	var por=parseFloat(porcentaje);
	var tot=parseFloat(total);
	var i = (por/100);
	var descuento= (tot /(1-i))*i;
	
	$("#total-val_venta").text(tot);
	
	if (por>0){
		$("#total-desc").text(descuento.toFixed(2));
		$("#total-prc").text(por);
	}	
}
//*******************************************************************

//********************************************************************
function cargarBonotonesInicio()
{
	
	if (dt_listdetalleventa.rows().count()>0){
	
	$("#tab_busqueda_informe_resumen_venta").prop("disabled", false);
	  $("#tab_busqueda_historico_cambios").prop("disabled", false);
	  $("#tab_busqueda_observaciones").prop("disabled", false);
	  $("#tab_busqueda_carta_confirmacion").prop("disabled", false);
	  $("#tab_busqueda_usos_impresiones").prop("disabled", false);
	  $("#tab_busqueda_facturar").prop("disabled", false);
	  $("#tab_busqueda_ver_formas_pago").prop("disabled", false);
	  $("#tab_busqueda_modificar_lineas_detalle").prop("disabled", false);
	  $("#tab_busqueda_mostrar_ocultar_num_bono_agencia").show();
	  $("#tab_busqueda_mostrar_ocultar_num_bono_agencia").prop("disabled", false);
	}else
	{
	
		$("#tab_busqueda_informe_resumen_venta").prop("disabled", true);
		  $("#tab_busqueda_historico_cambios").prop("disabled", false);
		  $("#tab_busqueda_observaciones").prop("disabled", false);
		  $("#tab_busqueda_carta_confirmacion").prop("disabled", true);
		  $("#tab_busqueda_facturar").prop("disabled", true);
		  $("#tab_busqueda_usos_impresiones").prop("disabled", false);		  
		  $("#tab_busqueda_ver_formas_pago").prop("disabled", false);
		  $("#tab_busqueda_modificar_lineas_detalle").prop("disabled", false);
		  $("#tab_busqueda_mostrar_ocultar_num_bono_agencia").show();
		  $("#tab_busqueda_mostrar_ocultar_num_bono_agencia").prop("disabled", true);
	}
	}
//************************************************
$("#lineas_detalle_venta-tab").click(function(){
  cargarBonotonesInicio();
});
$("#lineas_detalle_anuladas-tab").click(function(){

	 $("#tab_busqueda_informe_resumen_venta").prop("disabled", true);
	  $("#tab_busqueda_historico_cambios").prop("disabled", false);
	  $("#tab_busqueda_observaciones").prop("disabled", false);
	  $("#tab_busqueda_carta_confirmacion").prop("disabled", true);
	  $("#tab_busqueda_facturar").prop("disabled", true);
	  $("#tab_busqueda_usos_impresiones").prop("disabled", true);
	  $("#tab_busqueda_ver_formas_pago").prop("disabled", false);
	  $("#tab_busqueda_modificar_lineas_detalle").prop("disabled", true);
	  $("#tab_busqueda_mostrar_ocultar_num_bono_agencia").show();
	  $("#tab_busqueda_mostrar_ocultar_num_bono_agencia").prop("disabled", true);
	  
	  
	});
	
$("#entradas_anuladas-tab").click(function(){	  
	
	 $("#tab_busqueda_informe_resumen_venta").prop("disabled", true);
	  $("#tab_busqueda_historico_cambios").prop("disabled", false);
	  $("#tab_busqueda_observaciones").prop("disabled", false);
	  $("#tab_busqueda_carta_confirmacion").prop("disabled", true);
	  $("#tab_busqueda_facturar").prop("disabled", true);
	  $("#tab_busqueda_usos_impresiones").prop("disabled", true);
	  $("#tab_busqueda_ver_formas_pago").prop("disabled", false);
	  $("#tab_busqueda_modificar_lineas_detalle").prop("disabled", true);
	  $("#tab_busqueda_mostrar_ocultar_num_bono_agencia").hide();
	  
	});
//*************************************************
//*******************************************************************
$("#tab_busqueda_informe_resumen_venta").on("click", function(e) {
	window.open("../../jasper.post?idVenta=<x:out select = "$editar_venta_xml/Venta/idventa" />&report=Resumen_Venta","_blank");	
});
//****************************************************************
$("#tab_busqueda_historico_cambios").on("click", function(e) {
	showButtonSpinner("#tab_busqueda_historico_cambios");
	var id="<x:out select = "$editar_venta_xml/Venta/idventa" />";
	
	
	
	
	
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/ventareserva/busqueda/show_historico_cambios_venta.do'/>?id="+id, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "lg");
	});

	
});






//*****************************************************
$("#tab_editar_bono_ver").on("click", function(e) {
	borrarFormularioTabBono();
	var idVenta = $("#idVentaEditarBono").val();
	var data = dt_listdetalleventa.rows( { selected: true } ).data();
	
	$("#idventa").val(idVenta);
	if(data.length>0)
		$("#idlineadetalleVentaBono").val(data[0][0].idlineadetalle);
	
	
	dt_listbono.ajax.reload();
	
	$("#bono-tab").click();
	$("#modal-dialog-form").modal('hide');
})

//****************************************************************
$("#tab_editar_bono_ver_formas_pago").on("click", function(e) {
	showButtonSpinner("#tab_busqueda_ver_formas_pago");
	
	var idVenta = $("#idVentaEditarBono").val();
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/ventareserva/busqueda/show_formas_de_pago_venta.do'/>?idVenta="+idVenta, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "sm");
	});		
		
});

//****************************************************************
$("#tab_editar_lineas_detalle").on("click", function(e) {
	var data = dt_listdetalleventa.rows( { selected: true } ).data();
	

	
	var datos="";
	//Recorro todas las filas para obtener los valores de los id de las lineas de detalle y los id de productos
	
	var data = dt_listdetalleventa.rows().data();
	for(i=0;i<data.length;i++)
	{		
		datos+= data[i][0].idlineadetalle+"-"+data[i][0].producto.idproducto;		
		if (i<data.length-1)
			datos+="|";	}
	

	var idcanal=$("#idcanal_venta_bono").val();
	var idtipoventa=$("#idtipo_venta_bono").val();
	var idcliente=$("#idcliente_fiscal").val();
	var idVentaEditarBono=$("#idVentaEditarBono").val();

	
	 $("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/ventareserva/busqueda/bono/modificarLineas.do'/>?data="+datos+"&idventabono="+idVentaEditarBono+"&idcanal="+idcanal+"&idtipoventa="+idtipoventa+"&idcliente="+idcliente, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "lg");
	});		 
})



//*****************************************************
$("#tab_editar_bono_imprimir").on("click", function(e) {
	var data = dt_listdetalleventa.rows( { selected: true } ).data();
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.ninguna_seleccion.imprimir" />',		      
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	var xml = "<list>";
	
	for(i=0;i<data.length;i++)
	{
	
	xml+= "<int>"+data[i][0].bonosForIdlineadetalle.Bono.idbono+"</int>";
	}
	xml +="</list>";
	
	$("#xmlBonos").val(xml);
	
	var data = $("#form_busqueda_bono").serializeObject();
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/busqueda/bono/imprimir.do'/>",
		timeout : 100000,
		data: data, 
		success : function(data) {
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
	
	
	
})



//*****************************************************
$("#tab_editar_bono_reimprimir").on("click", function(e) {
var data = dt_listdetalleventa.rows( { selected: true } ).data();
	
	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.busqueda.tabs.bono.list.ninguna_seleccion.imprimir" />',		      
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	
	var xml = "<listIdBonos>";
	
	for(i=0;i<data.length;i++)
	{
	
	xml+= "<int>"+data[i][0].bonosForIdlineadetalle.Bono.idbono+"</int>";
	}
	xml +="</listIdBonos>";
	
	
	$("#xmlBonos").val(xml);
	
	var data = $("#form_busqueda_bono").serializeObject();
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/busqueda/bono/reimprimir.do'/>",
		timeout : 100000,
		data: data, 
		success : function(data) {
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		},
		error : function(exception) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	});
	
	
	
})


//*****************************************************
$("#tab_editar_bono_facturar").on("click", function(e) {
var data = dt_listdetalleventa.rows( { selected: true } ).data();
	
	var idVenta = data.idventa;
	var data = $("#form_venta_reserva_editar_bono_dialog").serializeObject();
	
	
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="venta.ventareserva.bono.facturar.alert" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
		  buttons: { closer: false, sticker: false	},
		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
				$.ajax({
					contenttype: "application/json; charset=utf-8",
					type : "post",
					url : "<c:url value='/ajax/ventareserva/busqueda/bono/generar_factura.do'/>",
					timeout : 100000,
					data: data, 
					success : function(data) {
						new PNotify({
							title : '<spring:message code="common.dialog.text.operacion_realizada" />',
							text : '<spring:message code="common.dialog.text.datos_guardados" />',
							type : "success",
							delay : 5000,
							buttons : {
								closer : true,
								sticker : false
							}
						});	
					},
					error : function(exception) {
						new PNotify({
						      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						      text: exception.responseText,
							  type: "error",		     
							  delay: 5000,
							  buttons: { sticker: false }
						   });			
					}
				});
	})
	
})

//*******************************************************************
$("#tab_busqueda_recibo_impreso_bono").on("click", function(e) { 
	showButtonSpinner("#tab_busqueda_recibo_impreso_bono");
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/imprimir_recibo.do'/>",
		timeout : 100000,
		data: {
			idventa: "<x:out select = "$editar_venta_xml/Venta/idventa" />"
		},
		success : function(data) {
			hideSpinner("#tab_busqueda_recibo_impreso_bono");
		},
		error : function(exception) {
			hideSpinner("#tab_busqueda_recibo_impreso_bono");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});		   
}); 



</script>






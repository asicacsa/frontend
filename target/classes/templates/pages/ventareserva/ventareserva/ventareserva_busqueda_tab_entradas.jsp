<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="x_panel filter_list thin_padding">
	<div class="x_title">
		<h2><spring:message code="common.text.filter_list" /></h2>
		<ul class="nav navbar-right panel_toolbox">
			<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
		</ul>
		<div class="clearfix"></div>
	</div>
	<div class="x_content" style="display: block;">
		<form id="form_busqueda_entradas" class="form-horizontal form-label-left">     
	    	<!--  <input type="hidden" name="idcliente_entradas" id="idcliente_entradas" value=""/>-->
	    	<input type="hidden" name="xml_entradas" id="xml_entradas"/>
	    	<div class="col-md-6 col-sm-6 col-xs-12">	
				<div class="form-group">
	      			<div class="form-group date-picker">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.foperacion" /></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<a type="button" class="btn btn-default btn-clear-date" id="button_foperacion_entradas_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.canje_bono.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
						  	</a>			
                        	<div class="input-prepend input-group">
                          		<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                          		<input type="text" name="foperacion-entradas" id="foperacion_entradas" class="form-control" value="" readonly/>                          	
                        	</div>
						</div>
					</div>				
					<div class="form-group date-picker">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.fvisita" /></label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<a type="button" class="btn btn-default btn-clear-date" id="button_fvisita_entradas_clear">
								<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.canje_bono.list.button.clear" />"> <span class="fa fa-trash"></span>
								</span>
						  	</a>			
                        	<div class="input-prepend input-group">
                          		<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                          		<input type="text" name="fvisita-entradas" id="fvisita_entradas" class="form-control" value="" readonly/>
                          	</div>
						</div>
					</div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
				 <fieldset id="ventas-entradas">
						<div class="form-group">
							<div class="checkbox col-md-3 col-sm-3 col-xs-12">
								<input type="radio" name="ventas-entradas" id="entradas-ventas" checked value="1" data-parsley-multiple="ventas" class="flat" style="position: absolute; opacity: 0;">
								<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.ventas" /></strong>
							</div>
							<div class="checkbox col-md-4 col-sm-4 col-xs-12">
								<input type="radio" name="ventas-entradas" id="entradas-reservas"  value="0" data-parsley-multiple="reservas" class="flat" style="position: absolute; opacity: 0;">
								<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.reservas" /></strong>
							</div>
							<div class="checkbox col-md-5 col-sm-5 col-xs-12">
								<input type="radio" name="ventas-entradas" id="entradas-bonos" value="2" data-parsley-multiple="ambas" class="flat" style="position: absolute; opacity: 0;">
								<strong><spring:message code="venta.ventareserva.busqueda.tabs.entradas.field.bonos" /></strong>
							</div>							
						</div>				
					</fieldset>
					<fieldset id="reservas-entradas">
						<div class="form-group">
							<div class="checkbox col-md-6 col-sm-6 col-xs-12">
								<input type="radio" name="reservagrupo" id="individuales_entradas"  value="0" data-parsley-multiple="reservas" class="flat" style="position: absolute; opacity: 0;">
								<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.individuales" /></strong>
							</div>
							<div class="checkbox col-md-6 col-sm-6 col-xs-12">
								<input type="radio" name="reservagrupo" id="grupo_entradas" value="1" data-parsley-multiple="ambas" class="flat" style="position: absolute; opacity: 0;">
								<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.grupo" /></strong>
							</div>							
						</div>				
					</fieldset>						
					
					<fieldset id="entradas">
						<div class="form-group">
							<div class="checkbox col-md-3 col-sm-3 col-xs-12">
								<input type="radio" name="anulada" id="todas-ventas-entadas" checked value="" data-parsley-multiple="ventas" class="flat" style="position: absolute; opacity: 0;">
								<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.todas" /></strong>
							</div>
							<div class="checkbox col-md-4 col-sm-4 col-xs-12">
								<input type="radio" name="anulada" id="anuladas-entadas"  value="1" data-parsley-multiple="reservas" class="flat" style="position: absolute; opacity: 0;">
								<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.anuladas" /></strong>
							</div>
							<div class="checkbox col-md-5 col-sm-5 col-xs-12">
								<input type="radio" name="anulada" id="noanuladas-entadas" value="0" data-parsley-multiple="ambas" class="flat" style="position: absolute; opacity: 0;">
								<strong><spring:message code="venta.ventareserva.tabs.busquedas.field.noanuladas" /></strong>
							</div>							
						</div>				
					</fieldset>
											
				</div>
				
	      	</div>
	      	<div id="grupo-datos2">	
				<div class="x_title">
					<div class="clearfix"></div>
				</div>				
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="form-group">
					<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.refventa" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input name="refventa" id="refventa_entradas" type="text" class="form-control" >
						</div>
					</div>
					<div class="form-group">
					<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.refreserva" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input name="refreserva" id="refreserva_entradas" type="text" class="form-control" >
						</div>
					</div>
					<div class="form-group">
					<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.refentrada" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input name="refentrada" id="refentrada_entradas" type="text" class="form-control" >
						</div>
					</div>

					
					
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.recinto" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<select name="idrecinto" id="selector_recinto_entradas" class="form-control" required="required">
						   		<option value="" ></option>
							</select>
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.producto" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<select name="idProducto" id="selector_producto_entradas" class="form-control" required="required">
						   		<option value="" ></option>
							</select>
						</div>						
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.contenido" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<select name="idContenido" id="selector_contenido_entradas" class="form-control" required="required">
						   		<option value="" ></option>
							</select>
						</div>						
					</div>
	
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.canal" /></label>
							<div class="col-md-8 col-sm-8 col-xs-12">
								<select name="idCanalVenta" id="selector_canal_entradas" class="form-control" required="required">
							   		<option value="" ></option>
								</select>
							</div>						
					</div>
					<div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.usuario" /></label>
							<div class="col-md-8 col-sm-8 col-xs-12">
								<select name="idUsuario" id="selector_usuario_entradas" class="form-control" required="required">
							   		<option value="" ></option>
								</select>
							</div>						
					</div>	
					<div class="form-group">
							<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.formaspago" /></label>
							<div class="col-md-8 col-sm-8 col-xs-12">
								<select name="idformapago" id="selector_formas_pago" class="form-control" required="required">
							   		<option value="" ></option>
								</select>
							</div>						
					</div>					
				</div>
			</div>	
			<div id="grupo-datos3">	
				<div class="x_title">
					<div class="clearfix"></div>
				</div>
				<div class="col-md-5 col-sm-5 col-xs-12">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.canje_bono.field.cliente" /></label>
						<div class="col-md-2 col-sm-2 col-xs-12">
								<input name="idcliente_entradas" id="idcliente_entradas" style="height:10" type="text" class="form-control"><!--  <button id="buscarClienteIndividual">v</button>-->
						</div>
						<div class="col-md-7 col-sm-7 col-xs-12">
							<input name="cliente" id="cliente_entradas" type="text" class="form-control" readonly>
						</div>
					</div>
					
					<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.nombreventa" /></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input name="nombreventa" id="nombreventa_entradas" type="text" class="form-control" >
						</div>
					</div>
					
				</div>		
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.identificadorCliente" /></label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<input name="dniCliente" id="identificadorCliente_entradas" type="text" class="form-control" >
						</div>
					</div>	
					<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.locAgencia" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input name="localizadoragencia" id="locAgencia_entradas" type="text" class="form-control" >
						</div>
					</div>
				</div>	
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.telefono" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input name="telefono" id="telefono_entradas" type="text" class="form-control" >
						</div>
					</div>	
					<div class="form-group">
					<label class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.field.movil" /></label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<input name="movil" id="movil_entradas" type="text" class="form-control" >
						</div>
					</div>	
				</div>	
			</div>
	      	<input type="hidden" name="start" value=""/>
			<input type="hidden" name="length" value="25"/>
	      
	      </form>
	      
		<div class="clearfix"></div>
		<div class="ln_solid"></div>
		<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
			<button id="button_search_entradas" type="button" class="btn btn-success pull-right">
				<spring:message code="common.button.filter" />
			</button>
			<button id="button_clean_entradas" type="button" class="btn btn-success pull-right">
				<spring:message code="venta.ventareserva.tabs.canje-bono.button.clean" />
			</button>
		</div>	      
	</div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">

	<div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_entradas_anular">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.button.anular" />"> <span class="fa fa-times"></span>
			</span>
		</a>	
		<a type="button" class="btn btn-info" id="tab_entradas_modificar">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.button.modificar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_entradas_imprimir">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.button.imprimir" />"> <span class="fa fa-print"></span>
			</span>
		</a>	
		<a type="button" class="btn btn-info" id="tab_entradas_reimprimir">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.button.reimprimir" />"> <span class="fa fa-paint-brush"></span>
			</span>
		</a>	
	</div>

	<table id="datatable-list-entradas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th></th>
				<th><spring:message code="venta.ventareserva.tabs.entradas.list.header.venta" /></th>
				<th><spring:message code="venta.ventareserva.tabs.entradas.list.header.entrada" /></th>
				<th><spring:message code="venta.ventareserva.tabs.entradas.list.header.usuario" /></th>
				<th><spring:message code="venta.ventareserva.tabs.entradas.list.header.fechaHora" /></th>
				<th><spring:message code="venta.ventareserva.tabs.entradas.list.header.usos" /></th>
				<th><spring:message code="venta.ventareserva.tabs.entradas.list.header.bonos" /></th>
				<th><spring:message code="venta.ventareserva.tabs.entradas.list.header.tarifa" /></th>
				<th><spring:message code="venta.ventareserva.tabs.entradas.list.header.descuento" /></th>
				<th><spring:message code="venta.ventareserva.tabs.entradas.list.header.importe" /></th>
				<th id="importePagado"></th>
				<th><spring:message code="venta.ventareserva.tabs.entradas.list.header.formapago" /></th>
				<th><spring:message code="venta.ventareserva.tabs.entradas.list.header.anulada" /></th>
				<th><spring:message code="venta.ventareserva.tabs.entradas.list.header.impreso" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script>





var dt_listentradas =$('#datatable-list-entradas').DataTable( {
	serverSide: true,
	searching: false,
	ordering: false,
	lengthMenu:[[25,50,100],[25,50,100]]
	, "pageLength":25,
	pagingType: "simple",
	info: false,
	deferLoading: 0,
    ajax: {
        url: "<c:url value='/ajax/venta/ventareserva/busqueda/list_entradas.do'/>",
        rowId: 'identrada',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Entrada)); return(""); },
        data: function (params) {
        	$('#form_busqueda_entradas input[name="start"]').val(params.start);
        	$('#form_busqueda_entradas input[name="length"]').val(params.length);
        	return ($("#form_busqueda_entradas").serializeObject());
       	},		        
        error: function (xhr, error, thrown) {
            if (xhr.responseText=="403") {
                  $("#tab_canje_bono").hide();
            }   
            else
            	{
            	new PNotify({
 					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
 					text : xhr.responseText,
 					type : "error",
 					delay : 5000,
 					buttons : {
 						closer : true,
 						sticker : false
 					}					
 				});
            	}
     },
    },
    initComplete: function( settings, json ) {
    	$('#datatable-list-entradas_length').hide();

        $('a#menu_toggle').on("click", function () {dt_listentradas.columns.adjust().draw(); });
	},
    columns: [
	{ data: null, type: "spanish-string", defaultContent: "", render: function ( data, type, row, meta ) { return data; }},
	{ data: "lineadetalle.venta.idventa", type: "spanish-string", defaultContent: ""},
	{ data: "identrada", type: "spanish-string", defaultContent: ""}, 
	{ data: "lineadetalle.venta.usuario.login", type: "spanish-string", defaultContent: "" },		 
	
	{ data: "lineadetalle.lineadetallezonasesions.Lineadetallezonasesion", className: "cell_centered",
     	  render: function ( data, type, row, meta ) { return showListInCell(data,"<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.text.contenido" />","zonasesion.sesion"); }	
           }, 	 
    { data: "entradatornoses", className: "cell_centered",
          	  render: function ( data, type, row, meta ) { 
          		if(data=="")
          		  return "";
          		 data = data.Entradatornos
          		 if (data!="") 
         		  {
                   if (data.length>0)
                   	return data[0].usos; 
                   else
                   	return data.usos;
            	  }
          		  return "";
                }},   
	{ data: "bono.idbono", type: "spanish-string", defaultContent: "" },
		
	{ data: "lineadetalle.perfilvisitante.nombre", type: "spanish-string", defaultContent: "" },
	{ data: "lineadetalle.descuentopromocional", type: "spanish-string", defaultContent: "" },
	{ data: "importe", type: "spanish-string", defaultContent: "" },
	{ data: "venta.importeparcialtotalventa", type: "spanish-string", defaultContent: "" },
	
	{ data: "lineadetalle.perfilvisitante.nombre", className: "cell_centered",
   	  render: function ( data, type, row, meta ) { 
   		  data = row.lineadetalle.venta.importeparcials;
   		  if(data!="")
   			  {
   			  return showListInCell(data,"<spring:message code="venta.ventareserva.tabs.entradas.list.texts.formasPago" />","importeparcial.formapago");
   			  }
   		  else return "";
    }}, 	 
 
	{ data: "anulada", className: "text_icon cell_centered",
		render: function ( data, type, row, meta ) {
	  	  	if (data==1) return '<i class="fa fa-check" aria-hidden="true"></i>'; else return '<i class="fa fa-close" aria-hidden="true"></i>';	
	}},
	{ data: "numimpresiones", type: "spanish-string", defaultContent: "" },
	],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-entradas') },
    select: { style: 'os', selector:'td:not(:nth-child(5),:nth-child(11))'},
	language: dataTableLanguage,
	processing: true,
	"columnDefs": [
                   { "visible": false, "targets": [0,10]}
                 ]
} );

insertSmallSpinner("#datatable-list-entradas_processing");

//**********************************************************************
$("#reservas-entradas").hide();

cargarRecintos("#selector_recinto_entradas");
cargarCanales();
cargarUsuarios();
cargarFormasPago();

//**********************************************************************
function cargarCanales() {
	var canal = $('#selector_canal_entradas');
	
    	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/ventareservas/listadoCanales.do'/>",
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				canal.find('option').remove();	
				canal.append('<option selected value=""></option>');
				if (data.ArrayList!="") {
                    var item=data.ArrayList.LabelValue;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	canal.append('<option value="' + val.value + '">' + val.label + '</option>');
                        });
                    else
                    	canal.append('<option selected value="' + item.value + '">' + item.label + '</option>');
             }				
								
			}
		});    
}

//**********************************************************************
function cargarUsuarios() {
	var usuario = $('#selector_usuario_entradas');
	
    	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/ventareservas/listadoUsuarios.do'/>",
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				usuario.find('option').remove();	
				usuario.append('<option selected value=""></option>');
				if (data.ArrayList!="") {
                    var item=data.ArrayList.LabelValue;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	usuario.append('<option value="' + val.value + '">' + val.label + '</option>');
                        });
                    else
                    	usuario.append('<option selected value="' + item.value + '">' + item.label + '</option>');
             }				
								
			}
		});    
}


//**********************************************************************
function cargarFormasPago() {
	var formaPago = $('#selector_formas_pago');
	
    	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/ventareservas/listadoFormasPago.do'/>",
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				formaPago.find('option').remove();	
				formaPago.append('<option selected value=""></option>');
				if (data.ArrayList!="") {
                    var item=data.ArrayList.LabelValue;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	formaPago.append('<option value="' + val.value + '">' + val.label + '</option>');
                        });
                    else
                    	formaPago.append('<option selected value="' + item.value + '">' + item.label + '</option>');
             }				
								
			}
		});    
}



//*************************************************
$("#selector_recinto_entradas").change(function() {	
	var idRecinto= $("#selector_recinto_entradas").val();
	$("#selector_producto_entradas").find('option').remove();
	cargarProductosPorRecinto(idRecinto,"#selector_producto_entradas");
	cargarContenidosPorRecinto(idRecinto,"#selector_contenido_entradas");
	
});

//********************************************************************
$("#selector_producto_entradas").change(function() {	
	var idProducto= $("#selector_producto_entradas").val();
	$("#selector_contenido_entradas").find('option').remove();
	cargarContenidosPorProducto(idProducto, "#selector_contenido_entradas");
	
});
//********************************************************************

$("#selector_canal_entradas").change(function() {	
	var idCanal= $("#selector_canal_entradas").val();
	$("#selector_usuario").find('option').remove();
	cargarUsuariosPorCanal(idCanal);
	
	
});
//*************************************************
function cargarUsuariosPorCanal(idCanal) {
	var usuario = $('#selector_usuario');
  
    	$.ajax({
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/ventareservas/listadoUsuariosCanal.do'/>?idCanal="+idCanal,
			timeout : 100000,
			data : null,
			dataType : 'json',
			success : function(data) {
				usuario.find('option').remove();	
				usuario.append('<option selected value=""></option>');
				if (data.ArrayList!="") {
                    var item=data.ArrayList.LabelValue;
                    if (item.length>0)
                        $.each(item, function(key, val){
                        	usuario.append('<option value="' + val.value + '">' + val.login + '</option>');
                        });
                    else
                    	usuario.append('<option selected value="' + item.value + '">' + item.login + '</option>');
             }				
				usuario.show();				
			}
		});
 }

	 
$("#button_foperacion_entradas_clear").on("click", function(e) {
	$('#form_busqueda_entradas input[name="foperacion-entradas"]').val('');
});

$("#button_fvisita_entradas_clear").on("click", function(e) {
	$('#form_busqueda_entradas input[name="fvisita-entradas"]').val('');
});


$('input[name="foperacion-entradas"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
	locale: $daterangepicker_sp
});


$('input[name="fvisita-entradas"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
	locale: $daterangepicker_sp
});


$('#form_busqueda_entradas input[name="foperacion-entradas"]').val('');
$('#form_busqueda_entradas input[name="fvisita-entradas"]').val('');



//**********************************************************************
$("input[name='ventas-entradas']").on("ifChanged", function(e) {
		
	if($("input[name='ventas-entradas']:checked").val() == 0 )
	{
		$("#reservas-entradas").show();		
	}
	else
	{
		$("#reservas-entradas").hide();		
	}			
})

//***************************************************************
$("#button_search_entradas").on("click", function(e) {
	dt_listentradas.ajax.reload();
	})

	
//***************************************************************
$("#tab_entradas_modificar").on("click", function(e) {
	var data = dt_listentradas.rows( { selected: true } ).data();
	
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#tab_entradas_modificar");
	
	var numUsos = data[0].entradatornoses.Entradatornos.usos;
	
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/ventareserva/busqueda/entrada/modificar.do'/>?numUsos="+numUsos, function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "xs");
	});
	})

	
	//***************************************************************
	$("#tab_entradas_anular").on("click", function(e) {
		var data = dt_listentradas.rows( { selected: true } ).data();
		var tipo = "V";
		
		if (data.length<=0) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.alert.ninguna_seleccion.anular" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}	
		
		var idVenta = ""+data[0].lineadetalle.venta.idventa;
		var importetotalpagado = data[0].lineadetalle.venta.importeparcialtotalventa;
				
		
		if(idVenta==('undefined'))
			{
			idVenta = data[0].lineadetalle.reserva.idreserva;
			tipo = "R";
			}
		
		
		
		var importe = 0;
		
		for( i=0;i<data.length;i++)
			{
			var idVentaNew = "" + data[i].lineadetalle.venta.idventa;
			importe = importe + data[i].importe;
			if(idVentaNew=='undefined')
				{
				idVentaNew = data[i].lineadetalle.reserva.idreserva;
				}
			
			if(idVentaNew!=idVenta)
				{
				new PNotify({
				      title: '<spring:message code="common.dialog.text.error" />',
				      text: '<spring:message code="venta.ventareserva.busqueda.tabs.entradas.list.alert.distintaVenta" />',
					  type: "error",
					  delay: 5000,
					  buttons: { sticker: false }
				   });
				return;
				}
			}
		
		
		//var itemslineadetalle = data[0].lineadetalle;
		var importetotalventa = importetotalpagado - importe ;
		
		if(tipo == "R")
			{
			importetotalpagado = 0;
			importetotalventa = 0 ;						
			}
		
		showButtonSpinner("#tab_entradas_anular");
		$("#modal-dialog-form .modal-content").load("<c:url value='/venta/ventareserva/busqueda/entradas/cancelar.do'/>?id="+idVenta+"&tipo="+tipo+"&importetotalventa="+importetotalventa+"&importetotalpagado="+importetotalpagado+"&idcanal=${sessionScope.idcanal}", function() {
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "sm");
		});		
		
		
	})

	//***************************************************************
	$("#tab_entradas_imprimir").on("click", function(e) {
		
		dt_listentradas.processing(true);
		
		var data = sanitizeArray(dt_listentradas.rows( { selected: true } ).data(),"identrada");
	   
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/entrada/imprimir.do'/>",
			timeout : 100000,
			data: { data: data.toString() }, 
			success : function(data) {
				dt_listentradas.ajax.reload();
			},
			error : function(exception) {
				dt_listentradas.processing(false);
				
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",		     
					  delay: 5000,
					  buttons: { sticker: false }
				   });			
			}
		});
		
	})
	
	
	//***************************************************************
	$("#tab_entradas_reimprimir").on("click", function(e) {
		
		dt_listentradas.processing(true);
		
		var data = sanitizeArray(dt_listentradas.rows( { selected: true } ).data(),"identrada");
	   
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/entrada/reimprimir.do'/>",
			timeout : 100000,
			data: { data: data.toString() }, 
			success : function(data) {
				dt_listentradas.ajax.reload();
			},
			error : function(exception) {
				dt_listentradas.processing(false);
				
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",		     
					  delay: 5000,
					  buttons: { sticker: false }
				   });			
			}
		});
		
	})	
	
	
 
$("#cliente_entradas").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>?editable=1", "#cliente_entradas", "#idcliente_entradas");

$("#idcliente_entradas").blur(function() {
	var idCliente =""+$("#idcliente_entradas").val(); 
	
	if(idCliente!="")
	{
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/facturacion/buscarClientePorId.do'/>",
		timeout : 100000,
		data: {
			idcliente: idCliente,    			
		},
		success : function(data) {    		     
				$("#cliente_entradas").val(data.Cliente.nombrecompleto); 
				
				
				
		},
		error : function(exception) {    	
			$("#idcliente_entradas").val("");
	    	$("#cliente_entradas").val("");
	    	

			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});
	}
	else
		$("#cliente_entradas").val("");
})


/***********************************************BOT�N EDITAR VENTA/RESERVA  *********************************/
$("#button_clean_entradas").on("click", function(e) {
	
	$("#refventa_entradas").val("");
	$("#refreserva_entradas").val("");
	$("#refentrada_entradas").val("");
	
	$("#foperacion_entradas").val("");
	$("#fvisita_entradas").val("");
	$("#idcliente_entradas").val("");
	$("#cliente_entradas").val("");
	$("#identificadorCliente_entradas").val("");
	
	$("#telefono_entradas").val("");
	$("#nombreventa_entradas").val("");
	$("#locAgencia_entradas").val("");
	$("#movil_entradas").val("");
	
	
	
    
    $("#selector_recinto_entradas option:first").prop("selected", "selected");
    $("#selector_producto_entradas option:first").prop("selected", "selected");
    $("#selector_contenido_entradas option:first").prop("selected", "selected");
    
    $("#selector_canal_entradas option:first").prop("selected", "selected");
    $("#selector_usuario_entradas option:first").prop("selected", "selected");
    $("#selector_formas_pago option:first").prop("selected", "selected");
})





//********************************************************************
</script>
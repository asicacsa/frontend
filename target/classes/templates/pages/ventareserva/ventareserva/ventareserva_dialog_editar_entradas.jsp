<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="modal fade" id="modal_editar_entradas" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close close_dialog" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">�</span>
				</button>
				<h4 class="modal-title">
						<spring:message code="venta.ventareserva.dialog.entradas_editar.title" />			
				</h4>	
			</div>
	<div class="modal-body">
		<form id="form_entradas_editar" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">		
			<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.impr" />*</label>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="numero_imprimir" id="numero_imprimir" data-inputmask="'mask': '9{0,10}'" class="form-control" required="required"/>								
					</div>								
				</div>			    
	
	
			</div>
	
		</form>
	
		<div class="modal-footer">			
			<button id="save_numero_button" type="submit" class="btn btn-primary save_editar_entradas_dialog">
					<spring:message code="common.button.save" />
				</button>
			<button type="button" class="btn btn-cancel close_dialog-editar_entradas">
				<spring:message code="common.button.cancel" />
			</button>
		</div>		
	</div>			
	</div>
</div>
</div>
<script>
	
$(":input").inputmask();
//*********************************************
$(".save_editar_entradas_dialog").on("click", function(e) { 
		$("#form_entradas_editar").submit();			
	})
//*******************************************
$("#form_entradas_editar").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		guardarDatos();
	}
}
);		
//*********************************************
function guardarDatos()
{
	dt_entradas.rows( { selected: true }).data()[0][8]=$("#numero_imprimir").val();
	dt_entradas.rows().invalidate().draw();
	dt_entradas.rows( { selected: true }).data()[0][0].imprimir=$("#numero_imprimir").val();
	
	
	$("#modal_editar_entradas").modal('hide');
	}
//****************************************
$(".close_dialog-editar_entradas").on("click", function(e){
	$("#modal_editar_entradas").modal('hide');
})
</script>
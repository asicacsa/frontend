<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_resumen}" var="ventareserva_resumen_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog close_venta" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.venta_resumen.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_venta_resumen_dialog" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input type="hidden" name="idcliente_fiscal" id="idcliente_fiscal" value="<x:out select = "$ventareserva_resumen_xml/Venta/clientefiscal/idcliente" />">

		<div id="columna-izquierda" class="col-md-5 col-sm-12 col-xs-12">
		
			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.refreserva" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen">&nbsp;</div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.refventa" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$ventareserva_resumen_xml/Venta/idventa" /></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.fechaventa" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$ventareserva_resumen_xml/Venta/fechayhoraventa" /></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.usuario" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$ventareserva_resumen_xml/Venta/usuario/nombre" />&nbsp;<x:out select = "$ventareserva_resumen_xml/Venta/usuario/apellidos" /></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.canal" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$ventareserva_resumen_xml/Venta/canalByIdcanal/nombre" /></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.estado" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$ventareserva_resumen_xml/Venta/estadooperacion/nombre" /></div>
			</div>
					
		</div>

		<div id="columna-central" class="col-md-5 col-sm-12 col-xs-12">
		
			<div class="callcenter">
			
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.cliente" /></label>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<input type="text" name="idcliente" id="idcliente" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/cliente/idcliente" />" disabled="disabled"/>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<input type="text" name="nombrecliente" id="nombrecliente" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/cliente/nombre" />"  disabled="disabled"/>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.nombre" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input type="text" name="nombre" id="nombre" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/nombre" />" disabled="disabled"/>
					</div>
				</div>
				
				<div class="form-group button-dialog capa_cp_dialog_venta_individual">
	
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="venta.ventareserva.dialog.venta_individual.field.nif" /></label>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<input type="text" name="nif" id="nif" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/nif" />"  disabled="disabled" />
						</div>
					</div>
				
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.cp" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<input id="cp_callcenter" type="text" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/cp" />"  disabled="disabled" />
						</div>
					</div>
					
				</div>		
				
				
				<div class="form-group">
	
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="venta.ventareserva.dialog.venta_individual.field.telefono" /></label>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<input type="text" name="telefono" id="telefono" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/telefono" />" disabled="disabled" />
						</div>
					</div>
				
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.movil" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<input type="text" name="movil" id="movil" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/movil" />" disabled="disabled" />
						</div>
					</div>
					
				</div>		
	
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.email" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input type="text" name="email" id="email" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/email" />"  disabled="disabled" />
					</div>
				</div>
					
			</div>

			<div id="taquilla" style="display:none;">
			
				<div class="form-group button-dialog capa_cp_dialog_venta_individual">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.cp" /></label>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<input id="cp_taquilla" type="text" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/cp" />" disabled="disabled"/>
					</div>
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.pais" /></label>
					<div class="col-md-2 col-sm-2 col-xs-2">
						<input id="pais_dialog_venta_individual" type="text" class="form-control" value="<x:out select = "$ventareserva_resumen_xml/Venta/pais/nombre" />"  disabled="disabled" />
					</div>
				</div>		
			</div>
					
		</div>
		
		<div id="columna-derecha" class="col-md-2 col-sm-12 col-xs-12">
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<span class="fa" id="check-facturada"/>&nbsp;<spring:message code="venta.ventareserva.dialog.venta_resumen.field.facturada" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<span class="fa" id="check-entradas"/>&nbsp;<spring:message code="venta.ventareserva.dialog.venta_resumen.field.entradas" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<span class="fa" id="check-recibo"/>&nbsp;<spring:message code="venta.ventareserva.dialog.venta_resumen.field.recibo" />
				</div>
			</div>
		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">
		
			<div class="totales-group totales_venta_resumen">
				Total: <span class="total-lbl"><span id="total-val">0.00</span>&euro;</span>&nbsp;<span class="descuento-lbl">Descuento: <span id="total-desc">0.00</span>&euro;&nbsp;(<span id="total-prc">0.00</span>%)</span>
			</div>
		
			<div class="btn-group pull-right btn-datatable">
				<a type="button" class="btn btn-info" id="button_venta_resumen_entradas">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.dialog.venta_resumen.button.entradas" />"> <span class="fa fa-ticket"></span>
					</span>
				</a>
				<a type="button" class="btn btn-info" id="button_venta_resumen_recibo">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.dialog.venta_resumen.button.recibo" />"> <span class="fa fa-print"></span>
					</span>
				</a>
			</div>
		
			<table id="datatable-list-venta-resumen" class="table table-striped table-bordered dt-responsive ventareserva" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.producto" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.fecha" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.contenido" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.etd" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.perfil" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.descuentos" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.importeunitario" /></th>
						<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.importe" /></th>
						<th><spring:message code="venta.ventareserva.tabs.venta_resumen.list.header.anulada" /></th>
						<th><spring:message code="venta.ventareserva.tabs.venta_resumen.list.header.impreso" /></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>		

		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">
				<button id="checkin_button" type="button" class="btn save_dialog">
					<spring:message code="common.button.checkin" />
				</button>
				<button id="accept_button" type="button" class="btn btn-primary close_dialog" data-dismiss="modal">
					<spring:message code="common.button.accept" />
				</button>
			</div>
		</div>
		
	</form>

</div>

<script>
hideSpinner("#main-ventareserva");

var dt_listventaresumen=$('#datatable-list-venta-resumen').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	scrollCollapse: true,
	ordering:  false,
	paging: false,
    select: { style: 'single' },
	columns: [
  		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{},
		{ className: "cell_centered",
		  render: function ( data, type, row, meta ) {
	       	  	  if (data==1) return '<i class="fa fa-check" style="color: green;" aria-hidden="true"></i>'; 
	       	  	  else return '<i class="fa fa-close" style="color: red;" aria-hidden="true"></i>'
        }},
		{ className: "cell_centered" }
	],
    "columnDefs": [
                   { "visible": false, "targets": [0]}
                 ],
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('datatable-list-venta-resumen');
   	}
} );


function ajustar_cabeceras_datatable()
{
	$('#datatable-list-venta-resumen').DataTable().columns.adjust().draw();
}

/*
 * Funciones para cargar las tablas en las ventas de resumen
 */

 function CargarLineasDetalle()	{
	var json = ${ventareserva_resumen_json};
	var i = 0;
	
	var lineasdetalle = json.Venta.lineadetalles;
			
	if (lineasdetalle !=""){
		if (lineasdetalle.Lineadetalle!="") {
	        var item=lineasdetalle.Lineadetalle;
	        if (item.length>0)
	            $.each(item, function(key, lineadetalle){
	            	CargarLineaDetalle(lineadetalle,dt_listventaresumen)
	            });
	        else
	        	CargarLineaDetalle(item,dt_listventaresumen)
		}
	}
}

function CargarLineaDetalle(lineadetalle, tabla) {	
		
	var fechas= "", contenidos= "", disponibles= "",retorno= false;
	var impreso = lineadetalle.entradasimpresas+"/"+lineadetalle.entradasnoanuladas;
	var sesiones = lineadetalle.lineadetallezonasesions.Lineadetallezonasesion;
	var descuento_nombre=""+lineadetalle.descuentopromocional.nombre;
	var numbonoagencia= lineadetalle.numerobonoagencia;
		
	
	if(descuento_nombre=="undefined")
		descuento_nombre =" ";
	
	var contenidos;
	
	 if (sesiones.length>0)		 
	 {
			for (var i=0; i<sesiones.length; i++) {
				if (retorno) {
					fechas+="</br>";
					contenidos+= "</br>";						
				}				
				var f= sesiones[i].zonasesion.sesion.fecha.split("-")[0];
				fechas+= f;					
				var hi=sesiones[i].zonasesion.sesion.horainicio;
				var  no=sesiones[i].zonasesion.sesion.contenido.nombre;
				contenidos+= hi+" "+no;					
				retorno= true;
			}	 
			
	 }
	 else
	 {
		fechas= lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.fecha.split("-")[0];
		contenidos= lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.horainicio+" "+lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.contenido.nombre;
			
	 }
	 
	tabla.row.add([
		lineadetalle,
	    lineadetalle.producto.nombre, 
		fechas,         
	    contenidos, 
	    lineadetalle.cantidad, 
	    lineadetalle.perfilvisitante.nombre, 
	    descuento_nombre, 
	    lineadetalle.tarifaproducto.importe,
	    lineadetalle.importe, 
	    lineadetalle.anulada, 
	    impreso 
	]);
}

//*****************************************************************************************

var facturada= Number("<x:out select = "$ventareserva_resumen_xml/Venta/facturada" />"),
	entradasimpresas= Number("<x:out select = "$ventareserva_resumen_xml/Venta/entradasimpresas" />"),
	reciboimpreso= Number("<x:out select = "$ventareserva_resumen_xml/Venta/reciboimpreso" />");
	
$("#check-facturada").addClass(facturada>0?'fa-check':'fa-close');
$("#check-facturada").css("color",facturada>0?'green':'red');
$("#check-entradas").addClass(entradasimpresas>0?'fa-check':'fa-close');
$("#check-entradas").css("color",entradasimpresas>0?'green':'red');
$("#check-recibo").addClass(reciboimpreso>0?'fa-check':'fa-close');
$("#check-recibo").css("color",reciboimpreso>0?'green':'red');

CargarLineasDetalle();

$('#datatable-list-venta-resumen').DataTable().columns.adjust().draw();
setTimeout(ajustar_cabeceras_datatable, 3000);
obtener_totales_venta_temporal("venta_resumen","venta_individual",$("#idcliente_venta_individual").val(),4,true);

//********************************************************************************
$("#button_venta_resumen_recibo").on("click", function(e) { 
	showButtonSpinner("#button_venta_resumen_recibo");
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/imprimir_recibo.do'/>",
		timeout : 100000,
		data: {
			idventa: "<x:out select = "$ventareserva_resumen_xml/Venta/idventa" />"
		},
		success : function(data) {
			hideSpinner("#button_venta_resumen_recibo");
		},
		error : function(exception) {
			hideSpinner("#button_venta_resumen_recibo");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});		   
}); 

//********************************************************************************
$(".close_venta").on("click", function(e) {
	if(ventaParcial)	  
	{
    dt_listventaindividual.rows( { selected: true } ).remove().draw();
	}
else
	{
	 $("#idcliente_venta_individual").val("");
    	$("#cliente_venta_individual").val("");
    	$("#cpcliente_venta_individual").val(""); 
		$("#emailcliente_venta_individual").val(""); 
		$("#telefonocliente_venta_individual").val(""); 
		$("#telefonomovilcliente_venta_individual").val("");
		dt_listventaindividual.rows().remove().draw();
	}
    
  
ventaParcial = false;
obtener_totales_venta_temporal("venta_individual","venta_individual",$("#idcliente_venta_individual").val(),4,false);

});

//********************************************************************************
$("#checkin_button").on("click", function(e) {
	 showButtonSpinner("#checkin_button");
	 $("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/venta/ventareserva/show_generar_factura.do?idventa=${idVenta}&idcliente="+$("#idcliente_venta_individual").val()+"'/>", function() {										  
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "xs");
	});
});

borrar_seleccion_lineas();

function borrar_seleccion_lineas()
{
	if(ventaParcial)	  
	{
    dt_listventaindividual.rows( { selected: true } ).remove().draw();
	}
else
	{
	 $("#idcliente_venta_individual").val("");
    	$("#cliente_venta_individual").val("");
    	$("#cpcliente_venta_individual").val(""); 
		$("#emailcliente_venta_individual").val(""); 
		$("#telefonocliente_venta_individual").val(""); 
		$("#telefonomovilcliente_venta_individual").val("");
		dt_listventaindividual.rows().remove().draw();
	}
    
  
ventaParcial = false;
obtener_totales_venta_temporal("venta_individual","venta_individual",$("#idcliente_venta_individual").val(),4,false);

}




//******************************************************************************
$("#button_venta_resumen_entradas").on("click", function(e) { 
	showButtonSpinner("#button_venta_resumen_entradas");
	 $("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/venta/ventareserva/imprimir_entradas.do?idventa=${idVenta}'/>", function() {										  
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "md");
		});
}); 
//****************************************************************************
</script>
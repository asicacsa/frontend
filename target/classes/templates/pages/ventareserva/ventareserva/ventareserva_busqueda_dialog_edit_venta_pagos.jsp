<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${motivosModificacion}" var="motivosModificacion_xml" />
<x:parse xml="${selectorFormasdePagoPorCanal}" var="selectorFormasdePagoPorCanal_xml" />
<x:parse xml="${importesParciales}" var="importesParciales_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">	
				<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.entradas.anular.venta.title" /> 
	</h4>	
</div>

<div class="modal-body">
	<form id="form_save_venta_pagos" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input type="hidden" name="id_anulacion" id="id_anulacion" value="${id}" />
		<input type="hidden" name="xml_lineas_detalle" id="xml_lineas_detalle" />
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
				<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.motivos" /></label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<select name="selector_motivos_anulacion" id="selector_motivos_anulacion" class="form-control" required="required">
						<option value=""></option>
						<x:forEach select="$motivosModificacion_xml/ArrayList/LabelValue" var="item">
							<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
						</x:forEach>
					</select>
				</div>
			</div>	
			<div class="form-group">
				<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.observaciones" /></label>
				<div class="col-md-8 col-sm-8 col-xs-12">
					<textarea name="observacion_anulacion" id="observacion_anulacion" class="form-control"  ></textarea>
				</div>
			</div>				
		</div>
		<div id="grupo-datosimportes">	
			<div class="x_title">
				<div class="clearfix"></div>
			</div>		
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.importetotalventa" /></label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input name="importetotalventa_anulacion" id="importetotalventa_anulacion" type="text" disabled="disabled" class="form-control" value="<x:out select = "$importeTotal" />" >
					</div>
				</div>
				<div class="form-group">
					<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.importetotalpagado" /></label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input name="importetotalpagado_anulacion" id="importetotalpagado_anulacion" type="text" disabled="disabled" class="form-control" value="<x:out select = "$importePagado" />">						
					</div>
				</div>	
				<div class="form-group" id="diferencia" name="diferencia">
					<label  class="control-label col-md-4 col-sm-4 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.diferencia" /></label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input name="diferencia_anulacion" id="diferencia_anulacion" type="text" disabled="disabled" class="form-control" value="<x:out select = "$diferencia" />">						
					</div>
				</div>		
			</div>
		</div>
		<div id="grupo-datoscobro">	
			<div class="x_title">
				<div class="clearfix"></div>
			</div>		
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
						<div class="checkbox col-md-12 col-sm-12 col-xs-12">
							<input type="checkbox" name="devolucion_cobro" id="devolucion_cobro" value="" class="flat" style="position: absolute; opacity: 0;">
							<strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.devolucion_cobro" /></strong>
						</div>
					
					<div class="form-group " id="operacion_anulacion_titulo">
						<label  class="control-label col-md-3 col-sm-3 col-xs-12"><strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.operacion" /></strong></label>
						<label  class="control-label col-md-3 col-sm-3 col-xs-12"><strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.formasdepago" /></strong></label>
						<label  class="control-label col-md-3 col-sm-3 col-xs-12"><strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.importe" /></strong></label>		
					</div>
					<div class="form-group " id="operacion_anulacion_1">
						<div class="col-md-3 col-sm-3 col-xs-12">
							<select name="selector_operacion1" id="selector_operacion1" class="form-control" >
								<option value="0"></option>
								<option value="1"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.cobro" /></option>
								<option value="2"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.devolucion" /></option>								
							</select>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<select name="selector_forma_pago1" id="selector_forma_pago1" class="form-control" >
								<option value=""></option>
									<x:forEach select="$selectorFormasdePagoPorCanal_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
							</select>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input name="importe1" id="importe1" type="text" class="form-control" value="" >							 
						</div>		
						<div class="col-md-1 col-sm-1 col-xs-12">
							<label  class="control-label"><strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.euro" /></strong></label>							 
						</div>
						<div class="col-md-1 col-sm-1 col-xs-12">
							<a type="button" class="btn btn-default btn-new-operacion1" id="button_new_operacion1">
									<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.list.button.nueva_devolucion" />"> <span class="fa fa-plus"></span>
									</span>
							  </a>
						</div>
					</div>
					<div class="form-group " id="operacion_anulacion_2">
							<div class="col-md-3 col-sm-3 col-xs-12">
							<select name="selector_operacion2" id="selector_operacion2" class="form-control" >
								<option value="0"></option>
								<option value="1"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.cobro" /></option>
								<option value="2"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.devolucion" /></option>								
							</select>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<select name="selector_forma_pago2" id="selector_forma_pago2" class="form-control" >
								<option value=""></option>
									<x:forEach select="$selectorFormasdePagoPorCanal_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
							</select>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input name="importe2" id="importe2" type="text"  class="form-control" value="" >							 
						</div>		
						<div class="col-md-1 col-sm-1 col-xs-12">
							<label  class="control-label"><strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.euro" /></strong></label>							 
						</div>
						<div class="col-md-1 col-sm-1 col-xs-12">
							<a type="button" class="btn btn-default btn-new-operacion2" id="button_new_operacion2">
									<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.list.button.nueva_devolucion" />"> <span class="fa fa-plus"></span>
									</span>
							  </a>
						</div>		
					</div>
					<div class="form-group " id="operacion_anulacion_3">
						<div class="col-md-3 col-sm-3 col-xs-12">
							<select name="selector_operacion3" id="selector_operacion3" class="form-control" >
								<option value="0"></option>
								<option value="1"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.cobro" /></option>
								<option value="2"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.devolucion" /></option>								
							</select>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<select name="selector_forma_pago3" id="selector_forma_pago3" class="form-control" >
								<option value=""></option>
									<x:forEach select="$selectorFormasdePagoPorCanal_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out select="$item/label" /></option>
								</x:forEach>
							</select>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input name="importe3" id="importe3" type="text" class="form-control" value="" >							 
						</div>		
						<div class="col-md-1 col-sm-1 col-xs-12">
							<label  class="control-label"><strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.euro" /></strong></label>							 
						</div>
									
					</div>
				</div>				
			</div>
		</div>
			<div >
				<div class="clearfix"></div>
			</div>		
			<div id="checks" class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<div class="checkbox col-md-2 col-sm-2 col-xs-12">
						<input type="checkbox" name="recibo_anular" id="recibo_anular" value="" class="flat" style="position: absolute; opacity: 0;">
						<strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.recibo" /></strong>
					</div>
				
					<div class="checkbox col-md-2 col-sm-2 col-xs-12">
						<input type="checkbox" name="copia_anular" id="copia_anular" value="" class="flat" style="position: absolute; opacity: 0;">
						<strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.copia" /></strong>
					</div>
				
					<div class="checkbox col-md-2 col-sm-2 col-xs-12">
						<input type="checkbox" name="entradas_anular" id="entradas_anular" value="" class="flat" style="position: absolute; opacity: 0;">
						<strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.entradas" /></strong>
					</div>
				
					<div class="checkbox col-md-4 col-sm-4 col-xs-12">
						<input type="checkbox" name="entradas_defecto" id="entradas_defecto" value="" class="flat" style="position: absolute; opacity: 0;">
						<strong><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.estradas_defecto" /></strong>
					</div>
				</div>
			</div>

	<div class="modal-footer">
		<button id="ver_formas_pago_button" type="button" class="btn btn-primary ver_formas_pago_dialog">
			<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.list.button.ver_formas_pago" />
		</button>	
		<button id="save_venta_button" type="submit" class="btn btn-primary anular_venta_reserva_dialog">
					<spring:message code="common.button.accept" />
		</button>		
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel"/>
		</button>
	</div>		
	</form>
</div>
<script>





hideSpinner("#tab_entradas_anular");


$("#operacion_anulacion_titulo").hide();
$("#operacion_anulacion_1").hide();
$("#operacion_anulacion_2").hide();
$("#operacion_anulacion_3").hide();
//$("#diferencia").hide();


$('input[name="copia_anular"]').iCheck('disable');
var importe= parseFloat(($("#diferencia_anulacion").val()).replace(",","."));

if(importe<=0)
	$("#selector_operacion1 option[value=2]").attr("selected",true);
else
	$("#selector_operacion1 option[value=1]").attr("selected",true);


$('input[name="importe1"]').val(importe);

$("#selector_operacion2 option[value=0]").attr("selected",true);
$("#selector_operacion3 option[value=0]").attr("selected",true);


//********************************************************
$('#importe1').inputmask({alias: 'numeric', 
                       allowMinus: false,  
                       digits: 2, 
                       max: 999.99});

//*****************************************************
$('#importe2').inputmask({alias: 'numeric', 
                       allowMinus: false,  
                       digits: 2, 
                       max: 999.99});
//*****************************************************
$('#importe3').inputmask({alias: 'numeric', 
                       allowMinus: false,  
                       digits: 2, 
                       max: 999.99});
//****************************************************
if ($("input.flat")[0]) {
    $(document).ready(function () {
        $('input.flat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });
}
//****************************************************
 
 var contador=0;
 var tipo_forma_pago;
 
 
<x:forEach select="$importesParciales_xml/ArrayList/Importeparcial" var="item">
	contador=contador+1;
	tipo_forma_pago= '<x:out select="$item/formapago/idformapago" />';
</x:forEach> 
if (contador==1)
	$("#selector_forma_pago1 option[value="+tipo_forma_pago+"]").attr("selected",true);

//****************************************************
  $("#devolucion_cobro").on("ifChanged", function(e) {
    	if ($('#operacion_anulacion_titulo').is(':hidden')){ 
    		$("#operacion_anulacion_titulo").show();
    		$("#operacion_anulacion_1").show();
    	}
        else {
        	$("#operacion_anulacion_titulo").hide();
        	$("#operacion_anulacion_1").hide();
        	$("#operacion_anulacion_2").hide();
        	$("#operacion_anulacion_3").hide();
        }
    });
 //***************************************************
 $("#button_new_operacion1").on("click", function(e) {
	 var diferencia = parseFloat($("#diferencia_anulacion").val().replace(",","."));
     
     var importe1 = parseFloat($("#importe1").val());
          
     var importe2;
     
     if ($('select[name=selector_operacion1]').val()=="2")
    	 importe2 = diferencia+importe1;
     else
    	 importe2 = diferencia-importe1;
     

	 $("#importe2").val(importe2);
	
	 if (importe2> 0)
	 	 $("#selector_operacion2 option[value=1]").attr("selected",true);	
	 else 	 
	 	 $("#selector_operacion2 option[value=2]").attr("selected",true);
	 
	
	 $("#operacion_anulacion_2").show();
});

//****************************************************
$("#button_new_operacion2").on("click", function(e) {
	
	 var diferencia = parseFloat($("#diferencia_anulacion").val().replace(",","."));
     
     var importe1 = parseFloat($("#importe1").val());
          
     var importe2 = parseFloat($("#importe2").val());
	
	 //Devolución
	if ($('select[name=selector_operacion1]').val()=="1")
		importe1=(-1.0) * importe1;	
	
	if ($('select[name=selector_operacion2]').val()=="1")
		importe2=(-1.0) * importe2;
	
	resul = diferencia+importe1+importe2;
	
	$("#importe3").val(resul);
	
	if (resul>0)
		$("#selector_operacion3 option[value=2]").attr("selected",true);
	else
		$("#selector_operacion3 option[value=1]").attr("selected",true);
	
	$("#operacion_anulacion_3").show();
	 
});
//****************************************************
$("#recibo_anular").on("ifChanged", function(e) {
	if ($('input[name="copia_anular"]').attr('disabled')) 		
		$('input[name="copia_anular"]').iCheck('enable');
	else{
		$('input[name="copia_anular"]').iCheck('disable');
		$('input[name="copia_anular"]').iCheck('uncheck');
	}		
    });
//****************************************************
$("#ver_formas_pago_button").on("click", function(e) {
	
	showButtonSpinner("#ver_formas_pago_button");
	$("#modal-dialog-form-4 .modal-content").load("<c:url value='/venta/ventareserva/busqueda/entrada/ver_formas_pago.do'/>?id=${id}", function() {
		$("#modal-dialog-form-4").modal('show');
		setModalDialogSize("#modal-dialog-form-4", "xs");
	});
	
	
});
//****************************************************
$("#form_save_venta_pagos").validate({
	onfocusout : false,
	onkeyup : false,
	unhighlight: function(element, errClass) {
        $(element).popover('hide');
	},		
	errorPlacement : function(err, element) {
		err.hide();
		$(element).attr('data-content', err.text());
		$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
		$(element).popover('show');									
	},
	submitHandler : function(form) {
		comprobarTipoFormaPago_no_vacio()	
	}
}
);


var dt_listentradas_anuladas =$('#datatable-list-entradas-anulacion').DataTable();
dt_listentradas_anuladas.rows().remove().draw();


//****************************************************
function actualizarLineasDetalle(){			
		showSpinner("#modal-dialog-form .modal-content");
		
		var xml;
		xml="<Modificacion>";
		xml = xml+"<idmodificacion/>";
		xml = xml+"<motivomodificacion>"+$("#selector_motivos_anulacion option:selected").text()+"</motivomodificacion>";
		xml = xml+"<observaciones>"+$("#observacion_anulacion").val()+"</observaciones>";
		xml = xml+"<venta>";
			xml = xml+"<idventa>"+$("#id_anulacion").val()+"</idventa>";
			var entr=false;
			if(	$("#entradas_anular").is(':checked'))
				entr=true;
			xml = xml+"<entradasimpresas>"+entr+"</entradasimpresas>";
			
			var valor;
			if(	$("#recibo_anular").is(':checked')){
				if ($("#copia_anular").is(':checked')){
					valor=2;				
				}else valor=1
			}else valor=0;
				
			xml = xml+"<reciboimpreso>"+valor+"</reciboimpreso>";		
			var entr_def=false;
			if(	$("#entradas_defecto").is(':checked'))
				entr_def=true;
			xml = xml+"<imprimirSoloEntradasXDefecto>"+entr_def+"</imprimirSoloEntradasXDefecto>";		
		xml = xml+"</venta>";
		
		xml = xml+"<reserva>";
			xml = xml+"<idreserva/>";
		xml = xml+"</reserva>";
		xml = xml+"<modificacionimporteparcials>";
		if(	$("#devolucion_cobro").is(':checked')){
			xml = xml+"<Modificacionimporteparcial>";
			xml = xml+"<importeparcial>";
				xml = xml+"<formapago>";
					xml = xml+"<idformapago>"+$("#selector_forma_pago1").val()+"</idformapago>";
				xml = xml+"</formapago>";
				xml = xml+"<bono>";
					xml = xml+"<numeracion/>";
				xml = xml+"</bono>";				
				if ($("#selector_operacion1").val()==2)
					xml = xml+"<importe>"+"-"+$("#importe1").val()+"</importe>";
				else if($("#selector_operacion1").val()==1)
					xml = xml+"<importe>"+$("#importe1").val()+"</importe>";
				else
					if($("#selector_forma_pago1").val()!=0)
						xml = xml+"<importe>NaN</importe>";	
				xml = xml+"<rts/>";
				xml = xml+"<numpedido/>";
				xml = xml+"<anulado/>";
			xml = xml+"</importeparcial>";
		xml = xml+"</Modificacionimporteparcial>";
		xml = xml+"<Modificacionimporteparcial>";
			xml = xml+"<importeparcial>";
				xml = xml+"<formapago>";
					xml = xml+"<idformapago>"+$("#selector_forma_pago2").val()+"</idformapago>";
				xml = xml+"</formapago>";
				xml = xml+"<bono>";
					xml = xml+"<numeracion/>";
				xml = xml+"</bono>";
			
				if ($("#selector_operacion2").val()==2)
					xml = xml+"<importe>"+"-"+$("#importe2").val()+"</importe>";
				else if($("#selector_operacion2").val()==1)
					xml = xml+"<importe>"+$("#importe2").val()+"</importe>";
				else
					if($("#selector_forma_pago2").val()!=0)
						xml = xml+"<importe>NaN</importe>";
				xml = xml+"<rts/>";
				xml = xml+"<numpedido/>";
				xml = xml+"<anulado/>";
			xml = xml+"</importeparcial>";
		xml = xml+"</Modificacionimporteparcial>";
		xml = xml+"<Modificacionimporteparcial>";
			xml = xml+"<importeparcial>";
				xml = xml+"<formapago>";
					xml = xml+"<idformapago>"+$("#selector_forma_pago3").val()+"</idformapago>";
				xml = xml+"</formapago>";
				xml = xml+"<bono>";
					xml = xml+"<numeracion/>";
				xml = xml+"</bono>";
				if ($("#selector_operacion3").val()==2)
					xml = xml+"<importe>"+"-"+$("#importe3").val()+"</importe>";
				else if($("#selector_operacion3").val()==1)
					xml = xml+"<importe>"+$("#importe3").val()+"</importe>";
				else
					if($("#selector_forma_pago3").val()!=0)
						xml = xml+"<importe>NaN</importe>";
				xml = xml+"<rts/>";
				xml = xml+"<numpedido/>";
				xml = xml+"<anulado/>";
			xml = xml+"</importeparcial>";
		xml = xml+"</Modificacionimporteparcial>";
	xml = xml+"</modificacionimporteparcials>";
		}else
		{
			xml = xml+"<Modificacionimporteparcial>";
			xml = xml+"<importeparcial>";
				xml = xml+"<formapago>";
					xml = xml+"<idformapago></idformapago>";
				xml = xml+"</formapago>";
				xml = xml+"<bono>";
					xml = xml+"<numeracion/>";
				xml = xml+"</bono>";
				xml = xml+"<importe></importe>";
				xml = xml+"<rts/>";
				xml = xml+"<numpedido/>";
				xml = xml+"<anulado/>";
			xml = xml+"</importeparcial>";
		xml = xml+"</Modificacionimporteparcial>";
		xml = xml+"<Modificacionimporteparcial>";
			xml = xml+"<importeparcial>";
				xml = xml+"<formapago>";
					xml = xml+"<idformapago></idformapago>";
				xml = xml+"</formapago>";
				xml = xml+"<bono>";
					xml = xml+"<numeracion/>";
				xml = xml+"</bono>";
				xml = xml+"<importe></importe>";
				xml = xml+"<rts/>";
				xml = xml+"<numpedido/>";
				xml = xml+"<anulado/>";
			xml = xml+"</importeparcial>";
		xml = xml+"</Modificacionimporteparcial>";
		xml = xml+"<Modificacionimporteparcial>";
			xml = xml+"<importeparcial>";
				xml = xml+"<formapago>";
					xml = xml+"<idformapago></idformapago>";
				xml = xml+"</formapago>";
				xml = xml+"<bono>";
					xml = xml+"<numeracion/>";
				xml = xml+"</bono>";
				xml = xml+"<importe></importe>";
				xml = xml+"<rts/>";
				xml = xml+"<numpedido/>";
				xml = xml+"<anulado/>";
			xml = xml+"</importeparcial>";
		xml = xml+"</Modificacionimporteparcial>";
	xml = xml+"</modificacionimporteparcials>";
		}
					
	var xml_lineasdetalle = construir_xml_lineasdetalle_pagos(${nombretabla}.rows().data());
	xml = xml+xml_lineasdetalle;
	xml = xml+"<anulaciondesdecaja/>";
	xml = xml+"</Modificacion>";
	$("#xml_lineas_detalle").val(xml);
	var data = $("#form_save_venta_pagos").serializeObject();
			
		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/ventareserva/busqueda/ventareservas/actualizarLineasDetalle.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {				
				$("#modal-dialog-form-3").modal('hide');
				$("#modal-dialog-form-2").modal('hide');
				

				//Dependiendo de si la venta es de bonos o de una venta normal:
				var nombre="${nombretabla}";
				actualizarResumen("${id}");
				
				var strEntradas = ""+data.ok.ArrayList;
				if(strEntradas!="")
					{
					var dt_listentradas_anuladas =$('#datatable-list-entradas-anulacion').DataTable();
					var entradas = data.ok.ArrayList.Entrada;
					if(data.ok.ArrayList.Entrada.length>=1)
						$.each(entradas, function(key, entrada){
							anadirEntrada(entrada);
			            });
					else						
						anadirEntrada(entradas);
					$("#anulacion_entradas").modal('show');
					}
					
					
								
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");
	
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});		
		
		
	}
	
function anadirEntrada(entrada)
{
	dt_listentradas_anuladas.row.add([
										entrada.identrada,
										entrada.lineadetalle.producto.nombre,
										entrada.codigobarras 
									]).draw();							
}
	
	
	
//*****************************************************
function construir_xml_lineasdetalle_pagos(lineas_detalle_sel) {
	
	var xml_lineasdetalle= "<modificacionlineadetalles>";
	xml_lineasdetalle+= "<Modificacionlineadetalle>";
	xml_lineasdetalle+= "<lineadetalle>";
	xml_lineasdetalle+= "<idlineadetalle/>";
	xml_lineasdetalle+= "</lineadetalle>";
	xml_lineasdetalle+= "</Modificacionlineadetalle>";

	
	//Se diferencia entre grabar la modificación de la linea de detalles de una venta de bono o de una venta normal
	var apertura_lineadetalle="";
	var cierre_lineadetalle="";
	var nombre="${nombretabla}";
	if (nombre=="dt_listemisionbono"){
		apertura_lineadetalle="<lineadetallebono>";
		cierre_lineadetalle="</lineadetallebono>";
	}else{
		apertura_lineadetalle="<lineadetalle>";
		cierre_lineadetalle="</lineadetalle>";
	}
	
	
	for (var i=0; i<lineas_detalle_sel.length; i++) { 
		xml_lineasdetalle+="<Modificacionlineadetalle>";
		xml_lineasdetalle+="<tipomodificacion>0</tipomodificacion>";
		xml_lineasdetalle+=apertura_lineadetalle;
		var lineaLoop = lineas_detalle_sel[i][0];
		var strLineaNueva = ""+lineaLoop.Lineadetalle;
		if(strLineaNueva!='undefined')
			lineaLoop = lineaLoop.Lineadetalle;
		xml_lineasdetalle+=json2xml(lineaLoop,"");
		xml_lineasdetalle+=cierre_lineadetalle;
		xml_lineasdetalle+="</Modificacionlineadetalle>";
	}
	
	
	for(i=0;i<lineasDetallesEliminadas.length;i++)
	   {
		lineaDetalle = lineasDetallesEliminadas[i];
		if (nombre=="dt_listemisionbono")
			{
			xml_lineasdetalle+="<Modificacionlineadetalle>";
			xml_lineasdetalle+="<tipomodificacion>1</tipomodificacion>";
			xml_lineasdetalle+="<lineadetallebono>";
			xml_lineasdetalle+="<idlineadetalle>"+lineaDetalle.idlineadetalle+"</idlineadetalle>";
			xml_lineasdetalle+="<idTipoBono>"+lineaDetalle.idTipoBono+"</idTipoBono>";
			xml_lineasdetalle+="<fechaIniVigencia>"+lineaDetalle.fechaFinVigencia+"</fechaIniVigencia>";
			xml_lineasdetalle+="<fechaFinVigencia>"+lineaDetalle.fechaIniVigencia+"</fechaFinVigencia>";
			xml_lineasdetalle+="</lineadetallebono>";
			xml_lineasdetalle+="</Modificacionlineadetalle>";
			}
		else
			{
			xml_lineasdetalle+="<Modificacionlineadetalle>";
			xml_lineasdetalle+="<tipomodificacion>1</tipomodificacion>";
			xml_lineasdetalle+="<lineadetalle><idlineadetalle>"+lineaDetalle.idlineadetalle+"</idlineadetalle></lineadetalle>";
			xml_lineasdetalle+="</Modificacionlineadetalle>";
			}
	   }
	    
	

	xml_lineasdetalle+="</modificacionlineadetalles>";
	
	return(xml_lineasdetalle);
}
//*****************************************************
function comprobarTipoFormaPago_no_vacio(){	
	if(	$("#devolucion_cobro").is(':checked')){
		if ($("#importe1").val()==""){
			if($("#selector_forma_pago1").val()!=0){
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : '<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.text.campos_obligatorios_importe" />',
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				return false;
			}
		}else
		{
			if($("#selector_forma_pago1").val()==0){
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : '<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.text.campos_obligatorios_forma_pago" />',
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				return false;
			}
		}
		
		if ($("#importe2").val()==""){
			if($("#selector_forma_pago2").val()!=0){
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : '<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.text.campos_obligatorios_importe" />',
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				return false;
			}
		}else
		{
			if($("#selector_forma_pago2").val()==0){
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : '<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.text.campos_obligatorios_forma_pago" />',
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				return false;
			}
		}
		
		if ($("#importe3").val()==""){
			if($("#selector_forma_pago3").val()!=0){
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : '<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.text.campos_obligatorios_importe" />',
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				return false;
			}
		}else
		{
			if($("#selector_forma_pago3").val()==0){
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : '<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.text.campos_obligatorios_forma_pago" />',
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				return false;
			}
		}		
		
		  actualizarLineasDetalle();
	}else{		
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.text.ninguna_seleccion_operacion" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
	  		  buttons: { closer: false, sticker: false	},
	  		  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
			   actualizarLineasDetalle();
		   }).on('pnotify.cancel', function() {
			   return false;
		   });	
		}
	}
//****************************************************
</script>
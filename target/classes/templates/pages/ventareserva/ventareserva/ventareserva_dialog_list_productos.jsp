<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.combinados.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_listar_productos" class="form-horizontal form-label-left">
	
		<div class="form-group">
	
			<div id="tabla-productos" class="col-md-12 col-sm-12 col-xs-12">
			
				<div class="col-md-12 col-sm-12 col-xs-12">
					<table id="datatable-list-productos" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th></th>
								<th><spring:message code="venta.ventareserva.dialog.combinados.list.header.producto" /></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					<span>&nbsp;</span>
				</div>
			</div>
		</div>
		
	</form>

	<div class="modal-footer">
		<button id="button_listar_productos_aceptar" type="button" class="btn btn-primary save_dialog">
			<spring:message code="common.button.accept" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>
	
</div>

<script>

hideSpinner("#button_${tabla}_descombinar");
showSpinner('#tabla-productos');

var productos= $("#datatable_list_${tabla}").DataTable().rows( { selected: true } ).data();

var dt_listproductos=$('#datatable-list-productos').DataTable( {
	language: dataTableLanguage,
	processing: false,
	scrollCollapse: true,
	paging: false,
	info: false,
	searching: false,
    select: { style: 'single'},
    ajax: {
        url: "<c:url value='/ajax/venta/ventareserva/list_productos.do'/>",
        rowId: 'idproducto',
        type: 'POST',
        data: {
        	idcombinado: productos[0][1]
        },        
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Producto)); return(""); }
    },
    initComplete: function( settings, json ) {
        $('a#menu_toggle').on("click", function () {if (dt_listproductos.data().count()>0) dt_listproductos.columns.adjust().draw(); });
        hideSpinner('#tabla-productos');
	},
	columnDefs: [
	             { "targets": 0, "visible": false }
	         ],
    columns: [
      	{ data: null, type: "spanish-string", defaultContent: "", render: function ( data, type, row, meta ) { return data; }},
        { data: "nombre", type: "spanish-string", defaultContent: ""}
    ],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-productos') }
} );

//********************************************************************************
function obtenerIdTarifa(tarifas,idperfilvisitante) {
	for (var i=0; i<tarifas.length; i++) 
		if (tarifas[i].idperfilvisitante==idperfilvisitante) return(tarifas[i].tarifa.idtarifa); 
	return("");
}

//********************************************************************************
$("#button_listar_productos_aceptar").on("click", function(e) {
	
	showButtonSpinner("#button_listar_productos_aceptar");
	
	var lineasdetalle= $("#datatable_list_${tabla}").DataTable().rows( { selected: true } ).data();
	
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/combinar_productos.do'/>",
		timeout : 100000,
		data: {
			idcliente: $("#idcliente_${tabla}").val(),
			idcanal: "${sessionScope.idcanal}",
			idtipoventa: 4, // Tipo venta No Numerada
			xml: construir_xml_lineasdetalle(lineasdetalle),
			producto: "<Producto>"+json2xml(dt_listproductos.rows( { selected: true } ).data()[0],"")+"</Producto>"
		},
		success : function(data) {
			
			var fechas= "", contenidos= "", disponibles= "",retorno= false;
			var sesiones= data.Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion;
			for (var i=0; i<sesiones.length; i++) {
				if (typeof sesiones[i].zonasesion.idzonasesion=="undefined") {
					hideSpinner("#button_listar_productos_aceptar");
					new PNotify({
					      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					      text: '<spring:message code="venta.ventareserva.list.alert.combinados.nulo" />',
						  type: "error",
						  delay: 5000,
						  buttons: { closer:true, sticker: false }			  
					});
					$("#modal-dialog-form").modal('hide');
					return;
				}
				if (retorno) {
					fechas+="</br>";
					contenidos+= "</br>";
					disponibles+= "</br>";
				}
				fechas+= sesiones[i].zonasesion.sesion.fecha.split("-")[0],
				contenidos+= sesiones[i].zonasesion.sesion.horainicio+" "+sesiones[i].zonasesion.sesion.contenido.nombre;
				disponibles+= sesiones[i].zonasesion.numlibres;
				retorno= true;
			}
			var bono= "";
			${list}.rows( { selected: true } ).remove().draw();
			sesion_data= data;
			sesion_data.Lineadetalle.importe= sesion_data.Lineadetalle.tarifaproducto.importe; // Al deshacer la combinación de productos me lo devuelve vacío por lo que lo relleno a mano.
			${list}.row.add([sesion_data,
			                 sesion_data.Lineadetalle.producto.idproducto, 
			                 sesion_data.Lineadetalle.producto.nombre,
			                 fechas,
			                 contenidos,
			                 disponibles,
			                 sesion_data.Lineadetalle.cantidad,  // Nº Etd
				             typeof sesion_data.Lineadetalle.perfilvisitante.nombre=="undefined"?"":sesion_data.Lineadetalle.perfilvisitante.nombre, // Perfil/Tarifa
				             "", // Descuentos
				             sesion_data.Lineadetalle.tarifaproducto.importe, // I. Unitario
				             sesion_data.Lineadetalle.importe,  // Importe total
				             "", // Datos de las sesiones
				             bono, // Bono
				             typeof sesion_data.Lineadetalle.perfiles.Perfilvisitante=="undefined"?"":obtenerIdTarifa(sesion_data.Lineadetalle.perfiles.Perfilvisitante,sesion_data.Lineadetalle.perfilvisitante.idperfilvisitante),
				             "",  // iddescuento
				             sesion_data.Lineadetalle.perfilvisitante.idperfilvisitante   // idperfil
			                ]).draw();
			obtener_totales_venta_temporal("${tabla}","${tabla}",$("#idcliente_${tabla}").val(),4);
			hideSpinner("#button_listar_productos_aceptar");
			$("#modal-dialog-form").modal('hide');
		},
		error : function(exception) {
			hideSpinner("#button_listar_productos_aceptar");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});
});

</script>


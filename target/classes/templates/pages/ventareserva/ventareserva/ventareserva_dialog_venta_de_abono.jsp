
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_venta}" var="editar_venta_xml" />
<x:parse xml="${listado_estados_venta}" var="listado_estados_venta_xml" />
<x:parse xml="${entradas_anuladas}" var="entradas_anuladas_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="venta.ventareserva.dialog.venta_resumen.title" />
	</h4>
</div>

<div class="modal-body">

	<form id="form_venta_reserva_editar_venta_dialog" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	<input type="hidden" name="observaciones_venta" id="observaciones_venta" value="<x:out select = "$editar_venta_xml/Venta/observaciones" />">
	<input type="hidden" name="idcliente_fiscal" id="idcliente_fiscal" value="<x:out select = "$editar_venta_xml/Venta/clientefiscal/idcliente" />">
	<input type="hidden" name="venta_xml" id="venta_xml">
	<input type="hidden" name="idVentaEditar" id="idVentaEditar" value="<x:out select = "$editar_venta_xml/Venta/idventa" />">
	<input type="hidden" name="fechaOperacion" id="fechaOperacion" value="<x:out select = "$editar_venta_xml/Venta/fechayhoraventa" />">
	<input type="hidden" id="idTipoVenta" name="idTipoVenta" value="<x:out select = "$editar_venta_xml/Venta/tipoventa/idtipoventa" />"/>
	<input type="hidden" id="idsesion_venta" name="idsesion_venta">
	<input type="hidden" id="idlineadetalle_venta" name="idlineadetalle_venta">
	<input type="hidden" id="idcanal_venta" name="idcanal_venta" value="<x:out select = "$editar_venta_xml/Venta/canalByIdcanal/idcanal" />">
	<input type="hidden" name="cp_callcenter" id="cp_callcenter" value="<x:out select = "$editar_venta_xml/Venta/cp" />">
	

		<div id="columna-izquierda" class="col-md-5 col-sm-12 col-xs-12">		
			

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.fechaventa" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$editar_venta_xml/Venta/fechayhoraventa" /></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.usuario" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$editar_venta_xml/Venta/usuario/nombre" />&nbsp;<x:out select = "$editar_venta_xml/Venta/usuario/apellidos" /></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.canal" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$editar_venta_xml/Venta/canalByIdcanal/nombre" /></div>
			</div>

			<div class="col-md-12 col-sm-12 col-xs-12">
				<label class="control-label col-md-5 col-sm-5 col-xs-5"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.estado" /></label>
				<div class="col-md-7 col-sm-7 col-xs-7 texto-resumen"><x:out select = "$editar_venta_xml/Venta/estadooperacion/nombre" /></div>
			</div>
					
		</div>

		<div id="columna-central" class="col-md-5 col-sm-12 col-xs-12">
		
			<div class="callcenter">
			
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_resumen.field.cliente" /></label>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<input type="text" name="idcliente" id="idcliente" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/cliente/idcliente" />" disabled="disabled"/>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<input type="text" name="nombrecliente" id="nombrecliente" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/cliente/nombre" />"  disabled="disabled"/>
					</div>
				</div>

					
				
				<div class="form-group">
	
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-6 col-sm-6 col-xs-6"><spring:message code="venta.ventareserva.dialog.venta_individual.field.telefono" /></label>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<input type="text" name="telefono" id="telefono" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/telefono" />"  />
						</div>
					</div>
				
					<div class="col-md-6 col-sm-6 col-xs-6">
						<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.movil" /></label>
						<div class="col-md-9 col-sm-9 col-xs-9">
							<input type="text" name="movil" id="movil" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/telefonomovil" />"  />
						</div>
					</div>
					
				</div>		
	
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.email" /></label>
					<div class="col-md-9 col-sm-9 col-xs-9">
						<input type="text" name="email" id="email" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/email" />"   />
					</div>
				</div>
					
			</div>

			<div id="taquilla" style="display:none;">
			
				<div class="form-group button-dialog capa_cp_dialog_venta_individual">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.cp" /></label>
					<div class="col-md-4 col-sm-4 col-xs-4">
						<input id="cp_taquilla" type="text" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/cp" />" disabled="disabled"/>
					</div>
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="venta.ventareserva.dialog.venta_individual.field.pais" /></label>
					<div class="col-md-2 col-sm-2 col-xs-2">
						<input id="pais_dialog_venta_individual" type="text" class="form-control" value="<x:out select = "$editar_venta_xml/Venta/pais/nombre" />"  disabled="disabled" />
					</div>
				</div>		
			</div>
					
		</div>
		
		<div id="columna-derecha" class="col-md-2 col-sm-12 col-xs-12">
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<span class="fa" id="check-facturada"/>&nbsp;<spring:message code="venta.ventareserva.dialog.venta_resumen.field.facturada" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<span class="fa" id="check-recibo"/>&nbsp;<spring:message code="venta.ventareserva.dialog.venta_resumen.field.recibo" />
				</div>
			</div>
		</div>
		
		<div class="col-md-12 col-sm-12 col-xs-12">		
			<div class="totales-group totales_venta_resumen">
				Total: <span class="total-lbl"><span id="total-val">0.00</span>&euro;</span>&nbsp;<span class="descuento-lbl">Descuento: <span id="total-desc">0.00</span>&euro;&nbsp;(<span id="total-prc">0.00</span>%)</span>
			</div>			
		</div>	
		
		<div class="btn-group pull-right btn-datatable">
			
			<a type="button" class="btn btn-info" id="tab_busqueda_ver_formas_pago">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.dialog.editar_venta.list.button.formas_pago" />"> <span class="fa fa-euro"></span>
				</span>
			</a>	
			<a type="button" class="btn btn-info" id="tab_busqueda_modificar_lineas_detalle">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.dialog.editar_venta.list.button.lineas_detalle" />"> <span class="fa fa-list"></span>
				</span>
			</a>	
			<a type="button" class="btn btn-info" id="tab_busqueda_facturar">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="common.button.checkin" />"> <span class="fa fa-calculator"></span>
				</span>
			</a>						
			<a type="button" class="btn btn-info" id="tab_busqueda_recibo_impreso">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.dialog.venta_resumen.button.recibo" />"> <span class="fa fa-print"></span>
				</span>
			</a>			
		</div>
		
<!-- Pestañas ------------------------------------------------------------->
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 maintab">
		<div class="" role="tabpanel" data-example-id="togglable-tabs">
			<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
				<li role="presentation" class="active">
					<a href="#tab_lineas_detalle_venta" id="lineas_detalle_venta-tab" role="tab" data-toggle="tab" aria-expanded="true">
					<spring:message	code="venta.ventareserva.tabs.busquedas.venta_reserva.dialog.editar_venta.tabs.linea_detalle_venta.title" /></a></li>
				<li role="presentation" class="">
					<a href="#tab_lineas_detalle_anuladas" role="tab" id="lineas_detalle_anuladas-tab" data-toggle="tab" aria-expanded="false">
					<spring:message	code="venta.ventareserva.tabs.busquedas.venta_reserva.dialog.editar_venta.tabs.linea_detalle_anuladas.title" /></a></li>
								
			</ul>
		</div>
	</div>
</div>

<!-- Area de trabajo ------------------------------------------------------>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel thin_padding">
			<div id="myTabContent" class="tab-content">				
				<div role="tabpanel" class="tab-pane fade active in" name="tab_lineas_detalle_venta" id="tab_lineas_detalle_venta" aria-labelledby="lineas_detalle_venta-tab">
					<table id="datatable-list-detalle-venta" class="table table-striped table-bordered dt-responsive ventareserva" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th></th>										
										<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.producto" /></th>
										<th><spring:message code="venta.tabs.abono_edit.text.abonado" /></th>
										<th><spring:message code="venta.tabs.abono_edit.text.fecha_inicio" /></th>
										<th><spring:message code="venta.tabs.abono_edit.text.fecha_fin" /></th>
										<th><spring:message code="venta.tabs.abono_edit.text.tipo_abono" /></th>
										<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.perfil" /></th>
										<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.importeunitario" /></th>
										<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.importe" /></th>
										
									</tr>
								</thead>
							<tbody>
							</tbody>
						</table>
				</div>
				<div role="tabpanel" class="tab-pane fade" id="tab_lineas_detalle_anuladas" name="tab_lineas_detalle_anuladas" aria-labelledby="lineas_detalle_anuladas-tab">
					<table id="datatable-list-detalles-anuladas" class="table table-striped table-bordered dt-responsive ventareserva" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th></th>										
											<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.producto" /></th>
											<th><spring:message code="venta.tabs.abono_edit.text.abonado" /></th>
											<th><spring:message code="venta.tabs.abono_edit.text.fecha_inicio" /></th>
											<th><spring:message code="venta.tabs.abono_edit.text.fecha_fin" /></th>
											<th><spring:message code="venta.tabs.abono_edit.text.tipo_abono" /></th>
											<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.perfil" /></th>
											<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.importeunitario" /></th>
											<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.importe" /></th>
										</tr>
									</thead>
								<tbody>
								</tbody>
							</table>
				</div>				
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="modal-footer">				
				<button id="save_venta_button" type="submit" class="btn btn-primary save_dialog">
					<spring:message code="common.button.accept" />
				</button>
				<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
					<spring:message code="common.button.cancel" />
				</button>
			</div>
		</div>
		
	</form>

</div>


<script>

var venta_json=${editar_venta_json};
//************************************************

hideSpinner("#tab_venta_reserva_editar");

showSpinner("#modal-dialog-form .modal-content");

//************************************************

function ocultar_spinner()
{
	hideSpinner("#modal-dialog-form .modal-content");
}

//************************************************
setTimeout(ocultar_spinner, 3000);

//***************************************************************
var dt_listdetalleventa=$('#datatable-list-detalle-venta').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	  initComplete: function( settings, json ) {
	    	 window.setTimeout(CargarLineasDetalle, 100);  
	    	 window.setTimeout(cargarBonotonesInicio, 1000);
	    	 
	 	  },
	scrollCollapse: true,
	ordering:  false,
	paging: false,	
    select: { style: 'single' },
    columns: [
              {},              
              {},
              {},
              {},
              {},
              {},
              {},
              {},
              {}              
        ],
        "columnDefs": [
                       { "visible": false, "targets": [0]}
                     ],
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('datatable-list-detalle-venta');
   	}
    
} );
//****************************************************************
function CargarLineasDetalle()
	{
		var json = ${editar_venta_json};
		
		
		var i = 0;
		
		var lineasdetalle = json.Venta.lineadetalles;
		
		
				
		if (lineasdetalle !=""){
			if (lineasdetalle.Lineadetalle!="") {
				var item=lineasdetalle.Lineadetalle;
				if (item.length>0)
		            $.each(item, function(key, lineadetalle){
		            	CargarLineaDetalle(lineadetalle,dt_listdetalleventa)
		            });
		        else
	       			CargarLineaDetalle(item,dt_listdetalleventa)
			}
			
		}
	}
		

	//*****************************************************************************************
	function CargarLineaDetalle(lineadetalle, tabla)
	{	
		var fechas= "", contenidos= "", disponibles= "",retorno= false;
		var impreso = lineadetalle.entradasimpresas+"/"+lineadetalle.entradasnoanuladas;
		var sesiones = lineadetalle.lineadetallezonasesions.Lineadetallezonasesion;
		var descuento_nombre=""+lineadetalle.descuentopromocional.nombre;
		var numbonoagencia= lineadetalle.numerobonoagencia;
			
		
		if(descuento_nombre=="undefined")
			descuento_nombre =" ";
		
		var contenidos;
		
 
 		var fecha_inicio = lineadetalle.abonosForIdlineadetalle.Abono.fechaemision.substring(0,10);
 		
 
 		var fecha_fin = lineadetalle.abonosForIdlineadetalle.Abono.fechacaducidad.substring(0,10);
 			
		tabla.row.add([
			lineadetalle,
		    lineadetalle.producto.nombre, 
		    lineadetalle.abonosForIdlineadetalle.Abono.clienteclub.cliente.nombrecompleto,         
		    fecha_inicio, 
		    fecha_fin, 
		    lineadetalle.abonosForIdlineadetalle.Abono.tipoabono.nombre, 
		    lineadetalle.perfilvisitante.nombre, 
		    lineadetalle.tarifaproducto.importe,
		    lineadetalle.importe				   
		]).draw();
		
			
}

//************************************************************************
	var dt_listdetallesanuladas=$('#datatable-list-detalles-anuladas').DataTable( {
		language: dataTableLanguage,
		info: false,
		searching: false,
		  initComplete: function( settings, json ) {
		    	 window.setTimeout(CargarLineasDetalleAnuladas, 200);    	 
		 	  },
		scrollCollapse: true,
		ordering:  false,
		paging: false,	
	    select: { style: 'single' },
	    columns: [	              
	              {},              
	              {},
	              {},
	              {},
	              {},
	              {},
	              {},
	              {},
	              {} 
	        ],
	        "columnDefs": [
	                       { "visible": false, "targets": [0]}
	                     ],
	    drawCallback: function( settings ) { 
	    	activateTooltipsInTable('datatable-list-detalles-anuladas');
	   	}
	    
	} ); 
	
	//*******************************************************************
	
function CargarLineasDetalleAnuladas()
	{
		var json = ${editar_venta_json};
		var i = 0;
		
		
		var lineasdetalle = json.Venta.lineadetallesanuladas;
		
		if (lineasdetalle !=""){
		if (lineasdetalle.Lineadetalle!="") {
	        var item=lineasdetalle.Lineadetalle;
	        if (item.length>0)
	            $.each(item, function(key, lineadetalle){
	            	CargarLineaDetalle(lineadetalle,dt_listdetallesanuladas)
	            });
	        else
	        	CargarLineaDetalle(item,dt_listdetallesanuladas)
			}
		}
	}
		

//****************************************************************

var dt_listentradasanuladas =$('#datatable-list-entradas-anuladas').DataTable( {
	ordering: false,
	info: false,
	deferLoading: 0,
	searching: false,
	paging: false,
	
    ajax: {
        url: "<c:url value='/ajax/ventareserva/busqueda/ventareservas/listado_entradasanuladas.do'/>?idventa=<x:out select = "$editar_venta_xml/Venta/idventa" />",
        rowId: 'identrada',
        type: 'POST',
        dataSrc: function (json) { if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.ArrayList.Entrada)); return(""); },
               
       
    },
    initComplete: function( settings, json ) {
    	
        $('a#menu_toggle').on("click", function () {dt_listentradasanuladas.columns.adjust().draw(); });
	},
    columns: [
	
	{ data: "lineadetalle.venta.idventa", type: "spanish-string", defaultContent: ""},
	{ data: "identrada", type: "spanish-string", defaultContent: ""}, 
	{ data: "lineadetalle.venta.usuario.login", type: "spanish-string", defaultContent: "" },		 
	{ data: "lineadetalle.lineadetallezonasesions.Lineadetallezonasesion", className: "cell_centered",
 	  render: function ( data, type, row, meta ) { return showListInCell(data,"<spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.text.contenido" />","zonasesion.sesion"); }	
    }, 	 
	{ data: "entradatornoses", className: "cell_centered",
      	  render: function ( data, type, row, meta ) {
      		  
      		  return "";
            }},   
	{ data: "bono", type: "spanish-string", defaultContent: "" },	
	{ data: "lineadetalle.perfilvisitante.nombre", type: "spanish-string", defaultContent: "" },
	{ data: "lineadetalle.descuentopromocional", type: "spanish-string", defaultContent: "" },
	{ data: "importe", type: "spanish-string", defaultContent: "" },
	{ data: "numimpresiones", className: "cell_centered",
	  render: function ( data, type, row, meta ) { 
		  data = row.lineadetalle.venta.importeparcials;
		  if(data!="")
			  {
			  return showListInCell(data,"<spring:message code="venta.ventareserva.tabs.entradas.list.texts.formasPago" />","importeparcial.formapago");
			  }
		  else return "";
	}},
	{ data: "anulada", className: "text_icon cell_centered",
	render: function ( data, type, row, meta ) {
  	  	if (data==1) return '<i class="fa fa-check" aria-hidden="true"></i>'; else return '<i class="fa fa-close" aria-hidden="true"></i>';	
	}},
	{ data: "numimpresiones", type: "spanish-string", defaultContent: "" },
	],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-entradas-anuladas') },
    select: { style: 'os', selector:'td:not(:nth-child(4),:nth-child(10))'},
   language: dataTableLanguage,
	processing: true
} );

//********************************************************************

var fecha_sesion="",
contenido= "",
facturada= Number("<x:out select = "$editar_venta_xml/Venta/facturada" />"),
entradasimpresas= Number("<x:out select = "$editar_venta_xml/Venta/entradasimpresas" />"),
reciboimpreso= Number("<x:out select = "$editar_venta_xml/Venta/reciboimpreso" />");
	
$("#check-facturada").addClass(facturada>0?'fa-check':'fa-close');
$("#check-facturada").css("color",facturada>0?'green':'red');
$("#check-entradas").addClass(entradasimpresas>0?'fa-check':'fa-close');
$("#check-entradas").css("color",entradasimpresas>0?'green':'red');
$("#check-recibo").addClass(reciboimpreso>0?'fa-check':'fa-close');
$("#check-recibo").css("color",reciboimpreso>0?'green':'red');

//********************************************************************************
obtener_totales_venta();
//********************************************************************************
function obtener_totales_venta() {
	var porcentaje = "<x:out select = "$editar_venta_xml/Venta/porcentajesDescuentoLD/porcentajedescuento" />";
	var total =  "<x:out select = "$editar_venta_xml/Venta/importetotalventa" />";
	var por=parseFloat(porcentaje);
	var tot=parseFloat(total);
	var i = (por/100);
	var descuento= ((tot /(1-i))*i).toFixed(2);
	
	$("#total-val").text(tot);
	
	if (por>0){
		$("#total-desc").text(descuento);
		$("#total-prc").text(por);
	}	
}
//*******************************************************************
function cargarBonotonesInicio()
{
	
	if (dt_listdetalleventa.rows().count()>0){
	
	$("#tab_busqueda_informe_resumen_venta").prop("disabled", false);
	  $("#tab_busqueda_historico_cambios").prop("disabled", false);
	  $("#tab_busqueda_observaciones").prop("disabled", false);
	  $("#tab_busqueda_carta_confirmacion_venta").prop("disabled", false);
	  $("#tab_busqueda_usos_impresiones").prop("disabled", false);
	  $("#tab_busqueda_facturar").prop("disabled", false);
	  $("#tab_busqueda_ver_formas_pago").prop("disabled", false);
	  $("#tab_busqueda_modificar_lineas_detalle").prop("disabled", false);
	  $("#tab_busqueda_mostrar_ocultar_num_bono_agencia").show();
	  $("#tab_busqueda_mostrar_ocultar_num_bono_agencia").prop("disabled", false);
	}else
	{
		
		$("#tab_busqueda_informe_resumen_venta").prop("disabled", true);
		  $("#tab_busqueda_historico_cambios").prop("disabled", false);
		  $("#tab_busqueda_observaciones").prop("disabled", false);
		  $("#tab_busqueda_carta_confirmacion_venta").prop("disabled", true);
		  $("#tab_busqueda_facturar").prop("disabled", true);
		  $("#tab_busqueda_usos_impresiones").prop("disabled", false);		  
		  $("#tab_busqueda_ver_formas_pago").prop("disabled", false);
		  $("#tab_busqueda_modificar_lineas_detalle").prop("disabled", false);
		  $("#tab_busqueda_mostrar_ocultar_num_bono_agencia").show();
		  $("#tab_busqueda_mostrar_ocultar_num_bono_agencia").prop("disabled", true);
	}
	}
//************************************************
$("#lineas_detalle_venta-tab").click(function(){
  cargarBonotonesInicio();
});

//*************************************************
$("#lineas_detalle_anuladas-tab").click(function(){

	 $("#tab_busqueda_informe_resumen_venta").prop("disabled", true);
	  $("#tab_busqueda_historico_cambios").prop("disabled", false);
	  $("#tab_busqueda_observaciones").prop("disabled", false);
	  $("#tab_busqueda_carta_confirmacion_venta").prop("disabled", true);
	  $("#tab_busqueda_facturar").prop("disabled", true);
	  $("#tab_busqueda_usos_impresiones").prop("disabled", true);
	  $("#tab_busqueda_ver_formas_pago").prop("disabled", false);
	  $("#tab_busqueda_modificar_lineas_detalle").prop("disabled", true);
	  $("#tab_busqueda_mostrar_ocultar_num_bono_agencia").show();
	  $("#tab_busqueda_mostrar_ocultar_num_bono_agencia").prop("disabled", true);
	  
	  
	});

//*****************************************************************
$("#entradas_anuladas-tab").click(function(){	  
	
	 $("#tab_busqueda_informe_resumen_venta").prop("disabled", true);
	  $("#tab_busqueda_historico_cambios").prop("disabled", false);
	  $("#tab_busqueda_observaciones").prop("disabled", false);
	  $("#tab_busqueda_carta_confirmacion_venta").prop("disabled", true);
	  $("#tab_busqueda_facturar").prop("disabled", true);
	  $("#tab_busqueda_usos_impresiones").prop("disabled", true);
	  $("#tab_busqueda_ver_formas_pago").prop("disabled", false);
	  $("#tab_busqueda_modificar_lineas_detalle").prop("disabled", true);
	  $("#tab_busqueda_mostrar_ocultar_num_bono_agencia").hide();
	  
	});
//*******************************************************************
$("#tab_busqueda_informe_resumen_venta").on("click", function(e) {
	window.open("../../jasper.post?idVenta=<x:out select = "$editar_venta_xml/Venta/idventa" />&report=Resumen_Venta","_blank");	
});
//****************************************************************
$("#tab_busqueda_historico_cambios").on("click", function(e) {
	showButtonSpinner("#tab_busqueda_historico_cambios");
	var id="<x:out select = "$editar_venta_xml/Venta/idventa" />";
	

	$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/ventareserva/busqueda/show_historico_cambios_venta.do'/>?id="+id, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "lg");
	});

	
});
//****************************************************************
$("#tab_busqueda_observaciones").on("click", function(e) {
	showButtonSpinner("#tab_busqueda_observaciones");

	var id="<x:out select = "$editar_venta_xml/Venta/idventa" />";
	var observaciones=$("#observaciones_venta").val();

	$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/ventareserva/busqueda/show_observaciones_venta.do'/>?id="+id+"&observaciones="+observaciones, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "sm");
	});	
});
//****************************************************************
$("#tab_busqueda_carta_confirmacion_venta").on("click", function(e) {
	showButtonSpinner("#tab_busqueda_carta_confirmacion_venta");

	var id="<x:out select = "$editar_venta_xml/Venta/idventa" />";
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/ventareserva/busqueda/show_carta_confirmacion_venta.do'/>?idVenta="+id, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "xs");
	});	
});

//****************************************************************
$("#tab_busqueda_facturar").on("click", function(e) {
	showButtonSpinner("#tab_busqueda_facturar");

	var id="<x:out select = "$editar_venta_xml/Venta/idventa" />";
	
	$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/ventareserva/busqueda/show_generar_factura.do'/>?idventa="+id+"&idcliente="+$("#idcliente_fiscal").val(), function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "xs");
	});	
	
});

//****************************************************************
$("#tab_busqueda_usos_impresiones").on("click", function(e) {
	showButtonSpinner("#tab_busqueda_usos_impresiones");

	var id="<x:out select = "$editar_venta_xml/Venta/idventa" />";
	
	$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/ventareserva/busqueda/show_usos_impresiones_venta.do'/>?idVenta="+id, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "lg");
	});		
});
//****************************************************************
$("#tab_busqueda_ver_formas_pago").on("click", function(e) {
	showButtonSpinner("#tab_busqueda_ver_formas_pago");
	
	var id="<x:out select = "$editar_venta_xml/Venta/idventa" />";
	$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/ventareserva/busqueda/show_formas_de_pago_venta.do'/>?idVenta="+id, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "sm");
	});		
		
});
//****************************************************************
$("#tab_busqueda_modificar_lineas_detalle").on("click", function(e) {
	showButtonSpinner("#tab_busqueda_modificar_lineas_detalle");
	
	$("#idlineadetalle_venta").val(dt_listdetalleventa.rows(0).data()[0][0].idlineadetalle);
	
	$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/ventareserva/busqueda/venta/showLineas.do'/>?idVenta="+$("#idVentaEditar").val(), function() {
	
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "lg");
	});		
});
//****************************************************************
$("#tab_busqueda_mostrar_ocultar_num_bono_agencia").on("click", function(e) {
	showButtonSpinner("#tab_busqueda_mostrar_ocultar_num_bono_agencia");
	
	var data = dt_listdetalleventa.rows( { selected: true } ).data();
	
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		hideSpinner("#tab_busqueda_mostrar_ocultar_num_bono_agencia");
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.tabs.ventareserva.dialog.entradas_venta.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		hideSpinner("#tab_busqueda_mostrar_ocultar_num_bono_agencia");
		return;
	}	
	
	var numero=dt_listdetalleventa.rows(".selected").data()[0][0].numerobonoagencia;
	var id=dt_listdetalleventa.rows(".selected").data()[0][0].idlineadetalle;
	
	
	
	$("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/ventareserva/busqueda/show_nom_bonos_agencia_venta.do'/>?idlineadetalle="+id+"&numbonoagencia="+numero, function() {
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "sm");
	});		


		
});
//*******************************************************************
$("#tab_busqueda_recibo_impreso").on("click", function(e) { 
	showButtonSpinner("#tab_busqueda_recibo_impreso");
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/venta/ventareserva/imprimir_recibo.do'/>",
		timeout : 100000,
		data: {
			idventa: "<x:out select = "$editar_venta_xml/Venta/idventa" />"
		},
		success : function(data) {
			hideSpinner("#tab_busqueda_recibo_impreso");
		},
		error : function(exception) {
			hideSpinner("#tab_busqueda_recibo_impreso");
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",
				  delay: 5000,
				  buttons: { closer:true, sticker: false }			  
			   });		
		}
	});		   
}); 
//********************************************************
$("#form_venta_reserva_editar_venta_dialog").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			grabarVenta();
		}
	}
	);
//********************************************************
function grabarVenta() { 
	
	venta_json.Venta.telefono=$("#telefono").val();
	venta_json.Venta.telefonomovil=$("#movil").val();
	venta_json.Venta.email=$("#email").val();
	venta_json.Venta.cp=$("#cp_callcenter").val();	
	
	$("#venta_xml").val(json2xml(venta_json));
	  var data = $("#form_venta_reserva_editar_venta_dialog").serializeObject();	 
	$.ajax({
		contenttype: "application/json; charset=utf-8",
		type : "post",
		url : "<c:url value='/ajax/ventareserva/busqueda/save_venta.do'/>",
		timeout : 100000,
		data: data, 
		success : function(data) {	
			$("#modal-dialog-form").modal('hide');
			dt_listbusqueda.ajax.reload(null,false);
			
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
			
		},
		error : function(exception) {
		
			new PNotify({
			      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
			      text: exception.responseText,
				  type: "error",		     
				  delay: 5000,
				  buttons: { sticker: false }
			   });			
		}
	}); 
	
	
}


//*********************************
function construir_xml_lineasdetalle(lineas_detalle_sel) {
	
	var xml_lineasdetalle= "<lineadetalles>";

	for (var i=0; i<lineas_detalle_sel.length; i++) { 
		xml_lineasdetalle+="<Lineadetalle>";
		var lineaLoop = lineas_detalle_sel[i][0];
		var strLineaNueva = ""+lineaLoop.Lineadetalle;
		if(strLineaNueva!='undefined')
			lineaLoop = lineaLoop.Lineadetalle;
		xml_lineasdetalle+=json2xml(lineaLoop,"");
		xml_lineasdetalle+="</Lineadetalle>";
	}
	xml_lineasdetalle+="</lineadetalles>";
	
	
	return(xml_lineasdetalle);
}
//******************************************************************************
$("#button_venta_resumen_entradas").on("click", function(e) { 
	showButtonSpinner("#button_venta_resumen_entradas");
	 $("#modal-dialog-form-2 .modal-content:first").load("<c:url value='/ajax/venta/ventareserva/imprimir_entradas.do?idventa="+$("#idVentaEditar").val()+"'/>", function() {										  
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "md");
		});
}); 
//*********************************************************

</script>






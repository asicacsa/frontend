<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${ventareserva_productos_principales}" var="ventareserva_productos_principales_xml" />
<x:parse xml="${motivosModificacion}" var="motivosModificacion_xml" />


<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>		
</div>

<div class="modal-body">
	<form id="form_Lineas_data" data-parsley-validate="" class="form-horizontal form-label-left" >
			<input type="hidden" id="xmlModificacion" name="xmlModificacion" />
			<input type="hidden" id="strMotivo" name="strMotivo" />
			<input type="hidden" id="idcliente_venta_individual" name="idcliente_venta_individual" value=""/>
			<input type="hidden" id="cifcliente_venta_individual" value=""/>
			<input type="hidden" id="cpcliente_venta_individual" value=""/>
			<input type="hidden" id="emailcliente_venta_individual" value=""/>
			<input type="hidden" id="telefonocliente_venta_individual" value=""/>
			<input type="hidden" id="fecha_venta_individual" name="fecha_venta_individual" value=""/>
			<input type="hidden" id="idTipoVentaLineas" name="idTipoVentaLineas" value="">
			<input type="hidden" id="idsesionlinea" name="idsesionlinea" value="${idsesion_venta}">
			<input type="hidden" id="idlineadetalle" name="idlineadetalle">
			<input type="hidden" id="xml_perfiles_tarifas" name="xml_perfiles_tarifas">
			
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label class="control-label"><spring:message code="venta.ventareserva.tabs.emision_bono.field.unidad" /></label>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<select class="form-control ventareserva" name="idunidad_venta_individual" id="idunidad_venta_individual" size="5" required="required">
							<x:forEach select="$ventareserva_productos_principales_xml/ArrayList/Entry/unidadnegocio" var="item">
								<option value="<x:out select="$item/idunidadnegocio" />"><x:out select="$item/nombre" /></option>
							</x:forEach>
						</select>
					</div>
				</div>
			</div>
				
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="form-group">
					<label class="control-label"><spring:message code="venta.ventareserva.tabs.emision_bono.field.producto" /></label>
					<div class="col-md-12 col-sm-12 col-xs-12" id="div_producto_venta_individual">
						<select class="form-control ventareserva" name="idproducto_venta_individual" id="idproducto_venta_individual" size="5" required="required">
							<option value=""></option>
						</select>
					</div>
				</div>				
			</div>
		

		<div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-8 col-sm-offset-8 col-xs-offset-8">
			<button id="button_add_venta_individual" type="button" class="btn btn-success pull-right">
				<spring:message code="venta.ventareserva.tabs.emision_bono.button.add" />
			</button>
		</div>
		
	<div class="col-md-12 col-sm-12 col-xs-12">
	
		<div class="totales-group totales_venta_individual">
			Total: <span class="total-lbl"><span id="total-val_linea">0.00</span>&euro;</span>&nbsp;<span class="descuento-lbl">Descuento: <span id="total-desc_linea">0.00</span>&euro;&nbsp;(<span id="total-prc_linea">0.00</span>%)</span>
		</div>
	
		<div class="btn-group pull-right btn-datatable">
			
			<a type="button" class="btn btn-info" id="button_venta_individual_sesiones">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.sesiones" />"> <span class="fa fa-history"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="button_venta_individual_combinar">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.combinar" />"> <span class="fa fa-object-group"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="button_venta_individual_descombinar">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.button.descombinar" />"> <span class="fa fa-object-ungroup"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="button_venta_individual_duplicar">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.emision_bono.list.button.duplicar" />"> <span class="fa fa-clone"></span>
				</span>
			</a>
			<a type="button" class="btn btn-info" id="button_venta_individual_eliminar">
				<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="venta.ventareserva.tabs.emision_bono.list.button.eliminar" />"> <span class="fa fa-trash"></span>
				</span>
			</a>			
		</div>
	
		<table id="datatable_list_venta_individual" class="table table-striped table-bordered dt-responsive ventareserva" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.producto" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.fecha" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.contenido" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.disponible" />.</th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.cnd" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.perfil" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.descuento" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.impUnitario" /></th>
					<th><spring:message code="facturacion.facturas.tabs.rectificarlineas.list.header.importe" /></th>
					<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.sesiones" /></th>
					<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.bono" /></th>
					<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.idtarifa" /></th>
					<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.iddescuento" /></th>
					<th><spring:message code="venta.ventareserva.tabs.reserva_grupo.list.header.idperfil" /></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
		<br/><br/>
		<div class="form-group">			
			<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.motivos" /></label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<select required="required" class="form-control" name="idmotivo_modificacion" id="idmotivo_modificacion">
						<option value=""></option>
							<x:forEach select="$motivosModificacion_xml/ArrayList/LabelValue" var="item">
								<option value="<x:out select="$item/value" />"><x:out
											select="$item/label" /></option>
								</x:forEach>
					</select>								
				</div>	
			<label  class="control-label col-md-2 col-sm-2 col-xs-12"><spring:message code="venta.ventareserva.tabs.busquedas.venta_reserva.anular.field.observaciones" /></label>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<textarea name="observacion_modificacion" id="observacion_modificacion" class="form-control"  ></textarea>
				</div>	
		</div>	
		<div class="form-group">
				
		</div>			
	</form>	
		
	<div class="modal-footer">
		<button id="save_venta_lineas_detalle_button" type="button" class="btn btn-primary rectificar_dialog-save">
			<spring:message code="common.button.accept" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>
<script>

hideSpinner("#tab_busqueda_modificar_lineas_detalle");
var lineasDetallesEliminadas = [];

$("#idcliente_venta_individual").val($("#idcliente").val());
$("#fecha_venta_individual").val($("#fecha_reserva").val().substr(0,10));
$("#idTipoVentaLineas").val($("#idTipoVenta").val());

var idtipoventa = $("#idTipoVenta").val();


var dt_listventaindividual=$('#datatable_list_venta_individual').DataTable( {
	language: dataTableLanguage,
	info: false,
	searching: false,
	scrollCollapse: true,
	paging: false,
	initComplete: function( settings, json ) {
   	 window.setTimeout(cargarLineasParaVenta, 100);    	 
	},
    select: { style: 'os' },
    columnDefs: [
        { "targets": 0, "visible": false },
        { "targets": 1, "visible": false },
        { "targets": 11, "visible": false },
        { "targets": 12, "visible": false },
        { "targets": 13, "visible": false },
        { "targets": 14, "visible": false },
        { "targets": 15, "visible": false }
    ],
    drawCallback: function( settings ) { 
    	activateTooltipsInTable('datatable_list_venta_individual');
   	}
} );

var dataTable = dt_listventaindividual;

$("#cliente_venta_individual").clientField("<c:url value='/ajax/show_buscadorcliente.do'/>", "#cliente_venta_individual", "#idcliente_venta_individual", "#cifcliente_venta_individual", "#cpcliente_venta_individual", "#emailcliente_venta_individual", "#telefonocliente_venta_individual");






var unidadesnegocio = new Array();
var productos= new Array();
<x:forEach select="$ventareserva_productos_principales_xml/ArrayList/Entry" var="item">
	productos=[];
	<x:forEach select="$item/productos/Producto" var="producto">
		productos.push({value: '<x:out select="$producto/value" />', label: '<x:out select="$producto/label" />'});
	</x:forEach>
	unidadesnegocio['<x:out select="$item/unidadnegocio/nombre" />']= productos;
</x:forEach>

$("#idunidad_venta_individual").on("change", function(e) {
	showFieldSpinner("#div_producto_venta_individual");
	
	var data = $("#idunidad_venta_individual :selected").val();
	$select=$("#idproducto_venta_individual");
	
	/* Primero se a�aden los productos principales */
	
	$select.html('');
	unidadesnegocio[$("#idunidad_venta_individual :selected").text()].forEach( function(element) { 
	      $select.append('<option class="option-main-product" value="' + element.value + '">' + element.label + '</option>');
	});
	
	/* Luego se a�aden los subproductos por ajax */
	
	if (data!="") { // La unidad de negocio COMBINADAS no tiene id
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/venta/ventareserva/list_subproductos.do'/>",
			timeout : 100000,
			data: {
		    	   id: data.toString()
				  }, 
			success : function(data) {
				hideSpinner("#div_producto_venta_individual");
				if (data.ArrayList!="") {
					var item= data.ArrayList.LabelValue;
					if (item.length>0) {
						item=sortJSON(item,"label",true);
					    $.each(item, function(key, val){
					      $select.append('<option value="' + val.value + '">' + val.label + '</option>');
					    });
					}
					else
					      $select.append('<option value="' + item.value + '">' + item.label + '</option>');
				}
			},
			error : function(exception) {
				hideSpinner("#div_producto_venta_individual");
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",
					  buttons: { sticker: false }				  
				   });		
			}
		});	
	}
	else hideSpinner("#div_producto_venta_individual");

});


/* Funci�n gen�rica para obtener el xml de las lineas de detalle en las operaciones de Venta y Reserva */
function construir_xml_lineasdetalle(lineas_detalle_sel) {
	
	var xml_lineasdetalle= "<lineadetalles>";

	for (var i=0; i<lineas_detalle_sel.length; i++) { 
		xml_lineasdetalle+="<Lineadetalle>";
		var lineaLoop = lineas_detalle_sel[i][0];
		var strLineaNueva = ""+lineaLoop.Lineadetalle;
		if(strLineaNueva!='undefined')
			lineaLoop = lineaLoop.Lineadetalle;
		xml_lineasdetalle+=json2xml(lineaLoop,"");
		xml_lineasdetalle+="</Lineadetalle>";
	}
	xml_lineasdetalle+="</lineadetalles>";
	
	
	return(xml_lineasdetalle);
}

function obtener_totales_venta_temporal(datatable,idcliente,idtipoventa) {
	var tabla_set= {};
	$("#total-val_linea").text(0);
	$("#total-desc_linea").text(0);
	$("#total-prc_linea").text(0);
	
	idcliente = ""+$("#idcliente_rectificacion").val();
	
	
	if(idcliente=="undefined")
	{
		if(reserva_json.Reserva.cliente.idcliente!="undefined")
			idcliente = ""+reserva_json.Reserva.cliente.idcliente;
	}
		
	if(idcliente=="undefined")
		idcliente = "";
	
	if(dataTable.rows().length>0)
		{
		var xml_lineasdetalle = construir_xml_lineasdetalle(dataTable.rows().data());
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/venta/ventareserva/obtener_totales_ventatemporal.do'/>",
			timeout : 100000,
			data: {
				idcliente: idcliente,
				idtipoventa: idtipoventa,
				xml: xml_lineasdetalle
			},
			success : function(data) {
				console.log(data);
				$("#total-val_linea").text(data.TotalVentaDTO.importeventa);
				$("#total-desc_linea").text(data.TotalVentaDTO.importedc);
				$("#total-prc_linea").text(data.TotalVentaDTO.porcentaje);		
				
			},
			error : function(exception) {
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",
					  delay: 5000,
					  buttons: { closer:true, sticker: false }			  
				   });		
			}
		});	
	}
}






function cargarLineasParaVenta()
{
var data = dt_reserva.rows().data();

data.each(function (value, index) {
	var lineaDetalle = dt_reserva.rows( index ).data()[0][0];
	var lineadetalle=lineaDetalle;
	var fechas= "", contenidos= "", disponibles= "",retorno= false;
	var impreso = lineadetalle.entradasimpresas+"/"+lineadetalle.entradasnoanuladas;
	var sesiones = lineadetalle.lineadetallezonasesions.Lineadetallezonasesion;
	var descuento_nombre=""+lineadetalle.descuentopromocional.nombre;
	var id_tarifa="",idperfilvisitante="",iddescuento="";
	var numbonoagencia= lineadetalle.numerobonoagencia;
	
	id_tarifa = lineadetalle.perfilvisitante.tarifa.idtarifa;
	idperfilvisitante = lineadetalle.perfilvisitante.idperfilvisitante;
	
	if(descuento_nombre=="undefined")
		descuento_nombre =" ";
	else
		{
		iddescuento = lineadetalle.descuentopromocional.iddescuentopromocional;
		}
	
	
	
	var contenidos;
	
	 if (sesiones.length>0)		 
	 {
		 for (var i=0; i<sesiones.length; i++) {
				if (retorno) {
					fechas+="</br>";
					contenidos+= "</br>";
					disponibles+="</br>";
				}				
				var f= sesiones[i].zonasesion.sesion.fecha.split("-")[0];
				fechas+= f;					
				var hi=sesiones[i].zonasesion.sesion.horainicio;
				var  no=sesiones[i].zonasesion.sesion.contenido.nombre;
				contenidos+= hi+" "+no;
				var d=sesiones[i].zonasesion.numlibres;
				disponibles+=d;
				retorno= true;
			}	 
			
	 }
	 else
	 {
		fechas= lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.fecha.split("-")[0];
		contenidos= lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.horainicio+" "+lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.sesion.contenido.nombre;
		disponibles= lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion.numlibres;
	}
			
	 
	 var idVentaLinea = ""+lineadetalle.venta.idventa;
	
	 if(idVentaLinea!="undefined")
		 lineadetalle.cantidad = 0;
	 
	 dt_listventaindividual.row.add([
			     lineadetalle,
			     lineadetalle.producto.idproducto, 
			     lineadetalle.producto.nombre,
			   	 fechas,
				 contenidos,
			 		disponibles,
			 		lineadetalle.cantidad, 
			 		 lineadetalle.perfilvisitante.nombre, 
			 		 descuento_nombre, 
			 		 lineadetalle.tarifaproducto.importe,
			 		lineadetalle.importe,  // Importe
					                            "", // Datos de las sesiones
					                            "", // Bono
					                            id_tarifa, // idtarifa
					                            iddescuento,	// iddescuento
					                            idperfilvisitante 	// idperfil
				                               ]).draw();
	
	
	});	
	
	
	var idcliente = $("#idcliente").val();
	var idtipoventa = $("#idTipoVentaLineas").val();
	
	obtener_totales_venta_temporal(dt_listventaindividual,idcliente,idtipoventa)

}

//********************************************************************************
$("#button_venta_individual_eliminar").on("click", function(e) { 
		
	new PNotify({
	      title: '<spring:message code="common.dialog.text.atencion" />',
	      text: '<spring:message code="venta.ventareserva.tabs.reserva_grupo.list.confirm.eliminar" />',
		  hide: false,
		  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
		  buttons: { closer: false, sticker: false	},
		  history: { history: false	}
	   }).get().on('pnotify.confirm', function() {
		   var lineas_seleccionadas =  dt_listventaindividual.rows( { selected: true } );
		   for(i=0;i<lineas_seleccionadas.length;i++)
			   lineasDetallesEliminadas.push(lineas_seleccionadas.data()[i][0]);
		    dt_listventaindividual.rows( { selected: true } ).remove().draw();
			var idcliente = $("#idcliente_rectificacion").val();
			var idtipoventa = $("#idTipoVenta_rectificacion").val();
		   obtener_totales_venta_temporal(dt_listventaindividual,idcliente,idtipoventa);
	   }).on('pnotify.cancel', function() {
	   });		 

}); 



//********************************************************************************
$("#button_venta_individual_duplicar").on("click", function(e) { 
		
	var data= $.extend(true,[],dt_listventaindividual.rows( { selected: true } ).data());

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	dt_listventaindividual.rows( { selected: true } ).data()[0][0].idlineadetalle=""
	var lineaDetalleZonaSession = dt_listventaindividual.rows( { selected: true } ).data()[0][0].lineadetallezonasesions.Lineadetallezonasesion;
	
	if(lineaDetalleZonaSession.length>0)
		{
		for( i=0;i<lineaDetalleZonaSession.length;i++)
			dt_listventaindividual.rows( { selected: true } ).data()[0][0].lineadetallezonasesions.Lineadetallezonasesion[i].idlineadetallezonasesion = "";
		}
	else
		{
		dt_listventaindividual.rows( { selected: true } ).data()[0][0].lineadetallezonasesions.Lineadetallezonasesion.idlineadetallezonasesion = "";
		}
	
	
	dt_listventaindividual.rows.add(data).draw();
	
	var idcliente = $("#idcliente_rectificacion").val();
	var idtipoventa = $("#idTipoVenta_rectificacion").val();
    obtener_totales_venta_temporal(dt_listventaindividual,idcliente,idtipoventa);

}); 

//********************************************************************************
$("#button_add_venta_individual").on("click", function(e) { 
	if ($("#idproducto_venta_individual option:selected").length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	//showButtonSpinner("#button_add_venta_individual");
	
	var formdata = $("#form_Lineas_data").serializeObject();
	
	
	
	if ($("#idunidad_venta_individual :selected").val()!="" && $("#idproducto_venta_individual option:selected").hasClass("option-main-product")) {
		
		$.ajax({
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/venta/ventareserva/get_producto.do'/>",
			timeout : 100000,
			data: {
				idcliente: formdata.idcliente_venta_individual,
				idcanal: "${sessionScope.idcanal}",
				idtipoventa: 4, // Tipo Venta no numerada
				idproducto: formdata.idproducto_venta_individual,
				fecha: formdata.fecha_venta_individual,
				principal: $("#idproducto_venta_individual :selected").hasClass("option-main-product"),
				autosesion: "true"
			},
			success : function(data) {
				
				var contenido="",
					fecha_sesion= "",
					disponibles= "";
				
				item= data.Lineadetalle.lineadetallezonasesions.Lineadetallezonasesion.zonasesion;
				
				if (item.length>0) {
					contenido= item[0].sesion.horainicio+" "+item[0].sesion.contenido.nombre;
					fecha_sesion= item[0].sesion.fecha;
					disponibles= item[0].numlibres;
				}
				else {
					contenido= item.sesion.horainicio+" "+item.sesion.contenido.nombre;
					fecha_sesion= item.sesion.fecha;
					disponibles= item.numlibres;
				}
				
				var tarifa= data.Lineadetalle.perfilvisitante.nombre;
			
				var importe= data.Lineadetalle.tarifaproducto.importe;
				
				dt_listventaindividual.row.add([data.Lineadetalle,
		                                	$("#idproducto_venta_individual :selected").val(), 
				                            $("#idproducto_venta_individual :selected").text(),
				                            fecha_sesion.split("-")[0],
				                            contenido,
				                            disponibles,
				                            1,  // N� Etd
				                            typeof tarifa!="undefined"?tarifa:"", // Perfil/Tarifa
				                            "", // Descuentos
				                            importe, // I. Unitario
				                            importe,  // Importe
				                            "", // Datos de las sesiones
				                            "", // Bono
				                            "", // idtarifa
				                            "",	// iddescuento
				                            "" 	// idperfil
			                               ]).draw();
				
				hideSpinner("#button_add_venta_individual");
				
				var idcliente = $("#idcliente_rectificacion").val();
				
				var idtipoventa = $("#idTipoVenta_rectificacion").val();
				
			    obtener_totales_venta_temporal(dt_listventaindividual,idcliente,idtipoventa);
			    

			},
			error : function(exception) {
				hideSpinner("#button_add_venta_individual");
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",
					  delay: 5000,
					  buttons: { closer:true, sticker: false }			  
				   });		
			}
		});	
	}
	else {
		
		$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/facturacion/ventareserva/show_disponibles.do'/>?idproducto="+formdata.idproducto_venta_individual+"&idcliente="+formdata.idcliente_venta_individual+"&idtipoventa=4&producto="+encodeURIComponent($("#idproducto_venta_individual option:selected").text())+"&fecha="+formdata.fecha_venta_individual+"&button=venta_individual&list=dt_listventaindividual", function() {
			$("#modal-dialog-form-3").modal('show');
			setModalDialogSize("#modal-dialog-form-3", "md");
		});	}

}); 

//********************************************************************************
$("#button_venta_individual_combinar").on("click", function(e) {
	
	if (dt_listventaindividual.rows( { selected: true }).data().length<=1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.combinados.no_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#button_venta_individual_combinar");	
	
	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/facturacion/show_list_combinados.do'/>?tabla=venta_individual&list=dt_listventaindividual", function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "xs");
	});
});

//********************************************************************************
$("#button_venta_individual_descombinar").on("click", function(e) {
	
	if (dt_listventaindividual.rows( { selected: true }).data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#button_venta_individual_descombinar");	
	
	if (dt_listventaindividual.rows( { selected: true }).data()[0][0].producto.tipoprodproductos.Tipoprodproducto.length>0)
	{
		showButtonSpinner("#button_venta_individual_descombinar");	
		
		$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/facturacion/show_list_productos.do'/>?tabla=venta_individual&list=dt_listventaindividual", function() {
            $("#modal-dialog-form-3").modal('show');
            setModalDialogSize("#modal-dialog-form-3", "xs");
      });

	}else
	{
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.producto_no_combinado" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		hideSpinner("#button_venta_individual_descombinar");	
		return;
	}
});

$("#save_venta_lineas_detalle_button").on("click", function(e) {
	
//Si alguno de los importes de alguna de las lineas de detalle es nulo no dejamos grabar
	
	for(i=0; i<dt_listventaindividual.rows().data().length;i++)
	{
		if(dt_listventaindividual.rows().data()[i][9]=="undefined"){
			new PNotify({
			      title: '<spring:message code="venta.ventareserva.dialog.edit_sesion.field.numero_lineadetalle" />',
			      text: "",
				  type: "error",
				  buttons: { sticker: false }				  
			   });		
			return;
		}
	}
	
	var idreserva = $("#idreserva").val();
    
	/*var url= "<c:url value='/ajax/ventareserva/busqueda/reserva/editarPago.do'/>?id="+idreserva;
	$("#modal-dialog-form-3 .modal-content").load(url, function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "sm");
	});*/
	guardarModificacionReserva()
	
	
})


$("#form_Lineas_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {	
			guardarModificacionReserva()			
		}
	}
)

//***************************************************
var xml="";

function guardarModificacionReserva()
{
	var idReserva = $("#idreserva").val();
	xml = "<Modificacion><idmodificacion/>";
	xml+="<observaciones>"+$("#observacion_modificacion").val()+"</observaciones>";
	xml+="<motivomodificacion>"+$("#idmotivo_modificacion option:selected").text()+"</motivomodificacion>";
	xml+="<venta>";
	xml+="<idventa/>";
	xml+="<entradasimpresas>false</entradasimpresas>";
	xml+="<reciboimpreso>0</reciboimpreso>";
	xml+="<imprimirSoloEntradasXDefecto>false</imprimirSoloEntradasXDefecto>";
	xml+="</venta>";
	
	xml+="<reserva><idreserva>"+idReserva+"</idreserva></reserva>";
	xml+="<modificacionimporteparcials>";
	xml+="<Modificacionimporteparcial><importeparcial><formapago><idformapago></idformapago></formapago><bono><numeracion/></bono><importe></importe><rts/><numpedido/><anulado/></importeparcial></Modificacionimporteparcial>";
	xml+="<Modificacionimporteparcial><importeparcial><formapago><idformapago></idformapago></formapago><bono><numeracion/></bono><importe></importe><rts/><numpedido/><anulado/></importeparcial></Modificacionimporteparcial>";
	xml+="<Modificacionimporteparcial><importeparcial><formapago><idformapago></idformapago></formapago><bono><numeracion/></bono><importe></importe><rts/><numpedido/><anulado/></importeparcial></Modificacionimporteparcial>";
	xml+="<anulaciondesdecaja/>";
	xml+="</modificacionimporteparcials>";
	var xml_lineasdetalle = construir_xml_lineasdetalle_pagos(dt_listventaindividual.rows().data());
	xml = xml+xml_lineasdetalle;
	xml+="</Modificacion>";
	$("#xmlModificacion").val(xml);
	
	var data = $("#form_Lineas_data").serializeObject();
	
	$.ajax({
		type : "post",
		url : "<c:url value='/ajax/ventareserva/busqueda/modificar_lineasdetalle_reserva.do'/>",
		timeout : 100000,
		data : data,
		success : function(data) {		
			$("#modal-dialog-form-3").modal('hide');
			$("#modal-dialog-form-2").modal('hide');
			
			
		 	$("#modal-dialog-form .modal-content").load("<c:url value='/venta/ventareserva/busqueda/ventareserva/editar_reserva.do'/>?id="+$("#idreserva").val(), function() {
				$("#modal-dialog-form").modal('show');
				setModalDialogSize("#modal-dialog-form", "md");
			}); 
			
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
			$("#modal-dialog-form-2").modal('hide');
		},
		error : function(exception) {
			

			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : exception.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		}
	}); 

}


 function procesarPago(fila)
{


	if(fila[1]=="")
	{
		var jsonFila = fila[0];
		
		
		xml+="<Modificacionimporteparcial>";
		xml+="<tipomodificacion>0</tipomodificacion>";
		xml+="<importeparcial isnew=\"false\" anulado=\"0\">";
		xml+="<idimporteparcial>"+jsonFila.idimporteparcial+"</idimporteparcial>";
		xml+="<formapago>"+json2xml(jsonFila.formapago)+"</formapago>";
		xml+="<importe>"+jsonFila.importe+"</importe>";
		xml+="<modificacionimporteparcials>"+json2xml(jsonFila.modificacionimporteparcials)+"</modificacionimporteparcials>";
		
		
		if (jsonFila.venta!=""){
			xml+="<venta>";
			xml+="<idventa>"+ $("#idVentaEditar").val()+"</idventa>";
			xml+="<operacioncaja>";
			xml+="<idoperacioncaja>"+jsonFila.venta.operacioncaja.idoperacioncaja+"</idoperacioncaja>";
			xml+="<caja>";
			xml+="<idcaja>"+jsonFila.venta.operacioncaja.caja.idcaja+"</idcaja>";
			xml+="</caja>";
			xml+="</operacioncaja>";
			xml+="</venta>";
		}else{
			xml+="<venta/>";
		}	
		
		xml+="<bono/><rts/><numpedido/>";
		xml+="</importeparcial>";			
		xml+="</Modificacionimporteparcial>";
	}
else
	{
		xml+="<Modificacionimporteparcial>";
		xml+="<tipomodificacion>0</tipomodificacion>";
		xml+="<importeparcial isnew=\"true\" anulado=\"0\">";
		xml+="<formapago>";
		xml+="<idformapago>"+fila[1]+"</idformapago>";
		xml+="<nombre></nombre>";
		xml+="</formapago>";
		xml+="<importe>"+fila[3]+"</importe>";
		xml+="<bono><idbono/></bono><rts/><numpedido/><anulado>0</anulado>";
		xml+="</importeparcial>";
		xml+="</Modificacionimporteparcial>";
	}
}
 
//*****************************************************
 function construir_xml_lineasdetalle_pagos(lineas_detalle_sel) {
 	
 	var xml_lineasdetalle= "<modificacionlineadetalles>";
 	xml_lineasdetalle+= "<Modificacionlineadetalle>";
 	xml_lineasdetalle+= "<lineadetalle>";
 	xml_lineasdetalle+= "<idlineadetalle/>";
 	xml_lineasdetalle+= "</lineadetalle>";
 	xml_lineasdetalle+= "</Modificacionlineadetalle>";

 	for (var i=0; i<lineas_detalle_sel.length; i++) { 
 		xml_lineasdetalle+="<Modificacionlineadetalle>";
 		xml_lineasdetalle+="<tipomodificacion>0</tipomodificacion>";
 		xml_lineasdetalle+="<lineadetalle>";
 		var lineaLoop = lineas_detalle_sel[i][0];
 		var strLineaNueva = ""+lineaLoop.Lineadetalle;
 		if(strLineaNueva!='undefined')
 			lineaLoop = lineaLoop.Lineadetalle;
 		xml_lineasdetalle+=json2xml(lineaLoop,"");
 		xml_lineasdetalle+="</lineadetalle>";
 		xml_lineasdetalle+="</Modificacionlineadetalle>";
 		
 	}
 	
 	for(i=0;i<lineasDetallesEliminadas.length;i++)
	   {
		lineaDetalle = lineasDetallesEliminadas[i];
		xml_lineasdetalle+="<Modificacionlineadetalle>";
		xml_lineasdetalle+="<tipomodificacion>1</tipomodificacion>";
		xml_lineasdetalle+="<lineadetalle><idlineadetalle>"+lineaDetalle.idlineadetalle+"</idlineadetalle></lineadetalle>";
		xml_lineasdetalle+="</Modificacionlineadetalle>";
	   }
 	
 	
 	xml_lineasdetalle+="</modificacionlineadetalles>";
 	
 	
 	return(xml_lineasdetalle);
 }


//********************************************************************************
$("#button_venta_individual_sesiones").on("click", function(e) {
	
	if (dt_listventaindividual.rows( { selected: true }).data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	if (dt_listventaindividual.rows( { selected: true }).data().length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	showButtonSpinner("#button_venta_individual_sesiones");	
	
		
	var formdata = $("#form_Lineas_data").serializeObject(),
		rowdata= dt_listventaindividual.rows( { selected: true }).data();
	
		
	var sesiones = rowdata[0][0].lineadetallezonasesions.Lineadetallezonasesion;
	var map = "<tipoProducto>";
	if (sesiones.length>=0)
		{
		for(i=0;i<sesiones.length;i++)
			{
			var session = sesiones[i].zonasesion.sesion;
			var id_producto_sesion = ""+session.contenido.tipoproducto.idtipoproducto;
			var fecha_sesion = session.fecha;
			map +="<entry><int>"+id_producto_sesion+"</int><date>"+fecha_sesion+"</date></entry>"			
			}
		}
	else
		{
		var session = sesiones.zonasesion.sesion;
		var id_producto_sesion = ""+session.contenido.tipoproducto.idtipoproducto;
		var fecha_sesion = session.fecha;
		map +="<entry><int>"+id_producto_sesion+"</int><date>"+fecha_sesion+"</date></entry>"
		}
	
	map +="</tipoProducto>";
	
	
	/*
	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/facturacion/ventareserva/show_disponibles.do'/>?idproducto="+rowdata[0][1]+"&idcliente="+formdata.idcliente_venta_individual+"&producto="+encodeURIComponent(rowdata[0][2])+"&fecha="+formdata.fecha_venta_individual+"&button=venta_individual&list=dt_listventaindividual&edicion=1", function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "md");
	});*/
	
	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/facturacion/ventareserva/show_disponibles.do'/>?idproducto="+rowdata[0][1]+"&idcliente="+formdata.idcliente_venta_individual+"&producto="+encodeURIComponent(rowdata[0][2])+"&fecha="+formdata.fecha_venta_individual+"&map="+map+"&button=venta_individual&list=dt_listventaindividual&edicion=1", function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "md");
	});
});


//**********************************************
var celda;
$("#datatable_list_venta_individual tbody").delegate("td", "click", function() {
	celda=this;
	var posicionCelda=0;    	
	posicionCelda=$("td", celda).context.cellIndex;
	if (posicionCelda=="4" || posicionCelda=="5" || posicionCelda=="6")    		
		window.setTimeout(editarLineaDetalleVentaIndividual, 100);
});   

//***************************************************************************************

function editarLineaDetalleVentaIndividual()
{
	
	if (dt_listventaindividual.rows( { selected: true }).data().length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	if (dt_listventaindividual.rows( { selected: true }).data().length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="venta.ventareserva.list.alert.productos.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	var data = $("#form_selector_venta_individual").serializeObject(),
	rowdata= dt_listventaindividual.rows( { selected: true }).data();

	
	
	var idproductolinea_detalle=dt_listventaindividual.rows( { selected: true }).data()[0][0].producto.idproducto;
	var nombre_producto=dt_listventaindividual.rows( { selected: true }).data()[0][2];
	var fecha=dt_listventaindividual.rows( { selected: true }).data()[0][3];
	var idtarifa=dt_listventaindividual.rows( { selected: true } ).data()[0][0].perfilvisitante.tarifa.idtarifa;
	
	
	var idcliente = $("#idcliente").val();
	$("#modal-dialog-form-3 .modal-content").load("<c:url value='/ajax/ventareserva/busqueda/venta/show_edit_sesion.do'/>?idproducto="+idproductolinea_detalle+"&idcliente="+idcliente+"&producto="+encodeURIComponent(nombre_producto)+"&fecha="+fecha+"&idtarifa="+idtarifa+"&idcanal=${sessionScope.idcanal}&idxnumero=6&idxunitario=9&idxtotal=10&idxbono=12&&idxtarifa_txt=7&idxtarifa_id=13&idxdescuento_txt=8&idxdescuento_id=14&idxperfil=15&button=venta_individual&list=dt_listventaindividual", function() {
		$("#modal-dialog-form-3").modal('show');
		setModalDialogSize("#modal-dialog-form-3", "lg");
	});
}

$( "#button_add_venta_individual" ).hide();

$( "#idproducto_venta_individual" ).click(function() {    	
	$( "#button_add_venta_individual" ).click();
	});

</script>
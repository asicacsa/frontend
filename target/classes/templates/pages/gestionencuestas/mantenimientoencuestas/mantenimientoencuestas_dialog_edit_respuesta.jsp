<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${crear_respuesta}" var="crear_respuesta_xml" />
<x:parse xml="${traduccion_respuesta}" var="traduccion_respuesta_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	 <h4 class="modal-title">	
		<x:choose>
			<x:when select="not($crear_respuesta_xml/ValorRespuesta)">
				<spring:message code="encuestas.mantenimientoencuestas.dialog.editar_respuesta.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="encuestas.mantenimientoencuestas.dialog.crear_respuesta.title" />
			</x:otherwise>
		</x:choose>		
	</h4>	
</div>

<div class="modal-body">
<form id="form_editar_respuesta_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
	<input id="xml_respuesta" name="xml_respuesta" type="hidden" />
	<input id="cod_valorrespuesta" name="cod_valorrespuesta" type="hidden" value="<x:out select = "$codvalorrespuesta"/>" />
	<input id="i18nFieldTexto_respuesta" name="i18nFieldTexto_respuesta" type="hidden" value="<x:forEach select="$traduccion_respuesta_xml/TreeSet/ValorRespuesta" var="item"><x:out select="$item/id/codTipoResp" />~<x:out select="$item/id/codIdioma" />~<x:out select="$item/valorResp" />^</x:forEach>" />
	<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="encuestas.mantenimientoencuestas.dialog.editar_respuesta.field.valor" />*</label>
			<div class="col-md-9 col-sm-9 col-xs-12">	
				<input name="texto_respuesta" id="texto_respuesta" required="required" class="form-control" value="<x:out select = "$crear_respuesta_xml/ValorRespuesta/valorResp"/>" />	
			</div>							
		</div>
	
	
</form>

<br/>
<div class="modal-footer">
		<button id="save_respuesta_button" type="button" class="btn btn-primary save_respuesta_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		

</div>

<script>

//***********************************************************
$("#texto_respuesta").i18nField_Encuesta("#i18nFieldTexto_respuesta",<c:out value="${sessionScope.i18nlangsEncuesta}"  escapeXml="false" />);
//******************************************************
$("#save_respuesta_button").on("click", function(e) {
			$("#form_editar_respuesta_data").submit();	
		})
//************************************************************
$("#form_editar_respuesta_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFormDataRespuesta();
		}
	}
	);
	
	//************************************************************************************************
	//Si es un alta el botón de internacionalización se desactiva

	<x:if select="not($crear_respuesta_xml/ValorRespuesta)">
		$("#texto_respuesta-i18n-button").prop("disabled",true);
	</x:if>
	//*************************************GUARDAR RESPUESTA*******************************************	
	function saveFormDataRespuesta() 
	{		
		
		var xml;
		showSpinner("#modal-dialog-form .modal-content");	
		var respuesta_nueva=false;
		
		if("${codvalorrespuesta}"=="")
		{
		respuesta_nueva=true;
		xml = "<getEdicionAltaValorRespuesta><TipoRespuesta>";
		xml = xml +"<cod>"+$("#cod_tiporespuesta").val()+"</cod>";
		xml = xml +"<des>"+$("#tiporespuesta option:selected").text()+"</des>";		
		xml = xml +"<valorRespuestas>";
		xml = xml +"<ValorRespuesta>";
		xml = xml +"<id>";
		xml = xml +"<codTipoResp>"+$("#cod_tiporespuesta").val()+"</codTipoResp>";
		xml = xml +"<codIdioma>1</codIdioma>";
		xml = xml +"<codValorResp>-1</codValorResp>";
		xml = xml +"</id>";
		xml = xml + "<valorResp>"+$("#texto_respuesta").val()+"</valorResp>";
		xml = xml +"</ValorRespuesta>";
		xml = xml +"</valorRespuestas>";
		xml = xml +"</TipoRespuesta></getEdicionAltaValorRespuesta>";
		}
		else
		{			
			xml = "<getEdicionAltaValorRespuesta>";
			xml = xml +"<ValorRespuesta>";
			xml = xml +"<id>";
			xml = xml +"<codTipoResp>"+$("#cod_tiporespuesta").val()+"</codTipoResp>";
			xml = xml +"<codIdioma>1</codIdioma>";
			xml = xml +"<codValorResp>${codvalorrespuesta}</codValorResp>";
			xml = xml +"</id>";
			xml = xml +"<valorResp>"+$("#texto_respuesta").val()+"</valorResp>";
			xml = xml +"</ValorRespuesta>";
			xml = xml + "</getEdicionAltaValorRespuesta>";
		}
		$("#xml_respuesta").val(xml);
		
		var data = $("#form_editar_respuesta_data").serializeObject();	
		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/save_respuesta.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");				
				$("#modal-dialog-form-3").modal('hide');				
				
				$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/show_pregunta.do?idpregunta'/>="+$("#id_pregunta").val(), function() {										  
					$("#modal-dialog-form-2").modal('show');
					setModalDialogSize("#modal-dialog-form-2", "sm");
				});
			   
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
	}
	
	
	//*******************GUARDAR INTERNACIONALIZACION ENCUESTA*************************	
	document.addEventListener("event-save-i18n-texto_respuesta", function(e) {
	     
		     
		 $.ajax({					
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/save_internacionalizacion_respuesta.do'/>",
			timeout : 100000,
			data: {
				  data: e.detail,
				  cod_tiporespuesta:$("#cod_tiporespuesta").val(),
				  codValorResp: $("#cod_valorrespuesta").val()				    
				}, 
			success : function(data) {
				//cargarDatosInternacionalizacion(false);		
			},
			error : function(exception) {
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",		     
					  delay: 5000,
					  buttons: { sticker: false }
				   });			
			}
		});
			
	})	

</script>

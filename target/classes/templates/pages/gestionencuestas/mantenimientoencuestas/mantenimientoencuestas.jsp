<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

  <div class="col-md-12 col-sm-12 col-xs-12">  	  
   <div class="btn-group pull-right btn-datatable">
		<a type="button" class="btn btn-info" id="tab_encuesta_new">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="encuestas.mantenimientoencuestas.list.button.nuevo" />"> <span class="fa fa-plus"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_encuesta_edit">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="encuestas.mantenimientoencuestas.list.button.editar" />"> <span class="fa fa-pencil"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="tab_encuesta_change">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="encuestas.mantenimientoencuestas.list.button.cambia" />"> <span class="fa fa-trash"></span>
			</span>
		</a>		
	</div>
   	<table id="datatable-list-encuestas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>			
			    <th><spring:message code="encuestas.mantenimientoencuestas.list.header.codigo" /></th>
				<th><spring:message code="encuestas.mantenimientoencuestas.list.header.titulo" /></th>
				<th><spring:message code="encuestas.mantenimientoencuestas.list.header.finicio" /></th>							
				<th><spring:message code="encuestas.mantenimientoencuestas.list.header.ffin" /></th>
				<th><spring:message code="encuestas.mantenimientoencuestas.list.header.estado"/></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-2"/>
<tiles:insertAttribute name="modal_dialog" />

<c:set var="modal_dialog_id" scope="request" value="modal-dialog-form-3"/>
<tiles:insertAttribute name="modal_dialog" />

<script>
var dt_listencuestas=$('#datatable-list-encuestas').DataTable( {
    ajax: {
        url: "<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/list_encuestas.do'/>",
        rowId: 'cod',
        type: 'POST',
        dataSrc: function (json) {if (json!=null && typeof json!="undefined") return (sanitizeJSON(json.TreeSet.Encuesta)); return(""); },
        
        error: function (xhr, error, thrown) { 
        	new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : xhr.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}					
			});
     },
    },
    initComplete: function( settings, json ) {    	
        $('a#menu_toggle').on("click", function () {dt_listencuestas.columns.adjust().draw(); });
	},
    columns: [			
		{ data: "cod", type: "spanish-string", defaultContent: ""}, 
		{ data: "des", type: "spanish-string", defaultContent: ""},
		{ data: "fecIni", type: "spanish-string" ,  defaultContent:"",
			 render: function ( data, type, row, meta ) {			 
	    	  	  return data.substr(0, 10);}	 
		 } ,
		 { data: "fecFin", type: "spanish-string" ,  defaultContent:"",
			 render: function ( data, type, row, meta ) {			 
	    	  	  return data.substr(0, 10);}	 
		 } ,
		{ data: "dadoDeBaja", className: "text_icon cell_centered",
    		render: function ( data, type, row, meta ) {
  	  	  	if (data==0) return '<i class="fa fa-check" aria-hidden="true"></i>'; else return '<i class="fa fa-close" aria-hidden="true"></i>';	
    	}},	     
    ],    
    drawCallback: function( settings ) { activateTooltipsInTable('datatable-list-encuestas') },
    select: { style: 'os'},
	language: dataTableLanguage,
	processing: true,
} );
	//*********************************************
	insertSmallSpinner("#datatable-lista-encuestas");
	//*********************************************BOT�N NUEVO*************************************
	$("#tab_encuesta_new").on("click", function(e) {
		 showButtonSpinner("#tab_encuesta_new");
		
		 $("#modal-dialog-form .modal-content").load("<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/show_encuesta.do'/>", function() {										  
			$("#modal-dialog-form").modal('show');
			setModalDialogSize("#modal-dialog-form", "sm");
		});
	})
	/***********************************************BOT�N EDITAR*************************************/
$("#tab_encuesta_edit").on("click", function(e) {
	
	var data = sanitizeArray(dt_listencuestas.rows( { selected: true } ).data(),"cod");
		
	if (data.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.list.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}

	if (data.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.list.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
		
	showButtonSpinner("#tab_encuesta_edit");
	$("#modal-dialog-form .modal-content").load("<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/show_encuesta.do'/>?id="+data[0], function() {
		$("#modal-dialog-form").modal('show');
		setModalDialogSize("#modal-dialog-form", "sm");
	});
})
	//*************************CAMBIAR ESTADO/ELIMINAR ENCUESTA***************************
	$("#tab_encuesta_change").on("click", function(e) { 
		var data = sanitizeArray(dt_listencuestas.rows( { selected: true } ).data(),"cod");
		
		if (data.length<=0) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="encuestas.mantenimientoencuestas.dialog.eliminar_encuesta.list.alert.ninguna_seleccion" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}	
		
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="encuestas.mantenimientoencuestas.list.alert.confirm.eliminar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
	  		  buttons: { closer: false, sticker: false	},
	  		  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {
		
			   dt_listencuestas.processing(true);
				
				
				
				$.ajax({
					contenttype: "application/json; charset=utf-8",
					type : "post",
					url : "<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/remove_encuesta.do'/>",
					timeout : 100000,
					data: { data: data.toString() }, 
					success : function(data) {
						dt_listencuestas.ajax.reload();					
					},
					error : function(exception) {
						dt_listencuestas.processing(false);
						
						new PNotify({
						      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						      text: exception.responseText,
							  type: "error",		     
							  delay: 5000,
							  buttons: { sticker: false }
						   });			
					}
				});

		   }).on('pnotify.cancel', function() {
		   });		 

	});
	
	</script>
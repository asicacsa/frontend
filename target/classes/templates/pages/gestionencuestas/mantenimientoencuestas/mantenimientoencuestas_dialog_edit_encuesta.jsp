<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${editar_encuesta}" var="editar_encuesta_xml" />
<x:parse xml="${traduccion_encuesta}" var="traduccion_encuesta_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	 <h4 class="modal-title">	
		<x:choose>
			<x:when select="not($editar_encuesta_xml/Encuesta)">
				<spring:message code="encuestas.mantenimientoencuestas.dialog.crear_encuesta.title" />
			</x:when>
			<x:otherwise>
				<spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.title" />
			</x:otherwise>
		</x:choose>		
	</h4>	
</div>


<div class="modal-body">
	<form id="form_encuesta_data" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
		<input id="idencuesta_editar" name="idencuesta_editar" type="hidden" value="<x:out select = "$editar_encuesta_xml/Encuesta/cod" />" />
		<input id="codigo_anterior" name="codigo_anterior" type="hidden" value="<x:out select = "$editar_encuesta_xml/Encuesta/codOld" />" />		
		<input id="i18nFieldNombre_encuesta" name="i18nFieldNombre_encuesta" type="hidden" value="<x:forEach select="$traduccion_encuesta_xml/TreeSet/TraduccionEncuesta" var="item"><x:out select="$item/codEncuesta" />~<x:out select="$item/codIdioma" />~<x:out select="$item/texto" />^</x:forEach>" />
		
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.field.codigo" />*</label>
			<div class="col-md-9 col-sm-9 col-xs-9">
				<input name="codigo" id="codigo" type="text" data-inputmask="'mask': '9{0,10}'"  class="form-control" required="required" value="<x:out select = "$editar_encuesta_xml/Encuesta/cod" />">
			</div>
		</div>			
		
			 <div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.field.titulo" />*</label>
				<div class="col-md-9 col-sm-9 col-xs-9">
					<input name="nombre" id="nombre" type="text" class="form-control" required="required" value="<x:out select = "$editar_encuesta_xml/Encuesta/des" />">
				</div>
			</div>
		
		
		<div class="form-group date-picker">
			<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.field.flanzamiento" /></label>
			<div class="col-md-5 col-sm-5 col-xs-12">
				  <a type="button" class="btn btn-default btn-clear-date" id="button_flanzamiento_clear">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.list.button.clear" />"> <span class="fa fa-trash"></span>
					</span>
				  </a>			
	              	<div class="input-prepend input-group">
	                	<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	                    	<input type="text" name="lanzamiento" id="lanzamiento" class="form-control" value="" readonly/>
	                       	<input type="hidden" required="required" name="fechalanzamiento" value="<x:out select = "$editar_encuesta_xml/Encuesta/fecIni" />"/>
	                </div>
			</div>
		</div>
		<div class="form-group date-picker">
			<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.field.fvencimiento" /></label>
			<div class="col-md-5 col-sm-5 col-xs-12">
				<a type="button" class="btn btn-default btn-clear-date" id="button_fvencimiento_clear">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.list.button.clear" />"> <span class="fa fa-trash"></span>
					</span>
				 </a>			
	        	<div class="input-prepend input-group">
	        		<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	            		<input type="text" name="vencimiento" id="vencimiento" class="form-control" value="" readonly/>
	                	<input type="hidden" required="required" name="fechavencimiento" value="<x:out select = "$editar_encuesta_xml/Encuesta/fecFin" />"/>
	       		</div>
			</div>
		</div>			
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-3"><spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.field.estado" /></label>
				<div class="col-md-6 col-sm-6 col-xs-9">
					 <input type="checkbox" id= "cbestado" name="cbestado" value="0" >
				</div>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12" id="preguntas"  hidden>
			<div class="btn-group pull-right btn-datatable">
				<a type="button" class="btn btn-info" id="tab_preguntas_new">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.list.button.nuevo" />"> <span class="fa fa-plus"></span>
					</span>
				</a>
				<a type="button" class="btn btn-info" id="tab_preguntas_edit">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.list.button.editar" />"> <span class="fa fa-pencil"></span>
					</span>
				</a>
				<a type="button" class="btn btn-info" id="tab_preguntas_search">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.list.button.buscar" />"> <span class="fa fa-search"></span>
					</span>
				</a>
				<a type="button" class="btn btn-info" id="tab_preguntas_remove">
					<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.list.button.borrar" />"> <span class="fa fa-trash"></span>
					</span>
				</a>		
			</div>
			<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.field.listadopreguntas" /></label>
			<table id="table-listapreguntas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%" >
				<thead>
					<tr>	
						<th></th>
						<th></th>
						<th><spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.list.header.texto" /></th>
						<th><spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.list.header.tiporespuesta" /></th>																
					</tr>
				</thead>
				<tbody>								
					<x:forEach select="$editar_encuesta_xml/Encuesta/preguntaEncuestas/PreguntaEncuesta" var="item">
						<tr>
							<td><x:out select="$item/id/codPregunta"/></td>
							<td><x:out select="$item/pregunta/tipoRespuesta/cod"/></td>
							<td><x:out select="$item/pregunta/des"/></td>
							<td><x:out select="$item/pregunta/tipoRespuesta/des"/></td>
						</tr>
					</x:forEach>
				</tbody>
			</table>
		</div>	
	</div>
	</form>
	<div class="modal-footer">
		<button id="save_encuesta_button" type="submit" class="btn btn-primary save_dialog">
			<spring:message code="common.button.save" />
		</button>
		
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>		
</div>	


<script>


//****************************************************
hideSpinner("#tab_encuesta_new");
hideSpinner("#tab_encuesta_edit");
//****************************************************
$("#nombre").i18nField_Encuesta("#i18nFieldNombre_encuesta",<c:out value="${sessionScope.i18nlangsEncuesta}"  escapeXml="false" />);
//****************************************************
var cambia_codigo=false;
//****************************************************
$(document).ready(function() {
	dtpreguntas=$('#table-listapreguntas').DataTable( {
		"paging": false,
		"info": false,
		"searching": false,
		select: { style: 'os'},
		language: dataTableLanguage,
		 "columnDefs": [
		                { "visible": false, "targets": [0,1]}
		              ]
		});
} );
//*****************************************************
$("#codigo").on("change", function(e) {
	if (dtpreguntas.data().count()>0)
		cambia_codigo=true;
	})
//****************************************************
$(":input").inputmask();

//****************************************************
$("#cbestado").prop("checked",false);
//****************************************************	
<c:set var="valor"><x:out select="$editar_encuesta_xml/Encuesta/dadoDeBaja"/></c:set>
 	
<c:if test="${empty valor}">
	$("#cbestado").prop("checked",true);
</c:if>
 	
<c:if test="${valor==0}">
	$("#cbestado").prop("checked",true);
</c:if>	
//*****************************************************************************
//En modo edicción se muestra la tabla de las preguntas

<c:set var="ediccion"><x:out select="$editar_encuesta_xml/Encuesta"/></c:set>	

<c:if test="${not empty ediccion}">
	$("#preguntas").show();	
	
</c:if>

//Si es un alta el botón de internacionalización se desactiva
<c:if test="${empty ediccion}">
	$("#nombre-i18n-button").prop("disabled",true);
</c:if>	

//*****************************************************************************
//Fecha lanzamiento
$today= moment().format("DD/MM/YYYY");
dia_inicio = $today;

if($('input[name="fechalanzamiento"]').val()!='')
{	
dia_inicio = $('input[name="fechalanzamiento"]').val().substring(0, 10);
}

$('input[name="lanzamiento"]').val( dia_inicio);

$('input[name="lanzamiento"]').daterangepicker({
	singleDatePicker: true,
	autoUpdateInput: false,
	linkedCalendars: false,
	showDropdowns: true,
	autoApply: true,
  	locale: $daterangepicker_sp
	}, function(start) {
  	  	 $('input[name="lanzamiento"]').val(start.format('DD/MM/YYYY'));
  	  	 $('input[name="fechalanzamiento"]').val(start.format('DD/MM/YYYY'));
  	  	 
	 });
//*******************************************************************************
$("#button_flanzamiento_clear").on("click", function(e) {
    $('input[name="lanzamiento"]').val('');
    $('input[name="fechalanzamiento"]').val('');    
});
//********************************************************
//Fecha vencimiento

if($('input[name="fechavencimiento"]').val()!='')
{	
dia_inicio = $('input[name="fechavencimiento"]').val().substring(0, 10);
}

$('input[name="vencimiento"]').val( dia_inicio);

$('input[name="vencimiento"]').daterangepicker({
	singleDatePicker: true,
	autoUpdateInput: false,
	linkedCalendars: false,
	showDropdowns: true,
	autoApply: true,
	locale: $daterangepicker_sp
	}, function(start) {
	  	 $('input[name="vencimiento"]').val(start.format('DD/MM/YYYY'));
	  	 $('input[name="fechavencimiento"]').val(start.format('DD/MM/YYYY'));
	  	 
	 });
//*******************************************************************************
$("#button_fvencimiento_clear").on("click", function(e) {
    $('input[name="vencimiento"]').val('');
    $('input[name="fechavencimiento"]').val('');    
});


//*********************************************************
$("#tab_preguntas_new").on("click", function(e) { 
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/show_pregunta.do?idencuesta'/>="+$("#idencuesta_editar").val(), function() {										  
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "sm");
	});
})
//***********************************************************
$("#tab_preguntas_edit").on("click", function(e) { 
	var datos = dtpreguntas.rows( '.selected' ).data();	
	
	if (datos.length>1) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="mantenimientoencuestas.alert.seleccion_multiple" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}
	if (datos.length<=0) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.error" />',
		      text: '<spring:message code="mantenimientoencuestas.alert.ninguna_seleccion" />',
			  type: "error",
			  delay: 5000,
			  buttons: { sticker: false }
		   });
		return;
	}	
	
	
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/show_pregunta.do?idpregunta'/>="+datos[0][0], function() {										  
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "sm");
	});
})
//***********************************************************
$("#tab_preguntas_search").on("click", function(e) { 
	$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/search_pregunta.do'/>", function() {										  
		$("#modal-dialog-form-2").modal('show');
		setModalDialogSize("#modal-dialog-form-2", "sm");
	});
})
//************************************************************
$("#save_encuesta_button").on("click", function(e) {
			$("#form_encuesta_data").submit();	
		})
//************************************************************
$("#form_encuesta_data").validate({
		onfocusout : false,
		onkeyup : false,
		unhighlight: function(element, errClass) {
            $(element).popover('hide');
		},		
		errorPlacement : function(err, element) {
			err.hide();
			$(element).attr('data-content', err.text());
			$(element).popover({ placement: 'bottom', offset: 20, trigger: 'manual' });
			$(element).popover('show');									
		},
		submitHandler : function(form) {
			saveFormDataEncuesta();
		}
	}
	);
	//***********************************GUARDAR ENCUESTA*********************************************	
	function saveFormDataEncuesta() 
	{
		
		if(cambia_codigo){ 
		new PNotify({
			 title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.cambio_codigo" />',
			type : "error",
			delay : 5000,
			buttons : {
				closer : true,
				sticker : false
			}
			
		});
		return;
		}
		var data = $("#form_encuesta_data").serializeObject();

		showSpinner("#modal-dialog-form .modal-content");

		$.ajax({
			type : "post",
			url : "<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/save_encuesta.do'/>",
			timeout : 100000,
			data : data,
			success : function(data) {
				hideSpinner("#modal-dialog-form .modal-content");				
				$("#modal-dialog-form").modal('hide');				
				dt_listencuestas.ajax.reload(null,false);
				
				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_realizada" />',
					text : '<spring:message code="common.dialog.text.datos_guardados" />',
					type : "success",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
				
				
			},
			error : function(exception) {
				hideSpinner("#modal-dialog-form .modal-content");

				new PNotify({
					title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
					text : exception.responseText,
					type : "error",
					delay : 5000,
					buttons : {
						closer : true,
						sticker : false
					}
				});
			}
		});
	}
	//******************************EIMINAR PREGUNTA/CAMBIAR SU ESTADO********************************
	$("#tab_preguntas_remove").on("click", function(e) {
		new PNotify({
		      title: '<spring:message code="common.dialog.text.atencion" />',
		      text: '<spring:message code="encuestas.mantenimientoencuestas.dialog.editar_encuesta.confirm.eliminar" />',
			  hide: false,
			  confirm: { confirm: true, buttons: [{text:"<spring:message code="common.button.ok" />"},{text:"<spring:message code="common.button.cancel" />"}] },
	  		  buttons: { closer: false, sticker: false	},
	  		  history: { history: false	}
		   }).get().on('pnotify.confirm', function() {			   
			   
				var data = dtpreguntas.rows( { selected: true } ).data();
				
				var xmlList ="<list>";
				 $.each(data, function(key, fila){
					 xmlList = xmlList + "<PreguntaEncuesta>";
						xmlList = xmlList + "<id>";
						xmlList = xmlList + "<codEncuesta>"+$("#idencuesta_editar").val()+"</codEncuesta>";
						xmlList = xmlList + "<codPregunta>"+fila[0]+"</codPregunta>";
						xmlList = xmlList + "<codIdioma>1</codIdioma>";
						xmlList = xmlList + "</id>";
						xmlList = xmlList + "</PreguntaEncuesta>";
                 });
				
				 xmlList = xmlList + "</list>";
				
				$.ajax({					
					contenttype: "application/json; charset=utf-8",
					type : "post",
					url : "<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/remove_pregunta.do'/>",
					timeout : 100000,
					data: {
						  data: xmlList 
						}, 
					success : function(data) {
						dtpreguntas.rows( '.selected' ).remove().draw();						
					},
					error : function(exception) {
						new PNotify({
						      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						      text: exception.responseText,
							  type: "error",		     
							  delay: 5000,
							  buttons: { sticker: false }
						   });			
					}
				});

		   }).on('pnotify.cancel', function() {
		   });		 
		})
	//*******************GUARDAR INTERNACIONALIZACION ENCUESTA*************************	
	document.addEventListener("event-save-i18n-nombre", function(e) {
	
		$.ajax({					
			contenttype: "application/json; charset=utf-8",
			type : "post",
			url : "<c:url value='/ajax/gestionencuestas/encuestas/mantenimientoencuestas/save_internacionalizacion_encuesta.do'/>",
			timeout : 100000,
			data: {
				  data: e.detail,
				  codEncuesta: $("#idencuesta_editar").val()
				}, 
			success : function(data) {
				//cargarDatosInternacionalizacion(false);		
			},
			error : function(exception) {
				new PNotify({
				      title: '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				      text: exception.responseText,
					  type: "error",		     
					  delay: 5000,
					  buttons: { sticker: false }
				   });			
			}
		});
			
	})	
	//**********************************************	

</script>

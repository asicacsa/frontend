<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="encuestas.mantenimientoencuestas.dialog.responder.title" />		
	</h4>	
</div>



<div class="modal-body">
	<form id="form_filter_list_encuestas"class="form-horizontal form-label-left">			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12"><spring:message code="encuestas.mantenimientoencuestas.dialog.responder.field.encuesta" /></label>			
				</div>			
			</div>				
		

<div class="col-md-12 col-sm-12 col-xs-12"> 
   
	<table id="datatable-lista-encuestas" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
		<thead>
			<tr>			
			    <th></th>				
			    <th><spring:message code="encuestas.mantenimientoencuestas.dialog.responder.list.header.titulo" /></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	
	<div class="modal-footer">
			<button id="seleccionar_encuesta" type="button" class="btn btn-primary">
				<spring:message code="common.button.continue" />
			</button>
	</div>	
</div>

</form>
</div>

<script>

hideSpinner("#seleccionar_idioma");
//*******************************
var json_encuestas=${encuestas};
//*******************************
dt_encuestas=$('#datatable-lista-encuestas').DataTable( {	
    "scrollCollapse": true,
    "paging":         false, 	
     "info": false,
     "searching": false,
     select: { style: 'single'},
     language: dataTableLanguage,
     initComplete: function( settings, json ) {
    		window.setTimeout(CargarEncuestas, 100);  
        },
     "columnDefs": [
                     { "visible": false, "targets": [0]}
                   ]
     });
//*****************************************************

 function CargarEncuestas()
	{	
	
	var encuesta = json_encuestas.ArrayList.Encuesta;
		 if (encuesta.length>0)		 
		 {
				for (var i=0; i<encuesta.length; i++) {						
					dt_encuestas.row.add([
									encuesta[i].cod,          
									encuesta[i].des
												    
								]).draw();
				}	 
				
		 }
		 else
		 {
			 dt_encuestas.row.add([
								encuesta.cod,          
								encuesta.des
											    
							]).draw();
		 }
			
} 
//**************************************************
 $("#seleccionar_encuesta").on("click", function(e) {
	 var data= dt_encuestas.rows( { selected: true } ).data();	 
	 if (data.length<=0) {
			new PNotify({
			      title: '<spring:message code="common.dialog.text.error" />',
			      text: '<spring:message code="encuestas.mantenimientoencuestas.dialog.responder.list.alert.ninguna_seleccion_encuesta" />',
				  type: "error",
				  delay: 5000,
				  buttons: { sticker: false }
			   });
			return;
		}	
	 	 
	
	 var code=data[0][0];
	 
	 showButtonSpinner("#seleccionar_encuesta");
		$("#modal-dialog-form-2 .modal-content").load("<c:url value='/ajax/gestionencuestas/encuestas/encuestasIdioma_show_preguntas.do'/>?idIdioma=${idioma}&idEncuesta="+code, function() {										  
			$("#modal-dialog-form-2").modal('show');
			setModalDialogSize("#modal-dialog-form-2", "lg");
		});
	 
 })
 //********************************************************************************

</script>
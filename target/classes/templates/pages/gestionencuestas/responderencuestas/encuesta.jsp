<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<x:parse xml="${encuesta}" var="encuesta_xml" />

<div class="modal-header">
	<button type="button" class="close close_dialog" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">�</span>
	</button>
	<h4 class="modal-title">
		<spring:message code="encuestas.mantenimientoencuestas.dialog.responder.title" />		
	</h4>	
</div>
<br/>
<div id="wizard" class="form_wizard wizard_horizontal">
	 <ul class="wizard_steps anchor">
		<x:forEach select="$encuesta_xml/Encuesta/preguntaEncuestas/PreguntaEncuesta" var="pregunta">
        	<li>
        		<a href="#step-<x:out select = "$pregunta/orden" />">
        		<span class="step_no"><x:out select = "$pregunta/orden" /></span>
                <span class="step_descr">
                	<spring:message code="encuestas.mantenimientoencuestas.dialog.responder.field.pregunta" /> <x:out select = "$pregunta/orden" /><br>
				</span>
        	 	</a>
            </li>
          </x:forEach>
		</ul> 
		<div class="stepContainer" style="height: 154px;">            
        	<form id="respuesta_encuesta" class="form-horizontal form-label-left">
        		<input type="hidden" name="xml_respuestas_encuesta" id="xml_respuestas_encuesta"/>
        		<x:forEach select="$encuesta_xml/Encuesta/preguntaEncuestas/PreguntaEncuesta" var="pregunta">
        			<div id="step-<x:out select = "$pregunta/orden" />" class="content" style="display: block;">
        				<div class="col-md-1 col-sm-1 col-xs-12"></div>
        				<div class="col-md-8 col-sm-8 col-xs-12">
							<div class="form-group">  			
        						<h4 class="modal-title"><x:out select = "$pregunta/pregunta/des" /></h4>
        						<br/>   
        							<div class="clearfix"></div>                  
                     					<x:choose>
                     						<x:when select="$pregunta//pregunta/tipoRespuesta/cod='0'">
                     							<textarea name="respuesta_<x:out select = "$pregunta/id/codPregunta" />"  id="respuesta_<x:out select = "$pregunta/id/codPregunta" />" class="form-control"  ></textarea>
											</x:when>
                     					<x:otherwise>                     				
                     					<fieldset id="respuesta_<x:out select = "$pregunta/id/codPregunta" />">
                     						<x:forEach select="$pregunta/pregunta/tipoRespuesta/valorRespuestas/ValorRespuesta" var="respuesta">
			                     				<div class="form-group">
													<div class="col-md-6 col-sm-6 col-xs-6 col-md-offset-1">
														<input type="radio" name="respuesta_<x:out select = "$pregunta/id/codPregunta" />" id="respuesta_<x:out select = "$pregunta/id/codPregunta" />_<x:out select = "$respuesta/id/codTipoResp" />_<x:out select = "$respuesta/id/codValorResp" />"
															<x:if select="$respuesta/id/codValorResp='1'">checked</x:if>
												 			value="<x:out select = "$respuesta/id/codValorResp" />" class="flat" style="position: absolute; opacity: 0;">
															<strong><x:out select = "$respuesta/valorResp" /></strong>
													</div>
												</div>	
											</x:forEach>												
										</fieldset>                  	
                     				</x:otherwise>
                     			</x:choose>
			                 </div>
            	         </div>
                	</div>
        	 	</x:forEach>
        	</form>
            
         </div>
         	
	</div>
	<div class="modal-footer">
		<a type="button" class="btn btn-info" id="anterior_paso">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="common.button.preview" />"> <span class="fa fa-arrow-left"></span>
			</span>
		</a>
		<a type="button" class="btn btn-info" id="siguiente_paso">
			<span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="<spring:message code="common.button.continue" />"> <span class="fa fa-arrow-right"></span>
			</span>
		</a>
		<button id="save_encuesta_button" type="button" class="btn btn-primary save_encuesta_dialog">
			<spring:message code="common.button.save" />
		</button>
		<button type="button" class="btn btn-cancel close_dialog" data-dismiss="modal">
			<spring:message code="common.button.cancel" />
		</button>
	</div>			
	<script>
		
	hideSpinner("#seleccionar_encuesta");
//************************************
var json_encuesta=${encuesta_json};

var smartWizard;

var paso = 1;
var numeroPasos = json_encuesta.Encuesta.preguntaEncuestas.PreguntaEncuesta.length;

$("#anterior_paso").hide();

if(numeroPasos==undefined)
	$("#siguiente_paso").hide();


//************************************			
	 $(document).ready(function() {
		
		
		 		 
		 
		 smartWizard = $('#wizard').smartWizard({
	        selected: 0,
	        keyNavigation: true,
	        enableAllSteps: false,
	        labelNext:'>', // label for Next button
	        labelPrevious:'<',
	        
	        
	        
	      
	       
	        }); 
	      // $('.buttonNext').addClass('btn btn-success');
	      // $('.buttonPrevious').addClass('btn btn-primary');
	        //$('.buttonFinish').addClass('btn btn-default');
	        $('.buttonNext').hide();
	        $('.buttonPrevious').hide();
	        $('.buttonFinish').hide();
	        window.setTimeout(mostrarPregunta, 350);  
	       
	      });
	      
	      
	 $("#anterior_paso").on("click", function() {
         // Navigate previous
         paso = paso-1;
         if(paso == 1)
    	 {
    	 $("#anterior_paso").hide();
    	 }
         $("#siguiente_paso").show();
         
         
         smartWizard.smartWizard('goToStep',paso);  
         window.setTimeout(mostrarPregunta, 350); 
         return true;
     });
	 
	 $("#siguiente_paso").on("click", function() {
         // Navigate next 
         paso = paso+1;
         if(paso == numeroPasos)
        	 {
        	 $("#siguiente_paso").hide();
        	 }
         
         $("#anterior_paso").show();
        smartWizard.smartWizard('goToStep',paso);
         window.setTimeout(mostrarPregunta, 350); 
         return true;
     });
	 
	//***************************************************
	 $(".buttonNext ").on("click", function(e) {
		 
		 
		 window.setTimeout(mostrarPregunta, 350);  
	 })
	 
	//***************************************************
	 $(".buttonPrevious ").on("click", function(e) {
		 window.setTimeout(mostrarPregunta, 350);  
	 })
		//***************************************************
	      function mostrarPregunta()
	      {
	    	  var alto = $("#respuesta_encuesta").height();
	    	  $('.stepContainer').attr('style','height: '+alto+'px;');	    	 
	      }
	    //**************************************************
	    
	if ($("input.flat")[0]) {
    	$(document).ready(function () {
        	$('input.flat').iCheck({
            	checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
    }
 
//***************************************************
function guardarRespuestas()
{
	var xml="";
	var preguntas = json_encuesta.Encuesta.preguntaEncuestas.PreguntaEncuesta;
	
	xml ="<list>";
	 if (preguntas.length>0)		 
	 {
			for (var i=0; i<preguntas.length; i++) {
				var pregunta = preguntas[i]; 
				xml = xml + "<PreguntaEncuestaDTO>";
				xml = xml + "<idEncuesta>"+pregunta.id.codEncuesta +"</idEncuesta>";
				xml = xml + "<idIdioma>"+pregunta.id.codIdioma+"</idIdioma>";
				xml = xml + "<idPregunta>"+pregunta.id.codPregunta+"</idPregunta>";
				if (pregunta.pregunta.tipoRespuesta.cod == 0)
					xml = xml + "<respuesta>"+$("#respuesta_"+pregunta.id.codPregunta).val()+"</respuesta>";
				else
					xml = xml + "<respuesta>"+$("input[name='respuesta_"+pregunta.id.codPregunta+"']:checked").val()+"</respuesta>";	
				
					xml = xml + "</PreguntaEncuestaDTO>";			
			}	 
	}
	 else
	 {
			xml = xml + "<PreguntaEncuestaDTO>";
			xml = xml + "<idEncuesta>"+preguntas.id.codEncuesta +"</idEncuesta>";
			xml = xml + "<idIdioma>"+preguntas.id.codIdioma+"</idIdioma>";
			xml = xml + "<idPregunta>"+preguntas.id.codPregunta+"</idPregunta>";
			if (pregunta.tipoRespuesta.cod == 0)
			{
				xml = xml + "<respuesta>"+$("input[name='respuesta_"+preguntas.id.codPregunta+"']").val()+"</respuesta>";
			}else
			{
				xml = xml + "<respuesta>"+$("input[name='respuesta_"+preguntas.id.codPregunta+"']:checked").val()+"</respuesta>";	
			}			
			xml = xml + "</PreguntaEncuestaDTO>";
	 }
	 xml = xml + "</list>";
	 
	return xml;
	}
//***************************************************
 $("#save_encuesta_button").on("click", function(e) {
	 
	
	showSpinner("#modal-dialog-form .modal-content");
	$("#xml_respuestas_encuesta").val(guardarRespuestas());
	var data = $("#respuesta_encuesta").serializeObject();

	$.ajax({
		type : "post",
		url : "<c:url value='/ajax/gestionencuestas/encuestas/save_contestacion_respuestas_encuesta.do'/>",
		timeout : 100000,
		data : data,
		success : function(data) {
			hideSpinner("#modal-dialog-form .modal-content");
		
			$("#modal-dialog-form-2").modal('hide');
			
			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_realizada" />',
				text : '<spring:message code="common.dialog.text.datos_guardados" />',
				type : "success",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
			
		},
		error : function(exception) {
			hideSpinner("#modal-dialog-form .modal-content");

			new PNotify({
				title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
				text : exception.responseText,
				type : "error",
				delay : 5000,
				buttons : {
					closer : true,
					sticker : false
				}
			});
		}
	});
	
	
	
 })
//****************************************************
</script>
			
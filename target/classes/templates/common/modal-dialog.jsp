<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="modal fade" id="${modal_dialog_id}"
	role="dialog" aria-hidden="true" style="display: none;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close close_dialog" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">�</span>
				</button>
				<h4 class="modal-title">
					TITULO					
				</h4>
			</div>
			<div class="modal-body">
				<p class="modal-text">TEXTO</p>
				<div class="modal-footer">
					<button type="button" class="btn btn-success close_dialog"
						data-dismiss="modal">
						<spring:message code="common.button.ok" />
					</button>
				</div>

			</div>
			
		</div>
	</div>
</div>
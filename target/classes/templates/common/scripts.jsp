<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
	
<script type="text/javascript" src="<c:url value='/resources/js/jquery-2.2.4.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/jquery-barcode.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/bootstrap/dist/js/bootstrap.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/validation/js/jquery.validate.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/jszip.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/vfs_fonts.js'/>"></script>
	
<script type="text/javascript" src="<c:url value='/resources/vendors/fastclick/lib/fastclick.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/iCheck/icheck.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/jquery-textext-master/src/js/textext.core.js'/>"></script>		
<script type="text/javascript" src="<c:url value='/resources/vendors/jquery-textext-master/src/js/textext.plugin.tags.js'/>"></script>		
<script type="text/javascript" src="<c:url value='/resources/vendors/jquery-textext-master/src/js/textext.plugin.autocomplete.js'/>"></script>		

<script type="text/javascript" src="<c:url value='/resources/vendors/datatables.net/js/jquery.dataTables.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/datatables.net-responsive/js/dataTables.responsive.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/datatables.net-scroller/js/dataTables.scroller.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/datatables.net-select/js/dataTables.select.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/moment/min/moment.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/bootstrap-daterangepicker/daterangepicker.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/bootstrap-datepicker-master/dist/locales/bootstrap-datepicker.es.min.js'/>" charset="UTF-8"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/fullcalendar/fullcalendar.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/fullcalendar/locale/es.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/jquery-file-upload/js/vendor/jquery.ui.widget.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/jquery-file-upload/js/jquery.iframe-transport.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/jquery-file-upload/js/jquery.fileupload.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/dataTableLanguage.js'/>"></script>

<script type="text/javascript" src="<c:url value='/resources/vendors/pnotify/dist/pnotify.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/pnotify/dist/pnotify.buttons.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/pnotify/dist/pnotify.confirm.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/pnotify/dist/pnotify.history.js'/>"></script>

<script type="text/javascript" src="<c:url value='/resources/vendors/switchery/dist/switchery.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/select2/dist/js/select2.full.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/select2/dist/js/i18n/es.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/select2/dist/js/select2totree.js'/>"></script>

<!-- Se debe respetar el orden de carga de los Javascripts -->
<script type="text/javascript" src="<c:url value='/resources/js/spin.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/bootstrap-select.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/gentelella.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/file-input/js/fileinput.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/vendors/file-input/js/locales/es.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/colossus.js'/>"></script>


<script type="text/javascript" src="<c:url value='/resources/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js'/>"></script> 
<script type="text/javascript" src="<c:url value='/resources/js/html2canvas.js'/>"></script>

<script type="text/javascript" src="<c:url value='/resources/js/jqueryprint.js'/>"></script>
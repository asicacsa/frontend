<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link href="<c:url value='/resources/vendors/bootstrap/dist/css/bootstrap.min.css'/>"	rel="stylesheet">
<link href="<c:url value='/resources/vendors/bootstrap/dist/css/bootstrap-select.min.css'/>"	rel="stylesheet">
<link href="<c:url value='/resources/vendors/font-awesome/css/font-awesome.min.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/vendors/iCheck/skins/flat/green.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/vendors/file-input/css/fileinput.min.css'/>" media="all" rel="stylesheet" type="text/css" />
<link href="<c:url value='/resources/vendors/jquery-textext-master/src/css/textext.core.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/vendors/jquery-textext-master/src/css/textext.plugin.autocomplete.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/vendors/jquery-textext-master/src/css/textext.plugin.tags.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/vendors/datatables.net-bs/css/dataTables.bootstrap.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/vendors/datatables.net-select/css/select.dataTables.min.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/vendors/bootstrap-daterangepicker/daterangepicker.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/vendors/bootstrap-datepicker-master/dist/css/bootstrap-datepicker.min.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/vendors/pnotify/dist/pnotify.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/vendors/pnotify/dist/pnotify.buttons.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/vendors/pnotify/dist/pnotify.history.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/vendors/switchery/dist/switchery.min.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/vendors/select2/dist/css/select2.min.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/vendors/select2/dist/css/select2totree.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/vendors/fullcalendar/fullcalendar.min.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/vendors/jquery-file-upload/css/jquery.fileupload.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/css/custom.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/css/colossus.css'/>" rel="stylesheet">

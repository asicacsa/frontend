<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="nav_menu">
	<nav class="" role="navigation">
		<div class="nav toggle">
			<a id="menu_toggle"><i class="fa fa-bars"></i></a>
		</div>
		<div class="nav title">${title}</div>
		<ul class="nav navbar-nav navbar-right">

			<li class=""><a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
					<div>
						<img src="<c:url value='/resources/images/user.png'/>" alt="">
					</div>
					<div>
						${sessionScope.nombre}<br />${sessionScope.apellidos}
					</div>
					<div>
						<span class=" fa fa-angle-down"></span>
					</div>
			</a>
				<ul class="dropdown-menu dropdown-usermenu pull-right">
					<li><a href="#" onclick="changeUserPassword();"><i class="fa  pull-right"></i> <spring:message code="dashboard.profile_menu.edit_password" /></a></li>

					<li><a href="<c:url value="/logout.do"/>"><i class="fa fa-sign-out pull-right"></i> <spring:message code="dashboard.profile_menu.logout" /></a></li>

				</ul></li>

		</ul>
	</nav>
</div>

<div id="form_notice_change_password" style="display: none;">
	<form id="change_user_password_form" class="pf-form pform_custom">
		<div class="pf-element pf-heading">
			<h3 style="margin-top: 0;">
				<spring:message code="dashboard.profile_menu.edit_password" />
			</h3>
		</div>
		<div class="pf-element">
			<label> <span class="pf-label"><spring:message code="dashboard.profile_menu.old_password" /></span> <input class="pf-field form-control" type="password" name="old_password" id="old_password"/>
			</label>
		</div>
		<div class="pf-element">
			<label> <span class="pf-label"><spring:message code="dashboard.profile_menu.new_password" /></span> <input class="pf-field form-control" type="password" name="new_password" id="new_password"/>
			</label>
		</div>
		<div class="pf-element">
			<label> <span class="pf-label"><spring:message code="dashboard.profile_menu.repeat_password" /></span> <input class="pf-field form-control" type="password" name="repeat_password" id="repeat_password"/>
			</label>
		</div>
		<div class="pf-element pf-buttons pf-centered">
			<input class="pf-button btn btn-primary" type="submit" name="submit" value="<spring:message code="common.button.save" />"> <input class="pf-button btn btn-default" type="button" name="cancel" value="<spring:message code="common.button.cancel" />">
		</div>
	</form>
</div>

<script>
	function changeUserPassword() {

		var notice = new PNotify({
			text : $('#form_notice_change_password').html(),
			icon : false,
			addclass: 'change-user-password-notify',
			type : 'info',
			width : 'auto',
			hide : false,
			buttons : {
				closer : false,
				sticker : false
			},
			insert_brs : false
		});
		notice.get().find('form.pf-form').on('click', '[name=cancel]', function() {
			notice.remove();
		}).submit(function() {

			var data = $(".change-user-password-notify #change_user_password_form").serializeObject();

			$.ajax({
				type : "post",
				url : "<c:url value='/ajax/change_password.do'/>",
				timeout : 100000,
				data : data,
				success : function(data) {
					notice.update({
						title : '<spring:message code="common.dialog.text.operacion_realizada" />',
						text : '<spring:message code="common.dialog.text.datos_guardados" />',
						icon : true,
						width : PNotify.prototype.options.width,
						hide : true,
						buttons : {
							closer : true,
							sticker : false
						},
						type : 'success'
					});
				},
				error : function(exception) {
					new PNotify({
						title : '<spring:message code="common.dialog.text.operacion_no_realizada" />',
						text : exception.responseText,
						type : "error",
						delay : 5000,
						buttons : {
							closer : true,
							sticker : false
						}
					});
				}
			});

			return false;
		});
	}
</script>
